<?php
/**
 * PLAYDATE - STRIPE RETURN URI
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isSpecialist) {
    header("Location: /");
}

if (isset($_REQUEST['code'])) { // Redirect w code
    $stripeCode = $_REQUEST['code'];
    
    $token_request_body = array(
        'client_secret' => $STRIPE_CONNECT_SECRET,
        'code' => $stripeCode,
        'grant_type' => 'authorization_code'
    );
    
    //$req = curl_init($TOKEN_URI .'?' . http_build_query($token_request_body));
    $req = curl_init($STRIPE_TOKEN_URI);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POST, true );
    curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
    
    $resp = json_decode(curl_exec($req), true);
    
    // TODO: Additional error handling
    //$respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
    
    /*
    echo("----------------------------------------<br/>");
    foreach($resp as $key => $value) {
        echo "$key | $value <br/>";
    }
    echo("----------------------------------------<br/>");
    */
    
    curl_close($req);
    
    $stripeUserId = $resp['stripe_user_id'];
    
    if($stripeUserId!="") {
        $specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
        if($specialist!=null) {
            SpecialistDAO::updateSpecialistStripeInfo($specialist->getId_specialist(), $stripeUserId);
        }
    }
    
    header("Location: /specialist-dashboard.php?pay_updated=1#playpacks");
}
?>
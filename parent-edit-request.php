<?php 
/**
 * PLAYDATE - PARENT EDIT REQUEST 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$parent = ParentDAO::getParent($_SESSION["parent_id"]);

$request = new PlaydateRequest();
$request->readFromRow($_REQUEST);
if($request->getId_request()>0) {
    $request = PlaydateRequestDAO::getPlaydateRequest($request->getId_request());
    $arr = explode(",",$request->getAge_range());
    
    $weekarr = explode(",",$request->getWhich_day_week());
    
    $prefarr = explode(",",$request->getPreferred_location_type());
    
    $themearr = explode(",",$request->getTheme_of_playdate());
    
    $extraarr = explode(",",$request->getPay_extra());
    
    $invitearr = explode(",",$request->getLike_to_invite());
} else {
    header("Location: /");
}
//echo "<pre>";print_r($request->getLike_to_invite());exit;

$operation = null;
if(isset($_REQUEST["operation"])) { 
    $operation = $_REQUEST["operation"];
}

$groupName = "";
if($request->getId_group()!=null) {
    $grp = GroupDAO::getGroup($request->getId_group());
    if($grp!=null) {
        $groupName = $grp->getName();
    }
}

$updated = false;

switch ($operation) {
    case "update": {
        $newRequest = new PlaydateRequest();
        $newRequest->readFromRow($_REQUEST);
//echo "<pre>";print_r($_POST);exit;
        if($_POST['playdate_type'] == 1){
            $_POST['time_init'] = $_POST['time_init'];
            $_POST['time_end'] = $_POST['time_end'];
        }
        else{
          $_POST['time_init'] = $_POST['time_init_1'];
          $_POST['time_end'] = $_POST['time_end_1'];
        }
       unset($_POST['time_init_1']);
       unset($_POST['time_end_1']);
       
      if(!empty($_POST['group_size1'])){
          $_POST['group_size'] = $_POST['group_size1'];
      }
       unset($_POST['group_size1']);
       if(sizeof($_POST['age_range']) > 0){
          $_POST['age_range'] = implode (",", $_POST['age_range']);
       }
       if(sizeof($_POST['which_day_of_week']) > 0){
            $_POST['which_day_of_week'] = implode (",", $_POST['which_day_of_week']);
         }
         
        if(sizeof($_POST['preferred_location_type']) > 0){
          $_POST['preferred_location_type'] = implode (",", $_POST['preferred_location_type']);
       }
       if(sizeof($_POST['theme_of_playdate']) > 0){
          $_POST['theme_of_playdate'] = implode (",", $_POST['theme_of_playdate']);
       }
       if(sizeof($_POST['pay_extra_bucks_for']) > 0){
          $_POST['pay_extra_bucks_for'] = implode (",", $_POST['pay_extra_bucks_for']);
       }
       if(sizeof($_POST['like_to_invite_guest']) > 0){
          $_POST['like_to_invite_guest'] = implode (",", $_POST['like_to_invite_guest']);
       }

        
        if(isset($_REQUEST["invite_group"]) && ($_REQUEST["invite_group"]!="")) {
            $inviteGroup = GroupDAO::getGroupByName($_REQUEST["invite_group"]);
            if($inviteGroup!=null) {
                $groupName = $inviteGroup->getName();
                $request->setId_group($inviteGroup->getId_group());
            }
        }

        $link = getConnection();
			
			$sql_frm = "UPDATE pd_playdate_requests SET 
			playdate_type='" . mysql_real_escape_string($_POST['playdate_type']) . "', " .
			"date=" .(($_POST['date']!=null)?("'".mysql_real_escape_string(BaseDAO::toMysqlDateFormat($_POST['date']))."'"):"null"). ", " .
			"time_init=" .(($_POST['time_init']!=null)?("'".mysql_real_escape_string($_POST['time_init'])."'"):"null"). ", " .
      "time_end=" .(($_POST['time_end']!=null)?("'".mysql_real_escape_string($_POST['time_end'])."'"):"null"). ", " .
			"group_size=" .(($_POST['group_size']!=null)?("'".mysql_real_escape_string($_POST['group_size'])."'"):"null"). ", " .
			"age_range=" .(($_POST['age_range']!=null)?("'".mysql_real_escape_string($_POST['age_range'])."'"):"null"). ", " .
			"no_of_playdate_per_week=" .(($_POST['no_of_playdate_per_week']!=null)?("'".mysql_real_escape_string($_POST['no_of_playdate_per_week'])."'"):"null"). ", " .
			"which_day_of_week=". (($_POST['which_day_of_week']!=null)?("'" .($_POST['which_day_of_week']) . "'"):"null"). ", " .
			"location_in_mind=". (($_POST['location_in_mind']!=null)?("'" . $_POST['location_in_mind'] . "'"):"null") . ", " .
			"preferred_location=". (($_POST['preferred_location']!=null)?("'" . $_POST['preferred_location'] . "'"):"null") . ", " .
			"preferred_location_type=". (($_POST['preferred_location_type']!=null)?("'" . $_POST['preferred_location_type'] . "'"):"null") . ", " .
			"theme_of_playdate=". (($_POST['theme_of_playdate']!=null)?("'" . $_POST['theme_of_playdate'] . "'"):"null") . ", " .
			"pay_extra_bucks_for=". (($_POST['pay_extra_bucks_for']!=null)?("'" . $_POST['pay_extra_bucks_for'] . "'"):"null") . ", " .
			"notes=". (($_POST['notes']!=null)?("'" . mysql_real_escape_string($_POST['notes']) . "'"):"null") . ", " .
			"like_to_invite_guest=". (($_POST['like_to_invite_guest']!=null)?("'" . $_POST['like_to_invite_guest']. "'"):"null") . ", " .
			"information_of_guests=". (($_POST['information_of_guests']!=null)?("'" . $_POST['information_of_guests'] . "'"):"null") . ", " .
      "playdate_lead_by_specialist=". (($_POST['playdate_lead_by_specialist']!=null)?("'" . $_POST['playdate_lead_by_specialist'] . "'"):"null") . ", " .
      "other_details=". (($_POST['other_details']!=null)?("'" . $_POST['other_details'] . "'"):"null") . ", " .
      "id_group=". (($_POST['id_group']!=null)?("'" . $_POST['id_group'] . "'"):"null") . ", " .
      "private_friend_name=". (($_POST['private_friend_name']!=null)?("'" . $_POST['private_friend_name'] . "'"):"null") . ", " .
      "private_friend_email=". (($_POST['private_friend_email']!=null)?("'" . $_POST['private_friend_email'] . "'"):"null") . ", " .
			"udate=now() " .
			"WHERE id_request=" . $request->getId_request();

			//echo("::::::[$sql_frm]:::::::");die();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$request = PlaydateRequestDAO::getPlaydateRequest($request->getId_request());
    
      //  $request = PlaydateRequestDAO::updatePlaydateRequest($newRequest);
        
        if($request->getId_request()>0) {
            $updated=true;
            //Insertamos sus intereses
            // InterestDAO::deleteAllRequestInterests($newRequest->getId_request());
            // if(!empty($_POST['interests'])) {
            //     foreach($_POST['interests'] as $interest) {
            //         InterestDAO::createRequestInterest($newRequest->getId_request(), $interest);
            //     }
            // }
        
            //Eliminamos asociaci�n children
            // PlaydateRequestDAO::deleteAllRequestChildren($request->getId_request());
            // //Insertamos los children
            // if(!empty($_POST['selectedchildren'])) {
            //     foreach($_POST['selectedchildren'] as $auxchild) {
            //         $childInsert = ChildrenDAO::getChildren($auxchild);
            //         PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), $auxchild, 1, $childInsert->getAge());
            //     }
            // }
            
            //Insertamos las localizaciones
            // NeighborhoodDAO::deleteAllRequestNeighborhood($newRequest->getId_request());
            // if(!empty($_POST['id_neighborhood'])) {
            //     foreach($_POST['id_neighborhood'] as $auxlocation) {
            //         NeighborhoodDAO::createRequestNeighborhood($newRequest->getId_request(), $auxlocation);
            //     }
            // }
            
            //Insertamos specialists
            PlaydateRequestDAO::deleteAllRequestSpecialist($newRequest->getId_request());
            if(!empty($_POST['id_specialist'])) {
                foreach($_POST['id_specialist'] as $auxSpec) {
                    PlaydateRequestDAO::addSpecialistToParentRequest($newRequest->getId_request(), $auxSpec);
                }
            }
            
            //Aplicamos las reservas solicitadsa
            //Borramos las antiguas plazas reservadas antes de crear las que nos mandan
            // PlaydateRequestDAO::deleteAllRequestChildren($newRequest->getId_request());
            
            // if(!empty($_POST['selectedchildren'])) {
            //     foreach($_POST['selectedchildren'] as $auxchild) {
            //         $kid = ChildrenDAO::getChildren($auxchild);
            //         if($kid!=null) {
            //             PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), $kid->getId_children(), 1, $kid->getAge()); //Added age and numchildren
            //         }
            //     }
            // }
            
            // if(!empty($_POST['num_kids'])) {
            //     $i=0;
            //     foreach($_POST['num_kids'] as $auxnum) {
            //         $numKids = $auxnum;
            //         $age= $_POST['ages'][$i];
                    
            //         if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
            //             PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), null, $numKids, $age); //Added age and numchildren, without id_children
            //         }
            //         $i++;
            //     }
            // }
            
            header("Location: /my-dashboard.php#requests");
        }
        
        
        
        break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

  <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit Request - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
	<style>
	.disabled{cursor: not-allowed; pointer-events: none;}
.time_error{color:red};
	    #frm_change_request {
  background-color: #ffffff;
  /* margin: 100px auto; */
  font-family: 'Poppins', sans-serif;
  padding: 0px 40px;
  /* width: 70%; */
  min-width: 300px;
}

h1 {
  text-align: center;  
  color:#724985
}
label{ padding: 10px 0px}
.red{color: red}
.p-color{color:#724985;margin-bottom: 10px;}
.subText{font-size: 10px;
    font-weight: 400;
    font-style: normal;
    line-height: 9px;
    color: #595d64;}
input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: 'Poppins', sans-serif;
  border: 2px solid #878787 !important;
  text-align: left!important;
}
.mb-10{margin-right: 10px!important;margin-top: 4px!important;}
/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd !important;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: 'Poppins', sans-serif;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}
.radioType{border-radius:10px!important; margin: 5px 5px 5px 0 !important;}
input[type="radio"]:focus{border-radius:10px!important; margin: 5px 5px 5px 0 !important;}

input{margin : 0px!important}
#type_yes_div{margin-top:20px}
#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}
	</style>
  </head>

  <body class="simple-page request-playdate full-day<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1 style="color:#fff">Change a Request</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo (($parent->getPicture()==null)?"/img/img-profile.jpg":$parent->getPicture()) ?>" alt="Picture of <?php echo $parent->getFullName() ?>">
                </div>
                <div class="info">
                  <a href="parent-public-profile.html" class="title"><?php echo $parent->getName() ?></a>
                  <span class="subtitle"><?php echo $parent->getUsername() ?></span>
                  <span class="subtitle members">Chelsea, NYC</span>
                </div>
                <div class="data">
                  <p class="titulo">Groups</p>
                  <?php 
                  $parentGrps = GroupDAO::getGroupsByParent($parent->getId_parent());
                  foreach ($parentGrps as $pGrp) {
                  ?>
                  <p><a href="/groups-dashboard.php?id_group=<?php echo $pGrp->getId_group() ?>" style="text-decoration:none;"><?php echo $pGrp->getName() ?></a></p>
                  <?php } ?>
                </div>
                <div class="data">
                  <p class="titulo">Children</p>
				  <?php 
				  $childsParent = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                  foreach ($childsParent as $pchild) {
                      $genderimg = "img/ico/main/0299-baby2.svg";
                      if($pchild->getGenre()=="girl") {
                          $genderimg = "img/ico/main/0298-baby.svg";
                      }
                  ?>
                  <p class="children"><img src="<?php echo $genderimg ?>" width="50" alt=""/> <span class="name"><?php echo $pchild->getName() ?></span> | <span class="age"><?php echo $pchild->getAge() ?></span></p>
                  <?php 
                  } 
                  ?>                  

                </div>
                <div class="data">
                  <p class="titulo">Availability</p>
                  <p><span class="day">Mon</span> <span class="hour"><?php echo(($parent->getChildren_avb_mon_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_init()):"")?> - <?php echo(($parent->getChildren_avb_mon_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_end()):"")?></span></p>
                  <p><span class="day">Tue</span> <span class="hour"><?php echo(($parent->getChildren_avb_tue_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_init()):"")?> - <?php echo(($parent->getChildren_avb_tue_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_end()):"")?></span></p>
                  <p><span class="day">Wed</span> <span class="hour"><?php echo(($parent->getChildren_avb_wed_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_init()):"")?> - <?php echo(($parent->getChildren_avb_wed_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_end()):"")?></span></p>
                  <p><span class="day">Thu</span> <span class="hour"><?php echo(($parent->getChildren_avb_thu_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_init()):"")?> - <?php echo(($parent->getChildren_avb_thu_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_end()):"")?></span></p>
                  <p><span class="day">Fri</span> <span class="hour"><?php echo(($parent->getChildren_avb_fri_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_init()):"")?> - <?php echo(($parent->getChildren_avb_fri_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_end()):"")?></span></p>
                  <p><span class="day">Sat</span> <span class="hour"><?php echo(($parent->getChildren_avb_sat_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_init()):"")?> - <?php echo(($parent->getChildren_avb_sat_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_end()):"")?></span></p>
                  <p><span class="day">Sun</span> <span class="hour"><?php echo(($parent->getChildren_avb_sun_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_init()):"")?> - <?php echo(($parent->getChildren_avb_sun_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_end()):"")?></span></p>
                </div>
              </div>

			  <?php /*?>
              <div class="data playdate-near">
                <p class="titulo">Playdates near you</p>
                <div class="info-near">
                  <a href="#" title="">Bronx Day Trip</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
                <div class="info-near">
                  <a href="#" title="">Dance Party</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
                <div class="info-near">
                  <a href="#" title="">Met Photo Exhibit</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
              </div>
              <?php */ ?>


        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <form class="form-request-playdate" id="frm_change_request" action="/parent-edit-request.php" method="post">
          	<input type="hidden" name="operation" value="update" /> 
          	<input type="hidden" name="id_parent" value="<?php echo $parent->getId_parent() ?>" />
          	<input type="hidden" name="id_request" value="<?php echo $request->getId_request() ?>" />
            
            <div class="tab"><h1>Playdate Type</h1>
              <label class="p-color">I Need A...<span class="red">*</span></label>
              <div class="row">
                <div class="col-6"><input type="radio" id="type_yes" class="radioType" name="playdate_type" <?php if($request->getPlaydate_type() == 1) echo "checked=checked"?>  value="1" onclick="divShow(1)"> One Off Playdate (or Party)</div>
                <div class="col-6" style="padding-left: 0px;"><input type="radio" id="type_no" class="radioType" name="playdate_type"  <?php if($request->getPlaydate_type() == 2) echo "checked=checked"?>  value="2" onclick="divShow(2)"> Recurring Playdate (1x or more per week)</div>
              </div> 
              
              <div id="type_yes_div">
                <p class="p-color">Date<span class="red">*</span></p>
                  <div class="input-group">
                    <input type="text" placeholder="MM/DD/YYYY" name="date" class="form-control datepicker readonly" readonly="readonly" id="dp1524041141130" style="text-align:left!important" value="<?php echo $request->getDate()?>">
                  </div>

                  <p class="p-color">Time<span class="red">*</span></p>
                <div id="time-container-profile">
                  <div class="day-time">
                    <div class="input-group" style="position: absolute;">
                        <div class="selectTime from">
                          <?php 
                          $fieldName = "time_init";
                          $fieldValue = $request->getTime_init();
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                    <span style="margin-left: 427px;"> - </span>
                    <div class="input-group" style="float: right;">
                    	<div class="selectTime to" style="position: absolute;">
                          <?php 
                          $fieldName = "time_end";
                          $fieldValue = $request->getTime_end();
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                  </div>
                </div>
                <span class="time_error"></span>
                  <p class="p-color">Group Size</p>
                  <input placeholder="No. of Kids - No. of Kids"  type="text"  name="group_size" value="<?php echo $request->getGroup_size()?>" pattern="^ *(([1-9])|([1-9] *- *[1-9])|([1-9] *- *[1-9][0-9])|([1-9][0-9] *- *[1-9][0-9])) *$">
                  <span id="groupErr" style="color:red"></span><br/>

                  <p class="p-color">Age Range</p>
                  <!--<p id="age_error" style="color:red"></p><br/>-->
                  <input type="checkbox" class="mb-10"    name="age_range[]" value="1" <?php if (in_array("1", $arr)) {echo "checked=checked";}?>/>
                     Under 3
                      <br />
                      <input type="checkbox" class="mb-10"    name="age_range[]" value="2" <?php if (in_array("2", $arr)) {echo "checked=checked";}?>/>
                      3 - 5
                      <br />
                      <input  type="checkbox" class="mb-10"    name="age_range[]" value="3" <?php if (in_array("3", $arr)) {echo "checked=checked";}?>/>
                      5 - 7
                      <br />
                      <input type="checkbox" class="mb-10"    name="age_range[]" value="4" <?php if (in_array("4", $arr)) {echo "checked=checked";}?>/>
                      7 or up
                 
              </div>

              <div id="type_no_div">
                <p class="p-color">I need .. (number) Playdates per week</p>
                <input type="radio" name="no_of_playdate_per_week" class="radioType" value="1" <?php if($request->getNo_of_playdate() == 1) echo "checked=checked"?>> 1 - 2<br>
                <input type="radio" name="no_of_playdate_per_week" class="radioType" value="2" <?php if($request->getNo_of_playdate() == 2) echo "checked=checked"?>> 2 - 3<br>
                <input type="radio" name="no_of_playdate_per_week" class="radioType" value="3" <?php if($request->getNo_of_playdate() == 3) echo "checked=checked"?>> 3 - 4<br>
                <input type="radio" name="no_of_playdate_per_week" class="radioType"  value="4" <?php if($request->getNo_of_playdate() == 4) echo "checked=checked"?>> 4 - 5<br>
                <input type="radio" name="no_of_playdate_per_week" class="radioType" value="5" <?php if($request->getNo_of_playdate() == 5) echo "checked=checked"?>> 5 - 6<br>
                <input type="radio" name="no_of_playdate_per_week" class="radioType" value="6" <?php if($request->getNo_of_playdate() == 6) echo "checked=checked"?>> 6 - 7<br>

                  <p class="p-color">Which day of the week?</p>
                  <input type="checkbox" class="mb-10" name="which_day_of_week[]" value="1" <?php if (in_array("1", $weekarr)) {echo "checked=checked";}?>/>
                     Monday
                      <br />
                      <input type="checkbox" class="mb-10" name="which_day_of_week[]" value="2" <?php if (in_array("2", $weekarr)) {echo "checked=checked";}?>/>
                     Tuesday
                      <br />
                      <input type="checkbox" class="mb-10" name="which_day_of_week[]" value="3" <?php if (in_array("3", $weekarr)) {echo "checked=checked";}?>/>
                      Wednesday
                      <br />
                      <input type="checkbox" class="mb-10" name="which_day_of_week[]" value="4" <?php if (in_array("4", $weekarr)) {echo "checked=checked";}?>/>
                      Thursday
                      <br />
                      <input type="checkbox" class="mb-10" name="which_day_of_week[]" value="5" <?php if (in_array("5", $weekarr)) {echo "checked=checked";}?>/>
                     Friday
                      <br />
                      <input type="checkbox" class="mb-10" name="which_day_of_week[]" value="6" <?php if (in_array("6", $weekarr)) {echo "checked=checked";}?>/>
                     Saturday
                      <br />
                      <input type="checkbox" class="mb-10" name="which_day_of_week[]" value="7" <?php if (in_array("7", $weekarr)) {echo "checked=checked";}?>/>
                     Sunday
                      <br />
                  
                  <p class="p-color">Time<span class="red">*</span></p>
                <div id="time-container-profile">
                  <div class="day-time">
                    <div class="input-group" style="position: absolute;">
                        <div class="selectTime from">
                          <?php 
                          $fieldName = "time_init_1";
                          $fieldValue = $request->getTime_init();
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                    <span style="margin-left: 427px;"> - </span>
                    <div class="input-group" style="float: right;">
                    	<div class="selectTime to" style="position: absolute;">
                          <?php 
                          $fieldName = "time_end_1";
                          $fieldValue = $request->getTime_end();
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                  </div>
                </div>
                <span class="time_error"></span>
                  <p class="p-color">Group Size</p>
                  <input placeholder="No. of Kids - No. of Kids" type="text" name="group_size1" value="<?php echo $request->getGroup_size()?>" pattern="^ *(([1-9])|([1-9] *- *[1-9])|([1-9] *- *[1-9][0-9])|([1-9][0-9] *- *[1-9][0-9])) *$">
                  <span id="groupErr1" style="color:red"></span><br/>

                  <p class="p-color">Age Range</p>
                  <input type="checkbox"  class="mb-10" name="age_range[]" value="1" <?php if (in_array("1", $arr)) {echo "checked=checked";}?>/>
                     Under 3
                      <br />
                      <input type="checkbox"    class="mb-10" name="age_range[]" value="2" <?php if (in_array("2", $arr)) {echo "checked=checked";}?>/>
                      3 - 5
                      <br />
                      <input type="checkbox"    class="mb-10" name="age_range[]" value="3" <?php if (in_array("3", $arr)) {echo "checked=checked";}?>/>
                      5 - 7
                      <br />
                      <input type="checkbox"    class="mb-10" name="age_range[]" value="4" <?php if (in_array("4", $arr)) {echo "checked=checked";}?>/>
                      7 or up
                  
              </div>
              <!-- <p><input placeholder="First name..."    name="fname"></p>
              <p><input placeholder="Last name..."    name="lname"></p> -->
            </div>
            <div class="tab"><h1>Alright Pal! Let's Get Started!</h1>
            <p class="p-color">Do you have a location in mind?</p>
              <input type="radio" name="location_in_mind" id="loc_mind_yes" class="radioType" value="1" onclick="divShow(3)"  <?php if($request->getLocation_in_mind() == 1) echo "checked=checked"?>> Yes<br>
              <input type="radio" name="location_in_mind" id="loc_mind_no" class="radioType" value="2" onclick="divShow(4)"  <?php if($request->getLocation_in_mind() == 2) echo "checked=checked"?>> No<br>
              <div id="location_yes">
               <p class="p-color">Preferred location</p>
               <textarea rows="6" cols="50" name="preferred_location"><?php echo ($request->getPreferred_location() != "null")? $request->getPreferred_location(): "";?></textarea>
              </div>
              <div id="location_no">
              <p class="p-color">What is your preferred location type?</p>
              <input type="checkbox" class="mb-10"    name="preferred_location_type[]" value="1" <?php if (in_array("1", $prefarr)) {echo "checked=checked";}?>/>
              Playground<br />
              <input type="checkbox" class="mb-10"    name="preferred_location_type[]" value="2" <?php if (in_array("2", $prefarr)) {echo "checked=checked";}?>/>
              Home <br />
              <input type="checkbox" class="mb-10"    name="preferred_location_type[]" value="3" <?php if (in_array("3", $prefarr)) {echo "checked=checked";}?>/>
              Public Attraction<br />
              <input type="checkbox" class="mb-10"    name="preferred_location_type[]" value="4" <?php if (in_array("4", $prefarr)) {echo "checked=checked";}?>/>
              Drop In Class <br />
              <input type="checkbox" class="mb-10"    name="preferred_location_type[]" value="5" <?php if (in_array("5", $prefarr)) {echo "checked=checked";}?>/>
              Kidpass Booking<br />
              <input type="checkbox" class="mb-10"    name="preferred_location_type[]" value="6" <?php if (in_array("6", $prefarr)) {echo "checked=checked";}?>/>
              Other
              </div>
            </div>

            <div class="tab"><h1>We're Getting There, Pal!</h1>
            <p class="p-color">Now for the fun part! Pick a theme for your Playdate...</p>
              <input type="checkbox" class="mb-10"    name="theme_of_playdate[]" value="1" <?php if (in_array("1", $themearr)) {echo "checked=checked";}?>/>
                      Anything fun!
                      <br />
                      <input type="checkbox" class="mb-10"    name="theme_of_playdate[]" value="2" <?php if (in_array("2", $themearr)) {echo "checked=checked";}?>/>
                      Academic
                      <br />
                      <input type="checkbox" class="mb-10"    name="theme_of_playdate[]" value="3" <?php if (in_array("3", $themearr)) {echo "checked=checked";}?>/>
                      Creative
                      <br />
                      <input type="checkbox" class="mb-10"    name="theme_of_playdate[]" value="4" <?php if (in_array("4", $themearr)) {echo "checked=checked";}?>/>
                     Active
                      <br />
                      <input type="checkbox" class="mb-10"    name="theme_of_playdate[]" value="5" <?php if (in_array("5", $themearr)) {echo "checked=checked";}?>/>
                      Parent Social
                      <br />
                      <span class="subText">Parent Socials include parents on site (think restaurant kids table or Mommy&Me). Caregivers are on site to help supervise and engage so parents can socialize too!</span>
                
            
                <p class="p-color">I'm willing to throw in a few extra bucks for...  </p>
                <input type="checkbox" class="mb-10"    name="pay_extra_bucks_for[]" value="1" <?php if (in_array("1", $extraarr)) {echo "checked=checked";}?>/>
                      Food
                      <br />
                      <input type="checkbox" class="mb-10"    name="pay_extra_bucks_for[]" value="2" <?php if (in_array("2", $extraarr)) {echo "checked=checked";}?>/>
                      Extra Supplies
                      <br />
                      <input type="checkbox" class="mb-10"    name="pay_extra_bucks_for[]" value="3" <?php if (in_array("3", $extraarr)) {echo "checked=checked";}?>/>
                      Admission
                      <br />
                      <input type="checkbox" class="mb-10"    name="pay_extra_bucks_for[]" value="4" <?php if (in_array("4", $extraarr)) {echo "checked=checked";}?>/>
                      Travel
                      <br />
                      <input type="checkbox" class="mb-10"    name="pay_extra_bucks_for[]" value="5" <?php if (in_array("5", $extraarr)) {echo "checked=checked";}?>/>
                      Extra Specialist
                      <br />
                      <input type="checkbox" class="mb-10"  name="pay_extra_bucks_for[]" value="6" <?php if (in_array("6", $extraarr)) {echo "checked=checked";}?>/>
                     No Thanks!<br />          
                <p class="p-color">Notes? (Optional)</p>
                <textarea rows="3" cols="50" name="notes"><?php echo ($request->getNotes() != "null")? $request->getNotes(): "";?></textarea>
            </div>

            <div class="tab"><h1>We're So Close, Pal.</h1>
              <p class="p-color">Life is always more fun with friends! I'd like to invite...</p>
              <input type="checkbox" class="mb-10"  name="like_to_invite_guest[]" value="1" <?php if (in_array("1", $invitearr)) {echo "checked=checked";}?> onclick="showFriendDiv(this)"/>
              Private Friend Group
              
              <div id="private_friend" style="display:none">
                <input type="text" id ="private_friend_name" name="private_friend_name" placeholder="Name" value="<?php echo $request->getPrivate_friend_name()?>"><br /><br />
                <input type="email" id ="private_friend_email" name="private_friend_email" placeholder="Email Address" value="<?php echo $request->getPrivate_friend_email()?>" onblur="validateEmail(this);"> 
                <span id="emailerror" style="color: red;"></span>
              </div>
              <br />
              <input type="checkbox" class="mb-10"  name="like_to_invite_guest[]" value="2" <?php if (in_array("2", $invitearr)) {echo "checked=checked";}?> />
              Anyone in the PAL Community
                      <br />
              <input type="checkbox" class="mb-10"  name="like_to_invite_guest[]" value="3" <?php if (in_array("3", $invitearr)) {echo "checked=checked";}?> />
              Anyone on Kidpass
                      <br />
               
              <p class="p-color">Input their information here:</p>
              <textarea rows="10" cols="50" placeholder="Name <email address>.."   name="information_of_guests"><?php echo ($request->getInformation_of_guest() != "null")? $request->getInformation_of_guest(): "";?></textarea>
            
              <!--<p class="p-color">I would like to request a specific Specialist to lead my playdate<span class="red">*</span></p>-->
              <!--<input type="radio" class="radioType" name="playdate_lead_by_specialist"  value="1" onclick="divShow(5)"> Yes<br>-->
              <!--<input type="radio" class="radioType" name="playdate_lead_by_specialist"  value="2" onclick="divShow(6)"> No<br> -->
              
              <!--<div id="specialist_info">-->
              <!--  <p class="p-color">Input Specialist information here: </p>-->
             
                <!--<input type="text" id="inputSpecialistName" class="form-control noMarginTop inputSpecialist" placeholder="Name"  autocomplete="new-password" />-->
                <!--<input type="text" id ="specialist_first_name" name="specialist_first_name" placeholder="First name" value=""><br /><br />-->
                <!--<input type="text" id ="specialist_last_name" name="specialist_last_name" placeholder="Last name" value=""> -->
                
              <!--</div>   -->
               <p class="p-color">Private PAL Group:</p>
               <input type="text" id="inputSelectGroup" name="invite_group" class="form-control noMarginTop" placeholder="Group Name" value="<?php echo $groupName ?>">
               
               <p class="p-color">I would like to request a specific Specialist to lead my playdate</p>
                  <input type="text" id="inputSpecialistName" class="form-control noMarginTop inputSpecialist" placeholder="Name"  autocomplete="new-password" />
                  <a href="javascript:;" class="bt-save-profile last bt-add-specialist" data-toggle="modal" data-target="#playdate-reserved" style="float:left">Add Specialist</a>
                  
                  <div class="row">                  
                <div class="col-lg-12">  
                  <div class="container-specialist-tags row-specialist">
                   <?php
                    $specialists = SpecialistDAO::getSpecialistsListByRequest($request->getId_request());
                    foreach ($specialists as $auxSpec) {
                        $pic = "/img/img-profile.jpg";
                        if(($auxSpec->getPicture()!=null) && ($auxSpec->getPicture()!="")) {
                            $pic = $auxSpec->getPicture();
                        }
                    ?>
                	<div class="specialist-tag">
                      <img src="<?php echo $pic ?>">
                      <a href="/specialist-profile.php?id_specialist=<?php echo $auxSpec->getId_specialist() ?>" title="<?php echo $auxSpec->getFullName()?>"><?php echo $auxSpec->getFullName()?></a>
                      <a class="bt-delete-tag" href="javascript:;" title="Remove specialist">X</a>
                      <input type="hidden" name="id_specialist[]" value='<?php echo $auxSpec->getId_specialist(); ?>'/>
                    </div>
                
                    <?php 
                    }
                    ?>
                  </div>
                </div>
              </div>
               <br />
              <p class="p-color">Any other details?</p>
              <textarea rows="10" cols="50"  placeholder="Allergies, Diapers, Special Needs" name="other_details"><?php echo ($request->getOther_details() != "null")? $request->getOther_details(): "";?></textarea>
              
              <!--<p class="p-color">How did you hear about us?<span class="red">*</span></p>-->
              <!--<input type="text" name="how_did_you_hear_about_us" value=""> -->
              <br /><br />
            </div>

          <!--  <div class="tab"><h1>We're SO HAPPY to have you Pal!</h1>-->
          <!--  <p class="p-color">Your Full Name<span class="red">*</span></p>-->
          <!--  <input type="text" name="first_name" placeholder="First Name" value=""><br /><br />-->
          <!--  <input type="text" name="last_name" placeholder="Last Name" value=""> -->
           
          <!-- <p class="p-color">Email Address<span class="red">*</span></p>-->
          <!-- <input type="email" name="email_address"  value="" style="border: 2px solid #878787;" onblur="validateEmail(this);"><br />-->
          <!--<span id="emailerror" style="color: red;"></span>-->
          <!--  </div><br />-->
            <br />
            <div style="overflow:auto; margin-top:15px">
              <div>
                <button type="button" id="prevBtn" onclick="nextPrev(-1)" style="float:left;">Previous</button>
                <button type="button" id="nextBtn" onclick="nextPrev(1)" style="float:right;">Next</button>
              </div>
            </div>
            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center;margin-top:40px;">
              <span class="step"></span>
              <span class="step"></span>
              <span class="step"></span>
              <span class="step"></span>
              <!--<span class="step"></span>-->
            </div>
            
          

          </form>
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

<?php if($request->getPlaydate_type() == 1) { ?>
document.getElementById("type_yes_div").style.display = "block";
document.getElementById("type_no_div").style.display = "none";
<?php } else { ?>
document.getElementById("type_no_div").style.display = "block";
document.getElementById("type_yes_div").style.display = "none";

<?php } if($request->getLocation_in_mind() == 1) { ?>
document.getElementById("location_yes").style.display = "block";
document.getElementById("location_no").style.display = "none";
<?php } else{?>
document.getElementById("location_no").style.display = "block";
document.getElementById("location_yes").style.display = "none";
<?php }  if (in_array("1", $invitearr)) { ?>
//alert('hi');
document.getElementById("private_friend").style.display = "block";
<?php } else{?>
document.getElementById("private_friend").style.display = "none";
<?php }?>
function showFriendDiv(val){
    if(val.checked == true){
        document.getElementById("private_friend").style.display = "block";
    }
    else{
        document.getElementById("private_friend").style.display = "none";
    }
}
function divShow(t){
  if(t ==1){
    document.getElementById("type_yes_div").style.display = "block";
    document.getElementById("type_no_div").style.display = "none";

  }

  if(t ==2){
    document.getElementById("type_no_div").style.display = "block";
    document.getElementById("type_yes_div").style.display = "none";
  }

  if(t == 3){
    document.getElementById("location_yes").style.display = "block";
    document.getElementById("location_no").style.display = "none";
  }

  if(t == 4){
    document.getElementById("location_no").style.display = "block";
    document.getElementById("location_yes").style.display = "none";
  }

//   if(t == 5){
//      document.getElementById("private_friend").style.display = "block";
//   }
//   if(t == 5){
//     document.getElementById("specialist_info").style.display = "block";
//     var innerTags =  document.getElementById("specialist_info").innerHTML;
//     if(innerTags != ""){
//         document.getElementById("specialist_info").innerHTML = innerTags;
//       }
//       else{
//         document.getElementById("specialist_info").innerHTML = "<p class='p-color'>Input Specialist information here: </p><input type='text' id ='specialist_first_name' name='specialist_first_name' placeholder='First name'><br /><br /><input type='text' id ='specialist_last_name' name='specialist_last_name' placeholder='Last name'>";
//       }
//   }
//   if(t == 6){
//     document.getElementById("specialist_info").innerHTML = "";
//     document.getElementById("specialist_info").style.display = "none";
//   }
}

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Save Changes";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("frm_change_request").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
   document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

 $('input[name=group_size]').on('blur', function(){
    validateField(this,'groupErr')
  });
  
   $('input[name=group_size1]').on('blur', function(){
    validateField(this,'groupErr1')
  });


function validateField(field,type){
    var value = field.value;
     var res = value.match(/^ *(([1-9])|([1-9] *- *[1-9])|([1-9] *- *[1-9][0-9])|([1-9][0-9] *- *[1-9][0-9])) *$/g);
     if(res !== null){
         var str = res[0].split('-');
         //alert(str)
         if(parseInt(str[0]) >= parseInt(str[1])){
              $('#'+type).html('Invalid format');
              $('#nextBtn').addClass('disabled');
         }
         else{
             $('#'+type).html('');
              $('#nextBtn').removeClass('disabled');
         }
     }
     else{
         $('#'+type).html('Invalid format');
         $('#nextBtn').addClass('disabled');
     }
}

function validateEmail(emailField){
        var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (reg.test(emailField.value) == false) 
        {
            $('#emailerror').html('Invalid Email Address');
            return false;
        }else{
            $('#emailerror').html('');
        }

        return true;

}

function AcceptDateCharacters(event, separator)
{
    if(separator.length != 1)  //only pass single character separators here
    {
        return false;
    }
    //lets allow digits
    var expression = "^[0-9";
    
    //lets allow the separator character
    expression += separator;
    
    //lets complete the expression
    expression += "]$";
    
    var regex = new RegExp(expression);    
    return AcceptRegExOnly(event, regex)  
};

function AcceptRegExOnly(event, regex)  
{   
    var keyCode = event.which ? event.which : event.keyCode;
    
    var keyPressed = String.fromCharCode(keyCode);
    return regex.test(keyPressed);
}; 

 $(".from").click(function () {
        var init = $('input[name=time_init]').val();
        if(init != ""){
             var end = $('input[name=time_end]').val();
             if(parseInt(init) >= parseInt(end) && end != ''){
                 //  alert('yes');
                 $('.time_error').html('Please select correct time');
                // $('input[name=time_init]').val(default_value);
                $('#nextBtn').addClass('disabled');
             }
             else{
               //  alert('1st');
                 $('.time_error').html('');
                 $('#nextBtn').removeClass('disabled');
             }
        }
        
        
        var init_1 = $('input[name=time_init_1]').val();
        if(init_1 != ""){
             var end_1 = $('input[name=time_end_1]').val();
             if(parseInt(init_1) >= parseInt(end_1) && end_1 != ''){
                 //  alert('yes');
                 $('.time_error').html('Please select correct time');
                // $('input[name=time_init]').val(default_value);
                $('#nextBtn').addClass('disabled');
             }
             else{
               //  alert('1st');
                 $('.time_error').html('');
                 $('#nextBtn').removeClass('disabled');
             }
        }
    });
    
    $(".to").click(function () {
        var end = $('input[name=time_end]').val();
        if(end != ""){
             var init = $('input[name=time_init]').val();
            //  alert(init);
            //  alert(end);
             if(parseInt(init) >= parseInt(end)){
            //     alert('hi');
                 $('.time_error').html('Please select correct time');
                  $('#nextBtn').addClass('disabled');
                // $('input[name=time_init]').val(default_value);
              //   alert('Please select correct time');
             }
             else{
              $('#nextBtn').removeClass('disabled');
                 $('.time_error').html('');
             }
        }
        
        var end_1 = $('input[name=time_end_1]').val();
        if(end_1 != ""){
             var init_1 = $('input[name=time_init_1]').val();
            //  alert(init);
            //  alert(end);
             if(parseInt(init_1) >= parseInt(end_1)){
            //     alert('hi');
                 $('.time_error').html('Please select correct time');
                  $('#nextBtn').addClass('disabled');
                // $('input[name=time_init]').val(default_value);
              //   alert('Please select correct time');
             }
             else{
              $('#nextBtn').removeClass('disabled');
                 $('.time_error').html('');
             }
        }
    });
    
    //alert($('#private_friend').css('display'));
function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  if(currentTab == 0){
    z = document.getElementById("type_yes_div").getAttribute("style");
    n =  document.getElementById("type_no_div").getAttribute("style");
    if(z == 'display: block;'){
      y =  document.getElementById('type_yes_div').getElementsByTagName('input')
      //alert(z.innerHtml);
    }
    
    if(n == 'display: block;'){
        y =  document.getElementById('type_no_div').getElementsByTagName('input')
    }
  }

    else if(currentTab == 3){
       if($('#private_friend').css('display') == 'block'){
         y = document.getElementById('private_friend').getElementsByTagName('input');
        }
        else{
            y = "";
        }
   
  }
  
  else{
       y = x[currentTab].getElementsByTagName("input");
  }
 
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    //  console.log(y[i].name+" "+y[i].value);
    if(y[i].name == 'date' || y[i].name == 'time' || y[i].name == 'time1' || y[i].name == 'age_range[]' || y[i].name == 'location_in_mind' || y[i].name == 'time_init' || y[i].name == 'time_init_1'
    || y[i].name == 'time_end' || y[i].name == 'time_end_1' || y[i].name == 'private_friend_name' || y[i].name == 'private_friend_email') {
          
           if((y[i].name == 'time_init' ||  y[i].name == 'time_init_1') && y[i].value == ""){
              $('.from').css("background-color", "#ffdddd !important");
              valid = false;
          }
          else if((y[i].name == 'time_end' ||  y[i].name == 'time_end_1') && y[i].value == ""){
              $('.to').css("background-color", "#ffdddd !important");
              valid = false;
          }
    // If a field is empty...
    else if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
   }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>
  </body>

</html>

<?php
/**
 * PLAYDATE - PARENTS - MY DASHBOARD SECTION
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isParent) {
    header("Location: /");
    
}
 
//Parent connected. Get full info
$parent = ParentDAO::getParent($_SESSION["parent_id"]);
$_SESSION['updateProfile'] = 0;
$selectedTab = "profile";
// $check = "";
//Selected Playdate to show in detail
$selectedPastPlaydate = null;
$selectedPlaydate = null;
$paramPlaydate = new Playdate();
$paramPlaydate->readFromRow($_REQUEST);
if($paramPlaydate->getId_playdate()>0) {
    $paramPlaydate = PlaydateDAO::getPlaydate($paramPlaydate->getId_playdate());
    
    if(($paramPlaydate!=null) && ($paramPlaydate->getId_playdate()>0)) {
        $selectedPlaydate = $paramPlaydate;
    }
} 

if(isset($_REQUEST["id_past_playdate"]) && ($_REQUEST["id_past_playdate"]!=null)) {
    $paramPastPlaydate = PlaydateDAO::getPlaydate($_REQUEST["id_past_playdate"]);
    if(($paramPastPlaydate!=null) && ($paramPastPlaydate->getId_playdate()>0)) {
        $selectedPastPlaydate = $paramPastPlaydate;
    }
}


$invited = false;
$invitedToPal = false;
$passwordchanged = false;
$nominated = false;
if(isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        
        case "nominate": {
            if(isset($_REQUEST["nominate_email"]) && ($_REQUEST["nominate_email"]!=null) && ($_REQUEST["nominate_email"]!="")) {
                MailUtils::sendNominateSpecialist($_REQUEST["nominate_email"]);
                $nominated = true;
            }
            
            break;
        }
        
        case "invite-friends": {
            if(!empty($_POST['inviteFriendsPopup']) && ($selectedPlaydate!=null)) {
                $name="Someone";
                if($parent!=null) {
                    $name = $parent->getName();
                } else if($specialist!=null) {
                    $name = $specialist->getName();
                }
                
                $event_place = $selectedPlaydate->getLoc_address().", ".$selectedPlaydate->getLoc_city().", ".$selectedPlaydate->getLoc_state();
                
                foreach($_POST['inviteFriendsPopup'] as $auxfriend) {
                    $friend = ParentDAO::getParentByUsernameOrEmail($auxfriend);
                    if($friend!=null) {
                        MailUtils::sendPlaydateInviteFriend($friend->getEmail(), $auxfriend, $name, $selectedPlaydate->getName(), Utils::dateFormat($selectedPlaydate->getDate()), $event_place, Utils::get12hourFormat($selectedPlaydate->getTime_init()),Utils::get12hourFormat($selectedPlaydate->getTime_end()), "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
                    } else if(filter_var($auxfriend, FILTER_VALIDATE_EMAIL)) {
                        MailUtils::sendPlaydateInviteFriend($auxfriend, $auxfriend, $name, $selectedPlaydate->getName(), Utils::dateFormat($selectedPlaydate->getDate()), $event_place, Utils::get12hourFormat($selectedPlaydate->getTime_init()),Utils::get12hourFormat($selectedPlaydate->getTime_end()), "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
                    }
                    
                }
                $invited = true;
            }
            
            
            break;
        }
        
        case "invite-friends-to-pal": {
            if(!empty($_POST['inviteFriendsPopup'])) {

                $name="Someone";
                if($parent!=null) {
                    $name = $parent->getName();
                } else if($specialist!=null) {
                    $name = $specialist->getName();
                }
                
                foreach($_POST['inviteFriendsPopup'] as $auxfriend) {
                    if(filter_var($auxfriend, FILTER_VALIDATE_EMAIL)) {
                        
                        //Create new Invitation
                        $newInvitation = InvitationDAO::createInvitationForEmail($auxfriend);
                        if($newInvitation!=null) {
                            MailUtils::sendPlaydateInvitationCode($newInvitation->getEmail(), $name, $newInvitation->getCode());
                        }
                    }
                    
                }
                $invitedToPal = true;
            }
            
            break;
        }
        
        case "addgroup": {
            if(isset($_REQUEST["name"]) && ($_REQUEST["name"]!=null) && ($_REQUEST["name"]!="")) {
                $newgroup = GroupDAO::getGroupByName($_REQUEST["name"]);
                
                if(($newgroup!=null) && ($newgroup->getId_group()>0)) {
                    if($newgroup->getPrivacity()==Group::$PRIVACITY_PRIVATE) {
                        //Si el grupo es privado, creamos solicitud
                        GroupDAO::createGroupParentRequest($newgroup->getId_group(), $parent->getId_parent());
                    } else if($newgroup->getPrivacity()==Group::$PRIVACITY_PUBLIC) {
                        //Si el grupo es p�blico, unimos al padre directamente
                        GroupDAO::parentJoinGroup($newgroup->getId_group(), $parent->getId_parent(), false);
                    }
                }
            }
            
            break;
        }
        
        case "updateprofile": {
            
            $editParent = $parent;
            $editParent->readFromRow($_REQUEST);
            
            //si hay fichero, subimos
            if(count($_FILES)>0) {
                $currentDir = getcwd();
                $uploadDirectory = "/uploads/";
                
                $errors = []; // Store all foreseen and unforseen errors here
                
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                
                $fileName = $_FILES['formfile']['name'];
                $fileSize = $_FILES['formfile']['size'];
                $fileTmpName  = $_FILES['formfile']['tmp_name'];
                $fileType = $_FILES['formfile']['type'];
                
                $tmpArr = explode('.',$fileName);
                $tmpExt = end($tmpArr);
                $fileExtension = strtolower($tmpExt);
                
                $uploadPath = $currentDir . $uploadDirectory . basename($fileName);
                
                $i=0;
                while (file_exists($uploadPath)) {
                    $i++;
                    $uploadPath = $currentDir . $uploadDirectory .$i."_".basename($fileName);
                }
                
                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }
                
                if ($fileSize > 24000000) {
                    $errors[] = "This file is more than 24MB. Sorry, it has to be less than or equal to 24MB";
                }
                
                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                    
                    if ($didUpload) {
                        $editParent->setPicture($uploadDirectory.basename($fileName)); //Actualizamos la ruta
                    } else {
                        echo "<!-- An error occurred somewhere. Try again or contact the admin -->";
                    }
                } else {
                    foreach ($errors as $error) {
                        //echo $error . "These are the errors" . "\n";
                    }
                }
            } //hay fichero
            
            //UPDATE CHILDREN
            for($i=1;$i<10;$i++) {
                //buscamos children
                $prefixChild = "ch".$i."_";
                if(isset($_REQUEST[$prefixChild."id_children"])) {
                    $formChildren = new Children();
                    $formChildren->readFromRow($_REQUEST, $prefixChild);
                    if($formChildren->getId_children()>0) {
                        $formChildren = ChildrenDAO::updateChildren($formChildren);
                        //Insert children interests
                        InterestDAO::deleteAllChildrenInterests($formChildren->getId_children());
                        if(!empty($_POST[$prefixChild.'interests'])) {
                            foreach($_POST[$prefixChild.'interests'] as $interest) {
                                InterestDAO::createChildrenInterest($formChildren->getId_children(), $interest);
                            }
                        }
                        
                        if(isset($_POST[$prefixChild.'remove']) && ($_POST[$prefixChild.'remove']==1)) {
                            //Orden de eliminar ni�o
                            ChildrenDAO::deleteChildren($formChildren->getId_children());
                            
                        }
                        
                    } 
                }
            }
            
            //NEW CHILDREN
            for($i=1;$i<10;$i++) {
                //buscamos children
                $prefixNewChild = "newch_".$i."_";
                if(isset($_REQUEST[$prefixNewChild."name"]) && ($_REQUEST[$prefixNewChild."name"]!=null)) {
                    $formNewChildren = new Children();
                    $formNewChildren->readFromRow($_REQUEST, $prefixNewChild);
                    $formNewChildren->setId_parent($parent->getId_parent());
                    ChildrenDAO::createChildren($formNewChildren);
                    
                } 
            }
            
            //UPDATE EMERGENCY
            for($i=1;$i<10;$i++) {
                //buscamos emergency
                $prefixEmergency = "em".$i."_";
                if(isset($_REQUEST[$prefixEmergency."id_emergency"])) {
                    $formEmergency = new Emergency();
                    $formEmergency->readFromRow($_REQUEST, $prefixEmergency);
                    $formEmergency->setId_parent($parent->getId_parent());
                    if($formEmergency->getId_emergency()>0) {
                        EmergencyDAO::updateEmergency($formEmergency);
                    }
                }
            }
            
            //NEW EMERGENCY
            for($i=1;$i<10;$i++) {
                //buscamos emergency
                $prefixNewEmergency = "newem_".$i."_";
                if(isset($_REQUEST[$prefixNewEmergency."name"]) && ($_REQUEST[$prefixNewEmergency."name"]!=null)) {
                    $formNewEmergency = new Emergency();
                    $formNewEmergency->readFromRow($_REQUEST, $prefixNewEmergency);
                    $formNewEmergency->setId_parent($parent->getId_parent());
                    EmergencyDAO::createEmergency($formNewEmergency);
                    
                } 
            }
            
            //INSERT LOCATIONS
            NeighborhoodDAO::deleteAllParentNeighborhood($parent->getId_parent());
            if(!empty($_POST['id_neighborhood'])) {
                foreach($_POST['id_neighborhood'] as $auxlocation) {
                    NeighborhoodDAO::createParentNeighborhood($parent->getId_parent(), $auxlocation);
                }
            }
            
            //UPDATE MAIN PROFILE
            $parent = ParentDAO::updateParent($editParent);
            
            if(isset($_POST['new-password']) && ($_POST['new-password']!=null) && ($_POST['new-password']!="")) {
                ParentDAO::changePassword($parent, $_POST['new-password']);
                $passwordchanged = true;
            }
            $_SESSION['checkLogin'] = 0;
            $completed = ParentDAO::isCompletedProfile($_SESSION["parent_id"]);
            if($completed) {
              $_SESSION['updateProfile'] = 1;
            }else{
              $_SESSION['updateProfile'] = 0;
            }
            //header("Location: /my-dashboard.php#nav-current");       
             break;

        }
        
        case "creategroup": {
            $newGroup = new Group();
            $newGroup->readFromRow($_REQUEST);
            if(($newGroup->getName()!=null) && ($newGroup->getName()!="")) {
                $newGroup = GroupDAO::createGroup($newGroup);
                if($newGroup->getId_group()>0) {
                    GroupDAO::parentJoinGroup($newGroup->getId_group(), $parent->getId_parent(), true); //El creador es administrador
                    header("Location: /groups-dashboard.php?id_group=".$newGroup->getId_group()."#profile");
                }
            }
            break;
        }
        
        case "invitationaccept": {
            $invitation = new GroupInvitation();
            $invitation->readFromRow($_REQUEST);
            if($invitation->getId_invitation()>0) {
                $invitation = GroupDAO::getGroupParentInvitation($invitation->getId_invitation());
                
                if(($invitation!=null) && ($invitation->getId_invitation()>0) && ($invitation->getId_parent()==$parent->getId_parent())) {
                    //ACCEPT INVITATION
                    GroupDAO::acceptGroupParentInvitation($invitation->getId_invitation());
                    GroupDAO::parentJoinGroup($invitation->getId_group(), $parent->getId_parent(), false);
                    
                    header("Location: /groups-dashboard.php?id_group=".$invitation->getId_group());
                }
            }
            break;
        }
        case "invitationdecline": {
            $invitation = new GroupInvitation();
            $invitation->readFromRow($_REQUEST);
            if($invitation->getId_invitation()>0) {
                $invitation = GroupDAO::getGroupParentInvitation($invitation->getId_invitation());
                
                if(($invitation!=null) && ($invitation->getId_invitation()>0) && ($invitation->getId_parent()==$parent->getId_parent())) {
                    //DECLINE INVITATION
                    GroupDAO::declineGroupParentInvitation($invitation->getId_invitation());
                }
            }
            break;
        }
        
        case "createrating": {
            //id_past_playdate
            $newRating = new Rating();
            $newRating->readFromRow($_REQUEST);
            $newRating->setId_parent($parent->getId_parent());
            $newRating->setId_playdate($selectedPastPlaydate->getId_playdate());
            $newRating->setId_specialist($selectedPastPlaydate->getId_specialist());
            
            $newRating = RatingDAO::createRating($newRating);
            
            break;
        }
        
        case "updaterating": {
            //id_past_playdate
            $newRating = new Rating();
            $newRating->readFromRow($_REQUEST);
            $newRating->setId_parent($parent->getId_parent());
            $newRating->setId_playdate($selectedPastPlaydate->getId_playdate());
            $newRating->setId_specialist($selectedPastPlaydate->getId_specialist());
            
            $newRating = RatingDAO::updateRating($newRating);
            
            break;
        }
        
        case "createupdateselected": {
            $comment = new Comment();
            $comment->readFromRow($_REQUEST);
            $comment->setId_playdate($selectedPlaydate->getId_playdate());
            $comment->setId_parent($parent->getId_parent());
            
            CommentDAO::createComment($comment);
            
            //Mail to playdate lead
            
            //Enviamos notificacion
            $pdTime =  Utils::dateFormat($selectedPlaydate->getDate())." ".Utils::get12hourFormat($selectedPlaydate->getTime_init())." - ".Utils::get12hourFormat($selectedPlaydate->getTime_end());
            $leadName = "No Lead";
            $leadMail = null;
            if($selectedPlaydate->getId_specialist()!=null) {
                $pdSpecialist = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                if($pdSpecialist!=null) {
                    $leadName = $pdSpecialist->getFullName();
                    $leadMail = $pdSpecialist->getEmail();
                }
            }
            
            if($leadMail!=null) {
                MailUtils::sendPlaydateParentUpdate($leadMail, $leadName, $selectedPlaydate->getName(), $pdTime, $comment->getComment(), $leadName, "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
            }
            
            break;
        }
        
        case "createupdatepast": {
            $comment = new Comment();
            $comment->readFromRow($_REQUEST);
            $comment->setId_playdate($selectedPastPlaydate->getId_playdate());
            $comment->setId_parent($parent->getId_parent());
            
            CommentDAO::createComment($comment);
            break;
        }
    }
    
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>My Dashboard - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
    <style type="text/css">body.modal-open {
             overflow: hidden;
             height :  100vh;
             /*position: fixed!important;*/
            }
             .type{color: #5b5b5b;
    font-weight: bold;}
    .invalid {
  background-color: #ffdddd !important;
}

    </style>
    <?php if($selectedPastPlaydate!=null || $selectedPlaydate!=null) { ?>
        <style type="text/css">
            .my-dashboard .tab-content, .my-dashboard .nav-tabs{display: none;}
           
        </style>
    <?php } ?>
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page my-dashboard<?php echo($isConnected?" connected":"") ?>">

     <!-- DATOS INCOMPLETOS -->
    <div class="modal fade itinerary-modal" id="incomplete-data" tabindex="-1" role="dialog" style="padding-right: 15px;
    display: block;
    background-size: cover;">
    <!--<span id="notclose" style="float: right;"><i class="fa fa-close" style="font-size: 30px;color:#000;font-weight: normal;"></i></span>-->
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="color: #ccc;border: 10px solid;background-color: rgba(0,0,0,0.5);background: url(../img/4.png) no-repeat center;">
          <div class="modal-header" style="background: none; background-color: rgba(0,0,0,0.5);">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="closeButton;font-size: 30px!important;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="background: none; background-color: rgba(0,0,0,0.5);">
            
            <div class="row">
              <h2 class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:50px">NOT SO FAST, PAL!</h2>
              <br />
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:17px;text-transform: uppercase;">I know, I know, We're excited too! </p><br />
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:17px;text-transform: uppercase;">Before you explore, </p><br />
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:17px;text-transform: uppercase;">Please complete your profile. </p><br/>
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:17px;text-transform: uppercase;">You're almost there!</p><br/>
              <!--<p class="itinerary-modal-heading noUppercase" style="color: #fff;font-size:18px "><a href="/my-dashboard.php#nav-profile" style="color: #fff;">Complete profile >></a></p>-->

            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- DATOS INCOMPLETOS -->
 
    <!-- complete profile -->
    <div class="modal fade itinerary-modal" id="complete-profile" tabindex="-1" role="dialog" style="padding-right: 15px;
    display: block; background-size: cover;">
      <!--<span id="yesclose" style="float: right;"><i class="fa fa-close" style="font-size: 30px;color:#000;font-weight: normal;"></i></span>-->
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="color: #ccc;border: 10px solid;background-color: rgba(0,0,0,0.5);background: url(../img/51.jpg) no-repeat center;">
          <div class="modal-header" style="background: none;background-color: rgba(0,0,0,0.5) !important;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="closeButton;font-size: 30px!important;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="background: none;background-color: rgba(0,0,0,0.5) !important;">
            
            <div class="row">
              <h2 class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:50px">You Did It!</h2>
              <br />
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:17px;text-transform: uppercase;">Your profile looks great! </p><br />
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:17px;text-transform: uppercase;">Now, let's do this thing.</p><br />
              <br/>
              <div class="col-4" style="color: #fff;
    text-transform: uppercase;
    font-weight: bold;text-align: center;">
    <a href="/playdates.php" style="color:#fff"><i class="fa fa-search" aria-hidden="true" style="
    margin-right: 10px;
    font-size: 25px;
"></i>Search</a></div>
              <div class="col-4" style="color: #fff;
    text-transform: uppercase;
    font-weight: bold;text-align: center;">
              <a href="/parent-request-playdate.php" id="request" style="color:#fff"><i class="fa fa-pencil-square-o" aria-hidden="true" style="
    margin-right: 10px;
    font-size: 25px;
"></i>Request</a>
              </div>
              <div class="col-4" style="color: #fff;
    text-transform: uppercase;
    font-weight: bold;text-align: center;">
    <a href="/groups.php" style="color:#fff"><i class="fa fa-users" aria-hidden="true" style="
    margin-right: 10px;
    font-size: 22px;
"></i>Build</a></div>
            </div>

          </div>
        </div>
      </div>
    </div>

 	<?php /* DASHBOARD WELCOME 
 	
    <!-- dashboard welcome -->
    <div class="modal fade" id="dashboard-welcome" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="dashboard-welcome-heading">Welcome to Playdate</h2>
            <div class="row">
              <div class="col-lg-6"><a class="bt-top" href="playdates.php" title="">Search Playdate</a></div>
              <div class="col-lg-6"><a class="bt-top" href="/parent-request-playdate.php" title="">Request Playdate</a></div>
            </div>
            <div class="row">
            <h2 class="dashboard-welcome-heading noUppercase">Suggested Playdates in Your Area</h2>
              <div class="col-lg-4">
                <div class="box-playdates attractions">
                  <div class="img-container">
                    <img src="img/box-home-1.jpg" alt="">
                    <span class="cat-box">NYC Attractions</span>
                  </div>
                  <div class="time">
                    <span class="date">Jan 13th 2018</span>
                    <span class="hour">12pm-2pm</span>
                  </div>
                  <div class="info">
                    <span class="title">Bronx Zoo Day Trip</span>
                    <span class="subtitle">Bronx, NYC</span>
                  </div>
                   <div class="data">
                    <div class="age">Ages: <span>7-8</span></div>
                    <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                    <div class="specialists">Specialist: <span>Anna Smith</span></div>
                  </div>
                  <div class="reserve">
                    <a href="#" class="bt-reserve">Reserve playdate</a>
                  </div>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="box-playdates creative">
                  <div class="img-container">
                    <img src="img/box-home-2.jpg" alt="">
                    <span class="cat-box">Creative</span>
                  </div>
                  <div class="time">
                    <span class="date">Jan 13th 2018</span>
                    <span class="hour">12pm-2pm</span>
                  </div>
                  <div class="info">
                    <span class="title">Color exploring</span>
                    <span class="subtitle">Bronx, NYC</span>
                  </div>
                   <div class="data">
                    <div class="age">Ages: <span>7-8</span></div>
                    <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                    <div class="specialists">Specialist: <span>Anna Smith</span></div>
                  </div>
                  <div class="reserve">
                    <a href="#" class="bt-reserve">Reserve playdate</a>
                  </div>
                </div>
              </div>


              <div class="col-lg-4">
                <div class="box-playdates outdoor">
                  <div class="img-container">
                    <img src="img/box-home-3.jpg" alt="">
                    <span class="cat-box">Outdoor</span>
                  </div>
                  <div class="time">
                    <span class="date">Jan 13th 2018</span>
                    <span class="hour">12pm-2pm</span>
                  </div>
                  <div class="info">
                    <span class="title">Outdoors we go</span>
                    <span class="subtitle">Bronx, NYC</span>
                  </div>
                   <div class="data">
                    <div class="age">Ages: <span>7-8</span></div>
                    <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                    <div class="specialists">Specialist: <span>Anna Smith</span></div>
                  </div>
                  <div class="reserve">
                    <a href="#" class="bt-reserve">Reserve playdate</a>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- dashboard welcome -->
    */?>
    
    <!-- Invite friends -->
    <div class="modal fade itinerary-modal" id="invite-friends-modal" class="itinerary-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          	<form name="frm_invite_popup" action="/my-dashboard.php" id="frm_invite_popup" method="post">
          	<input type="hidden" name="action" value="invite-friends-to-pal">
            <h2 class="itinerary-modal-heading">INVITE FRIENDS TO PAL</h2>
            
            <div class="row">
              <h2 class="itinerary-modal-heading noUppercase">Playdates are more fun with friends!</h2>
            </div>

            <div class="row row-zip-code">

              <div class="col-lg-2"></div>
              <div class="col-lg-8">
                <label for="inputInviteFriends" class="sr-only">Email</label>
                <input type="text" id="inputInviteFriends" class="form-control noMarginTop input-invite-friend" placeholder="Email" name="inviteFriendsPopup[]">
                <?php if($invitedToPal) { ?>
                <p style="text-align:center;">Invitations have been sent!</p>
                <?php } ?>
                <p class='errorLocation'></p>
              </div>
              <div class="col-lg-2"></div>

              <div class="col-lg-12">
                <div class="container-tags container-tags-email">
                  
                </div>
              </div>
              
            </div> 

            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-top" href="javascript:document.frm_invite_popup.submit();" title="">Invite Parents</a></div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-link-reserved-playdate" href="#" title="">Nominate a specialist</a></div>
              <div class="col-lg-2"></div>
            </div>
			</form>
          </div>
        </div>
      </div>
    </div>
    <!-- Invite friends -->

	<?php include ('components/menu.php'); ?>

				<?php if($selectedPastPlaydate!=null) { 
				    $specialist = SpecialistDAO::getSpecialist($selectedPastPlaydate->getId_specialist());
				    $rating = RatingDAO::getRatingByParentPlaydate($parent->getId_parent(), $selectedPastPlaydate->getId_playdate());
				    
				    //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
				    if(($selectedPastPlaydate->getId_request()!=null) && is_numeric($selectedPastPlaydate->getId_request())) {
				        $minAgePd = 100;
				        $maxAgePd = -1;
				        $reservations = PlaydateDAO::getChildrenReservations($selectedPastPlaydate->getId_playdate());
				        foreach ($reservations as $auxReserv) {
				            if($auxReserv->getAge()>$maxAgePd) {
				                $maxAgePd = $auxReserv->getAge();
				            }
				            
				            if($auxReserv->getAge()<$minAgePd) {
				                $minAgePd = $auxReserv->getAge();
				            }
				        }
				        
				        
				        if(($maxAgePd>=0) && ($minAgePd<100)) {
				            $selectedPastPlaydate->setAge_init($minAgePd);
				            $selectedPastPlaydate->setAge_end($maxAgePd);
				        }
				        
				    }
				?>
                <!-- last playdate -->
                <?php 
                $ratingvalue = 0;
                if($rating!=null) {
                    $ratingvalue = $rating->getValue();
                }
                ?>
                <div class="modal fade" id="last-playdate" class="last-playdate" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form class="form-last-playdate" action="/my-dashboard.php?id_past_playdate=<?php echo $selectedPastPlaydate->getId_playdate()?>#past" method="post">
                        	<input type="hidden" name="id_past_playdate" value="<?php echo $selectedPastPlaydate->getId_playdate()?>" />
                        	<input type="hidden" name="value" id="vote" value="0" />
                        	<?php if($rating!=null) { ?>
                        		<input type="hidden" name="action" value="updaterating" />
                        		<input type="hidden" name="id_rating" value="<?php echo $rating->getId_rating() ?>" />
                        	<?php } else { ?>
                        		<input type="hidden" name="action" value="createrating" />
                        	<?php } ?>
                          <h2 class="form-last-playdate-heading">How was your playdate? </h2>
                          <div class="playdate-info">
                            <div class="titulo"><?php echo $selectedPastPlaydate->getName() ?></div>
                            <div class="info">
                              <span class="fecha"><?php echo(Utils::dateFormat($selectedPastPlaydate->getDate())) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_end()) ?></span>
                              <span class="place">Bronx, NYC</span>
                            </div>
                          </div>
                          <div class="specialist">
                            <div class="name"><?php echo $specialist->getFullName() ?>  <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Rate your Specialist here by clicking 1-5 circles!"><i class="fa fa-question-circle"></i></a></div>
                            <div class="puntuacion">
                              <ul>
                                <li id="voto1" class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                                <li id="voto2" class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                                <li id="voto3" class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                                <li id="voto4" class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                                <li id="voto5" class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                              </ul>
                              <span class="num"><?php echo $ratingvalue ?>/5</span>
                            </div>
                          </div>
                          <div class="col-lg-12">                    
                                 <label for="aboutSpecialist" class="sr-only">About Specialist</label>
                                 <textarea name="comment_specialist" class="form-control" id="aboutSpecialist" placeholder="Tell us about your Lead Specialist! Your feedback will appear as a Parent Review on the Specialist's profile." rows="4"><?php echo(($rating!=null)?$rating->getComment_specialist():"")?></textarea>
                                 <label for="aboutPlaydate" class="sr-only">About Playdate</label>
                                 <textarea name="comment_playdate" class="form-control" id="aboutPlaydate" placeholder="Tell us about your playdate experience. Your feedback will be public in the playdate comments section." rows="4"><?php echo(($rating!=null)?$rating->getComment_playdate():"")?></textarea>
                            </div>
                          <button class="btn btn-md btn-primary" type="submit">Send</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- last playdate -->
                <?php } ?>
    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>My Dashboard</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $parent->getPicture() ?>" alt="">
                </div>
                <div class="info">
                  <a href="parent-public-profile.php" class="title"><?php echo $parent->getFullName() ?></a>
                  <span class="subtitle"><?php echo $parent->getUsername() ?></span>
                  <?php 
				$neigh = NeighborhoodDAO::getNeighborhoodsListByParent($parent->getId_parent());
				foreach($neigh as $auxNeigh) {
				    $city = NeighborhoodDAO::getCity($auxNeigh->getId_city());
				?>
				<span class="subtitle members"><?php echo $auxNeigh->getName() ?>, <?php echo $city->getName() ?></span>
				<?php 
				}
				?>
                </div>
                <div class="data">
                  <p class="titulo">Groups</p>
                  <?php 
                  $parentGrps = GroupDAO::getGroupsByParent($parent->getId_parent());
                  foreach ($parentGrps as $pGrp) {
                  ?>
                  <p><?php echo $pGrp->getName() ?></p>
                  <?php } ?>
                </div>
                <div class="data">
                  <p class="titulo">Children</p>
                  <?php 
                  $childsParent = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                  foreach ($childsParent as $pchild) {
                      $genderimg = "img/ico/main/0299-baby2.svg";
                      if($pchild->getGenre()=="girl") {
                          $genderimg = "img/ico/main/0298-baby.svg";
                      }
                  ?>
                  <p class="children"><img src="<?php echo $genderimg ?>" width="50" alt=""/> <span class="name"><?php echo $pchild->getName() ?></span> | <span class="age"><?php echo $pchild->getAge() ?></span></p>
                  <?php 
                  } 
                  ?>
                </div>
                <div class="data">
                  <p class="titulo">Availability <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="By keeping your calendar up to date, we can suggest playdates in your area that are most relevant to your needs."><i class="fa fa-question-circle"></i></a></p>
                  <p><span class="day">Mon</span> <span class="hour"><?php echo(($parent->getChildren_avb_mon_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_init()):"")?> - <?php echo(($parent->getChildren_avb_mon_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_end()):"")?></span></p>
                  <p><span class="day">Tue</span> <span class="hour"><?php echo(($parent->getChildren_avb_tue_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_init()):"")?> - <?php echo(($parent->getChildren_avb_tue_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_end()):"")?></span></p>
                  <p><span class="day">Wed</span> <span class="hour"><?php echo(($parent->getChildren_avb_wed_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_init()):"")?> - <?php echo(($parent->getChildren_avb_wed_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_end()):"")?></span></p>
                  <p><span class="day">Thu</span> <span class="hour"><?php echo(($parent->getChildren_avb_thu_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_init()):"")?> - <?php echo(($parent->getChildren_avb_thu_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_end()):"")?></span></p>
                  <p><span class="day">Fri</span> <span class="hour"><?php echo(($parent->getChildren_avb_fri_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_init()):"")?> - <?php echo(($parent->getChildren_avb_fri_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_end()):"")?></span></p>
                  <p><span class="day">Sat</span> <span class="hour"><?php echo(($parent->getChildren_avb_sat_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_init()):"")?> - <?php echo(($parent->getChildren_avb_sat_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_end()):"")?></span></p>
                  <p><span class="day">Sun</span> <span class="hour"><?php echo(($parent->getChildren_avb_sun_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_init()):"")?> - <?php echo(($parent->getChildren_avb_sun_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_end()):"")?></span></p>
                </div>
              </div>

              <div class="data playdate-near">
                <p class="titulo">Near you</p>
                <?php $playdatesNear = PlaydateDAO::getPlaydatesList(1, 3, null, null);?>
                <?php foreach($playdatesNear as $auxNear) { 
                    $linkNear = "/playdate.php?id_playdate=".$auxNear->getId_playdate()."&slug=".$auxNear->getSlug();
                ?>
                <div class="info-near">
                  <a href="<?php echo $linkNear?>" title=""><?php echo $auxNear->getName() ?></a>
                  <p><span class="day"><?php echo Utils::dateFormat($auxNear->getDate()) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($auxNear->getTime_init())?> - <?php echo Utils::get12hourFormat($auxNear->getTime_end())?></span></p>
                </div>
                <?php } ?>
              </div>

        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9 my-dashboard">
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-current-tab" data-toggle="tab" href="#nav-current" role="tab" aria-controls="nav-home" aria-selected="true">Current Playdates</a>
              <a class="nav-item nav-link" id="nav-request-tab" data-toggle="tab" href="#nav-request" role="tab" aria-controls="nav-request" aria-selected="false">Requests</a>
              <a class="nav-item nav-link" id="nav-past-tab" data-toggle="tab" href="#nav-past" role="tab" aria-controls="nav-past" aria-selected="false">Past</a>
              <a class="nav-item nav-link" id="nav-groups-tab" data-toggle="tab" href="#nav-groups" role="tab" aria-controls="nav-groups" aria-selected="false" >Groups</a>
              <a class="nav-item nav-link" id="nav-specialists-tab" data-toggle="tab" href="#nav-specialists" role="tab" aria-controls="nav-specialists" aria-selected="false">Specialists</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>              
              <a class="nav-item nav-link" id="nav-playpacks-tab" data-toggle="tab" href="#nav-playpacks" role="tab" aria-controls="nav-playpacks" aria-selected="false">Playpacks</a>
              <a class="nav-item nav-link bt-invite-friends"  data-toggle="modal" data-target="#invite-friends-modal"  href="javascript:;">Invite Friends</a>
            </div>
          </nav>

          <div class="tab-content" id="nav-tabContent">

            <div class="tab-pane fade show active" id="nav-current" role="tabpanel" aria-labelledby="nav-current-tab">
              <div class="row row-tabs">
              	<?php 
              	$upcomingPlaydates = PlaydateDAO::getUpcomingPlaydatesListByParent($parent->getId_parent(), 1, 8, null, null);
              	foreach ($upcomingPlaydates as $auxPlaydate) {
              	    
              	    //TODO - De momento elegimos el primero para mostrar el detalle posterior
              	    $link = "/my-dashboard.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
              	    
              	    $interestName = "";
              	    $interestType = "";
              	    
              	    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
              	    $auxInterest = null;
              	    if(count($interests)>0) {
              	        $auxInterest = $interests[0];
              	        $interestName =$auxInterest->getName();
              	        
              	        switch ($auxInterest->getType()) {
              	            case Interest::$TYPE_TRAINING: {
              	                $interestType=" stem";
              	                break;
              	            }
              	            case Interest::$TYPE_ART: {
              	                $interestType=" creative";
              	                break;
              	            }
              	            case Interest::$TYPE_PLAY: {
              	                $interestType=" outdoor";
              	                break;
              	            }
              	            case Interest::$TYPE_CULTURAL: {
              	                $interestType=" attractions";
              	                break;
              	            }
              	        }
              	    }
              	    
              	    $openSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
              	    $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
              	    
              	    $selectedBox = (($selectedPlaydate!=null) && ($selectedPlaydate->getId_playdate()==$auxPlaydate->getId_playdate()));
              	    
              	    //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
              	    if(($auxPlaydate->getId_request()!=null) && is_numeric($auxPlaydate->getId_request())) {
              	        $minAgePd = 100;
              	        $maxAgePd = -1;
              	        $reservations = PlaydateDAO::getChildrenReservations($auxPlaydate->getId_playdate());
              	        foreach ($reservations as $auxReserv) {
              	            if($auxReserv->getAge()>$maxAgePd) {
              	                $maxAgePd = $auxReserv->getAge();
              	            }
              	            
              	            if($auxReserv->getAge()<$minAgePd) {
              	                $minAgePd = $auxReserv->getAge();
              	            }
              	        }
              	        
              	        
              	        if(($maxAgePd>=0) && ($minAgePd<100)) {
              	            $auxPlaydate->setAge_init($minAgePd);
              	            $auxPlaydate->setAge_end($maxAgePd);
              	        }
              	        
              	    }
              	    
              	?>

                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?><?php echo($selectedBox?" selected-box":"")?>">
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="info">
                      <span class="title"><a href="<?php echo $link ?>"><?php echo ($auxPlaydate->getName()) ?></a></span>
                      <span class="subtitle"><?php echo($auxPlaydate->getLoc_state()) ?></span>
                    </div>
                     <div class="data">
                      <div  class="age"><span><?php echo $auxPlaydate->getAge_init() ?>-<?php echo $auxPlaydate->getAge_end() ?> years old</span></div>
                      <div class="participants"><span><?php echo $auxPlaydate->getNum_children() ?> kids / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                      <?php if($specialist!=null) { ?>
                  		<div class="specialists">Specialist <span><a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                  	   <?php } ?>
                    </div>
                  </div>
                </div>
				<?php } ?>
                <?php 
                if(($upcomingPlaydates==null) || (count($upcomingPlaydates)<=0)) {
                ?>
                  <div class="col-lg-12">
                    <div class="no-results"><img src="img/cactus.png"/>
                      <p>You have no upcoming playdates reserved. </p>
                      <p>Book a playdate today because life is always better with friends!</p>
                    </div>
                  </div>
                <?php   
                }
                ?>
              </div>
              
              <?php if($selectedPlaydate!=null) {
              
                  $interestName = "";
                  $interestType = "";
                  
                  $interests = InterestDAO::getInterestsByPlaydate($selectedPlaydate->getId_playdate());
                  $auxInterest = null;
                  if(count($interests)>0) {
                      $auxInterest = $interests[0];
                      $interestName =$auxInterest->getName();
                      
                      switch ($auxInterest->getType()) {
                          case Interest::$TYPE_TRAINING: {
                              $interestType=" stem";
                              break;
                          }
                          case Interest::$TYPE_ART: {
                              $interestType=" creative";
                              break;
                          }
                          case Interest::$TYPE_PLAY: {
                              $interestType=" outdoor";
                              break;
                          }
                          case Interest::$TYPE_CULTURAL: {
                              $interestType=" attractions";
                              break;
                          }
                      }
                  }
                  
                  $openSpots = $selectedPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($selectedPlaydate->getId_playdate());
                  $specialist = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                  
                  //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
                  if(($selectedPlaydate->getId_request()!=null) && is_numeric($selectedPlaydate->getId_request())) {
                      $minAgePd = 100;
                      $maxAgePd = -1;
                      $reservations = PlaydateDAO::getChildrenReservations($selectedPlaydate->getId_playdate());
                      foreach ($reservations as $auxReserv) {
                          if($auxReserv->getAge()>$maxAgePd) {
                              $maxAgePd = $auxReserv->getAge();
                          }
                          
                          if($auxReserv->getAge()<$minAgePd) {
                              $minAgePd = $auxReserv->getAge();
                          }
                      }
                      
                      
                      if(($maxAgePd>=0) && ($minAgePd<100)) {
                          $selectedPlaydate->setAge_init($minAgePd);
                          $selectedPlaydate->setAge_end($maxAgePd);
                      }
                      
                  }
                  
              ?>
              
              <div id="selected-playdate" class="row row-current-dashboard row-eq-height">
                <div class="col-lg-4 vcenter" >
                  <div class="box-playdates <?php echo $interestType ?> no-text">
                    <div class="img-container">
                      <a href="/playdate.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>"><img src="<?php echo $selectedPlaydate->getPicture() ?>" alt=""></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                  <div class="info-current-dashboard">
                    <div class="info">
                      <span class="title"><a href="/playdate.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>"><?php echo $selectedPlaydate->getName() ?></a></span>
                      <span class="subtitle"><?php echo $selectedPlaydate->getLoc_city() ?></span>
                    </div>
                     <div class="data">
                      <p  class="place">
                        <span class="address"><?php echo $selectedPlaydate->getLoc_address() ?></span>
                        <span class="city"><?php echo $selectedPlaydate->getLoc_city() ?>, <?php echo $selectedPlaydate->getLoc_state() ?></span>
                      </p>
                    </div>
                    <div class="data">
                      <span class="date"><?php echo(Utils::dateFormat($selectedPlaydate->getDate())) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($selectedPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($selectedPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="data">
                      <div class="participants"><span><?php echo $selectedPlaydate->getNum_children() ?> / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                <form class="form-invite" action="/my-dashboard.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>" method="post">
                  <input type="hidden" name="action" value="invite-friends" />
                  <input type="hidden" name="id_playdate" value="<?php echo $selectedPlaydate->getId_playdate() ?>" />
                  <label for="inputEmail" class="sr-only">Username or Email</label>
                  <input type="text" id="inputEmail" class="form-control" placeholder="Username or Email" name="inviteFriendsPopup[]" required="required" >
                  <?php if($invited) { ?>
                  	<p style="color:#838383;width:100%;text-align:center">Invitation has been sent</p>
                  <?php } ?>
                  <button class="btn btn-md btn-primary btn-block" type="submit">Invite</button>
                </form>
                </div>
              </div>

              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Reserved By</p>
                  <div class="container-specialist-tags row">
                  	<?php $parents = ParentDAO::getParentsListByPlaydate($selectedPlaydate->getId_playdate(), 1, 10, null, null) ?>
                  	<?php foreach ($parents as $auxParent) {
                  	     $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPlaydate->getId_playdate());
                  	     
                  	     $childrenReservation = PlaydateDAO::getChildrenReservationsByReservation($reservation->getId_reservation());
                  	     $numSpots = 0;
                  	     foreach($childrenReservation as $auxChildren) {
                  	         $numSpots += $auxChildren->getNum_children();
                  	     }
                  	?>
                    <div class="specialist-tag">
                      <img src="<?php echo (($auxParent->getPicture()==null)?"/img/img-profile.jpg":$auxParent->getPicture()) ?>">
                      <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getName() ?><span><?php echo $numSpots ?> spot<?php if($numSpots!=1) { echo ("s");} ?></span></a>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
				
			  <?php /* 
              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Invited Friends</p>
                  <div class="row">
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">jonh@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">tom@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">chris@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">gabrielle@gmail.com</a>
                    </div>
                  </div>
                </div>
              </div>
              */ ?>

               <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Specialists</p>
                  
 					<div class="container-specialist-tags row">
                  	  <?php 
                  	  $selectedPlaydateLead = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                  	  ?>
                  
                      <a href="/specialist-profile.php?id_specialist=<?php echo $selectedPlaydateLead->getId_specialist() ?>&slug=<?php echo $selectedPlaydateLead->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $selectedPlaydateLead->getPicture()?>">
                        <span><?php echo $selectedPlaydateLead->getFullName() ?> | Lead</span>
                      </div></a>
                      
                      <?php 
                      $supporters = SpecialistDAO::getSupportersByPlaydate($selectedPlaydate->getId_playdate());
                      foreach($supporters as $auxSupporter) {
                      ?>
                      <a href="/specialist-profile.php?id_specialist=<?php echo $auxSupporter->getId_specialist() ?>&slug=<?php echo $auxSupporter->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $auxSupporter->getPicture()?>">
                        <span><?php echo $auxSupporter->getFullName() ?> | Supporter</span>
                      </div></a>
                      <?php  
                      }
                      ?>
                      
                  </div>


                </div>
              </div>

			  <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Comment(s)</p>
                </div>
                <?php $updates = CommentDAO::getCommentListByPlaydate($selectedPlaydate->getId_playdate()); ?>
                <?php foreach($updates as $auxUpdate) {  
                    $username = "Unknown";
                    if(($auxUpdate->getId_parent()!=null) && is_numeric($auxUpdate->getId_parent())) {
                        $auxParent = ParentDAO::getParent($auxUpdate->getId_parent());
                        $username = $auxParent->getName();
                        $pic = $auxParent->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    } else if(($auxUpdate->getId_specialist()!=null) && is_numeric($auxUpdate->getId_specialist())) {
                    
                        $auxSpecialist = SpecialistDAO::getSpecialist($auxUpdate->getId_specialist());
                        $username = $auxSpecialist->getName();
                        $pic = $auxSpecialist->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    }
                        
                ?>
                <div class="row info-updates">
                    <div class="col-lg-2 date">
                      <img src="<?php echo $pic ?>">
                      <span><strong><?php echo $username ?></strong></span>
                    </div>
                    <div class="col-lg-10 texto">
                      <p class="day"><strong><?php echo Utils::dateFormat($auxUpdate->getIdate())?></strong></p>
                      <p style="color:#878787"><?php echo $auxUpdate->getComment() ?></p>
                    </div>
                </div>
                
                <?php  
                    }
                ?>
              </div>
			  
              
              <div class="row row-tabs">
                <div class="col-lg-12">
                <form class="form-comment" id="form-profile" method="post" enctype="multipart/form-data" action="my-dashboard.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>">
                    <input type="hidden" name="action" value="createupdateselected" />
                    <input type="hidden" name="id_playdate" value="<?php echo $selectedPlaydate->getId_playdate() ?>" />    
                    <p class="titulo uppercase">New comment</p>
                    <div class="row">
                      <div class="col-lg-12">                    
                         <label for="comment" class="sr-only">New comment</label>
                         <textarea class="form-control" placeholder="Comment here to notify your Lead Specialist of any updates or last minute needs. Your comment will be visible to every participant in this playdate." id="comment" rows="3" name="comment"></textarea>
                      </div>
                    </div>
                    <div class="row row-tabs bt-send">
                  		<div class="col-lg-12">
                     		<button class="btn btn-md btn-primary bt-save-profile" type="submit">Upload Comment</button>
                  		</div>
                	</div>
                </form>
                                  
                </div>
              </div>
              <?php } ?>
            </div>

            <div class="tab-pane fade" id="nav-request" role="tabpanel" aria-labelledby="nav-request-tab">
              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo uppercase">Open</p>
                </div>
                
                <?php $requests = PlaydateRequestDAO::getOpenPlaydateRequestsListByParent($parent->getId_parent()); ?>
                
                <?php foreach ($requests as $auxRequest) {
                    //echo "<pre>";print_r($auxRequest);exit;
                    // $interestName = "";
                    // $interestType = "";
                    
                    // $interests = InterestDAO::getInterestsByRequest($auxRequest->getId_request());
                    // $auxInterest = null;
                    // if(count($interests)>0) {
                    //     $auxInterest = $interests[0];
                    //     $interestName =$auxInterest->getName();
                        
                    //     //echo("::[".$interestName."]::::");
                        
                    //     switch ($auxInterest->getType()) {
                    //         case Interest::$TYPE_TRAINING: {
                    //             $interestType=" stem";
                    //             break;
                    //         }
                    //         case Interest::$TYPE_ART: {
                    //             $interestType=" creative";
                    //             break;
                    //         }
                    //         case Interest::$TYPE_PLAY: {
                    //             $interestType=" outdoor";
                    //             break;
                    //         }
                    //         case Interest::$TYPE_CULTURAL: {
                    //             $interestType=" attractions";
                    //             break;
                    //         }
                    //     }
                    // }
                    
                ?>
                <div class="col-lg-4">
                  <div class="box-playdates <?php //echo $interestType ?>">
                    <div class="type">  
                      <?php 
                  if($auxRequest->getPlaydate_type() == 1) {
                    echo "One off Playdate"; } 
                    else { 
                      echo "Recurring Playdate";
                   }
                  ?></div>
                    <div class="time" style="margin-top:0px">
                      <span class="date">
                      <?php //if($auxRequest->getPlaydateDate()){
                     // $new_date_format = date('M d, Y', strtotime($auxRequest->getPlaydateDate()));
                      //  echo $new_date_format; }
                        echo Utils::dateFormat($auxRequest->getDate())?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($auxRequest->getTime_init())?> - <?php echo Utils::get12hourFormat($auxRequest->getTime_end())?></span>
                    </div>
                    <div class="info">
                      <span class="title">Request Pending</span>
                      <span class="subtitle"><?php //echo $auxRequest->getLocation() ?></span>
                    </div>
                    <div class="reserve">
                      <a href="/parent-edit-request.php?id_request=<?php echo $auxRequest->getId_request() ?>" class="bt-reserve">Change</a>
                    </div>
                  </div>
                </div>
                <?php } ?>
                
                <div class="col-lg-12">
                  <p class="titulo uppercase">Received</p>
                </div>
                <?php 
                
                $responses = PlaydateDAO::getPlaydatesListByParentRequest($parent->getId_parent());
                foreach($responses as $auxResponse) {
                    //TODO - De momento elegimos el primero para mostrar el detalle posterior
                    $link = "/my-dashboard.php?id_playdate=".$auxResponse->getId_playdate()."&slug=".$auxResponse->getSlug();
                    
                    $interestName = "";
                    $interestType = "";
                    
                    $interests = InterestDAO::getInterestsByPlaydate($auxResponse->getId_playdate());
                    $auxInterest = null;
                    if(count($interests)>0) {
                        $auxInterest = $interests[0];
                        $interestName =$auxInterest->getName();
                        
                        switch ($auxInterest->getType()) {
                            case Interest::$TYPE_TRAINING: {
                                $interestType=" stem";
                                break;
                            }
                            case Interest::$TYPE_ART: {
                                $interestType=" creative";
                                break;
                            }
                            case Interest::$TYPE_PLAY: {
                                $interestType=" outdoor";
                                break;
                            }
                            case Interest::$TYPE_CULTURAL: {
                                $interestType=" attractions";
                                break;
                            }
                        }
                    }
                    
                    $specialist = SpecialistDAO::getSpecialist($auxResponse->getId_specialist());
                    
                    $leadName = "";
                    if($specialist!=null) {
                        $leadName = $specialist->getFullName();
                    } else {
                        $leadName = "<span style=\"color:red\">No lead</span>";
                    }
                ?>
                
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?>">
                    <div class="time">
					   <span class="date"><?php echo Utils::dateFormat($auxResponse->getDate())?></span>
                       <span class="hour"><?php echo Utils::get12hourFormat($auxResponse->getTime_init())?>-<?php echo Utils::get12hourFormat($auxResponse->getTime_end())?></span>
                    </div>
                    <div class="info">
                      <span class="title"><?php echo $auxResponse->getName() ?></span>
                      <span class="subtitle"><?php echo $auxResponse->getLoc_city()?>, <?php echo $auxResponse->getLoc_state()?></span>
                    </div>
                    <div class="data">
                      <div class="specialists">Itinerary by <span><?php echo $leadName ?></span></div>
                    </div>
                    <?php if($auxResponse->getStatus()==Playdate::$STATUS_DECLINED) { ?>
                    <div class="reserve">
                      <a href="javascript:;" class="bt-reserve declined">Declined</a>
                    </div>
                    <?php } else { ?>
                    <div class="reserve">
                      <a href="/playdate.php?id_playdate=<?php echo $auxResponse->getId_playdate() ?>" class="bt-reserve">Review</a>
                    </div>
                    <?php } ?>
                  </div>
                </div>
				<?php } ?>

              </div>
            </div>

            <div class="tab-pane fade rate-playdate" id="nav-past" role="tabpanel" aria-labelledby="nav-past-tab">
              <div class="row row-tabs">
              <?php 
              	$pastPlaydates = PlaydateDAO::getPastPlaydatesListByParent($parent->getId_parent(), 1, 8, null, null);
              	
              	foreach ($pastPlaydates as $auxPlaydate) {
              	    
              	    //TODO - De momento elegimos el primero para mostrar el detalle posterior
              	    $link = "/my-dashboard.php?id_past_playdate=".$auxPlaydate->getId_playdate()."#past";
              	    
              	    $interestName = "";
              	    $interestType = "";
              	    
              	    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
              	    $auxInterest = null;
              	    if(count($interests)>0) {
              	        $auxInterest = $interests[0];
              	        $interestName =$auxInterest->getName();
              	        
              	        switch ($auxInterest->getType()) {
              	            case Interest::$TYPE_TRAINING: {
              	                $interestType=" stem";
              	                break;
              	            }
              	            case Interest::$TYPE_ART: {
              	                $interestType=" creative";
              	                break;
              	            }
              	            case Interest::$TYPE_PLAY: {
              	                $interestType=" outdoor";
              	                break;
              	            }
              	            case Interest::$TYPE_CULTURAL: {
              	                $interestType=" attractions";
              	                break;
              	            }
              	        }
              	    }
              	    
              	    $openSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
              	    $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
              	    
              	    $selectedBox = (($selectedPastPlaydate!=null) && ($selectedPastPlaydate->getId_playdate()==$auxPlaydate->getId_playdate()));
              	    
              	    
              	    $leadName = "";
              	    if($specialist!=null) {
              	        $leadName = $specialist->getFullName();
              	    } else {
              	        $leadName = "<span style=\"color:red\">No lead</span>";
              	    }
              ?>
                
              
              
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?><?php echo($selectedBox?" selected-box":"")?>">
                  <?php /*?>
                    <div class="img-container">
                      <a href="<?php echo $link ?>"><img src="<?php echo $auxPlaydate->getPicture() ?>" alt=""></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                    */?>
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate())?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init())?>-<?php echo Utils::get12hourFormat($auxPlaydate->getTime_end())?></span>
                    </div>
                    <div class="info">
                      <span class="title"><a href="<?php echo $link ?>"><?php echo $auxPlaydate->getName()?></a></span>
                      <span class="subtitle"><?php echo $auxPlaydate->getLoc_city()?>, <?php echo $auxPlaydate->getLoc_state()?></span>
                    </div>
                    <div class="data">
                    <?php if($specialist!=null) { ?>
                      <div class="specialists">Specialist: <span><a href="/specialist-public-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                    <?php } else { ?>
                      <div class="specialists">Specialist: <span>No Lead</span></div>
                    <?php } ?>
                      <!-- <div class="specialists"><a class="bt-rate"  data-toggle="modal" data-target="#last-playdate" href="javascript:;" title="">Rate Specialist</a></div>  -->
                    </div>
                   
                    <div class="reserve">
                      <a href="<?php echo $link ?>"  <?php // data-toggle="modal" data-target="#last-playdate" */?> class="feedback bt-reserve">More Info</a>
                    </div>
                  </div>
                </div>
				<?php } ?>
                <?php 
                if(($pastPlaydates==null) || (count($pastPlaydates)<=0)) {
                ?>
                  <div class="col-lg-12">
                    <div class="no-results"><img src="img/cactus.png"/>
                      <p>You have no past playdates reserved. </p>
                      <p>Book a playdate today because life is always better with friends!</p>
                    </div>
                  </div>
                <?php   
                }
                ?>
              </div>
              
              <?php if($selectedPastPlaydate!=null) {
              
                  $interestName = "";
                  $interestType = "";
                  
                  $interests = InterestDAO::getInterestsByPlaydate($selectedPastPlaydate->getId_playdate());
                  $auxInterest = null;
                  if(count($interests)>0) {
                      $auxInterest = $interests[0];
                      $interestName =$auxInterest->getName();
                      
                      switch ($auxInterest->getType()) {
                          case Interest::$TYPE_TRAINING: {
                              $interestType=" stem";
                              break;
                          }
                          case Interest::$TYPE_ART: {
                              $interestType=" creative";
                              break;
                          }
                          case Interest::$TYPE_PLAY: {
                              $interestType=" outdoor";
                              break;
                          }
                          case Interest::$TYPE_CULTURAL: {
                              $interestType=" attractions";
                              break;
                          }
                      }
                  }
                  
                  $openSpots = $selectedPastPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($selectedPastPlaydate->getId_playdate());
                  $specialist = SpecialistDAO::getSpecialist($selectedPastPlaydate->getId_specialist());
                  
              ?>
              
              <div id="selected-past-playdate" class="row row-current-dashboard row-eq-height">
                <div class="col-lg-4 vcenter" >
                  <div class="box-playdates <?php echo $interestType ?> no-text">
                    <div class="img-container">
                      <a href="/playdate.php?id_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>"><img src="<?php echo $selectedPastPlaydate->getPicture() ?>" alt="" /></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                  <div class="info-current-dashboard">
                    <div class="info">
                      <span class="title"><a href="/playdate.php?id_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>"><?php echo $selectedPastPlaydate->getName() ?></a></span>
                      <span class="subtitle"><?php echo $selectedPastPlaydate->getLoc_city() ?></span>
                    </div>
                     <div class="data">
                      <p  class="place">
                        <span class="address"><?php echo $selectedPastPlaydate->getLoc_address() ?></span>
                        <span class="city"><?php echo $selectedPastPlaydate->getLoc_city() ?>, <?php echo $selectedPastPlaydate->getLoc_state() ?></span>
                      </p>
                    </div>
                    <div class="data">
                      <span class="date"><?php echo(Utils::dateFormat($selectedPastPlaydate->getDate())) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="data">
                      <div class="participants"><span><?php echo $selectedPastPlaydate->getNum_children() ?> / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                    </div>
                    <div class="reserve">
                      <a href="#"  data-toggle="modal" data-target="#last-playdate" class="feedback bt-reserve">
                      <?php if($rating==null) { ?>
                      Leave feedback
                      <?php } else { ?>
                      Change feedback
                      <?php }?>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Reserved By</p>
                  <div class="container-specialist-tags row">
                  	<?php $parents = ParentDAO::getParentsListByPlaydate($selectedPastPlaydate->getId_playdate(), 1, 10, null, null) ?>
                  	<?php foreach ($parents as $auxParent) {
                  	    $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPastPlaydate->getId_playdate());
                  	?>
                    <div class="specialist-tag">
                      <img src="<?php echo (($auxParent->getPicture()==null)?"/img/img-profile.jpg":$auxParent->getPicture()) ?>">
                      <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getName() ?><span><?php echo $reservation->getNum_spots()?> spot<?php if($reservation->getNum_spots()!=1) { echo ("s");} ?></span></a>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
				
			 <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Specialists</p>
                  <div class="container-specialist-tags row">
                      <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $specialist->getPicture()?>">
                        <span><?php echo $specialist->getFullName() ?> | Lead</span>
                      </div></a>
                  </div>
                </div>
              </div>

			  <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Comment(s)</p>
                </div>
                <?php $updates = CommentDAO::getCommentListByPlaydate($selectedPastPlaydate->getId_playdate()); ?>
                <?php foreach($updates as $auxUpdate) {  
                    $username = "Unknown";
                    if(($auxUpdate->getId_parent()!=null) && is_numeric($auxUpdate->getId_parent())) {
                        $auxParent = ParentDAO::getParent($auxUpdate->getId_parent());
                        $username = $auxParent->getName();
                        $pic = $auxParent->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    } else if(($auxUpdate->getId_specialist()!=null) && is_numeric($auxUpdate->getId_specialist())) {
                    
                        $auxSpecialist = SpecialistDAO::getSpecialist($auxUpdate->getId_specialist());
                        $username = $auxSpecialist->getUsername();
                        $pic = $auxSpecialist->getName();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    }
                        
                ?>
                <div class="row info-updates">
                    <div class="col-lg-2 date">
                      <img src="<?php echo $pic ?>">
                      <span><strong><?php echo $username ?></strong></span>
                    </div>
                    <div class="col-lg-10 texto">
                      <p class="day"><strong><?php echo Utils::dateFormat($auxUpdate->getIdate())?></strong></p>
                      <p style="color:#878787"><?php echo $auxUpdate->getComment() ?></p>
                    </div>
                </div>
                
                <?php  
                    }
                ?>
              </div>
              
              <div class="row row-tabs">
                <div class="col-lg-12">
                <form class="form-comment" id="form-profile" method="post" enctype="multipart/form-data" action="my-dashboard.php?id_past_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>#past">
                    <input type="hidden" name="action" value="createupdatepast" />
                    <input type="hidden" name="id_playdate" value="<?php echo $selectedPastPlaydate->getId_playdate() ?>" />    
                    <p class="titulo uppercase">New comment</p>
                    <div class="row">
                      <div class="col-lg-12">                    
                         <label for="comment" class="sr-only">New comment</label>
                         <textarea class="form-control" placeholder="Comment here to notify your Lead Specialist of any updates or last minute needs. Your comment will be visible to every participant in this playdate. Upload Comment." id="comment" rows="3" name="comment"></textarea>
                      </div>
                    </div>
                    <div class="row row-tabs bt-send">
                  		<div class="col-lg-12">
                     		<button class="btn btn-md btn-primary bt-save-profile" type="submit">Upload Comment</button>
                  		</div>
                	</div>
                </form>
                                  
                </div>
              </div>              
			  
			  <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Feedback</p>
                </div>
                <?php $ratings = RatingDAO::getRatingListByPlaydate($selectedPastPlaydate->getId_playdate()) ?>
                <?php foreach($ratings as $auxRating) {
                    if(($auxRating->getComment_playdate()!=null) && ($auxRating->getComment_playdate()!="")) {
                        $username = "Unknown";
                        if(($auxRating->getId_parent()!=null) && is_numeric($auxRating->getId_parent())) {
                            $auxParent = ParentDAO::getParent($auxRating->getId_parent());
                            $username = $auxParent->getName();
                            $pic = $auxParent->getPicture();
                            if(($pic==null) || ($pic=="")) {
                                $pic = "img/img-profile.jpg";
                            }
                        } else if(($auxRating->getId_specialist()!=null) && is_numeric($auxRating->getId_specialist())) {
                        
                            $auxSpecialist = SpecialistDAO::getSpecialist($auxRating->getId_specialist());
                            $username = $auxSpecialist->getUsername();
                            $pic = $auxSpecialist->getName();
                            if(($pic==null) || ($pic=="")) {
                                $pic = "img/img-profile.jpg";
                            }
                        }
                            
                    ?>
                    <div class="row info-updates">
                        <div class="col-lg-2 date">
                          <img src="<?php echo $pic ?>">
                          <span><strong><?php echo $username ?></strong></span>
                        </div>
                        <div class="col-lg-10 texto">
                          <p class="day"><strong><?php echo Utils::dateFormat($auxRating->getIdate())?></strong></p>
                          <p style="color:#878787"><?php echo $auxRating->getComment_playdate() ?></p>
                        </div>
                    </div>
                    
                    <?php  
                        }
                    }
                ?>
              </div>
			  
			  
              <?php } ?>
              
              
            </div>

            <div class="tab-pane fade" id="nav-groups" role="tabpanel" aria-labelledby="nav-groups-tab">
              <div class="row row-tabs">

			  <!-- grp invitations -->
              <?php 
              $grpInvitations = GroupDAO::getWaitingGroupInvitationsByParent($parent->getId_parent());
              foreach($grpInvitations as $auxInvitation) {
                  $numMembers = GroupDAO::countGroupParents($auxInvitation->getId_group());
              
              ?>
              <div class="col-lg-4">
                <div class="box-playdates invitation">
                  <div class="invited-title">
                    <p>Your are invited!</p>
                  </div>
                  <div class="info">
                    <span class="title"><?php echo $auxInvitation->getName() ?></span>
                    <span class="subtitle"><?php echo $auxInvitation->getZipcode() ?></span>
                    <span class="subtitle members"><?php echo $numMembers ?> members</span>
                  </div>
                   <div class="data">
                    <p><?php echo $auxInvitation->getDescription(120); ?></p>
                    <a href="/group-public-profile.php?id_group=<?php echo $auxInvitation->getId_group() ?>" class="bt-learn-more">Learn more</a>
                  </div>
                  <div class="reserve">
                    <a href="/my-dashboard.php?action=invitationaccept&id_invitation=<?php echo $auxInvitation->getId_invitation()?>#groups" class="bt-reserve bt-join">Join Group</a>
                    <a href="/my-dashboard.php?action=invitationdecline&id_invitation=<?php echo $auxInvitation->getId_invitation()?>#groups" class="bt-reserve">Decline</a>
                  </div>
                </div>
              </div>
              <?php } ?>
              <!-- /grp invitations -->
              <!-- parent groups -->
			  <?php 
			  $groups = GroupDAO::getGroupsByParent($parent->getId_parent());
			  
			  foreach($groups as $auxGroup) {
			      
			      $numMembers = GroupDAO::countGroupParents($auxGroup->getId_group()) + GroupDAO::countGroupSpecialists($auxGroup->getId_group());
			      
			      $interestName = "";
			      $interestType = "";
			      
			      $interests = InterestDAO::getInterestsByGroup($auxGroup->getId_group());
			      $auxInterest = null;
			      if(count($interests)>0) {
			          $auxInterest = $interests[0];
			          $interestName =$auxInterest->getName();
			          
			          switch ($auxInterest->getType()) {
			              case Interest::$TYPE_TRAINING: {
			                  $interestType=" stem";
			                  break;
			              }
			              case Interest::$TYPE_ART: {
			                  $interestType=" creative";
			                  break;
			              }
			              case Interest::$TYPE_PLAY: {
			                  $interestType=" outdoor";
			                  break;
			              }
			              case Interest::$TYPE_CULTURAL: {
			                  $interestType=" attractions";
			                  break;
			              }
			          }
			      }
			      
			      $grpPicture = $auxGroup->getPicture();
			      if(($grpPicture==null) || ($grpPicture=="")) {
			          $grpPicture = "/img/default-img-group-profile.jpg";
			      }
			  ?>
              <div class="col-lg-4">
                <div class="box-playdates <?php echo $interestType ?>">
                  <div class="img-container">
                    <img src="<?php echo $grpPicture ?>"/ alt="">
                    <span class="cat-box"><?php echo $interestName ?></span>
                  </div>
                  <div class="info">
                    <span class="title"><?php echo $auxGroup->getName() ?></span>
                    <span class="subtitle"><?php echo $auxGroup->getZipcode() ?></span>
                    <span class="subtitle members"><?php echo $numMembers ?> members</span>
                  </div>
                   <div class="data">
                    <p><?php echo $auxGroup->getDescription(120); ?></p>
                  </div>
                  <div class="reserve">
                    <a href="/groups-dashboard.php?id_group=<?php echo $auxGroup->getId_group() ?>" class="bt-reserve">Dashboard</a>
                  </div>
                </div>
              </div>
              <?php } ?>
			  <!-- /parent groups -->
              </div> 

              <div class="row info-box groups">
                <div class="col-lg-6">                        
                  <p>Add a New Group</p>
                  <form class="form-add-group" method="post" action="/my-dashboard.php#groups">
                  	<input type="hidden" name="action" value="addgroup" />
                    <label for="inputSelectGroup" class="sr-only">Select group</label>
                    <input type="text" id="inputSelectGroup" class="form-control" placeholder="Select group" required="" name="name">
                    <button class="btn btn-md btn-primary bt-titulo" type="submit">Request Invite</button>  
                  </form>
                </div>
                <div class="col-lg-6">
                  <p>Create a New Group</p>
                  <form class="form-create-group" action="/my-dashboard.php#groups" method="post">
                    <input type="hidden" name="action" value="creategroup" />
                    <label for="inputGroupName" class="sr-only">Group Name</label>
                    <input type="text" id="inputGroupName" class="form-control" placeholder="Group Name" required="required" name="name" />
                    <?php /*
                    <p class="text-invite">Invite friends</p>
                    <div class="mail-container">
                      <label for="inputInviteUser" class="sr-only">Username or Email</label>
                      <div class="cont-invite">
                        <input type="text" id="inputInviteUser" class="form-control" placeholder="Username or Email" required="" ><a href="javascript:;" class="bt-invite" title=""><img src="img/bt-add.png" alt=""/></a>
                      </div>
                    </div>
                    */?>
                    <button class="btn btn-md btn-primary bt-titulo" type="submit">Create Group</button>  
                  </form>
                </div>         
              </div>

            </div>


            <div class="tab-pane fade" id="nav-specialists" role="tabpanel" aria-labelledby="nav-specialists-tab">
              <div class="row featured-specialists">
              
		<?php 
		$specialists = SpecialistDAO::getSpecialistsListByParent($parent->getId_parent(), 1, 4, null, null);
		
		foreach ($specialists as $specialistaux) {
		    $ratingvalue = RatingDAO::getSpecialistValue($specialistaux->getId_specialist());
		    $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialistaux->getId_specialist());
		    $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialistaux->getId_specialist());
		?>
        <div class="col-lg-4 col-md-6 col-sm-6 box-specialist">
           <div class="flip">
                <div class="card">
                  <div class="specialist face front">
                    <div class="info">
                       <p><?php echo $specialistaux->getFullName() ?></p>
                       <div class="puntuacion">
                        <ul>
                          <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                        </ul>
                        <span class="num"><?php echo $ratingvalue ?>/5</span>
                      </div>                
                      
                    </div>             
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                  <div class="face back">
                    <div class="info-back">
                      <div class="ico-specialist family"><span><?php echo $numFamilies ?><br>repeat families</span></div>
                      <div class="ico-specialist pd"><span><?php echo $numPlaydates ?><br>playdates</span></div>
                      <p><?php echo $specialistaux->getAbout_me() ?></p>
                      <a class="bt-profile" title="" href="/specialist-profile.php?id_specialist=<?php echo $specialistaux->getId_specialist()?>&slug=<?php echo $specialistaux->getSlug() ?>">View profile</a>
                    </div>            
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                </div>
              </div>
        </div>
		<?php } ?>
              
              
                  
              </div>
			  
              <div class="row info-box" id="specialistsnominate">

                  <div class="col-lg-12">
                    <div class="titulo noImages">
                      <h2>Nominate a specialist</h2>
                      <?php if($nominated) { ?>
                      <p>Your nomination has been sent!</p>
                      <?php } ?>
                      <p>Do you know a child care professional that should join playdate? </p>
                      <form class="form-nominate" action="/my-dashboard.php#specialists" method="post">
                        <input type="hidden" name="action" value="nominate" />
                        <label for="inputEmail" class="sr-only">Email</label>
                        <input type="text" id="inputEmail" class="form-control" placeholder="Email" required="" name="nominate_email">
                        <button class="btn btn-md btn-primary bt-titulo" type="submit">Nominate</button>  
                      </form>
                    </div>

                  </div>

              </div>

            </div>


            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

              <form class="form-profile" id="form-profile" method="post" enctype="multipart/form-data" action="/my-dashboard.php#profile">
                <input type="hidden" name="action" value="updateprofile" />
                <div class="row row-tabs">
                  <div class="col-lg-12">
                     <button class="btn btn-md btn-primary bt-save-profile" type="submit">Save changes</button>
                  </div>
                </div>
                <div class="row row-tabs">
                  <div class="col-lg-4 cont-img-profile">
                  	<?php if(($parent->getPicture()==null) || ($parent->getPicture() == "")) { ?>
                    <img src="img/img-profile.jpg" alt="" class="img-profile">
                    <?php } else { ?>
                    <img src="<?php echo $parent->getPicture() ?>" alt="" class="img-profile">
                    <?php } ?>
                    <label class="custom-file">
                      <input type="hidden" name="picture" value="<?php echo $parent->getPicture() ?>" />
                      <input type="file" id="file" class="custom-file-input" name="formfile"   onchange="previewFile()" style="cursor: pointer;"/>
                      <span class="custom-file-control"></span>
                    </label>
                  </div>
                  <div class="col-lg-8 your-personal-info">
                    <p class="titulo">Your Personal Info</p>
                    <p class="profile-name info">Name: <span><?php echo $parent->getFullName() ?></span></p>
                    <p class="profile-username info">Username: <span><?php echo $parent->getUsername() ?></span></p>
                    <p class="profile-email info">Email: <span><?php echo $parent->getEmail() ?></span></p>
                    <?php /*
                    <p class="profile-password info"><a href="#" title="">Reset password</a></p>
                    */?>
                    <div class="row">
                      <div class="col-lg-6">
                        <label for="inputNewPassword" class="small">New Password</label>
                        <input type="password" id="inputNewPassword" class="form-control noMarginTop" placeholder="" name="new-password" value="">
                      </div>
                      <div class="col-lg-6">
                      	<?php if($passwordchanged) { ?>
                      	<label for="inputNewPassword" class="small"></label>
                      	<p style="color:#815087;font-weight:bold">Password changed</p>
                      	<?php } ?>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-lg-6">
                        <label for="inputCellPhone" class="small">Cell Phone *</label>
                        <input type="text" id="inputCellPhone" class="form-control noMarginTop" placeholder="XXX XXX XXX" name="cellphone" value="<?php echo $parent->getCellphone() ?>">
                      </div>
                      <div class="col-lg-6">
                        <label for="inputPhoneWork" class="small">Work Phone</label>
                        <input type="text" id="inputPhoneWork" class="form-control noMarginTop" placeholder="XXX XXX XXX" name="phone" value="<?php echo $parent->getPhone() ?>">
                      </div>
                    </div>

                    
                  </div>
                </div>
                <div class="row row-tabs">
                  <div class="col-lg-12">                    
                    <p class="titulo uppercase">Your Neighborhoods</p>
                    <div class="row row-zip-code">
                      <div class="col-lg-12 ">
                        <label for="inputLocation" class="sr-only">Your Neighborhoods</label>
                        <input type="text" id="location" name="location" class="form-control noMarginTop inputLocation" placeholder="neighborhood">
                        <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                      </div>
                      <div class="container-tags">
                        <?php
                        $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByParent($parent->getId_parent());
                        foreach ($neighborhoods as $auxItem) {
                        ?>
                          <div class="tag" style="margin-right: 5px;">
                            <span><?php echo $auxItem->getName()?></span>
                            <a class="bt-delete-tag" href="javascript:;" title="Remove"><i class="fa fa-window-close" aria-hidden="true"></i></a>
                            <input type="hidden" value="<?php echo $auxItem->getId_neighborhood() ?>" name="id_neighborhood[]">
                          </div>
                        <?php 
                        }
                        ?>
                      </div>
                    </div>                    
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">                    
                     <p class="titulo">Your Children's Availability for Playdates</p>
                     <div class="row">
                        <div class="col-lg-5">
                         <div id="time-container-profile">
                            <div class="day-time">
                              <span>Mon </span>
                              <div class="timeFT">
                                <div class="input-group">
                                  <label class="sr-only" for="inputFrom1">From</label>
                                  <div class="selectTime from">
                                    <?php 
                                    $fieldName = "children_avb_mon_init";
                                    $fieldValue = $parent->getChildren_avb_mon_init();
                                    include("components/input-time-options.php");
                                    ?>
                                  </div>
                                </div>
                                <span> - </span>
                                <div class="input-group">
                                  <label class="sr-only" for="inputTo1">To</label>
                                  <div class="selectTime to">
                                    <?php 
                                    $fieldName = "children_avb_mon_end";
                                    $fieldValue = $parent->getChildren_avb_mon_end();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                              </div>
                            </div>
                            
                            <div class="day-time">
                              <span>Tue </span>
                              <div class="timeFT">
                                <div class="input-group">
                                  <label class="sr-only" for="inputFrom1">From</label>
                                  <div class="selectTime from">
                                    <?php 
                                    $fieldName = "children_avb_tue_init";
                                    $fieldValue = $parent->getChildren_avb_tue_init();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                                <span> - </span>
                                <div class="input-group">
                                  <label class="sr-only" for="inputTo1">To</label>
                                  <div class="selectTime to">
                                    <?php 
                                    $fieldName = "children_avb_tue_end";
                                    $fieldValue = $parent->getChildren_avb_tue_end();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                              </div>
                            </div>
                            <div class="day-time">
                              <span>Wed </span>
                              <div class="timeFT">
                                <div class="input-group">
                                  <label class="sr-only" for="inputFrom1">From</label>
                                  <div class="selectTime from">
                                    <?php 
                                    $fieldName = "children_avb_wed_init";
                                    $fieldValue = $parent->getChildren_avb_wed_init();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                                <span> - </span>
                                <div class="input-group">
                                  <label class="sr-only" for="inputTo1">To</label>
                                  <div class="selectTime to">
                                    <?php 
                                    $fieldName = "children_avb_wed_end";
                                    $fieldValue = $parent->getChildren_avb_wed_end();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                              </div>
                            </div>
                            <div class="day-time">
                              <span>Thu </span>
                              <div class="timeFT">
                                <div class="input-group">
                                  <label class="sr-only" for="inputFrom1">From</label>
                                  <div class="selectTime from">
                                    <?php 
                                    $fieldName = "children_avb_thu_init";
                                    $fieldValue = $parent->getChildren_avb_thu_init();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                                <span> - </span>
                                <div class="input-group">
                                  <label class="sr-only" for="inputTo1">To</label>
                                  <div class="selectTime to">
                                    <?php 
                                    $fieldName = "children_avb_thu_end";
                                    $fieldValue = $parent->getChildren_avb_thu_end();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                              </div>
                            </div>
                            <div class="day-time">
                              <span>Fri </span>
                              <div class="timeFT">
                                <div class="input-group">
                                  <label class="sr-only" for="inputFrom1">From</label>
                                  <div class="selectTime from">
                                    <?php 
                                    $fieldName = "children_avb_fri_init";
                                    $fieldValue = $parent->getChildren_avb_fri_init();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                                <span> - </span>
                                <div class="input-group">
                                  <label class="sr-only" for="inputTo1">To</label>
                                  <div class="selectTime to">
                                    <?php 
                                    $fieldName = "children_avb_fri_end";
                                    $fieldValue = $parent->getChildren_avb_fri_end();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                              </div>
                            </div>
                            <div class="day-time">
                              <span>Sat </span>
                              <div class="timeFT">
                                <div class="input-group">
                                  <label class="sr-only" for="inputFrom1">From</label>
                                  <div class="selectTime from">
                                    <?php 
                                    $fieldName = "children_avb_sat_init";
                                    $fieldValue = $parent->getChildren_avb_sat_init();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                                <span> - </span>
                                <div class="input-group">
                                  <label class="sr-only" for="inputTo1">To</label>
                                  <div class="selectTime to">
                                    <?php 
                                    $fieldName = "children_avb_sat_end";
                                    $fieldValue = $parent->getChildren_avb_sat_end();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                              </div>
                            </div>
                            <div class="day-time">
                              <span>Sun </span>
                              <div class="timeFT">
                                <div class="input-group">
                                  <label class="sr-only" for="inputFrom1">From</label>
                                  <div class="selectTime from">
                                    <?php 
                                    $fieldName = "children_avb_sun_init";
                                    $fieldValue = $parent->getChildren_avb_sun_init();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                                <span> - </span>
                                <div class="input-group">
                                  <label class="sr-only" for="inputTo1">To</label>
                                  <div class="selectTime to">
                                    <?php 
                                    $fieldName = "children_avb_sun_end";
                                    $fieldValue = $parent->getChildren_avb_sun_end();
                                    include("components/input-time-options.php");
                                    ?>
                                   </div>
                                </div>
                              </div>
                            </div>
                            
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row row-tabs row-child">
                  <div class="col-lg-12">
                    <p class="titulo uppercase">Your Children *</p>
                    
                    <?php 
                    $children = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                    
                    $i=1;
                    foreach($children as $auxChildren) {
                    ?>
                    <div style="border-bottom: 1px solid #dddddd; padding-bottom: 50px;">
                    <input type="hidden" name="ch<?php echo $i ?>_id_children" value="<?php echo $auxChildren->getId_children() ?>">
                    <input type="hidden" name="ch<?php echo $i ?>_id_parent" value="<?php echo $parent->getId_parent() ?>"/>
                    <!--  child <?php echo $auxChildren->getId_children() ?> -->
                    <div class="row row-data-child">
                      <div class="col-lg-2">
                       <div class="selectGenre">
                        <a href="javascript:;" class="boy child<?php echo(($auxChildren->getGenre()=="boy")?" selected":"") ?>" data-genre="boy"><img  src="img/ico/main/0299-baby2.svg" width="50" alt=""/></a>
                        <a href="javascript:;" class="girl child<?php echo(($auxChildren->getGenre()=="girl")?" selected":"") ?>" data-genre="girl"><img  src="img/ico/main/0298-baby.svg" width="50" alt=""/></a>
                        <input type="hidden" id="inputSelectGenre" name="ch<?php echo $i ?>_genre" value="<?php echo $auxChildren->getGenre() ?>"/>
                       </div>
                      </div>
                      <div class="col-lg-4">
                        <label for="inputChildName" class="small">Name</label>
                        <input type="text" id="inputChildName" class="form-control noMarginTop" placeholder="Name" name="ch<?php echo $i ?>_name" value="<?php echo $auxChildren->getName() ?>" required>
                      </div>

                      <div class="col-lg-3">
                        <label for="inputChildAge" class="small">Age</label>
                        <input type="text" id="inputChildAge" class="form-control noMarginTop" placeholder="Age" name="ch<?php echo $i ?>_age" value="<?php echo $auxChildren->getAge() ?>" required>
                      </div>
                      <div class="col-lg-3">
                        <label for="inputChildBirthday" class="small">Birthday</label>
                        <input type="text" id="inputChildBirthday" class="form-control noMarginTop" placeholder="MM/DD/YYYY" name="ch<?php echo $i ?>_birthdate" value="<?php echo $auxChildren->getBirthdate() ?>" required>
                      </div>
                      <div class="col-lg-12">                    
                         <label for="importantToKnow" class="small">Important to know</label>
                         <textarea class="form-control" placeholder="Important to know" id="importantToKnow" rows="7"  name="ch<?php echo $i ?>_notes"><?php echo $auxChildren->getNotes() ?></textarea>
                      </div>
                      <div class="col-lg-12">
						<div class="form-group" style="margin-top:0px!important">
                             <input type="checkbox" id="ch<?php echo $i ?>_remove"  value="1" class="form-control" name="ch<?php echo $i ?>_remove" />
                             <label for="ch<?php echo $i ?>_remove" style="width:auto; margin-top:15px;">Remove this kid</label>
                        </div>
                      </div>
                    </div>
                      <?php $interests = InterestDAO::getInterestsIdByChildren($auxChildren->getId_children()); ?>
                      <!-- Interests -->
                      <div class="col-lg-12 col-list-container">
                        <label for="importantToKnow" class="small">Interests</label>
                        <div class="row">
                          <div class="col-lg-3">
                            <div class="list-container stem">
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_1"  value="1" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(1, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_1">Academic</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_2"  value="2" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(2, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_2">STEM</label>
                              </div> 
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="list-container creative">
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_3"  value="3" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(3, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_3">Creative</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_4"  value="4" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(4, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_4">Performing Arts</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_5"  value="5" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(5, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_5">Language &amp; Culture</label>
                              </div> 
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="list-container outdoor">
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_6" value="6" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(6, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_6">Play</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_7" value="7" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(7, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_7">Sports &amp; Recreation</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_8" value="8" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(8, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_8">Health &amp; Wellness</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_9" value="9" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(9, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_9">Outdoor</label>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="list-container attractions">
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_10" value="10" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(10, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_10">NYC Museums</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_11" value="11" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(11, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_11">NYC Attractions</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="ch<?php echo $i ?>_int_12" value="12" class="form-control" placeholder="keyword" name="ch<?php echo $i ?>_interests[]" <?php echo(in_array(12, $interests)?"checked=\"checked\"":""); ?>>
                                <label for="ch<?php echo $i ?>_int_12">NYC Theater</label>
                              </div> 
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /Interests -->
                      
                    
                    <!--  /child <?php echo $auxChildren->getId_children() ?> -->
                    </div>
                    <?php 
                    $i++;
                    } ?>
                             
                                     
                  </div>
                </div>
                
                      <?php /*
                      <!-- Interests -->
                      <div class="col-lg-12 col-list-container">
                        <label for="importantToKnow" class="small">Interests</label>
                        <div class="row">
                          <div class="col-lg-3">
                            <div class="list-container stem">
                              <div class="form-group">
                                <input type="checkbox" id="0" class="form-control" placeholder="keyword">
                                <label for="0">Academic</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="1" class="form-control" placeholder="keyword">
                                <label for="1">STEM</label>
                              </div> 
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="list-container creative">
                              <div class="form-group">
                                <input type="checkbox" id="2" class="form-control" placeholder="keyword">
                                <label for="2">Creative</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="0" class="form-control" placeholder="keyword">
                                <label for="3">Performing Arts</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="4" class="form-control" placeholder="keyword">
                                <label for="4">Language &amp; Culture</label>
                              </div> 
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="list-container outdoor">
                              <div class="form-group">
                                <input type="checkbox" id="5" class="form-control" placeholder="keyword">
                                <label for="5">Play</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="6" class="form-control" placeholder="keyword">
                                <label for="6">Sports &amp; Recreation</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="7" class="form-control" placeholder="keyword">
                                <label for="7">Health &amp; Wellness</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="8" class="form-control" placeholder="keyword">
                                <label for="8">Outdoor</label>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="list-container attractions">
                              <div class="form-group">
                                <input type="checkbox" id="9" class="form-control" placeholder="keyword">
                                <label for="9">NYC Museums</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="10" class="form-control" placeholder="keyword">
                                <label for="10">NYC Attractions</label>
                              </div> 
                              <div class="form-group">
                                <input type="checkbox" id="11" class="form-control" placeholder="keyword">
                                <label for="11">NYC Theater</label>
                              </div> 
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /Interests -->
                      */?>
                    <!--  /newchild -->   
                    <div class="row  row-tabs">
                  <div class="col-lg-12">
                     <button class="btn btn-md btn-primary bt-add-item" type="button" id="addchildren">+ Add Children</button>
                  </div>
                </div> 
                 <div class="row row-tabs">
                  <div class="col-lg-12">
                    <p class="titulo uppercase">Emergency Contact & Authorized for Pickup *</p>
                  </div>
                 </div>
                <div class="container-emergency">   
                    <?php 
                    $emergencys = EmergencyDAO::getEmergencyListByParent($parent->getId_parent());
                        
                    $i=1;
                    foreach($emergencys as $auxEmergency) {
                    ?>
                    <input type="hidden" name="em<?php echo $i ?>_id_emergency" value="<?php echo $auxEmergency->getId_emergency() ?>">
                    <input type="hidden" name="em<?php echo $i ?>_id_parent" value="<?php echo $parent->getId_parent() ?>"/>
                 
                    <div class="row row-tabs row-emergency">
                      <div class="col-lg-12">
                        <div class="row">
                          <div class="col-lg-4">
                            <label for="inputEmergencyName" class="small">Name</label>
                            <input type="text" id="inputEmergencyName" class="form-control noMarginTop" placeholder="Name" name="em<?php echo $i ?>_name" value="<?php echo $auxEmergency->getName() ?>" required>
                          </div>

                          <div class="col-lg-4">
                            <label for="inputEmergencyMail" class="small">Email</label>
                            <input type="text" id="inputEmergencyMail" class="form-control noMarginTop" placeholder="Email"  name="em<?php echo $i ?>_mail" value="<?php echo $auxEmergency->getMail() ?>" required>
                          </div>
                          <div class="col-lg-4">
                            <label for="inputEmergencyPhone" class="small">Phone Number</label>
                            <input type="text" id="inputEmergencyPhone" class="form-control noMarginTop" placeholder="Phone number"  name="em<?php echo $i ?>_phone" value="<?php echo $auxEmergency->getPhone() ?>" required>
                          </div>
                          <div class="col-lg-4">
                            <label for="inputEmergencyRelation" class="small">Relation to Children</label>
                            <input type="text" id="inputEmergencyRelation" class="form-control noMarginTop" placeholder="Relation to Children"  name="em<?php echo $i ?>_relation" value="<?php echo $auxEmergency->getRelation() ?>" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php
                      $i++;
                    } 
                    ?>
                </div>
                <div class="row  row-tabs">
                  <div class="col-lg-12">
                     <button class="btn btn-md btn-primary bt-add-item" type="button" id="addemergency">+ Add Contact</button>
                  </div>
                </div> 
                <div class="row  row-tabs">
                  <div class="col-lg-12">
                    <p class="legal">*Mandatory Fields</p>
                     <button class="btn btn-md btn-primary bt-save-profile last"  type="submit">Save changes</button>
                  </div>
                </div> 
              </form>
              

            </div>
            
            <div class="tab-pane fade" id="nav-playpacks" role="tabpanel" aria-labelledby="nav-playpacks-tab">
              <div class="row row-tabs">

                <div class="col-lg-5">
                  <div class="playpack-price">
                    <div class="price">$<?php echo Utils::moneyFormat($parent->getCredit()) ?></div>
                    <div class="texto">available</div>
                  </div>
                </div>

                <div class="col-lg-7">
                  <a class="bt-top" href="/get-playpacks.php" title="">Get Playpack</a>
                </div>
              </div>
              </form>
              <form action="/my-dashboard.php#playpacks" method="post" id="purchase_form"> 
              <div class="row row-tabs">
              	  <div class="col-lg-12">
                    <p class="titulo uppercase">Purchase Playpack History</p>
                    
                    <div class="row">
                      <div class="col-lg-5" >
                        <div id="date-container-profile">
                        <?php 
                        $initDate = null;
                        $endDate = null;
                        if(isset($_REQUEST["date_init"]) && ($_REQUEST["date_init"]!="")) {
                            $initDate = $_REQUEST["date_init"];
                        }
                        if(isset($_REQUEST["date_end"]) && ($_REQUEST["date_end"]!="")) {
                            $endDate = $_REQUEST["date_end"];
                        }
                        $purchases = PurchaseDAO::getPurchaseListByParent($parent->getId_parent(), $initDate, $endDate);
                        ?>
                        
                          <div class="input-group date">
                            <input type="text" placeholder="MM/DD/YYYY" class="form-control datepicker2"  name="date_init" id="purchase_init" value="<?php echo(($initDate!=null)?$initDate:"")?>">
                          </div>
                          <span> - </span>
                          <div class="input-group date">
                            <input type="text" placeholder="MM/DD/YYYY" class="form-control datepicker2" name="date_end"  id="purchase_end" value="<?php echo(($endDate!=null)?$endDate:"")?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3" >
                          	<input type="submit" class="bt-save-profile last" value="Filter">
                      </div>
                    </div>

                    <div class="row row-tabs">
                      <div class="col-lg-12">
                        <table id="playpack-history" class="table table-striped" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Date</th>
                                  <th>Playpack Amount</th>
                                  <th>Note</th>
                              </tr>
                          </thead>
                          <tbody>
                          	<?php 
                          	foreach($purchases as $auxPurchase) {
                          	?>
                              <tr>
                                  <td><?php echo Utils::dateFormat($auxPurchase->getIdate())?></td>
                                  <td>$<?php echo Utils::moneyFormat($auxPurchase->getAmount())?></td>
                                  <td><?php echo $auxPurchase->getDescription()?></td>
                              </tr>
							<?php } ?>                              
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              </div> 
              
              <div class="row row-tabs">
                  <div class="col-lg-12">
                    <p class="titulo uppercase">Used credit history</p>
                    
                    <div class="row">
                      <div class="col-lg-5" >
                        <div id="date-container-profile">
                        
                        <?php 
                        $initDateHist = null;
                        $endDateHist = null;
                        if(isset($_REQUEST["history_init"]) && ($_REQUEST["history_init"]!="")) {
                            $initDateHist = $_REQUEST["history_init"];
                        }
                        if(isset($_REQUEST["history_end"]) && ($_REQUEST["history_end"]!="")) {
                            $endDateHist = $_REQUEST["history_end"];
                        }
                        $payments = PaymentDAO::getPaymentListByParent($parent->getId_parent(), $initDateHist, $endDateHist);
                        ?>
                          <div class="input-group date">
                            <input type="text" placeholder="DD/MM/YYYY" class="form-control datepicker2" name="history_init" value="<?php echo(($initDateHist!=null)?$initDateHist:"")?>">
                          </div>
                          <span> - </span>
                          <div class="input-group date">
                            <input type="text" placeholder="DD/MM/YYYY" class="form-control datepicker2" name="history_end" value="<?php echo(($endDateHist!=null)?$endDateHist:"")?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3" >
                          	<input type="submit" class="bt-save-profile last" value="Filter">
                      </div>
                    </div>
                    
                    <div class="row row-tabs">
                      <div class="col-lg-12">
                        <table id="playpack-credit" class="table table-striped" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Date</th>
                                  <th>Playdate</th>
                                  <th>Amount Playdate</th>
                                  <th>Amount Addons</th>
                                  <th>Note</th>
                              </tr>
                          </thead>
                          <tbody>
                          	<?php 
                          	foreach($payments as $auxPayment) {
                          	?>
                              <tr>
                                  <td><?php echo Utils::dateFormat($auxPayment->getIdate())?></td>
                                  <td><?php echo $auxPayment->getName() ?></td>
                                  <td>$<?php echo Utils::moneyFormat($auxPayment->getAmount_playdate())?></td>
                                  <td>$<?php echo Utils::moneyFormat($auxPayment->getAmount_addons())?></td>
                                  <td><?php echo $auxPayment->getComment() ?></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              </div> 
              </form>
            </div>
          </div>
        
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
		<script>
		$(document).ready(function (){
		    document.body.scrollTop = 0;
		    document.documentElement.scrollTop = 0;
		    if($(".row-data-child").length == 0){
		        $(".row-child > .col-lg-12").append('<div class="row row-data-child"><div class="col-lg-2"><div class="selectGenre"><a href="javascript:;" class="boy child selected" data-genre="boy"><img src="img/ico/main/0299-baby2.svg" width="50" alt=""></a><a href="javascript:;" class="girl child" data-genre="girl"><img src="img/ico/main/0298-baby.svg" width="50" alt=""></a><input type="hidden" id="inputSelectGenre" name="newch_1_genre" value="boy"></div></div><div class="col-lg-4"><label for="inputChildName" class="small">Name</label><input type="text" id="inputChildName" class="form-control noMarginTop" placeholder="Name" name="newch_1_name" value="" required></div><div class="col-lg-3"><label for="inputChildAge" class="small">Age</label><input type="text" id="inputChildAge" class="form-control noMarginTop" placeholder="Age" name="newch_1_age" value="" required></div><div class="col-lg-3"><label for="inputChildBirthday" class="small">Birthday</label><input type="text" id="inputChildBirthday" class="form-control noMarginTop" placeholder="MM/DD/YYYY" name="newch_1_birthdate" value="" required></div><div class="col-lg-12"><label for="importantToKnow" class="small">Important to know</label><textarea class="form-control" placeholder="Important to know" id="importantToKnow" rows="7" name="newch_1_notes"></textarea></div></div>')
		    }
            if($(".row-emergency").length == 0){
                $(".container-emergency").append('<div class="row row-tabs row-emergency"><div class="col-lg-12"><div class="row"><div class="col-lg-4"><label for="inputEmergencyName" class="small">Name</label><input type="text" id="inputEmergencyName" class="form-control noMarginTop" placeholder="Name" name="newem_1_name" value="" required></div><div class="col-lg-4"><label for="inputEmergencyMail" class="small">Email</label><input type="text" id="inputEmergencyMail" class="form-control noMarginTop" placeholder="Email" name="newem_1_mail" value="" required></div><div class="col-lg-4"><label for="inputEmergencyPhone" class="small">Phone Number</label><input type="text" id="inputEmergencyPhone" class="form-control noMarginTop" placeholder="Phone number" name="newem_1_phone" value="" required></div><div class="col-lg-4"><label for="inputEmergencyRelation" class="small">Relation to Children</label><input type="text" id="inputEmergencyRelation" class="form-control noMarginTop" placeholder="Relation to Children" name="newem_1_relation" value="" required></div></div></div></div>');
            }
			$("#addchildren").click(function () {
                childNumber= $(".row-data-child").length +1;
                $(".row-child > .col-lg-12").append('<div class="row row-data-child"><div class="col-lg-2"><div class="selectGenre"><a href="javascript:;" class="boy child selected" data-genre="boy"><img src="img/ico/main/0299-baby2.svg" width="50" alt=""></a><a href="javascript:;" class="girl child" data-genre="girl"><img src="img/ico/main/0298-baby.svg" width="50" alt=""></a><input type="hidden" id="inputSelectGenre" name="newch_'+childNumber+'_genre" value="boy"></div></div><div class="col-lg-4"><label for="inputChildName" class="small">Name</label><input type="text" id="inputChildName" class="form-control noMarginTop" placeholder="Name" name="newch_'+childNumber+'_name" value="" required></div><div class="col-lg-3"><label for="inputChildAge" class="small">Age</label><input type="text" id="inputChildAge" class="form-control noMarginTop" placeholder="Age" name="newch_'+childNumber+'_age" value="" required></div><div class="col-lg-3"><label for="inputChildBirthday" class="small">Birthday</label><input type="text" id="inputChildBirthday" class="form-control noMarginTop" placeholder="MM/DD/YYYY" name="newch_'+childNumber+'_birthdate" value="" required></div><div class="col-lg-12"><label for="importantToKnow" class="small">Important to know</label><textarea class="form-control" placeholder="Important to know" id="importantToKnow" rows="7" name="newch_'+childNumber+'_notes"></textarea></div></div>')
	        });

			$("#addemergency").click(function () {
                emergencyNumber= $(".row-emergency").length +1;
	            $(".container-emergency").append('<div class="row row-tabs row-emergency"><div class="col-lg-12"><div class="row"><div class="col-lg-4"><label for="inputEmergencyName" class="small">Name</label><input type="text" id="inputEmergencyName" class="form-control noMarginTop" placeholder="Name" name="newem_'+emergencyNumber+'_name" value="" required></div><div class="col-lg-4"><label for="inputEmergencyMail" class="small">Email</label><input type="text" id="inputEmergencyMail" class="form-control noMarginTop" placeholder="Email" name="newem_'+emergencyNumber+'_mail" value="" required></div><div class="col-lg-4"><label for="inputEmergencyPhone" class="small">Phone Number</label><input type="text" id="inputEmergencyPhone" class="form-control noMarginTop" placeholder="Phone number" name="newem_'+emergencyNumber+'_phone" value="" required></div><div class="col-lg-4"><label for="inputEmergencyRelation" class="small">Relation to Children</label><input type="text" id="inputEmergencyRelation" class="form-control noMarginTop" placeholder="Relation to Children" name="newem_'+emergencyNumber+'_relation" value="" required></div></div></div></div>');
	        });

            <?php 
           // $completed = ParentDAO::isCompletedProfile($_SESSION["parent_id"]);
		   // if(!$completed) { ?>
		    if($('#inputCellPhone').val() == ""){
		        $('#inputCellPhone').addClass(" invalid");
		    }
		    if($('#inputChildName').val() == ""){
		        $('#inputChildName').addClass(" invalid");
		    }
		    if($('#inputChildAge').val() == ""){
		        $('#inputChildAge').addClass(" invalid");
		    }
		    if($('#inputChildBirthday').val() == ""){
		        $('#inputChildBirthday').addClass(" invalid");
		    }
		    /*if($('#importantToKnow').val() == ""){
		        $('#importantToKnow').addClass(" invalid");
		    }*/
		    if($('#inputEmergencyName').val() == ""){
		        $('#inputEmergencyName').addClass(" invalid");
		    }
		    if($('#inputEmergencyMail').val() == ""){
		        $('#inputEmergencyMail').addClass(" invalid");
		    }
		    if($('#inputEmergencyPhone').val() == ""){
		        $('#inputEmergencyPhone').addClass(" invalid");
		    }
		    if($('#inputEmergencyRelation').val() == ""){
		        $('#inputEmergencyRelation').addClass(" invalid");
		    }
	        <?php  //} ?>
		    });
		    
		   
		    
		</script>
		
		<?php if($selectedPlaydate!=null) { ?>
          <script>
            $(document).ready(function (){
              $(".my-dashboard .nav-tabs").css("display","flex");
              $(".my-dashboard .tab-content").fadeIn(function(){
                  $("html, body").animate({ scrollTop: $('#selected-playdate').offset().top-300}, 1000);
              }); 
            });     
          </script>
        <?php } ?>
		
		<?php if($selectedPastPlaydate!=null) { ?>
    		<script>
                $(document).ready(function (){
                    $(".my-dashboard .nav-tabs").css("display","flex");
                    $(".my-dashboard .tab-content").fadeIn(function(){
                        $("html, body").animate({ scrollTop: $('#selected-past-playdate').offset().top-300}, 1000);
                    }); 
                }); 		
		  </script>
		<?php } ?>
		<script>
		  var url =  window.location.hash;
		  var res = url.split("=");
		  if(res[1] == "pal"){ 
      $('#incomplete-data').attr("style", "display:none"); 
      $('#complete-profile').attr("style", "display:none");
    } 
		 else {
      <?php
		if ($isParent) {
		    if($_SESSION['checkLogin'] == 1){ ?>
		    $('#complete-profile').attr("style", "display:none");
            $('#incomplete-data').attr("style", "display:none");
            
	<?php $_SESSION['checkLogin'] = 0; }
	else {
		    
		    $completed = ParentDAO::isCompletedProfile($_SESSION["parent_id"]);
		    if(!$completed) {
		?>
		
		$('#incomplete-data').modal('show');
		$('#complete-profile').attr("style", "display:none");
			
		<?php 
        }
        else{
          if($_SESSION['updateProfile'] == 1) {?>
            $('#complete-profile').modal('show');
            $('#incomplete-data').attr("style", "display:none");
       <?php } else {?>
            $('#complete-profile').attr("style", "display:none");
            $('#incomplete-data').attr("style", "display:none");
      <?php }
        }
	  }
	 }
	?>
     }
  </script>
	
	<?php if($invitedToPal) { ?>
        	<script>$('#invite-friends-modal').modal('show');</script>
        <?php } ?>
        
        <script>
        var url = location.pathname;
        
        if(url.indexOf('specialistsnominate') > -1) {
            	$("html, body").animate({ scrollTop: $('#specialistsnominate').offset().top-100}, 1000);
        }

        $('.bt-link-reserved-playdate').click(function(){
          $('.nav-tabs a[href="#nav-specialists"]').tab('show');
          $('#invite-friends-modal').modal('hide');
        })
        // $('#request').click(function(){
        //   $('#complete-profile').modal('hide');
        //   $('a[href="#nav-request"]').tab('show');
        //   // $( "#nav-request" ).tabs("select", "#nav-request" );
        // })
        </script>
<!--        <script>$('#notclose').click(function(e){-->
<!--		$('#incomplete-data').modal('hide');-->
<!--	});-->
<!--	$('#yesclose').click(function(e){-->
<!--		$('#complete-profile').modal('hide');-->
<!--	})-->
<!--</script>-->
  </body>

</html>

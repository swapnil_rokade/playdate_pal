<?php
/**
 * PLAYDATE - SPECIALIST CREATE PLAYDATE
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isSpecialist) { //Funci�n solo para especialistas
    header("Location: /");
}
$specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);


if(isset($_REQUEST["operation"]) && ($_REQUEST["operation"]=="createplaydate")) {
    //Crear playdate
    $playdate = new Playdate();
    $playdate->readFromRow($_REQUEST);
    if($playdate->getName()!=null) {
        $playdate->setId_specialist($specialist->getId_specialist());
        $playdate->setStatus(Playdate::$STATUS_PUBLISHED); //publicado
        //$playdate->setPrivacy_type(Playdate::$PRIVACITY_PUBLIC); //always public playdate
        
        //Addedby Priya
        if($_REQUEST['add1_id_check'] == 'on'){
            $playdate->setAdd1_id_check(1);
        }
        if($_REQUEST['add2_id_check'] == 'on'){
            $playdate->setAdd2_id_check(1);
        }
        if($_REQUEST['add3_id_check'] == 'on'){
            $playdate->setAdd3_id_check(1);
        }
        if($_REQUEST['add4_id_check'] == 'on'){
            $playdate->setAdd4_id_check(1);
        }
        $playdate->setType_of_Playdate(1);
      //  echo "<pre>";print_r($playdate);exit;
        $playdate = PlaydateDAO::createPlaydate($playdate);
        
        //Insertamos sus intereses
        if(!empty($_POST['interests'])) {
            foreach($_POST['interests'] as $interest) {
                InterestDAO::createPlaydateInterest($playdate->getId_playdate(), $interest);
            }
        }
        
        //Insertamos sus supporters, si hay
        if(!empty($_POST['id_supporter'])) {
            foreach($_POST['id_supporter'] as $id_supporter) {
                //SpecialistDAO::createPlaydateSupporter($playdate->getId_playdate(), $id_supporter);
                
                //Mail to supporters with info so they can apply
                $auxSupporter = SpecialistDAO::getSpecialist($id_supporter);
                if($auxSupporter!=null){
                    MailUtils::sendPlaydateSupportSuggest($auxSupporter->getEmail(), $specialist->getFullName(), $auxSupporter->getName(), $playdate->getName(), $playdate->getId_playdate());
                }
                
                
            }
        }
        
        //Insertamos las localizaciones
        if(!empty($_POST['id_neighborhood'])) {
            foreach($_POST['id_neighborhood'] as $auxlocation) {
                NeighborhoodDAO::createPlaydateNeighborhood($playdate->getId_playdate(), $auxlocation);
            }
        }
        
        if(!empty($_POST['inviteFriendsPopup'])) {
            $name = $specialist->getName();
            
            $event_place = $playdate->getLoc_address().", ".$playdate->getLoc_city().", ".$playdate->getLoc_state();
            
            foreach($_POST['inviteFriendsPopup'] as $auxfriend) {
                $friend = ParentDAO::getParentByUsernameOrEmail($auxfriend);
                if($friend!=null) {
                    MailUtils::sendPlaydateInviteFriend($friend->getEmail(), $auxfriend, $name, $playdate->getName(), Utils::dateFormat($playdate->getDate()), $event_place, Utils::get12hourFormat($playdate->getTime_init()),Utils::get12hourFormat($playdate->getTime_end()), "/playdate.php?id_playdate=".$playdate->getId_playdate());
                } else if(filter_var($auxfriend, FILTER_VALIDATE_EMAIL)) {
                    MailUtils::sendPlaydateInviteFriend($auxfriend, $auxfriend, $name, $playdate->getName(), Utils::dateFormat($playdate->getDate()), $event_place, Utils::get12hourFormat($playdate->getTime_init()), Utils::get12hourFormat($playdate->getTime_end()), "/playdate.php?id_playdate=".$playdate->getId_playdate());
                }
            }
        }
        
        if(($playdate->getPrivacy_type()==Playdate::$PRIVACITY_GROUP) && ($playdate->getPrivacy_group_id()!=null) && is_numeric($playdate->getPrivacy_group_id())) {
            //associate playdate with group
            PlaydateDAO::addPlaydateToGroup($playdate->getId_playdate(), $playdate->getPrivacy_group_id());
        }
        
        
        //Notification to supporters if public and num supporters > 0
        if(($playdate->getPrivacy_type()==Playdate::$PRIVACITY_PUBLIC) && ($playdate->getNum_supporters()!=null) && ($playdate->getNum_supporters()>0)) {
            $supportCandidates = SpecialistDAO::getSupportersList(1,1000, null, null);
            if($supportCandidates!=null) {
                foreach($supportCandidates as $candidate) {
                    
                    //Enviamos notificacion
                    $pdTime =  Utils::dateFormat($playdate->getDate())." ".Utils::get12hourFormat($playdate->getTime_init())." - ".Utils::get12hourFormat($playdate->getTime_end());
                    $leadName = "No Lead";
                    $leadMail = null;
                    if($playdate->getId_specialist()!=null) {
                        $pdSpecialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
                        if($pdSpecialist!=null) {
                            $leadName = $pdSpecialist->getFullName();
                            $leadMail = $pdSpecialist->getEmail();
                        }
                    }
                    
                    //Mail Booking Confirmation to this support candidate
                    MailUtils::sendPlaydateNewJob($candidate->getEmail(), $candidate->getName(), $playdate->getName(), $pdTime, $leadName);
                    
                }
            }
        }
        
        header("Location: /specialist-dashboard.php#current");
    }
    
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create a Playdate - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
	<style>
    .selectTime a.selected{padding: 0px 7px;!important}
  </style>
  </head>

  <body class="simple-page create-playdate<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>
	<?php include("modal/modal-background-check.php") ?>
    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Create a Playdate</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <?php 
        $ratingvalue = RatingDAO::getSpecialistValue($specialist->getId_specialist());
        $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialist->getId_specialist());
        ?>
        <div class="col-lg-3 sidebar  my-dashboard">
          <div class="box-playdates">
            <div class="img-container">
              <img src="<?php echo $specialist->getPicture() ?>" alt="Picture of <?php echo $specialist->getFullName() ?>">
            </div>
            <div class="info">
              <a href="/specialist-dashboard.php" class="title"><?php echo $specialist->getName() ?></a>
              <span class="subtitle"><?php echo $specialist->getUsername() ?></span>
            </div>
            <div class="puntuacion">
              <ul>
                <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
              	<li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
              </ul>
              <span class="num"><?php echo $ratingvalue ?>/5</span>
            </div>
            <div class="separator"></div>
            <div class="data certifications">
              <div class="row">
                <div class="col-lg-6">
                  <?php if($specialist->getUs_citizen()==0) { ?>
                      <div class="info check" data-toggle="modal" data-target="#modal-legal"><span>background check</span></div>
                      <?php } else { ?>
                      <div class="info check"><span>background check</span></div>
                      <?php } ?>
                </div>
                 <div class="col-lg-6">
                  <div class="info certificate"><span>approved by PAL</span></div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="info family"><span>1 repeat families</span></div>
                </div>
                 <div class="col-lg-6">
                  <div class="info playdate"><span><?php echo $numPlaydates ?><br/>playdates</span></div>
                </div>
              </div>
            </div>
             <div class="separator"></div>
            <div class="data">
              <p><?php echo $specialist->getAbout_me(); ?></p>
              <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist()?>&slug=<?php echo $specialist->getSlug() ?>" title="" class="bt-profile">View full Profile</a>
            </div>
          </div>

        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <form class="form-request-playdate" id="formCreatePlaydate" action="/specialist-create-playdate.php" method="post" autocomplete="off"> 
            <input type="hidden" name="operation" value="createplaydate" />
          	
            <div class="row">

              

              <div class="col-lg-4">
                <p class="titulo">Name</p>
                <label for="inputPlaydateName" class="sr-only">Playdate Name</label>
                <input type="text" id="inputPlaydateName" class="form-control noMarginTop" placeholder="Playdate Name" name="name" required="required">
              </div>

              <div class="col-lg-4">
                <p class="titulo">Location</p>
                <div class="form-group">
                  <div class="selectdiv">
                    <label for="venue">
                      <select class="form-control" id="venue" name="venue">
                        <option value="venue">Venue</option>
                        <option value="playground">Playground</option>
                        <option value="familyHome">Family Home</option>
                        <option value="publicAttraction">Public Attraction</option>
                        <option value="indoorPark">Indoor Park</option>
                        <option value="dropInClass">Drop In Class</option>
                        <option value="dropInPlayspace">Drop In Playspace</option>
                        <option value="artStudio">Art Studio</option>
                        <option value="fitnessCenter">Fitness Center</option>
                        <option value="educationalInstitution">Educational Institution</option>
                        <option value="communityCenter">Community Center</option>
                        <option value="familyTheatre">Family Theatre</option>
                        <option value="specialEvent">Special Event</option>
                      </select>
                    </label>
                  </div>
                </div> 
                 <div class="form-group">
                  <label for="inputAddress" class="sr-only">Address</label>
                  <input type="text" id="inputAddress" class="form-control noMarginTop" placeholder="Address" name="loc_address" value="">
                </div> 
                 <div class="form-group">
                  <label for="inputCity" class="sr-only">City</label>
                  <input type="text" id="inputCity" class="form-control noMarginTop" placeholder="City" name="loc_city" value="">
                </div> 
                 <div class="form-group">
                  <label for="inputVenue" class="sr-only">State</label>
                  <input type="text" id="inputState" class="form-control noMarginTop" placeholder="State" name="loc_state" value="">
                </div> 
                 <div class="form-group">
                  <label for="inputZipCode" class="sr-only">Neighborhoods</label>
                  <input type="text" id="location" name="location" class="form-control noMarginTop inputLocation" placeholder="neighborhood">
                  <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                </div> 
                <div class="container-tags">

                 </div>
              </div>

              <div class="col-lg-4 type-of-care">
                <p class="titulo">Type of care</p>
              
                  <?php 
                  $types = TypecareDAO::getTypecareList();
                  foreach ($types as $auxType) {
                      if($auxType->getId_typecare()!=7) {
                  ?>
              	  <div class="form-group">
                    <input type="radio" name="type" id="<?php echo $auxType->getId_typecare()?>" value="<?php echo $auxType->getId_typecare()?>" class="form-control" style="border-radius: 25px;">
                    <label for="<?php echo $auxType->getId_typecare()?>"><?php echo $auxType->getName() ?> 
                    	<?php if($auxType->getDescription()!=null) { ?>
                    	<a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="<?php echo $auxType->getDescription() ?>"><i class="fa fa-question-circle"></i></a>
                    	<?php } ?>
                    </label>
                  </div> 
              	  <?php 
                      }
                  }
              	  ?>
                
              </div>

            </div>

            <div class="row row-tabs">

               <div class="col-lg-4 date-range">
                <p class="titulo">Date</p>
                  <div class="input-group">
                    <input type="text" placeholder="MM/DD/YYYY" class="form-control datepicker readonly" readonly="readonly" id="dp1524041141130" name="date">
                  </div>

              </div>

              <div class="col-lg-4">
                <p class="titulo">Time</p>
                <div id="time-container-profile">
                  <div class="day-time">
                    <div class="input-group">
                      <div class="playdate selectTime from">
                         <?php 
                          $fieldName = "time_init";
                          $fieldValue = null;
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                    <span> - </span>
                    <div class="input-group">
                      <div class="playdate selectTime to">
                          <?php 
                          $fieldName = "time_end";
                          $fieldValue = null;
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-4">
                <p class="titulo">Age Range</p>
                <div id="age-container-profile">
                  <div class="day-time">
                    <div class="input-group">
                      <label class="sr-only" for="inputAgeFrom">From</label>
                      <input type="text" id="inputAgeFrom" placeholder="-" class="form-control input-age" name="age_init">
                    </div>
                    <span> - </span>
                    <div class="input-group">
                      <label class="sr-only" for="inputAgeTo">To</label>
                      <input type="text" id="inputAgeTo" placeholder="-" class="form-control  input-age" name="age_end">
                    </div>
                  </div>
                </div>
              </div>

          </div>

          <div class="row row-tabs">
            
            <div class="col-lg-4">
              <div class="row">
                <p class="titulo">Categories</p>
                <div class="col">
                  <div class="collapse multi-collapse show" id="categories-container">
                    
<div class="list-container stem">
                        <div class="form-group">
                          <input type="checkbox" id="1" value="1" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="1">Academic</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="2" value="2" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="2">STEM</label>
                        </div> 
                      </div>
                      
                      <div class="list-container creative">
                        <div class="form-group">
                          <input type="checkbox" id="3" value="3" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="3">Creative</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="4" value="4" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="4">Performing Arts</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="5" value="5" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="5">Language &amp; Culture</label>
                        </div> 
                      </div>

                      <div class="list-container outdoor">
                        <div class="form-group">
                          <input type="checkbox" id="6" value="6" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="6">Play</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="7" value="7" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="7">Sports &amp; Recreation</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="8" value="8" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="8">Health &amp; Wellness</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="9" value="9" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="9">Outdoor</label>
                        </div>
                      </div>

                      <div class="list-container attractions">
                        <div class="form-group">
                          <input type="checkbox" id="10" value="10" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="10">NYC Museums</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="11" value="11" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="11">NYC Attractions</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="12" value="12" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="12">NYC Theater</label>
                        </div> 
                      </div>


                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-8">                    
              <p class="titulo">Description</p>
              <label for="descriptionPlaydate" class="sr-only">Description</label>
              <textarea class="form-control" placeholder="Describe your playdate (* optional)" id="descriptionPlaydate" rows="22" name="description"></textarea>                
            </div>

          </div>

          <div class="row row-tabs">
            
            <div class="col-lg-4">
              <p class="titulo">Banners</p>
              <div class="admin-slider">
                <div class="all-slides">
                  <?php /* ?>
                  <div class="slide selected"><img src="img/box-home-1.jpg"></div>
                  <div class="slide"><img src="img/box-home-2.jpg"></div>
                  <div class="slide"><img src="img/box-home-3.jpg"></div>
                  <div class="slide"><img src="img/box-home-4.jpg"></div>
                  */?>
                  <input type="hidden" name="picture" class="image-selected" value="">
                </div>
                <div class="pagination">
                  <ul>
                  <?php /* ?>
                    <li><a href="javascript:;">1</a></li>
                    <li><a href="javascript:;" class="selected">2</a></li>
                    <li><a href="javascript:;">3</a></li>
                    <li><a href="javascript:;">4</a></li>
                  */?>
                  </ul>
                </div>
              </div>
              
            </div>


            <div class="col-lg-8 itinerary">                    
              <p class="titulo">Itinerary</p>
              
              <!-- act1 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act1_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act1_time";
                    //$fieldValue = $parent->getChildren_avb_mon_init();
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity1" name="act1_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act1_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act1_note">
                </div>
              </div>
              <!-- /act1 -->
              
              <!-- act2 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act2_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act2_time";
                    //$fieldValue = $parent->getChildren_avb_mon_init();
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity2" name="act2_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act2_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act2_note">
                </div>
              </div>
              <!-- /act2 -->

              <!-- act3 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act3_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act3_time";
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity3" name="act3_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act3_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act3_note">
                </div>
              </div>
              <!-- /act3 -->
            
            <!-- act4 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act4_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act4_time";
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity4" name="act4_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act4_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act4_note">
                </div>
              </div>
              <!-- /act4 --> 
              
                          
            </div>

          </div>

          <div class="row row-tabs">

              <div class="col-lg-4 privacy">
                  
                <p class="titulo">Privacy</p>
                <div class="form-group">
                  <input type="radio" id="privacy1" class="form-control" name="privacy_type" value="<?php echo Playdate::$PRIVACITY_PUBLIC ?>" checked="checked">
                  <label for="privacy1" style="font-size:0.9em">Public- Open to anyone</label>
                </div>
                <div class="form-group">
                  <input type="radio" id="privacy2" class="form-control"  name="privacy_type" value="<?php echo Playdate::$PRIVACITY_PRIVATE ?>">
                  <label for="privacy2" style="font-size:0.9em">Private- Only invited parents</label>
                </div>
                <div class="form-group">
                  <input type="radio" id="privacy3" class="form-control"  name="privacy_type" value="<?php echo Playdate::$PRIVACITY_GROUP?>">
                  <label for="availableGroups" style="font-size:0.9em">Open only to Group</label>
                </div>
                <div class="form-group">
                  <label for="privacy3" class="sr-only">Group name</label>
                  <?php /* ?><input type="text" id="inputSelectGroup" class="form-control noMarginTop" placeholder="Group Name" autocomplete="off"> */?>
                  <div class="selectdiv">
                    <label for="inputNumspots">
                      <select class="form-control" id="inputNumspots" name="privacy_group_id">
                      	<option value="">---</option>
                      	<?php 
                      	$specGroups = GroupDAO::getGroupsBySpecialist($specialist->getId_specialist());
                      	foreach($specGroups as $auxGrp) {
                      	?>
                      	<option value="<?php echo $auxGrp->getId_group()?>"><?php echo $auxGrp->getName() ?></option>
                      	<?php     
                      	}
                      	?>
                      </select>
                    </label>
                  </div>
                </div>
              




              </div>

              <div class="col-lg-8 itinerary activityBudget">                    
              <p class="titulo">Activity Budget <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="The Activity Budget is a per child rate that will be charged to each Parent. If you prefer to be paid in cash or using a mobile payment app, please leave this section blank and include your budget preferences in the description portion above. Activity Budgets are not necessary for a successful playdate, but should be considered if there may be admissions, meals or additional supplies associated with the experience."><i class="fa fa-question-circle"></i></a></p>
              
<!-- addon1 -->
              <div class="row">
                <div class="col-lg-6">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add1_id" name="add1_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="number" id="budget1" placeholder="$0" class="form-control  input-budget" name="add1_price">
                </div>
                <div class="col-lg-3">
                  <input type="checkbox" class="form-control" name="add1_id_check" id="add1_id_check" onclick="checkOption(add1_id_check)">
                  <label for="add1_id_check" style="font-size:0.9em">Optional</label>
                </div>
              </div>  
              <!-- /addon1 -->
              
              <!-- addon2 -->
              <div class="row">
                <div class="col-lg-6">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add2_id" name="add2_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="number" id="budget2" placeholder="$0" class="form-control  input-budget" name="add2_price">
                </div>
                <div class="col-lg-3">
                  <input type="checkbox" class="form-control" name="add2_id_check" id="add2_id_check" onclick="checkOption(add2_id_check)">
                  <label for="add2_id_check" style="font-size:0.9em">Optional</label>
                </div>
              </div>  
              <!-- /addon2 -->

              <!-- addon3 -->
              <div class="row">
                <div class="col-lg-6">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add3_id" name="add3_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="number" id="budget3" placeholder="$0" class="form-control  input-budget" name="add3_price">
                </div>
                <div class="col-lg-3">
                  <input type="checkbox" class="form-control" name="add3_id_check" id="add3_id_check" onclick="checkOption(add3_id_check)">
                  <label for="add3_id_check" style="font-size:0.9em">Optional</label>
                </div>
              </div>  
              <!-- /addon3 -->
              
              <!-- addon4 -->
              <div class="row">
                <div class="col-lg-6">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add4_id" name="add4_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="number" id="budget4" placeholder="$0" class="form-control  input-budget" name="add4_price">
                </div>
                <div class="col-lg-3">
                  <input type="checkbox" class="form-control" name="add4_id_check" id="add4_id_check" onclick="checkOption(add4_id_check)">
                  <label for="add4_id_check" style="font-size:0.9em">Optional</label>
                </div>
              </div>  
              <!-- /addon4 -->              

          
            </div>

          </div>

          <div class="row row-tabs">

			  <div class="col-lg-4">
                  <p class="titulo">Invite Parents</p>
                  <label for="inputInviteParents" class="sr-only">Invite Parents</label>
                  <input type="text" id="inputInviteFriends" class="form-control noMarginTop input-invite-friend" placeholder="Username or Email">
                  <a href="javascript:;" class="bt-save-profile last bt-add-friend">Add Parent</a>
                  <p class="errorLocation"></p>
                  <div class="container-tagss container-tags-email">
                  </div>
              </div>                
                
              <div class="col-lg-8 itinerary">                    
                <p class="titulo">Supervised travel for up 2 kids <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="This is only a suggested budget for Supervised Travel. It will not be included in the per child price of this playdate. Payment via cash or mobile cash app must be facilitated by Parents and Specialists directly to cover the cost of travel and travel time (if time is not already included in Playdate itinerary)."><i class="fa fa-question-circle"></i></a></p>
                <div class="row">
                	<div class="col-lg-12">
                		<input type="text" id="inputAddress" class="form-control noMarginTop" placeholder="pick up address * optional" name="pickup_address" value="">
                	</div>
                </div>
                <div class="row">
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputWalk">Walk</label>
                    <input type="text" id="inputWalk" placeholder="$0" class="form-control input-walk" name="pickup_walk">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTrain">Train</label>
                    <input type="text" id="inputTrain" placeholder="$0" class="form-control input-train" name="pickup_train">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputCar">Car</label>
                    <input type="text" id="inputCar" placeholder="$0" class="form-control input-car" name="pickup_car">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTaxi">Taxi</label>
                    <input type="text" id="inputTaxi" placeholder="$0" class="form-control input-taxi" name="pickup_taxi">
                  </div>
                </div>
                <p class=""></p>
                <div class="row">
                	<div class="col-lg-12">
                		<input type="text" id="inputAddress" class="form-control noMarginTop" placeholder="drop off address * optional" name="dropoff_address" value="">
                	</div>
                </div>
                <div class="row">
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputWalk">Walk</label>
                    <input type="text" id="inputWalk" placeholder="$0" class="form-control input-walk" name="dropoff_walk">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTrain">Train</label>
                    <input type="text" id="inputTrain" placeholder="$0" class="form-control input-train" name="dropoff_train">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputCar">Car</label>
                    <input type="text" id="inputCar" placeholder="$0" class="form-control input-car" name="dropoff_car">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTaxi">Taxi</label>
                    <input type="text" id="inputTaxi" placeholder="$0" class="form-control input-taxi" name="dropoff_taxi">
                  </div>
                </div>

              </div>

            </div>

            <div class="row row-tabs">

			  <div class="col-lg-4">
                  <p class="titulo">Minimum Kids</p>
                  <div class="selectdiv">
                    <label for="inputNumspots">
                      <select class="form-control" id="inputMinKid" name="min_kids" onchange="myFunction(this.value)">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                      </select>
                    </label>
                  </div>
              </div>

              <div class="col-lg-4">
                  <p class="titulo">Open Spots <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="This is the number of children that this playdate is open to."><i class="fa fa-question-circle"></i></a></p>
                  <div class="selectdiv">
                    <label for="inputNumspots">
                      <select class="form-control" id="inputNumspots" name="open_spots">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                      </select>
                    </label>
                  </div>
              </div>
              
              <div class="col-lg-4 itinerary">                    
               <p class="titulo">Support <span>*optional</span>  <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Use our Suggested Adult to Child Ratio below to hire the appropriate number of Support Specialists. Suggested Age Group Ratios are: Under 3 - 2 children to 1 adult. 3 - 4 years - 4 children to 1 adult. 5 - 7 years - 5 children to 1 adult. 8+ years - 6 children to 1 adult. We strongly suggest keeping this as your minimum team size."><i class="fa fa-question-circle"></i></a></p>
                <div class="row">
                  <div class="col-lg-12">
                  <label for="inputSpecialistName" class="sr-only">Name</label>
                    <input type="text" id="inputSpecialistName" class="form-control noMarginTop inputSupporter" placeholder="Name">
                    <a href="javascript:;" class="bt-save-profile last bt-add-supporter" data-toggle="modal" data-target="#playdate-reserved">Add Supporter</a>
                    <div class="container-supporter-tags"></div>
                  </div>
                </div>
                
              </div>

            </div>
            
            <div class="row row-tabs">

              <div class="col-lg-4">
                  <div class="btnSubmit">
                    <div class="form-group">
                      <p class="totalPrice">$<span class="initPrice"> </span><span class="endPrice"> </span></p>
                      <p for="checkPublish" class="messagePrice">select playdate time to calculate</p>
                    </div>
                    <button class="btn btn-md btn-primary bt-save-profile last" type="submit">Publish Playdate</button>
                  </div>
              </div>
            </div>


		  <?php /*?>
          <div class="row row-tabs">
            <div class="col-lg-4">
              <div class="btnSubmit">
                <div class="form-group">
                  <p class="totalPrice">$60-$80</p>
                  <p for="checkPublish">you earn if 2-3 children book</p>
                </div>
                <button class="btn btn-md btn-primary bt-save-profile last" type="submit">Publish Playdate</button>
              </div>
            </div>
          </div>
          */?>
          </form>
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
	<script>
	checkInterests();
  var initTime;
  var endTime;
  var openSpots;
  function payCalculator(){
    totaltime= endTime-initTime;
   // console.log(totaltime)
    if(!isNaN(totaltime) && totaltime>0){
      $(".initPrice").html(25*totaltime);
      $(".endPrice").html(" ");
     // $(".endPrice").html("-$"+(30*totaltime));
      $(".messagePrice").html("")
    }else{
      $(".initPrice").html(" ");
      $(".endPrice").html(" ");
      $(".messagePrice").html("select playdate time to calculate")
    }
  }
  $(".playdate.selectTime.from a").on("click", function(){
    initTime=parseFloat($(this).attr("data-time"));
    myFunction($('#inputMinKid').val());
    //payCalculator();
  })
  $(".playdate.selectTime.to a").on("click", function(){
    endTime=parseFloat($(this).attr("data-time"));
    myFunction($('#inputMinKid').val());
    //payCalculator();
  })

function checkOption(id){
   
      myFunction($('#inputMinKid').val());
 
}

function myFunction(value){
    var childCount = value;
   // alert(initTime);
   totaltime= endTime-initTime;
   //alert(initTime);
   if(!isNaN(totaltime) && totaltime>0){
       if(childCount == 1){
           var price = 18 * totaltime;
           var endprice = "";
          // var endprice = " -$"+ (30*totaltime);
           //var msg = "Price Per Child. Supervised Travel Charged Separately";
          
       }
       else if(childCount == 2){
           var price = 18 * totaltime;
           var endprice = "";
          // var msg = "Price Per Child. Supervised Travel Charged Separately";
       }
       else if(childCount == 3){
           var price = 16 * totaltime;
          var endprice = "";
         //  var msg = "25% off second child. Add-On fees charged separately (admissions, supplies, food, etc)";
       }
       else if(childCount == 4 || childCount == 5){
           var price = 12 * totaltime;
           var endprice = "";
         //  var msg = "25% off second child. Add-On fees charged separately (admissions, supplies, food, etc)";
       }
       else if(childCount == 6){
           var price = 11 * totaltime;
           var endprice = "";
         //  var msg = "25% off second child. Add-On fees charged separately (admissions, supplies, food, etc)";
       }
       else if(childCount == 7){
           var price = 10 * totaltime;
           var endprice = "";
         //  var msg = "25% off second child. Add-On fees charged separately (admissions, supplies, food, etc)";
       }
       else{
            var price = 10 * totaltime;
            var endprice = "";
         //   var msg = "25% off second child. Add-On fees charged separately (admissions, supplies, food, etc)";
       }
       
       if($('#add1_id').val() != ''){
        // console.log($('#add1_id_check').val());
         
         if($('#add1_id_check').is(":checked")){
          price = Number(price);
         }
         else{
          price = Number(price) + Number($('#budget1').val());
         }
       }
       if($('#add2_id').val() != ''){
          if($('#add2_id_check').is(":checked")){
          price = Number(price);
         }
         else{
         price = Number(price) + Number($('#budget2').val());
         }
         // console.log('budget2'+price);
       }
       if($('#add3_id').val() != ''){
         if($('#add3_id_check').is(":checked")){
          price = Number(price);
         }
         else{
         price = Number(price) + Number($('#budget3').val());
         }
        // console.log('budget3'+price);
       }
       if($('#add4_id').val() != ''){
          if($('#add4_id_check').is(":checked")){
          price = Number(price);
         }
         else{
         price = Number(price) + Number($('#budget4').val());
         }
        // console.log('budget4'+price);
       }
     // console.log('final'+price);
       $(".initPrice").html(price);
       $(".endPrice").html(endprice);
       $(".messagePrice").html("Supervised Travel Charged Separately")
   }
   else{
       $(".initPrice").html(" ");
      $(".endPrice").html(" ");
      $(".messagePrice").html("select playdate time to calculate")
   }
  }
  
  $('#formCreatePlaydate').on('submit', function(event) {
      event.preventDefault(); 
      $(".activityBudget .row").each(function(e){
        selectVal = $(this).find("select").val();
        inputVal = $(this).find("input").val();
        if(selectVal != ""){
          if(inputVal ==""){
            activityBudgetError("selectVal")
            return false;
          }          
        }else if(inputVal != ""){
          if(selectVal ==""){            
            activityBudgetError("inputVal");
            return false;
          }          
        }else{
          $('#formCreatePlaydate')[0].submit();
        }
      })
  });

  $(".activityBudget .row select").change(function(){
    $(".errorLocation").remove();
    if($(this).val()!=""){
      if($(this).parent().parent().parent().parent().find("input").val()==""){
        $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you select an option, you have to set its price</p>");
      }
    }else{
      if($(this).parent().parent().parent().parent().find("input").val()!=""){
        $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you select an option, you have to set its price</p>");
      }
    }
  })

  $(".activityBudget .row input").keyup(function(){
      //alert($(this).val());
    $(".errorLocation").remove();
    if($(this).val()!=""){
      if($(this).parent().parent().find("select").val()==""){
        $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you set a price, you have to select its option</p>");
      }
      else{
          myFunction($('#inputMinKid').val());
      }
    }else{
      if($(this).parent().parent().find("select").val()!=""){
        $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you set a price, you have to select its option</p>");
        myFunction($('#inputMinKid').val());
      }
      else{
          myFunction($('#inputMinKid').val());
      }
    }
  })

  function activityBudgetError(errorMsg){
    $(".errorLocation").remove();
    if(errorMsg=="selectVal"){
      $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you select an option, you have to set its price</p>");
    }else{
      $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you set a price, you have to select its option</p>");
    }
    $("#formCreatePlaydate button[type='submit']").after("<p class='errorLocation centered'>Review Activity Budget to continue</p>");
  }




  $(".bt-add-friend").on("click", function(){
	  $(".errorLocation").hide()
	    if($(".input-invite-friend").val()!=""){
	      $(".container-tags-email .tag").each(function(e){
	        if($("span", this).html()==$(".input-invite-friend").val()){
	          newEmail =true;
	        }
	      })
	      if(newEmail==true){
	        $(".errorLocation").show()
	        $(".errorLocation").html("The email already exist.");
	        $(".input-invite-friend").val("")
	        return false;
	        newEmail =false;
	      }else{
	        $(".errorLocation").hide()
	        $(".container-tags-email").append('<div class="tag"><span>'+$(".input-invite-friend").val()+'</span><a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a><input type="hidden" value="'+$(".input-invite-friend").val()+'"  name="inviteFriendsPopup[]"/></div>'); 
	        $(".inputLocation").val("");
	        $(".bt-save-profile.last.bt-add-location").css("display","none");
	        $(".input-invite-friend").val("")
	      }   
	    }else{
	      $(".errorLocation").show()
	      $(".errorLocation").html("The field can't be empty.</p>");
	    }
	    $(".input-invite-friend").blur();
	 });
   
	</script>

  </body>

</html>
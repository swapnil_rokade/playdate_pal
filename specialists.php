<?php 
/**
 * PLAYDATE - SPECIALISTS LIST
 */
include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");

//Recogemos los criterios de busqueda
$keyword = null;
if(isset($_REQUEST["keyword"]) && ($_REQUEST["keyword"]!="")) {
    $keyword = $_REQUEST["keyword"];
}

$age_1 = null;
if(isset($_REQUEST["age_1"]) && ($_REQUEST["age_1"]!="")) {
    $age_1 = $_REQUEST["age_1"];
}
$age_2 = null;
if(isset($_REQUEST["age_2"]) && ($_REQUEST["age_2"]!="")) {
    $age_2 = $_REQUEST["age_2"];
}
$age_3 = null;
if(isset($_REQUEST["age_3"]) && ($_REQUEST["age_3"]!="")) {
    $age_3 = $_REQUEST["age_3"];
}

$neighborhoods = null;
if(!empty($_POST['id_neighborhood'])) { 
    $neighborhoods = array();
    foreach($_POST['id_neighborhood'] as $auxlocation) {
        $neighborhoods[] = $auxlocation;
    }
}

$action = null;
if(isset($_REQUEST["action"])) {
    $action = $_REQUEST["action"];
}
$allspecialists = SpecialistDAO::getSpecialistsLeadList(1, 100, null, null);
$specialists = $allspecialists;

$allsupporters = SpecialistDAO::getSupportersList(1, 100, null, null);
$supporters = $allsupporters;

switch ($action) {
    case "search": {
        //Filter contents with criteria
        if($keyword!=null) {
            $filteredspecialists = array();
            foreach($specialists as $auxSpec) {
                if(strpos(strtolower($auxSpec->getAbout_me()), strtolower($_REQUEST["keyword"])) !==false) {
                    $filteredspecialists[] = $auxSpec;
                } else if(strpos(strtolower($auxSpec->getFullName()), strtolower($_REQUEST["keyword"])) !==false) {
                    $filteredspecialists[] = $auxSpec;
                }
            }
            $specialists = $filteredspecialists;
            
            $filteredsupporters = array();
            foreach($supporters as $auxSupp) {
                if(strpos(strtolower($auxSupp->getAbout_me()), strtolower($_REQUEST["keyword"])) !==false) {
                    $filteredsupporters[] = $auxSupp;
                } else if(strpos(strtolower($auxSupp->getFullName()), strtolower($_REQUEST["keyword"])) !==false) {
                    $filteredsupporters[] = $auxSupp;
                }
            }
            $supporters = $filteredsupporters;
        }
        
        //Age1
        if(($age_1=="1") || ($age_2=="1") || ($age_3=="1")) {
            $filteredspecialists = array();
            $filteredsupporters = array();
            if($age_1=="1") {
                foreach($specialists as $auxSpec) {
                    if(($auxSpec->getExp_range1()!=null) && ($auxSpec->getExp_range1()>0)) {
                        $filteredspecialists[] = $auxSpec;
                    }
                }
                
                foreach($supporters as $auxSpec) {
                    if(($auxSpec->getExp_range1()!=null) && ($auxSpec->getExp_range1()>0)) {
                        $filteredsupporters[] = $auxSpec;
                    }
                }
            }
            
            //Age2
            if($age_2=="1") {
                foreach($specialists as $auxSpec) {
                    if(($auxSpec->getExp_range2()!=null) && ($auxSpec->getExp_range2()>0)) {
                        $filteredspecialists[] = $auxSpec;
                    }
                }
                foreach($supporters as $auxSpec) {
                    if(($auxSpec->getExp_range2()!=null) && ($auxSpec->getExp_range2()>0)) {
                        $filteredsupporters[] = $auxSpec;
                    }
                }
            }
            
            //Age2
            if($age_3=="1") {
                foreach($specialists as $auxSpec) {
                    if(($auxSpec->getExp_range3()!=null) && ($auxSpec->getExp_range3()>0)) {
                        $filteredspecialists[] = $auxSpec;
                    }
                }
                foreach($supporters as $auxSpec) {
                    if(($auxSpec->getExp_range3()!=null) && ($auxSpec->getExp_range3()>0)) {
                        $filteredsupporters[] = $auxSpec;
                    }
                }
            }
            $specialists = $filteredspecialists;
            $supporters = $filteredsupporters;
        }
        
        //Neighborhoods
        if($neighborhoods!=null) {
            $filteredspecialists = array();
            foreach($specialists as $auxSpec) {
                $pdNeighList = NeighborhoodDAO::getNeighborhoodsListBySpecialist($auxSpec->getId_specialist());
                $included = false;
                foreach ($pdNeighList as $auxNeigh) {
                    if(in_array($auxNeigh->getId_neighborhood(), $neighborhoods)) {
                        $filteredspecialists[] = $auxSpec;
                    }
                }
            }
            $specialists = $filteredspecialists;
            
            
            $filteredsupporters = array();
            foreach($supporters as $auxSpec) {
                $pdNeighList = NeighborhoodDAO::getNeighborhoodsListBySpecialist($auxSpec->getId_specialist());
                $included = false;
                foreach ($pdNeighList as $auxNeigh) {
                    if(in_array($auxNeigh->getId_neighborhood(), $neighborhoods)) {
                        $filteredsupporters[] = $auxSpec;
                    }
                }
            }
            $supporters = $filteredsupporters;
            
        }
        
        
        break;
    }
}
        

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Specialists - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS --> 
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Specialists</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar">
		  <form name="frm_search" action="/specialists.php" method="post">
		  <input type="hidden" name="action" value="search" />
          <!-- Search -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#search-container" role="button" aria-expanded="false" aria-controls="search-container">Search</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="search-container">
                
                  <label for="inputSearch" class="sr-only">Search</label>
                  <input type="text" id="inputSearch" class="form-control" placeholder="keyword" name="keyword" value="<?php echo(($keyword!=null)?$keyword:"") ?>">
                
              </div>
            </div>
          </div>


          <!-- Location -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#location-container" role="button" aria-expanded="false" aria-controls="location-container">Service Area</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="location-container">
                
                  <label for="inputLocation" class="sr-only">Service Area</label>
                  <input type="text" id="inputLocation" class="form-control inputLocation" placeholder="neighborhood">
                  <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                
                <div class="container-tags">
					<?php 
                    if(($neighborhoods!=null) && (count($neighborhoods)>0)) {
                      foreach ($neighborhoods as $auxNeigh) {
                          $nei = NeighborhoodDAO::getNeighborhood($auxNeigh);
                          if($nei!=null) {
                    ?>
					<div class="tag">
                    	<span><?php echo $nei->getName()?></span>
                        <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                        <input type="hidden" value="<?php echo $nei->getId_neighborhood() ?>" name="id_neighborhood[]">
                    </div>
                  	<?php 
                          }
                      }
                    }
                    ?>                  
                </div>
              </div>
            </div>
          </div>

          <!-- Age -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#age-container" role="button" aria-expanded="false" aria-controls="age-container">Age Range<br>Experience</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="age-container">
                  
                  <div class="form-group first">
                    <input type="checkbox" id="0" class="form-control" placeholder="keyword" name="age_1" value="1"<?php echo (($age_1=="1")?"checked=\"checked\"":"")?>>
                    <label for="0">Under 1 year old</label>
                  </div> 
                  <div class="form-group">
                    <input type="checkbox" id="1" class="form-control" placeholder="keyword" name="age_2" value="1"<?php echo (($age_2=="1")?"checked=\"checked\"":"")?>>
                    <label for="1">2-3 years old</label>
                  </div> 
                  <div class="form-group">
                    <input type="checkbox" id="2" class="form-control" placeholder="keyword" name="age_3" value="1"<?php echo (($age_3=="1")?"checked=\"checked\"":"")?>>
                    <label for="2">4+ years old</label>
                  </div> 
                
              </div>
            </div>
          </div>

		  <div class="row">
            <div class="col">
            	<input type="submit" class="bt-save-profile last" value="Apply Filter">
            </div>
          </div>
          <div class="row">
            <div class="col">
            	<input type="button" class="bt-save-profile last" value="Clear Filter" onclick="top.location='/specialists.php'">
            </div>
          </div>
		  </form>
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          
          <div class="row featured-specialists">
          
			<?php 
		    //$specialists = SpecialistDAO::getSpecialistsList(1, 4, null, null);
		    foreach ($specialists as $specialistaux) {
		        $ratingvalue = RatingDAO::getSpecialistValue($specialistaux->getId_specialist());
		        $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialistaux->getId_specialist());
		        $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialistaux->getId_specialist());
            ?>
          
            <div class="col-lg-4 col-md-6 col-sm-6 box-specialist">
              <div class="flip">
                <div class="card">
                  <div class="specialist face front">
                    <div class="info">
                       <p><?php echo $specialistaux->getFullName() ?></p>
                       <div class="puntuacion">
                        <ul>
                          <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                        </ul>
                        <span class="num"><?php echo $ratingvalue ?>/5</span>
                      </div>                
                      
                    </div>             
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                  <div class="face back"  style="cursor:hand!important">
                    <div class="info-back">
                      <div class="ico-specialist family"><span><?php echo $numFamilies ?><br>repeat families</span></div>
                      <div class="ico-specialist pd"><span><?php echo $numPlaydates ?><br>playdates</span></div>
                      <p><?php echo $specialistaux->getAbout_me(250) ?></p>
                      <a class="bt-profile" style="z-index:100000!important" title="" href="/specialist-profile.php?id_specialist=<?php echo $specialistaux->getId_specialist()?>&slug=<?php echo $specialistaux->getSlug() ?>">View profile</a>
                    </div>           
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                </div>
              </div>
            </div>
			<?php } ?>

          </div>

          <div class="row featured-specialists">
            <div class="col-lg-12">                    
              <p class="titulo2 uppercase">Supporters <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Support Specialists review the itinerary and expectations set by their Lead, they show up on time and are responsible for ensuring children are safe, engaged and nurtered throughout the playdate experience. Both Leads and Supports must successfully pass the PAL vetting process."><i class="fa fa-question-circle"></i></a></p>              
            </div>
      <?php 
        
        foreach ($supporters as $specialistaux) {
            $ratingvalue = RatingDAO::getSpecialistValue($specialistaux->getId_specialist());
            $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialistaux->getId_specialist());
            $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialistaux->getId_specialist());
            ?>
          
            <div class="col-lg-4 col-md-6 col-sm-6 box-specialist">
              <div class="flip">
                <div class="card">
                  <div class="specialist face front">
                    <div class="info">
                       <p><?php echo $specialistaux->getFullName() ?></p>
                       <div class="puntuacion">
                        <ul>
                          <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                        </ul>
                        <span class="num"><?php echo $ratingvalue ?>/5</span>
                      </div>                
                      
                    </div>             
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                  <div class="face back">
                    <div class="info-back">
                      <div class="ico-specialist family"><span><?php echo $numFamilies ?><br>repeat families</span></div>
                      <div class="ico-specialist pd"><span><?php echo $numPlaydates ?><br>playdates</span></div>
                      <p><?php echo $specialistaux->getAbout_me(250) ?></p>
                      <a class="bt-profile" title="" href="/specialist-profile.php?id_specialist=<?php echo $specialistaux->getId_specialist()?>&slug=<?php echo $specialistaux->getSlug() ?>">View profile</a>
                    </div>           
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                </div>
              </div>
            </div>
      <?php } ?>

          </div>

		
            
          <div class="row info-box">

                  <div class="col-lg-12">
                    <div class="titulo noImages">
                      <h2>Passionate about Child Care?</h2>
                      <p>Well-paid and meaningful job opportunities in child care and enrichment.</p>
                      <form class="form-nominate" action="/apply-specialist.php" method="post">
                        <button class="btn btn-md btn-primary bt-titulo" type="submit">Apply to be a Playdate Specialist</button>  
                      </form>
                    </div>

                  </div>

              </div>

        
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

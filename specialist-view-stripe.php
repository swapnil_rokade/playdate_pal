<?php
/**
 * PLAYDATE - SPECIALIST CONNECT STRIPE AND OPEN STRIPE ACCOUNT 
 */

require_once('vendor/autoload.php');

include_once("classes/all_classes.php");
include_once("common.php");
include_once("connection.php");

use Stripe\Account;

if(!$isSpecialist) {
    header("Location: /");
}

\Stripe\Stripe::setApiKey($STRIPE_CONNECT_SECRET);

$specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
if(($specialist!=null) && ($specialist->getPay_stripeid()!=null) && ($specialist->getPay_stripeid()!="")) {
    $account = null;
    try {
        $account = Account::retrieve($specialist->getPay_stripeid());
    } catch(Exception  $e) {
        
    }
    if($account!=null) {
        $retorno = $account->login_links->create();
        header("Location: ".$retorno->url);
    } else {
        //Error - Account not found
        header("Location: /specialist-dashboard.php#playpacks");
    }
} else {
    header("Location: /specialist-dashboard.php#playpacks");
}
?>
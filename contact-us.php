<?php 
/**
 * PLAYDATE - CONTACT US SECTION
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");
require('constant.php');
$contactsent = false;

if(isset($_REQUEST["action"]) && ($_REQUEST["action"]=="contact")) {
    //ACTION - SAVE FORM INFO
    
    $contact = new Contact();
    $contact->readFromRow($_REQUEST);
    if($contact->getName()!=null) {
        $contact = ContactDAO::createContact($contact);
        $contactsent = ($contact->getId_contact()>0); 
    }
    
}

$backUrl = "/contact-us.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Contact Us - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?> contact-us-page">
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Contact us</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">



    <!-- Featured Specialists -->
    <div class="row featured-specialists">
        
        <div class="col-lg-3">
          <div class="flip">
            <div class="card">
              <div class="specialist face front">
                <div class="info">
                   <p>Amanda</p>    
                </div>             
                <img src="img/team/team-amanda.jpg"/>
              </div>
              <div class="face back">
                <div class="info-back">
                  <div class="info-team">
                    <p>About Amanda:</p>
                    <p>As the founder of Project Playdate, Amanda has been supporting children in the cultivation of joy, curiosity, and meaningful connection since 2012. Amanda graduated from New York University with a B.A. in Social Work and holds her MBA from Columbia University. After over 13 years as a childcare professional and entrepreneur, Amanda has social care down to a science. She is using this passion and expertise to drive the vision behind PAL." class="grey-tooltip</p>
                    <a class="bt-profile"  style="z-index:100000!important" title="As the founder of Project Playdate, Amanda has been supporting children in the cultivation of joy, curiosity, and meaningful connection since 2012. Amanda graduated from New York University with a B.A. in Social Work and holds her MBA from Columbia University. After over 13 years as a childcare professional and entrepreneur, Amanda has social care down to a science. She is using this passion and expertise to drive the vision behind PAL."  href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top">View profile</a>
                  </div>
                </div>            
                <img src="img/team/team-amanda.jpg"/>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-6">
          <div class="flip">
            <div class="card">
              <div class="specialist face front">
                <div class="info">
                   <p>Courtney</p>
                  
                </div>             
                <img src="img/team/team-courtney.jpg"/>
              </div>
              <div class="face back">
                <div class="info-back">
                  <div class="info-team">
                    <p>About Courtney:</p>
                    <p>Courtney is an advertising account leader with 14 years of advertising and marketing experience. She has worked with some of the most popular children and family brands and products including; LEGO, Hasbro, Mattel, Nickelodeon, Kid Cuisine, DreamWorks, Crayola, Walmart Kids, and NBA Hoop Troop. Courtney holds her MBA from Columbia University and is beyond thrilled to leverage her professional experiences to bring the PAL vision to life.</p>
                    <a class="bt-profile"  style="z-index:100000!important" title="Courtney is an advertising account leader with 14 years of advertising and marketing experience. She has worked with some of the most popular children and family brands and products including; LEGO, Hasbro, Mattel, Nickelodeon, Kid Cuisine, DreamWorks, Crayola, Walmart Kids, and NBA Hoop Troop. Courtney holds her MBA from Columbia University and is beyond thrilled to leverage her professional experiences to bring the PAL vision to life."  href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top">View profile</a>
                  </div>
                </div>           
                <img src="img/team/team-courtney.jpg"/>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-6">
          <div class="flip">
            <div class="card">
              <div class="specialist face front">
                <div class="info">
                   <p>Nicole</p>
                  
                </div>             
                <img src="img/team/team-nicole.jpg"/>
              </div>
              <div class="face back">
                <div class="info-back">
                  <div class="info-team">
                    <p>About Nicole:</p>
                    <p>Nicole has worked professionally as a head teacher, Education Director and Manager of one of NYC's most elite preschool prep programs, specializing in Early Education for ages 3 months through 3.5 years. With a B.A. in Music and Communications from Fairfield University, her passion for music combined with her experience as an Early Childhood Educator ensures that every group care experience is creative and engaging. Nicole is eager to build a community that sets PAL apart as a thought leader in 'Social Care', pioneering a new standard for teaching professionals and parents.</p>
                    <a class="bt-profile"  style="z-index:100000!important" title="Nicole has worked professionally as a head teacher, Education Director and Manager of one of NYC's most elite preschool prep programs, specializing in Early Education for ages 3 months through 3.5 years. With a B.A. in Music and Communications from Fairfield University, her passion for music combined with her experience as an Early Childhood Educator ensures that every group care experience is creative and engaging. Nicole is eager to build a community that sets PAL apart as a thought leader in 'Social Care', pioneering a new standard for teaching professionals and parents."  href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top">View profile</a>
                  </div>
                </div>           
                <img src="img/team/team-nicole.jpg"/>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3">
          <div class="flip">
            <div class="card">
              <div class="specialist face front">
                <div class="info">
                   <p>Zoe</p>
                  
                </div>             
                <img src="img/team/team-zoe.jpg"/>
              </div>
              <div class="face back">
                <div class="info-back">
                  <div class="info-team">
                    <p>About Zoe:</p>
                    <p>Zoe Lathan, a Columbus, OH transplant, joined the Project Playdate family in 2015, working as a Support Specialist, Event Manager, After School Coordinator, then ultimately Manager of Operations. Zoe has grown with the company and refined a particular expertise in overseeing talent recruitment and development. Having worked so many 'Social Care' experiences through Project Playdate, Zoe is our champion in the cultivation of our incredible community of Specialists.</p>
                    <a class="bt-profile" style="z-index:100000!important" title="Zoe Lathan, a Columbus, OH transplant, joined the Project Playdate family in 2015, working as a Support Specialist, Event Manager, After School Coordinator, then ultimately Manager of Operations. Zoe has grown with the company and refined a particular expertise in overseeing talent recruitment and development. Having worked so many 'Social Care' experiences through Project Playdate, Zoe is our champion in the cultivation of our incredible community of Specialists."  href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top">View profile</a>
                  </div>
                </div>           
                <img src="img/team/team-zoe.jpg"/>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-12">
          <div class="titulo">
            <h2>Our Team</h2>
            <p>We are a team passionate about enriching childhoods with happy and nurturing experiences. We want to give their parents some time to breathe without feeling guilty or anxious about child care. We believe strongly in the powerful impact of social connection and play during our earliest years of life. We know that the only way to achieve safe and quality child care is by recruiting the best people and by getting them paid more.</p>
          </div>
        </div>
    </div>

<!-- How to -->
    <div class="row row-eq-height how-to become-partner-container">
        <div class="col-lg-12 vtop">
          <div class="content-title">
            <h2 class="title-in">Become a partner</h2>
            <div class="row texto become-partner">
              <div class="col-lg-3 links">
                <ul>
                  <li><a href="javascript:;" data-target="text1" class="selected" title="">Corporate Partnerships</a></li>
                  <li><a href="javascript:;" data-target="text2" title="">Playdate Partners</a></li>
                  <li><a href="javascript:;" data-target="text3" title="">Schools and Camps</a></li>
                  <li><a href="javascript:;" data-target="text4" title="">Hotels and Events Venues</a></li>
                  <li><a href="javascript:;" data-target="text5" title="">Media</a></li>
                </ul>
              </div>
              <div class="col-lg-9 descriptions">
                <div class="text" id="text1">
                  <p>Give your employees the support they seek most.</p>
                  <p><strong>Emergency back-up care. </strong>With PAL credits, your employees will have access to a wide network of on-demand safe and social drop off child care experiences.</p>
                  <p><strong>Post-parental leave. </strong>
                  Our care-shares for the under 3 give parents access to the safest and most qualified infant care providers - registered nurses. (Coming Soon)</p>
                  <!--<p>Contact <a href="mailto:pagrawal@webcubator.co">pagrawal@webcubator.co</a> to explore partnership opportunities with PAL.</p>-->
                  <p>Contact <a href="mailto:partner@playdatepal.com">partner@playdatepal.com</a> to explore partnership opportunities with PAL.</p>
                </div>
                <div class="text" id="text2">
                  <p>Are you a family-friendly attraction or early childhood facility?</p>
                  <p>Playdate partners can leverage the PAL platform to optimize downtime, showcase their best offerings and talent, and fill their drop in classes. With their own private groups on PAL, they have ongoing direct communication opportunities with a highly engaged family network.</p>
                  <p><i>We will work with you to best support the amazing work that you do! Facilitators can be your in-house staff or hired through our network.</i></p>
                  <!--<p>Contact <a href="mailto:pagrawal@webcubator.co">pagrawal@webcubator.co</a> to explore partnership opportunities with PAL.</p>-->
                  <p>Contact <a href="mailto:partner@playdatepal.com">partner@playdatepal.com</a> to explore partnership opportunities with PAL.</p>
                </div>
                <div class="text" id="text3">
                  <p>After care and holidays are always a challenge for families, but now we are here to help! Through your own private group, your families will have access to a simple and affordable solution for their after care needs. As long as you can host, our network will take care of the rest.</p>
                  <p><i>Facilitators can be your own staff or hired through our network. Our in-house team can offer additional coordination and curriculum building services.</i></p>
                  <!--<p>Contact <a href="mailto:pagrawal@webcubator.co">pagrawal@webcubator.co</a> to explore partnership opportunities with PAL.</p>-->
                  <p>Contact <a href="mailto:partner@playdatepal.com">partner@playdatepal.com</a> to explore partnership opportunities with PAL.</p>
                </div>
                <div class="text" id="text4">
                  <p>Use PAL to provide care for visiting families or guests at the next big event or wedding you host. Our facilitators will ensure that participating children will have a fun and engaging experience so that their Parents can enjoy some time off, guilt-free.</p>
                  <p><i>We will help organize an invaluable resource to your guests, simply and seamlessly. Our in-house event planning team can design any child-care experience from beginning to end, including coordinating guest registration, supplies, decor and additional entertainment.</i></p>
                  <!--<p>Contact <a href="mailto:pagrawal@webcubator.co">pagrawal@webcubator.co</a> to explore partnership opportunities with PAL.</p>-->
                  <p>Contact <a href="mailto:partner@playdatepal.com">partner@playdatepal.com</a> to explore partnership opportunities with PAL.</p>
                </div>
                <div class="text" id="text5">
                  <!--<p>For media inquiries please contact <a href="mailto:pagrawal@webcubator.co">pagrawal@webcubator.co</a>.</p>-->
                  <p>For media inquiries please contact <a href="mailto:media@playdatepal.com">media@playdatepal.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

<!-- How to -->
	<a name="contact"></a>
    <div class="row row-eq-height how-to contact-form">
        <div class="col-lg-12 vtop">
          <div class="content-title">
            
            
            <h2 class="title-in">Contact form</h2>
            <div class="row texto">
              <div class="col-lg-12">
              	
                <form id="contact-form" method="post" action="/contact-us.php" role="form" novalidate="true">
                	<input type="hidden" name="action" value="contact" />
                	<input type="hidden" name="type" value="contact" />
                          <div class="controls">
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="form-group">
                                          <label class="sr-only" for="form_name">Name</label>
                                          <input id="form_name" type="text" name="name" class="form-control" placeholder="Name" required="required" data-error="Firstname is required.">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="form-group">
                                          <label class="sr-only" for="last_name">Lastname</label>
                                          <input id="form_lastname" type="text" name="last_name" class="form-control" placeholder="Last Name" required="false" data-error="Last Name is required.">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="form-group">
                                          <label class="sr-only" for="email">Email</label>
                                          <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-12">
                                      <div class="form-group">
                                          <label class="sr-only" for="message">Message</label>
                                          <textarea style="color:white!important" id="message" name="message" class="form-control" placeholder="Message" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                      </div>
                                  </div>
                                  <div class="col-lg-12">
                                      <input type="submit" class="btn btn-send" value="Contact">
                                  </div>
                              </div>
							  <div class="g-recaptcha" data-sitekey="<?php echo SITE_KEY; ?>"></div>
                              <?php if($contactsent) {?>
                              <div class="row request-access-row">
                            	<div class="col-lg-2">&nbsp;</div>
                            	<div class="col-lg-8" id="message" style="text-align:center;color:white;background-color:#815087;padding: 40px 20px;">
	                                Your message was sent successfully. We will get in touch shortly.
                            	</div>
                            	<div class="col-lg-2">&nbsp;</div>
                        	  </div>
                              <?php } ?>
                              
                              
                          </div>

                      </form>
                      <div class="messageOK">Your message was sent successfully! We will get in touch shortly</div>
              </div>
            </div>
            
          </div>
        </div>
    </div>


    </div>

    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

	<?php if($contactsent) { ?>
		<script>
		$(document).ready(function (){
			$("html, body").animate({ scrollTop: $('#message').offset().top-300}, 1000);
		});
		</script>
	<?php } ?>

  </body>

</html>

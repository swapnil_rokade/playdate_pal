<?php 
/**
 * PLAYDATE - GET PLAYPACKS - CHECKOUT PAGE
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");
if((!isset($isParent)) || (!$isParent)) {
    header("Location: /");
}
//Parent connected. Get full info
$parent = ParentDAO::getParent($_SESSION["parent_id"]);

$backUrl = "/get-playpacks.php";

$amount = 1000; //minimo = 10$
$description = "Playdate Playpacks"; 

if(isset($_REQUEST["amount"]) && ($_REQUEST["amount"]!=null) && is_numeric($_REQUEST["amount"])) {
    $amount = $_REQUEST["amount"]*100;
}

$_SESSION['amount_charge'] = $amount;

$payingAmount = $amount;
//APPLY DISCOUNT IF THE AMOUNT IS ONE OF THIS
if($amount==12000) {
    $payingAmount = 11400;
} else if($amount==24000) {
    $payingAmount = 22800;
} else if($amount==36000) {
    $payingAmount = 34200;
} else if($amount==23000) {
    $payingAmount = 21850;
} else if($amount==46000) {
    $payingAmount = 43700;
} else if($amount==68400) {
    $payingAmount = 64980;
}


if(isset($_REQUEST["description"]) && ($_REQUEST["description"]!=null) && ($_REQUEST["description"]!="")) {
    $description = $_REQUEST["description"];
}


?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content=""> 
    <meta name="author" content="">

    <title>Checkout - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Get Playpacks - Checkout</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">




<!-- Featured playdates -->
      <div class="row featured-playdates get-playpacks">
        <div class="col-lg-3">
          <div class="titulo">
            <h2 class="budget">$<?php echo Utils::moneyFormat($parent->getCredit()) ?></h2>
            <p>Your available credit</p>
          </div>
        </div>
        <div class="col-lg-9">
        <form action="charge.php" method="post">
          <div class="row intro-playpacks  row-eq-height">
            <div class="col-lg-4 vcenter">
              <p>You are buying the following credit in Playdates.</p>
            </div>
              <div class="col-lg-4 vcenter">
                <input type="number" name="amount" placeholder="150" id="playpacks" value="<?php echo Utils::moneyFormatField($amount) ?>" disabled="disabled" style="color:#815087!important;-webkit-text-fill-color: rgba(129, 80, 135, 1);-webkit-opacity: 1;">
              </div>
              <div class="col-lg-4 vcenter">
                
  					<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
          				data-key="<?php echo $STRIPE_PK ?>"
          				data-description="<?php echo $description ?>"
          				data-amount="<?php echo $payingAmount ?>"
          				data-locale="auto"></script>

              </div>
          </div>
		</form>        
        <div class="row get-playpacks-info">

        <div class="col-lg-12">

          <button class="btn btn-md btn-primary btn-block" type="submit">Purchase history</button>

          <div class="terms">Credits are non-refundable. Playpacks based on signature 3-hour playdates , not including "Just In Time" rates or optional add-on fees (i.e. travel, admissions and additional materials). Visit  <a href="terms-of-service.php" title="Terms and Conditions">Terms and Conditions</a> for more information.</div>
          
        </div>
      </div>
      </div>
    </div>

      



    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

<?php
/**
 * PLAYDATE - STRIPE RETURN URI
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isSpecialist) {
    header("Location: /");
}

if (isset($_REQUEST['code'])) { // Redirect w/ code
    $stripeCode = $_REQUEST['code'];
    //Go to validate code
    header("Location: /stripe-validate-code.php?code=".$stripeCode);
    
} else { 
    //No code - go to dashboard
    header("Location: /specialist-dashboard.php#playpacks");
}
?>
<?php 
/**
 * PLAYDATE - GROUP DASHBOARD SECTION
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$group = null;
if(isset($_REQUEST["id_group"]) && ($_REQUEST["id_group"]!=null)) {
    $group = GroupDAO::getGroup($_REQUEST["id_group"]);
}

//Test it is logged
if(!$isConnected || ($group==null)) {
    header("Location: /groups.php");
}

//Test parent or specialist is in group and if is admin
$ingroup = false; 
$isgroupadmin = false;  

$sessionName = "";  

$parent = null;
$specialist=null;

if($isParent) {
    $parent = ParentDAO::getParent($_SESSION["parent_id"]);
    $ingroup = GroupDAO::isParentInGroup($_SESSION["parent_id"], $group->getId_group());
    $isgroupadmin = GroupDAO::isParentAdminInGroup($_SESSION["parent_id"], $group->getId_group());
    $sessionparent = ParentDAO::getParent($_SESSION["parent_id"]);
    $sessionName = $sessionparent->getFullName();
} else if($isSpecialist) {
    $specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
    $ingroup = GroupDAO::isSpecialistInGroup($_SESSION["spec_id"], $group->getId_group());
    $isgroupadmin = GroupDAO::isSpecialistAdminInGroup($_SESSION["spec_id"], $group->getId_group());
    $sessionspecialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
    $sessionName = $sessionspecialist->getFullName();
}
//Test ingroup
if(!$ingroup) {
    header("Location: /groups.php");
}

$groupMembers = 0;
$groupMembers += GroupDAO::countGroupParents($group->getId_group());
$groupMembers += GroupDAO::countGroupSpecialists($group->getId_group());

$invitationSentResult = null;
$invitationSpecialistSentResult = null;

//Selected Playdate to show in detail
$selectedPastPlaydate = null;
$selectedPlaydate = null;
$paramPlaydate = new Playdate();
$paramPlaydate->readFromRow($_REQUEST);
if($paramPlaydate->getId_playdate()>0) {
    $paramPlaydate = PlaydateDAO::getPlaydate($paramPlaydate->getId_playdate());
    
    if(($paramPlaydate!=null) && ($paramPlaydate->getId_playdate()>0)) {
        $selectedPlaydate = $paramPlaydate;
    }
}

if(isset($_REQUEST["id_past_playdate"]) && ($_REQUEST["id_past_playdate"]!=null)) {
    $paramPastPlaydate = PlaydateDAO::getPlaydate($_REQUEST["id_past_playdate"]);
    if(($paramPastPlaydate!=null) && ($paramPastPlaydate->getId_playdate()>0)) {
        $selectedPastPlaydate = $paramPastPlaydate;
    }
}

$invited = false; //Invite friend to selected playdate

if(isset($_REQUEST["action"])) {
    
    switch ($_REQUEST["action"]) {
        
        case "createupdateselected": {
            $comment = new Comment();
            $comment->readFromRow($_REQUEST);
            $comment->setId_playdate($selectedPlaydate->getId_playdate());
            $comment->setId_parent($parent->getId_parent());
            
            CommentDAO::createComment($comment);
            
            //Mail to playdate lead
            $pdTime =  Utils::dateFormat($selectedPlaydate->getDate())." ".Utils::get12hourFormat($selectedPlaydate->getTime_init())." - ".Utils::get12hourFormat($selectedPlaydate->getTime_end());
            $leadName = "No Lead";
            $leadMail = null;
            if($selectedPlaydate->getId_specialist()!=null) {
                $pdSpecialist = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                if($pdSpecialist!=null) {
                    $leadName = $pdSpecialist->getFullName();
                    $leadMail = $pdSpecialist->getEmail();
                }
            }
            
            if($leadMail!=null) {
                MailUtils::sendPlaydateParentUpdate($leadMail, $leadName, $selectedPlaydate->getName(), $pdTime, $comment->getComment(), $leadName, "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
            }
            
            break;
        }
        
        case "createupdatepast": {
            $comment = new Comment();
            $comment->readFromRow($_REQUEST);
            $comment->setId_playdate($selectedPastPlaydate->getId_playdate());
            $comment->setId_parent($parent->getId_parent());
            
            CommentDAO::createComment($comment);
            break;
        }
        
        case "invite-friends": {
            if(!empty($_POST['inviteFriendsPopup']) && ($selectedPlaydate!=null)) {
                $name="Someone";
                if($parent!=null) {
                    $name = $parent->getName();
                } else if($specialist!=null) {
                    $name = $specialist->getName();
                }
                
                $event_place = $selectedPlaydate->getLoc_address().", ".$selectedPlaydate->getLoc_city().", ".$selectedPlaydate->getLoc_state();
                
                foreach($_POST['inviteFriendsPopup'] as $auxfriend) {
                    $friend = ParentDAO::getParentByUsernameOrEmail($auxfriend);
                    if($friend!=null) {
                        MailUtils::sendPlaydateInviteFriend($friend->getEmail(), $auxfriend, $name, $selectedPlaydate->getName(), Utils::dateFormat($selectedPlaydate->getDate()), $event_place, Utils::get12hourFormat($selectedPlaydate->getTime_init()),Utils::get12hourFormat($selectedPlaydate->getTime_end()), "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
                    } else if(filter_var($auxfriend, FILTER_VALIDATE_EMAIL)) {
                        MailUtils::sendPlaydateInviteFriend($auxfriend, $auxfriend, $name, $selectedPlaydate->getName(), Utils::dateFormat($selectedPlaydate->getDate()), $event_place, Utils::get12hourFormat($selectedPlaydate->getTime_init()), Utils::get12hourFormat($selectedPlaydate->getTime_end()), "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
                    }
                    
                }
                $invited = true;
            }
            
            
            break;
        }
        
        case "upload-media": {
            //If file, upload
            if(count($_FILES)>0) {
                $currentDir = getcwd();
                $uploadDirectory = "/uploads/group-media/".$group->getId_group()."/";
                if(!file_exists($currentDir.$uploadDirectory)) {
                    mkdir($currentDir.$uploadDirectory, 0755, true);
                }
                
                $errors = []; // Store all foreseen and unforseen errors here
                
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                
                $fileName = $_FILES['formfile']['name'];
                $fileSize = $_FILES['formfile']['size'];
                $fileTmpName  = $_FILES['formfile']['tmp_name'];
                $fileType = $_FILES['formfile']['type'];
                
                $tmpArr = explode('.',$fileName);
                $tmpExt = end($tmpArr);
                $fileExtension = strtolower($tmpExt);
                
                $uploadPath = $currentDir . $uploadDirectory . basename($fileName);
                
                $i=0;
                while (file_exists($uploadPath)) {
                    $i++;
                    $uploadPath = $currentDir . $uploadDirectory .$i."_".basename($fileName);
                }
                
                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }
                
                if ($fileSize > 24000000) {
                    $errors[] = "This file is more than 24MB. Sorry, it has to be less than or equal to 24MB";
                }
                
                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                    
                    if ($didUpload) {
                        $media = new GroupMedia();
                        $media->readFromRow($_REQUEST);
                        $media->setId_group($group->getId_group());
                        if($parent!=null) {
                            $media->setId_parent($parent->getId_parent());
                        }
                        if($specialist!=null) {
                            $media->setId_specialist($specialist->getId_specialist());
                        }
                        $media->setPicture($uploadDirectory.basename($fileName));
                        
                        GroupDAO::createMedia($media);
                        
                    } else {
                        echo "<!-- An error occurred somewhere. Try again or contact the admin -->";
                    }
                } else {
                    foreach ($errors as $error) {
                        //echo $error . "These are the errors" . "\n";
                    }
                }
            } //there is file
            
            
            break;
        }
        
        case "changeprivacity": {
            $newprivacity = Group::$PRIVACITY_PRIVATE; //default, private = 1
            if(isset($_REQUEST["newprivacity"]) && ($_REQUEST["newprivacity"]!=null) && ($_REQUEST["newprivacity"]==Group::$PRIVACITY_PUBLIC)) {
                $newprivacity = Group::$PRIVACITY_PUBLIC;
            }
                
            GroupDAO::updateGroupPrivacity($group->getId_group(), $newprivacity);
            $group = GroupDAO::getGroup($group->getId_group());
            break;
        }
        
        case "inviteparent": {
            $parentinfo = null;
            if(isset($_REQUEST["parentinfo"]) && ($_REQUEST["parentinfo"]!=null) && ($_REQUEST["parentinfo"]!="")) {
                $parentinfo = $_REQUEST["parentinfo"];
                $parentToInvite = ParentDAO::getParentByUsernameOrEmail($parentinfo);
                if(($parentToInvite!=null) && ($parentToInvite->getId_parent()>0)) {
                    //Create invitation
                    if(GroupDAO::isParentInGroup($parentToInvite->getId_parent(), $group->getId_group())) {
                        $invitationSentResult = "This parent is already in the group!";
                    } else {
                    
                        GroupDAO::createGroupParentInvitation($group->getId_group(), $parentToInvite->getId_parent());
                    
                        // Send mail invitation
                        MailUtils::sendGroupInvite($parentToInvite->getEmail(), $parentToInvite->getName(), $sessionName, $group->getName());
                    
                        $invitationSentResult = "Your invitation has been sent!";
                    }
                    
                } else if(filter_var($parentinfo, FILTER_VALIDATE_EMAIL)) {
                    
                    //Send mail invitation
                    MailUtils::sendGroupInvite($parentinfo, $parentinfo, $sessionName, $group->getName());
                    
                    $invitationSentResult = "Your invitation has been sent!";
                    
                } else {
                    $invitationSentResult = "The parent username or email you sent doesn't exist!";
                }
                
            }
            break;
        }
        
        case "invitespecialist": {
            $specialistinfo = null;
            if(isset($_REQUEST["specialistinfo"]) && ($_REQUEST["specialistinfo"]!=null) && ($_REQUEST["specialistinfo"]!="")) {
                $specialistinfo = $_REQUEST["specialistinfo"];
                $specialistToInvite = SpecialistDAO::getSpecialistByUsernameOrEmail($specialistinfo);
                if(($specialistToInvite!=null) && ($specialistToInvite->getId_specialist()>0)) {
                    
                    if(GroupDAO::isSpecialistInGroup($specialistToInvite->getId_specialist(), $group->getId_group())) {
                        //If specialist is already in the group, don't send notification - don't create invitation
                        $invitationSpecialistSentResult = "This specialist is already in the group!";
                    } else {
                        //Create group invitation
                        GroupDAO::createGroupSpecialistInvitation($group->getId_group(), $specialistToInvite->getId_specialist());
                        
                        //Send mail invitation to specialist
                        MailUtils::sendGroupInvite($specialistToInvite->getEmail(), $specialistToInvite->getName(), $sessionName, $group->getName());
                        
                        $invitationSpecialistSentResult = "Your invitation has been sent!";
                    }
                    
                } else {
                    $invitationSpecialistSentResult = "The specialist username or email you sent doesn't exist!";
                    
                }
                
            }
            break;
        }
        
        case "accept-parent": {
            $request = new GroupRequest();
            $request->readFromRow($_REQUEST);
            
            if($request->getId_request()>0) {
                
                GroupDAO::acceptParentRequest($request->getId_request());
                $request = GroupDAO::getGroupParentRequest($request->getId_request());
                
                GroupDAO::parentJoinGroup($request->getId_group(), $request->getId_parent(), false);
                
                $parentGrp = ParentDAO::getParent($request->getId_parent());
                
                MailUtils::sendGroupAccept($parentGrp->getEmail(), $parentGrp->getName(), $group->getName(), $request->getId_group());
            }
            break;
        }
        
        case "decline-parent": {
            $request = new GroupRequest();
            $request->readFromRow($_REQUEST);
            if($request->getId_request()>0) {
                GroupDAO::declineParentRequest($request->getId_request());
                $request = GroupDAO::getGroupParentRequest($request->getId_request());
                $parentGrp = ParentDAO::getParent($request->getId_parent());
                
                MailUtils::sendGroupDenied($parentGrp->getEmail(), $parentGrp->getName(), $group->getName());
            }
            break;
        }
        
        case "accept-specialist": {
            $request = new GroupRequest();
            $request->readFromRow($_REQUEST);
            if($request->getId_request()>0) {
                GroupDAO::acceptSpecialistRequest($request->getId_request());
                $request = GroupDAO::getGroupSpecialistRequest($request->getId_request());
                GroupDAO::specialistJoinGroup($request->getId_group(), $request->getId_specialist(), false);
                $specialistGrp = SpecialistDAO::getSpecialist($request->getId_specialist());
                
                MailUtils::sendGroupAccept($specialistGrp->getEmail(), $specialistGrp->getName(), $group->getName(), $request->getId_group());
            }
            break;
        }
        
        case "decline-specialist": {
            $request = new GroupRequest();
            $request->readFromRow($_REQUEST);
            if($request->getId_request()>0) {
                GroupDAO::declineSpecialistRequest($request->getId_request());
                
                $request = GroupDAO::getGroupSpecialistRequest($request->getId_request());
                $specialistGrp = SpecialistDAO::getSpecialist($request->getId_specialist());
                
                MailUtils::sendGroupDenied($specialistGrp->getEmail(), $specialistGrp->getName(), $group->getName());
            }
            break;
        }
        
        case "updateprofile": {
            $editGroup = $group;
            $editGroup->readFromRow($_REQUEST);
            
            if($isgroupadmin) {
                
                //If file, upload
                if(count($_FILES)>0) {
                    $currentDir = getcwd();
                    $uploadDirectory = "/uploads/";
                    
                    $errors = []; // Store all foreseen and unforseen errors here
                    
                    $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                    
                    $fileName = $_FILES['formfile']['name'];
                    $fileSize = $_FILES['formfile']['size'];
                    $fileTmpName  = $_FILES['formfile']['tmp_name'];
                    $fileType = $_FILES['formfile']['type'];
                    
                    $tmpArr = explode('.',$fileName);
                    $tmpExt = end($tmpArr);
                    $fileExtension = strtolower($tmpExt);
                    
                    $uploadPath = $currentDir . $uploadDirectory . basename($fileName);
                    
                    $i=0;
                    while (file_exists($uploadPath)) {
                        $i++;
                        $uploadPath = $currentDir . $uploadDirectory .$i."_".basename($fileName);
                    }
                    
                    if (! in_array($fileExtension,$fileExtensions)) {
                        $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                    }
                    
                    if ($fileSize > 24000000) {
                        $errors[] = "This file is more than 24MB. Sorry, it has to be less than or equal to 24MB";
                    }
                    
                    if (empty($errors)) {
                        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                        
                        if ($didUpload) {
                            $editGroup->setPicture($uploadDirectory.basename($fileName)); //Actualizamos la ruta
                        } else {
                            echo "<!-- An error occurred somewhere. Try again or contact the admin -->";
                        }
                    } else {
                        foreach ($errors as $error) {
                            //echo $error . "These are the errors" . "\n";
                        }
                    }
                } //there is file
                
                //Insert locations
                NeighborhoodDAO::deleteAllGroupNeighborhood($editGroup->getId_group());
                if(!empty($_POST['id_neighborhood'])) {
                    foreach($_POST['id_neighborhood'] as $auxlocation) {
                        NeighborhoodDAO::createGroupNeighborhood($editGroup->getId_group(), $auxlocation);
                    }
                }
                
                GroupDAO::updateGroup($editGroup);
            }
            header("Location: /groups-dashboard.php?id_group=".$editGroup->getId_group()."#profile");
            break;
        }
    }
}


?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $group->getName() ?> - Group Dashboard - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>


    <!-- UPLOAD MEDIA MODAL -->

    <div class="modal fade" id="last-playdate" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-last-playdate" action="/groups-dashboard.php?id_group=<?php echo $group->getId_group() ?>#media" method="post" enctype="multipart/form-data">
              <input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
              <input type="hidden" name="action" value="upload-media" />
              <h2 class="form-last-playdate-heading">Upload media</h2>

              <div class="row">
                  <div class="col-lg-4 cont-img-profile">
                    <img src="/img/no-image.jpg" alt="" class="mediaUpload">
                    <label class="custom-file">
                      <input type="hidden" name="picture" value="/img/no-image.jpg">
                      <input type="file" id="mediaUpload" class="custom-file-input" name="formfile"   onchange="previewFileMedia()"/>
                      <span class="custom-file-control"></span>
                    </label>
                  </div>
                  <div class="col-lg-8 your-personal-info">    
                     <label for="mediaDescription" class="sr-only">Description</label>
                     <textarea class="form-control" placeholder="Description" id="mediaDescription" rows="7" name="description"></textarea>
                  </div>
                </div>

              <button class="btn btn-md btn-primary" type="submit">Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!--  FIN UPLOAD MEDIA MODAL -->


    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1><?php echo $group->getName() ?></h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <?php 
        $picGroup = $group->getPicture();
        if(($picGroup==null) || ($picGroup=="")) {
            $picGroup = "img/default-img-group-profile.jpg";
        }
        ?>
        <div class="col-lg-3 sidebar">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $picGroup ?>" alt="">
                </div>
                <div class="info">
                  <span class="title"><?php echo $group->getName() ?></span>
                  <span class="subtitle"><?php echo $group->getZipcode() ?></span>
                  <span class="subtitle members"><?php echo $groupMembers?> members</span>
                </div>
                 <div class="data">
                  <p><?php echo $group->getDescription() ?></p>
                </div>
              </div>
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-current-tab" data-toggle="tab" href="#nav-current" role="tab" aria-controls="nav-current" aria-selected="true">Current Playdates</a>
              <a class="nav-item nav-link" id="nav-past-tab" data-toggle="tab" href="#nav-past" role="tab" aria-controls="nav-past" aria-selected="false">Past Playdates</a>
              <a class="nav-item nav-link" id="nav-members-tab" data-toggle="tab" href="#nav-members" role="tab" aria-controls="nav-members" aria-selected="false">Members</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Group Profile</a>
              <a class="nav-item nav-link" id="nav-specialists-tab" data-toggle="tab" href="#nav-specialists" role="tab" aria-controls="nav-specialists" aria-selected="false">Specialists</a>
              <a class="nav-item nav-link" id="nav-media-tab" data-toggle="tab" href="#nav-media" role="tab" aria-controls="nav-media" aria-selected="false">Media</a>
            </div>
          </nav>

          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-current" role="tabpanel" aria-labelledby="nav-current-tab">
              <div class="row row-tabs">
              
              <?php $upcomingPlaydates = PlaydateDAO::getUpcomingPlaydatesListByGroup($group->getId_group(), 1, 8, null, null); 
              
              foreach ($upcomingPlaydates as $auxPlaydate) {
                  //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
                  if(($auxPlaydate->getId_request()!=null) && is_numeric($auxPlaydate->getId_request())) {
                      $minAgePd = 100;
                      $maxAgePd = -1;
                      $reservations = PlaydateDAO::getChildrenReservations($auxPlaydate->getId_playdate());
                      foreach ($reservations as $auxReserv) {
                          if($auxReserv->getAge()>$maxAgePd) {
                              $maxAgePd = $auxReserv->getAge();
                          }
                          
                          if($auxReserv->getAge()<$minAgePd) {
                              $minAgePd = $auxReserv->getAge();
                          }
                      }
                      
                      
                      if(($maxAgePd>=0) && ($minAgePd<100)) {
                          $auxPlaydate->setAge_init($minAgePd);
                          $auxPlaydate->setAge_end($maxAgePd);
                      }
                      
                  }
                  
                  
                  //TODO - De momento elegimos el primero para mostrar el detalle posterior
                  $link = "/groups-dashboard.php?id_group=".$group->getId_group()."&id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
                  
                  $interestName = "";
                  $interestType = "";
                  
                  $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
                  $auxInterest = null;
                  if(count($interests)>0) {
                      $auxInterest = $interests[0];
                      $interestName =$auxInterest->getName();
                      
                      switch ($auxInterest->getType()) {
                          case Interest::$TYPE_TRAINING: {
                              $interestType=" stem";
                              break;
                          }
                          case Interest::$TYPE_ART: {
                              $interestType=" creative";
                              break;
                          }
                          case Interest::$TYPE_PLAY: {
                              $interestType=" outdoor";
                              break;
                          }
                          case Interest::$TYPE_CULTURAL: {
                              $interestType=" attractions";
                              break;
                          }
                      }
                  }
                  
                  $openSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
                  $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
                  
                  $selectedBox = (($selectedPlaydate!=null) && ($selectedPlaydate->getId_playdate()==$auxPlaydate->getId_playdate()));
                  ?>

                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?><?php echo($selectedBox?" selected-box":"")?>">
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="info">
                      <span class="title"><a href="<?php echo $link ?>"><?php echo ($auxPlaydate->getName()) ?></a></span>
                      <span class="subtitle"><?php echo($auxPlaydate->getLoc_state()) ?></span>
                    </div>
                     <div class="data">
                      <div  class="age"><span><?php echo $auxPlaydate->getAge_init() ?>-<?php echo $auxPlaydate->getAge_end() ?> years old</span></div>
                      <div class="participants"><span><?php echo $auxPlaydate->getNum_children() ?> kids / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                      <?php if($specialist!=null) { ?>
                  		<div class="specialists">Specialist <span><a href="/specialists-public-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                  	   <?php } ?>
                    </div>
                  </div>
                </div>
				<?php } ?>
                <?php 
                if(($upcomingPlaydates==null) || (count($upcomingPlaydates)<=0)) {
                ?>
                  <div class="col-lg-12">
                    <div class="no-results"><img src="img/cactus.png"/>
                      <p>You have no upcoming playdates reserved. </p>
                      <p>Book a playdate today because life is always better with friends!</p>
                    </div>
                  </div>
                <?php   
                }
                ?>
              </div>
              
              <?php if($selectedPlaydate!=null) {
              
                  $interestName = "";
                  $interestType = "";
                  
                  $interests = InterestDAO::getInterestsByPlaydate($selectedPlaydate->getId_playdate());
                  $auxInterest = null;
                  if(count($interests)>0) {
                      $auxInterest = $interests[0];
                      $interestName =$auxInterest->getName();
                      
                      switch ($auxInterest->getType()) {
                          case Interest::$TYPE_TRAINING: {
                              $interestType=" stem";
                              break;
                          }
                          case Interest::$TYPE_ART: {
                              $interestType=" creative";
                              break;
                          }
                          case Interest::$TYPE_PLAY: {
                              $interestType=" outdoor";
                              break;
                          }
                          case Interest::$TYPE_CULTURAL: {
                              $interestType=" attractions";
                              break;
                          }
                      }
                  }
                  
                  $openSpots = $selectedPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($selectedPlaydate->getId_playdate());
                  $specialist = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                  
              ?>
              
              <div id="selected-playdate" class="row row-current-dashboard row-eq-height">
                <div class="col-lg-4 vcenter" >
                  <div class="box-playdates <?php echo $interestType ?> no-text">
                    <div class="img-container">
                      <a href="/playdate.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>"><img src="<?php echo $selectedPlaydate->getPicture() ?>" alt="" /></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                  <div class="info-current-dashboard">
                    <div class="info">
                      <span class="title"><a href="/playdate.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>"><?php echo $selectedPlaydate->getName() ?></a></span>
                      <span class="subtitle"><?php echo $selectedPlaydate->getLoc_city() ?></span>
                    </div>
                     <div class="data">
                      <p  class="place">
                        <span class="address"><?php echo $selectedPlaydate->getLoc_address() ?></span>
                        <span class="city"><?php echo $selectedPlaydate->getLoc_city() ?>, <?php echo $selectedPlaydate->getLoc_state() ?></span>
                      </p>
                    </div>
                    <div class="data">
                      <span class="date"><?php echo(Utils::dateFormat($selectedPlaydate->getDate())) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($selectedPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($selectedPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="data">
                      <div class="participants"><span><?php echo $selectedPlaydate->getNum_children() ?> / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                <form class="form-invite" action="/groups-dashboard.php?id_group=<?php echo $group->getId_group() ?>&id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>" method="post">
                  <input type="hidden" name="action" value="invite-friends" />
                  <input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
                  <input type="hidden" name="id_playdate" value="<?php echo $selectedPlaydate->getId_playdate() ?>" />
                  <label for="inputEmail" class="sr-only">Username or Email</label>
                  <input type="text" id="inputEmail" class="form-control" placeholder="Username or Email" name="inviteFriendsPopup[]" required="required" >
                  <?php if($invited) { ?>
                  	<p style="color:#838383;width:100%;text-align:center">Invitation has been sent</p>
                  <?php } ?>
                  <button class="btn btn-md btn-primary btn-block" type="submit">Invite</button>
                </form>
                </div>
              </div>

              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Reserved By</p>
                  <div class="container-specialist-tags row">
                  	<?php $parents = ParentDAO::getParentsListByPlaydate($selectedPlaydate->getId_playdate(), 1, 10, null, null) ?>
                  	<?php foreach ($parents as $auxParent) {
                  	     $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPlaydate->getId_playdate());
                  	?>
                    <div class="specialist-tag">
                      <img src="<?php echo (($auxParent->getPicture()==null)?"/img/img-profile.jpg":$auxParent->getPicture()) ?>">
                      <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getName() ?><span><?php echo $reservation->getNum_spots()?> spots</span></a>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
				
			  <?php /* 
              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Invited Friends</p>
                  <div class="row">
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">jonh@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">tom@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">chris@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">gabrielle@gmail.com</a>
                    </div>
                  </div>
                </div>
              </div>
              */ ?>

               <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Specialists</p>
                  <div class="container-specialist-tags row">
                      <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $specialist->getPicture()?>">
                        <span><?php echo $specialist->getFullName() ?> | Lead</span>
                      </div></a>
                  </div>
                </div>
              </div>

              <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Comment(s)</p>
                </div>
                <?php $updates = CommentDAO::getCommentListByPlaydate($selectedPlaydate->getId_playdate()); ?>
                <?php foreach($updates as $auxUpdate) {  
                    $username = "Unknown";
                    if(($auxUpdate->getId_parent()!=null) && is_numeric($auxUpdate->getId_parent())) {
                        $auxParent = ParentDAO::getParent($auxUpdate->getId_parent());
                        $username = $auxParent->getName();
                        $pic = $auxParent->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    } else if(($auxUpdate->getId_specialist()!=null) && is_numeric($auxUpdate->getId_specialist())) {
                    
                        $auxSpecialist = SpecialistDAO::getSpecialist($auxUpdate->getId_specialist());
                        $username = $auxSpecialist->getName();
                        $pic = $auxSpecialist->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    }
                        
                ?>
                <div class="row info-updates">
                    <div class="col-lg-2 date">
                      <img src="<?php echo $pic ?>">
                      <span><strong><?php echo $username ?></strong></span>
                    </div>
                    <div class="col-lg-10 texto">
                      <p class="day"><strong><?php echo Utils::dateFormat($auxUpdate->getIdate())?></strong></p>
                      <p style="color:#878787"><?php echo $auxUpdate->getComment() ?></p>
                    </div>
                </div>
                
                <?php  
                    }
                ?>
              </div>
			  
              
              <div class="row row-tabs">
                <div class="col-lg-12">
                <form class="form-comment" id="form-profile" method="post" enctype="multipart/form-data" action="/groups-dashboard.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>">
                    <input type="hidden" name="action" value="createupdateselected" />
                    <input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
                    <input type="hidden" name="id_playdate" value="<?php echo $selectedPlaydate->getId_playdate() ?>" />    
                    <p class="titulo uppercase">New comment</p>
                    <div class="row">
                      <div class="col-lg-12">                    
                         <label for="comment" class="sr-only">New comment</label>
                         <textarea class="form-control" placeholder="Comment here to notify your Lead Specialist of any updates or last minute needs. Your comment will be visible to every participant in this playdate." id="comment" rows="3" name="comment"></textarea>
                      </div>
                    </div>
                    <div class="row row-tabs bt-send">
                  		<div class="col-lg-12">
                     		<button class="btn btn-md btn-primary bt-save-profile" type="submit">Upload Comment</button>
                  		</div>
                	</div>
                </form>
                                  
                </div>
              </div>
              
              
              <?php } ?>
              
              
            </div>
            <div class="tab-pane fade rate-playdate" id="nav-past" role="tabpanel" aria-labelledby="nav-past-tab">
              <div class="row row-tabs">
              
				<?php $pastPlaydates = PlaydateDAO::getPastPlaydatesListByGroup($group->getId_group(), 1, 8, null, null); 
              
				foreach ($pastPlaydates as $auxPlaydate) {
				    
				    //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
				    if(($auxPlaydate->getId_request()!=null) && is_numeric($auxPlaydate->getId_request())) {
				        $minAgePd = 100;
				        $maxAgePd = -1;
				        $reservations = PlaydateDAO::getChildrenReservations($auxPlaydate->getId_playdate());
				        foreach ($reservations as $auxReserv) {
				            if($auxReserv->getAge()>$maxAgePd) {
				                $maxAgePd = $auxReserv->getAge();
				            }
				            
				            if($auxReserv->getAge()<$minAgePd) {
				                $minAgePd = $auxReserv->getAge();
				            }
				        }
				        
				        
				        if(($maxAgePd>=0) && ($minAgePd<100)) {
				            $auxPlaydate->setAge_init($minAgePd);
				            $auxPlaydate->setAge_end($maxAgePd);
				        }
				        
				    }
				    
				    //TODO - De momento elegimos el primero para mostrar el detalle posterior
				    $link = "/groups-dashboard.php?id_group=".$group->getId_group()."&id_past_playdate=".$auxPlaydate->getId_playdate()."#past";
				    
				    $interestName = "";
				    $interestType = "";
				    
				    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
				    $auxInterest = null;
				    if(count($interests)>0) {
				        $auxInterest = $interests[0];
				        $interestName =$auxInterest->getName();
				        
				        switch ($auxInterest->getType()) {
				            case Interest::$TYPE_TRAINING: {
				                $interestType=" stem";
				                break;
				            }
				            case Interest::$TYPE_ART: {
				                $interestType=" creative";
				                break;
				            }
				            case Interest::$TYPE_PLAY: {
				                $interestType=" outdoor";
				                break;
				            }
				            case Interest::$TYPE_CULTURAL: {
				                $interestType=" attractions";
				                break;
				            }
				        }
				    }
				    
				    $openSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
				    $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
				    
				    $selectedBox = (($selectedPastPlaydate!=null) && ($selectedPastPlaydate->getId_playdate()==$auxPlaydate->getId_playdate()));
				    
				    ?>
              
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?><?php echo($selectedBox?" selected-box":"")?>">
                  <?php /*?>
                    <div class="img-container">
                      <a href="<?php echo $link ?>"><img src="<?php echo $auxPlaydate->getPicture() ?>" alt=""></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                    */?>
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate())?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init())?>-<?php echo Utils::get12hourFormat($auxPlaydate->getTime_end())?></span>
                    </div>
                    <div class="info">
                      <span class="title"><a href="<?php echo $link ?>"><?php echo $auxPlaydate->getName()?></a></span>
                      <span class="subtitle"><?php echo $auxPlaydate->getLoc_city()?>, <?php echo $auxPlaydate->getLoc_state()?></span>
                    </div>
                    <div class="data">
                      <div class="specialists">Specialist: <span><a href="/specialists-public-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                      <div class="specialists"><a class="bt-rate"  data-toggle="modal" data-target="#last-playdate" href="javascript:;" title="">Rate Specialist</a></div>
                    </div>
                   
                    <div class="reserve">
                      <a href="#"  data-toggle="modal" data-target="#last-playdate" class="feedback bt-reserve">Leave feedback</a>
                    </div>
                  </div>
                </div>
				<?php } ?>
				<?php 
                if(($pastPlaydates==null) || (count($pastPlaydates)<=0)) {
                ?>
                  <div class="col-lg-12">
                    <div class="no-results"><img src="img/cactus.png"/>
                      <p>You have no past playdates reserved. </p>
                      <p>Book a playdate today because life is always better with friends!</p>
                    </div>
                  </div>
                <?php   
                }
                ?>

              </div>
              
              <?php if($selectedPastPlaydate!=null) {
              
                  $interestName = "";
                  $interestType = "";
                  
                  $interests = InterestDAO::getInterestsByPlaydate($selectedPastPlaydate->getId_playdate());
                  $auxInterest = null;
                  if(count($interests)>0) {
                      $auxInterest = $interests[0];
                      $interestName =$auxInterest->getName();
                      
                      switch ($auxInterest->getType()) {
                          case Interest::$TYPE_TRAINING: {
                              $interestType=" stem";
                              break;
                          }
                          case Interest::$TYPE_ART: {
                              $interestType=" creative";
                              break;
                          }
                          case Interest::$TYPE_PLAY: {
                              $interestType=" outdoor";
                              break;
                          }
                          case Interest::$TYPE_CULTURAL: {
                              $interestType=" attractions";
                              break;
                          }
                      }
                  }
                  
                  $openSpots = $selectedPastPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($selectedPastPlaydate->getId_playdate());
                  $specialist = SpecialistDAO::getSpecialist($selectedPastPlaydate->getId_specialist());
                  
              ?>
              
              <div id="selected-past-playdate" class="row row-current-dashboard row-eq-height">
                <div class="col-lg-4 vcenter" >
                  <div class="box-playdates <?php echo $interestType ?> no-text">
                    <div class="img-container">
                      <a href="/playdate.php?id_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>"><img src="<?php echo $selectedPastPlaydate->getPicture() ?>" alt="" /></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                  <div class="info-current-dashboard">
                    <div class="info">
                      <span class="title"><a href="/playdate.php?id_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>"><?php echo $selectedPastPlaydate->getName() ?></a></span>
                      <span class="subtitle"><?php echo $selectedPastPlaydate->getLoc_city() ?></span>
                    </div>
                     <div class="data">
                      <p  class="place">
                        <span class="address"><?php echo $selectedPastPlaydate->getLoc_address() ?></span>
                        <span class="city"><?php echo $selectedPastPlaydate->getLoc_city() ?>, <?php echo $selectedPastPlaydate->getLoc_state() ?></span>
                      </p>
                    </div>
                    <div class="data">
                      <span class="date"><?php echo(Utils::dateFormat($selectedPastPlaydate->getDate())) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="data">
                      <div class="participants"><span><?php echo $selectedPastPlaydate->getNum_children() ?> / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Reserved By</p>
                  <div class="container-specialist-tags row">
                  	<?php $parents = ParentDAO::getParentsListByPlaydate($selectedPastPlaydate->getId_playdate(), 1, 10, null, null) ?>
                  	<?php foreach ($parents as $auxParent) {
                  	    $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPastPlaydate->getId_playdate());
                  	?>
                    <div class="specialist-tag">
                      <img src="<?php echo (($auxParent->getPicture()==null)?"/img/img-profile.jpg":$auxParent->getPicture()) ?>">
                      <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getName() ?><span><?php echo $reservation->getNum_spots()?> spots</span></a>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
				
			 <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Specialists</p>
                  <div class="container-specialist-tags row">
                      <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $specialist->getPicture()?>">
                        <span><?php echo $specialist->getFullName() ?> | Lead</span>
                      </div></a>
                  </div>
                </div>
              </div>

              
<div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Comment(s)</p>
                </div>
                <?php $updates = CommentDAO::getCommentListByPlaydate($selectedPastPlaydate->getId_playdate()); ?>
                <?php foreach($updates as $auxUpdate) {  
                    $username = "Unknown";
                    if(($auxUpdate->getId_parent()!=null) && is_numeric($auxUpdate->getId_parent())) {
                        $auxParent = ParentDAO::getParent($auxUpdate->getId_parent());
                        $username = $auxParent->getName();
                        $pic = $auxParent->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    } else if(($auxUpdate->getId_specialist()!=null) && is_numeric($auxUpdate->getId_specialist())) {
                    
                        $auxSpecialist = SpecialistDAO::getSpecialist($auxUpdate->getId_specialist());
                        $username = $auxSpecialist->getUsername();
                        $pic = $auxSpecialist->getName();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    }
                        
                ?>
                <div class="row info-updates">
                    <div class="col-lg-2 date">
                      <img src="<?php echo $pic ?>">
                      <span><strong><?php echo $username ?></strong></span>
                    </div>
                    <div class="col-lg-10 texto">
                      <p class="day"><strong><?php echo Utils::dateFormat($auxUpdate->getIdate())?></strong></p>
                      <p style="color:#878787"><?php echo $auxUpdate->getComment() ?></p>
                    </div>
                </div>
                
                <?php  
                    }
                ?>
              </div>
              
              <div class="row row-tabs">
                <div class="col-lg-12">
                <form class="form-comment" id="form-profile" method="post" enctype="multipart/form-data" action="/groups-dashboard.php?id_past_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>#past">
                    <input type="hidden" name="action" value="createupdatepast" />
                    <input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />    
                    <input type="hidden" name="id_playdate" value="<?php echo $selectedPastPlaydate->getId_playdate() ?>" />    
                    <p class="titulo uppercase">New comment</p>
                    <div class="row">
                      <div class="col-lg-12">                    
                         <label for="comment" class="sr-only">New comment</label>
                         <textarea class="form-control" placeholder="Comment here to notify your Lead Specialist of any updates or last minute needs. Your comment will be visible to every participant in this playdate. Upload Comment." id="comment" rows="3" name="comment"></textarea>
                      </div>
                    </div>
                    <div class="row row-tabs bt-send">
                  		<div class="col-lg-12">
                     		<button class="btn btn-md btn-primary bt-save-profile" type="submit">Upload Comment</button>
                  		</div>
                	</div>
                </form>
                                  
                </div>
              </div>              
			  
			  <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Feedback</p>
                </div>
                <?php $ratings = RatingDAO::getRatingListByPlaydate($selectedPastPlaydate->getId_playdate()) ?>
                <?php foreach($ratings as $auxRating) {
                    if(($auxRating->getComment_playdate()!=null) && ($auxRating->getComment_playdate()!="")) {
                        $username = "Unknown";
                        if(($auxRating->getId_parent()!=null) && is_numeric($auxRating->getId_parent())) {
                            $auxParent = ParentDAO::getParent($auxRating->getId_parent());
                            $username = $auxParent->getName();
                            $pic = $auxParent->getPicture();
                            if(($pic==null) || ($pic=="")) {
                                $pic = "img/img-profile.jpg";
                            }
                        } else if(($auxRating->getId_specialist()!=null) && is_numeric($auxRating->getId_specialist())) {
                        
                            $auxSpecialist = SpecialistDAO::getSpecialist($auxRating->getId_specialist());
                            $username = $auxSpecialist->getUsername();
                            $pic = $auxSpecialist->getName();
                            if(($pic==null) || ($pic=="")) {
                                $pic = "img/img-profile.jpg";
                            }
                        }
                            
                    ?>
                    <div class="row info-updates">
                        <div class="col-lg-2 date">
                          <img src="<?php echo $pic ?>">
                          <span><strong><?php echo $username ?></strong></span>
                        </div>
                        <div class="col-lg-10 texto">
                          <p class="day"><strong><?php echo Utils::dateFormat($auxRating->getIdate())?></strong></p>
                          <p style="color:#878787"><?php echo $auxRating->getComment_playdate() ?></p>
                        </div>
                    </div>
                    
                    <?php  
                        }
                    }
                ?>
              </div>


              <?php } ?>
              
            </div>
            <div class="tab-pane fade tab-member" id="nav-members" role="tabpanel" aria-labelledby="nav-members-tab">
              <div class="row row-tabs">
              	<?php
              	$parentsGroup = ParentDAO::getParentsListByGroup($group->getId_group(), 1, 100, null, null);
              	foreach($parentsGroup as $auxParent) {
              	    $pic = $auxParent->getPicture();
              	    if(($pic==null) || ($pic=="")) {
              	        $pic = "img/img-profile.jpg";
              	    }
              	?>
              
                <div class="col-lg-2">
                  <?php if($isgroupadmin) { ?>
                  <a href="javascript:;" class="bt-close">X</a>
                  <?php } ?>
                  <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title="<?php echo $auxParent->getFullName() ?>" class="box-member">
                    <img src="<?php echo $pic ?>"/>
                    <p><?php echo $auxParent->getName() ?></p>
                  </a>
                </div>
                <?php } ?>
                <?php
                $specialistsGroup = SpecialistDAO::getSpecialistListByGroupMembers($group->getId_group(), 1, 100, null, null);
              	foreach($specialistsGroup as $auxSpecialist) {
              	    $pic = $auxSpecialist->getPicture();
              	    if(($pic==null) || ($pic=="")) {
              	        $pic = "img/img-profile.jpg";
              	    }
              	?>
              
                <div class="col-lg-2">
                  <a href="javascript:;" class="bt-close">X</a>
                  <a href="/specialists-public-profile.php?id_specialist=<?php echo $auxSpecialist->getId_specialist() ?>" title="<?php echo $auxSpecialist->getFullName() ?>" class="box-member">
                    <img src="<?php echo $pic ?>"/>
                    <p><?php echo $auxSpecialist->getName() ?></p>
                  </a>
                </div>
                <?php } ?>

              </div>
              
              
              <?php if($isgroupadmin) { ?>
                  <div class="row row-tabs">
                    <p class="titulo">Requests to Join Group</p>
                    <div class="col-lg-12">
                    <form action="/groups-dashboard.php#members" method="get">
                    	<input type="hidden" name="action" value="changeprivacity" />
                    	<input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
                      <div class="form-group">
                        <input type="checkbox" id="0" class="form-control" placeholder="keyword" name="newprivacity" value="<?php echo Group::$PRIVACITY_PUBLIC ?>" <?php echo (($group->getPrivacity()==Group::$PRIVACITY_PUBLIC)?"checked=\"checked\"":"") ?> onchange="this.form.submit()" />
                        <label for="0">Automatically accept all requests<a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Automatically accept all requests"><i class="fa fa-question-circle"></i></a></label>
                      </div>
                     </form>
                    </div>
                    <?php $groupRequests = GroupDAO::getParentRequestsByGroup($group->getId_group(), null, null); ?>
                    <?php foreach($groupRequests as $auxRequest) { 
                        $parentReq = ParentDAO::getParent($auxRequest->getId_parent());
                        $pic = $parentReq->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    ?>
                    <div class="col-lg-2">
                      <a href="#" title="" class="box-member">
                        <img src="<?php echo $pic ?>"/>
                        <p><?php echo $parentReq->getName() ?></p>
                        <a href="/groups-dashboard.php?action=accept-parent&id_request=<?php echo $auxRequest->getId_request()?>&id_group=<?php echo $group->getId_group() ?>" class="accept-member">Accept</a>
                        <a href="/groups-dashboard.php?action=decline-parent&id_request=<?php echo $auxRequest->getId_request()?>&id_group=<?php echo $group->getId_group() ?>" class="decline-member">Decline</a>
                      </a>
                    </div>
                    <?php } ?>
                    
                    <?php $groupRequests = GroupDAO::getSpecialistRequestsByGroup($group->getId_group(), null, null); ?>
                    <?php foreach($groupRequests as $auxRequest) { 
                        $specialistReq = SpecialistDAO::getSpecialist($auxRequest->getId_specialist());
                        $pic = $specialistReq->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    ?>
                    <div class="col-lg-2">
                      <a href="#" title="" class="box-member">
                        <img src="<?php echo $pic ?>"/>
                        <p><?php echo $specialistReq->getName() ?></p>
                        <a href="/groups-dashboard.php?action=accept-specialist&id_request=<?php echo $auxRequest->getId_request()?>&id_group=<?php echo $group->getId_group() ?>" class="accept-member">Accept</a>
                        <a href="/groups-dashboard.php?action=decline-specialist&id_request=<?php echo $auxRequest->getId_request()?>&id_group=<?php echo $group->getId_group() ?>" class="decline-member">Decline</a>
                      </a>
                    </div>
                    <?php } ?>
                    
                  </div>
              <?php } ?>
              
              
              
              <!-- INVITE PARENTS -->
                <div class="row info-box">
                    <div class="col-lg-12">
                      <div class="titulo noImages">
                        <h2>Invite Parents to Group</h2>
                        <?php if($invitationSentResult!=null) { ?>
                        <p><?php echo $invitationSentResult ?></p>
                        <?php } ?>
                        <p>Do you know any parents that should join our group?</p>
                        <form class="form-nominate" action="/groups-dashboard.php#members" method="post">
                          <input type="hidden" name="action" value="inviteparent" />
                          <input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
                          <label for="inputParent" class="sr-only">Username or email</label>
                          <input type="text" id="inputParent" class="form-control noMarginTop" placeholder="Username or email" name="parentinfo">
                          <button class="btn btn-md btn-primary bt-titulo" type="submit" >Invite</button>  
                        </form>
                      </div>
                    </div>
              	</div>
                <!-- /INVITE PARENTS -->
                            
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

			  <?php 
			  $picGroup = $group->getPicture();
			  if(($picGroup==null) || ($picGroup=="")) {
			      $picGroup = "img/default-img-group-profile.jpg";
			  }
			  ?>
              <form class="form-profile" id="form-profile" method="post" enctype="multipart/form-data" action="groups-dashboard.php#profile">
              	<input type="hidden" name="action" value="updateprofile" />
              	<input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
              	<?php if($isgroupadmin) { ?>
                <div class="row row-tabs">
                  <div class="col-lg-12">
                     <button class="btn btn-md btn-primary bt-save-profile" type="submit">Save changes</button>
                  </div>
                </div>
                <?php } ?>
                <div class="row row-tabs">
                  <div class="col-lg-4">
                    <img src="<?php echo $picGroup ?>" alt="" class="img-profile">
                    <?php if($isgroupadmin) { ?>
                    <label class="custom-file">
                      <input type="file" id="file" class="custom-file-input"  name="formfile" onchange="previewFile()"/>
                      <span class="custom-file-control"></span>
                    </label>
                    <?php } ?>
                  </div>
                  <div class="col-lg-8 dashboard-profile">
                    <label for="inputName">Name</label>
                    <input type="text" id="inputName" class="form-control noMarginTop" placeholder="Name" name="name" required="required" value="<?php echo $group->getName() ?>"<?php echo((!$isgroupadmin)?" disabled=\"disabled\"":"")?>>
                    <label for="inputNeighborhoods">Your neighborhoods</label>
                    <input type="text" id="location" name="location" class="form-control noMarginTop inputLocation" placeholder="neighborhood">
                    <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                    
					<div class="container-tags">
                    <?php
                    $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByGroup($group->getId_group());
                    foreach ($neighborhoods as $auxItem) {
                    ?>
                      <div class="tag">
                        <span><?php echo $auxItem->getName()?></span>
                        <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                        <input type="hidden" value="<?php echo $auxItem->getId_neighborhood() ?>" name="id_neighborhood[]">
                      </div>
                    
                    <?php 
                    }
                    ?>
                    
                    </div>

                  </div>
                </div>
                <div class="row row-tabs">
                  <div class="col-lg-12">                    
                    <p class="titulo uppercase">Admin</p>
                    <div class="container-specialist-tags row">
                    
                    <?php
                  	$parentsGroup = ParentDAO::getParentsListByGroupAdmin($group->getId_group(), 1, 100, null, null);
                  	foreach($parentsGroup as $auxParent) {
                  	    $pic = $auxParent->getPicture();
                  	    if(($pic==null) || ($pic=="")) {
                  	        $pic = "img/img-profile.jpg";
                  	    }
                  	?>
              
                    
                      <div class="specialist-tag">
                        <img src="<?php echo $pic ?>">
                        <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getFullName() ?></a>
                      </div>
                      <?php 
                  	}
                      ?>
                      
                      <?php
                  	$specialistsGroup = SpecialistDAO::getSpecialistListByGroupAdmin($group->getId_group(), 1, 100, null, null);
                  	foreach($specialistsGroup as $auxSpec) {
                  	    $pic = $auxSpec->getPicture();
                  	    if(($pic==null) || ($pic=="")) {
                  	        $pic = "img/img-profile.jpg";
                  	    }
                  	?>
              
                    
                      <div class="specialist-tag">
                        <img src="<?php echo $pic ?>">
                        <a href="/specialists-public-profile.php?id_specialist=<?php echo $auxSpec->getId_specialist() ?>" title=""><?php echo $auxSpec->getFullName() ?></a>
                      </div>
                      <?php 
                  	}
                      ?>
                    </div>

					<?php if($isgroupadmin) { ?>
                    <div class="row">
                      <div class="col-lg-3">
                      <label for="inputUsername" class="sr-only">Username or email</label>
                        <input type="text" id="inputUsername" class="form-control" placeholder="Username or email" >
                      </div>
                      <div class="col-lg-3">
                        <button class="btn btn-md btn-primary bt-add-admin form-control" type="submit">Add Admin</button>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">                    
                     <label for="aboutGroup">About Group</label>
                     <textarea class="form-control" placeholder="About Group" id="aboutGroup" rows="7" name="description"<?php echo((!$isgroupadmin)?" disabled=\"disabled\"":"")?>><?php echo $group->getDescription() ?></textarea>
                  </div>
                </div>
                <?php if($isgroupadmin) { ?>
                <div class="row">
                  <div class="col-lg-12">
                     <button class="btn btn-md btn-primary bt-save-profile last" type="submit">Save changes</button>
                  </div>
                </div> 
                <?php } ?>
              </form>
              
		      <p>&nbsp;</p>
            </div>
            <div class="tab-pane fade" id="nav-specialists" role="tabpanel" aria-labelledby="nav-specialists-tab">
              <div class="row featured-specialists">
                  
                <?php 
        		$specialists = SpecialistDAO::getSpecialistsListByGroup($group->getId_group(), 1, 8, null, null);
        		
        		foreach ($specialists as $specialistaux) {
        		    $ratingvalue = RatingDAO::getSpecialistValue($specialistaux->getId_specialist());
        		    $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialistaux->getId_specialist());
        		?>
                <div class="col-lg-4 col-md-6 col-sm-6 box-specialist">
                   <div class="flip">
                        <div class="card">
                          <div class="specialist face front">
                            <div class="info">
                               <p><?php echo $specialistaux->getFullName() ?></p>
                               <div class="puntuacion">
                          <ul>
                            <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                            <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                            <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                            <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                          	<li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                          </ul>
                          <span class="num"><?php echo $ratingvalue ?>/5</span>
                        </div>    
                              
                            </div>             
                            <img src="<?php echo $specialistaux->getPicture() ?>"/>
                          </div>
                          <div class="face back">
                            <div class="info-back">
                              <div class="ico-specialist family"><span>1<br>repeat families</span></div>
                              <div class="ico-specialist pd"><span><?php echo $numPlaydates ?><br>playdates</span></div>
                              <p><?php echo $specialistaux->getAbout_me() ?></p>
                              <a class="bt-profile" title="" href="/specialist-profile.php?id_specialist=<?php echo $specialistaux->getId_specialist()?>&slug=<?php echo $specialistaux->getSlug() ?>">View profile</a>
                            </div>            
                            <img src="<?php echo $specialistaux->getPicture() ?>"/>
                          </div>
                        </div>
                      </div>
                </div>
        		<?php } ?>
              </div>
                <!-- INVITE SPECIALIST -->
                <div class="row info-box">
                    <div class="col-lg-12">
                      <div class="titulo noImages">
                        <h2>Invite Specialists to Group</h2>
                        <?php if($invitationSpecialistSentResult!=null) { ?>
                        <p><?php echo $invitationSpecialistSentResult ?></p>
                        <?php } ?>
                        <p>Do you know any specialists that should join our group?</p>
                        <form class="form-nominate" action="/groups-dashboard.php#specialists" method="post">
                          <input type="hidden" name="action" value="invitespecialist" />
                          <input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
                          <label for="inputParent" class="sr-only">Username or email</label>
                          <input type="text" id="inputParent" class="form-control noMarginTop" placeholder="Username or email" name="specialistinfo">
                          <button class="btn btn-md btn-primary bt-titulo" type="submit" >Invite</button>  
                        </form>
                      </div>
                    </div>
              	</div>
                <!-- /INVITE SPECIALIST -->
			  <?php /*
              <div class="row info-box">

                  <div class="col-lg-12">
                    <div class="titulo noImages">
                      <h2>Nominate a specialist</h2>
                      <p>Do you know a child care professional that should join playdate? </p>
                      <form class="form-nominate">
                        <label for="inputEmail" class="sr-only">Username or Email</label>
                        <input type="text" id="inputEmail" class="form-control" placeholder="Username or Email" required="" >
                        <button class="btn btn-md btn-primary bt-titulo" type="submit">Nominate</button>  
                      </form>
                    </div>

                  </div>

              </div>
              */?>

            </div>
            <div class="tab-pane fade" id="nav-media" role="tabpanel" aria-labelledby="nav-media-tab">
              <div class="row row-tabs">
                <?php 
                $media = GroupDAO::getMediaListByGroup($group->getId_group());
                foreach($media as $auxMedia) {
                
                ?>

                <div class="col-lg-4">
                  <a href="#" title="" class="box-media">
                    <img src="<?php echo $auxMedia->getPicture() ?>"/>
                    <p><?php echo $auxMedia->getDescription() ?></p>
                  </a>
                </div>
				<?php } ?>
                 

                <div class="col-lg-12">
                  <a href="javascript:;" class="bt-save-profile last bt-upload-media"  data-toggle="modal" data-target="#last-playdate">Upload Media</a>
                </div>

              </div> 
              
                <!-- INVITE PARENTS -->
                <div class="row info-box">
                    <div class="col-lg-12">
                      <div class="titulo noImages">
                        <h2>Invite Parents to Group</h2>
                        <?php if($invitationSentResult!=null) { ?>
                        <p><?php echo $invitationSentResult ?></p>
                        <?php } ?>
                        <p>Do you know any parents that should join our group?</p>
                        <form class="form-nominate" action="/groups-dashboard.php" method="post">
                          <input type="hidden" name="action" value="inviteparent" />
                          <input type="hidden" name="id_group" value="<?php echo $group->getId_group() ?>" />
                          <label for="inputParent" class="sr-only">Username or email</label>
                          <input type="text" id="inputParent" class="form-control noMarginTop" placeholder="Username or email" name="parentinfo">
                          <button class="btn btn-md btn-primary bt-titulo" type="submit" >Invite</button>  
                        </form>
                      </div>
                    </div>
              	</div>
                <!-- /INVITE PARENTS -->
              
            </div>
            
          </div>
        
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

	<?php if($invitationSentResult!=null) { ?>
		<script>
		$(document).ready(function (){
			$("html, body").animate({ scrollTop: $('.info-box').offset().top-300}, 1000);
		});
		</script>
	<?php } ?>
	
		<?php if($selectedPlaydate!=null) { ?>
		<script>
		$(document).ready(function (){
			$("html, body").animate({ scrollTop: $('#selected-playdate').offset().top-300}, 1000);
		});
		</script>
		<?php } ?>
		
		<?php if($selectedPastPlaydate!=null) { ?>
		<script>
		$(document).ready(function (){
			$("html, body").animate({ scrollTop: $('#selected-past-playdate').offset().top-300}, 1000);
		});
		</script>
		<?php } ?>
		
	
  </body>

</html>

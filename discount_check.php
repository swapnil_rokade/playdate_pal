<?php 
/**
 * Discount related oprations
 */
include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");

if (isset($_POST['discountCode'])) {
    $discount_code = $_POST['discountCode'];
    $discount = DiscountDAO::getDiscountByCode($discount_code);
    
    if($discount){
        $discount_details = DiscountDAO::getDiscountDetails($discount_code,$_POST['parentId']);
        if($discount_details){
            $response_array['status'] = 'error';
            $response_array['message'] = 'Discount Already Used!';      
        }else {
            $discount_type = DiscountDAO::getDiscountType($discount->getType());
            $discount_percentage = $discount_type->getDiscount();
            $response_array['status'] = 'success';
            $response_array['message'] = 'Data Found';
            $response_array['discount_percentage'] = $discount_percentage;      
        }
    }else {
        $response_array['status'] = 'error';
        $response_array['message'] = 'Discount Code is invalid or expired.';  
    }

    header('Content-type: application/json');
    echo json_encode($response_array);
    
}

?>

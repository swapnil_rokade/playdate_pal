<?php 
/**
 * PLAYDATE - TERMS OF SERVICE 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$backUrl = "/terms-of-service.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Terms of Service - PAL by Project Playdate</title>
 
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">

	<?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Terms of service</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">
      <div class="row terms-of-service">
        <div class="col-lg-12">
          <p>Effective Date: June 30, 2018</p>
<p>Playdate Labs, Inc. (hereinafter "Playdate Labs," "we" or "us") provides this website, our computing applications, and the data, information, tools, updates and similar materials delivered or provided by us (collectively, the "Service"), subject to your agreement to and compliance with the conditions set forth in this Terms of Service agreement (the "Agreement").</p>
<p>This Agreement sets forth the legally binding terms and conditions governing your use of the Service. By using the Service or otherwise entering into this Agreement, you are creating a binding contract with us. If you do not agree to these terms and conditions, you may not use the Service.<p>
<p>We may revise or update this Agreement by posting an amended version through the Service and making you aware of the revisions, which may be through posting to the Service or otherwise. Your use of the Service following an update to this Agreement (or other acceptance method) is considered acceptance of the updated Agreement.</p>
<!--p><b>License</b></p>
<p>As long as you are in compliance with the conditions of this Agreement and all incorporated documents, we hereby grant you a limited, revocable, non-assignable, non-transferrable, non-sublicensable, non-exclusive license to access, receive and use the Service. No rights not explicitly listed are granted.</p-->
<p>Incorporated Terms</p>
<p>The following additional terms are incorporated into this Agreement as if fully set forth herein:</p>
<p>&bullet;	<b>Privacy Policy</b></p>
<p>&bullet; <b>Copyright Policy</b></p>
<p>&bullet; <b>Complaint Policy (including Privacy and Trademark)</b></p>
<p><b>Eligibility</b></p>
<p>There are two types of users on the Service: those offering the engagement of their own services ("Providers") and those seeking to engage Providers ("Customers", and together with Providers, "Users").</p>
<p>You must be at least eighteen (18) years old to use the Service as a Customer, and at least twenty one (21) years old to use the Service as a Provider. By using the Service, you represent that you meet the applicable minimum age requirement.</p>
<p>Some parts or all of the Service may not be available to the general public, and we may impose eligibility rules from time to time. We reserve the right to amend or eliminate these eligibility requirements at any time.</p>
<p>By requesting to use, registering to use and/or using the Service, you represent and warrant that you have the right, authority and capacity to enter into these Terms and you commit to abide by all of the terms and conditions hereof. You also represent and warrant that you meet the following eligibility criteria:</p>
<p>&bullet; If you are a Provider, you are permitted to legally work within the United States;</p>
<p>&bullet; Neither you nor any member of your household have ever been (i) the subject of a complaint, restraining order or any other legal action involving, arrested for, charged with, or convicted of any felony, any criminal offense involving violence, abuse, neglect, fraud or larceny, or any offense that involves endangering the safety of others, dishonesty, negligence or drugs, or (ii) registered, or currently required to register, as a sex offender with any government entity; and</p>
<p>&bullet; You are not a competitor of Playdate Labs.</p>
<p><b>IMPORTANT NOTICES</b></p>
<p>We do not represent or warrant that access to the Service will be error-free or uninterrupted, or without defect, and we do not guarantee that Users will be able to access or use the Service, or its features, at all times. We reserve the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service, or any part thereof, with or without notice.</p>
<p>The Service may contain typographical errors or inaccuracies, including relating to price, and may not be complete or current. We reserve the right to correct any such errors, inaccuracies or omissions and to change or update information at any time without prior notice, even if your order has already been accepted or processed.</p>
<p>Although the Service may identify a recommended size and child/caregiver ratio to each Playdate (as defined herein), we do not guarantee that this ratio will be actually reflected in the Playdate when it is run. We do not control whether or not Providers admit additional attendees to a particular Playdate.</p>
<p>The Playdates that Providers advertise and manage through the Service are group activities. We do not control the makeup of the children in each Playdate, and we are not responsible for ensuring their health, vaccinations, or behavior . All Playdates involve children, which are inherently unpredictable.</p> 
<p>We are not responsible for transportation to or from a Playdate, or for the return of children to their parents or guardians at the end of a Playdate. Parents are solely responsible for travel and safe drop-off and pickup.</p>
<p>WE DO NOT PROVIDE ANY OF THE SERVICES BEING OFFERED OR RENDERED BY PROVIDERS (THE "Provider Services"). OUR SERVICE IS LIMITED TO A MEANS OF COMMUNICATING, COORDINATING AND TRANSACTING WITH OTHER PARTIES. IF OUR SERVICE IS USED TO COORDINATE THE PURCHASE OF PROVIDER SERVICES, IT IS UP TO THE PROVIDER TO OFFER THEIR PROVIDER SERVICES, AND FOR THE CUSTOMER TO DETERMINE THE SUITABILITY OR SAFETY OF SUCH PROVIDER SERVICES.</p>
<p>WE HAVE NO RESPONSIBILITY OR LIABILITY RELATING TO ANY PROVIDER SERVICES, ANY CUSTOMER PAYMENT, OR THE ACTIONS OR CONDUCT OF ANY USER.</p>
<p>ALTHOUGH WE MAY ELECT NOT TO INVESTIGATE OR PERFORM BACKGROUND SEARCHES ON USERS OF THE SERVICE, AS A USER, YOU CONSENT TO US CONDUCTING A FULL BACKGROUND SEARCH ON YOU. YOU ACKNOWLEDGE AND AGREE THAT WE DO NOT HAVE CONTROL OVER OR ASSUME ANY RESPONSIBILITY FOR THE QUALITY, ACCURACY, OR RELIABILITY OF THE INFORMATION INCLUDED IN ANY SUCH BACKGROUND CHECK. THE RESULTS OF BACKGROUND CHECKS MAY BE SHARED WITH OTHER USERS OF THE SERVICE. NEVERTHELESS, IT IS ULTIMATELY UP TO EACH USER TO ENSURE THEIR OWN SAFETY IN YOUR INTERACTIONS AND MEETINGS WITH OTHER USERS OF THE SERVICE.</p>
<p>Any and all claims relating to your satisfaction with any services or goods provided to you by Providers shall solely be brought against such Providers, and you agree not to include us in any such claim.</p>
<p><b>Photography Consent</b></p>
<p>The Service may provide functionality for Provider to upload and share content produced related to the Playdate (as defined herein) to a group of users that are related to that Playdate. Such content may include depictions of Customers and/or Customer's minor children. As a Customer, you hereby consent grant your consent for your child to be photographed or recorded during the Playdate, and that such content may be shared with us, with Provider and with other users of the Service relating to the Playdate in which you or your child is participating.</p>
<p><b>Offerings, Cancellations and Payments</b></p>
<p>As a Provider, you may post the availability of Provider Services to the Service that consist of an organized group activity (a "Playdate"). We may require you to provide certain details about such Playdate as may change from time to time, including but not limited to the location, length, date (the "Event Date"), RSVP date (typically 48 hours before the event date), maximum party size and any additional supplies or admissions costs associated with the Playdate.</p>
<p>If you are a Provider you represent and warrant that you are licensed to sell and/or perform all goods or services that you offer on the Service, and that all such goods or services comply with applicable law, are safe and suitable for the intended purpose and for use by children, and will not cause damage or injury to any party. IT IS A PROVIDER'S SOLE OBLIGATION TO ENSURE THE LEGALITY, SUITABILITY AND APPROPRIATENESS OF THEIR SERVICES. USERS AGREE THAT WE ARE NOT RESPONSIBLE FOR THE FOREGOING, AND THAT USERS WILL NOT CLAIM THAT WE ARE RESPONSIBLE FOR THE FOREGOING. As a Provider, you contractually agree with us to render the goods or services that you offer on the Service and that are purchased by Customers.</p>
<p>If you, as a Provider, post the availability of a Playdate to other users, we will specify the charges to Customers for those that Playdate, and the percentage of the overall charges that will be paid to the Provider ("Provider's Pay"), as disclosed to Provider in the Service's Provider knowledge center, and as updated by us from time to time. A Customer may reserve a spot in the Playdate by booking the spot on the Service prior to the RSVP date.</p>
<p>Either a Customer or Provider may cancel a booking in accordance with our cancellation policy, available here: _________ (the "Cancellation Policy"). NOTE THAT CANCELLATION CHARGES MAY APPLY FOR CANCELLATIONS WITHIN A CERTAIN TIME OF THE SCHEDULED PERFORMANCE BY A PROVIDER.</p>
<p>On the RSVP Date, Customer will be charged the rate for the Playdate, as published on the Service and communicated on the RSVP date. Subject to availability After the RSVP Date, the Service may permit Customers to book a spot in specific Playdate. Under such circumstances, Customer will be charged immediately. Such bookings are subject to the Cancellation Policy.</p>
<p></p>
<p>Subject to the Cancellation Policy, all transactions are final, and no refunds are available.</p>
<p>We may use a third-party payment processor (the "Payment Processor") to bill and remit payments to users and to draw amounts from users' accounts as indicated. The processing of payments will be subject to the terms, conditions and privacy policies of the Payment Processor in addition to these Terms. We are not responsible for error by the Payment Processor. You agree to provide us accurate and complete information about you and your chosen payment provider or account (your "Payment Method"), and you authorize us to share such information as well as transaction information with the Payment Processor. You agree to pay us, through the Payment Processor, all charges at the prices then in effect for any use of the Service. You agree to make payment using that selected Payment Method, and you authorize us, through the Payment Processor, to charge your Payment Method. We reserve the right to correct any errors or mistakes that it makes even if it has already requested or received payment.</p>
<p>At the conclusion of the Playdate, Provider shall notify us, and we will promptly initiate the payment of Provider's Pay from the Playdate to Provider through the Payment Processor, or as otherwise agreement between Provider and us.</p>
<p><b>Provider as Independent Contractors</b></p>
<p>If you are a Provider, you agree that we are a vendor to you. You are not providing services to us of any kind, and at all times we will be considered independent contractors. You agree that (i) we have no obligation to provide you (or any contractor, employee or agent of Provider, if applicable) with disability insurance, worker's compensation, unemployment or other such insurance; (ii) you shall provide you're your own expense, such insurance as is lawfully required; and, (iii) you (and any of your contractors, employees or agents, if applicable) will have no right to participate in our health insurance or other employee benefit plans.  We will not withhold nor make payments for social security, unemployment insurance or disability insurance contributions, or obtain workers' compensation insurance on your behalf. Provider understands that, as the parties are independent contractors, Provider is solely and exclusively liable for complying with, and making all payments under, all applicable state, federal and international laws, including laws governing self-employed individuals if applicable, such as laws related to payment of taxes, social security, disability, and other contributions based on amounts that we remit to Provider.</p>  
<p><b>Rules of Conduct</b></p>
<p>Your use of the Service is conditioned on your compliance with the terms of this Agreement, including but not limited to these rules of conduct.</p>
<p>You agree that you will not violate any applicable law or regulation in connection with your use of the Service.</p>
<p>You agree not to distribute, upload, make available or otherwise publish through the Service any suggestions, information, ideas, comments, causes, promotions, documents, questions, notes, plans, drawings, proposals, or materials similar thereto ("Submissions") or graphics, text, information, links, profiles, audio, photos, software, music, sounds, video, comments, messages or tags, or similar materials ("Content") that:</p>
<p>&bullet; is unlawful or encourages another to engage in anything unlawful;</p>
<p>&bullet; contains a virus or any other similar programs or software which may damage the operation of our or another's computer;</p>
<p>&bullet; violates the rights of any party or infringes upon the patent, trademark, trade secret, copyright, right of privacy or publicity or other intellectual property right of any party; or</p>
<p>&bullet; is libelous, defamatory, pornographic, obscene, lewd, indecent, inappropriate, invasive of privacy or publicity rights, abusing, harassing, threatening or bullying.</p>
<p>You must keep your user name and password and any other information needed to login to the Service, if applicable, confidential and secure. We are not responsible for any unauthorized access to your account or profile by others.</p>
<p>You further agree that you will not do any of the following:</p>
<p>&bullet; breach, through the Service, any agreements that you enter into with any third parties;</p>
<p>&bullet; stalk, harass, injure, or harm another individual through the Service;</p>
<p>&bullet; modify, adapt, translate, copy, reverse engineer, decompile or disassemble any portion of the Service;</p>
<p>&bullet; interfere with or disrupt the operation of the Service, including restricting or inhibiting any other person from using the Service by means of hacking or defacing;</p>
<p>&bullet; transmit to or make available in connection with the Service any denial of service attack, virus, worm, Trojan horse or other harmful code or activity;</p>
<p>&bullet; attempt to probe, scan or test the vulnerability of a system or network of the Service or to breach security or authentication measures without proper authorization;</p>
<p>&bullet; take any action that imposes, or may impose, in our sole discretion, an unreasonable or disproportionately large load on our infrastructure;</p>
<p>&bullet; harvest or collect the email address, contact information, or any other personal information of other users of the Service;</p>
<p>&bullet; use any means to crawl, scrape or collect content from the Service via automated or large group means;</p>
<p>&bullet; submit, post or make available false, incomplete or misleading information to the Service, or otherwise provide such information to us;</p>
<p>&bullet; register for more than one user account; or</p>
<p>&bullet; impersonate any other person or business.</p>
<p>You are not licensed to access any portion of the Service that is not public, and you may not attempt to override any security measures in place on the Service.</p>
<p>We reserve the right, in our sole discretion, to protect our users from violators and violations of these rules of conduct, including but not limited to restricting your use of the Services, restricting your ability to upload Submissions or Content, immediately terminating your use of the Service, or terminating your use of the Service by blocking certain IP addresses from accessing the Service. Notwithstanding the foregoing, our unlimited right to terminate your access to the Service shall not be limited to violations of these rules of conduct.</p>
<p><b>Content Submitted or Made Available to Us</b></p>
<p>Unless otherwise noted, we will not claim ownership of any Content. However, in order for us to provide the Service, we need your permission to process, display, reproduce and otherwise use content you make available to us.</p>
<p>Therefore, if you choose to submit any Content to the Service, or otherwise make available any Content through the Service, you hereby grant to us a perpetual, irrevocable, transferrable, sub-licensable, non-exclusive, worldwide, royalty-free license to reproduce, use, modify, display, perform, transmit, distribute, translate and create derivative works from any such Content, including without limitation distributing part or all of the Content in any media format through any media channels, including but not limited to the right to commercially use the rights of publicity, persona, trademark, image and name of the individuals and entities depicted in such Content.</p>
<p>By submitting any Content or Submissions to us you hereby agree, warrant and represent that: (a) the Content and Submissions do not contain proprietary or confidential information, and the provision of the Content and Submissions is not a violation of any third-party's rights; (b) all such Submissions and Content are accurate and true, (c) we are not under any confidentiality obligation relating to the Content or Submissions; (d) we shall be entitled to use or disclose the Content or Submissions in any way; and (e) you are not entitled to compensation or attribution from us in exchange for the Submissions or Content.</p>
<p>You acknowledge that we are under no obligation to maintain the Service, or any information, materials, Submissions, Content or other matter you submit, post or make available to or on the Service. We reserve the right to withhold, remove and or discard any such material at any time.</p>
<p><b>Our Intellectual Property</b></p>
<p>Our graphics, logos, names, designs, page headers, button icons, scripts, and service names are our trademarks, trade names and/or trade dress. The "look" and "feel" of the Service (including color combinations, button shapes, layout, design and all other graphical elements) are protected by U.S. copyright and trademark law. All product names, names of services, trademarks and service marks ("Marks") are our property or the property of their respective owners, as indicated. You may not use the Marks or copyrights for any purpose whatsoever other than as permitted by this Agreement.</p>
<p>You acknowledge that the software used to provide the Service, and all enhancements, updates, upgrades, corrections and modifications to the software, all copyrights, patents, trade secrets, or trademarks or other intellectual property rights protecting or pertaining to any aspect of the software (or any enhancements, corrections or modifications) and any and all documentation therefor, are and shall remain our sole and exclusive property or that of our licensors, as the case may be. This Agreement does not convey title or ownership to you, but instead gives you only the limited rights set forth herein.</p>
<p><b>Data Collection and Use</b></p>
<p>You understand and agree that our Privacy Policy shall govern the collection and use of data obtained by us through your use of the Service.</p>
<p>Enforcement and Termination</p>
<p>We reserve the right to deny all or some portion of the Service to any user, in our sole discretion, at any time. Without limiting the foregoing or assuming additional legal obligations, we have a policy of terminating repeat violators of the Copyright Act, in accordance with applicable law.</p>
<p>All grants of any rights from you to us related to Content, Submissions, or other materials, including but not limited to copyright licenses, shall survive any termination of this Agreement. Further, your representations, defense and indemnification obligations survive any termination of this Agreement.</p>
<p><b>Links and Third-Party Content</b></p>
<p>The Service may contain links. Such links are provided for informational purposes only, and we do not endorse any website or services through the provision of such a link.</p>
<p>The Service may contain articles, text, imagery, video, audio, data, information and other similar materials originating from third-parties. We do not endorse any third party content that may appear on the Service or that may be derived from content that may appear on the Service, even if such content was summarized, collected, reformatted or otherwise edited by us.</p>
<p><b>DISCLAIMERS AND LIMITATION ON LIABILITY</b></p>
<p>EXCEPT WHERE NOT PERMITTED BY LAW, YOU AGREE AND ACKNOWLEDGE THAT THE SERVICE IS PROVIDED "AS IS" AND "AS AVAILABLE", WITHOUT ANY WARRANTY OR CONDITION, EXPRESS, IMPLIED OR STATUTORY, AND WE, AND OUR PARENTS, SUBSIDIARIES, OFFICERS, DIRECTORS, SHAREHOLDERS, MEMBERS, MANAGERS, EMPLOYEES AND SUPPLIERS, SPECIFICALLY DISCLAIM ANY IMPLIED WARRANTIES OF TITLE, ACCURACY, SUITABILITY, APPLICABILITY, MERCHANTABILITY, PERFORMANCE, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OR ANY OTHER WARRANTIES OF ANY KIND IN AND TO THE SERVICE. NO ADVICE OR INFORMATION (ORAL OR WRITTEN) OBTAINED BY YOU FROM US SHALL CREATE ANY WARRANTY.</p>
<p>FURTHER, OPINIONS, ADVICE, STATEMENTS, OFFERS, SUBMISSIONS OR OTHER INFORMATION OR CONTENT MADE AVAILABLE THROUGH THE SERVICE, BUT NOT DIRECTLY BY US, ARE THOSE OF THEIR RESPECTIVE AUTHORS, AND SHOULD NOT BE RELIED UPON. SUCH AUTHORS ARE SOLELY RESPONSIBLE FOR SUCH CONTENT.</p>
<p>USE OF THE SERVICE IS AT YOUR SOLE RISK. WE DO NOT WARRANT THAT YOU WILL BE ABLE TO ACCESS OR USE THE SERVICE AT THE TIMES OR LOCATIONS OF YOUR CHOOSING; THAT THE SERVICE WILL BE UNINTERRUPTED OR ERROR-FREE; THAT DEFECTS WILL BE CORRECTED; THAT DATA TRANSMISSION OR STORAGE IS SECURE OR THAT THE SERVICE IS FREE OF INACCURACIES, MISREPRESENTATIONS, VIRUSES OR OTHER HARMFUL INFORMATION OR COMPONENTS.</p>
<p>TO THE MAXIMUM EXTENT PERMITTED BY LAW, AND EXCEPT AS PROHIBITED BY LAW, IN NO EVENT SHALL WE OR OUR AFFILIATES, LICENSORS AND BUSINESS PARTNERS (COLLECTIVELY, THE "RELATED PARTIES") BE LIABLE TO YOU BASED ON OR RELATED TO THE SERVICE, WHETHER BASED IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, AND SHALL NOT BE RESPONSIBLE FOR ANY LOSSES OR DAMAGES, INCLUDING WITHOUT LIMITATION DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, OR SPECIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH ACCESS TO OR USE OF THE SERVICE.</p>
<p>Notwithstanding the foregoing, in the event that a court shall find that the above disclaimers are not enforceable, then, to the maximum extent permissible by law, you agree that neither we nor any of our subsidiaries, affiliated companies, employees, members, shareholders, officers or directors shall be liable for (1) any damages in excess of the greater of (a) $500.00 or (b) the amounts paid to, or by, you through the Service within the last six months, or (2) any indirect, incidental, punitive, special, or consequential damages or loss of use, lost revenue, lost profits or data to you or any third party from your use of the Service or any goods sold or provided by us. This limitation shall apply regardless of the basis of your claim, whether other provisions of this Agreement have been breached, or whether or not the limited remedies provided herein fail of their essential purpose.</p>
<p>This limitation shall not apply to any damage that we cause you intentionally and knowingly in violation of this Agreement or applicable law that cannot be disclaimed in this Agreement.</p>
<p>SOME STATES, INCLUDING NEW JERSEY, MAY NOT PERMIT CERTAIN DISCLAIMERS AND LIMITATIONS, AND ANY SUCH DISCLAIMERS OR LIMITATIONS ARE VOID WHERE PROHIBITED.</p>
<p><b>Indemnification</b></p>
<p>You agree to defend, indemnify and hold us and our suppliers, subsidiaries, licensors, and licensees, and each of their officers, directors, shareholders, members, employees and agents harmless from all allegations, judgments, awards, losses, liabilities, costs and expenses, including but not limited to reasonable attorney's fees, expert witness fees, and costs of litigation arising out of or based on (a) Submissions or Content you submit, post to or transmit through the Service (b) your use of the Service, (c) your violation of this Agreement, and (d) any conduct, activity or action which is unlawful or illegal under any state, federal or common law, or is violative of the rights of any individual or entity, engaged in, caused by, or facilitated in any way through the use of the Service.</p>
<p><b>Governing Law and Jurisdiction; Arbitration</b></p>
<p>You agree that any claim or dispute arising out of or relating in any way to the Service will be resolved solely and exclusively by binding arbitration, rather than in court, except that you may assert claims in small claims court if your claims qualify. The Federal Arbitration Act and federal arbitration law apply to this agreement. The laws of the State of New York shall govern this Agreement, and shall be used in any arbitration proceeding.</p>
<p>There is no judge or jury in arbitration, and court review of an arbitration award is limited. However, an arbitrator can award on an individual basis the same damages and relief as a court (including injunctive and declaratory relief or statutory damages), and must follow the terms of this Agreement as a court would.</p>
<p>To begin an arbitration proceeding, you must send a letter requesting arbitration and describing your claim to the following address: 175 Varick Street, WeWork Soho West CSL, New York, NY 10014</p>
<p>Arbitration under this Agreement will be conducted by the American Arbitration Association (AAA) under its rules then in effect, shall be conducted in English, and shall be located in New York, New York. Payment of all filing, administration and arbitrator fees will be governed by the AAA's rules.</p>
<p>You and Playdate Labs agree that any dispute resolution proceedings will be conducted only on an individual basis and not in a class, consolidated or representative action. If for any reason a claim proceeds in court rather than in arbitration, both you and Playdate Labs agree that each have waived any right to a jury trial.</p>
<p>Notwithstanding the foregoing, you agree that we may bring suit in court to enjoin infringement or other misuse of intellectual property or other proprietary rights.</p>
<p>All aspects of the arbitration proceeding, and any ruling, decision or award by the arbitrator, will be strictly confidential for the benefit of all parties.</p>
<p>To the extent arbitrations does not apply, you agree that any dispute arising out of or relating to the Service, or to us, may only be brought by you in a state or federal court located in New York, New York. YOU HEREBY WAIVE ANY OBJECTION TO THIS VENUE AS INCONVENIENT OR INAPPROPRIATE, AND AGREE TO EXCLUSIVE JURISDICTION AND VENUE IN NEW YORK.</p>
<p><b>POLICIES FOR CHILDREN</b></p> 
<p>The Service is not directed to individuals under the age of 18. In the event that we discover that a child under the age of 13 has provided personally identifiable information to us, we will make efforts to delete the child's information if required by the Children's Online Privacy Protection Act. Please see the Federal Trade Commission's website for (www.ftc.gov) for more information.</p>
<p>Notwithstanding the foregoing, pursuant to 47 U.S.C. Section 230 (d), as amended, we hereby notify you that parental control protections are commercially available to assist you in limiting access to material that is harmful to minors. More information on the availability of such software can be found through publicly available sources. You may wish to contact your internet service provider for more information.</p>
<p><b>GENERAL</b></p>
<p><u>Severability</u>. If any provision of this Agreement is found for any reason to be unlawful, void or unenforceable, then that provision will be given its maximum enforceable effect, or shall be deemed severable from this Agreement and will not affect the validity and enforceability of any remaining provision.</p>
<p><u>Revisions</u>. This Agreement is subject to change on a prospective basis at any time. In the event that we change this Agreement, you may be required to re-affirm the Agreement through use of the Service or otherwise. Your use of the Service after the effective date of any changes will constitute your acceptance of such changes.</p>
<p><u>No Partnership</u>. You agree that no joint venture, partnership, employment, or agency relationship exists between you and us as a result of this Agreement or your use of the Service.</p>
<p><u>Assignment</u>. We may assign our rights under this Agreement, in whole or in part, to any person or entity at any time with or without your consent. You may not assign the Agreement without our prior written consent, and any unauthorized assignment by you shall be null and void.</p>
<p><u>No Waiver</u>. Our failure to enforce any provision of this Agreement shall in no way be construed to be a present or future waiver of such provision, nor in any way affect the right of any party to enforce each and every such provision thereafter. The express waiver by us of any provision, condition or requirement of this Agreement shall not constitute a waiver of any future obligation to comply with such provision, condition or requirement.</p>
<p><u>Notices</u>. All notices given by you or required under this Agreement shall be in writing and addressed to: _____.</p>
<p><u>Equitable Remedies</u>. You hereby agree that we would be irreparably damaged if the terms of this Agreement were not specifically enforced, and therefore you agree that we shall be entitled, without bond, other security, or proof of damages, to appropriate equitable remedies with respect to breaches of this Agreement, in addition to such other remedies as we may otherwise have available to us under applicable laws.</p>
<p><u>Entire Agreement</u>. This Agreement, including the documents expressly incorporated by reference, constitutes the entire agreement between you and us with respect to the Service, and supersedes all prior or contemporaneous communications, whether electronic, oral or written.</p>
<p><b>COPYRIGHT POLICY</b></p>
<p>If you believe in good faith that any material posted on our Services infringes the copyright in your work, please contact our copyright agent, designated under the Digital Millennium Copyright Act ("DMCA") (17 U.S.C. &sect;512(c)(3)), with correspondence containing the following:</p>
<p>&bullet; A physical or electronic signature of the owner, or a person authorized to act on behalf of the owner, of the copyright that is allegedly infringed;</p>
<p>&bullet; Identification of the copyrighted work claimed to have been infringed;</p>
<p>&bullet; Identification, with information reasonably sufficient to allow its location of the material that is claimed to be infringing;</p>
<p>&bullet; Information reasonably sufficient to permit us to contact you;</p>
<p>&bullet; A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and,</p>
<p>&bullet; A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</p>
<p>You acknowledge that if you fail to comply with all of the requirements of this policy, your DMCA notice may not be valid. For any questions regarding this procedure, or to submit a complaint, please contact our designated DMCA Copyright Agent:</p>
<p><b>Copyright Agent</b></p>
<p>Playdate Labs, Inc.<br>
175 Varick St.<br>
c/o CSL<br>
New York, NY 10014<br>
e-mail: <a href="mailto:amanda@playdatepal.com">amanda@playdatepal.com</a></p>
<p><b>COMPLAINT POLICY (INCLUDING TRADEMARK AND PRIVACY)</b></p>
<p>If you believe in good faith that any material posted on the Services infringes any of your rights other than in copyright, or is otherwise unlawful, you must send a notice to <a href="mailto:amanda@playdatepal.com">amanda@playdatepal.com</a>, containing the following information:</p>
<p>&bullet; Your name, physical address, e-mail address and phone number;</p>
<p>&bullet; A description of the material posted on the Site that you believe violates your rights or is otherwise unlawful, and which parts of said materials you believe should be remedied or removed;</p>
<p>&bullet; Identification of the location of the material on the Site;</p>
<p>&bullet; If you believe that the material violates your rights, a statement as to the basis of the rights that you claim are violated;</p>
<p>&bullet; If you believe that the material is unlawful or violates the rights of others, a statement as to the basis of this belief;</p>
<p>&bullet; A statement under penalty of perjury that you have a good faith belief that use of the material in the manner complained of is not authorized and that the information you are providing is accurate to the best of your knowledge and in good faith; and</p>
<p>&bullet; Your physical or electronic signature.</p>
<p>If we receive a message that complies with all of these requirements, we will evaluate the submission, and if appropriate, in our sole discretion, we will take action. We may disclose your submission to the poster of the claimed violative material, or any other party.</p>
<p><b>ADDITIONAL TERMS APPLICABLE TO iOS-POWERED SOFTWARE</b></p>
<p>In the event that you use the Service through software operating on iOS, the following additional terms apply:</p>
<p>1.	You acknowledge that these terms of use are concluded between you and us only, and not with Apple, Inc. ("Apple"). We, and not Apple, are solely responsible for its iOS application and the services and materials available thereon.</p>
<p>2.	Your use of our iOS application is subject to the Usage Rules set forth in Apple's then-current App Store Terms of Service.</p>
<p>3.	You agree that Apple has no duty or obligation to provide support or maintenance services with respect to our iOS application.</p>
<p>4.	To the maximum extent permitted by applicable law, Apple will have no warranty obligation with respect to our iOS application.</p>
<p>5.	You agree that we, and not Apple, are responsible for addressing any claims by you or any third party relating to our iOS application or your possession and/or use of our iOS application, including, but not limited to: (i) product liability claims; (ii) any claim that the application fails to conform to any applicable legal or regulatory requirement; and (iii) claims arising under consumer protection or similar legislation.</p>
<p>6.	You agree that Apple is not responsible for the investigation, defense, settlement and discharge of any third party intellectual property infringement claim related to our iOS application or your possession and use of our iOS application.</p>
<p>7.	You agree to comply with all applicable third party terms of agreement when using our iOS application, such as the terms of your wireless carrier, where applicable.</p>
<p>8.	The parties agree that Apple and Apple's subsidiaries are third party beneficiaries to the terms of use applicable to our iOS application. Upon your acceptance of the Terms of Service, Apple will have the right (and will be deemed to have accepted the right) to enforce the Agreement against you as a third party beneficiary thereof.</p>

        </div> 
      </div>
    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

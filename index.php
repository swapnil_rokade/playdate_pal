<?php 
/**
 * PLAYDATE - HOME 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");

$welcome = false;
if(isset($_SESSION['welcome']) && ($_SESSION['welcome']==1)) {
    $welcome = true;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="home<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>
    
    <?php 
    if($isSpecialist && $welcome) {
        $requests = PlaydateRequestDAO::getUpcomingOpenPlaydateRequestsJobBoardList(1, 3, "date", "asc");
        if($requests==null) {
        	$requests = array();
        }
        ?>
        <!-- welcome specialist -->
        <div class="modal fade" id="dashboard-welcome" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <h2 class="dashboard-welcome-heading">Welcome to PAL</h2>
                <div class="row">
                  <div class="col-lg-6"><a class="bt-top" href="/specialist-job-board.php" title="">Search Jobs</a></div>
                  <div class="col-lg-6"><a class="bt-top" href="/specialist-create-playdate.php" title="">Create Playdate</a></div>
                </div>
                <div class="row">
                <?php if(count($requests)>0) { ?>
                <h2 class="dashboard-welcome-heading noUppercase">Suggested Jobs in Your Area</h2>
                <?php } ?>
                <?php 
                foreach ($requests as $auxRequest) {
                    $link = "/specialist-submit-itinerary.php?id_request=".$auxRequest->getId_request();
                    
                    $interestName = "";
                    $interestType = "";
                    
                    $interests = InterestDAO::getInterestsByRequest($auxRequest->getId_request());
                    $auxInterest = null;
                    if(count($interests)>0) {
                        $auxInterest = $interests[0];
                        $interestName =$auxInterest->getName();
                        
                        switch ($auxInterest->getType()) {
                            case Interest::$TYPE_TRAINING: {
                                $interestType=" stem";
                                break;
                            }
                            case Interest::$TYPE_ART: {
                                $interestType=" creative";
                                break;
                            }
                            case Interest::$TYPE_PLAY: {
                                $interestType=" outdoor";
                                break;
                            }
                            case Interest::$TYPE_CULTURAL: {
                                $interestType=" attractions";
                                break;
                            }
                        }
                    }
                    
                    $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByRequest($auxRequest->getId_request());
                    $place = "";
                    if($neighborhoods!=null) {
                        $neigh = $neighborhoods[0];
                        $place = $neigh->getName();
                    }
                    
                    $ageMin = 0;
                    $ageMax = 0;
                    $childs = ChildrenDAO::getChildrenListByRequest($auxRequest->getId_request());
                    foreach($childs as $auxChild) {
                        if( ($ageMin==0) || ($auxChild->getAge() <= $ageMin)) {
                            $ageMin =$auxChild->getAge();
                        }
                        if($auxChild->getAge() > $ageMax) {
                            $ageMax =$auxChild->getAge();
                        }
                    }
                    
                    $parent = ParentDAO::getParent($auxRequest->getId_parent());
                    ?>
                  <div class="col-lg-4">
                    <div class="box-playdates outdoor <?php echo $interestType ?>">
                      <div class="time">
                        <span class="date"><?php echo Utils::dateFormat($auxRequest->getDate()) ?></span>
                      	<span class="hour"><?php echo Utils::get12hourFormat($auxRequest->getTime_init()) ?> - <?php echo Utils::get12hourFormat($auxRequest->getTime_end()) ?></span>
                      </div>
                      <div class="info">
                        <span class="title"><a href="<?php echo $link ?>">Parent Request</a></span>
                      	<span class="subtitle"><?php echo $place ?></span>
                      </div>
                       <div class="data">
                        <div  class="age">Ages: <span><?php echo $ageMin ?>-<?php echo $ageMax ?></span></div>
                        <?php /*
                        <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                        <div class="specialists">Specialist: <span>Anna Smith</span></div>
                        */?>
                        <div class="specialists">Parent: <span><a href="/parent-full-profile.php?id_parent=<?php echo $parent->getId_parent() ?>"><?php echo $parent->getFullName() ?></a></span></div>
                      </div>
                      <div class="reserve">
                        <a href="<?php echo $link ?>" class="bt-reserve">View Request</a>
                      </div>
                    </div>
                  </div>
    			<?php 
                } //foreach request
    			
    			?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /welcome specialist -->
    <?php } //if welcome ?>
    

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="site-heading">
              <h1><span>Childcare:</span><span class="reimagined">reimagined</span></h1>
              <span class="subheading">Interactive playdates with expert caregivers.</span>
            </div>
          </div>
          <div class="col-lg-6 cont-slider-home">
            <div class="slider-home">
              <video poster="/video/video-home.jpg" autoplay muted  playsinline preload="metadata" loop tabindex="0">
                <source src="/video/video-home.webm" type='video/webm' />
                <source src="/video/video-home.ogv" type='video/ogg' />
                <source src="/video/video-home.mp4" type='video/mp4' />
              </video>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">



<!-- Featured Playdates -->
      <div class="row featured-playdates">
         <div class="col-lg-12">
          <div class="titulo">
            <h2>Featured Playdates</h2>
            <p>Open Playdates to Book Instantly</p>
          </div>
          <div class="row">

            <?php 
            //$playdates = PlaydateDAO::getPlaydatesList(1, 4, "date", "desc"); 
            $playdates = PlaydateDAO::getUpcomingFeaturedPlaydatesList(1, 4, "date", "desc");
            if(($playdates==null) || (count($playdates)==0)) {
                $playdates = PlaydateDAO::getUpcomingPlaydatesList(1, 4, "date", "desc");
            }
            
            foreach ($playdates as $auxPlaydate) {
				//Code updated by SR 31 Jan 2019
				$addonsPrice = 0;
				if($auxPlaydate->getAdd1_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd1_price()*100;
				}
				if($auxPlaydate->getAdd2_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd2_price()*100;
				}
				if($auxPlaydate->getAdd3_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd3_price()*100;
				}
				if($auxPlaydate->getAdd4_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd4_price()*100;
				}
				// Code End
                $link = "/playdate.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
                
                $auxOpenSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
                $auxPlaydate->setOpen_spots($auxOpenSpots);
                
                $interestName = "";
                $interestType = "";
                
                $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
                $auxInterest = null;
                if(count($interests)>0) {
                    $auxInterest = $interests[0];
                    $interestName =$auxInterest->getName();
                    
                    switch ($auxInterest->getType()) {
                        case Interest::$TYPE_TRAINING: {
                            $interestType=" stem";
                            break;
                        }
                        case Interest::$TYPE_ART: {
                            $interestType=" creative";
                            break;
                        }
                        case Interest::$TYPE_PLAY: {
                            $interestType=" outdoor";
                            break;
                        }
                        case Interest::$TYPE_CULTURAL: {
                            $interestType=" attractions";
                            break;
                        }
                    }
                }
                
                $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
                
                $picPlaydate = "img/default-playdate-image.jpg";
                if(($auxPlaydate->getPicture()!=null) && ($auxPlaydate->getPicture()!="")) {
                    $picPlaydate = $auxPlaydate->getPicture();
                }
            ?>
        
        
          <div class="col-lg-3 col-md-6 col-sm-6 col-6 first">
            <div class="box-playdates first outdoor <?php echo $interestType ?>">
              <?php //if($isConnected) { ?>
              <a href="<?php echo $link ?>" class="img-container">
              <?php //} else { ?>
              <!-- <a data-toggle="modal" data-target="#login" href="#"  class="img-container"> -->
              <?php //} ?>
                <img src="<?php echo $picPlaydate ?>"/ alt="">
                <?php if($auxInterest!=null) { ?>
                  <span class="cat-box"><?php echo $interestName ?></span>
                <?php } ?>
              </a>
              
              <?php if($isConnected) { ?>
              <div class="time">
                <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?>-<?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
              </div>
			  <?php } ?>

              <div class="info">
                <?php //if($isConnected) { ?>
                <a href="<?php echo $link ?>" class="title">
                <?php //} else { ?>
                <!-- <a data-toggle="modal" data-target="#login" href="#" class="title"> -->
                <?php //} ?>
                <?php echo $auxPlaydate->getName() ?></a>
                <?php if($isConnected) { ?>
                <span class="subtitle"><?php echo $auxPlaydate->getLoc_city() ?>, <?php echo $auxPlaydate->getLoc_state() ?></span>
                <?php } ?>
              </div>

              <div class="data">
                <div><span class="price">$<?php echo Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePrice($auxPlaydate->getId_playdate())+$addonsPrice), 0); ?></span> or less | <span class="spots<?php echo($auxPlaydate->getOpen_spots()==1?" destacado":"") ?>"><?php echo $auxPlaydate->getOpen_spots() ?> open spot<?php if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>
              </div>

               <div class="data">
                <div  class="age">Ages: <span><?php echo $auxPlaydate->getAge_init() ?> - <?php echo $auxPlaydate->getAge_end() ?></span></div>
                <?php if($specialist!=null) { ?>
                <div class="specialists">Led by <span><a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                <?php } ?>
              </div>
              <div class="reserve">
                  <?php //if($isConnected) { ?>
                  <a href="<?php echo $link ?>" class="bt-reserve">View Playdate</a>
                <?php //} else { ?>
                  <!-- <a data-toggle="modal" data-target="#login" href="#" class="bt-reserve">View Playdate</a> -->
                <?php //} ?>
              </div>
            </div>
          </div>
          <?php } ?>
          
        </div>
        </div>
        
      </div>

<!-- /Featured playdates -->

<!-- How to -->
    <div class="row row-eq-height how-to">
        <div class="col-lg-6 vbottom">
          <div class="content-title">
            <h2 class="title-in">How to PAL
              <span class="subtitle">Three Easy tools to Coordinate Social Care</span>
            </h2>
            <div class="texto">
              <ul class="list-icon">
                <li class="ico-search">
                  <a href="/help-center.php"><strong>Search</strong></a>
                  <p>Search for a drop off playdate near you facilitated by vetted and highly qualified child care professionals.</p>
                  
                </li>
                 <li class="ico-request">
                  <a href="/help-center.php"><strong>Request</strong></a>
                  <p>Request a one-off playdate or recurring care-share on your schedule.</p>                  
                </li>
                 <li class="ico-build">
                  <a href="/help-center.php"><strong>Build</strong></a>
                  <p>Build a members-only group to coordinate child care based on collective needs.</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-6 vbottom">
          <div class="content-title">
            <h2 class="title-in">On trust and safety
              <span class="subtitle">Our Commitment to High Quality Care</span>
            </h2>
            <div class="texto padding10bottom">
              <ul class="list-icon titles">
                <li class="ico-search">
                  <a href="/about-us.php"><strong>Detailed Background Checks</strong></a>
                </li>
                <li class="ico-safety">
                  <a href="/about-us.php"><strong>First Aid and CPR Trained</strong></a>
                </li>
                <li class="ico-build">
                 <a href="/about-us.php"> <strong>Comprehensive Verification Process</strong></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </div>

<!-- How to -->
    <div class="row row-eq-height how-to affordability">
          <div class="col-lg-12 vtop">
            <div class="content-title">
              <h2 class="title-in">Affordability
                <span class="subtitle">Shared Pricing lowers the cost of care</span>
              </h2>
              <div class="row texto">
                <p><strong>Use the PAL Calculator to check pricing rates.</strong></p>               
              </div>
              <div class="row texto calculator">
                  <div class="col-lg-3 col-md-6">
                      <input type="number" pattern="[2-8]" name="" placeholder="0" id="numKids"/> 
                      <label for="numKids">Kids in Playdate</label>
                      <p class="discount">25% off on 2nd child per family</p>
                  </div>  
                  <div class="col-lg-3 col-md-6">
                      <input type="number"  pattern="[1-9]" name="" placeholder="0" id="hours"/> 
                      <label for="hours">Hours</label>
                  </div>  
                  <div class="col-lg-3 col-md-6">
                      <input type="text" name="" placeholder="$ -" id="baseRate" readonly/> 
                      <label for="baseRate">Hourly Rate</label>
                  </div>  
                   <div class="col-lg-3 col-md-6">
                      <input type="text" name="" placeholder="$ -"  id="youPay" readonly/> 
                      <label for="youPay">You Pay</label>
                  </div>
                  <?php /*?> 
                  <div class="col-lg-12 legal">
                    <p>*$25 per hour flat rate after RSVP date has passed.</p>
                  </div>  
                  */?>             
              </div>
            </div>
          </div>   
      </div>

<!-- Featured Specialists -->
    <div class="row featured-specialists">

		<?php 
		$specialists = SpecialistDAO::getSpecialistsList(1, 4, null, null);
		
		foreach ($specialists as $specialistaux) {
		    $ratingvalue = RatingDAO::getSpecialistValue($specialistaux->getId_specialist());
		    $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialistaux->getId_specialist());
		    $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialistaux->getId_specialist());
		?>
    
        <div class="col-lg-3 col-md-6 col-sm-6 col-6 first">
           <div class="flip">
                <div class="card">
                  <div class="specialist face front">
                    <div class="info">
                       <p><?php echo $specialistaux->getFullName() ?></p>
                       <div class="puntuacion">
                        <ul>
                          <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                        </ul>
                        <span class="num"><?php echo $ratingvalue ?>/5</span>
                      </div>                
                      
                    </div>             
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                  <div class="face back">
                    <div class="info-back">
                      <div class="ico-specialist family"><span><?php echo $numFamilies ?><br>repeat families</span></div>
                      <div class="ico-specialist pd"><span><?php echo $numPlaydates ?><br>playdates</span></div>
                      <p><?php echo $specialistaux->getAbout_me() ?></p>
                      <a class="bt-profile" title="" href="/specialist-profile.php?id_specialist=<?php echo $specialistaux->getId_specialist()?>&slug=<?php echo $specialistaux->getSlug() ?>">View profile</a>
                    </div>            
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                </div>
              </div>
        </div>
		<?php } ?>


        <div class="col-lg-12">
          <div class="titulo">
            <h2>A highly selective community of talented child care professionals</h2>
            <p>Well-paid and meaningful job opportunities in child care and enrichment.</p>
            <a href="/apply-specialist.php" title="" class="bt-titulo">Apply to be a PAL Specialist</a>
          </div>
        </div>
    </div>

    <!-- TESTIMONIALS -->
    <div class="row playdates-testimonials">
        <div class="col-lg-12">
          <div class="titulo">
            <div class="testimonio">
              <p class="texto">The kids loved it! We moved to NY in April and finding kids for them to play with has been not easy. This was exactly what they needed. The caregivers were outstanding. We have had trouble finding someone our kids were comfortable staying with. My husband and I were thrilled that both kids ran off to have fun and didn't even look back. We will definitely be back and will certainly recommend and support your program anyway that we can.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">My husband and I are thrilled. Our son had a great time. We had a great time - guilt free! This is just an all around a win-win situation.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">This is awesome! Your service was unparalleled by anything else we've experienced.</p>
              <p class="name">Maryam, Playdate Partner Host</p>
            </div>
            <div class="testimonio">
              <p class="texto">Project Playdate is a wonderful service. My husband and I were so grateful to have a night out and to know that our son was in good hands. Now how do we get Project Playdate to come to California?!</p>
              <p class="name">Shenna Deveza, Mom of One, Union Square</p>
            </div>
             <ul class="nav-testimonios"></ul>
          </div>

        </div>
    </div>
    <!-- FIN TESTIMONIALS-->


    </div>

    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
    
    <?php 
	if($isLead && $welcome) {
	    $_SESSION['welcome'] = 0;
	?>
		<script>$('#dashboard-welcome').modal('show');</script>
	<?php 
	}
  ?>
  
  

    
  </body>

</html>
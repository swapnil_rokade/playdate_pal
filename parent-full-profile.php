<?php 
/**
 * PLAYDATE - PARENT FULL PROFILE FOR SPECIALIST 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$parent=null;

if(!$isConnected) {
    header("Location: /");
}

$parentParam = new ParentPd();
$parentParam->readFromRow($_REQUEST);
$parentParam = ParentDAO::getParent($parentParam->getId_parent());
 
if($parentParam!=null) {
    $parent = $parentParam;
}

if($parent==null) {
    header("Location: /");
} else if($isParent) {
    //if parent, public profile
    header("Location: /parent-public-profile.php?id_parent=".$parent->getId_parent());
}

$picture = $parent->getPicture();
if(($picture==null) || ($picture=="")) {
    $picture = "img/img-profile.jpg";
}
?><!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Parent Profile - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page submit-itinerary public-profile<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1><?php echo $parent->getFullName() ?></h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
          <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $picture ?>" alt="Picture of <?php echo $parent->getFullName() ?>">
                </div>
              </div>

        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <div class="row">

            

            <div class="col-lg-4">
              <p class="titulo">Profile</p>
              <div class="data">
                <p><strong class="big"><?php echo $parent->getFullName() ?></strong></p>
                <p><?php echo $parent->getUsername() ?></p>
                <p>Chelsea, NY</p>
              </div>
              <div class="data">
                <p>Cellphone: <?php echo $parent->getCellphone() ?></p>
                <p>Work: <?php echo $parent->getPhone() ?></p>
              </div>
              <p class="titulo">Availability</p>
              <div class="data">
				<p><span class="day">Mon</span> <span class="hour"><?php echo(($parent->getChildren_avb_mon_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_init()):"")?> - <?php echo(($parent->getChildren_avb_mon_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_end()):"")?></span></p>
				<p><span class="day">Tue</span> <span class="hour"><?php echo(($parent->getChildren_avb_tue_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_init()):"")?> - <?php echo(($parent->getChildren_avb_tue_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_end()):"")?></span></p>
				<p><span class="day">Wed</span> <span class="hour"><?php echo(($parent->getChildren_avb_wed_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_init()):"")?> - <?php echo(($parent->getChildren_avb_wed_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_end()):"")?></span></p>
				<p><span class="day">Thu</span> <span class="hour"><?php echo(($parent->getChildren_avb_thu_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_init()):"")?> - <?php echo(($parent->getChildren_avb_thu_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_end()):"")?></span></p>
				<p><span class="day">Fri</span> <span class="hour"><?php echo(($parent->getChildren_avb_fri_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_init()):"")?> - <?php echo(($parent->getChildren_avb_fri_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_end()):"")?></span></p>
				<p><span class="day">Sat</span> <span class="hour"><?php echo(($parent->getChildren_avb_sat_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_init()):"")?> - <?php echo(($parent->getChildren_avb_sat_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_end()):"")?></span></p>
				<p><span class="day">Sun</span> <span class="hour"><?php echo(($parent->getChildren_avb_sun_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_init()):"")?> - <?php echo(($parent->getChildren_avb_sun_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_end()):"")?></span></p>
              </div> 
              <p class="titulo">Groups</p>            
              <div class="data">
              	<?php 
              	$groups=GroupDAO::getGroupsByParent($parent->getId_parent());
              	foreach($groups as $auxGroup) {
              	?>
                <p><a style="color:#878787" href="/group-public-profile.php?id_group=<?php echo $auxGroup->getId_group() ?>"><?php echo $auxGroup->getName() ?></a></p>
                <?php } ?>
              </div>            
            </div>

            <div class="col-lg-8">
              <p class="titulo">Children</p>
              <?php 
              $children = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
              foreach($children as $auxChildren) {
              ?>
              <div class="data data-children">                  
                <p class="children full"><img src="img/ico/main/0299-baby2.svg" width="50" > <span class="name"><?php echo $auxChildren->getName() ?></span> | <span class="age"><?php echo $auxChildren->getAge() ?></span>
                  <p class="info"><?php echo $auxChildren->getNotes() ?></p>
                </p>
                <div class="interests">
                  <p><strong>Interests</strong></p>
                  <div class="row">
                    <div class="col-lg-12">
                    <?php 
                    $interests = InterestDAO::getInterestsByChildren($auxChildren->getId_children());
                    foreach ($interests as $auxInterest) {
                        $interestType = "";
                        switch ($auxInterest->getType()) {
                            case Interest::$TYPE_TRAINING: {
                                $interestType=" stem";
                                break;
                            }
                            case Interest::$TYPE_ART: {
                                $interestType=" creative";
                                break;
                            }
                            case Interest::$TYPE_PLAY: {
                                $interestType=" outdoor";
                                break;
                            }
                            case Interest::$TYPE_CULTURAL: {
                                $interestType=" attractions";
                                break;
                            }
                        }
                        
                        
                    ?>
                      <p class="interest-name <?php echo $interestType?>"><?php echo $auxInterest->getName() ?></p>
                    <?php } ?>
                    </div>
                  </div>
                  
                </div>
              </div>
              <?php } //end foreach children ?>
              

              <p class="titulo">Emergency contact & authorized for pickup</p>
              <?php 
              $emergencies = EmergencyDAO::getEmergencyListByParent($parent->getId_parent());
              foreach($emergencies as $auxEmergency) {
              ?>
              <div class="data">
                <p><?php echo $auxEmergency->getName() ?> | <?php echo $auxEmergency->getRelation() ?></p>
                <p><a style="color:#878787" href="mailto:<?php echo $auxEmergency->getMail() ?>"><?php echo $auxEmergency->getMail() ?></a></p>
                <p><?php echo $auxEmergency->getPhone() ?> </p>
              </div>
              <?php } ?>
            </div>

          </div>

        </div>
        <!-- Content -->

      </div>


    </div>

    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

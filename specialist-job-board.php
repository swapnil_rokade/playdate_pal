<?php 
/**
 * PLAYDATE - SPECIALIST JOB BOARD 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");

$backUrl = "/playdates.php";


//Recogemos los criterios de busqueda
$keyword = null;
if(isset($_REQUEST["keyword"]) && ($_REQUEST["keyword"]!="")) {
    $keyword = $_REQUEST["keyword"];
}

$age_1 = null;
if(isset($_REQUEST["age_1"]) && ($_REQUEST["age_1"]!="")) {
    $age_1 = $_REQUEST["age_1"];
}
$age_2 = null;
if(isset($_REQUEST["age_2"]) && ($_REQUEST["age_2"]!="")) {
    $age_2 = $_REQUEST["age_2"]; 
}
$age_3 = null;
if(isset($_REQUEST["age_3"]) && ($_REQUEST["age_3"]!="")) {
    $age_3 = $_REQUEST["age_3"];
}
 
$neighborhoods = null;
if(!empty($_POST['id_neighborhood'])) {
    $neighborhoods = array();
    foreach($_POST['id_neighborhood'] as $auxlocation) {
        $neighborhoods[] = $auxlocation;
    }
}

$date_init=null;
if(isset($_REQUEST["date_init"]) && ($_REQUEST["date_init"]!="")) {
    $date_init = $_REQUEST["date_init"];
}

$date_end=null;
if(isset($_REQUEST["date_end"]) && ($_REQUEST["date_end"]!="")) {
    $date_end = $_REQUEST["date_end"];
}

$time_init=null;
if(isset($_REQUEST["time_init"]) && ($_REQUEST["time_init"]!="")) {
    $time_init = $_REQUEST["time_init"];
}

$time_end=null;
if(isset($_REQUEST["time_end"]) && ($_REQUEST["time_end"]!="")) {
    $time_end = $_REQUEST["time_end"];
}

$budget_init=null;
if(isset($_REQUEST["budget_init"]) && ($_REQUEST["budget_init"]!="") && is_numeric($_REQUEST["budget_init"])) {
    $budget_init = $_REQUEST["budget_init"];
}

$budget_end=null;
if(isset($_REQUEST["budget_end"]) && ($_REQUEST["budget_end"]!="") && is_numeric($_REQUEST["budget_end"])) {
    $budget_end = $_REQUEST["budget_end"];
}


$selectedInterests = null;
if(!empty($_POST['interests'])) {
    $selectedInterests = array();
    foreach($_POST['interests'] as $interest) {
        $selectedInterests[] = $interest;
    }
}

$selectedTypes = null;
if(!empty($_POST['type'])) {
    $selectedTypes = array();
    foreach($_POST['type'] as $auxType) {
        $selectedTypes[] = $auxType;
    }
}

$parents = null;
if(!empty($_POST['id_parent'])) {
    $parents = array();
    foreach($_POST['id_parent'] as $auxparent) {
        $parents[] = $auxparent;
    }
}

if(!$isSpecialist) { //Función solo para especialistas
    header("Location: /");
}
$specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);


$action = null;
if(isset($_REQUEST["action"])) {
    $action = $_REQUEST["action"];
}

$allrequests = PlaydateRequestDAO::getUpcomingOpenPlaydateRequestsJobBoardListForSpecialist($specialist->getId_specialist(), 1, 1000, "date", "asc");
$requests = $allrequests;

//echo "<pre>";print_r($requests);exit;
switch ($action) {
    case "search": {
        //Filter contents with criteria
        if($keyword!=null) {
            $filteredrequests = array();
            foreach($allrequests as $auxRequest) {
                if(strpos(strtolower($auxRequest->getInfo()), strtolower($_REQUEST["keyword"])) !==false) {
                    $filteredrequests[] = $auxRequest;
                } 
            }
            $requests = $filteredrequests;
        }
        
        //Age1
        if(($age_1=="1") || ($age_2=="1") || ($age_3=="1")) {
            $filteredrequests = array();
            if($age_1=="1") {
                foreach($requests as $auxRequest) {
                    $ageMin = 0;
                    $ageMax = 0;
                    $childs = ChildrenDAO::getChildrenListByRequest($auxRequest->getId_request());
                    foreach($childs as $auxChild) {
                        if( ($ageMin==0) || ($auxChild->getAge() <= $ageMin)) {
                            $ageMin =$auxChild->getAge();
                        }
                        if($auxChild->getAge() > $ageMax) {
                            $ageMax =$auxChild->getAge();
                        }
                    }
                    
                    //Tenemos age_min y age_max de la request
                    if(($ageMin>2) && ($ageMax<6)) {
                        $filteredrequests[] = $auxRequest;
                    }
                }
            }
            
            //Age2
            if($age_2=="1") {
                foreach($requests as $auxRequest) {
                    $ageMin = 0;
                    $ageMax = 0;
                    $childs = ChildrenDAO::getChildrenListByRequest($auxRequest->getId_request());
                    foreach($childs as $auxChild) {
                        if( ($ageMin==0) || ($auxChild->getAge() <= $ageMin)) {
                            $ageMin =$auxChild->getAge();
                        }
                        if($auxChild->getAge() > $ageMax) {
                            $ageMax =$auxChild->getAge();
                        }
                    }
                    
                    //Tenemos age_min y age_max de la request
                    if(($ageMin>5) && ($ageMax<9)) {
                        $filteredrequests[] = $auxRequest;
                    }
                }
            }
    
            //Age3
            if($age_3=="1") {
                foreach($requests as $auxRequest) {
                    $ageMin = 0;
                    $ageMax = 0;
                    $childs = ChildrenDAO::getChildrenListByRequest($auxRequest->getId_request());
                    foreach($childs as $auxChild) {
                        if( ($ageMin==0) || ($auxChild->getAge() <= $ageMin)) {
                            $ageMin =$auxChild->getAge();
                        }
                        if($auxChild->getAge() > $ageMax) {
                            $ageMax =$auxChild->getAge();
                        }
                    }
                    
                    //Tenemos age_min y age_max de la request
                    if(($ageMin>8) && ($ageMax<13)) {
                        $filteredrequests[] = $auxRequest;
                    }
                }
            }
            $requests = $filteredrequests;
        }
        
        //Neighborhoods
        if($neighborhoods!=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                $pdNeighList = NeighborhoodDAO::getNeighborhoodsListByRequest($auxRequest->getId_request());
                $included = false;
                foreach ($pdNeighList as $auxNeigh) {
                    if(in_array($auxNeigh->getId_neighborhood(), $neighborhoods)) {
                        $filteredrequests[] = $auxRequest;
                    }
                }
            }
            $requests = $filteredrequests;
        }
        
        //Date Init
        if($date_init !=null) {
            $filteredrequests = array();
            $initTs = strtotime(str_replace('/', '-', $date_init));
            foreach($requests as $auxRequest) {
                $pdTs = strtotime(str_replace('/', '-', $auxRequest->getDate()));
                if($pdTs>$initTs) {
                    $filteredrequests[] = $auxRequest;                    
                } 
            }
            $requests = $filteredrequests;
        }
        
        //Date End
        if($date_end !=null) {
            $filteredrequests = array();
            $endTs = strtotime(str_replace('/', '-', $date_end));
            foreach($requests as $auxRequest) {
                $pdTs = strtotime(str_replace('/', '-', $auxRequest->getDate()));
                if($pdTs<=$endTs) {
                    $filteredrequests[] = $auxRequest;
                }
            }
            $requests = $filteredrequests;
        }
        
        //Time Init
        if($time_init !=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                if($auxRequest->getTime_init()>=$time_init) {
                    $filteredrequests[] = $auxRequest;
                }
            }
            $requests = $filteredrequests;
        }
        
        //Time End
        if($time_end !=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                if($auxRequest->getTime_end()<=$time_end) {
                    $filteredrequests[] = $auxRequest;
                }
            }
            $requests = $filteredrequests;
        }
        
        //Budget Init
        if($budget_init !=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                if($auxRequest->getBudget_init()>=$budget_init) {
                    $filteredrequests[] = $auxRequest;
                }
            }
            $requests = $filteredrequests;
        }
        
        //Budget End
        if($budget_end !=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                if($auxRequest->getBudget_end()<$budget_end) {
                    $filteredrequests[] = $auxRequest;
                }
            }
            $requests = $filteredrequests;
        }
        
        
        
        //Interests
        if($selectedInterests!=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                $pdIntList = InterestDAO::getInterestsByRequest($auxRequest->getId_request());
                $included = false;
                foreach ($pdIntList as $auxInt) {
                    if(in_array($auxInt->getId_interest(), $selectedInterests)) {
                        $filteredrequests[] = $auxRequest;
                    }
                }
            }
            $requests = $filteredrequests;
        }
        
        //Type of care
        if($selectedTypes!=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                $included = false;
                if(in_array($auxRequest->getType(), $selectedTypes)) {
                    $filteredrequests[] = $auxRequest;
                }
                
            }
            $requests = $filteredrequests;
        }
        
        //Parents
        if($parents!=null) {
            $filteredrequests = array();
            foreach($requests as $auxRequest) {
                $included = false;
                if(in_array($auxRequest->getId_parent(), $parents)) {
                    $filteredrequests[] = $auxRequest;
                }
            }
            $requests = $filteredrequests;
        }
        break;
    }
}

?>
<!DOCTYPE html>
<html lang="en">

  <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Specialist Job Board - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Job Board</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar">
		  <form name="frm_search" action="/specialist-job-board.php" method="post">
		  	<input type="hidden" name="action" value="search" />
          <!-- Search -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#search-container" role="button" aria-expanded="false" aria-controls="search-container">Search</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="search-container">
                <div class="search-sidebar">
                  <label for="inputSearch" class="sr-only">Search</label>
                  <input type="text" id="inputSearch" class="form-control" placeholder="keyword" name="keyword" value="<?php echo(($keyword!=null)?$keyword:"") ?>">
                </div>
              </div>
            </div>
          </div>

          <!-- Age -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#age-container" role="button" aria-expanded="false" aria-controls="age-container">Age</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="age-container">
                <div class="age-sidebar">  
                  <div class="form-group">
                    <input type="checkbox" id="10" class="form-control" placeholder="keyword" name="age_1" value="1"<?php echo (($age_1=="1")?"checked=\"checked\"":"")?>>
                    <label for="age_1">2.5-5</label>
                  </div> 
                  <div class="form-group">
                    <input type="checkbox" id="11" class="form-control" placeholder="keyword" name="age_2" value="1"<?php echo (($age_2=="1")?"checked=\"checked\"":"")?>>
                    <label for="age_2">6-8</label>
                  </div> 
                  <div class="form-group">
                    <input type="checkbox" id="12" class="form-control" placeholder="keyword" name="age_3" value="1"<?php echo (($age_3=="1")?"checked=\"checked\"":"")?>>
                    <label for="age_3">9-12</label>
                  </div> 
                </div>
              </div>
            </div>
          </div>

          <!-- Location -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#location-container" role="button" aria-expanded="false" aria-controls="location-container">Location</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="location-container">
                <div class="location-sidebar">
                  <label for="inputLocation" class="sr-only">Location</label>
                  <input type="text" id="inputLocation" class="form-control inputLocation" placeholder="neighborhood">
                  <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                </div>
                <div class="container-tags">
                  <?php 
                  if(($neighborhoods!=null) && (count($neighborhoods)>0)) {
                      foreach ($neighborhoods as $auxNeigh) {
                          $nei = NeighborhoodDAO::getNeighborhood($auxNeigh);
                          if($nei!=null) {
                  ?>
					<div class="tag">
                    	<span><?php echo $nei->getName()?></span>
                        <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                        <input type="hidden" value="<?php echo $nei->getId_neighborhood() ?>" name="id_neighborhood[]">
                    </div>
                  <?php 
                          }
                      }
                  }
                  ?>
                </div>
                <p class="legal">*If we are not yet near you <a href="/contact-us.php" title="Nominate your city">nominate</a> your city</p>
              </div>
            </div>
          </div>

          <!-- Date -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#date-container" role="button" aria-expanded="false" aria-controls="date-container">Date Range</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="date-container">
                <div class="date-sidebar">
                  <div class='input-group date' >
                    <input type='text' placeholder="DD/MM/YY" class="form-control  datepicker" name="date_init" value="<?php echo (($date_init!=null)?$date_init:"")?>"/>
                  </div>
                  <span> - </span>
                  <div class='input-group date' >
                    <input type='text' placeholder="DD/MM/YY" class="form-control  datepicker" name="date_end"  value="<?php echo (($date_end!=null)?$date_end:"")?>" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Time -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#time-container" role="button" aria-expanded="false" aria-controls="time-container">Time</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="time-container">
                <div class="time-sidebar">
                  <div class="input-group" style="padding-top:25px">
                  	<div class="selectTime from">
                         <?php 
                          $fieldName = "time_init";
                          $fieldValue = $time_init;
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                    </div>
                  </div>
                  <span> - </span>
                  <div class="input-group" style="padding-top:25px">
                  	<div class="selectTime from">
                         <?php 
                          $fieldName = "time_end";
                          $fieldValue = $time_end;
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Activity budget -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#budget-container" role="button" aria-expanded="false" aria-controls="budget-container">Budget</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="budget-container">
                <div class="budget-sidebar">
                  <div class='input-group' >
                    <input type='text' placeholder="$ -" class="form-control  input-budget" name="budget_init" <?php echo (($budget_init!=null)?"value=\"".$budget_init."\"":"") ?>/>
                  </div>
                  <span> - </span>
                  <div class='input-group' >
                    <input type='text' placeholder="$ -" class="form-control  input-budget" name="budget_end" <?php echo (($budget_end!=null)?"value=\"".$budget_end."\"":"") ?>/>
                  </div>
                </div>
              </div>
            </div>
          </div>

           <!-- Categories -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#categories-container" role="button" aria-expanded="false" aria-controls="categories-container">Categories</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="categories-container">
              	<?php 
              	if($selectedInterests==null) {
              	    //Para evitar error si no hay filtro
              	    $selectedInterests=array();
              	} 
              	?>
                <div class="categories-sidebar">
                  <p><strong>INTERESTS</strong></p>
                  <div class="list-container stem">
                    <div class="form-group">
                      <input type="checkbox" id="int_1" class="form-control" placeholder="keyword" name="interests[]" value="1" <?php echo((in_array(1, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="0">Academic</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_2" class="form-control" placeholder="keyword" name="interests[]" value="2" <?php echo((in_array(2, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="1">STEM</label>
                    </div> 
                  </div>
                  
                  <div class="list-container creative">
                    <div class="form-group">
                      <input type="checkbox" id="int_3" class="form-control" placeholder="keyword" name="interests[]" value="3" <?php echo((in_array(3, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="2">Creative</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_4" class="form-control" placeholder="keyword" name="interests[]" value="4" <?php echo((in_array(4, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="3">Performing Arts</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_5" class="form-control" placeholder="keyword" name="interests[]" value="5" <?php echo((in_array(5, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="4">Language & Culture</label>
                    </div> 
                  </div>

                  <div class="list-container outdoor">
                    <div class="form-group">
                      <input type="checkbox" id="int_6" class="form-control" placeholder="keyword"  name="interests[]" value="6" <?php echo((in_array(6, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="5">Play</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_7" class="form-control" placeholder="keyword" name="interests[]" value="7" <?php echo((in_array(7, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="6">Sports & Recreation</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_8" class="form-control" placeholder="keyword" name="interests[]" value="8" <?php echo((in_array(8, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="7">Health & Wellness</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_9" class="form-control" placeholder="keyword" name="interests[]" value="9" <?php echo((in_array(9, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="8">Outdoor</label>
                    </div>
                  </div>

                  <div class="list-container attractions">
                    <div class="form-group">
                      <input type="checkbox" id="int_10" class="form-control" placeholder="keyword" name="interests[]" value="10" <?php echo((in_array(10, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="9">NYC Museums</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_11" class="form-control" placeholder="keyword" name="interests[]" value="11" <?php echo((in_array(11, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="10">NYC Attractions</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="int_12" class="form-control" placeholder="keyword" name="interests[]" value="12" <?php echo((in_array(12, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="11">NYC Theater</label>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Type of Care -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#care-container" role="button" aria-expanded="false" aria-controls="care-container">Type of Care</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="care-container">
                <div class="care-sidebar">
                <?php 
                  $types = TypecareDAO::getTypecareList();
                  if($selectedTypes==null) {
                      //Para evitar error si no hay filtro
                      $selectedTypes=array();
                  }
                
                  foreach ($types as $auxType) {
                      $selected = in_array($auxType->getId_typecare(), $selectedTypes);
                      
                  ?>
              	  <div class="form-group">
                    <input type="checkbox" name="type[]" id="type_<?php echo $auxType->getId_typecare()?>" value="<?php echo $auxType->getId_typecare()?>" class="form-control" placeholder="" <?php echo(($selected)?"checked=\"checked\"":"") ?>>
                    <label for="<?php echo $auxType->getId_typecare()?>"><?php echo $auxType->getName() ?> 
                    	<?php if($auxType->getDescription()!=null) { ?>
                    	<a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="<?php echo $auxType->getDescription() ?>"><i class="fa fa-question-circle"></i></a>
                    	<?php } ?>
                    </label>
                  </div> 
              	  <?php 
                  }
              	  ?>
                </div>
              </div>
            </div>
          </div>

		  <?php /*?>
          <!-- Parents -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#specialists-container" role="button" aria-expanded="false" aria-controls="specialists-container">Parents</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="specialists-container">
                <div class="search-sidebar">
                  <label for="inputParents" class="sr-only">Parents</label>
                  <input type="text" id="inputParents" class="form-control inputParent" placeholder="name">
                  <a href="javascript:;" class="bt-save-profile last bt-add-parent" data-toggle="modal" data-target="#playdate-reserved">Add Parent</a>
                </div>
                <div class="container-parent-tags">
                  <!-- automated load parents  -->
                  <?php 
                  if(($parents!=null) && (count($parents)>0)) {
                      foreach ($parents as $auxPar) {
                          $auxParent = ParentDAO::getParent($auxPar);
                          if($auxParent!=null) {
                  ?>
                  <div class="specialist-tag">
                    <img src="<?php echo $auxParent->getPicture(); ?>"/>
                    <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent(); ?>" data-idSpecialist="1" title=""><?php echo $auxParent->getFullName(); ?></a>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                    <input type="hidden" name="id_parent[]" value='<?php echo $auxParent->getId_parent(); ?>'/>
                  </div>
                  <?php 
                          }
                      }
                  }
                  ?>
                  
                </div>
              </div>
            </div>
          </div>
          <!-- /Parents -->
          */?>

          <!-- Open Spots -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#spots-container" role="button" aria-expanded="false" aria-controls="spots-container">Open Spots</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="spots-container">
                <div class="spots-sidebar">
                  <div class="selectdiv">
                    <label for="inputSpots">
                      <select class="form-control" id="inputSpots" name="openspots">
                      	<option>--</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col">
            	<input type="submit" class="bt-save-profile last" value="Apply Filter">
            </div>
          </div>
          <div class="row">
            <div class="col">
            	<input type="button" class="bt-save-profile last" value="Clear Filter" onclick="top.location='/playdates.php'">
            </div>
          </div>


		</form>
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <div class="row">
          
          	<?php 
          	foreach ($requests as $auxRequest) {
          	    $link = "/specialist-submit-itinerary.php?id_request=".$auxRequest->getId_request();
          	    
          	    $interestName = "";
          	    $interestType = "";
          	    
          	    $interests = InterestDAO::getInterestsByRequest($auxRequest->getId_request());
          	    $auxInterest = null;
          	    if(count($interests)>0) {
          	        $auxInterest = $interests[0];
          	        $interestName =$auxInterest->getName();
          	        
          	        switch ($auxInterest->getType()) {
          	            case Interest::$TYPE_TRAINING: {
          	                $interestType=" stem";
          	                break;
          	            }
          	            case Interest::$TYPE_ART: {
          	                $interestType=" creative";
          	                break;
          	            }
          	            case Interest::$TYPE_PLAY: {
          	                $interestType=" outdoor";
          	                break;
          	            }
          	            case Interest::$TYPE_CULTURAL: {
          	                $interestType=" attractions";
          	                break;
          	            }
          	        }
          	    }
          	    
          	    $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByRequest($auxRequest->getId_request());
          	    $place = "";
          	    if($neighborhoods!=null) {
          	        $neigh = $neighborhoods[0];
          	        $place = $neigh->getName();
          	    }
          	    
          	    $ageMin = 0;
          	    $ageMax = 0;
          	    $childs = ChildrenDAO::getChildrenListByRequest($auxRequest->getId_request());
          	    foreach($childs as $auxChild) {
          	        if( ($ageMin==0) || ($auxChild->getAge() <= $ageMin)) {
          	            $ageMin =$auxChild->getAge();
          	        }
          	        if($auxChild->getAge() > $ageMax) {
          	            $ageMax =$auxChild->getAge();
          	        }
          	    }
          	    
          	    $parent = ParentDAO::getParent($auxRequest->getId_parent());
          	?>
            
            <div class="col-lg-4">
              <div class="box-playdates <?php echo $interestType ?>">
                <div class="time" style="height: 25px;!important">
                  <span class="date"><?php echo Utils::dateFormat($auxRequest->getDate()) ?></span>
                  <span class="hour"><?php echo Utils::get12hourFormat($auxRequest->getTime_init()) ?> - <?php echo Utils::get12hourFormat($auxRequest->getTime_end()) ?></span>
                </div>
                <div class="info" style="height: 40px;!important">
                  <span class="title"><a href="<?php echo $link ?>">Parent Request</a></span>
                  <span class="subtitle"><?php echo $place ?></span>
                </div>
                 <div class="data">
                  <div  class="age">Ages: <span>
                  <?php //echo $ageMin -?><?php //echo $ageMax ?>
                  <?php 
									$arr = explode(",",$auxRequest->getAge_range());
									if(!empty($arr)){
									    foreach($arr as $k=>$v){
									        if($v == 1)
									        $arr[$k] = "Under 3";
									         if($v == 2)
									        $arr[$k] = "3 - 5";
									         if($v == 3)
									        $arr[$k] = "5 - 7";
									         if($v == 4)
									        $arr[$k] = "7 or up";
									    }
									}
									$arr = implode (" & ", $arr);
							        echo $arr;
                                    ?>
                  </span></div>
                  <?php /* <div class="participants">Participants: <span>5 / 1 open spot</span></div> */ ?>
                  <?php /* ?>
                  <div class="specialists">Specialist: <span>Anna Smith</span></div>
                  <?*/?>
                  <div class="specialists">Parent: <span><a href="/parent-full-profile.php?id_parent=<?php echo $parent->getId_parent() ?>"><?php echo $parent->getFullName() ?></a></span></div>
                  <div class="specialists" style="margin: 10px 0px;">Playdate Type : <span><?php 
                  if($auxRequest->getPlaydate_type() == 1){
                    $typeName = "Oneoff Playdate";
                  }
                  else{
                        $typeName = "Recurring Playdate";
                  }
                  echo $typeName;
                 ?></span></div>
                 <div class="specialists">Theme of Playdate: 
                <span>
                <?php $tarr = explode(",",$auxRequest->getTheme_of_playdate());
				if(!empty($tarr)){
				   foreach($tarr as $k=>$v){
				    if($v == 1)
				      $tarr[$k] = "Anything fun!";
					if($v == 2)
					  $tarr[$k] = "Academic";
					if($v == 3)
					  $tarr[$k] = "Creative";
					if($v == 4)
					  $tarr[$k] = "Active";
					if($v == 5)
					  $tarr[$k] = "Parent Social";
					}
			    }
				$tarr = implode (" & ", $tarr);
				echo $tarr;
				?>
                </span></div>
                </div>
                
                <div class="reserve" style="height: 40px;!important">
                  <a href="<?php echo $link ?>" class="bt-reserve">View Request</a>
                </div>
              </div>
            </div>
          <?php } ?>
          
          
          <?php 
          $playdatesJobBoard = PlaydateDAO::getUpcomingPlaydatesFindingSupportersListBySpecialist($specialist->getId_specialist(), 1, 100, null, null);
          
          $playdates = array();
          
          //Actualizamos sus open Spots y edades
          foreach ($playdatesJobBoard as $auxPlaydate) {
              $auxOpenSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
              $auxPlaydate->setOpen_spots($auxOpenSpots);
              
              //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
              if(($auxPlaydate->getId_request()!=null) && is_numeric($auxPlaydate->getId_request())) {
                  $minAgePd = 100;
                  $maxAgePd = -1;
                  $reservations = PlaydateDAO::getChildrenReservations($auxPlaydate->getId_playdate());
                  foreach ($reservations as $auxReserv) {
                      if($auxReserv->getAge()>$maxAgePd) {
                          $maxAgePd = $auxReserv->getAge();
                      }
                      
                      if($auxReserv->getAge()<$minAgePd) {
                          $minAgePd = $auxReserv->getAge();
                      }
                  }
                  
                  
                  if(($maxAgePd>=0) && ($minAgePd<100)) {
                      $auxPlaydate->setAge_init($minAgePd);
                      $auxPlaydate->setAge_end($maxAgePd);
                  }
                  
              }
              
              
              
              $playdates[] = $auxPlaydate;
          }
          
          
          foreach ($playdates as $auxPlaydate) {
              $link = "/playdate.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
              
              $interestName = "";
              $interestType = "";
              
              $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
              $auxInterest = null;
              if(count($interests)>0) {
                  $auxInterest = $interests[0];
                  $interestName =$auxInterest->getName();
                  
                  switch ($auxInterest->getType()) {
                      case Interest::$TYPE_TRAINING: {
                          $interestType=" stem";
                          break;
                      }
                      case Interest::$TYPE_ART: {
                          $interestType=" creative";
                          break;
                      }
                      case Interest::$TYPE_PLAY: {
                          $interestType=" outdoor";
                          break;
                      }
                      case Interest::$TYPE_CULTURAL: {
                          $interestType=" attractions";
                          break;
                      }
                  }
              }
              
              $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
              
              $picPlaydate = "img/default-playdate-image.jpg";
              if(($auxPlaydate->getPicture()!=null) && ($auxPlaydate->getPicture()!="")) {
                  $picPlaydate = $auxPlaydate->getPicture();
              }
              ?>
            
            <div class="col-lg-4">
              <div class="box-playdates <?php echo $interestType ?>">
                <?php if($isConnected) { ?>
              	<a href="<?php echo $link ?>" class="img-container">
              	<?php } else { ?>
              	<a data-toggle="modal" data-target="#login" href="#"  class="img-container">
              	<?php } ?>
                  <img src="<?php echo $picPlaydate ?>" alt="" />
                  <?php if($auxInterest!=null) { ?>
                  <span class="cat-box"><?php echo $interestName ?></span>
                  <?php } ?>
                </a>
                <div class="time" style="height: 25px;!important">
                  <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                  <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
                </div>
                <div class="info" style="height: 40px;!important">
                  <?php if($isConnected) { ?>
                	<a href="<?php echo $link ?>" class="title">
                	<?php } else { ?>
                	<a data-toggle="modal" data-target="#login" href="#" class="title">
                	<?php } ?>
                	<?php echo $auxPlaydate->getName() ?>
                	</a>
                  <span class="subtitle"><?php echo $auxPlaydate->getLoc_city() ?>, <?php echo $auxPlaydate->getLoc_state() ?></span>
                </div>

                 <div class="data" style="height: 55px;!important">
                  <div><span class="price">$<?php echo Utils::moneyFormat(PlaydatePriceDAO::getPlaydatePrice($auxPlaydate->getId_playdate()), 0); ?></span> or less | <span class="spots<?php echo($auxPlaydate->getOpen_spots()==1?" destacado":"") ?>"><?php echo $auxPlaydate->getOpen_spots() ?> open spot<?php if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>
                </div>

                 <div class="data">
                  <div  class="age">Ages: <span><?php echo $auxPlaydate->getAge_init() ?> - <?php echo $auxPlaydate->getAge_end() ?></span></div>
                  <?php if($specialist!=null) { ?>
                  <div class="specialists">Led by <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><span><?php echo $specialist->getFullName() ?></span></a></div>
                  <?php } ?>
                </div>
                <div class="reserve" style="height: 40px;!important">
                	<?php if($isParent || $isSpecialist) { ?>
            			<a href="<?php echo $link ?>" class="bt-reserve">Seeking Supporter</a>
            		<?php } else { ?>
            			<a data-toggle="modal" data-target="#login" href="#" class="bt-reserve">Seeking Supporter</a>
            		<?php } ?>
                </div>
              </div>
            </div>
          
          <?php } ?>
          
          </div>
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->


  </body>

</html>

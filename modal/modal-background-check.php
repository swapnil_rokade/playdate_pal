<?php 
/**
 * PLAYATES - Modal Background Check
 */
?>
<!-- Modal Background Check -->
    <div class="modal fade itinerary-modal" id="modal-legal" class="itinerary-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="itinerary-modal-heading">BACKGROUND CHECK</h2>
            
            <div class="row row-tabs">
              <div class="col-lg-12" style="font-size:0.8em">
                <p>International Background Checks have limitations. Please read below to better understand what our international background check screens for:</p>
				<p>Criminal record search of country(ies) and/or territory(ies) based on the last 5-7 years of candidate's self-disclosed address history, excluding U.S. and Canada. Sources may include local, state or federal courts, police records, or other sources of criminal record information. Reported records are limited to pending and conviction cases for indictable and summary offenses (or other equivalent to US felony and misdemeanor records) during the period of residence disclosed by the candidate. Report may include case type, file date, file location, case number, charge information, disposition information, and sentencing information, subject to availability and applicable reporting limitations. Major coverage includes: Latin America and the Caribbean (LAC); Europe, Middle East, and Africa (EMEA); and Asia Pacific (APAC).</p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
<!-- /Modal Background Check -->

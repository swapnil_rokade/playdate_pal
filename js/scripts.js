var numTestimonio=1;
var totalTestimonio=$(".testimonio").length;
$(window).scroll(function(){
  if($(window).scrollTop()!=0){
    $(".navbar").addClass("is-fixed")
  }else{
  	$(".navbar").removeClass("is-fixed")
  }
  $(".admin-slider.front .all-slides").css("height",$(".admin-slider.front .all-slides .slide img").css("height"))
})

$(".bt-travel").on("click",function(){
  $(".bt-travel").not(this).removeClass("selected")  
  $(this).toggleClass("selected")
  $(".bt-travel").each(function(){
    if($(this).hasClass("selected")){
      $("input#travel").val($(this).attr("data-value")) 
    }else{
      $("input#travel").val("")
    }
  })
})
selectedChildren=0;
$("#frm_request_playdate .children input[type='checkbox']").each(function(e){
  if($(this).is(':checked')) {
    selectedChildren++;
  }
})
$("#frm_request_playdate .children input[type='checkbox']").on('change', function() {
    if($(this).is(':checked')) {
      selectedChildren++;
    }else{
      selectedChildren--;
    }
});

$('#frm_request_playdate').on('submit', function(event) {
    event.preventDefault(); 
   if(selectedChildren>0){
    $(this)[0].submit();
   }else{
    $(".errorLocation").remove();
    $("#frm_request_playdate button[type='submit']").after("<p class='errorLocation centered'>You must select at least one child to continue.</p>");
   }
});

selectedChildrenRequest=0;
$("#frm_change_request .children input[type='checkbox']").each(function(e){
  if($(this).is(':checked')) {
    selectedChildrenRequest++;
  }
})
$("#frm_change_request .children input[type='checkbox']").on('change', function() {
    if($(this).is(':checked')) {
      selectedChildrenRequest++;
    }else{
      selectedChildrenRequest--;
    }
});

$('#frm_change_request').on('submit', function(event) {
    event.preventDefault(); 
   if(selectedChildrenRequest>0){
    $(this)[0].submit();
   }else{
    $(".errorLocation").remove();
    $("#frm_change_request button[type='submit']").after("<p class='errorLocation centered'>You must select at least one child to continue.</p>");
   }
});
selectedChildrenReserve=0;
$("#frm_reserve .kids-playdates .kids input[type='checkbox']").each(function(e){
  if($(this).is(':checked')) {
    selectedChildrenReserve++;
  }
})
$("#frm_reserve .kids-playdates .kids input[type='checkbox']").on('change', function() {
    if($(this).is(':checked')) {
      selectedChildrenReserve++;
    }else{
      selectedChildrenReserve--;
    }
});

$('#frm_reserve').on('submit', function(event) {
    event.preventDefault(); 
   if(selectedChildrenReserve>0){
      $(this)[0].submit();
    
   }else{
    $(".errorLocation").remove();
    $("#frm_reserve input[type='submit']").after("<p class='errorLocation centered'>You must select at least one child to continue.</p>");
   }
});

var position = $(window).scrollTop(); 
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if($(window).width()>1199){
      if(scroll > position) {
          $(".navbar").addClass("is-fixed-down");
          $(".navbar").removeClass("is-fixed");
      } else {
          $(".navbar").addClass("is-fixed");
          $(".navbar").removeClass("is-fixed-down");
      }
      position = scroll;
      $(".navbar-brand img").attr("src","/img/logo-color-playdate.png");
      if($(window).scrollTop()==0){
        $(".navbar").removeClass("is-fixed-down");
        $(".navbar").removeClass("is-fixed");
        $(".navbar-brand img").attr("src","/img/logo-playdate.png");
      }
    }
});


$(".bt-get-started").on("click", function(){
  $(".create-step1").hide()
  $(".create-group").fadeIn()
})

$(document).on("click", ".row.row-child-detail .bt-add-kid-detail",function(){
  
  $(this).parent().parent().after('<div class="row row-child-detail"><div class="col-lg-5"> <div class="selectdiv"> <label for="inputKids"> <select class="form-control" id="inputKids" name="num_kids[]"><option value="">Kids</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></label></div></div><div class="col-lg-5"><div class="selectdiv"><label for="inputKids"><select class="form-control" id="inputKids" name="ages[]"><option value="">Age</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option> <option value="5">5</option> <option value="6">6</option> <option value="7">7</option> <option value="8">8</option> </select> </label></div></div><div class="col-lg-2"><a href="javascript:;" class="bt-add-kid-detail"><img src="img/ico/main/0860-plus-circle.svg"></a></div></div>');
  $(this).hide()
})
$(document).on("click", ".bt-add-kid",function(){
  $(this).parent().parent().clone().appendTo(".kids-playdates");
  $(this).hide()
})


$(".modal .close.bt-current-playdates").on("click",function(){
  $(location).attr('href',"/my-dashboard.php");
})

$(document).ready(function(){
 if($(window).scrollTop()!=0 && $(window).width()>1199){
    $(".navbar").addClass("is-fixed");
    $(".navbar-brand img").attr("src","/img/logo-color-playdate.png");
  }

  $(".image-selected").val($(".admin-slider .all-slides .slide.selected img").attr("src"))
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  
  $(".bt-sidebar").on("click", function(){
  	$(this).toggleClass("open");
  })
  $('[data-toggle="tooltip"]').tooltip({
    container: 'body',
    trigger : 'hover'
  });

$(".contact-us-page .bt-profile").on("mouseout", function(){
  $(".ui-helper-hidden-accessible div").css("display","none")
})

  $('.datepicker, .input-time, .input-budget, .input-age, .input-walk, .input-train, .input-car, .input-taxi ').on("blur",function(){
  	if($(this).val()!=""){
  		$(this).addClass("filled")
  	}else{
  		$(this).removeClass("filled")
  	}
  })

  $('.datepicker').datepicker({
    defaultDate: '+3d',
    minDate: '+3d',
    dateFormat: 'mm/dd/yy',
    onSelect: function(date) { 
      if(date !=""){
        $(this).addClass("filled")
      }else{
        $(this).removeClass("filled")
      }
    } 
  });
  $('.datepicker2').datepicker({
    dateFormat: 'mm/dd/yy'
  });


  $(".content-title .texto .links ul li a").on("click", function(){
  	$(".content-title .texto .links ul li a").removeClass("selected");
  	$(this).addClass("selected");
  	content = $(this).attr("data-target");
  	$(".content-title .texto .text").fadeOut(500);
  	$("#"+content).fadeIn(500);
  })

  $(document).on("click", ".bt-delete-tag",function (e) {
  	$(this).parent().fadeOut(500).remove();
  })

  if($("body").hasClass("last-playdate-page")){
    $("#last-playdate").modal('show');
  }

  if($("body").hasClass("my-dashboard")){
    if($(location).attr("href").indexOf("profile")==-1){
     $("#dashboard-welcome").modal('show');
    }
  }
  if($("body").hasClass("specialist-dashboard")){
    $("#dashboard-welcome").modal('show');
  }

  $("#last-playdate .puntuacion ul li, .rate-playdate .puntuacion.rate ul li").on("click",function(){
    $(this).siblings().removeClass("active")
    $(this).prevAll().addClass("active");
    $(this).addClass("active");
    $(this).parent().parent().find(".num").html(($(this).attr("id").charAt(4))+"/5");
    $('input#vote').val(($(this).attr("id").charAt(4)))
  })




  $(document).on("click", ".selectGenre .child", function(){
    if( $(this).parent().hasClass("opened")){
      $(this).parent().find(".child").removeClass("selected")
      $(this).addClass("selected")
      $(this).parent().find("#inputSelectGenre").val($(this).attr("data-genre"));
    }

    $(this).parent().toggleClass("opened")
  })

  $(".selectTime a").on("click", function(){
    $(this).parent().find("a.selected").html($(this).html())
    $(this).parent().find("a.selected").attr("data-time", $(this).attr("data-time"))
    $(this).parent().find(".inputSelectTime").val($(this).attr("data-time"));
    $(this).parent().removeClass("opened")
  })

  $(".selectTime a.selected").on("click", function(){
    $(this).parent().toggleClass("opened")
  })



  if($("#playpack-history").length>1){
    $("#playpack-history, #playpack-credit").DataTable({searching: false, paging: false,bFilter: false, bInfo: false});;
  }

  $(document).on("click",".admin-slider .pagination ul li a",function (e) {
    $(".admin-slider .pagination ul li a").removeClass("selected");
    $(".admin-slider .all-slides .slide").removeClass("selected");
    //numSlide=parseFloat($(this).parent().index())+1;
    numSlide=parseFloat($(this).parent().index())+2;
    $(".admin-slider .all-slides .slide:nth-child("+numSlide+")").addClass("selected")
    $(".image-selected").val($(".admin-slider .all-slides .slide:nth-child("+numSlide+") img").attr("src"))
    $(this).addClass("selected");
  })

  numSliderFront=1;
  $(".admin-slider.front .bt-slider-left").on("click",function (e) {
    totalSlider=$(".admin-slider.front .slide").length
    if(numSliderFront==1){
      numSliderFront=totalSlider;
    }else{
      numSliderFront--;
    }
    $(".admin-slider.front .all-slides .slide").removeClass("selected")
    $(".admin-slider.front .all-slides .slide:nth-child("+numSliderFront+")").addClass("selected")
    $(".image-selected").val($(".admin-slider.front .all-slides .slide:nth-child("+numSliderFront+") img").attr("src"))
  })
  $(".admin-slider.front .bt-slider-right").on("click",function (e) {
    totalSlider=$(".admin-slider.front .slide").length
    if(numSliderFront==totalSlider){
      numSliderFront=1;
    }else{
      numSliderFront++;
    }
    $(".admin-slider.front .all-slides .slide").removeClass("selected")
    $(".admin-slider.front .all-slides .slide:nth-child("+numSliderFront+")").addClass("selected")
  })

  $(".admin-slider.front .all-slides").css("height",$(".admin-slider.front .all-slides .slide img").css("height"))

if($(location).attr("href").indexOf("current")!=-1){
	    $("#nav-current-tab").tab('show');
}
  if($(location).attr("href").indexOf("requests")!=-1){
	    $("#nav-request-tab").tab('show');
}
if($(location).attr("href").indexOf("past")!=-1){
	    $("#nav-past-tab").tab('show');
}
if($(location).attr("href").indexOf("groups")!=-1){
    $("#nav-groups-tab").tab('show');
}
if($(location).attr("href").indexOf("specialists")!=-1){
    $("#nav-specialists-tab").tab('show');
}
if($(location).attr("href").indexOf("parents")!=-1){
    $("#nav-parents-tab").tab('show');
}
if($(location).attr("href").indexOf("profile")!=-1){
    $("#nav-profile-tab").tab('show');
}
if($(location).attr("href").indexOf("members")!=-1){
    $("#nav-members-tab").tab('show');
}
if($(location).attr("href").indexOf("playpacks")!=-1){
    $("#nav-playpacks-tab").tab('show');
}
if($(location).attr("href").indexOf("#media")!=-1){
    $("#nav-media-tab").tab('show');
}
  
  
  
  if($(".testimonio").length>0){
    i=1;
    $(".testimonio").each(function(e){
      if(i==1){
        $(".nav-testimonios").append('<li class="btn active" id="'+i+'"></li>')
      }else{
        $(".nav-testimonios").append('<li class="btn" id="'+i+'"></li>')
      }      
      $(this).attr("id",i);
      i++;
    })
     $(".playdates-testimonials .nav-testimonios li#1").addClass("active")
  }
  $(".playdates-testimonials .nav-testimonios li").on("click",function(){
    $(".playdates-testimonials .nav-testimonios li").removeClass("active")
     $(this).addClass("active")
     $(".testimonio").fadeOut();
     $(".testimonio#"+$(this).attr("id")).fadeIn();
     numTestimonio=$(this).attr("id");
  })
  $(".content-title h2.host").on("click",function(){
    $(".contact-form.apply").addClass("host")
    $(".content-title h2.host").removeClass("inactive")
    $(".content-title h2.specialist").addClass("inactive")
    $(".form-host").show()
    $(".form-specialist").hide()
  })
   $(".content-title h2.specialist").on("click",function(){
    $(".contact-form.apply").removeClass("host")
    $(".content-title h2.specialist").removeClass("inactive")
    $(".content-title h2.host").addClass("inactive")
    $(".form-specialist").show()
    $(".form-host").hide()
  })


})  


function testimonialAuto(){
  $(".playdates-testimonials .nav-testimonios li").removeClass("active")
  $(".testimonio#"+numTestimonio).fadeOut();
  if(numTestimonio==totalTestimonio){
    numTestimonio=1;
  }else{
    numTestimonio++;
  }
  $(".testimonio#"+numTestimonio).fadeIn();
  $(".playdates-testimonials .nav-testimonios li#"+numTestimonio).addClass("active")
  clearTimeout(testimonioTime);
  testimonioTime = setTimeout(testimonialAuto, 5000);
}
testimonioTime = setTimeout(testimonialAuto, 5000);

$(".box-member,#nav-members .bt-close").on("mouseover", function(){
  $(this).parent().find(".bt-close").addClass("visible")
})
$(".box-member,#nav-members .bt-close").on("mouseout", function(){
  $("#nav-members .bt-close").removeClass("visible")
})
$('#nav-members .bt-close').on('click',  function(e) {
  $(this).parent().fadeOut(500).remove();
})

function previewFile() {
  console.log("lleegooooooo")
  var preview = document.querySelector('.img-profile');
  var file    = document.querySelector('#file').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}

function previewFileMedia() {
	  var preview = document.querySelector('.mediaUpload');
	  var file    = document.querySelector('#mediaUpload').files[0];
	  var reader  = new FileReader();

	  reader.addEventListener("load", function () {
	    preview.src = reader.result;
	  }, false);

	  if (file) {
	    reader.readAsDataURL(file);
	  }
	}


$(".forgot, .loginError a").on("click",function(){
  $("#form-recovery").fadeIn();
  $("#form-login").hide();  
})

$(".nav-link").on("click",function(){
  $("#form-recovery").hide();
  $("#form-login").show();  
})

function scrollContact(){
  $("html, body").delay(500).animate({scrollTop: $('#contact-form').offset().top }, 2000);
  $("#contact-form").hide();
  $(".messageOK").show();
}

$(".input-invite-friend").on('keyup', function (e) {
    e.preventDefault();
    newEmail=false
      if (e.keyCode == 13) {
         $(".errorLocation").hide()
        if($(".input-invite-friend").val()!=""){
          $(".container-tags-email .tag").each(function(e){
            if($("span", this).html()==$(".input-invite-friend").val()){
              newEmail =true;
            }
          })
          if(newEmail==true){
            $(".errorLocation").show()
            $(".errorLocation").html("The email already exist.");
            $(".input-invite-friend").val("")
            return false;
            newEmail =false;
          }else{
            $(".errorLocation").hide()
            $(".container-tags-email").append('<div class="tag"><span>'+$(this).val()+'</span><a class="bt-delete-tag" href="javascript:;" title="Remove" style="color: #724985;"><i class="fa fa-window-close" aria-hidden="true"></i></a><input type="hidden" value='+$(this).val()+'  name="inviteFriendsPopup[]"/></div>'); 
            $(".inputLocation").val("");
            $(".bt-save-profile.last.bt-add-location").css("display","none");
            $(".input-invite-friend").val("")
          }   
        }else{
          $(".errorLocation").show()
          $(".errorLocation").html("The field can't be empty.</p>");
        }
        $(".input-invite-friend").blur();
      }
  });
$(".input-invite-friend").on("focus",function(){
  $(this).val("");
})


$.getJSON("locations-search.php", function (data) {
  var allLocations = [];      // THE ARRAY TO STORE JSON ITEMS.
  $.each(data, function (index, value) {
      allLocations.push(value);       // PUSH THE VALUES INSIDE THE ARRAY.
  });

  var arrLocations = [];
  for (var i = 0; i < allLocations.length; i++) {
      for (var key in allLocations[i]) {
          if (arrLocations.indexOf(key) === -1) {
              arrLocations.push(key);
          }
      }
  }
  $(".bt-add-location").on("click", function(){
    if($(".inputLocation").val()!=""){
      for(i=0; i<allLocations.length; i++){
        if($(".inputLocation").val()==allLocations[i].name){
          $(".container-tags").append('<div class="tag" style="margin-right: 5px;"><span>'+allLocations[i].name+'</span><a class="bt-delete-tag" href="javascript:;" title="Remove" style="color: #fff;"><i class="fa fa-window-close" aria-hidden="true"></i></a><input type="hidden" value='+allLocations[i].id_location+'  name="id_neighborhood[]"/></div>'); 
          $(".inputLocation").val("");
          $(".bt-save-profile.last.bt-add-location").css("display","none");
        }
      }
    }
  })

  $(".inputLocation").on('keyup', function (e) {
    e.preventDefault();
      if (e.keyCode == 13) {
        if($(this).val()!=""){
          for(i=0; i<allLocations.length; i++){
            if($(".inputLocation").val()==allLocations[i].name){
              $(".container-tags").append('<div class="tag"><span>'+allLocations[i].name+'</span><a class="bt-delete-tag" href="javascript:;" title="Remove" style="color: #724985;"><i class="fa fa-window-close" aria-hidden="true"></i></a><input type="hidden" value='+allLocations[i].id_location+'  name="id_neighborhood[]"/></div>'); 
              $(".inputLocation").val("");
              $(".bt-save-profile.last.bt-add-location").css("display","none");
            }
          }
        }
      }
  });

});


function interests(jsonPath){
  $.getJSON(jsonPath, function (data) {
  var allInterests = [];      // THE ARRAY TO STORE JSON ITEMS.
  $.each(data, function (index, value) {
      allInterests.push(value);    // PUSH THE VALUES INSIDE THE ARRAY.
  });
  
  var pictureInit = "";
  $(".admin-slider .all-slides .slide, .admin-slider .pagination ul li").remove();
  var arrInterests = [];
  for (var i = 0; i < allInterests.length; i++) {
      for (var key in allInterests[i]) {
          if (arrInterests.indexOf(key) === -1) {
              arrInterests.push(key);
          }
      }
      pictureInit = allInterests[0].picture;
      $(".admin-slider .all-slides").append('<div class="slide pos'+i+'"><img src='+allInterests[i].picture+'></div>')
      $(".admin-slider .pagination ul").append('<li><a href="javascript:;" style="margin-right:10px">'+(i+1)+'</a></li>')
  }
  $(".admin-slider .all-slides .slide:first()").addClass("selected")
  $(".admin-slider .pagination ul li:first() a").addClass("selected")
  
  $(".image-selected").val(pictureInit);
 });
  
  
  
}


$("#categories-container input[type='checkbox'], .type-of-care input[type='radio']").change(function(){
  interestlist="";
  typelist="";
  $("#categories-container input[type='checkbox']").each(function(e){
  	
    if($(this).is(':checked')) {
  	if(interestlist!="") {interestlist+=",";}
      interestlist+=$(this).val();
    }
  })
  $(".type-of-care input[type='radio']").each(function(e){
    if($(this).is(':checked')) {
  	if(typelist!="") {typelist+=",";}
      typelist+=$(this).val();
    } 
  })
  //pathJson="/images-search.php?interestlist="+interestlist+"&typelist="+typelist;
  
  pathJson="/images-search.php?t="+Date.now();
  if(interestlist!="") {pathJson+="&interestlist="+interestlist;}
  if(typelist!="") {pathJson+="&typelist="+typelist;}
  
  interests(pathJson);
  
})

function checkInterests(){
  interestlist="";
  typelist="";
  $("#categories-container input[type='checkbox']").each(function(e){
    
    if($(this).is(':checked')) {
    if(interestlist!="") {interestlist+=",";}
      interestlist+=$(this).val();
    }
  })
  $(".type-of-care input[type='radio']").each(function(e){
    if($(this).is(':checked')) {
    if(typelist!="") {typelist+=",";}
      typelist+=$(this).val();
    } 
  })
  //pathJson="/images-search.php?interestlist="+interestlist+"&typelist="+typelist;
  
  pathJson="/images-search.php?t="+Date.now();
  if(interestlist!="") {pathJson+="&interestlist="+interestlist;}
  if(typelist!="") {pathJson+="&typelist="+typelist;}
  interests(pathJson);
}

/*$(".input-budget").on('keyup', function (e) {
    if($(this).val().length>1){
      $(this).val("$" + $(this).val().substring(1));
     }else if($(this).val().length==1){
      if($(this).val()!="$"){
        $(this).val("$" + $(this).val());
      }else{
        $(this).val();
      }      
     }else{
      $(this).val();       
     }
   
});*/

$(".bt-help-type.providers").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.providers").length>0){
    $("#accordion .card.providers").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})

$(".bt-help-type.account").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.account").length>0){
    $("#accordion .card.account").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})

$(".bt-help-type.parents").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.parents").length>0){
    $("#accordion .card.parents").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})

$(".bt-help-type.general").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.general").length>0){
    $("#accordion .card.general").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})




$.getJSON("specialists-search.php", function (data) {
  var allSpecialists = [];      // THE ARRAY TO STORE JSON ITEMS.
  $.each(data, function (index, value) {
      allSpecialists.push(value);       // PUSH THE VALUES INSIDE THE ARRAY.
  });

  var arrSpecialists = [];
  for (var i = 0; i < allSpecialists.length; i++) {
      for (var key in allSpecialists[i]) {
          if (arrSpecialists.indexOf(key) === -1) {
              arrSpecialists.push(key);
          }
      }
  }
  $(".bt-add-specialist").on("click", function(){
    if($(".inputSpecialist").val()!=""){
      for(i=0; i<allSpecialists.length; i++){
        if($(".inputSpecialist").val()==allSpecialists[i].name){
          $(".container-specialist-tags").append('<div class="specialist-tag"><img src="'+allSpecialists[i].picture+'"/><a href="/specialist-profile.php?id_specialist='+allSpecialists[i].id_specialist+'" data-idSpecialist="'+allSpecialists[i].id_specialist+'" title="">'+allSpecialists[i].name+'</a><a class="bt-delete-tag" href="javascript:;" title="Remove" style="color: #724985;"><i class="fa fa-window-close" aria-hidden="true"></i></a><input type="hidden" name="id_specialist[]" value="'+allSpecialists[i].id_specialist+'"/></div>');
          $(".inputSpecialist").val("");
          $(".bt-save-profile.last.bt-add-specialist").css("display","none");
        }
      }
    }
  })

  $(".inputSpecialist").on('keyup', function (e) {
    if (e.keyCode == 13) {
      for(i=0; i<allSpecialists.length; i++){
        if($(".inputSpecialist").val()==allSpecialists[i].name){
          $(".container-specialist-tags").append('<div class="specialist-tag"><img src="'+allSpecialists[i].picture+'"/><a href="specialist-profile.php?id_specialist='+allSpecialists[i].id_specialist+'" data-idSpecialist="'+allSpecialists[i].id_specialist+'" title="">'+allSpecialists[i].name+'</a><a class="bt-delete-tag" href="javascript:;" title="Remove" style="color: #724985;"><i class="fa fa-window-close" aria-hidden="true"></i></a><input type="hidden" name="id_specialist[]" value="'+allSpecialists[i].id_specialist+'"/></div>');
          $(".inputSpecialist").val("");
          $(".bt-save-profile.last.bt-add-specialist").css("display","none");
        }
      }
    }
  });

});



$.getJSON("supporters-search.php", function (data) {
  var allSupporters = [];      // THE ARRAY TO STORE JSON ITEMS.
  $.each(data, function (index, value) {
      allSupporters.push(value);       // PUSH THE VALUES INSIDE THE ARRAY.
  });

  var arrSupporters = [];
  for (var i = 0; i < allSupporters.length; i++) {
      for (var key in allSupporters[i]) {
          if (arrSupporters.indexOf(key) === -1) {
              arrSupporters.push(key);
          }
      }
  }
  $(".bt-add-supporter").on("click", function(){
    if($(".inputSupporter").val()!=""){
      for(i=0; i<allSupporters.length; i++){
        if($(".inputSupporter").val()==allSupporters[i].name){
          $(".container-supporter-tags").append('<div class="specialist-tag"><img src="'+allSupporters[i].picture+'"/><a href="/specialist-profile.php?id_specialist='+allSupporters[i].id_specialist+'" data-idSupporter="'+allSupporters[i].id_specialist+'" title="">'+allSupporters[i].name+'</a><a class="bt-delete-tag" href="javascript:;" title="Remove" style="color: #724985;"><i class="fa fa-window-close" aria-hidden="true"></i></a><input type="hidden" name="id_supporter[]" value="'+allSupporters[i].id_specialist+'"/></div>');
          $(".inputSupporter").val("");
          $(".bt-save-profile.last.bt-add-supporter").css("display","none");
        }
      }
    }
  })

  $(".inputSupporter").on('keyup', function (e) {
    if (e.keyCode == 13) {
      for(i=0; i<allSupporters.length; i++){
        if($(".inputSupporter").val()==allSupporters[i].name){
          $(".container-supporter-tags").append('<div class="specialist-tag"><img src="'+allSupporters[i].picture+'"/><a href="specialist-profile.php?id_specialist='+allSupporters[i].id_specialist+'" data-idSupporter="'+allSupporters[i].id_specialist+'" title="">'+allSupporters[i].name+'</a><a class="bt-delete-tag" href="javascript:;" title="Remove" style="color: #724985;"><i class="fa fa-window-close" aria-hidden="true"></i></a><input type="hidden" name="id_supporter[]" value="'+allSupporters[i].id_specialist+'"/></div>');
          $(".inputSupporter").val("");
          $(".bt-save-profile.last.bt-add-supporter").css("display","none");
        }
      }
    }
  });

});







/* AUTO COMPLETE */

var allLocations = [];
   
  // retrieve JSon from external url and load the data inside an array :
  $.getJSON( "locations-search.php", function( data ) {
    $.each( data, function( key, val ) {
      allLocations.push(val.name);
    });
  });
  $(".inputLocation").autocomplete({
    source: allLocations,
    select: function (e, ui) {
      repeatLocation=false;
      $(".container-tags .tag").each(function(e){
        if($("span",this).html()== ui.item.value){
          $(".inputLocation").blur();
          $(".inputLocation").after("<p class='errorLocation'>The location is already selected.</p>");
          repeatLocation=true;
          return false;
        }
      })
      
      if(repeatLocation==false){
        $(".bt-add-location").css("display","table")
      }
    }
  });
  $(".inputLocation").on("focus", function(){
    $(".errorLocation").hide();
    $(this).val("");
  })
  var allSpecialists = [];
   
  // retrieve JSon from external url and load the data inside an array :
  $.getJSON( "specialists-search.php", function( data ) {
    $.each( data, function( key, val ) {
      allSpecialists.push(val.name);
    });
  });
  $(".inputSpecialist").autocomplete({
    source: allSpecialists,
    select: function (e, ui) {
      repeatSpecialists=false;
      $(".container-specialist-tags .specialist-tag").each(function(e){
        if($("a",this).html()== ui.item.value){
          $(".inputSpecialist").blur();
          $(".inputSpecialist").after("<p class='errorLocation'>The specialist is already selected.</p>");
          repeatSpecialists=true;          
          return false;
        }
      })
      
      if(repeatSpecialists==false){
        $(".bt-add-specialist").css("display","table")
      }
    }
  });
  $(".inputSpecialist").on("focus", function(){
    $(".errorLocation").hide();
    $(this).val("");
  })


  /////////////////////////////////////

  var allSupporters = [];
   
  // retrieve JSon from external url and load the data inside an array :
  $.getJSON( "supporters-search.php", function( data ) {
    $.each( data, function( key, val ) {
      allSupporters.push(val.name);
    });
  });
  $(".inputSupporter").autocomplete({
    source: allSupporters,
    select: function (e, ui) {
      repeatSupporters=false;
      $(".container-supporter-tags .specialist-tag").each(function(e){
        if($("a",this).html()== ui.item.value){
          $(".inputSupporter").blur();
          $(".inputSupporter").after("<p class='errorLocation'>The supporter is already selected.</p>");
          repeatSupporters=true;          
          return false;
        }
      })
      
      if(repeatSupporters==false){
        $(".bt-add-supporter").css("display","table")
      }
    }
  });
  $(".inputSupporter").on("focus", function(){
    $(".errorLocation").hide();
    $(this).val("");
  })

  ///////////////////////////////////

  var allGroups = [];

  $.getJSON( "groups-search.php", function( data ) {
    $.each( data, function( key, val ) {
      allGroups.push(val.name);
    });
  });

  $("#inputSelectGroup").autocomplete({
    source: allGroups/*,
    select: function (e, ui) {
      $(".bt-add-specialist").css("display", "table")
    }*/
  });

  

$('#login').on('hidden.bs.modal', function () {
  $(".loginError").hide();
})


/* CALCULATOR */

$('#numKids').on('input', function () {    
    var value = $(this).val();    
    if ((value !== '') && (value.indexOf('.') === -1)) {        
        $(this).val(Math.max(Math.min(value, 8), 2));
    }
});

$('#hours').on('input', function () {    
    var value = $(this).val();    
    if ((value !== '') && (value.indexOf('.') === -1)) {        
        $(this).val(Math.max(Math.min(value, 9), 1));
    }
});


$('#numKids,#hours').on('input',function(e){
    calculatePlaydate($("#numKids").val(), $("#hours").val())
});

function calculatePlaydate(numKids, hours){
  if($("#numKids").val()!="" && $("#hours").val()!=""){
    if(numKids==2){
      $("#baseRate").val("$18");      
    }else if(numKids==3){
      $("#baseRate").val("$16");  
    }else if(numKids>=4 && numKids<=5){
      $("#baseRate").val("$12");  
    }else if(numKids==6){
      $("#baseRate").val("$11");  
    }else if(numKids>=7 && numKids<=8){
      $("#baseRate").val("$10");  
    }
     $("#youPay").val("$"+ $("#baseRate").val().substring(1) * $("#hours").val())
  }else{
     $("#youPay").val("");
     $("#baseRate").val("");
  } 
}


$('.flip').mouseover(function(){
  $(this).find(".card").addClass('flipped');
});
$('.flip').mouseout(function(){
  $(this).find(".card").removeClass('flipped');
});

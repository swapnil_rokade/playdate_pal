var numTestimonio=1;
var totalTestimonio=$(".testimonio").length;
$(window).scroll(function(){
  if($(window).scrollTop()!=0){
    $(".navbar").addClass("is-fixed")
  }else{
  	$(".navbar").removeClass("is-fixed")
  }
  $(".admin-slider.front .all-slides").css("height",$(".admin-slider.front .all-slides .slide img").css("height"))
})

$(".bt-travel").on("click",function(){
  $(".bt-travel").not(this).removeClass("selected")
  $(this).toggleClass("selected")
})

var position = $(window).scrollTop(); 
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if($(window).width()>1199){
      if(scroll > position) {
          $(".navbar").addClass("is-fixed-down");
          $(".navbar").removeClass("is-fixed");
      } else {
          $(".navbar").addClass("is-fixed");
          $(".navbar").removeClass("is-fixed-down");
      }
      position = scroll;
      $(".navbar-brand img").attr("src","/img/logo-color-playdate.png");
      if($(window).scrollTop()==0){
        $(".navbar").removeClass("is-fixed-down");
        $(".navbar").removeClass("is-fixed");
        $(".navbar-brand img").attr("src","/img/logo-playdate.png");
      }
    }
});


$(".bt-get-started").on("click", function(){
  $(".create-step1").hide()
  $(".create-group").fadeIn()
})

$(document).on("click", ".bt-add-kid",function(){
  $(this).parent().parent().clone().appendTo(".kids-playdates");
  $(this).hide()
})


$(document).ready(function(){
 if($(window).scrollTop()!=0 && $(window).width()>1199){
    $(".navbar").addClass("is-fixed");
    $(".navbar-brand img").attr("src","/img/logo-color-playdate.png");
  }
  
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  
  $(".bt-sidebar").on("click", function(){
  	$(this).toggleClass("open");
  })
  
  $('[data-toggle="tooltip"]').tooltip();
  $('.flip').hover(function(){
    $(this).find('.card').toggleClass('flipped');
  });

  $('.datepicker, .input-time, .input-budget, .input-age, .input-walk, .input-train, .input-car, .input-taxi ').on("blur",function(){
  	if($(this).val()!=""){
  		$(this).addClass("filled")
  	}else{
  		$(this).removeClass("filled")
  	}
  })
  $('.datepicker').datepicker({
    dateFormat: 'dd/mm/yy',
    onSelect: function(date) { 
      if(date !=""){
        $(this).addClass("filled")
      }else{
        $(this).removeClass("filled")
      }
    } 
  });



  $(".content-title .texto .links ul li a").on("click", function(){
  	$(".content-title .texto .links ul li a").removeClass("selected");
  	$(this).addClass("selected");
  	content = $(this).attr("data-target");
  	$(".content-title .texto .text").fadeOut(500);
  	$("#"+content).fadeIn(500);
  })

  $(document).on("click", ".bt-delete-tag",function (e) {
  	$(this).parent().fadeOut(500).remove();
  })

  if($("body").hasClass("last-playdate-page")){
    $("#last-playdate").modal('show');
  }

  if($("body").hasClass("my-dashboard")){
    if($(location).attr("href").indexOf("profile")==-1){
     $("#dashboard-welcome").modal('show');
    }
  }
  if($("body").hasClass("specialist-dashboard")){
    $("#dashboard-welcome").modal('show');
  }

  $("#last-playdate .puntuacion ul li, .rate-playdate .puntuacion.rate ul li").on("click",function(){
    $(this).siblings().removeClass("active")
    $(this).prevAll().addClass("active");
    $(this).addClass("active");
    $(this).parent().parent().find(".num").html(($(this).attr("id").charAt(4))+"/5")
  })




  $(".selectGenre .child").on("click", function(){
    if( $(this).parent().hasClass("opened")){
      $(this).parent().find(".child").removeClass("selected")
      $(this).addClass("selected")
      $(this).parent().find("#inputSelectGenre").val($(this).attr("data-genre"));
    }

    $(this).parent().toggleClass("opened")
  })

  $(".selectTime a").on("click", function(){
    $(this).parent().find("a.selected").html($(this).html())
    $(this).parent().find("a.selected").attr("data-time", $(this).attr("data-time"))
    $(this).parent().find("#inputSelectTime").val($(this).attr("data-time"));
    $(this).parent().removeClass("opened")
  })

  $(".selectTime a.selected").on("click", function(){
    $(this).parent().toggleClass("opened")
  })



  if($("#playpack-history").length>1){
    $("#playpack-history, #playpack-credit").DataTable({searching: false, paging: false,bFilter: false, bInfo: false});;
  }

  $(".admin-slider .pagination ul li a").on("click",function (e) {
    $(".admin-slider .pagination ul li a").removeClass("selected");
    $(".admin-slider .all-slides .slide").removeClass("selected");
    numSlide=parseFloat($(this).parent().index())+1;
    $(".admin-slider .all-slides .slide:nth-child("+numSlide+")").addClass("selected")
    $(this).addClass("selected");
  })

  numSliderFront=1;
  $(".admin-slider.front .bt-slider-left").on("click",function (e) {
    totalSlider=$(".admin-slider.front .slide").length
    if(numSliderFront==1){
      numSliderFront=totalSlider;
    }else{
      numSliderFront--;
    }
    $(".admin-slider.front .all-slides .slide").removeClass("selected")
    $(".admin-slider.front .all-slides .slide:nth-child("+numSliderFront+")").addClass("selected")
  })
  $(".admin-slider.front .bt-slider-right").on("click",function (e) {
    totalSlider=$(".admin-slider.front .slide").length
    if(numSliderFront==totalSlider){
      numSliderFront=1;
    }else{
      numSliderFront++;
    }
    $(".admin-slider.front .all-slides .slide").removeClass("selected")
    $(".admin-slider.front .all-slides .slide:nth-child("+numSliderFront+")").addClass("selected")
  })

  $(".admin-slider.front .all-slides").css("height",$(".admin-slider.front .all-slides .slide img").css("height"))

if($(location).attr("href").indexOf("current")!=-1){
	    $("#nav-current-tab").tab('show');
}
  if($(location).attr("href").indexOf("requests")!=-1){
	    $("#nav-request-tab").tab('show');
}
if($(location).attr("href").indexOf("past")!=-1){
	    $("#nav-past-tab").tab('show');
}
if($(location).attr("href").indexOf("groups")!=-1){
    $("#nav-groups-tab").tab('show');
}
if($(location).attr("href").indexOf("specialists")!=-1){
    $("#nav-specialists-tab").tab('show');
}
if($(location).attr("href").indexOf("profile")!=-1){
    $("#nav-profile-tab").tab('show');
}
if($(location).attr("href").indexOf("members")!=-1){
    $("#nav-members-tab").tab('show');
}
if($(location).attr("href").indexOf("playpacks")!=-1){
    $("#nav-playpacks-tab").tab('show');
}
  
  
  
  if($(".testimonio").length>0){
    i=1;
    $(".testimonio").each(function(e){
      if(i==1){
        $(".nav-testimonios").append('<li class="btn active" id="'+i+'"></li>')
      }else{
        $(".nav-testimonios").append('<li class="btn" id="'+i+'"></li>')
      }      
      $(this).attr("id",i);
      i++;
    })
     $(".playdates-testimonials .nav-testimonios li#1").addClass("active")
  }
  $(".playdates-testimonials .nav-testimonios li").on("click",function(){
    $(".playdates-testimonials .nav-testimonios li").removeClass("active")
     $(this).addClass("active")
     $(".testimonio").fadeOut();
     $(".testimonio#"+$(this).attr("id")).fadeIn();
     numTestimonio=$(this).attr("id");
  })
  $(".content-title h2.host").on("click",function(){
    $(".contact-form.apply").addClass("host")
    $(".content-title h2.host").removeClass("inactive")
    $(".content-title h2.specialist").addClass("inactive")
    $(".form-host").show()
    $(".form-specialist").hide()
  })
   $(".content-title h2.specialist").on("click",function(){
    $(".contact-form.apply").removeClass("host")
    $(".content-title h2.specialist").removeClass("inactive")
    $(".content-title h2.host").addClass("inactive")
    $(".form-specialist").show()
    $(".form-host").hide()
  })


})  


function testimonialAuto(){
  $(".playdates-testimonials .nav-testimonios li").removeClass("active")
  $(".testimonio#"+numTestimonio).fadeOut();
  if(numTestimonio==totalTestimonio){
    numTestimonio=1;
  }else{
    numTestimonio++;
  }
  $(".testimonio#"+numTestimonio).fadeIn();
  $(".playdates-testimonials .nav-testimonios li#"+numTestimonio).addClass("active")
  clearTimeout(testimonioTime);
  testimonioTime = setTimeout(testimonialAuto, 5000);
}
testimonioTime = setTimeout(testimonialAuto, 5000);

$(".box-member,#nav-members .bt-close").on("mouseover", function(){
  $(this).parent().find(".bt-close").addClass("visible")
})
$(".box-member,#nav-members .bt-close").on("mouseout", function(){
  $("#nav-members .bt-close").removeClass("visible")
})
$('#nav-members .bt-close').on('click',  function(e) {
  $(this).parent().fadeOut(500).remove();
})

function previewFile() {
  var preview = document.querySelector('.img-profile');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}


$(".forgot, .loginError a").on("click",function(){
  $("#form-recovery").fadeIn();
  $("#form-login").hide();  
})

$(".nav-link").on("click",function(){
  $("#form-recovery").hide();
  $("#form-login").show();  
})

function scrollContact(){
  $("html, body").delay(500).animate({scrollTop: $('#contact-form').offset().top }, 2000);
  $("#contact-form").hide();
  $(".messageOK").show();
}





$.getJSON("locations-search.php", function (data) {
  var allLocations = [];      // THE ARRAY TO STORE JSON ITEMS.
  $.each(data, function (index, value) {
      allLocations.push(value);       // PUSH THE VALUES INSIDE THE ARRAY.
  });

  var arrLocations = [];
  for (var i = 0; i < allLocations.length; i++) {
      for (var key in allLocations[i]) {
          if (arrLocations.indexOf(key) === -1) {
              arrLocations.push(key);
          }
      }
  }
  $(".bt-add-location").on("click", function(){
    if($(".inputLocation").val()!=""){
      for(i=0; i<allLocations.length; i++){
        if($(".inputLocation").val()==allLocations[i].neighborhood || $(".inputLocation").val()==allLocations[i].name){
          $(".container-tags").append('<div class="tag"><span>'+allLocations[i].neighborhood+'</span><a class="bt-delete-tag" href="javascripit:;" title="Eliminar tag">X</a><input type="hidden" value='+allLocations[i].id_location+'  name="zipcode[]"/></div>'); 
          $(".inputLocation").val("");
          $(".bt-save-profile.last.bt-add-location").css("display","none");
        }
      }
    }
  })

  $(".inputLocation").on('keyup', function (e) {
    e.preventDefault();
      if (e.keyCode == 13) {
        if($(this).val()!=""){
          for(i=0; i<allLocations.length; i++){
            if($(".inputLocation").val()==allLocations[i].neighborhood || $(".inputLocation").val()==allLocations[i].name){
              $(".container-tags").append('<div class="tag"><span>'+allLocations[i].neighborhood+'</span><a class="bt-delete-tag" href="javascripit:;" title="Eliminar tag">X</a><input type="hidden" value='+allLocations[i].id_location+'  name="zipcode[]"/></div>'); 
              $(".inputLocation").val("");
              $(".bt-save-profile.last.bt-add-location").css("display","none");
            }
          }
        }
      }
  });

});

/*$(".input-budget").on('keyup', function (e) {
    if($(this).val().length>1){
      $(this).val("$" + $(this).val().substring(1));
     }else if($(this).val().length==1){
      if($(this).val()!="$"){
        $(this).val("$" + $(this).val());
      }else{
        $(this).val();
      }      
     }else{
      $(this).val();       
     }
   
});*/

$(".bt-help-type.providers").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.providers").length>0){
    $("#accordion .card.providers").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})

$(".bt-help-type.account").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.account").length>0){
    $("#accordion .card.account").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})

$(".bt-help-type.parents").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.parents").length>0){
    $("#accordion .card.parents").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})

$(".bt-help-type.general").on("click",function(){
  $("#accordion .card").hide()
  $(".all-faqs .no-results").hide()
  if($("#accordion .card.general").length>0){
    $("#accordion .card.general").fadeIn()
  }else{
    $(".all-faqs .no-results").fadeIn()
  }
  
})




$.getJSON("specialists-search.php", function (data) {
  var allSpecialists = [];      // THE ARRAY TO STORE JSON ITEMS.
  $.each(data, function (index, value) {
      allSpecialists.push(value);       // PUSH THE VALUES INSIDE THE ARRAY.
  });

  var arrSpecialists = [];
  for (var i = 0; i < allSpecialists.length; i++) {
      for (var key in allSpecialists[i]) {
          if (arrSpecialists.indexOf(key) === -1) {
              arrSpecialists.push(key);
          }
      }
  }
  $(".bt-add-specialist").on("click", function(){
    if($("#inputSpecialists").val()!=""){
      for(i=0; i<allSpecialists.length; i++){
        if($("#inputSpecialists").val()==allSpecialists[i].name){
          $(".container-specialist-tags").append('<div class="specialist-tag"><img src="'+allSpecialists[i].picture+'"/><a href="specialists-public-profile.html" data-idSpecialist="'+allSpecialists[i].id_specialist+'" title="">'+allSpecialists[i].name+'</a><a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a><input type="hidden" name="id_specialist[]" value="'+allSpecialists[i].id_specialist+'"/></div>');
          $("#inputSpecialists").val("");
          $(".bt-save-profile.last.bt-add-specialist").css("display","none");
        }
      }
    }
  })

  $("#inputSpecialists").on('keyup', function (e) {
    if (e.keyCode == 13) {
      for(i=0; i<allSpecialists.length; i++){
        if($("#inputSpecialists").val()==allSpecialists[i].name){
          $(".container-specialist-tags").append('<div class="specialist-tag"><img src="'+allSpecialists[i].picture+'"/><a href="specialists-public-profile.html" data-idSpecialist="'+allSpecialists[i].id_specialist+'" title="">'+allSpecialists[i].name+'</a><a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a><input type="hidden" name="id_specialist[]" value="'+allSpecialists[i].id_specialist+'"/></div>');
          $("#inputSpecialists").val("");
          $(".bt-save-profile.last.bt-add-specialist").css("display","none");
        }
      }
    }
  });

});

/* AUTO COMPLETE */

var allLocations = [];
   
  // retrieve JSon from external url and load the data inside an array :
  $.getJSON( "locations-search.php", function( data ) {
    $.each( data, function( key, val ) {
      allLocations.push(val.name, val.neighborhood);
    });
  });
  $(".inputLocation").autocomplete({
    source: allLocations,
    select: function (e, ui) {
      $(".bt-add-location").css("display", "table")
    }
  });

  var allSpecialists = [];
   
  // retrieve JSon from external url and load the data inside an array :
  $.getJSON( "specialists-search.php", function( data ) {
    $.each( data, function( key, val ) {
      allSpecialists.push(val.name);
    });
  });

  $("#inputSpecialists").autocomplete({
    source: allSpecialists,
    select: function (e, ui) {
      $(".bt-add-specialist").css("display", "table")
    }
  });

$('#login').on('hidden.bs.modal', function () {
  $(".loginError").hide();
})


/* CALCULATOR */

$('#numKids').on('input', function () {    
    var value = $(this).val();    
    if ((value !== '') && (value.indexOf('.') === -1)) {        
        $(this).val(Math.max(Math.min(value, 8), 2));
    }
});

$('#hours').on('input', function () {    
    var value = $(this).val();    
    if ((value !== '') && (value.indexOf('.') === -1)) {        
        $(this).val(Math.max(Math.min(value, 9), 1));
    }
});


$('#numKids,#hours').on('input',function(e){
    calculatePlaydate($("#numKids").val(), $("#hours").val())
});

function calculatePlaydate(numKids, hours){
  if($("#numKids").val()!="" && $("#hours").val()!=""){
    if(numKids==2){
      $("#baseRate").val("$18");      
    }else if(numKids==3){
      $("#baseRate").val("$15");  
    }else if(numKids>=4 && numKids<=5){
      $("#baseRate").val("$15");  
    }else if(numKids>=6 && numKids<=8){
      $("#baseRate").val("$10");  
    }
     $("#youPay").val("$"+ $("#baseRate").val().substring(1) * $("#hours").val())
  }else{
     $("#youPay").val("");
     $("#baseRate").val("");
  } 
}
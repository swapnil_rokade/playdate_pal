$(window).scroll(function(){
  if($(window).scrollTop()!=0){
    $(".navbar").addClass("is-fixed")
  }else{
  	$(".navbar").removeClass("is-fixed")
  }
  $(".admin-slider.front .all-slides").css("height",$(".admin-slider.front .all-slides .slide img").css("height"))
})

$(".bt-travel").on("click",function(){
  $(".bt-travel").not(this).removeClass("selected")
  $(this).toggleClass("selected")
})

$(document).ready(function(){

  if($(window).scrollTop()!=0){
    $(".navbar").addClass("is-fixed")
  }else{
  	$(".navbar").removeClass("is-fixed")
  }

  $(".bt-sidebar").on("click", function(){
  	$(this).toggleClass("open");
  })
  
  $('[data-toggle="tooltip"]').tooltip();
  $('.datepicker').datepicker();
  $('.flip').hover(function(){
    $(this).find('.card').toggleClass('flipped');
  });

  $('.datepicker, .input-time, .input-budget, .input-age, .input-walk, .input-train, .input-car, .input-taxi ').on("blur",function(){
  	if($(this).val()!=""){
  		$(this).addClass("filled")
  	}else{
  		$(this).removeClass("filled")
  	}
  })

  $(".content-title .texto .links ul li a").on("click", function(){
  	$(".content-title .texto .links ul li a").removeClass("selected");
  	$(this).addClass("selected");
  	content = $(this).attr("data-target");
  	$(".content-title .texto .text").fadeOut(500);
  	$("#"+content).fadeIn(500);
  })

  $(".bt-delete-tag").on("click",function (e) {
  	$(this).parent().fadeOut(500).remove();
  })

  if($("body").hasClass("last-playdate-page")){
    $("#last-playdate").modal('show');
  }

  if($("body").hasClass("my-dashboard")){
    if($(location).attr("href").indexOf("profile")==-1){
     $("#dashboard-welcome").modal('show');
    }
  }
  if($("body").hasClass("specialist-dashboard")){
    $("#dashboard-welcome").modal('show');
  }

  $("#last-playdate .puntuacion ul li, .rate-playdate .puntuacion.rate ul li").on("click",function(){
    $(this).siblings().removeClass("active")
    $(this).prevAll().addClass("active");
    $(this).addClass("active");
    $(this).parent().parent().find(".num").html(($(this).attr("id").charAt(4))+"/5")
  })



  $(".selectGenre .child").on("click", function(){
    if( $(this).parent().hasClass("opened")){
      $(this).parent().find(".child").removeClass("selected")
      $(this).addClass("selected")
    }
    $(this).parent().toggleClass("opened")
  })

  if($("#playpack-history").length>1){
    $("#playpack-history, #playpack-credit").DataTable({searching: false, paging: false,bFilter: false, bInfo: false});;
  }

  $(".admin-slider .pagination ul li a").on("click",function (e) {
    $(".admin-slider .pagination ul li a").removeClass("selected");
    $(".admin-slider .all-slides .slide").removeClass("selected");
    numSlide=parseFloat($(this).parent().index())+1;
    $(".admin-slider .all-slides .slide:nth-child("+numSlide+")").addClass("selected")
    $(this).addClass("selected");
  })

  numSliderFront=1;
  $(".admin-slider.front .bt-slider-left").on("click",function (e) {
    totalSlider=$(".admin-slider.front .slide").length
    if(numSliderFront==1){
      numSliderFront=totalSlider;
    }else{
      numSliderFront--;
    }
    $(".admin-slider.front .all-slides .slide").removeClass("selected")
    $(".admin-slider.front .all-slides .slide:nth-child("+numSliderFront+")").addClass("selected")
  })
  $(".admin-slider.front .bt-slider-right").on("click",function (e) {
    totalSlider=$(".admin-slider.front .slide").length
    if(numSliderFront==totalSlider){
      numSliderFront=1;
    }else{
      numSliderFront++;
    }
    $(".admin-slider.front .all-slides .slide").removeClass("selected")
    $(".admin-slider.front .all-slides .slide:nth-child("+numSliderFront+")").addClass("selected")
  })

  $(".admin-slider.front .all-slides").css("height",$(".admin-slider.front .all-slides .slide img").css("height"))

  if($(location).attr("href").indexOf("profile")!=-1){
    $("#nav-profile-tab").tab('show');
  }

  if($(".testimonio").length>0){
    i=1;
    $(".testimonio").each(function(e){
      if(i==1){
        $(".nav-testimonios").append('<li class="btn active" id="'+i+'"></li>')
      }else{
        $(".nav-testimonios").append('<li class="btn" id="'+i+'"></li>')
      }      
      $(this).attr("id",i);
      i++;
    })
     $(".playdates-testimonials .nav-testimonios li#1").addClass("active")
  }
  $(".playdates-testimonials .nav-testimonios li").on("click",function(){
    $(".playdates-testimonials .nav-testimonios li").removeClass("active")
     $(this).addClass("active")
     $(".testimonio").fadeOut();
     $(".testimonio#"+$(this).attr("id")).fadeIn();
  })
  $(".content-title h2.host").on("click",function(){
    $(".contact-form.apply").addClass("host")
    $(".content-title h2.host").removeClass("inactive")
    $(".content-title h2.specialist").addClass("inactive")
    $(".form-host").show()
    $(".form-specialist").hide()
  })
   $(".content-title h2.specialist").on("click",function(){
    $(".contact-form.apply").removeClass("host")
    $(".content-title h2.specialist").removeClass("inactive")
    $(".content-title h2.host").addClass("inactive")
    $(".form-specialist").show()
    $(".form-host").hide()
  })


})  


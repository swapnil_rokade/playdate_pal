<?php
/**
 * PLAYDATE - STRIPE RETURN URI - CONNECT STRIPE
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isSpecialist) {
    header("Location: /");
}

//Stripe connect

$AUTHORIZE_URI = 'https://connect.stripe.com/express/oauth/authorize';
$TOKEN_URI = 'https://connect.stripe.com/oauth/token';
$PLAYDATE_STRIPE_URI = 'https://www.playdatepal/stripe-connect.php';
//$PLAYDATE_STRIPE_URI = 'http://playdate.local/stripe-connect.php';

if (isset($_REQUEST['code'])) { // Redirect w/ code
    $stripeCode = $_REQUEST['code'];
    
    echo("::::RECIBIDO CODE: ".$stripeCode.":::::<br/>");
    echo("::::VALIDANDO TOKEN:::::<br/>");
    $token_request_body = array(
        'client_secret' => $STRIPE_TEST_SECRET,
        'code' => $stripeCode,
        'grant_type' => 'authorization_code'
        //'client_id' => $STRIPE_TEST_CLIENTID,
    );
    
    echo("::::DIRECCI�N TOKEN:".($TOKEN_URI .'?' . http_build_query($token_request_body))."::::<br/>");
    
    $req = curl_init($TOKEN_URI .'?' . http_build_query($token_request_body));
    
    //$req = curl_init($TOKEN_URI);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POST, true );
    curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
    
    // TODO: Additional error handling
    $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
    
    $resp = json_decode(curl_exec($req), true);
    
    echo("----------------------------------------<br/>");
    foreach($resp as $key => $value) {
        echo "$key | $value <br/>";
    }
    echo("----------------------------------------<br/>");
    curl_close($req);
    
    $stripeUserId = $resp['stripe_user_id'];
    
    //TODO - SAVE THIS ID IN SPECIALIST PROFILE
    $specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
    if($specialist!=null) {
        SpecialistDAO::updateSpecialistStripeInfo($specialist->getId_specialist(), $stripeUserId);
    }
    
    echo (":::::::::CONECTADO CON USER ID:::::[".$stripeUserId."]:::::::::::::::");
    
} else if (isset($_GET['error'])) { // Error
    
    echo (":::::::::ERROR:::::::::".$_GET['error_description'].":::::::::::::::");
    
} else { // Show OAuth link
    $authorize_request_body = array(
        //'response_type' => 'code',
        //'scope' => 'read_write',
        'state' => '1234',
        'client_id' => $STRIPE_TEST_CLIENTID,
        'redirect_uri' => $PLAYDATE_STRIPE_URI
    );
    
    $url = $AUTHORIZE_URI . '?' . http_build_query($authorize_request_body);
    echo "<a class='stripe-connect' href='$url'>Connect with Stripe</a>";
}
?>
<?php 
//TEST CHARGE STRIPE FORM
require_once('./stripe-config.php'); ?>
<form action="charge.php" method="post">
  <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
          data-key="<?php echo $stripe['publishable_key']; ?>"
          data-description="Test Playpack"
          data-amount="100"
          data-locale="auto"></script>
</form>
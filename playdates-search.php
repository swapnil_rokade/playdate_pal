[
<?php 
/**
 * PLAYDATE - PLAYDATES LIST SEARCH 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$backUrl = "/playdates.php";

$playdates = PlaydateDAO::getPlaydatesList(1, 100, "date", "desc");

$i = 1;
foreach ($playdates as $auxPlaydate) {
    $link = "/playdate.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
    
    $interestName = "";
    $interestType = "";
    
    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
    $auxInterest = null;
    if(count($interests)>0) {
        $auxInterest = $interests[0];
        $interestName =$auxInterest->getName();
        
        switch ($auxInterest->getType()) {
            case Interest::$TYPE_TRAINING: {
                $interestType=" stem";
                break;
            }
            case Interest::$TYPE_ART: {
                $interestType=" creative";
                break;
            }
            case Interest::$TYPE_PLAY: {
                $interestType=" outdoor";
                break;
            }
            case Interest::$TYPE_CULTURAL: {
                $interestType=" attractions";
                break;
            }
        }
    }
    
    $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
    ?>
   {
    "id_playdate":"<?php echo $auxPlaydate->getId_playdate()?>",
    "picture":"<?php echo $auxPlaydate->getPicture()?>",
    "interest_name":"<?php echo $interestName ?>",
    "date":"<?php echo $auxPlaydate->getDate()?>",
    "time_init":"<?php echo $auxPlaydate->getTime_init()?>",
    "time_end":"<?php echo $auxPlaydate->getTime_end()?>",
    "name":"<?php echo $auxPlaydate->getName()?>",
    "loc_city":"<?php echo $auxPlaydate->getLoc_city()?>",
    "loc_state":"<?php echo $auxPlaydate->getLoc_state()?>",
    "age_init":"<?php echo $auxPlaydate->getAge_init()?>",
    "age_end":"<?php echo $auxPlaydate->getAge_end()?>",
    "num_children":"<?php echo $auxPlaydate->getNum_children()?>",
    "open_spots":"<?php echo $auxPlaydate->getOpen_spots()?>",
    "specialist_name":"<?php echo $specialist->getFullName() ?>",
    "interest_type":"<?php echo $interestType?>",
    "link":"<?php echo $link ?>"
   }<?php if($i<count($playdates)) {echo(", \r\n");} ?>
<?php $i++;} ?>
]
<?php 
/**
 * PLAYDATE - PARENTS WELCOME - When logged in  
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");
//echo $_SESSION['login'];exit;
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
 
    <title>Welcome - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
     <style type="text/css">
            body.modal-open {
             overflow: hidden;
             height :  100vh;
             /*position: fixed!important;*/
            }
        </style>
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page last-playdate-page<?php echo($isConnected?" connected":"") ?>">

	<?php /* LAST PLAYDATE VALORATION MODAL */
	/*
    <!-- last playdate -->
    <div class="modal fade" id="last-playdate" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-last-playdate">
              <h2 class="form-last-playdate-heading">How was your last playdate? </h2>
              <div class="playdate-info">
                <div class="titulo">Bronx Zoo Day Trip</div>
                <div class="info">
                  <span class="fecha">Jan 13th 2018</span> | <span class="hour">12pm-2pm</span>
                  <span class="place">Bronx, NYC</span>
                </div>
              </div>
              <div class="specialist">
                <div class="name">Monica Allen</div>
                <div class="puntuacion">
                  <ul>
                    <li id="voto1"></li>
                    <li id="voto2"></li>
                    <li id="voto3"></li>
                    <li id="voto4"></li>
                    <li id="voto5"></li>
                  </ul>
                  <span class="num">0/5</span>
                </div>
              </div>
              <div class="col-lg-12">                    
                     <label for="aboutSpecialist" class="sr-only">About Group</label>
                     <textarea class="form-control" id="aboutSpecialist" placeholder="Tell us about the specialist" rows="4"></textarea>
                     <label for="aboutPlaydate" class="sr-only">About Group</label>
                     <textarea class="form-control" id="aboutPlaydate" placeholder="Tell us about the playdate" rows="4"></textarea>
                </div>
              <button class="btn btn-md btn-primary" type="submit">Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- last playdate -->
    */ ?>

	<?php include ('components/menu.php'); ?>

  <div class="modal fade itinerary-modal" id="parent-welcome" tabindex="-1" role="dialog" style="padding-right: 15px;display: block;background-size: cover;">
      <!--<span id="close" style="float: right;"><i class="fa fa-close" style="font-size: 30px;color:#000;font-weight: normal;"></i></span>-->
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="color: #ccc;border: 10px solid;background-color: rgba(0,0,0,0.5);background: url(../img/3.png) no-repeat center;">
          <div class="modal-header" style="background: none;background-color: rgba(0,0,0,0.5);">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="closeButton;font-size: 30px!important;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="background: none;background-color: rgba(0,0,0,0.5);">
            
            <div class="row">
              <h2 class="itinerary-modal-heading noUppercase" style="color: #fff;font-weight: bold;font-size:30px">WELCOME TO PAL!</h2>
              <br />
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-size:17px; text-transform: uppercase; ">We are so very happy to have you here! </p><br />
              <p class="itinerary-modal-heading noUppercase" style="color: #fff;font-size:17px;text-transform: uppercase; ">Complete your profile, then start exploring! </p><br />
              <p class="itinerary-modal-heading noUppercase"><button type="button" onclick="location.href='/my-dashboard.php#nav-profile?var=pal'">Click Here!</button></p>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Welcome back</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">




<!-- Featured playdates -->
      <div class="row featured-playdates dashboard">
         
        <div class="col-lg-12">
          <div class="titulo">
            <h2>Hi 
            <?php if(isset($_SESSION['spec_name'])) { 
                        echo $_SESSION['spec_name']; 
                  } else if(isset($_SESSION['parent_name'])) { 
                      echo $_SESSION['parent_name']; 
                  } ?>!</h2>
          </div>
        </div>

        <div class="row row-eq-height">
            
            <div class="col-lg-3 vcenter">
              <a href="/playdates.php" title="" class="box-playdates">
                <div class="img-container">
                  <img src="img/ico/gray/0803-magnifier.svg" width="50" alt=""/>
                </div>
                <p>Search for a Playdate</p>
              </a>
            </div>

           <div class="col-lg-3 vcenter">
              <a href="/parent-request-playdate.php" title="" class="box-playdates">
                <div class="img-container">
                  <img src="img/ico/gray/0020-pencil5.svg" width="50" alt=""/>
                </div>
                <p>Request a Playdate</p>
              </a>
            </div>


            <div class="col-lg-3 vcenter">
              <a href="/get-playpacks.php" title=""  class="box-playdates">
                <div class="img-container">
                  <img src="img/ico/gray/0343-tags.svg" width="50" alt=""/>
                </div>
                <p>Purchase Playpack</p>
              </a>
            </div>

            <div class="col-lg-3 vcenter">
              <a href="/my-dashboard.php#nav-profile" title="Your Dashboard"  class="box-playdates">
                <div class="img-container">
                  <img src="img/ico/gray/0287-user.svg" width="50" alt=""/>
                </div>
                <p>Your Dashboard</p>
              </a>
            </div>
          
        </div>
      </div>

      <div class="row suggested-playdates">

        <div class="col-lg-12">
          <div class="titulo">Suggested Playdates in Your Area</div>
        </div>

        <div class="col-lg-12">
            <div class="row">
            
            
          	<?php 
          	$playdates = PlaydateDAO::getUpcomingPlaydatesList(1, 4, "date", "desc"); 
          	
          	foreach ($playdates as $auxPlaydate) {
          	    //Code updated by SR 31 Jan 2019
				$addonsPrice = 0;
				if($auxPlaydate->getAdd1_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd1_price()*100;
				}
				if($auxPlaydate->getAdd2_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd2_price()*100;
				}
				if($auxPlaydate->getAdd3_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd3_price()*100;
				}
				if($auxPlaydate->getAdd4_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd4_price()*100;
				}
        // Code End
          	    $link = "/playdate.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
          	    
          	    $auxOpenSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
          	    $auxPlaydate->setOpen_spots($auxOpenSpots);
          	    
          	    $interestName = "";
          	    $interestType = "";
          	    
          	    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
          	    $auxInterest = null;
          	    if(count($interests)>0) {
          	        $auxInterest = $interests[0];
          	        $interestName =$auxInterest->getName();
          	        
          	        switch ($auxInterest->getType()) {
          	            case Interest::$TYPE_TRAINING: {
          	                $interestType=" stem";
          	                break;
          	            }
          	            case Interest::$TYPE_ART: {
          	                $interestType=" creative";
          	                break;
          	            }
          	            case Interest::$TYPE_PLAY: {
          	                $interestType=" outdoor";
          	                break;
          	            }
          	            case Interest::$TYPE_CULTURAL: {
          	                $interestType=" attractions";
          	                break;
          	            }
          	        }
          	    }
          	    
          	    $specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
          	    
          	    $picPlaydate = "img/default-playdate-image.jpg";
          	    if(($auxPlaydate->getPicture()!=null) && ($auxPlaydate->getPicture()!="")) {
          	        $picPlaydate = $auxPlaydate->getPicture();
          	    }
          	    ?>
        
        
          <div class="col-lg-3 col-md-6 col-sm-6 col-6 first">
            <div class="box-playdates first <?php echo $interestType ?>">
              <?php if($isConnected) { ?>
              <a href="<?php echo $link ?>" class="img-container">
              <?php } else { ?>
              <a data-toggle="modal" data-target="#login" href="#"  class="img-container">
              <?php } ?>
                <img src="<?php echo $picPlaydate ?>"/ alt="">
                <?php if($auxInterest!=null) { ?>
                  <span class="cat-box"><?php echo $interestName ?></span>
                <?php } ?>
              </a>
              <div class="time">
                <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?>-<?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
              </div>
              <div class="info">
                <?php if($isConnected) { ?>
                <a href="<?php echo $link ?>" class="title">
                <?php } else { ?>
                <a data-toggle="modal" data-target="#login" href="#" class="title">
                <?php } ?>
                <?php echo $auxPlaydate->getName() ?></a>
                <span class="subtitle"><?php echo $auxPlaydate->getLoc_city() ?>, <?php echo $auxPlaydate->getLoc_state() ?></span>
              </div>

              <!--<div class="data">-->
              <!--  <div><span class="price">$60</span> or less | <span class="spots<?php echo($auxPlaydate->getOpen_spots()==1?" destacado":"") ?>"><?php echo $auxPlaydate->getOpen_spots() ?> open spot<?php if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>-->
              <!--</div>-->
                <div class="data" style="margin:0px!important">
                <div><span class="price">$<?php echo Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePrice($auxPlaydate->getId_playdate())+$addonsPrice), 0); ?></span> or less | <span class="spots<?php echo($auxPlaydate->getOpen_spots()==1?" destacado":"") ?>"><?php echo $auxPlaydate->getOpen_spots() ?> open spot<?php if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>
              </div>
              
               <div class="data">
                <div  class="age">Ages: <span><?php echo $auxPlaydate->getAge_init() ?> - <?php echo $auxPlaydate->getAge_end() ?></span></div>
                <?php if($specialist!=null) { ?>
                <div class="specialists">Led by <span><a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                <?php } ?>
              </div>
              <div class="reserve">
                  <?php if($isConnected) { ?>
                  <a href="<?php echo $link ?>" class="bt-reserve">View Playdate</a>
                <?php } else { ?>
                  <a data-toggle="modal" data-target="#login" href="#" class="bt-reserve">View Playdate</a>
                <?php } ?>
              </div>
            </div>
          </div>
          <?php } ?>            
            
           

            </div>
        </div>
      </div>



    </div>


    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
    <?php 
	if($isParent && ($_SESSION['welcome'] = 1)) {
	//    echo $_SESSION['login'];exit;
    $completed = ParentDAO::isCompletedProfile($_SESSION["parent_id"]);
		    if(!$completed  && ($_SESSION['login'] == 0)) {
	?>
		<script>  //$('#parent-welcome').attr("style", "display:block");
		    $('#parent-welcome').modal('show');</script>
	<?php 
  }else{?>
<script>
    $('#parent-welcome').attr("style", "display:none");</script>
  <?php }
}
	?>
<!--<script>$('#close').click(function(e){-->
<!--		$('#parent-welcome').modal('hide');-->
<!--	})-->
<!--</script>-->
  </body>

</html>

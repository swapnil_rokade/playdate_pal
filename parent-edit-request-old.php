<?php 
/**
 * PLAYDATE - PARENT EDIT REQUEST 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$parent = ParentDAO::getParent($_SESSION["parent_id"]);

$request = new PlaydateRequest();
$request->readFromRow($_REQUEST);
if($request->getId_request()>0) {
    $request = PlaydateRequestDAO::getPlaydateRequest($request->getId_request());
} else {
    header("Location: /");
}


$operation = null;
if(isset($_REQUEST["operation"])) { 
    $operation = $_REQUEST["operation"];
}

$groupName = "";
if($request->getId_group()!=null) {
    $grp = GroupDAO::getGroup($request->getId_group());
    if($grp!=null) {
        $groupName = $grp->getName();
    }
}

$updated = false;

switch ($operation) {
    case "update": {
        $newRequest = new PlaydateRequest();
        $newRequest->readFromRow($_REQUEST);
        
        if(isset($_REQUEST["invite_group"]) && ($_REQUEST["invite_group"]!="")) {
            $inviteGroup = GroupDAO::getGroupByName($_REQUEST["invite_group"]);
            if($inviteGroup!=null) {
                $groupName = $inviteGroup->getName();
                $request->setId_group($inviteGroup->getId_group());
            }
        }
        
        $request = PlaydateRequestDAO::updatePlaydateRequest($newRequest);
        
        if($request->getId_request()>0) {
            $updated=true;
            //Insertamos sus intereses
            InterestDAO::deleteAllRequestInterests($newRequest->getId_request());
            if(!empty($_POST['interests'])) {
                foreach($_POST['interests'] as $interest) {
                    InterestDAO::createRequestInterest($newRequest->getId_request(), $interest);
                }
            }
        
            //Eliminamos asociación children
            PlaydateRequestDAO::deleteAllRequestChildren($request->getId_request());
            //Insertamos los children
            if(!empty($_POST['selectedchildren'])) {
                foreach($_POST['selectedchildren'] as $auxchild) {
                    $childInsert = ChildrenDAO::getChildren($auxchild);
                    PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), $auxchild, 1, $childInsert->getAge());
                }
            }
            
            //Insertamos las localizaciones
            NeighborhoodDAO::deleteAllRequestNeighborhood($newRequest->getId_request());
            if(!empty($_POST['id_neighborhood'])) {
                foreach($_POST['id_neighborhood'] as $auxlocation) {
                    NeighborhoodDAO::createRequestNeighborhood($newRequest->getId_request(), $auxlocation);
                }
            }
            
            //Insertamos specialists
            PlaydateRequestDAO::deleteAllRequestSpecialist($newRequest->getId_request());
            if(!empty($_POST['id_specialist'])) {
                foreach($_POST['id_specialist'] as $auxSpec) {
                    PlaydateRequestDAO::addSpecialistToParentRequest($newRequest->getId_request(), $auxSpec);
                }
            }
            
            //Aplicamos las reservas solicitadsa
            //Borramos las antiguas plazas reservadas antes de crear las que nos mandan
            PlaydateRequestDAO::deleteAllRequestChildren($newRequest->getId_request());
            
            if(!empty($_POST['selectedchildren'])) {
                foreach($_POST['selectedchildren'] as $auxchild) {
                    $kid = ChildrenDAO::getChildren($auxchild);
                    if($kid!=null) {
                        PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), $kid->getId_children(), 1, $kid->getAge()); //Added age and numchildren
                    }
                }
            }
            
            if(!empty($_POST['num_kids'])) {
                $i=0;
                foreach($_POST['num_kids'] as $auxnum) {
                    $numKids = $auxnum;
                    $age= $_POST['ages'][$i];
                    
                    if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                        PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), null, $numKids, $age); //Added age and numchildren, without id_children
                    }
                    $i++;
                }
            }
            
            
        }
        
        header("Location: /my-dashboard.php#requests");
        
        break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit Request - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page request-playdate full-day<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Change a Request</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo (($parent->getPicture()==null)?"/img/img-profile.jpg":$parent->getPicture()) ?>" alt="Picture of <?php echo $parent->getFullName() ?>">
                </div>
                <div class="info">
                  <a href="parent-public-profile.html" class="title"><?php echo $parent->getName() ?></a>
                  <span class="subtitle"><?php echo $parent->getUsername() ?></span>
                  <span class="subtitle members">Chelsea, NYC</span>
                </div>
                <div class="data">
                  <p class="titulo">Groups</p>
                  <?php 
                  $parentGrps = GroupDAO::getGroupsByParent($parent->getId_parent());
                  foreach ($parentGrps as $pGrp) {
                  ?>
                  <p><a href="/groups-dashboard.php?id_group=<?php echo $pGrp->getId_group() ?>" style="text-decoration:none;"><?php echo $pGrp->getName() ?></a></p>
                  <?php } ?>
                </div>
                <div class="data">
                  <p class="titulo">Children</p>
				  <?php 
				  $childsParent = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                  foreach ($childsParent as $pchild) {
                      $genderimg = "img/ico/main/0299-baby2.svg";
                      if($pchild->getGenre()=="girl") {
                          $genderimg = "img/ico/main/0298-baby.svg";
                      }
                  ?>
                  <p class="children"><img src="<?php echo $genderimg ?>" width="50" alt=""/> <span class="name"><?php echo $pchild->getName() ?></span> | <span class="age"><?php echo $pchild->getAge() ?></span></p>
                  <?php 
                  } 
                  ?>                  

                </div>
                <div class="data">
                  <p class="titulo">Availability</p>
                  <p><span class="day">Mon</span> <span class="hour"><?php echo(($parent->getChildren_avb_mon_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_init()):"")?> - <?php echo(($parent->getChildren_avb_mon_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_end()):"")?></span></p>
                  <p><span class="day">Tue</span> <span class="hour"><?php echo(($parent->getChildren_avb_tue_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_init()):"")?> - <?php echo(($parent->getChildren_avb_tue_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_end()):"")?></span></p>
                  <p><span class="day">Wed</span> <span class="hour"><?php echo(($parent->getChildren_avb_wed_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_init()):"")?> - <?php echo(($parent->getChildren_avb_wed_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_end()):"")?></span></p>
                  <p><span class="day">Thu</span> <span class="hour"><?php echo(($parent->getChildren_avb_thu_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_init()):"")?> - <?php echo(($parent->getChildren_avb_thu_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_end()):"")?></span></p>
                  <p><span class="day">Fri</span> <span class="hour"><?php echo(($parent->getChildren_avb_fri_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_init()):"")?> - <?php echo(($parent->getChildren_avb_fri_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_end()):"")?></span></p>
                  <p><span class="day">Sat</span> <span class="hour"><?php echo(($parent->getChildren_avb_sat_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_init()):"")?> - <?php echo(($parent->getChildren_avb_sat_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_end()):"")?></span></p>
                  <p><span class="day">Sun</span> <span class="hour"><?php echo(($parent->getChildren_avb_sun_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_init()):"")?> - <?php echo(($parent->getChildren_avb_sun_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_end()):"")?></span></p>
                </div>
              </div>

			  <?php /*?>
              <div class="data playdate-near">
                <p class="titulo">Playdates near you</p>
                <div class="info-near">
                  <a href="#" title="">Bronx Day Trip</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
                <div class="info-near">
                  <a href="#" title="">Dance Party</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
                <div class="info-near">
                  <a href="#" title="">Met Photo Exhibit</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
              </div>
              <?php */ ?>


        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <form class="form-request-playdate" id="frm_change_request" action="/parent-edit-request.php" method="post">
          	<input type="hidden" name="operation" value="update" /> 
          	<input type="hidden" name="id_parent" value="<?php echo $parent->getId_parent() ?>" />
          	<input type="hidden" name="id_request" value="<?php echo $request->getId_request() ?>" />
            <div class="row">

              <div class="col-lg-4 type-of-care">
                <p class="titulo">Type of care</p>
                
                  <?php 
                  $types = TypecareDAO::getTypecareList();
                  foreach ($types as $auxType) {
                  ?>
              	  <div class="form-group">
                    <input type="radio" name="type" id="<?php echo $auxType->getId_typecare()?>" value="<?php echo $auxType->getId_typecare()?>" class="form-control" placeholder="" <?php echo(($auxType->getId_typecare()==$request->getType())?"checked=\"checked\"":"") ?>>
                    <label for="<?php echo $auxType->getId_typecare()?>"><?php echo $auxType->getName() ?> 
                    	<?php if($auxType->getDescription()!=null) { ?>
                    	<a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="<?php echo $auxType->getDescription() ?>"><i class="fa fa-question-circle"></i></a>
                    	<?php } ?>
                    </label>
                  </div> 
              	  <?php 
                  }
              	  ?>

              </div>

              <div class="col-lg-4">
                <p class="titulo">Location</p>
                <label for="location" class="sr-only">Location</label>
                <input type="text" id="location" name="location" class="form-control noMarginTop inputLocation" placeholder="neighborhood">
                <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                <div class="container-tags">
                <?php
                $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByRequest($request->getId_request());
                foreach ($neighborhoods as $auxItem) {
                ?>
                  <div class="tag">
                    <span><?php echo $auxItem->getName()?></span>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                    <input type="hidden" value="<?php echo $auxItem->getId_neighborhood() ?>" name="id_neighborhood[]">
                  </div>
                
                <?php 
                }
                ?>
                
                </div>
              </div>

              <div class="col-lg-4 addons">
              <p class="titulo">Supervised Travel</p>
              <div class="form-group">
                <label for="travel_pickup" class="sr-only">Pick up address * optional</label>
                <input type="text" id="travel_pickup" name="travel_pickup" class="form-control noMarginTop" placeholder="Pick up * optional" value="<?php echo($request->getTravel_pickup()) ?>" >
              </div>
               <div class="form-group">
                <label for="travel_dropoff" class="sr-only">Drop off address * optional</label>
                <input type="text" id="travel_dropoff" name="travel_dropoff" class="form-control noMarginTop" placeholder="Drop off * optional"  value="<?php echo($request->getTravel_dropoff()) ?>">
              </div>
            	</div>

            </div>
            <div class="row row-tabs">

              <div class="col-lg-4 date-range">
                <p class="titulo">Date</p>
                  <div class="input-group">
                    <input type="text" placeholder="DD/MM/YYYY" name="date" class="form-control datepicker" id="dp1524041141130" value="<?php echo($request->getDate()) ?>">
                  </div>

              </div>

              <div class="col-lg-4">
                <p class="titulo">Time</p>
                <div id="time-container-profile">
                  <div class="day-time">
                    <div class="input-group">
                      <div class="selectTime from">
                          <?php 
                          $fieldName = "time_init";
                          $fieldValue = $request->getTime_init();
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                      </div>
                      
                    </div>
                    <span> - </span>
                    <div class="input-group">
                      <div class="selectTime from">
                          <?php 
                          $fieldName = "time_end";
                          $fieldValue = $request->getTime_end();
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              

              <div class="col-lg-4">
              <p class="titulo">Activity Budget <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="The Activity Budget is only related to the fees associated with third party providers and attractions such as admissions for children and caregivers. This can also be used for expenses associated with extended travel, additional supplies, meals, and/or decor requested by parents. "><i class="fa fa-question-circle"></i></a></p>
              <div id="budget-container">
                <div class="input-group">
                  <input type="text" placeholder="$30" name="budget_init" class="form-control  input-budget" value="<?php echo($request->getBudget_init()) ?>">
                </div>
                <span> - </span>
                <div class="input-group">
                  <input type="text" placeholder="$50" name="budget_end" class="form-control  input-budget" value="<?php echo($request->getBudget_end()) ?>">
                </div>
              </div>
              </div>
            </div>

          <div class="row row-tabs">
            <div class="col-lg-4 children">
              <p class="titulo">Children</p>
              <?php 
                  $childsRequest = ChildrenDAO::getChildrenListByRequest($request->getId_request());
              
                  $childsParent = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                  foreach ($childsParent as $pchild) {
                      $chSelected = false;
                      foreach($childsRequest as $chReq) {
                          if($chReq->getId_children() == $pchild->getId_children()) {$chSelected=true;}
                      }
                      $genderimg = "img/ico/main/0299-baby2.svg";
                      if($pchild->getGenre()=="girl") {
                          $genderimg = "img/ico/main/0298-baby.svg";
                      }
                  ?>
                  
              <div class="form-group">
                <input type="checkbox" id="child1" class="form-control" placeholder="keyword" name="selectedchildren[]" value="<?php echo $pchild->getId_children() ?>" <?php echo(($chSelected)?"checked=\"checked\"":"")?>>
                <label for="child1"><?php echo $pchild->getName() ?></label>
              </div>
              <?php } ?>
              
            </div>
            <div class="col-lg-4 addons">
              <p class="titulo">EXTRA KIDS <span class="optional">*optional</span></p>
              		  <?php 
              		  $childrenRequests = PlaydateRequestDAO::getChildrenRequests($request->getId_request());
              		  
              		  foreach ($childrenRequests as $auxReq) {
              		      if($auxReq->getId_children()==null) {
              		          
              		  ?>
              	 	  <div class="row row-child-detail">
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="num_kids[]">
                                    <option value="-1">Kids</option>
                                    <option value="1" <?php echo(($auxReq->getNum_children()==1)?"selected=\"selected\"":"") ?>>1 kid</option>
                                    <option value="2" <?php echo(($auxReq->getNum_children()==2)?"selected=\"selected\"":"") ?>>2 kids</option>
                                    <option value="3" <?php echo(($auxReq->getNum_children()==3)?"selected=\"selected\"":"") ?>>3 kids</option>
                                    <option value="4" <?php echo(($auxReq->getNum_children()==4)?"selected=\"selected\"":"") ?>>4 kids</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="ages[]">
                                    <option value="-1">Age</option>
                                    <option value="1" <?php echo(($auxReq->getAge()==1)?"selected=\"selected\"":"") ?>>1 year</option>
                                    <option value="2" <?php echo(($auxReq->getAge()==2)?"selected=\"selected\"":"") ?>>2 years</option>
                                    <option value="3" <?php echo(($auxReq->getAge()==3)?"selected=\"selected\"":"") ?>>3 years</option>
                                    <option value="4" <?php echo(($auxReq->getAge()==4)?"selected=\"selected\"":"") ?>>4 years</option>
                                    <option value="5" <?php echo(($auxReq->getAge()==5)?"selected=\"selected\"":"") ?>>5 years</option>
                                    <option value="6" <?php echo(($auxReq->getAge()==6)?"selected=\"selected\"":"") ?>>6 years</option>
                                    <option value="7" <?php echo(($auxReq->getAge()==7)?"selected=\"selected\"":"") ?>>7 years</option>
                                    <option value="8" <?php echo(($auxReq->getAge()==8)?"selected=\"selected\"":"") ?>>8 years</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          
                      </div>
                      <?php 
              		      } 
              		  }
              		  ?>
                      
                      <div class="row row-child-detail">
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="num_kids[]">
                                    <option value="-1">Kids</option>
                                    <option value="1">1 kid</option>
                                    <option value="2">2 kids</option>
                                    <option value="3">3 kids</option>
                                    <option value="4">4 kids</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="ages[]">
                                    <option value="-1">Age</option>
                                    <option value="1">1 year</option>
                                    <option value="2">2 years</option>
                                    <option value="3">3 years</option>
                                    <option value="4">4 years</option>
                                    <option value="5">5 years</option>
                                    <option value="6">6 years</option>
                                    <option value="7">7 years</option>
                                    <option value="8">8 years</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-2">
                            <a href="javascript:;" class="bt-add-kid-detail"><img src="img/ico/main/0860-plus-circle.svg"/></a>
                          </div>
                      </div>

            </div>

            <div class="col-lg-4">
                  <p class="titulo">Invite Group <span class="optional">*optional</span></p>
                  <label for="invite_group" class="sr-only">Invite Group</label>
                  <input type="text" id="inputSelectGroup" name="invite_group" class="form-control noMarginTop" placeholder="Group Name" value="<?php echo $groupName ?>">
                  <?php /*?>
                  <div class="container-tags">
                    <div class="tag">
                      <span>Dance 1001</span>
                      <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                    </div>
                  </div>
                  */?>
              </div>

          </div>

          <div class="row row-tabs">
            <div class="col-lg-8">                    
              <p class="titulo">Additional info <span class="optional">*optional</span></p>
              <label for="info" class="sr-only">Additional info</label>
              <textarea class="form-control" placeholder="Anything else we should know?" id="info" name="info" rows="22"><?php echo $request->getInfo() ?></textarea>                
            </div>
            <div class="col-lg-4">
              <div class="row">
                <p class="titulo">Categories <span class="optional">*optional</span></p>
                <div class="col">
                  <div class="collapse multi-collapse show" id="categories-container">
                    
					  <?php $interests = InterestDAO::getInterestsIdByRequest($request->getId_request()); ?>
                      <div class="list-container stem">
                        <div class="form-group">
                          <input type="checkbox" id="1" value="1" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(1, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="1">Academic</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="2" value="2" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(2, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="2">STEM</label>
                        </div> 
                      </div>
                      
                      <div class="list-container creative">
                        <div class="form-group">
                          <input type="checkbox" id="3" value="3" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(3, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="3">Creative</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="4" value="4" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(4, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="4">Performing Arts</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="5" value="5" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(5, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="5">Language &amp; Culture</label>
                        </div> 
                      </div>

                      <div class="list-container outdoor">
                        <div class="form-group">
                          <input type="checkbox" id="6" value="6" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(6, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="6">Play</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="7" value="7" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(7, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="7">Sports &amp; Recreation</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="8" value="8" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(8, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="8">Health &amp; Wellness</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="9" value="9" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(9, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="9">Outdoor</label>
                        </div>
                      </div>

                      <div class="list-container attractions">
                        <div class="form-group">
                          <input type="checkbox" id="10" value="10" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(10, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="10">NYC Museums</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="11" value="11" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(11, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="11">NYC Attractions</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="12" value="12" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(12, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="12">NYC Theater</label>
                        </div> 
                      </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="row row-tabs">
            <div class="col-lg-8">
              <p class="titulo">Specialists <span>*optional</span></p>
              <div class="row">
                <div class="col-lg-12">
                <label for="inputSpecialists" class="sr-only">Name</label>
                  <input type="text" id="inputSpecialists" class="form-control noMarginTop inputSpecialist" placeholder="name">
                  <a href="javascript:;" class="bt-save-profile last bt-add-specialist" data-toggle="modal" data-target="#playdate-reserved">Add Specialist</a>
                </div>
              </div>
              
              <div class="row">                  
                <div class="col-lg-12">
					<div class="container-specialist-tags row-specialist">  
                    <?php
                    $specialists = SpecialistDAO::getSpecialistsListByRequest($request->getId_request());
                    foreach ($specialists as $auxSpec) {
                        $pic = "/img/img-profile.jpg";
                        if(($auxSpec->getPicture()!=null) && ($auxSpec->getPicture()!="")) {
                            $pic = $auxSpec->getPicture();
                        }
                    ?>
                	<div class="specialist-tag">
                      <img src="<?php echo $pic ?>">
                      <a href="/specialist-profile.php?id_specialist=<?php echo $auxSpec->getId_specialist() ?>" title="<?php echo $auxSpec->getFullName()?>"><?php echo $auxSpec->getFullName()?></a>
                      <a class="bt-delete-tag" href="javascript:;" title="Remove specialist">X</a>
                      <input type="hidden" name="id_specialist[]" value='<?php echo $auxSpec->getId_specialist(); ?>'/>
                    </div>
                
                    <?php 
                    }
                    ?>
                	</div>  
                  
                </div>
              </div>

              
            </div>
            <div class="col-lg-4">
              <p class="titulo">Supporters <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Under 3 - Nurse Care without Parents, coming soon!. 3-4 yrs - 4 children to 1 adult. 5-7 yrs - 5 children to 1 adult. +7 or Under 3 Parent Social - 6 children to 1 adult. *Additional Support Specialists can be hired with an add-on fee. PAL suggests an extra Specialist for children under 7 for playdates at public attractions."><i class="fa fa-question-circle"></i></a></p>
              <div class="selectdiv">
                <label for="num_supporters">
                  <select class="form-control" id="num_supporters" name="num_supporters">
                    <option>0</option>
                    <option value="1" <?php echo(($request->getNum_supporters()==1)?"selected=\"selected\"":"") ?>>1</option>
                    <option value="2" <?php echo(($request->getNum_supporters()==2)?"selected=\"selected\"":"") ?>>2</option>
                    <option value="3" <?php echo(($request->getNum_supporters()==3)?"selected=\"selected\"":"") ?>>3</option>
                    <option value="4" <?php echo(($request->getNum_supporters()==4)?"selected=\"selected\"":"") ?>>4</option>
                    <option value="5" <?php echo(($request->getNum_supporters()==5)?"selected=\"selected\"":"") ?>>5</option>
                  </select>
                </label>
              </div>
            </div>
          </div>

          <div class="row  row-tabs">
            <div class="col-lg-12">
              <div class="form-group">
                <div class="checkAccept">
                  <input type="checkbox" id="public_job_board" name="public_job_board" class="form-control" placeholder="0" value="1" <?php echo(($request->getPublic_job_board()>0)?"checked=\"checked\"":"") ?>>
                  <label for="public_job_board">Publish Playdate in Specialist Job Board so all specialists may apply</label>
                </div>
              </div>
            </div>
          </div>

          <div class="row  row-tabs">
            <div class="col-lg-12">
              <button class="btn btn-md btn-primary bt-save-profile last" type="submit">Save changes</button>
            </div>
          </div>


          </form>
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

<?php 
/**
 * PLAYDATE - SPECIALIST PUBLIC PROFILE 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Specialist Profile - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS --> 
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page submit-itinerary public-profile<?php echo($isConnected?" connected":"") ?>">
    
	<?php include ('components/menu.php'); ?>
	
    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Monica</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="img/img-box-specialist.jpg" alt="">
                </div>
                <div class="info">
                  <span class="title">Monica Allen</span>
                  <span class="subtitle">lead</span>
                </div>
                <div class="puntuacion">
                  <ul>
                    <li class="active"></li>
                    <li class="active"></li>
                    <li class="active"></li>
                    <li class="active"></li>
                    <li class="active"></li>
                  </ul>
                  <span class="num">5/5</span>
                </div>
                <div class="separator"></div>
                <div class="data certifications">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="info check"><span>background check</span></div>
                    </div>
                     <div class="col-lg-6">
                      <div class="info certificate"><span>approved by PAL</span></div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="info family"><span>100 repeat families</span></div>
                    </div>
                     <div class="col-lg-6">
                      <div class="info playdate"><span>300 playdates</span></div>
                    </div>
                  </div>
                </div>
              </div>



        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">

          <div class="row">

            

            <div class="col-lg-4">
              <p class="titulo">Profile</p>
              <div class="data">
                <p><strong class="big">Monica</strong></p>
                <p>Lead</p>
                <p>Chelsea, NY, Soho, NY</p>
              </div>       
            </div>
            <div class="col-lg-4 ">
             <p class="titulo uppercase">Languages</p>
              <div class="data">
                <p>English</p>
                <p>Spanish</p>
              </div>                  
                              
            </div>
            <div class="col-lg-4">
              <button onclick="window.location.href='request-playdate.html'" class="btn btn-md btn-primary bt-save-profile" type="button">Request Playdate</button>
              <a href="mailto:parentconcierge@playdatepal.com" class="bt-save-profile bt-concierge">Get in touch with Concierge</a>
            </div>
          </div>

          <div class="row row-tabs">

            

            <div class="col-lg-8">
              <p class="titulo">About Me</p>
              <div class="data">
                <p>Hi my name is Monica and I am a music and drama department head at Camp Wawenock, an all-girls sleep-away camp. I have also conducted drama activities and directed plays with girls from age 8 to 15. I graduated at Mason Gross School of Arts. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>       
            </div>
            <div class="col-lg-4">
              <p class="titulo">Experience</p>
              <div class="data">
                <p><strong>Under 1 year old</strong></p>
                <p>2 years of experience</p>
              </div> 
              <div class="data">
                <p><strong>2-3 year old</strong></p>
                <p>5 years of experience</p>
              </div> 
              <div class="data">
                <p><strong>4+ year old</strong></p>
                <p>3 years of experience</p>
              </div> 
            </div>
          </div>



          <div class="row row-tabs">

             <div class="col-lg-12">
              <p class="titulo uppercase">Certifications</p>
              <div class="box-certification">
                 <img src="img/ico/main/0219-graduation-hat.svg" width="50" alt=""/><span>Undergraduate Degree in Nursing</span>
              </div>
              <div class="box-certification">
                <img src="img/ico/main/0489-heart-pulse.svg" width="50" alt=""/><span>Red Cross Special Education Certification</span>
              </div>
              <div class="box-certification">
                <img src="img/ico/main/0275-book.svg" width="50" alt=""/><span>Teaching Second Language</span>
              </div> 
            </div>
          </div>

            <div class="row row-tabs">

            

            <div class="col-lg-8">
              <p class="titulo">Additional</p>
              <p>Special Needs</p>
              <p>Diabetic Children</p>
            </div>
            <div class="col-lg-4">
              <p  class="titulo">Groups</p>             
              <div class="data">
                <p><a href="group-public-profile.html" title="">Elementary School Chelsea</a></p>
                <p><a href="group-public-profile.html" title="">Dance 10001</a></p>
                <p><a href="group-public-profile.html" title="">Park Slope Parents</a></p>
              </div> 
            </div>
          </div>

          <div class="row row-tabs">

            <div class="col-lg-12">
              <p class="titulo centered">Upcoming Playdates Led by Monica</p>
              <div class="row">
                <div class="col-lg-4">
                  <div class="box-playdates attractions">
                    <div class="img-container">
                      <img src="img/box-home-1.jpg" alt="">
                      <span class="cat-box">NYC Attractions</span>
                    </div>
                    <div class="time">
                      <span class="date">Jan 13th 2018</span>
                      <span class="hour">12pm-2pm</span>
                    </div>
                    <div class="info">
                      <span class="title">Bronx Zoo Day Trip</span>
                      <span class="subtitle">Bronx, NYC</span>
                    </div>
                     <div class="data">
                      <div class="age">Ages: <span>7-8</span></div>
                      <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                      <div class="specialists">Specialist: <span>Anna Smith</span></div>
                    </div>
                    <div class="reserve">
                      <a href="reserve-playdate.html" class="bt-reserve">Reserve Playdate</a>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="box-playdates creative">
                    <div class="img-container">
                      <img src="img/box-home-2.jpg" alt="">
                      <span class="cat-box">Creative</span>
                    </div>
                    <div class="time">
                      <span class="date">Jan 13th 2018</span>
                      <span class="hour">12pm-2pm</span>
                    </div>
                    <div class="info">
                      <span class="title">Color exploring</span>
                      <span class="subtitle">Bronx, NYC</span>
                    </div>
                     <div class="data">
                      <div class="age">Ages: <span>7-8</span></div>
                      <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                      <div class="specialists">Specialist: <span>Anna Smith</span></div>
                    </div>
                    <div class="reserve">
                      <a href="reserve-playdate.html" class="bt-reserve">Reserve Playdate</a>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="box-playdates outdoor">
                    <div class="img-container">
                      <img src="img/box-home-3.jpg" alt="">
                      <span class="cat-box">Outdoor</span>
                    </div>
                    <div class="time">
                      <span class="date">Jan 13th 2018</span>
                      <span class="hour">12pm-2pm</span>
                    </div>
                    <div class="info">
                      <span class="title">Outdoors we go</span>
                      <span class="subtitle">Bronx, NYC</span>
                    </div>
                     <div class="data">
                      <div class="age">Ages: <span>7-8</span></div>
                      <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                      <div class="specialists">Specialist: <span>Anna Smith</span></div>
                    </div>
                    <div class="reserve">
                      <a href="reserve-playdate.html" class="bt-reserve">Reserve Playdate</a>
                    </div>
                  </div>
                </div>

              </div>      
            </div>
          </div>

          <div class="row row-tabs">

            <div class="col-lg-12">
              <p class="titulo centered">Past Playdates Led by Monica</p>
              <div class="row">
                <div class="col-lg-4">
                  <div class="box-playdates attractions">
                    <div class="img-container">
                      <img src="img/box-home-1.jpg" alt="">
                      <span class="cat-box">NYC Attractions</span>
                    </div>
                    <div class="time">
                      <span class="date">Jan 13th 2018</span>
                      <span class="hour">12pm-2pm</span>
                    </div>
                    <div class="info">
                      <span class="title">Bronx Zoo Day Trip</span>
                      <span class="subtitle">Bronx, NYC</span>
                    </div>
                     <div class="data">
                      <div class="age">Ages: <span>7-8</span></div>
                      <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                      <div class="specialists">Specialist: <span>Anna Smith</span></div>
                    </div>
                    <div class="reserve">
                      <a href="view-playdate.html" class="bt-reserve">View Playdate</a>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="box-playdates creative">
                    <div class="img-container">
                      <img src="img/box-home-2.jpg" alt="">
                      <span class="cat-box">Creative</span>
                    </div>
                    <div class="time">
                      <span class="date">Jan 13th 2018</span>
                      <span class="hour">12pm-2pm</span>
                    </div>
                    <div class="info">
                      <span class="title">Color exploring</span>
                      <span class="subtitle">Bronx, NYC</span>
                    </div>
                     <div class="data">
                      <div class="age">Ages: <span>7-8</span></div>
                      <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                      <div class="specialists">Specialist: <span>Anna Smith</span></div>
                    </div>
                    <div class="reserve">
                      <a href="view-playdate.html" class="bt-reserve">View Playdate</a>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="box-playdates outdoor">
                    <div class="img-container">
                      <img src="img/box-home-3.jpg" alt="">
                      <span class="cat-box">Outdoor</span>
                    </div>
                    <div class="time">
                      <span class="date">Jan 13th 2018</span>
                      <span class="hour">12pm-2pm</span>
                    </div>
                    <div class="info">
                      <span class="title">Outdoors we go</span>
                      <span class="subtitle">Bronx, NYC</span>
                    </div>
                     <div class="data">
                      <div class="age">Ages: <span>7-8</span></div>
                      <div class="participants">Participants: <span>5 / 1 open spot</span></div>
                      <div class="specialists">Specialist: <span>Anna Smith</span></div>
                    </div>
                    <div class="reserve">
                      <a href="view-playdate.html" class="bt-reserve">View Playdate</a>
                    </div>
                  </div>
                </div>
                
              </div>      
            </div>
          </div>


        </div>
        <!-- Content -->

      </div>


    </div>

    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

<?php 
/**
 * PLAYDATE - HOME 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$backUrl = "/help-center.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=1.2" rel="stylesheet">

  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Help Center</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row help-center">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar">
          <div class="row">
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type general">
                <img src="img/ico/white/0003-home3.svg" alt="" />
                <span class="texto">General</span>
              </a>
            </div>
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type account">
                <img src="img/ico/white/0287-user.svg" alt="" />
                <span class="texto">Account</span>
              </a>
            </div>            
          </div>
          <div class="row">
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type parents">
                <img src="img/ico/white/0299-baby2.svg" alt="" />
                <span class="texto">Parents</span>
              </a>
            </div>
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type providers">
                <img src="img/ico/white/0215-reading.svg" alt="" />
                <span class="texto">Providers</span>
              </a>
            </div>
            
          </div>
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9 all-faqs">

          <div class="col-lg-12">
            <div class="no-results"><img src="img/cactus.png"/>
              <p>No faqs available</p>
            </div>
          </div>


          <div id="accordion">

            <div class="card general">
              <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Where is PAL available?
                  </button>
                </h5>
              </div>
              <div id="collapseOne" class="collapse  show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                  <p>Currently Playdate is only available in the New York City Area. We will be expanding to additional cities in the near future. If you’d like to nominate your city, click here.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading2">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    What is a PAL Group?
                  </button>
                </h5>
              </div>
              <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                <div class="card-body">
                  <p>"A PAL group is a public or private community of parents with shared child care needs administered by parents or 3rd party partners. Group administrators are authorized to manage group participation and child care planning.</p>
                  <p>You can start a group Playdate group by clicking here. You can join a group by clicking here."</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading3">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                    What is the difference between a Lead and Support Specialist?
                  </button>
                </h5>
              </div>
              <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                <div class="card-body">
                  <p>A Support has successfully completed the Playdate vetting process. This includes a background check, personality test, reference check, online application and online interview. A Specialist, in addition to completing the vetting process, has received his or her First/Aid CPR Certification. He or she has pursued continued learning as a child care provider. A Specialist has also completed at least 10 hours in a Support role, received consistent positive feedback from parents, and approval from another pre-approved Specialist. Every Playdate is required to have at least one Specialist assigned to facilitate the experience.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading4">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                    Can you help me find a place to host my Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                <div class="card-body">
                  <p>Third party hosts can be found in the Groups section of the Playdate platform. You can join these groups to inquire about a Playdate you are interested in planning at their site. We also recommend considering the following: a public attraction, local kid-friendly restaurant, local playground, in-home, or in-school (if approved) Playdate location.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading5">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                    What is the adult to child ratio?
                  </button>
                </h5>
              </div>
              <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                <div class="card-body">
                  <img src="img/img-suggested-ratio.jpg"/>
                  <p>Insert picture from About Us page. These are suggested ratios. Parents or providers can request an additional support at an additional cost.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading6">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                    What is the vetting process for child care providers?
                  </button>
                </h5>
              </div>
              <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                <div class="card-body">
                  <p>"For all roles -</p>
                  <p>Online Application Day100 Personality Test Day100 Reference Check Online or In-Person Interview Background Check.</p>
                  <p>After passing these 5 steps, providers are welcome to make a profile and apply for Support Roles. They will be unable to design their own playdates or apply for Lead Specialist positions.</p>
                  <p>To become a Playdate Lead Specialist, all candidates must:</p>
                  <p>Complete 10 hours work in the Support Role Receive a First Aid and CPR Certification Complete Continued Learning Approval from Playdate Lead and Consisted Positive Feedback from Parents"</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading7">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                    Does Playdate provide a W-2 or 1099 to Playdate providers?
                  </button>
                </h5>
              </div>
              <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                <div class="card-body">
                  <p>No. Tax filings for independent contractors are the responsibility of parents and providers connecting on the Playdate platform.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading8">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                    I have a business that provides kids enrichment or hosts playdates, how do I become a partner?
                  </button>
                </h5>
              </div>
              <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                <div class="card-body">
                  <p>See our Work with Us page for more information.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading9">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                    How do I flag someone's account to Support?
                  </button>
                </h5>
              </div>
              <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
                <div class="card-body">
                  <p>Contact the Playdate support team directly with any suspicious content that may violate our Terms. Once the Support Team is notified of these flags, we will take any action that may be necessary, and follows up with the member who flagged as soon as possible. Flags are totally confidential - rest assured that the member you flag will not be notified of your flag to Support.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading10">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                    Why was my account suspended?
                  </button>
                </h5>
              </div>
              <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
                <div class="card-body">
                  <p>To protect the member experience on Playdate Labs, Inc., Playdate Labs reserves the right, in its full discretion, to take corrective action on members' accounts. Our reliability policy is in place in order to maintain the quality of the Playdate platform and to ensure that members can trust the reliability of others While we recognize that life does happen, members deemed to be unreliable may have action taken on their account, including permanent account closure. These members would be removed from Playdate for being in violation for our Terms of Service and Community Guidelines.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading11">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                    How do you address no-shows or reliability concerns?
                  </button>
                </h5>
              </div>
              <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                <div class="card-body">
                  <p>"Not showing up to an accepted booking, especially without communication, is not acceptable conduct on Playdate. If a parent or sitter is a no-show for a booking, it should be reported as soon as possible by contacting Playdate Support or by flagging the member's profile. Our Team takes action in regards to every reported no-show.</p>
                  <p>We review all cancellations and no-shows in detail and use all the information available to us to determine if a cancellation or no-show will warrant action taken on the member's account. Each situation merits its own review, but we abide by the following guidelines in our review process to determine which reliability concerns are problematic.</p>
                  <p>When did the member cancel? The closer to the date and time of the job that you cancel, the more concerning it is, with no-shows being the most serious.</p>
                  <p>Is this a first-time booking? The fewer times the parent and sitter have worked together, the more concerning the cancellation is — cancelling on your first booking with a parent or sitter is the most serious.</p>
                  <p>What was the reason given? We evaluate cancellations and no-shows based on the reason provided by the member.</p>
                  <p>To protect the member experience on Playdate, Playdate reserves the right, in its full discretion, to take corrective action on members' accounts. Our Reliability Policy is in place in order to maintain the quality of the Playdate platform and to ensure that members can trust the reliability of others.</p>
                  <p>While we recognize that life does happen, members deemed to be unreliable may have action taken on their account, including permanent account closure. These members would be removed from Playdate for being in violation for our Terms of Service and Community Guidelines."</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading12">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                    What if the hours worked by a Specialist do not line up with the hours booked online?
                  </button>
                </h5>
              </div>
              <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                <div class="card-body">
                  <p>If a Specialist has worked over time or under time, either the Specialist or Parent should contact support directly. We will facilitate a refund for over-pay or an invoice if under-paid.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading13">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                    How do I sign up? (Parent)
                  </button>
                </h5>
              </div>
              <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion">
                <div class="card-body">
                  <p>"Playdate is an invite only platform. You will need to receive an access code from a friend already on the Playdate platform to register. You can request an invitation by clicking here.</p>
                  <p>When requesting an invitation, you will need to verify your identity by submitting additional information via email. It's free to sign up!"</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading14">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                    What information do I need to Create an Account (Parent)
                  </button>
                </h5>
              </div>
              <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
                <div class="card-body">
                  <p>"Account information required includes name, photo, neighborhood, and basic information around your child care needs. Your credit card will not be filed.</p>
                  <p>We respect your privacy and do not post any information publicly without your permission.</p>
                  <p>Ready to get started on PAL? Click here!"</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading15">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                    How do I request a Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
                <div class="card-body">
                  <p>Provide your basic child care needs by Requesting a Playdate (hyperlink) and this will post to our private job board, notifying our lead Playdate Specialists available for care in your area. You will receive a notification every time a Specialist applies for your job opening. Once you hire your caregiver, the Playdate will open up to other families within your personal network for booking.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading16">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                    How do I join a Private PAL Group?
                  </button>
                </h5>
              </div>
              <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordion">
                <div class="card-body">
                  <p>To gain access to a Private Playdate group, you must be invited by the Group administrator. You can request an invitation by contacting the Group administrator directly.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading17">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                    What if I cannot find a Specialist to work my Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordion">
                <div class="card-body">
                  <p>Keep the job open on the job board and email <a href="mailto:support@playdatepal.com">support@playdatepal.com</a> We will do our best to assist you. We recommend a lead time of one week to find a Specialist available for your job.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading18">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
                    What if I cannot find another family to match my Playdate by the time it expires?
                  </button>
                </h5>
              </div>
              <div id="collapse18" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                <div class="card-body">
                  <p>"You have two (2) options!</p>
                  <p>Repost this Playdate - Your provider agrees to hold their time for another 48 hours or until you find a match. Pay Social, Go Solo - You need high quality care fast and you are not interested in a waiting for a match. You can pay the rate of two kids, for one."</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading19">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse19" aria-expanded="false" aria-controls="collapse19">
                    What if my provider can no longer work the Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse19" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                <div class="card-body">
                  <p>If your provider needs to cancel after the RSVP date has passed, your provider will be encouraged to find a substitute Specialist or Supporter BEFORE cancellation. If they cancel before the RSVP date, we will post your request back on the job board and help you find another provider. A Specialist can cancel after the Playdate RSVP date expires without finding a substitute IF you have not found a match.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading20">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse20" aria-expanded="false" aria-controls="collapse20">
                    Is there a discount for siblings?
                  </button>
                </h5>
              </div>
              <div id="collapse20" class="collapse" aria-labelledby="heading20" data-parent="#accordion">
                <div class="card-body">
                  <p>The second child in the family will receive a 25% discount.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading21">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse21" aria-expanded="false" aria-controls="collapse21">
                    Can I pay for other participating children in my Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse21" class="collapse" aria-labelledby="heading21" data-parent="#accordion">
                <div class="card-body">
                  <p>Yes, you can do so by reserving more spots for the Playdate.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading22">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapse22">
                    How do I pay for a Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#accordion">
                <div class="card-body">
                  <p>You can purchase a Play Pack and these credits can then be used towards any Playdate reservation. You can also simply RSVP for any playdate in the marketplace. The charge on your card will be at held at the highest rate, but only charged after the RSVP date has passed at the correct rate per hour. If you cancel before the RSVP date has passed you will be charged $5. If you cancel after the RSVP date has passed you will be charged $10.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading23">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapse23">
                    What is your cancellation policy? (Parents)
                  </button>
                </h5>
              </div>
              <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                <div class="card-body">
                  <p>There is a cancellation fee of $10 within 48 hours of the Playdate. There is a cancellation fee of $5 within the week.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading24">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse24" aria-expanded="false" aria-controls="collapse24">
                    Does my Play Pack expire?
                  </button>
                </h5>
              </div>
              <div id="collapse24" class="collapse" aria-labelledby="heading24" data-parent="#accordion">
                <div class="card-body">
                  <p>Your Play Pack will expire in a year past the purchase date.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading25">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse25" aria-expanded="false" aria-controls="collapse25">
                    How do I book a Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse25" class="collapse" aria-labelledby="heading25" data-parent="#accordion">
                <div class="card-body">
                  <p>You can book a Playdate by reserving a spot with any open Playdate in the Playdate marketplace. You can also reserve a spot for any playdate you are invited to in one of your groups. Finally, you can book a Playdate by requesting one that is custom to your schedule and child care needs. This will post on our job board, then you will receive inquiries from our specialists available for hire.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading26">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse26" aria-expanded="false" aria-controls="collapse26">
                    How do I invite my friends to join Playdate? (Parent)
                  </button>
                </h5>
              </div>
              <div id="collapse26" class="collapse" aria-labelledby="heading26" data-parent="#accordion">
                <div class="card-body">
                  <p>On every parent’s home dashboard there is a box to input the names and emails of friends you’d like to invite. They will receive an invite from you with a special access code. You can also invite friends to specific playdates by inputting their emails on that playdate’s page.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading27">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse27" aria-expanded="false" aria-controls="collapse27">
                    How do I invite my favorite child care provider to join Playdate? (Parent)
                  </button>
                </h5>
              </div>
              <div id="collapse27" class="collapse" aria-labelledby="heading27" data-parent="#accordion">
                <div class="card-body">
                  <p>On every parent's dashboard there is a tab that says 'Specialists'. Under this tab you can nominate your favorite child care professional.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading28">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse28" aria-expanded="false" aria-controls="collapse28">
                    Can I book travel with Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse28" class="collapse" aria-labelledby="heading28" data-parent="#accordion">
                <div class="card-body">
                  <p>Supervised travel is not available for all open playdates, and is offered on a first come first serve basis when it is available. For open playdates, Specialists or Supports will receive an additional $15 each way for their time. Parents must also book and pay for the cost of travel. If you are designing a private playdate, supervised travel can be included in the playdate itinerary at no additional cost for time. Parents are still required to book and pay for travel accommodations. We also recommend chatting with other parents in your group or playdate to facilitate a carpool or "walkpool" with others attending your playdate.</p>
                </div>
              </div>
            </div>

            <!--<div class="card parents">
              <div class="card-header" id="heading29">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse29" aria-expanded="false" aria-controls="collapse29">
                    Can I book travel with Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse29" class="collapse" aria-labelledby="heading29" data-parent="#accordion">
                <div class="card-body">
                  <p>Supervised travel is not available for all open playdates, and is offered on a first come first serve basis when it is available. For open playdates, Specialists or Supports will receive an additional $15 each way for their time. Parents must also book and pay for the cost of travel. If you are designing a private playdate, supervised travel can be included in the playdate itinerary at no additional cost for time. Parents are still required to book and pay for travel accommodations. We also recommend chatting with other parents in your group or playdate to facilitate a carpool or "walkpool" with others attending your playdate.</p>
                </div>
              </div>
            </div>-->

            <div class="card parents">
              <div class="card-header" id="heading30">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse30" aria-expanded="false" aria-controls="collapse30">
                   Is my credit card information secure?
                  </button>
                </h5>
              </div>
              <div id="collapse30" class="collapse" aria-labelledby="heading30" data-parent="#accordion">
                <div class="card-body">
                  <p>We take security very seriously and have put measures in place to prevent the loss, misuse and alteration of any information under our control. All purchases are transmitted over secure internet connections using SSL (Secure Sockets Layer) encryption technology.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading31">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse31" aria-expanded="false" aria-controls="collapse31">
                    How do I post a review about a Specialist or Playdate experience?
                  </button>
                </h5>
              </div>
              <div id="collapse31" class="collapse" aria-labelledby="heading31" data-parent="#accordion">
                <div class="card-body">
                  <p>"After you have completed a Playdate you will be asked for feedback when you log back in. If you have completed at least one booking with a Playdate Specialist on Playdate Labs, you have the ability to post a review to their profile. A public review will be visible to all users on Playdate Labs, including other parents and the sitter themselves. You also have the option to submit a private review will only be visible to the Playdate Labs support team.</p>
                  <p>To post a review, simply navigate to the sitter’s profile and click on the ‘Review’ button available there. This will take you to a form where you can fill our the star count, description and indicate the visibility setting"</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading32">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse32" aria-expanded="false" aria-controls="collapse32">
                    How do I delete my account? (Parents)
                  </button>
                </h5>
              </div>
              <div id="collapse32" class="collapse" aria-labelledby="heading32" data-parent="#accordion">
                <div class="card-body">
                  <p>Go to your "Playpacks" tab, where you will be able to click on the link to "Hold or cancel your account", which will provide either hold or cancel instructions depending upon what option you choose. Note that all payments are non-refundable. There are no credits or refunds for partially used Play Packs. All unused credits will automatically expire at the end of your annual cycle. When you cancel your profile, all bookings beyond your current billing period will automatically be cancelled.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading33">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse33" aria-expanded="false" aria-controls="collapse33">
                    How much does PAL cost?
                  </button>
                </h5>
              </div>
              <div id="collapse33" class="collapse" aria-labelledby="heading33" data-parent="#accordion">
                <div class="card-body">
                  <p>PAL costs are dependent on the number of kids, activity budget, age group, and hours of care. For every child that signs up the price goes down per parent. Playdates with 3rd party hosts or providers will require activity budgets that can influence the final price listed for your playdate. Other add-ons may include supervised travel, additional supplies, or meals.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading34">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse34" aria-expanded="false" aria-controls="collapse34">
                    How to add a certification
                  </button>
                </h5>
              </div>
              <div id="collapse34" class="collapse" aria-labelledby="heading34" data-parent="#accordion">
                <div class="card-body">
                  <p>To add a certification to your profile, please upload evidence of this certification to your dashboard. Our team will do diligence to approve this certification before it becomes visible to parents.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading35">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse35" aria-expanded="false" aria-controls="collapse35">
                    How do I sign up? (Specialist)
                  </button>
                </h5>
              </div>
              <div id="collapse35" class="collapse" aria-labelledby="heading35" data-parent="#accordion">
                <div class="card-body">
                  <p>If you are interested in being a Specialist on the PAL platform you can submit basic information about yourself including your name, location and why you want to join our network. You will be required to pass our comprehensive vetting process in order to have access to the PAL network. Ready to get started on PAL? Click here!</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading36">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse36" aria-expanded="false" aria-controls="collapse36">
                    What is your cancellation policy? (Providers)
                  </button>
                </h5>
              </div>
              <div id="collapse36" class="collapse" aria-labelledby="heading36" data-parent="#accordion">
                <div class="card-body">
                  <p>"Both parents and sitters are expected to uphold their booking commitments. However, we do understand that life can lead to unexpected circumstances which may cause a member to cancel a booking. If either member needs to cancel in case of an emergency, they should officially 'cancel' the booking through Playdate as soon as possible. Cancellations come at the inconvenience of the other member, so immediately notifying the parents or other providers allows the member to make other arrangements.</p>
                  <p>When canceling a booking, members have the opportunity to select a reason for canceling as well as add a personal note. The reasons include: 'illness or emergency', 'playdate specialist/parent canceled', or 'other'. If you are canceling the booking because the other member canceled verbally or through text, make sure to select the reason 'playdate specialistr/parent canceled'.</p>
                  <p>If you have to cancel a job you committed to we strongly recommend finding a replacement before cancelling on the families. We understand that things happen, but if we see consistent cancellations your profile and participation will be under review and potentially suspended."</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading37">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse37" aria-expanded="false" aria-controls="collapse37">
                    When do I get paid? (Specialist)
                  </button>
                </h5>
              </div>
              <div id="collapse37" class="collapse" aria-labelledby="heading37" data-parent="#accordion">
                <div class="card-body">
                  <p>Payment will be processed directly into your bank account typically within 2-3 days after the Playdate has passed. If a parent indicates that the hours booked do not match your time worked, you may experience a delay in the transaction.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading38">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse38" aria-expanded="false" aria-controls="collapse38">
                    What pay can I expect? (Specialist)
                  </button>
                </h5>
              </div>
              <div id="collapse38" class="collapse" aria-labelledby="heading38" data-parent="#accordion">
                <div class="card-body">
                  <p>As a Specialist you will receive anywhere between $25-$50 per hour, depending on the number and age of the children attending. You will receive more per hour every time another child signs up. As a support you can make anywhere between $23-$25 per hour. This also depends on the number and age of children attending.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading39">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse39" aria-expanded="false" aria-controls="collapse39">
                    How do I invite my friends to join Playdate as a professional?
                  </button>
                </h5>
              </div>
              <div id="collapse39" class="collapse" aria-labelledby="heading39" data-parent="#accordion">
                <div class="card-body">
                  <p>On every Specialist’s dashboard there is a tab that says "Specialists". Under this tab you can nominate your favorite child care professional.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading40">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse40" aria-expanded="false" aria-controls="collapse40">
                    How do I invite families that I know to join Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse40" class="collapse" aria-labelledby="heading40" data-parent="#accordion">
                <div class="card-body">
                  <p>On every Specialist’s dashboard there is a tab that says "Parents". Under this tab you can input the names and emails of friends you’d like to invite. They will receive an invite from you with a special access code.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading41">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse41" aria-expanded="false" aria-controls="collapse41">
                    Can I Negotiate a Booking? (Specialist)
                  </button>
                </h5>
              </div>
              <div id="collapse41" class="collapse" aria-labelledby="heading41" data-parent="#accordion">
                <div class="card-body">
                  <p>"After accepting a job, Playdate Specialists are expected to adhere to all booking details and terms. The terms of a booking include the payment method,, hourly rate, number of children, time, and location of the booking. Playdate Specialists have the ability to decline booking requests when they are not the right fit. If a provider finds they cannot agree to all terms of the booking, we ask that they politely decline the request.</p>
                  <p>We routinely advise providers not to negotiate booking terms after accepting a job. Repeated reports of these negotiations may result in a probationary period or permanent action on the Playdate specialist's account."</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading42">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse42" aria-expanded="false" aria-controls="collapse42">
                    How do I delete my account (Specialists)
                  </button>
                </h5>
              </div>
              <div id="collapse42" class="collapse" aria-labelledby="heading42" data-parent="#accordion">
                <div class="card-body">
                  <p>Go to your "Payments" tab, where you will be able to click on the link to "Hold or cancel your account", which will provide either hold or cancel instructions depending upon what option you choose. When you cancel your profile, all bookings beyond your current billing period will automatically be cancelled.</p>
                </div>
              </div>
            </div>

             <div class="card providers">
              <div class="card-header" id="heading43">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse43" aria-expanded="false" aria-controls="collapse43">
                    How do I find Playdate jobs?
                  </button>
                </h5>
              </div>
              <div id="collapse43" class="collapse" aria-labelledby="heading43" data-parent="#accordion">
                <div class="card-body">
                  <p>"On Playdate, there are three ways to book a job:</p>
                  <p>Browse the Playdate Job Board - view support roles, lead roles (if applicable), and recurring jobs that parents and other providers have posted. Then express interest in the jobs you'd like to be considered for. Express interest in all of the jobs that work for you to increase your chances of being booked! Please read job posts thoroughly before expressing interest as details, cannot be negotiated. Receive a booking request from parents in search for a Lead in their area and specify a date & time for their job. Make sure that your availability is updated so you will show up in parents' search results. Apply with your interest and, if requested, a proposed Playdate itinerary. As a Lead, you can also post your own playdates to the marketplace and build a following of families. You can post your playdate to the job board if you need to find a support to help you lead it."</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading44">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse44" aria-expanded="false" aria-controls="collapse44">
                    Why am I unable to apply for certain jobs?
                  </button>
                </h5>
              </div>
              <div id="collapse44" class="collapse" aria-labelledby="heading44" data-parent="#accordion">
                <div class="card-body">
                  <p>Certain jobs are only available to Specialist in Lead roles, not Support roles. Click here to learn more about how to become a Lead.</p>
                </div>
              </div>
            </div>

             <div class="card providers">
              <div class="card-header" id="heading45">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse45" aria-expanded="false" aria-controls="collapse45">
                    How do I follow up with parents after a Playdate? (specialist)
                  </button>
                </h5>
              </div>
              <div id="collapse45" class="collapse" aria-labelledby="heading45" data-parent="#accordion">
                <div class="card-body">
                  <p>You can follow up with parents after your playdate by sending a direct thank you email or updating playdate highlights directly to the playdate page.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading46">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse46" aria-expanded="false" aria-controls="collapse46">
                    How do I hire a support staff?
                  </button>
                </h5>
              </div>
              <div id="collapse46" class="collapse" aria-labelledby="heading46" data-parent="#accordion">
                <div class="card-body">
                  <p>Only Lead Specialists can hire support staff. You can do so by posting your Playdate needs on the job board.</p>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

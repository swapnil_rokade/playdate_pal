<?php 
/**
 * PLAYDATE - APPLY SPECIALIST SECTION
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$backUrl = "/apply-specialist.php";

$contactsent = false;

if(isset($_REQUEST["action"]) && ($_REQUEST["action"]=="contact")) {
    //Action - Save contact form
    
    $contact = new Contact();
    $contact->readFromRow($_REQUEST);
    if($contact->getName()!=null) {
        $contact = ContactDAO::createContact($contact);
        $contactsent = ($contact->getId_contact()>0);
    }
    
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Apply Specialist - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>
    
    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Apply to be a Specialist or Partner</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">



    <!-- Featured Specialists -->
    <div class="row intro-row">
        
        <div class="col-lg-6 texto vcenter">
          <h2>Celebrated providers.</h2>
          <p>The driving force behind PAL are the incredibly talented Specialists dedicated to bringing our children the best possible care. We collaborate with brilliant working professionals in the child care and education industry who are passionate about providing safe and meaningful experiences to the kids we serve. We partner with playdate hosts that align with the same mission. If you are an accomplished child care professional or outstanding host, then we invite you to join our movement.</p>
          <p style="margin-top:30px"><a href="#apply-form"title="" class="bt-titulo">Apply now</a></p>
        </div>
        <div class="col-lg-6 imagen">
          <img src="img/img-apply-specialist.jpg" alt="">
        </div>
    </div>

    <!-- How to -->
    <div class="row row-eq-height how-to">
        <div class="col-lg-12 vtop">
          <div class="content-title">
            <h2 class="title-in">The PAL Vetting Process Requires:</h2>
            <div class="row texto become-partner">
              <div class="col-lg-12 descriptions">
                  <p><strong>An Online Application </strong>
                  We are seeking professionals with clear child care experience, proven professionalism, and a passion for childhood development.</p>
                  <p><strong>A Personality Test  </strong>
                  We are seeking individuals that test high in SQ and EQ.</p>
                  <p><strong>A Reference Check</strong>
                  We are seeking peer feedback from individuals who know the candidate well.</p>
                  <p><strong>A Background Check </strong>
                  A no brainer.</p>
                  <p><strong>An Interview </strong>
                  We are seeking an individual who we believe can align with our Leadership Principles.</p>
              </div>
            </div>
          </div>
        </div>
    </div>


      <div class="row featured-playdates requirements">

        <div class="col-lg-12">
          <div class="titulo">
            <h2>To become a PAL lead</h2>
            <p>*Once vetted to be on the PAL platform, these steps are required to become a PAL Lead</p>
          </div>
        </div>

        <div class="row row-eq-height col5">
          
          <div class="col-lg-2 col-md-6 vcenter">
            <a href="#" title="" class="box-playdates">
              <div class="img-container">
                <img src="img/ico/gray/0747-alarm2.svg" alt=""/>
              </div>
              <p>Complete at least 10 hours work in a support role</p>
            </a>
          </div>

          <div class="col-lg-2 col-md-6 vcenter">
            <a href="#" title="" class="box-playdates">
              <div class="img-container">
                <img src="img/ico/gray/0493-first-aid.svg" alt=""/>
              </div>
              <p>Receive First Aid/CPR Certification</p>
            </a>
          </div>


          <div class="col-lg-2 col-md-6 vcenter">
            <a href="#" title="" class="box-playdates last">
              <div class="img-container">
                <img src="img/ico/gray/0220-license.svg" alt=""/>
              </div>
              <p>Evidence of continued learning in professional child care</p>
            </a>
          </div>

          <div class="col-lg-2 col-md-6 vcenter">
            <a href="#" title="" class="box-playdates  last">
              <div class="img-container">
                <img src="img/ico/gray/0650-happy.svg" alt=""/>
              </div>
              <p>Approval from a Pre-Approved PAL Lead</p>
            </a>
          </div>

          <div class="col-lg-2 col-md-6 vcenter">
            <a href="#" title="" class="box-playdates  last">
              <div class="img-container">
                <img src="img/ico/gray/0650-happy.svg" alt=""/>
              </div>
              <p>Approval from a Pre-Approved PAL Lead</p>
            </a>
          </div>

        </div>
        
        <div class="col-lg-12">
          <p class="legal"><strong>*Streamlined vetting available to candidates already employed with a trusted child care or education provider.*</strong></p>
        </div>
        
      </div>
      <a name="apply-form"></a>
      <div class="row row-eq-height how-to contact-form apply">
        <div class="col-lg-12 vtop">
          <div class="content-title">
			
            <h2 class="title-in specialist">I'm a Specialist</h2>
            <h2 class="title-in host inactive">I'm a host</h2>
            <div class="row texto">
              <div class="col-lg-12">
              
                <form id="contact-form-specialist" class="form-specialist" method="post" action="/apply-specialist.php" role="form" novalidate="true">
                	<input type="hidden" name="action" value="contact" />
                	<input type="hidden" name="type" value="specialist" />
                    <div class="controls">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="name">Fist Name</label>
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Name" required="required" data-error="First Name is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="last_name">Last Name</label>
                                    <input id="form_lastname" type="text" name="last_name" class="form-control" placeholder="Lastname" required="required" data-error="Last Name is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="email">Email</label>
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="zipcode">Zip code</label>
                                    <input id="form_zipcode" type="text" name="zipcode" class="form-control inputLocation" placeholder="Zip code or neighborhood" required="required" data-error="Zip code is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="experience">Experience</label>
                                    <textarea style="color:white!important;" id="form_experience" name="experience" class="form-control" placeholder="Experience" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <input type="submit" class="btn btn-send" value="Apply">
                            </div>
                            <div class="col-lg-12">
                              <p class="legal">*Once you have submitted this preliminary form, our team will contact you to continue with the application process.</p>
                            </div>
                        </div>
                        <?php if($contactsent) {?>
                        <div class="row request-access-row">
                        	<div class="col-lg-2">&nbsp;</div>
                            <div class="col-lg-8" id="message" style="text-align:center;color:white;background-color:#815087;padding: 40px 20px;">
	                        	Your application was sent successfully. We will get in touch shortly.
                           	</div>
                            <div class="col-lg-2">&nbsp;</div>
                        </div>
                        <?php } ?>
                        
                    </div>
                </form>

                <form id="contact-form-host" class="form-host" method="post" action="/apply-specialist.php" role="form" novalidate="true">
                	<input type="hidden" name="action" value="contact" />
                	<input type="hidden" name="type" value="host" />
                    <div class="controls">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="form_name">Name</label>
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Name" required="required" data-error="First Name is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="last_name">Last Name</label>
                                    <input id="form_lastname" type="text" name="last_name" class="form-control" placeholder="Last Name" required="required" data-error="Last Name is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="email">Email</label>
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="company_name">Company Name</label>
                                    <input id="form_zipcode" type="text" name="company_name" class="form-control" placeholder="Company Name" required="false" data-error="Company Name is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="company_web">Company Website</label>
                                    <input id="form_zipcode" type="text" name="company_web" class="form-control" placeholder="Company Website" required="false" data-error="Company Website is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="company_address">Company Address</label>
                                    <input id="form_zipcode" type="text" name="company_address" class="form-control" placeholder="Company Address" required="false" data-error="Company Address is required.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="availability">General Playdate Availability</label>
                                    <textarea style="color:white!important;" id="form_availability" name="availability" class="form-control" placeholder="General Playdate Availability" rows="4" required="false" data-error="General Playdate Availability"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <input type="submit" class="btn btn-send" value="Apply">
                            </div>
                            <div class="col-lg-12">
                              <p class="legal">*After submiting this first step we will contact you to continue with the application process.</p>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
          
          </div>

        </div>
    </div>

        <!-- TESTIMONIALS -->
    <div class="row playdates-testimonials">
        <div class="col-lg-12">
          <div class="titulo">
            <div class="testimonio">
              <p class="texto">The kids loved it! We moved to NY in April and finding kids for them to play with has been not easy. This was exactly what they needed. The caregivers were outstanding. We have had trouble finding someone our kids were comfortable staying with. My husband and I were thrilled that both kids ran off to have fun and didn't even look back. We will definitely be back and will certainly recommend and support your program anyway that we can.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">My husband and I are thrilled. Our son had a great time. We had a great time - guilt free! This is just an all around a win-win situation.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">This is awesome! Your service was unparalleled by anything else we've experienced.</p>
              <p class="name">Maryam, Playdate Partner Host</p>
            </div>
            <div class="testimonio">
              <p class="texto">Project Playdate is a wonderful service. My husband and I were so grateful to have a night out and to know that our son was in good hands. Now how do we get Project Playdate to come to California?!</p>
              <p class="name">Shenna Deveza, Mom of One, Union Square</p>
            </div>
             <ul class="nav-testimonios"></ul>
          </div>

        </div>
    </div>
    <!-- END TESTIMONIALS-->

    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

	<?php if($contactsent) { ?>
		<script>
		$(document).ready(function (){
			$("html, body").animate({ scrollTop: $('#message').offset().top-300}, 1000);
		});
		</script>
	<?php } ?>
	
  </body>

</html>

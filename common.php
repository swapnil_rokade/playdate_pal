<?php
/**
 * PLAYDATE - COMMON DEFINITIONS AND CONFIG FILE 
 */
$RELEASE_VERSION = "1.1.5";

$serverName = null;
if(isset($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME']!=null)) {
    $serverName = $_SERVER['SERVER_NAME'];
}
    

if($serverName=="playdate.airtouchmedia.com") { //TEST ENVIROMENT
    $DOCROOT = "/var/www/vhosts/playdate.airtouchmedia.com/httpdocs/";
    
} else if(($serverName=="www.playdatepal.com")||($serverName=="playdatepal.com")) { //PROD ENVIROMENT
    
   // $DOCROOT = "/var/www/vhosts/playdate.airtouchmedia.com/httpdocs/";
   $DOCROOT = $_SERVER['DOCUMENT_ROOT'];
    
} else if($serverName=="playdate.local") { //LOCAL ENVIROMENT
    
    $DOCROOT = "/PROYECTOS/PLAYDATES/workspace/Playdate/www";
    
} else { //LOCAL ENVIROMENT
	//Code updated on 21 January 2019 by SR
    //$DOCROOT = "/PROYECTOS/PLAYDATES/workspace/Playdate/www";
	$DOCROOT = $_SERVER['DOCUMENT_ROOT'];
    
}

//Stripe connect
$STRIPE_AUTHORIZE_URI = 'https://connect.stripe.com/express/oauth/authorize';
$STRIPE_TOKEN_URI = 'https://connect.stripe.com/oauth/token';
$PLAYDATE_STRIPE_URI = 'https://www.playdatepal.com/stripe-connect.php';

/* STRIPE DATA - LIVE ENVIROMENT - BUY FOR PARENTS */
//$STRIPE_SECRET="sk_live_TtpUhAcQjr3wQdICKW5N352X";
//$STRIPE_PK="pk_live_yBW7nuwSbCpMzGVTIJAdwxE3";
//$STRIPE_CLIENTID="ca_DsJrSoJiMJwSHjm4JTjdUgykqMYoF3ar"; //Stripe Connect


/* STRIPE DATA - TEST ENVIROMENT - PLAYDATES */
$STRIPE_SECRET="sk_test_8Eo5OHCswaS3xjgHBNphoSda"; //sr account
$STRIPE_PK="pk_test_uqbubCpAGjCMuxqGXV3CTmW1"; //sr account
$STRIPE_CLIENTID="ca_Ep3mG1muuuwOqqwDkPWzFSrzK3ZnrSwc";  //sr account //Stripe Connect

$STRIPE_CONNECT_SECRET="sk_test_8Eo5OHCswaS3xjgHBNphoSda"; //sr account 
//"sk_test_NFJJMJlmy2x70nlKvmelIfaJ"; playdate
$STRIPE_CONNECT_CLIENTID="ca_Ep3mG1muuuwOqqwDkPWzFSrzK3ZnrSwc"; //sr account 
//"ca_DsJr640bivSXTFjzAT8VpWcJQGSBDkkk"; //Stripe Connect playdate
/*
$STRIPE_CONNECT_SECRET="sk_test_NFJJMJlmy2x70nlKvmelIfaJ"; //playdate
$STRIPE_CONNECT_CLIENTID="ca_DsJr640bivSXTFjzAT8VpWcJQGSBDkkk"; //Stripe Connect playdate
*/

/* STRIPE DATA - LIVE ENVIROMENT  */
//$STRIPE_CONNECT_SECRET="sk_live_TtpUhAcQjr3wQdICKW5N352X";
//$STRIPE_CONNECT_CLIENTID="ca_DsJrSoJiMJwSHjm4JTjdUgykqMYoF3ar"; //Stripe Connect

require $DOCROOT.'/mail/PHPMailer/Exception.php';
require $DOCROOT.'/mail/PHPMailer/PHPMailer.php';
require $DOCROOT.'/mail/PHPMailer/SMTP.php';


//COMMON ZONE
if(session_id()=="") {
	session_start();
}

$isSpecialist = false;
$isLead = false;
$isSupporter = false;
$isNurse = false;
$isParent = false;

$isSpecialist = (isset($_SESSION["spec_id"]) && ($_SESSION["spec_id"] != ""));
$isLead = (isset($_SESSION["spec_profile"]) && ($_SESSION["spec_profile"] == Specialist::$PROFILE_LEAD));
$isSupporter = (isset($_SESSION["spec_profile"]) && ($_SESSION["spec_profile"] == Specialist::$PROFILE_SUPPORTER));
$isNurse = (isset($_SESSION["spec_profile"]) && ($_SESSION["spec_profile"] == Specialist::$PROFILE_NURSE));
$isParent = (isset($_SESSION["parent_id"]) && ($_SESSION["parent_id"] != ""));

$isConnected = $isSpecialist || $isParent;

$backUrl = null;  //address to go back after login 


/* CREATE SLUG FUNCTION */

function create_slug($string, $ext='.html'){
	$replace = '-';
	$string = strtolower($string);

	//replace / and . with white space
	$string = preg_replace("/[\/\.]/", " ", $string);
	$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);

	//remove multiple dashes or whitespaces
	$string = preg_replace("/[\s-]+/", " ", $string);

	//convert whitespaces and underscore to $replace
	$string = preg_replace("/[\s_]/", $replace, $string);

	//limit the slug size
	$string = substr($string, 0, 100);

	//slug is generated
	return ($ext) ? $string.$ext : $string;
}
?>
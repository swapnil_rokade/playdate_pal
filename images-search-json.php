<?php 
/**
 * PLAYDATE - IMAGES SEARCH SERVICE - JSON
 */
include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");


$jsondata = array();

$typelist = null;
$interestlist = null;
if(isset($_REQUEST["typelist"]) && ($_REQUEST["typelist"]!="")) {
    $typelist = $_REQUEST["typelist"];
}

if(isset($_REQUEST["interestlist"]) && ($_REQUEST["interestlist"]!="")) {
    $interestlist = $_REQUEST["interestlist"];
}

$sql = "SELECT picture FROM pd_image_catalog WHERE 1=0"; //any specialist can be supporter

if(($typelist!=null) || ($interestlist!=null)) {
    $sql .= " OR (";
    if($typelist!=null) {
        $sql.="id_typecare IN (".$typelist.")";
    }
    
    if($interestlist!=null) {
        if($typelist!=null) {$sql.="OR ";}
        
        $sql.="id_interest IN (".$interestlist.")";
    }
    $sql .=")";
}

$link = getConnection();
//Obtenemos los resultados
$result = mysql_query($sql, $link);
$i=0;
$total=mysql_num_rows($result);
while($row = mysql_fetch_assoc($result)) {
    $i++;
    $picture = $row['picture'];
    
    $jsondata[] = ["picture" => $picture];
    
}
mysql_close($link);

header('Content-type: application/json');
echo json_encode($jsondata);
?>
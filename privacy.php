<?php 
/**
 * PLAYDATE - PRIVACY SECTION
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$backUrl = "/privacy.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Privacy - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>
    
    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Privacy</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">
      <div class="row terms-of-service">
        <div class="col-lg-12">
<p>Playdate Labs, Inc. ("PAL") has created this Privacy Policy (the "Policy") in order to explain our privacy practices. The following discloses our information-gathering and dissemination practices for our Company's website at www.playdatepal.com (the "Site"), the Company's mobile application (the "Application") and the services offered thereunder (collectively, the "Service"). PAL enables people ("Users") seeking child care services for children ("Child Care Services") to be connected with qualified and experienced child care providers ("PAL Providers"). Both the Users and PAL Providers are referred to herein as "you" from time to time. Each time that you use the Service, you are accepting the practices described in this Policy at that time.</p>
<p><b>INFORMATION RECEIVED BY PAL</b></p>
<p>Personal Information</p>
<p>PAL collects Personal Information only if you voluntarily choose to share such information with PAL. You may browse the Site without disclosing any Personal Information, however, in order for you to take advantage of particular features provided through the Service, as a User or a PALProvider, or to download the Application, we will require that you furnish Personal Information. Any information voluntarily provided by you to PAL or Users described in this section, including without limitation Child Information, is "Personal Information".</p>
<p>Upon employment as a PALProvider and/or when you register your account, download the Application, use the Service, sign up for newsletters, complete forms for optional programs, surveys, contests and other entries, and through communications transmitted and received through the Service to PAL, its employees, including the PAL Providers, and Users, PAL will collect personally identifiable information including but not limited to your name, address, phone number and email address.</p>
<p>Child Information</p>
<p>Upon registration, Users may include information regarding the child(ren) whose care is being managed by PAL (each, a "Child") such as name, age, physical or mental condition, preferences, diet, photo images, etc. In addition, during the provision of Child Care Services (a "Sitting Job"), Users may choose to view images or a live video stream taken by the PAL Provider's mobile device, and/or be provided with geolocation information for the Child during the Job based on information collected from the PAL Provider's mobile device. All such information shall be considered "Child Information".</p>
<p>Financial Information</p>
<p>PAL may also require you to provide us with your financial information, including billing name, address and credit card number ("Financial Information") in order to process payments for Child Care Services.</p>
<p>Public Content</p>
<p>Users will have the opportunity to provide reviews about the PAL Providers they have used and both Users and PAL Providers will have other opportunities to contribute to the Site and Application, such as public forums, bulletin boards and talk threads ("Public Content").</p>
<p>Anonymous or Aggregated Data</p>
<p>PAL collects and stores certain data about the use of the Service on an aggregate and/or anonymous basis. PAL does not link this information to anything personally identifiable to you.</p>
<p>Cookies and Other Technologies</p>
<p>When you visit websites on the Internet, your browser may automatically transmit information to the websites you visit, throughout each visit. Like many websites, we may use "cookies" to collect information. A cookie is a small data file that we transfer to your computer's hard disk for record-keeping purposes. We may utilize persistent cookies that only PAL can read and use to save your login information for future logins to the Site or Application, to hold session information, and to track user preferences. We may utilize session ID cookies to enable certain features of the Service, to better understand how you interact with the Site and Application and to monitor aggregate usage by you and web traffic routing on the Service, to track the number of entries in PAL promotions, sweepstakes and contests and to identify visited areas of the Site and Application. Unlike persistent cookies, session cookies are deleted from your computer when you log off from the Site and Application and then close your browser. Third party advertisers on the Site may also place or read cookies on your browser and we may use Flash cookies (or local shared objects) to store your preferences or display content based upon what you view on our site to personalize your visit. You can instruct your browser, by changing its options, to stop accepting cookies or to prompt you before accepting a cookie from the websites you visit. If you do not accept cookies, however, you will not be able to use all portions or all functionalities of the Service. We and our service providers may also use "pixel tags," "web beacons," "clear GIFs," or similar means in connection with the Service and HTML-formatted email messages to, among other things, track the actions of Users, PAL Providers and email recipients, to determine the success of marketing campaigns and to compile aggregate statistics about Site usage and response rates. We may also engage one or more third party service providers to serve online advertisements on our behalf. They may use a "pixel tag" to collect anonymous information about your visits to the Site and to other websites, and they may use that information to target advertisements for goods and services. This information is collected anonymously, in a manner that does not personally identify you.</p>
<p>IP Address</p>
<p>When you visit and interact with the Site and Application, PAL and/or third parties with whom PAL has contracted to provide services to PAL may collect your Internet Protocol Address ("IP Address"), which is a number that is automatically assigned to the computer that you are using by your Internet Service Provider. This number is identified and logged automatically in our server log files whenever you visit the Site or use the Application, along with the time(s) of your visit(s) and the page(s) that you visited. We use IP Addresses to monitor the regions from which you navigate the Site and Application and for fraud prevention purposes.</p>
<p>Information from Mobile Devices</p>
<p>When you interact with our Site or Application on a mobile device, your mobile carrier may automatically send us your device and hardware ID, device type, carrier, carrier user ID, the content of your request and basic usage statistics about your device and usage of our Service. We do not store this information or use it for any purpose. If you provide it to us or send us a message from your mobile device, we may also collect your phone number. Your device may also store information about your usage of our Service. Please consult your device documentation on how to manage local storage. You may be asked for access to geo-location services through the permission system used by your mobile operating system and we may collect the precise location of your device when the app in running in the foreground or background.</p>
<p>Information from PAL Employees and Independent Contractors</p>
<p>The Company also collects and uses personal data regarding its employees and Independent Contractors, including but not limited to PAL Providers, ("Employee Data"), which may include: name, address, job title and other job information, location, compensation information, identification number, employment history, and a copy of employment agreements.</p>
<p><b>PAL'S USE OF PERSONAL INFORMATION</b></p>
<p>PAL uses your Personal Information for the following general purposes:</p>
<p>1.	to provide the Service,</p>
<p>2.	to connect Users with PAL Providers,</p>
<p>3.	to customize products and services for you,</p>
<p>4.	for billing, identification and authentication, and fraud prevention,</p>
<p>5.	to analyze Site and Application usage, improve the Service and develop new services,</p>
<p>6.	to contact you and deliver notices and communications relevant to your use of the Service, including appointment status reminders and to request reviews,</p>
<p>7.	request further Personal Information from you and/or use third party services, to verify your account or registration information or compliance with PAL's Terms of Service,</p>
<p>8.	to send you promotional emails such as monthly newsletters, new service offerings, special discounts and third-party offers -- if you would rather not receive promotional emails, please see the section below titled "Choice/Opt-Out",</p>
<p>9.	for internal market research, troubleshooting problems, and detecting and protecting against error, fraud or other criminal activity,</p>
<p>10.	for distribution to third-party contractors that provide services to PAL and partners of PAL who are bound by privacy restrictions at least as restrictive as those set forth in this Policy,</p>
<p>11.	to enforce our Terms of Service, and</p>
<p>12.	as otherwise set forth in the Policy.</p>
<p>PAL may use your cell phone number to call or text you in order to provide the Service. If you would like more information about our Policy, or to opt out, please review the Section below called Choice/Opt-out or email <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>. In addition, the PAL Providers will use the information to provide and communicate about the Child Care Services.</p>
<p>We may use Public Content, to display it to other Users and PAL Providers, and use it for promotional purposes. Your Public Content becomes public information once you post it, and copies of it may continue to be viewable even after you delete your account. Your Public Content may appear on other websites, applications or web searches, and therefore this information can be read, collected and used by others. Note that if you include Personal Information about yourself in your Public Content, it can be used and viewed by others. Please remember that you are not permitted to disclose any Personal Information about another User, another PAL Provider or anyone else in your Public Content.</p>
<p>Two factors drive PAL's decisions regarding the use of information collected from Users and PALProviders: 1) improving user experience by offering personalized services, content and advertising, and 2) conducting valuable market research.</p>
<p>PAL may advertise products, services, companies and events that we think might interest you through the email address you provide.</p>
<p>PAL may analyze Personal Information and web navigational data based on your interests and may use the analysis from this information in a way that doesn't reveal a your Personal Information ("Market Research") in order to:</p>
<p>1.	Market the Service</p>
<p>2.	Improve the likelihood of a sale to a group of Users</p>
<p>3.	Increase the likelihood that a group's experience with the Service is relevant to its interests as a whole</p>
<p>PAL may use Personal Information to sell products and services to Users who express an interest in these products and services, through a poll for example, or to Users who can be presumed to have an interest based on results from our Market Research.</p>
<p>Some websites have a "Do Not Track" feature. This feature lets you tell websites you visit that you do not want to have your online activity tracked. PAL does not engage in online tracking of its users (i.e., the collection of Personal Information about a user's online activities over time and across third party websites or online services), and therefore, the Site and Application is not currently set up to respond to "Do Not Track" signals. PAL also does not authorize third parties to collect Personal Information about an individual consumer's online activities over time and across different websites when a consumer uses the Service.</p>
<p><b>CHOICE/OPT-OUT</b></p>
<p>Users may opt-out of receiving communications from us and our partners, remove User's information from our database, and choose to not receive future communications unrelated to the Service by emailing <a href="mailto:support@playdatepal.com">support@playdatepal.com</a> at any time. We will identify such User's email as a "no contact" email and not send User any further communications. Please note that a User cannot opt-out of receiving administrative and transaction-related emails, push notifications or texts from us because these are emails that relate to your User account or your activity as a User on the Service</p>
<p>During registration Users choose whether to receive correspondence from either PAL or its partners and whether such User want to participate in a variety of PAL programs. This information remains on a User's Profile where such User can, at any point, easily edit it to indicate that such User has changed its mind. After logging on, click on "Your Account" within the Service, then select "Notifications." A User may then click on "Email Alerts," and make its selections.</p>
<p>If you receive unsolicited email from PAL domain, please contact us at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>.</p>
<p><b>PAL'S INFORMATION RETENTION POLICY</b></p>
<p>To preserve the integrity of our databases, we may retain information submitted by you for an indefinite length of time. If required by law, we will delete Personal Information by erasing it from our database. We will also respond to User and PAL Provider requests to delete or correct account or Personal Information, which you can do by contacting PAL at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>, or by selecting the "Notifications" option under your account icon within the Service.</p>
<p><b>SECURITY OF COLLECTED INFORMATION</b></p>
<p>Your PAL account is password-protected so that only you and authorized PAL staff have access to your User profile and Personal Information. In order to maintain this protection, do not give your password to anyone. If someone who represents to you they are PAL staff and asks for any Personal Information, including your password, check their URL. If it doesn't say they are <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>, do not provide them with any Personal Information. Also, if you share a computer, you should sign out of your PAL account and close the browser window before someone else logs on. This will help protect your information entered on public terminals from disclosure to third parties.</p>
<p>Your PAL account is password-protected so that only you and authorized PAL staff have access to your User profile and Personal Information. In order to maintain this protection, do not give your password to anyone. If someone who represents to you they are PAL staff and asks for any Personal Information, including your password, check their URL. If it doesn't say they are <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>, do not provide them with any Personal Information. Also, if you share a computer, you should sign out of your PAL account and close the browser window before someone else logs on. This will help protect your information entered on public terminals from disclosure to third parties.</p>
<p><b>WITH WHOM DOES PAL SHARE INFORMATION?</b></p>
<p>When you submit information to PAL, it is collected, processed and maintained solely by the Company, PAL Providers or by third party agents who are bound by privacy restrictions at least as restrictive as those set forth in this Policy. We may allow our business partners to collect information about Users that is not extremely sensitive Information. If a User would like to opt out from the onward transfer of its information to our non-agent partners, such User may send us an email at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>.</p>
<p>We also may share Personal Information with vendors who perform services on behalf of PAL, including without limitation vendors who provide email services, demographic information, or geo-location information, vendors who perform authentication or verification checks, vendors who process credit card payments, and vendors who send SMS messages to Users' and PAL Providers' mobile phone numbers, in each case to the extent applicable.</p>
<p>A User may choose a location feature as part of the Service that will automatically collect geo-location information of the PAL Provider during the provision of Child Care Services for such User. In such event, PAL will share such geo-location information to the User so they know where their Child is located during the provision of Child Care Services.</p>
<p>As described above in the section called "PAL' Use of Personal Information", any Public Content created by you may be accessible to other PAL Providers, Users (including non-registered Users) and/or otherwise available to the public.</p>
<p>PAL reserves the right, at our discretion, to disclose Personal Information from both private and public areas of the Service (a) if required by law, (b) if we are given reason to believe that someone is causing injury to or interference with the rights of Users, a Child or PAL Provider, the general public, or PAL, or (c) to comply with a judicial proceeding, law enforcement investigation, court order or legal process.</p>
<p>Finally, PAL reserves the right to transfer, sell and/or distribute all information collected through the Service to an affiliate, subsidiary, or third party in the event of any reorganization, merger, sale, joint venture, assignment, transfer or other disposition of all or part of our business, assets or stock.</p>
<p><b>CHILDREN'S ONLINE PRIVACY PROTECTION ACT</b></p>
<p>The Service is not intended for individuals under the age of 18. We do not knowingly collect Personal Information from individuals under 18, and no part of the Service is designed to attract anyone under the age of 18. If you become aware that your child has provided us with Personal Information, please contact us. If we become aware that an individual under 18 has opened an account and provided us with Personal Information, we take steps to remove such information and terminate that person's account.</p>
<p><b>VHOW CAN USERS EDIT THEIR INFORMATION?</b></p>
<p>PAL believes strongly in giving you the ability to access and edit your Personal Information. To update your personal info, including any saved credit card information, click My Account within the Service. There you can view, update and correct your account information. You may edit your account information at any time -- all you need is your user name and password.</p>
<p>So that we can protect the integrity of sensitive data, there are certain pieces of information, such as your age, that you cannot alter yourself. For example, since children under 18 are not allowed to register as Users, we need to take reasonable measures to preserve the accuracy of our Users' ages. Please contact us at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a> to find out how to change information you cannot access through your account.</p>
<p>Our databases automatically update any Personal Information you edit in your User profile, or that you request we edit. Information transmitted through boards, chats, polls or through any other means remains in our databases and becomes the property of PAL upon submission.</p>
<p><b>NOTIFICATION OF CHANGES</b></p>
<p>PAL's security and Policy are periodically reviewed and enhanced as necessary. This Policy might change as PAL updates and expands the Service. PAL will endeavor to notify you of these changes by email, but will not be liable for any failure to do so. PAL also encourages you to review this Policy periodically. If you do not understand any of the terms or conditions of any of PAL's policies, you may inquire regarding the same via email at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>.</p>
<p><b>CALIFORNIA PRIVACY RIGHTS</b></p>
<p>Users who are California residents may request and obtain from us once a year, free of charge, certain information about the Personal Information (if any) we disclosed to third parties for direct marketing purposes in the preceding calendar year. If applicable, this information would include a list of the categories of Personal Information that was shared and the names and addresses of all third parties with which we shared information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request in writing to <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>.</p>
<p><b>CONTACTING US?</b></p>
<p>If you have technical problems, questions or other issues related to your use of PAL, please click the link Contact Us on the home at the bottom of any page of this website. You can also contact us at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>.</p>
<p>PAL's staff will respond to all requests sent through mail or email from Users and PAL Providers interested in knowing more about what Personal Information is stored on the PAL database, or if they want their Personal Information nullified, or have additional questions regarding privacy.</p>
<p>If you have any questions about this privacy statement, the practices of this site, or your dealings with this website, you may contact us at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>.</p>
          
        </div> 
      </div>
    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

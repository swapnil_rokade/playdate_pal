<?php
/**
 * PLAYDATE - REQUEST PLAYDATE
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");

$parent = null;

if(isset($_SESSION["parent_id"]) && ($_SESSION["parent_id"]!=null)) {

    $parent = ParentDAO::getParent($_SESSION["parent_id"]);
}

if($parent==null) {
    header("Location: /");
}

$operation = null;
if(isset($_REQUEST["operation"])) {
    $operation = $_REQUEST["operation"];
}

$created = false;
$sent = false;
 
switch ($operation) {
    case "create": {
        $newRequest = new PlaydateRequest();
        $newRequest->readFromRow($_REQUEST);
        
        if(isset($_REQUEST["invite_group"]) && ($_REQUEST["invite_group"]!="")) {
            $inviteGroup = GroupDAO::getGroupByName($_REQUEST["invite_group"]);
            if($inviteGroup!=null) {
                $groupName = $inviteGroup->getName();
                $newRequest->setId_group($inviteGroup->getId_group());
            }
        }
        
        
        $newRequest = PlaydateRequestDAO::createPlaydateRequest($newRequest);
        
        if($newRequest->getId_request()>0) {
            $created=true;
            
            //Insertamos sus intereses
            if(!empty($_POST['interests'])) {
                foreach($_POST['interests'] as $interest) {
                    InterestDAO::createRequestInterest($newRequest->getId_request(), $interest);
                }
            }
            
            
            //Insertamos los ni�os elegidos
            if(!empty($_POST['selectedchildren'])) {
                foreach($_POST['selectedchildren'] as $auxchild) {
                    $childInsert = ChildrenDAO::getChildren($auxchild);
                    if(($childInsert!=null) && ($childInsert->getId_children()>0)) {
                        PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), $auxchild, 1, $childInsert->getAge());
                    }
                }
            }
            if(!empty($_POST['num_kids'])) {
                $i=0;
                foreach($_POST['num_kids'] as $auxnum) {
                    $numKids = $auxnum;
                    $age= $_POST['ages'][$i];
                    
                    if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                        PlaydateRequestDAO::addChildToParentRequest($newRequest->getId_request(), null, $numKids, $age); //Added age and numchildren, without id_children
                        $totalNumKids += $numKids;
                    }
                    $i++;
                }
            }
            
            //Insertamos las localizaciones
            if(!empty($_POST['id_neighborhood'])) {
                foreach($_POST['id_neighborhood'] as $auxlocation) {
                    NeighborhoodDAO::createRequestNeighborhood($newRequest->getId_request(), $auxlocation);
                }
            }
            
            //Insertamos specialists
            if(!empty($_POST['id_specialist'])) {
                foreach($_POST['id_specialist'] as $auxSpec) {
                    PlaydateRequestDAO::addSpecialistToParentRequest($newRequest->getId_request(), $auxSpec);
                }
            }
            
            MailUtils::sendPlaydateParentRequest($parent->getEmail(), $parent->getName());
            
            header("Location: /my-dashboard.php#requests");
        }
        
        break;
    }
    case "send-request": {
        
        //We save it as a type of contact
        $contact = new Contact();
        $contact->readFromRow($_REQUEST);
        $contact->setType(Contact::$TYPE_REQUEST);
        $contact->setReq_id_parent($parent->getId_parent());
        $contact->setName($parent->getName());
        $contact->setLast_name($parent->getLastname());
        $contact->setEmail($parent->getEmail());
        
        if($contact->getName()!=null) {
            $contact = ContactDAO::createContact($contact);
            $sent=true;
        }
        
        MailUtils::sendPlaydateParentRequest($parent->getEmail(), $parent->getName());
        
        break;
    }
}
?>
<!DOCTYPE html>
<html lang="en" <?php if($parent->getProfile()==ParentPd::$PROFILE_STANDARD) {echo("class=\"comingSoon\"");}?>>

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Request Playdate - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page request-playdate full-day<?php echo($isConnected?" connected":"") ?>">
    <!-- LIGHTBOX COMING SOON -->
    <div class="coming-soon <?php if($parent->getProfile()==ParentPd::$PROFILE_STANDARD) {echo("visible");}?>">
      <div class="overlay-coming-soon"></div>
      <div class="content-coming-soon">
        <div class="title">Coming soon</div>
        <?php 
        if($sent) {
        ?>
        <h2 class="dashboard-welcome-heading">Your request has been sent</h2>
        <?php } else { ?>
        <div class="text">If you would like to request a custom playdate, enter these 4 details below and parent concierge will be in touch!</div>
        <form id="form-coming-soon" action="/parent-request-playdate.php" method="post" autocomplete="off">
        	<input type="hidden" name="operation" value="send-request" /> 
          	<input type="hidden" name="id_parent" value="<?php echo $parent->getId_parent() ?>" />
          <div class="row-input">
            <label for="dateCustomRequest" class="sr-only">Date</label>
            <input type="text" id="dateCustomRequest" name="req_date" class="form-control datepicker" placeholder="Date"  >
          </div>
          <div class="row-input">
            <label for="timeCustomRequest" class="sr-only">Time</label>
            <input type="text" id="timeCustomRequest" name="req_time" class="form-control" placeholder="Time"  >
          </div>
          <div class="row-input">
            <label for="kidsCustomRequest" class="sr-only">Estimated number of kids</label>
            <input type="text" id="kidsCustomRequest" name="req_kids" class="form-control" placeholder="Estimated no. kids"  >
          </div>
          <div class="row-input">
            <label for="ageCustomRequest" class="sr-only">Age Range</label>
            <input type="text" id="ageCustomRequest" name="req_age" class="form-control" placeholder="Age Range"  >
          </div>
          <div class="row-input">
            <input type="submit" class="sendCustomRequest" value="Send">
          </div>
        </form>
      <?php } ?>
      </div>
    </div>


    <!-- LIGHTBOX COMING SOON -->
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Request a Playdate</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo (($parent->getPicture()==null)?"/img/img-profile.jpg":$parent->getPicture()) ?>" alt="Picture of <?php echo $parent->getFullName() ?>">
                </div>
                <div class="info">
                  <a href="parent-public-profile.html" class="title"><?php echo $parent->getName() ?></a>
                  <span class="subtitle"><?php echo $parent->getUsername() ?></span>
                  <span class="subtitle members">Chelsea, NYC</span>
                </div>
                <div class="data">
                  <p class="titulo">Groups</p>
                  <?php 
                  $parentGrps = GroupDAO::getGroupsByParent($parent->getId_parent());
                  foreach ($parentGrps as $pGrp) {
                  ?>
                  <p><a href="/groups-dashboard.php?id_group=<?php echo $pGrp->getId_group() ?>" style="text-decoration:none;"><?php echo $pGrp->getName() ?></a></p>
                  <?php } ?>
                </div>
                <div class="data">
                  <p class="titulo">Children</p>
				  <?php 
                  $childsParent = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                  foreach ($childsParent as $pchild) {
                      $genderimg = "img/ico/main/0299-baby2.svg";
                      if($pchild->getGenre()=="girl") {
                          $genderimg = "img/ico/main/0298-baby.svg";
                      }
                  ?>
                  <p class="children"><img src="<?php echo $genderimg ?>" width="50" alt=""/> <span class="name"><?php echo $pchild->getName() ?></span> | <span class="age"><?php echo $pchild->getAge() ?></span></p>
                  <?php 
                  } 
                  ?>                  

                </div>
                <div class="data">
                  <p class="titulo">Availability</p>
                  <p><span class="day">Mon</span> <span class="hour"><?php echo(($parent->getChildren_avb_mon_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_init()):"")?> - <?php echo(($parent->getChildren_avb_mon_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_end()):"")?></span></p>
                  <p><span class="day">Tue</span> <span class="hour"><?php echo(($parent->getChildren_avb_tue_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_init()):"")?> - <?php echo(($parent->getChildren_avb_tue_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_end()):"")?></span></p>
                  <p><span class="day">Wed</span> <span class="hour"><?php echo(($parent->getChildren_avb_wed_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_init()):"")?> - <?php echo(($parent->getChildren_avb_wed_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_end()):"")?></span></p>
                  <p><span class="day">Thu</span> <span class="hour"><?php echo(($parent->getChildren_avb_thu_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_init()):"")?> - <?php echo(($parent->getChildren_avb_thu_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_end()):"")?></span></p>
                  <p><span class="day">Fri</span> <span class="hour"><?php echo(($parent->getChildren_avb_fri_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_init()):"")?> - <?php echo(($parent->getChildren_avb_fri_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_end()):"")?></span></p>
                  <p><span class="day">Sat</span> <span class="hour"><?php echo(($parent->getChildren_avb_sat_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_init()):"")?> - <?php echo(($parent->getChildren_avb_sat_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_end()):"")?></span></p>
                  <p><span class="day">Sun</span> <span class="hour"><?php echo(($parent->getChildren_avb_sun_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_init()):"")?> - <?php echo(($parent->getChildren_avb_sun_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_end()):"")?></span></p>
                </div>
              </div>

			  <?php /*?>
              <div class="data playdate-near">
                <p class="titulo">Playdates near you</p>
                <div class="info-near">
                  <a href="#" title="">Bronx Day Trip</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
                <div class="info-near">
                  <a href="#" title="">Dance Party</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
                <div class="info-near">
                  <a href="#" title="">Met Photo Exhibit</a>
                  <p><span class="day">Jan 14 2018</span> | <span class="hour">9am - 12pm</span></p>
                </div>
              </div>
              <?php */ ?>


        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <form class="form-request-playdate" action="/parent-request-playdate.php" method="post" autocomplete="off" id="frm_request_playdate">
          	<input type="hidden" name="operation" value="create" /> 
          	<input type="hidden" name="id_parent" value="<?php echo $parent->getId_parent() ?>" />
            <div class="row">

              <div class="col-lg-4 type-of-care">
                <p class="titulo">Type of care</p>
                
                  <?php 
                  $types = TypecareDAO::getTypecareList();
                  foreach ($types as $auxType) {
                  ?>
              	  <div class="form-group">
                    <input type="radio" name="type" id="<?php echo $auxType->getId_typecare()?>" value="<?php echo $auxType->getId_typecare()?>" class="form-control" placeholder=""  >
                    <label for="<?php echo $auxType->getId_typecare()?>"><?php echo $auxType->getName() ?> 
                    	<?php if($auxType->getDescription()!=null) { ?>
                    	<a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="<?php echo $auxType->getDescription() ?>"><i class="fa fa-question-circle"></i></a>
                    	<?php } ?>
                    </label>
                  </div> 
              	  <?php 
                  }
              	  ?>

              </div>

              <div class="col-lg-4">
                <p class="titulo">Location</p>
                <label for="location" class="sr-only">Location</label>
                <input type="text" id="location" name="location" class="form-control noMarginTop inputLocation" placeholder="neighborhood">
                <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                <div class="container-tags">
                </div>
              </div>

              <div class="col-lg-4 addons">
              <p class="titulo" style="letter-spacing: -0.9px;">Supervised Travel  <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="This is only a suggested budget for Supervised Travel. It will not be included in the per child price of this playdate. Payment via cash or mobile cash app must be facilitated by Parents and Specialists directly to cover the cost of travel and travel time (if time is not already included in Playdate itinerary)."><i class="fa fa-question-circle"></i></a></p>
              <div class="form-group">
                <label for="travel_pickup" class="sr-only">Pickup address * optional</label>
                <input type="text" id="travel_pickup" name="travel_pickup" class="form-control noMarginTop" placeholder="pickup address * optional"  >
              </div>
               <div class="form-group">
                <label for="travel_dropoff" class="sr-only">Drop off address * optional</label>
                <input type="text" id="travel_dropoff" name="travel_dropoff" class="form-control noMarginTop" placeholder="drop off address * optional"  >
              </div>
            	</div>

            </div>
            <div class="row row-tabs">

              <div class="col-lg-4 date-range">
                <p class="titulo">Date</p>
                  <div class="input-group">
                    <input type="text" placeholder="MM/DD/YYYY" name="date" class="form-control datepicker readonly" readonly="readonly" id="dp1524041141130">
                  </div>

              </div>

              <div class="col-lg-4">
                <p class="titulo">Time</p>
                <div id="time-container-profile">
                  <div class="day-time">
                    <div class="input-group">
                        <div class="selectTime from">
                          <?php 
                          $fieldName = "time_init";
                          $fieldValue = null;
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                    <span> - </span>
                    <div class="input-group">
                    	<div class="selectTime to">
                          <?php 
                          $fieldName = "time_end";
                          $fieldValue = null;
                          $fieldClasses="form-control input-time";
                          include("components/input-time-options.php");
                          ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              

              <div class="col-lg-4">
              <p class="titulo">Activity Budget <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="The Activity Budget is a per child rate charged to each Parent to cover admissions, meals or additional supplies associated with the experience. You can pay your Specialist in cash or using a mobile payment app."><i class="fa fa-question-circle"></i></a></p>
              <div id="budget-container">
                <div class="input-group">
                  <input type="text" placeholder="$5" name="budget_init" class="form-control  input-budget">
                </div>
                <span> - </span>
                <div class="input-group">
                  <input type="text" placeholder="$10" name="budget_end" class="form-control  input-budget">
                </div>
              </div>
              </div>
            </div>

          <div class="row row-tabs">
            <div class="col-lg-4 children">
              <p class="titulo">Children</p>
              
              <?php 
                  $childsParent = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                  foreach ($childsParent as $pchild) {
                      $genderimg = "img/ico/main/0299-baby2.svg";
                      if($pchild->getGenre()=="girl") {
                          $genderimg = "img/ico/main/0298-baby.svg";
                      }
                  ?>
                  
              <div class="form-group">
                <input type="checkbox" id="child1" class="form-control" placeholder="keyword" name="selectedchildren[]" value="<?php echo $pchild->getId_children() ?>">
                <label for="child1"><?php echo $pchild->getName() ?></label>
              </div>
              <?php } ?>
               
              
            </div>
            <div class="col-lg-4 addons">
              <!-- SELECT EXTRA KIDS -->
              <p class="titulo">EXTRA KIDS <span class="optional">*optional</span> <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="If you would like to reserve and pay for additional children, you can indicate the number of kids and their age(s) below. Please note that to confirm additional spots for non-members, parents must sign a Guest Registration Form. "><i class="fa fa-question-circle"></i></a></p>
              
                      <div class="row row-child-detail">
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="num_kids[]">
                                    <option value="">Kids</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="ages[]">
                                    <option value="">Age</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-2">
                            <a href="javascript:;" class="bt-add-kid-detail"><img src="img/ico/main/0860-plus-circle.svg"/></a>
                          </div>
                        </div>
              
              
              
              
              
            </div>

            <div class="col-lg-4">
                  <p class="titulo">Invite Group <span class="optional">*optional</span>  <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="In order to invite your friends to join your playdate, you have to create a group with them first. This will allow you to organize details together in one place. Please know that all members of any group you include here will be able to see your playdate request. "><i class="fa fa-question-circle"></i></a></p>
                  <label for="invite_group" class="sr-only">Invite Group</label>
                  <input type="text" id="inputSelectGroup" name="invite_group" class="form-control noMarginTop" placeholder="Group Name">
                  <?php /*
                  <div class="container-tags">
                    <div class="tag">
                      <span>Dance 1001</span>
                      <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                    </div>
                  </div>
                  */?>
              </div>

          </div>

          <div class="row row-tabs">
            <div class="col-lg-8">                    
              <p class="titulo">Additional info <span class="optional">*optional</span></p>
              <label for="info" class="sr-only">Additional info</label>
              <textarea class="form-control" placeholder="Anything else we should know? Please indicate how many spots, if any, are available to other group members. Any suggested venues or activities? Potty training? Allergies? The more we know, the easier it will be to create the perfect playdate experience for you!" id="info" name="info" rows="22"></textarea>                
            </div>
            <div class="col-lg-4">
              <div class="row">
                <p class="titulo">Categories <span class="optional">*optional</span></p>
                <div class="col">
                  <div class="collapse multi-collapse show" id="categories-container">
                   
                      <div class="list-container stem">
                        <div class="form-group">
                          <input type="checkbox" id="1" value="1" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="1">Academic</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="2" value="2" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="2">STEM</label>
                        </div> 
                      </div>
                      
                      <div class="list-container creative">
                        <div class="form-group">
                          <input type="checkbox" id="3" value="3" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="3">Creative</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="4" value="4" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="4">Performing Arts</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="5" value="5" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="5">Language &amp; Culture</label>
                        </div> 
                      </div>

                      <div class="list-container outdoor">
                        <div class="form-group">
                          <input type="checkbox" id="6" value="6" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="6">Play</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="7" value="7" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="7">Sports &amp; Recreation</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="8" value="8" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="8">Health &amp; Wellness</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="9" value="9" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="9">Outdoor</label>
                        </div>
                      </div>

                      <div class="list-container attractions">
                        <div class="form-group">
                          <input type="checkbox" id="10" value="10" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="10">NYC Museums</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="11" value="11" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="11">NYC Attractions</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="12" value="12" class="form-control" name="interests[]" placeholder="keyword"  >
                          <label for="12">NYC Theater</label>
                        </div> 
                      </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="row row-tabs">
            <div class="col-lg-8">
              <p class="titulo">Specialists <span>*optional</span>  <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="You can send this job request to a specific Specialist you love or simply post it on the job board."><i class="fa fa-question-circle"></i></a></p>
              <div class="row">
                <div class="col-lg-12">
                <label for="inputSpecialistName" class="sr-only">Name</label>
                  <input type="text" id="inputSpecialistName" class="form-control noMarginTop inputSpecialist" placeholder="Name"  autocomplete="new-password" />
                  <a href="javascript:;" class="bt-save-profile last bt-add-specialist" data-toggle="modal" data-target="#playdate-reserved">Add Specialist</a>
                </div>
              </div>
              
              <div class="row">                  
                <div class="col-lg-12">  
                  <div class="container-specialist-tags row-specialist">
                  
                  </div>
                </div>
              </div>

              
            </div>
            <div class="col-lg-4">
              <p class="titulo">Support <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Suggested Age Group Ratios: Under 3 - 2 children to 1 adult. 3-4 years - 4 children to 1 adult. 5-7 years - 5 children to 1 adult. 8+ years - 6 children to 1 adult. If the age group spans two age ranges, then we will default to the ratio for the youngest. If you would prefer to hire another Support Specialist on top of the default ratio, you can do so by requesting additional Support here. This will be at a cost of $23/hour split among all Parents in the group."><i class="fa fa-question-circle"></i></a></p>
              <div class="selectdiv">
                <label for="num_supporters">
                  <select class="form-control" id="num_supporters" name="num_supporters">
                    <option>0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </label>
              </div>
            </div>
          </div>

          <div class="row  row-tabs">
            <div class="col-lg-12">
              <div class="form-group">
                <div class="checkAccept">
                  <input type="checkbox" id="public_job_board" name="public_job_board" class="form-control" placeholder="0" value="1">
                  <label for="public_job_board">Publish your playdate for all Lead Specialists to apply. By selecting this option, you will increase your chances of booking.</label>
                </div>
              </div>
            </div>
          </div>

          <div class="row  row-tabs">
            <div class="col-lg-12">
              <button class="btn btn-md btn-primary bt-save-profile last" type="submit">Request Playdate</button>
            </div>
          </div>


          </form>
        </div>
        <!-- Content -->

      </div>


    </div>

    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

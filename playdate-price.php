<?php 
/**
 * PLAYDATE - SERVICE - CHECK PRICE
 */
include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$addonsPrice = 0;
if(isset($_REQUEST["addonsprice"]) && ($_REQUEST["addonsprice"]!=null)) {
    $addonsPrice = $_REQUEST["addonsprice"];
}

$num_kids = 0;
if(isset($_REQUEST["num_kids"]) && ($_REQUEST["num_kids"]!=null)) {
    $num_kids = $_REQUEST["num_kids"];
}


$playdate = new Playdate();
$playdate->readFromRow($_REQUEST);
$playdate = PlaydateDAO::getPlaydate($playdate->getId_playdate());

echo "$".Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePriceForNewReservations($playdate->getId_playdate(), $num_kids)+$addonsPrice), 0);
?>
<?php 
/**
 * PLAYDATE - SPECIALIST PROFILE 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$specialist=new Specialist();
$specialist->readFromRow($_REQUEST);
if($specialist->getId_specialist()>0) {
    $specialist= SpecialistDAO::getSpecialist($specialist->getId_specialist());
} 

if(($specialist==null) || ($specialist->getId_specialist()<=0)) {
    header("Location: /");
}

$profileName = "Specialist";
switch ($specialist->getProfile()) {
    case Specialist::$PROFILE_LEAD: {
        $profileName = "Lead";
        break; 
    }
    
    case Specialist::$PROFILE_NURSE: { 
        $profileName = "Nurse";
        break;
    }
    
    case Specialist::$PROFILE_SUPPORTER: {
        $profileName = "Supporter";
        break;
    } 
}

$backUrl = "/specialist-profile.php?id_specialist=".$specialist->getId_specialist()."&slug=".$specialist->getSlug();
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $specialist->getFullName() ?> - Specialist - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page submit-itinerary public-profile<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>
    
    <?php include("modal/modal-background-check.php") ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1><?php echo $specialist->getFullName() ?></h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <?php 
        $ratingvalue = RatingDAO::getSpecialistValue($specialist->getId_specialist());
        $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialist->getId_specialist());
        $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialist->getId_specialist());
        ?>
        <div class="col-lg-3 sidebar  my-dashboard">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $specialist->getPicture() ?>" alt="">
                </div>
                <div class="info">
                  <span class="title"><?php echo $specialist->getFullName() ?></span>
                  <span class="subtitle"><?php echo $profileName ?></span>
                </div>
                <div class="puntuacion">
                        <ul>
                          <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                          <li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                        </ul>
                        <span class="num"><?php echo $ratingvalue ?>/5</span>
                      </div>       
                <div class="separator"></div>
                <div class="data certifications">
                  <div class="row">
                    <div class="col-lg-6">
                      <?php if($specialist->getUs_citizen()==0) { ?>
                      <div class="info check" data-toggle="modal" data-target="#modal-legal"><span>background check</span></div>
                      <?php } else { ?>
                      <div class="info check"><span>background check</span></div>
                      <?php } ?>
                    </div>
                     <div class="col-lg-6">
                      <div class="info certificate"><span>approved by PAL</span></div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="info family"><span><?php echo $numFamilies ?> repeat families</span></div>
                    </div>
                     <div class="col-lg-6">
                      <div class="info playdate"><span><?php echo $numPlaydates?><br/>playdates</span></div>
                    </div>
                  </div>
                </div>
              </div>



        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">

          <div class="row">
            <div class="col-lg-4">
              <p class="titulo">Profile</p>
              <div class="data">
                <p><strong class="big"><?php echo strtoupper($specialist->getFullName()) ?></strong></p>
                <p><?php echo $profileName ?></p>
                <?php 
				$neigh = NeighborhoodDAO::getNeighborhoodsListBySpecialist($specialist->getId_specialist());
				foreach($neigh as $auxNeigh) {
				    $city = NeighborhoodDAO::getCity($auxNeigh->getId_city());
				?>
				<p><?php echo $auxNeigh->getName() ?>, <?php echo $city->getName() ?></p>
				<?php 
				}
				?>
				
              </div>       
            </div>
            <div class="col-lg-4  specialist-certification">   
              <p class="titulo">Languages</p>
              <div class="data">
              	<?php if($specialist->getLanguage_1()!="") { ?><p><?php echo $specialist->getLanguage_1() ?></p><?php } ?>
                <?php if($specialist->getLanguage_2()!="") { ?><p><?php echo $specialist->getLanguage_2() ?></p><?php } ?>
                <?php if($specialist->getLanguage_3()!="") { ?><p><?php echo $specialist->getLanguage_3() ?></p><?php } ?>
              </div>                 
                               
            </div>
            <?php if($isParent) { ?>
            <div class="col-lg-4">
              <button onclick="location='/parent-request-playdate.php'" class="btn btn-md btn-primary bt-save-profile" type="button">Request Playdate</button>
              <a href="mailto:parentconcierge@playdatepal.com" class="bt-save-profile bt-concierge">Get in touch with Concierge</a>
            </div>
            <?php } else { ?>
			<div class="col-lg-4">
              <p class="titulo">Experience</p>
              <div class="data">
                <p><strong>Under 1 year old</strong></p>
                <p><?php echo $specialist->getExp_range1() ?> years of experience</p>
              </div> 
              <div class="data">
                <p><strong>2-3 year old</strong></p>
                <p><?php echo $specialist->getExp_range2() ?> years of experience</p>
              </div> 
              <div class="data">
                <p><strong>4+ year old</strong></p>
                <p><?php echo $specialist->getExp_range3() ?> years of experience</p>
              </div> 
            </div>            
            <?php } ?>
          </div>

          <div class="row row-tabs">

            <div class="col-lg-8">
              <p class="titulo">About Me</p>
              <div class="data">
                <p><?php echo $specialist->getAbout_me() ?></p>
              </div>       
            </div>
            <?php if($isParent) { ?>
            <div class="col-lg-4">
              <p class="titulo">Experience</p>
              <div class="data">
                <p><strong>Under 1 year old</strong></p>
                <p><?php echo $specialist->getExp_range1() ?> years of experience</p>
              </div> 
              <div class="data">
                <p><strong>2-3 year old</strong></p>
                <p><?php echo $specialist->getExp_range2() ?> years of experience</p>
              </div> 
              <div class="data">
                <p><strong>4+ year old</strong></p>
                <p><?php echo $specialist->getExp_range3() ?> years of experience</p>
              </div> 
            </div>
            <?php } ?>
          </div>



          <div class="row row-tabs">

            <div class="col-lg-12">
              <p class="titulo uppercase">Certifications</p>
              <?php if($specialist->getCert_nursing()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0219-graduation-hat.svg" width="50" alt=""/><span>Undergraduate Degree in Nursing</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_redcross()>0) { ?>
              <div class="box-certification"  style="color:#878787;">
                <img src="img/ico/main/0489-heart-pulse.svg" width="50" alt=""/><span>Red Cross Special Education</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_language()>0) { ?>
              <div class="box-certification"  style="color:#878787;">
                <img src="img/ico/main/0275-book.svg" width="50" alt=""/><span>Teaching Second Language</span>
              </div> 
              <?php } ?>
              <?php if($specialist->getCert_teaching()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0219-graduation-hat.svg" width="50" alt=""/><span>Teaching Professional</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_child_dev()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0634-accessibility.svg" width="50" alt=""/><span>Child Development Training</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_cpcr()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0489-heart-pulse.svg" width="50" alt=""/><span>CPR Certified</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_partner()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0142-star.svg" width="50" alt=""/><span>Approved Partner</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_firstaid()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0493-first-aid.svg" width="50" alt=""/><span>First Aid Certified</span>
              </div>
              <?php } ?>
              
            </div>
           
            
          </div>

          <div class="row row-tabs">

            <div class="col-lg-8">
              <p class="titulo">Additional</p>
              <?php if($specialist->getAdditional_1()!="") { ?>
              <p><?php echo $specialist->getAdditional_1() ?></p>
              <?php } ?>
              <?php if($specialist->getAdditional_2()!="") { ?>
              <p><?php echo $specialist->getAdditional_2() ?></p>
              <?php } ?>
              <?php if($specialist->getAdditional_3()!="") { ?>
              <p><?php echo $specialist->getAdditional_3() ?></p>
              <?php } ?>
              <?php if($specialist->getAdditional_4()!="") { ?>
              <p><?php echo $specialist->getAdditional_4() ?></p>
              <?php } ?>
            </div>

            <div class="col-lg-4">
              <p class="titulo">Groups</p>             
              <div class="data">
              <?php 
              $groups = GroupDAO::getGroupsBySpecialist($specialist->getId_specialist());
              foreach($groups as $auxGroup) {
              ?>
                <p><a href="/group-public-profile.php?id_group=<?php echo $auxGroup->getId_group() ?>" title=""  style="color:#878787;"><?php echo $auxGroup->getName() ?></a></p>
              <?php } ?>
              </div> 
            </div>
            
          </div>

		  <?php $upPlaydates = PlaydateDAO::getUpcomingPlaydatesListBySpecialist($specialist->getId_specialist(), 1, 3, "date", "asc"); 
		  if(count($upPlaydates)>0) {
		  ?>
          <div class="row row-tabs">
			
            <div class="col-lg-12">
              <p class="titulo centered">Upcoming Playdates Led by <?php echo $specialist->getName() ?></p>
              <div class="row">
                
                <?php foreach ($upPlaydates as $auxPlaydate) { 
                    //Code updated by SR 31 Jan 2019
				$addonsPrice = 0;
				if($auxPlaydate->getAdd1_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd1_price()*100;
				}
				if($auxPlaydate->getAdd2_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd2_price()*100;
				}
				if($auxPlaydate->getAdd3_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd3_price()*100;
				}
				if($auxPlaydate->getAdd4_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd4_price()*100;
				}
				// Code End
				
                    $link = "/playdate.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
                    
                    $interestName = "";
                    $interestType = "";
                    
                    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
                    $auxInterest = null;
                    if(count($interests)>0) {
                        $auxInterest = $interests[0];
                        $interestName =$auxInterest->getName();
                        
                        switch ($auxInterest->getType()) {
                            case Interest::$TYPE_TRAINING: {
                                $interestType=" stem";
                                break;
                            }
                            case Interest::$TYPE_ART: {
                                $interestType=" creative";
                                break;
                            }
                            case Interest::$TYPE_PLAY: {
                                $interestType=" outdoor";
                                break;
                            }
                            case Interest::$TYPE_CULTURAL: {
                                $interestType=" attractions";
                                break;
                            }
                        }
                    }
                ?>
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?>">
                    <div class="img-container">
                      <img src="<?php echo $auxPlaydate->getPicture() ?>" alt="">
                      <?php if($auxInterest!=null) { ?>
                  		<span class="cat-box"><?php echo $interestName ?></span>
                  	   <?php } ?>
                    </div>
                    <?php if($isConnected) { ?>
                    <div class="time">
                      	<span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                		<span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?>-<?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
                    </div>
                    <?php } ?>
                    <div class="info">
                      <span class="title"><?php echo $auxPlaydate->getName() ?></span>
                       <?php if($isConnected) { ?>
                      <span class="subtitle"><?php echo $auxPlaydate->getLoc_city() ?>, <?php echo $auxPlaydate->getLoc_state() ?></span>
                      <?php } ?>
                    </div>
                    <div class="data" style="margin:0px!important">
                <div><span class="price">$<?php echo Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePrice($auxPlaydate->getId_playdate())+$addonsPrice), 0); ?></span> or less | <span class="spots<?php echo($auxPlaydate->getOpen_spots()==1?" destacado":"") ?>"><?php echo $auxPlaydate->getOpen_spots() ?> open spot<?php if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>
              </div>

               <div class="data">
                <div  class="age">Ages: <span><?php echo $auxPlaydate->getAge_init() ?> - <?php echo $auxPlaydate->getAge_end() ?></span></div>
                <?php if($specialist!=null) { ?>
                <div class="specialists">Led by <span><a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                <?php } ?>
              </div>
                    <!-- <div class="data">-->
                    <!--  <div class="age">Ages: <span><?php //echo $auxPlaydate->getAge_init() ?> - <?php //echo $auxPlaydate->getAge_end() ?></span></div>-->
                    <!--  <div class="participants">Participants: <span><?php //echo $auxPlaydate->getNum_children() ?> / <?php //echo $auxPlaydate->getOpen_spots() ?> open spot<?php //if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>-->
                    <!--  <div class="specialists">Specialist: <span><?php //echo $specialist->getFullName() ?></span></div>-->
                    <!--</div>-->
                    <div class="reserve">
                	<?php //if($isParent || $isSpecialist) { ?>
            			<a href="<?php echo $link ?>" class="bt-reserve">Reserve playdate</a>
            		<?php //} else { ?>
            			<!--<a data-toggle="modal" data-target="#login" href="#" class="bt-reserve">Reserve playdate</a>-->
            		<?php //} ?>
                	</div>
                  </div>
                </div>
				<?php } ?>
                

              </div>      
            </div>
          </div>
		<?php } ?>
		
		<?php $pastPlaydates = PlaydateDAO::getPastPlaydatesListBySpecialist($specialist->getId_specialist(), 1, 3, "date", "asc"); 
		  if(count($pastPlaydates)>0) {
		  ?>
          <div class="row row-tabs">
			
            <div class="col-lg-12">
              <p class="titulo centered">Past Playdates Led by <?php echo $specialist->getName() ?></p>
              <div class="row">
                
                <?php foreach ($pastPlaydates as $auxPlaydate) { 
                    //Code updated by SR 31 Jan 2019
				$addonsPrice = 0;
				if($auxPlaydate->getAdd1_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd1_price()*100;
				}
				if($auxPlaydate->getAdd2_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd2_price()*100;
				}
				if($auxPlaydate->getAdd3_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd3_price()*100;
				}
				if($auxPlaydate->getAdd4_price()!=null) {
					$addonsPrice += $auxPlaydate->getAdd4_price()*100;
				}
				// Code End
                    $link = "/playdate.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
                    
                    $interestName = "";
                    $interestType = "";
                    
                    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
                    $auxInterest = null;
                    if(count($interests)>0) {
                        $auxInterest = $interests[0];
                        $interestName =$auxInterest->getName();
                        
                        switch ($auxInterest->getType()) {
                            case Interest::$TYPE_TRAINING: {
                                $interestType=" stem";
                                break;
                            }
                            case Interest::$TYPE_ART: {
                                $interestType=" creative";
                                break;
                            }
                            case Interest::$TYPE_PLAY: {
                                $interestType=" outdoor";
                                break;
                            }
                            case Interest::$TYPE_CULTURAL: {
                                $interestType=" attractions";
                                break;
                            }
                        }
                    }
                ?>
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?>">
                    <div class="img-container">
                      <img src="<?php echo $auxPlaydate->getPicture() ?>" alt="">
                      <?php if($auxInterest!=null) { ?>
                  		<span class="cat-box"><?php echo $interestName ?></span>
                  	   <?php } ?>
                    </div>
                    <?php if($isConnected) { ?>
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                	  <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?>-<?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
                    </div>
                    <?php } ?>
                    <div class="info">
                      <span class="title"><?php echo $auxPlaydate->getName() ?></span>
                       <?php if($isConnected) { ?>
                      <span class="subtitle"><?php echo $auxPlaydate->getLoc_city() ?>, <?php echo $auxPlaydate->getLoc_state() ?></span>
                      <?php } ?>
                    </div>
                    <div class="data" style="margin:0px!important">
                <div><span class="price">$<?php echo Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePrice($auxPlaydate->getId_playdate())+$addonsPrice), 0); ?></span> or less | <span class="spots<?php echo($auxPlaydate->getOpen_spots()==1?" destacado":"") ?>"><?php echo $auxPlaydate->getOpen_spots() ?> open spot<?php if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>
              </div>

               <div class="data">
                <div  class="age">Ages: <span><?php echo $auxPlaydate->getAge_init() ?> - <?php echo $auxPlaydate->getAge_end() ?></span></div>
                <?php if($specialist!=null) { ?>
                <div class="specialists">Led by <span><a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><?php echo $specialist->getFullName() ?></a></span></div>
                <?php } ?>
              </div>
                    <!-- <div class="data">-->
                    <!--  <div class="age">Ages: <span><?php //echo $auxPlaydate->getAge_init() ?> - <?php //echo $auxPlaydate->getAge_end() ?></span></div>-->
                    <!--  <div class="participants">Participants: <span><?php //echo $auxPlaydate->getNum_children() ?> / <?php //echo $auxPlaydate->getOpen_spots() ?> open spot<?php //if($auxPlaydate->getOpen_spots()!=1) { echo ("s");} ?></span></div>-->
                    <!--  <div class="specialists">Specialist: <span><?php //echo $specialist->getFullName() ?></span></div>-->
                    <!--</div>-->
                    <div class="reserve">
                	<?php //if($isParent || $isSpecialist) { ?>
            			<a href="<?php echo $link ?>" class="bt-reserve">View playdate</a>
            		<?php //} else { ?>
            			<!--<a data-toggle="modal" data-target="#login" href="#" class="bt-reserve">View playdate</a>-->
            		<?php //} ?>
                	</div>
                  </div>
                </div>
				<?php } ?>
                

              </div>      
            </div>
          </div>
		<?php } ?>
		
		<?php 
          $ratings = RatingDAO::getRatingListBySpecialist($specialist->getId_specialist());
          if(($ratings!=null) && (count($ratings)>0)) {
        ?>
	    <div class="row row-dashboard updates parents">
          <div class="col-lg-12">
            <p class="titulo">Reviews</p>
          </div>
          
              <?php 
              foreach($ratings as $auxRating) {
                  if(($auxRating->getComment_specialist() != null) && ($auxRating->getComment_specialist() != "")) {
                      if($auxRating->getId_parent()!=null) {
                          $auxParent = ParentDAO::getParent($auxRating->getId_parent());
                          $pic = $auxParent->getPicture();
                          if(($pic==null) || ($pic=="")) {
                              $pic = "img/img-profile.jpg";
                          }
                          $name = $auxParent->getUsername();
                      } else {
                          $pic = "/img/logo-color-playdate.png";
                          //$name = "PAL";
                          $name = "";
                      }
                      
              ?>
          <div class="row info-updates">
            <div class="col-lg-2 date">
              <img src="<?php echo $pic ?>">
              <span><strong><?php echo $name ?></strong></span>
            </div>
            <div class="col-lg-10 texto">
              <p class="day"><strong><?php echo Utils::dateFormat($auxRating->getIdate())?></strong></p>
              <p><?php echo $auxRating->getComment_specialist() ?></p>
            </div>
          </div>
              
              <?php 
                  } 
              }
          }
          ?>
          
          
        </div>

        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

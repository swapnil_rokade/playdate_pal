<?php
/**
 * OPTIONS FOR LEAD SELECT
 * PARAMS:  $selectedLead = null
 */

if(!isset($selectedLead)) {
    $selectedLead = -1;
}

$leads = SpecialistDAO::getSpecialistsLeadList();
?>
<option value=""></option>
<?php 
foreach ($leads as $auxLead) {
    ?>
<option value="<?php echo $auxLead->getId_specialist() ?>" <?php echo(($selectedLead==$auxLead->getId_specialist())?"selected=\"selected\"":"")?>>[#<?php echo $auxLead->getId_specialist() ?>] <?php echo $auxLead->getFullName() ?> - <?php echo $auxLead->getEmail() ?></option>
<?php } ?>
<?php
/**
 * PLAYDATE ADMIN - PLAYDATE FUll EDIT FORM 
 */
if (! isset($editPlaydate)) {
    header("Location: /admin/playdate.php");
}

$leadName = "";
if($editPlaydate->getId_specialist()) {
    $leadName = SpecialistDAO::getSpecialist($editPlaydate->getId_specialist())->getFullName();
}

$numReservations = PlaydateDAO::countPlaydateReservations($editPlaydate->getId_playdate());

$addonsPrice = 0;
if($editPlaydate->getAdd1_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd1_price()*100;
}
if($editPlaydate->getAdd2_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd2_price()*100;
}
if($editPlaydate->getAdd3_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd3_price()*100;
}
if($editPlaydate->getAdd4_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd4_price()*100;
}

$numHours = $editPlaydate->getTime_end()-$editPlaydate->getTime_init();

$pricePerChild = PlaydatePriceDAO::getPlaydatePrice($editPlaydate->getId_playdate());

$payments = PaymentDAO::getPaymentListByPlaydate($editPlaydate->getId_playdate());

$incomes = 0; 
$expenses = 0;

if($payments!=null) {
    foreach($payments as $auxPay) {
        if($auxPay->getId_parent()!=null) {
            if(($auxPay->getStatus()==Payment::$STATUS_COMPLETED) || ($auxPay->getStatus()==Payment::$STATUS_PARTIAL)) {
                $incomes += $auxPay->getAmount_playdate()+$auxPay->getAmount_addons();
            }
        }
        
        if($auxPay->getId_specialist()!=null) {
            $expenses += $auxPay->getAmount_playdate()+$auxPay->getAmount_addons();
        }
    }
}

$playdateStatus = "PUBLISHED";
switch ($editPlaydate->getStatus()) {
    case Playdate::$STATUS_DECLINED: {
        $playdateStatus = "DECLINED";
        break;
    }
    
    case Playdate::$STATUS_FINISHED: {
        $playdateStatus = "FINISHED";
        break;
    }
    
    case Playdate::$STATUS_PUBLISHED: {
        $playdateStatus = "PUBLISHED";
        break;
    }
    
    case Playdate::$STATUS_REQUEST: {
        $playdateStatus = "REQUEST";
        break;
    }
    
    case Playdate::$STATUS_RSVP: {
        $playdateStatus = "IN RSVP";
        break;
    }
}

?>
<style>
.income {color:green;}
.expense {color:red;}
.completed {color:green;}
.partial {color:red;}
.pending {color:orange;}
</style>
<!-- START CONTENT -->
<section id="main-content" class=" ">
<form action="/admin/playdates.php" method="post">
	<input type="hidden" name="action" value="update-full" />
	<input type="hidden" name="id_playdate" value="<?php echo $editPlaydate->getId_playdate() ?>" />
								
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Edit Playdate</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/playdates.php">Playdates</a></li>
						<li class="active"><a href="/admin/playdates.php?action=edit&id_playdate=<?php echo $editPlaydate->getId_playdate() ?>"><strong>Edit Playdate</strong></a></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Full Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>
							
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<label class="form-label" for="field-1">Featured</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="featured" style="height:34px; width:100%">
											<option value="<?php echo Playdate::$FEATURED_NO?>"<?php echo (($editPlaydate->getFeatured()==Playdate::$FEATURED_NO)?" selected=\"selected\"":"") ?>>No</option>
											<option value="<?php echo Playdate::$FEATURED_YES?>"<?php echo (($editPlaydate->getFeatured()==Playdate::$FEATURED_YES)?" selected=\"selected\"":"") ?>>Yes</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<label class="form-label" for="type">Type of Care</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="type" style="height:34px; width:100%">
											<?php 
											$types = TypecareDAO::getTypecareList();
											foreach ($types as $auxType) {
									        ?>
                                       	    <option value="<?php echo $auxType->getId_typecare()?>" <?php if($auxType->getId_typecare()==$editPlaydate->getType()) {echo("selected=\"selected\"");}?>><?php echo $auxType->getName() ?></option>
                                            <?php 
                                            }
											?>
										</select>
									</div>
								</div>
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="form-label" for="field-1">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getName()?>" class="form-control" id="field-1" name="name"  placeholder="Name" />
									</div>
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">	
								<div class="form-group">
									<label class="form-label" for="field-1">Date (MM/DD/YYYY)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getDate()?>" class="form-control datepicker" id="field-1" name="date"  placeholder="DD/MM/YYYY" />
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">	
								<div class="form-group">
									<label class="form-label" for="field-1">Time Init</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="time_init" style="height:34px;width:100%">
											<?php 
											$fieldValue = $editPlaydate->getTime_init();
											include("input-time-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<label class="form-label" for="field-1">Time End</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="time_end" style="height:34px;width:100%">
											<?php 
											$fieldValue = $editPlaydate->getTime_end();
											include("input-time-options.php");
											?>
										</select>
									</div>
								</div>	
							</div>
							
							
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">	
								<div class="form-group">
									<label class="form-label" for="field-1">Num. Children</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getNum_children()?>" class="form-control" id="field-1" name="num_children"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="age_init">Age Min</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAge_init()?>" class="form-control" id="age_init" name="age_init"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="age_end">Age Max</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAge_end()?>" class="form-control" id="age_end" name="age_end"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">	
								<div class="form-group">
									<label class="form-label" for="min_kids">Min. Kids</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getMin_kids()?>" class="form-control" id="field-1" name="min_kids"  placeholder="" />
									</div>
								</div>
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
								<div class="form-group">
									<label class="form-label" for="loc_address">Location Address</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getLoc_address()?>" class="form-control" id="loc_address" name="loc_address"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<label class="form-label" for="loc_city">Location City</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getLoc_city()?>" class="form-control" id="loc_city" name="loc_city"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<label class="form-label" for="loc_state">Location State</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getLoc_state()?>" class="form-control" id="loc_state" name="loc_state"  placeholder="" />
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
								<div class="form-group">
									<label class="form-label" for="field-1">Description</label> <span
										class="desc"></span>
									<div class="controls">
										<textarea name="description" placeholder="Playdate Description" rows="6" cols="80" style="width:100%"><?php echo $editPlaydate->getDescription()?></textarea>
									</div>
								</div>
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="form-label" for="picture">Banner</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="picture" style="height:34px; width:100%" onchange="jQuery('#banner').attr('src', this.value)">
											<?php 
											$sqlImg = "SELECT picture FROM pd_image_catalog";
											$linkImg = getConnection();
											//Obtenemos los resultados
											$resultImg = mysql_query($sqlImg, $linkImg);
											while($rowImg = mysql_fetch_assoc($resultImg)) {
											    $pictureImg = $rowImg['picture'];
											?>    
											<option value="<?php echo $pictureImg?>" <?php if($pictureImg==$editPlaydate->getPicture()) {echo("selected=\"selected\"");} ?>><?php echo $pictureImg?></option>
											<?php     
											}
											mysql_close($link);
											?>											
										</select>
										
									</div>
									<img id="banner" src="<?php echo $editPlaydate->getPicture() ?>" style="width:100%" />
								</div>
								
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:20px;border-top:1px dotted gray"></div>
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">	
								<div class="form-group">
									<label class="form-label" for="act1_id">Activ. 1</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act1_id" style="height:34px;width:100%">
											<?php 
											$selectedActivity = $editPlaydate->getAct1_id();
											include("input-activity-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="act1_time">Activ. 1 Time</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act1_time" style="height:34px;width:100%">
											<?php 
											$fieldValue = $editPlaydate->getAct1_time();
											include("input-time-options-advanced.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								<div class="form-group">
									<label class="form-label" for="act1_note">Activ. 1 Note</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAct1_note()?>" class="form-control" id="act1_note" name="act1_note"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">	
								<div class="form-group">
									<label class="form-label" for="act2_id">Activ. 2</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act2_id" style="height:34px;width:100%">
											<?php 
											$selectedActivity = $editPlaydate->getAct2_id();
											include("input-activity-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="act2_time">Activ. 2 Time</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act2_time" style="height:34px;width:100%">
											<?php 
											$fieldValue = $editPlaydate->getAct2_time();
											include("input-time-options-advanced.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								<div class="form-group">
									<label class="form-label" for="act2_note">Activ. 2 Note</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAct2_note()?>" class="form-control" id="act2_note" name="act2_note"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">	
								<div class="form-group">
									<label class="form-label" for="act3_id">Activ. 3</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act3_id" style="height:34px;width:100%">
											<?php 
											$selectedActivity = $editPlaydate->getAct3_id();
											include("input-activity-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="act3_time">Activ. 3 Time</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act3_time" style="height:34px;width:100%">
											<?php 
											$fieldValue = $editPlaydate->getAct3_time();
											include("input-time-options-advanced.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								<div class="form-group">
									<label class="form-label" for="act3_note">Activ. 3 Note</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAct3_note()?>" class="form-control" id="act3_note" name="act3_note"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">	
								<div class="form-group">
									<label class="form-label" for="act4_id">Activ. 4</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act4_id" style="height:34px;width:100%">
											<?php 
											$selectedActivity = $editPlaydate->getAct4_id();
											include("input-activity-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="act3_time">Activ. 4 Time</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="act4_time" style="height:34px;width:100%">
											<?php 
											$fieldValue = $editPlaydate->getAct4_time();
											include("input-time-options-advanced.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								<div class="form-group">
									<label class="form-label" for="act4_note">Activ. 4 Note</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAct4_note()?>" class="form-control" id="act4_note" name="act4_note"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:20px;border-top:1px dotted gray"></div>
							
							
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">	
								<div class="form-group">
									<label class="form-label" for="add1_id">Addon 1</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="add1_id" style="height:34px;width:100%">
											<?php 
											$selectedAddon = $editPlaydate->getAdd1_id();
											include("input-addon-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add1_price">Addon 1 Price ($)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAdd1_price()?>" class="form-control" id="add1_price" name="add1_price"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add1_id_check">Optional</label> <span
										class="desc"></span>
									<div class="controls">
									<input type="checkbox" class="form-control" name="add1_id_check" id="add1_id_check" <?php echo ($editPlaydate->getAdd1_id_check() == 1 ? "checked=\"checked\"":"")?> style="width: 20px;
    margin-left: 10px;">
									<!-- <input type="text" value="<?php //echo $editPlaydate->getAdd1_price()?>" class="form-control" id="add1_price" name="add1_price"  placeholder="" /> -->
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">	
								<div class="form-group">
									<label class="form-label" for="add2_id">Addon 2</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="add2_id" style="height:34px;width:100%">
											<?php 
											$selectedAddon = $editPlaydate->getAdd2_id();
											include("input-addon-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add2_price">Addon 2 Price ($)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAdd2_price()?>" class="form-control" id="add2_price" name="add2_price"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add1_id_check">Optional</label> <span
										class="desc"></span>
									<div class="controls">
									<input type="checkbox" class="form-control" name="add2_id_check" id="add2_id_check" <?php echo ($editPlaydate->getAdd2_id_check() == 1 ? "checked=\"checked\"":"")?> style="width: 20px;
    margin-left: 10px;">
									<!-- <input type="text" value="<?php //echo $editPlaydate->getAdd1_price()?>" class="form-control" id="add1_price" name="add1_price"  placeholder="" /> -->
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">	
								<div class="form-group">
									<label class="form-label" for="add3_id">Addon 3</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="add3_id" style="height:34px;width:100%">
											<?php 
											$selectedAddon = $editPlaydate->getAdd3_id();
											include("input-addon-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add3_price">Addon 3 Price ($)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAdd3_price()?>" class="form-control" id="add3_price" name="add3_price"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add1_id_check">Optional</label> <span
										class="desc"></span>
									<div class="controls">
									<input type="checkbox" class="form-control" name="add3_id_check" id="add3_id_check" <?php echo ($editPlaydate->getAdd3_id_check() == 1 ? "checked=\"checked\"":"")?> style="width: 20px;
    margin-left: 10px;">
									<!-- <input type="text" value="<?php //echo $editPlaydate->getAdd1_price()?>" class="form-control" id="add1_price" name="add1_price"  placeholder="" /> -->
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">	
								<div class="form-group">
									<label class="form-label" for="add4_id">Addon 4</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="add4_id" style="height:34px;width:100%">
											<?php 
											$selectedAddon = $editPlaydate->getAdd4_id();
											include("input-addon-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add4_price">Addon 4 Price ($)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getAdd4_price()?>" class="form-control" id="add4_price" name="add4_price"  placeholder="" />
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<label class="form-label" for="add1_id_check">Optional</label> <span
										class="desc"></span>
									<div class="controls">
									<input type="checkbox" class="form-control" name="add4_id_check" id="add4_id_check" <?php echo ($editPlaydate->getAdd4_id_check() == 1 ? "checked=\"checked\"":"")?> style="width: 20px;
    margin-left: 10px;">
									<!-- <input type="text" value="<?php //echo $editPlaydate->getAdd1_price()?>" class="form-control" id="add1_price" name="add1_price"  placeholder="" /> -->
									</div>
								</div>
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:20px;border-top:1px dotted gray"></div>
							
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">	
								<div class="form-group">
									<label class="form-label" for="id_specialist">Lead</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="id_specialist" style="height:34px;width:100%">
											<?php 
											$selectedLead = $editPlaydate->getId_specialist();
											include("input-lead-options.php");
											?>
										</select>
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<label class="form-label" for="field-1">Status</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="status" style="height:34px; width:100%">
											<option value="<?php echo Playdate::$STATUS_PUBLISHED?>"<?php echo (($editPlaydate->getStatus()==Playdate::$STATUS_PUBLISHED)?" selected=\"selected\"":"") ?>>PUBLISHED</option>
											<option value="<?php echo Playdate::$STATUS_RSVP?>"<?php echo (($editPlaydate->getStatus()==Playdate::$STATUS_RSVP)?" selected=\"selected\"":"") ?>>IN RSVP</option>
											<option value="<?php echo Playdate::$STATUS_DECLINED?>"<?php echo (($editPlaydate->getStatus()==Playdate::$STATUS_DECLINED)?" selected=\"selected\"":"") ?>>DECLINED</option>
											<option value="<?php echo Playdate::$STATUS_FINISHED?>"<?php echo (($editPlaydate->getStatus()==Playdate::$STATUS_FINISHED)?" selected=\"selected\"":"") ?>>FINISHED</option>
										</select>
									</div>
								</div>
							</div>
									
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:20px;border-top:1px dotted gray"></div>
							
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">	
								<div class="form-group">
									<label class="form-label" for="privacy_type">Privacity</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="privacy_type" style="height:34px;width:100%">
											<option value="<?php echo Playdate::$PRIVACITY_PUBLIC?>"<?php echo (($editPlaydate->getPrivacy_type()==Playdate::$PRIVACITY_PUBLIC)?" selected=\"selected\"":"") ?>>PUBLIC - OPEN TO ANYONE</option>
											<option value="<?php echo Playdate::$PRIVACITY_PRIVATE?>"<?php echo (($editPlaydate->getPrivacy_type()==Playdate::$PRIVACITY_PRIVATE)?" selected=\"selected\"":"") ?>>PRIVATE - ONLY INVITED PARENTS</option>
											<option value="<?php echo Playdate::$PRIVACITY_GROUP?>"<?php echo (($editPlaydate->getPrivacy_type()==Playdate::$PRIVACITY_GROUP)?" selected=\"selected\"":"") ?>>PRIVATE - ONLY SELECTED GROUP</option>
										</select>
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<label class="form-label" for="privacy_group_id">Selected Group</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="privacy_group_id" style="height:34px; width:100%">
											<?php 
											$selectedGroup = $editPlaydate->getPrivacy_group_id();
											include("input-group-options.php");
											?>
										</select>
									</div>
								</div>
							</div>			
							
						</div>
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
    							<div class="form-group">
    								<div class="text-left">
    									<button type="submit" class="btn btn-primary">Update Playdate Info</button>
    								</div>
								</div>
								
							</div>
						
					</div>


				</div>
				
			</section>
		</div>
		</form>

		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Support</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					
					<form name="frm_add_support" action="/admin/playdates.php">
								<input type="hidden" name="action" value="add-support" />
								<input type="hidden" name="id_playdate" value="<?php echo $editPlaydate->getId_playdate() ?>" />
								
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
    								<div class="form-group">
    									<label class="form-label" for="id_specialist">Add New Support</label> <span
    										class="desc"></span>
    									<div class="controls">
    										<select name="id_specialist" style="height:34px; width:100%">
    											<?php 
    											$selectedLead = -1;
    											include("input-lead-full-options.php");
    											?>
    										</select>
    									</div>
    								</div>
    							</div>
    							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    							<label class="form-label" for="--">&nbsp;</label> <span
    										class="desc"></span>
        							<div class="form-group">
        								<div class="text-left">
        									<button type="submit" class="btn btn-primary" style="margin-top:5px;width:100%">Add Supporter</button>
        								</div>
    								</div>
    							</div>
								
							</form>
					
					<?php 
					$supports = SpecialistDAO::getSupportersByPlaydate($editPlaydate->getId_playdate());
					?>
					
					<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($supports as $auxSuppot) {
									?>
									<tr>
										<td><?php echo $auxSuppot->getId_specialist() ?></td>
										<td><?php echo $auxSuppot->getName() ?></td>
										<td><?php echo $auxSuppot->getLastname() ?></td>
										<td><?php echo $auxSuppot->getEmail() ?></td>
										<td><?php echo $auxSuppot->getUsername() ?></td>
										<td><a onclick="return confirm('Are you sure you want to remove this support? This action cannot be undone.')" href="/admin/playdates.php?action=remove-support&id_playdate=<?php echo $editPlaydate->getId_playdate() ?>&id_specialist=<?php echo $auxSuppot->getId_specialist()?>" class="bt-edit"
											title="Remove"><span class="fa fa-trash"></span></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
									
				</div>
				
				
			</section>
		</div>


	</section>
</section>
<!-- END CONTENT -->

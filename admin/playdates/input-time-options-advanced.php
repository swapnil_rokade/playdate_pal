<?php 
/**
 * OPTIONS FOR TIME SELECT
 * PARAMS:  $fieldValue
 */

$formValues= array("8", "8:30","9", "9:30", "10", "10:30", "11", "11:30", "12", "12:30", "13", "13:30", "14", "14:30", "15", "15:30", "16", "16:30", "17", "17:30", "18", "18:30", "19", "19:30", "20", "20:30", "21", "21:30", "22", "22:30", "23", "23:30", "24");
if((!isset($fieldValue)) || ($fieldValue==null)) {$fieldValue="";}
?>
  <option value=""></option>
  <?php foreach ($formValues as $auxValue) { ?>
  <option value="<?php echo $auxValue ?>" <?php if($auxValue==$fieldValue) { echo("selected=\"selected\"");} ?>> <?php echo Utils::get12hourFormatInput($auxValue) ?></option>
  <?php } ?>  
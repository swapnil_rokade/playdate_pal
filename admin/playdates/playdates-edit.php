<?php
/**
 * PLAYDATE ADMIN - PLAYDATE VIEW FORM 
 */
if (! isset($editPlaydate)) {
    header("Location: /admin/playdate.php");
}

$leadName = "";
if($editPlaydate->getId_specialist()) {
    $leadName = SpecialistDAO::getSpecialist($editPlaydate->getId_specialist())->getFullName();
}

$numReservations = PlaydateDAO::countPlaydateReservations($editPlaydate->getId_playdate());

$addonsPrice = 0;
if($editPlaydate->getAdd1_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd1_price()*100;
}
if($editPlaydate->getAdd2_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd2_price()*100;
}
if($editPlaydate->getAdd3_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd3_price()*100;
}
if($editPlaydate->getAdd4_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd4_price()*100;
}

$numHours = $editPlaydate->getTime_end()-$editPlaydate->getTime_init();

$pricePerChild = PlaydatePriceDAO::getPlaydatePrice($editPlaydate->getId_playdate());

$payments = PaymentDAO::getPaymentListByPlaydate($editPlaydate->getId_playdate());

$incomes = 0; 
$expenses = 0;

if($payments!=null) {
    foreach($payments as $auxPay) {
        if($auxPay->getId_parent()!=null) {
            if(($auxPay->getStatus()==Payment::$STATUS_COMPLETED) || ($auxPay->getStatus()==Payment::$STATUS_PARTIAL)) {
                $incomes += $auxPay->getAmount_playdate()+$auxPay->getAmount_addons();
            }
        }
        
        if($auxPay->getId_specialist()!=null) {
            $expenses += $auxPay->getAmount_playdate()+$auxPay->getAmount_addons();
        }
    }
}

$playdateStatus = "PUBLISHED";
switch ($editPlaydate->getStatus()) {
    case Playdate::$STATUS_DECLINED: {
        $playdateStatus = "DECLINED";
        break;
    }
    
    case Playdate::$STATUS_FINISHED: {
        $playdateStatus = "FINISHED";
        break;
    }
    
    case Playdate::$STATUS_PUBLISHED: {
        $playdateStatus = "PUBLISHED";
        break;
    }
    
    case Playdate::$STATUS_REQUEST: {
        $playdateStatus = "REQUEST";
        break;
    }
    
    case Playdate::$STATUS_RSVP: {
        $playdateStatus = "IN RSVP";
        break;
    }
}

?>
<style>
.income {color:green;}
.expense {color:red;}
.completed {color:green;}
.partial {color:red;}
.pending {color:orange;}
</style>
<!-- START CONTENT -->
<section id="main-content" class=" ">
<form action="/admin/playdates.php" method="post">
	<input type="hidden" name="action" value="update-basic" />
	<input type="hidden" name="id_playdate" value="<?php echo $editPlaydate->getId_playdate() ?>" />
								
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Edit Playdate</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/playdates.php">Playdates</a></li>
						<li class="active"><a href="/admin/playdates.php?action=edit&id_playdate=<?php echo $editPlaydate->getId_playdate() ?>"><strong>Edit Playdate</strong></a></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Basic Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>
							
							
								<div class="form-group">
									<label class="form-label" for="field-1">Featured</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="featured">
											<option value="<?php echo Playdate::$FEATURED_NO?>"<?php echo (($editPlaydate->getFeatured()==Playdate::$FEATURED_NO)?" selected=\"selected\"":"") ?>>No</option>
											<option value="<?php echo Playdate::$FEATURED_YES?>"<?php echo (($editPlaydate->getFeatured()==Playdate::$FEATURED_YES)?" selected=\"selected\"":"") ?>>Yes</option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="field-1">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getName()?>" class="form-control" id="field-1" name="name"  placeholder="Name" />
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="field-1">Date (MM/DD/YYYY)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getDate()?>" class="form-control datepicker" id="field-1" name="date"  placeholder="DD/MM/YYYY" />
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="field-1">Num. Children</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaydate->getNum_children()?>" class="form-control" id="field-1" name="num_children"  placeholder="" />
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="field-1">Description</label> <span
										class="desc"></span>
									<div class="controls">
										<textarea name="description" placeholder="Playdate Description" rows="6" cols="80" style="width:100%"><?php echo $editPlaydate->getDescription()?></textarea>
									</div>
								</div>
								
								
								
								<div class="form-group">
									<strong>Lead</strong>: 
									<p><?php echo $leadName ?></p>
								</div>
								<div class="form-group">
									<strong>Status</strong>: <?php echo $playdateStatus ?> 
								</div>
										
							</div>
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
    							<div class="form-group">
    								<div class="text-left">
    									<button type="submit" class="btn btn-primary">Update basic info</button>
    								</div>
								</div>
								
							</div>
						
					</div>


				</div>
				
			</section>
		</div>
		</form>

	</section>
</section>
<!-- END CONTENT -->
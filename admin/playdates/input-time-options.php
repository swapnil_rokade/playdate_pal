<?php 
/**
 * OPTIONS FOR TIME SELECT
 * PARAMS:  $fieldValue
 */

$formValues= array("8","9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
if((!isset($fieldValue)) || ($fieldValue==null)) {$fieldValue="";}
?>
  <?php foreach ($formValues as $auxValue) { ?>
  <option value="<?php echo $auxValue ?>" <?php if($auxValue==$fieldValue) { echo("selected=\"selected\"");} ?>> <?php echo Utils::get12hourFormatInput($auxValue) ?></option>
  <?php } ?>  
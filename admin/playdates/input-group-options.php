<?php
/**
 * OPTIONS FOR GROUP SELECT
 * PARAMS:  $selectedGroup = null
 */

if(!isset($selectedGroup)) {
    $selectedGroup = -1;
}

$groups = GroupDAO::getGroupsList(1, 100, null, null);
?>
<option value=""></option>
<?php 
foreach ($groups as $auxGroup) {
    ?>
<option value="<?php echo $auxGroup->getId_group() ?>" <?php echo(($selectedGroup==$auxGroup->getId_group())?"selected=\"selected\"":"")?>><?php echo $auxGroup->getName() ?></option>
<?php } ?>
<?php 
/**
 * PLAYDATE ADMIN - PART - PLAYDATES LIST
 * 
 */
if(!isset($playdates)) {$playdates=array();}

?>
<!-- START ADMINISTRATORS LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Parents</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/playdates.php">Playdates</a></li>
						<li class="active"><strong>All playdates</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All playdates</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Date</th>
										<th>Name</th>
										<th>Lead</th>
										<th>Max. Kids</th>
										<th>Reservations</th>
										<th>Hours</th>
										<th>Price per Child</th>
										<th>Price Addons</th>
										<th>Featured</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($playdates as $auxPlaydate) {
									    $leadName = "";
										if($auxPlaydate->getId_specialist()>0) {
											$spec = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
											if($spec!=null) {
												$leadName = $spec->getFullName();
											} else {
											    $leadName = "<span style=\"color:red\">! No lead</span>";
											}
									    }
										
									    
									    $numReservations = PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
									    
									    $addonsPrice = 0;
									    if($auxPlaydate->getAdd1_price()!=null) {
									        $addonsPrice += $auxPlaydate->getAdd1_price()*100;
									    }
									    if($auxPlaydate->getAdd2_price()!=null) {
									        $addonsPrice += $auxPlaydate->getAdd2_price()*100;
									    }
									    if($auxPlaydate->getAdd3_price()!=null) {
									        $addonsPrice += $auxPlaydate->getAdd3_price()*100;
									    }
									    if($auxPlaydate->getAdd4_price()!=null) {
									        $addonsPrice += $auxPlaydate->getAdd4_price()*100;
									    }
									    
									    $numHours = $auxPlaydate->getTime_end() -$auxPlaydate->getTime_init();
									?>
									<tr>
										<td><?php echo $auxPlaydate->getId_playdate() ?></td>
										<td><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></td>
										<td><?php echo $auxPlaydate->getName() ?></td>
										<td><?php echo $leadName ?></td>
										<td><?php echo $auxPlaydate->getNum_children() ?></td>
										<td><?php echo $numReservations ?></td>
										<td><?php echo $numHours ?></td>
										<td>$ <?php echo Utils::moneyFormat(PlaydatePriceDAO::getPlaydatePrice($auxPlaydate->getId_playdate()),2) ?></td>
										<td>$ <?php echo Utils::moneyFormat($addonsPrice, 2) ?></td>
										<td><?php echo (($auxPlaydate->getFeatured()==Playdate::$FEATURED_YES)?"Yes":"No") ?></td>
										<td><a href="/admin/playdates.php?action=view&id_playdate=<?php echo $auxPlaydate->getId_playdate() ?>" class="bt-view"
											title="View"><span class="fa fa-eye"></span></a>
											<a
											title="Delete" href="/admin/playdates.php?action=delete&id_playdate=<?php echo $auxPlaydate->getId_playdate() ?>" onclick="return confirm('Are you sure you want to remove this playdate and all its information? This operation cannot be undone.');" class="bt-delete"><span class="fa fa-trash"></span></a>
											</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END ADMINISTRATORS LIST CONTENT -->
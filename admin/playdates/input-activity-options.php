<?php
/**
 * OPTIONS FOR ACTIVITY SELECT
 * PARAMS:  $selectedValue = null
 */

if(!isset($selectedActivity)) {
    $selectedActivity = -1;
}

$activities = ActivityDAO::getActivityList();
?>
<option value=""></option>
<?php 
foreach ($activities as $auxAct) {
    ?>
<option value="<?php echo $auxAct->getId_activity() ?>" <?php echo(($selectedActivity==$auxAct->getId_activity())?"selected=\"selected\"":"")?>><?php echo $auxAct->getName() ?></option>
<?php } ?>
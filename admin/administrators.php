<?php 
/**
 * PLAYDATE - ADMIN 
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "administrators";
$menu_option = "administrators-all";
$main_section = "administrators/administratos-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    case "new": {
        $menu_section = "administrators";
        $menu_option = "administrators-new";
        $main_section = "administrators/administrator-new.php"; //Section to load as main content
        
        break;
    }
    
    case "create": {
        $menu_section = "administrators";
        $menu_option = "administrators-all";
        $main_section = "administrators/administrator-list.php"; //Section to load as main content
        
        //Create new Administrator
        $newAdmin = new Administrator();
        $newAdmin->readFromRow($_REQUEST);
        $newAdmin = AdministratorDAO::createAdministrator($newAdmin);
        if(($newAdmin!=null) && ($newAdmin->getId_administrator()>0)) {
            //ok
            $successMessage = "New administrator has been created";
            
            $menu_section = "administrators";
            $menu_option = "administrators-all";
            $main_section = "administrators/administrator-list.php"; //Section to load as main content
            
            $administrators = AdministratorDAO::getAdministratorsList();
            
        } else {
            //error - go to new, with error message
            $errorMessage = "Error creating administrator. Please, review info and try again";
            
            $menu_section = "administrators";
            $menu_option = "administrators-new";
            $main_section = "administrators/administrator-new.php"; //Section to load as main content
        }
        
        break;
    }
    
    case "delete": {
        $menu_section = "administrators";
        $menu_option = "administrators-all";
        $main_section = "administrators/administrator-list.php"; //Section to load as main content
        
        //delete Administrator
        $delAdmin = new Administrator();
        $delAdmin->readFromRow($_REQUEST);
        $deleted = AdministratorDAO::deleteAdministrator($delAdmin);
        if($deleted) {
            //ok with message
            $successMessage = "Administrator has been deleted";
            
            $menu_section = "administrators";
            $menu_option = "administrators-all";
            $main_section = "administrators/administrator-list.php"; //Section to load as main content
            
            $administrators = AdministratorDAO::getAdministratorsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting administrator. Please, try again";
            
            $menu_section = "administrators";
            $menu_option = "administrators-all";
            $main_section = "administrators/administrator-list.php"; //Section to load as main content
            
            $administrators = AdministratorDAO::getAdministratorsList();
        }
        
        break;
    }
    
    case "edit": {
        //edit administrator
        $editAdmin = new Administrator();
        $editAdmin->readFromRow($_REQUEST);
        
        if($editAdmin->getId_administrator()>0) {
            $editAdmin = AdministratorDAO::getAdministrator($editAdmin->getId_administrator());
        }
        
        if(($editAdmin!=null) && ($editAdmin->getId_administrator()>0)) {
            $menu_section = "administrators";
            $main_section = "administrators/administrator-edit.php"; //Section to load as main content
        } else {
            //admin not found
            header("Location: /admin/administrators.php");
        }
        break;
    }
    
    case "update": {
        $updated = false;
        
        $menu_section = "administrators";
        $menu_option = "administrators-all";
        $main_section = "administrators/administrator-list.php"; //Section to load as main content
        
        //update administrator
        $updAdmin = new Administrator();
        $updAdmin->readFromRow($_REQUEST);
        
        if($updAdmin->getId_administrator()>0) {
            
            $updAdmin = AdministratorDAO::getAdministrator($updAdmin->getId_administrator()); //read all admin info
            
            $picture = $updAdmin->getPicture();
            
            $updAdmin->readFromRow($_REQUEST); //change only this form info
            
            //TODO - SUBIR FOTO SI LLEGA FICHERO
            
            //si hay fichero, subimos
            if(count($_FILES)>0) {
                //echo(":::::::::FILES::::::::");die();
                //$currentDir = getcwd();
                $currentDir = dirname( dirname(__FILE__) );
                $uploadDirectory = "/uploads/";
                
                $errors = []; // Store all foreseen and unforseen errors here
                
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                
                $fileName = $_FILES['formfile']['name'];
                $fileSize = $_FILES['formfile']['size'];
                $fileTmpName  = $_FILES['formfile']['tmp_name'];
                $fileType = $_FILES['formfile']['type'];
                
                $tmpArr = explode('.',$fileName);
                $tmpExt = end($tmpArr);
                $fileExtension = strtolower($tmpExt);
                
                $uploadPath = $currentDir . $uploadDirectory . basename($fileName);
                
                $i=0;
                while (file_exists($uploadPath)) {
                    $i++;
                    $uploadPath = $currentDir . $uploadDirectory .$i."_".basename($fileName);
                }
                
                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }
                
                if ($fileSize > 6000000) {
                    $errors[] = "This file is more than 6MB. Sorry, it has to be less than or equal to 2MB";
                }
                
                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                    
                    if ($didUpload) {
                        $updAdmin->setPicture($uploadDirectory.basename($fileName)); //Actualizamos la ruta
                    } else {
                        echo "<!-- An error occurred somewhere. Try again or contact the admin -->";
                    }
                } else {
                    foreach ($errors as $error) {
                        //echo $error . "These are the errors" . "\n";
                    }
                }
            } //hay fichero
            
            
            $updAdmin = AdministratorDAO::updateAdministrator($updAdmin);
            
            if(isset($_REQUEST["password"]) && ($_REQUEST["password"]!="")) {
                AdministratorDAO::updateAdministratorPassword($updAdmin->getId_administrator(), $_REQUEST["password"]);
            }
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Administrator has been updated";
            
            $menu_section = "administrators";
            $menu_option = "administrators-all";
            $main_section = "administrators/administrator-list.php"; //Section to load as main content
            
            $administrators = AdministratorDAO::getAdministratorsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating administrator. Please, try again";
            
            $menu_section = "administrators";
            $menu_option = "administrators-all";
            $main_section = "administrators/administrator-list.php"; //Section to load as main content
            
            $administrators = AdministratorDAO::getAdministratorsList();
        }
        
        break;
    }
    
    default: {
        
        $menu_section = "administrators";
        $menu_option = "administrators-all";
        $main_section = "administrators/administrator-list.php"; //Section to load as main content
        
        $administrators = AdministratorDAO::getAdministratorsList();
        
        break;
    }
}



include("template.php");
?>
<?php 
/**
 * PLAYDATE - ADMIN - CONTACTS
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "contacts";
$menu_option = "contacts-all";
$main_section = "contacts/contact-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    
    case "view": {
        //view contact message
        $viewContact = new Contact();
        $viewContact->readFromRow($_REQUEST);
        
        if($viewContact->getId_contact()>0) {
            $viewContact = ContactDAO::getContact($viewContact->getId_contact());
        }
        //echo("1");die();
        if(($viewContact!=null) && ($viewContact->getId_contact()>0)) {
            
            ContactDAO::markContactRead($viewContact->getId_contact());
            
            $menu_section = "contacts";
            $main_section = "contacts/contact-view.php"; //Section to load as main content
        } else {
            //admin not found
            header("Location: /admin/contacts.php");
        }
        break;
    }
    
    
    
    case "markread": {
        $menu_section = "contacts";
        $menu_option = "contacts-all";
        $main_section = "contacts/contact-list.php"; //Section to load as main content
        
        //mark contact read
        $updContact = new Contact();
        $updContact->readFromRow($_REQUEST);
        $marked = ContactDAO::markContactRead($updContact);
        if($marked) {
            //ok with message
            $successMessage = "Contact has been marked as read";
            
            $menu_section = "contacts";
            $menu_option = "contacts-all";
            $main_section = "contacts/contact-list.php"; //Section to load as main content
            
            $contacts = ContactDAO::getContactsByType(Contact::$TYPE_CONTACT);
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting administrator. Please, try again";
            
            $menu_section = "administrators";
            $menu_option = "administrators-all";
            $main_section = "administrators/administrator-list.php"; //Section to load as main content
            
            $contacts = ContactDAO::getContactsByType(Contact::$TYPE_CONTACT);
        }
        
        break;
    }
    
    case "markunread": {
        $menu_section = "contacts";
        $menu_option = "contacts-all";
        $main_section = "contacts/contact-list.php"; //Section to load as main content
        
        //mark contact read
        $updContact = new Contact();
        $updContact->readFromRow($_REQUEST);
        $marked = ContactDAO::markContactUnread($updContact->getId_contact());
        if($marked) {
            //ok with message
            $successMessage = "Contact has been marked as unread";
            
            $menu_section = "contacts";
            $menu_option = "contacts-all";
            $main_section = "contacts/contact-list.php"; //Section to load as main content
            
            $contacts = ContactDAO::getContactsByType(Contact::$TYPE_CONTACT);
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error changing contact message status. Please, try again";
            
            $menu_section = "contacts";
            $menu_option = "contacts-all";
            $main_section = "contacts/contact-list.php"; //Section to load as main content
            
            $contacts = ContactDAO::getContactsByType(Contact::$TYPE_CONTACT);
        }
        
        break;
    }
    
    
    
    case "delete": {
        $menu_section = "contacts";
        $menu_option = "contacts-all";
        $main_section = "contacts/contact-list.php"; //Section to load as main content
        
        //delete contact
        $delContact = new Contact();
        $delContact->readFromRow($_REQUEST);
        $deleted = ContactDAO::deleteContact($delContact);
        if($deleted) {
            //ok with message
            $successMessage = "Contact message has been deleted";
            
            $menu_section = "contacts";
            $menu_option = "contacts-all";
            $main_section = "contacts/contact-list.php"; //Section to load as main content
            
            $contacts = ContactDAO::getContactsByType(Contact::$TYPE_CONTACT);
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting contact message. Please, try again";
            
            $menu_section = "contacts";
            $menu_option = "contacts-all";
            $main_section = "contacts/contact-list.php"; //Section to load as main content
            
            $contacts = ContactDAO::getContactsByType(Contact::$TYPE_CONTACT);
        }
        
        break;
    }
    
    
    case "read": {
        
        $menu_section = "contacts";
        $menu_option = "contacts-read";
        $main_section = "contacts/contact-list.php"; //Section to load as main content
        
        $contacts = ContactDAO::getReadContactList(Contact::$TYPE_CONTACT);
        
        break;
    }
    
    case "unread": {
        
        $menu_section = "contacts";
        $menu_option = "contacts-unread";
        $main_section = "contacts/contact-list.php"; //Section to load as main content
        
        $contacts = ContactDAO::getUnreadContactListByType(Contact::$TYPE_CONTACT);
        
        break;
    }
    
    
    default: {
        
        $menu_section = "contacts";
        $menu_option = "contacts-all";
        $main_section = "contacts/contact-list.php"; //Section to load as main content
        
        $contacts = ContactDAO::getContactsByType(Contact::$TYPE_CONTACT);
        
        break;
    }
}



include("template.php");
?>

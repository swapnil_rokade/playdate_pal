<?php 
/**
 * PLAYDATE ADMIN - PART - PARENT REQUEST LIST
 * 
 */
if(!isset($parentrequests)) {$parentrequests=array();}

?>
<!-- START PARENT REQUESTS LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Parent Requests</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/parentrequests.php">Parent Requests</a></li>
						<li class="active"><strong>All requests</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All parent requests</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Date</th>
										<th>Parent</th>
										<th>Info</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($parentrequests as $auxRequest) {
									    $parentName = "";
									    if($auxRequest->getId_parent()>0) {
									        $parent = ParentDAO::getParent($auxRequest->getId_parent());
									        if($parent!=null) {
									            $parentName = $parent->getFullName();
											} else {
											    $parentName = "<span style=\"color:red\">! No Parent</span>";
											}
									    }
									?>
									<tr>
										<td><?php echo $auxRequest->getId_request() ?></td>
										<td><?php echo Utils::dateFormat($auxRequest->getDate()) ?></td>
										<td><?php echo $parentName ?></td>
										<td><?php echo Utils::getLimitedText($auxRequest->getInfo(), 120) ?></td>
										<td>
										
										<a href="/admin/parentrequests.php?action=view&id_request=<?php echo $auxRequest->getId_request() ?>" class="bt-view"
											title="View"><span class="fa fa-eye"></span</a>
											
											<a
											title="Delete" href="/admin/parentrequests.php?action=delete&id_request=<?php echo $auxRequest->getId_request() ?>" onclick="return confirm('Are you sure you want to remove this parent request and all its information? This operation cannot be undone.');" class="bt-delete">
											<span class="fa fa-trash"></span></a>
											</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END PARENT REQUESTS LIST CONTENT -->
<?php
/**
 * PLAYDATE ADMIN - PLAYDATE VIEW FORM 
 */
if (! isset($editPlaydateRequest)) {
    header("Location: /admin/parentrequests.php");
}

//$editPlaydateRequest = new PlaydateRequest();

?>
<style>
.income {color:green;}
.expense {color:red;}
.completed {color:green;}
.partial {color:red;}
.pending {color:orange;}
</style>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">View Playdate Request</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/playdates.php">Parent Requests</a></li>
						<li class="active"><a href="/admin/parentrequests.php?action=view&id_request=<?php echo $editPlaydateRequest->getId_request() ?>"><strong>View Playdate Request</strong></a></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Basic Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>
							
								<div class="form-group">
									<strong>Parent</strong>: 
									<p>
									<?php 
									$parent = ParentDAO::getParent($editPlaydateRequest->getId_parent());
									?>
									<?php echo $parent->getFullName() ?> (<?php echo $parent->getEmail() ?>) - <?php echo $parent->getCellphone() ?></p>
								</div>
								
								
								
								<div class="form-group">
									<strong>Date</strong>: 
									<p><?php echo Utils::dateFormat($editPlaydateRequest->getDate()) ?>, <?php echo Utils::get12hourFormat($editPlaydateRequest->getTime_init()) ?> - <?php echo Utils::get12hourFormat($editPlaydateRequest->getTime_end()) ?></p>
								</div>
								
								<div class="form-group">
									<strong>Type</strong>: 
									<?php 
									$typeName = "Not selected";
									if($editPlaydateRequest->getType()!=null) {
									    $mytype = TypecareDAO::getTypecare($editPlaydateRequest->getType());
									    if($mytype!=null) {
									        $typeName = $mytype->getName();
									    }
									}
									else if($editPlaydateRequest->getPlaydate_type()!=null){
									    if($editPlaydateRequest->getPlaydate_type() == 1){
									        	$typeName = "Oneoff Playdate";
									    }
									    else{
									        $typeName = "Recurring Playdate";
									    }
									}
									?>
									<p><?php echo $typeName ?></p>
								</div>
								
								<div class="form-group">
									<strong>Location</strong>: 
									<p><?php echo (($editPlaydateRequest->getLocation()!=null)?$editPlaydateRequest->getLocation():"Not set") ?></p>
								</div>
								
								<div class="form-group">
									<strong>Travel Pickup</strong>: 
									<p><?php echo (($editPlaydateRequest->getTravel_pickup()!=null)?$editPlaydateRequest->getTravel_pickup():"Not set") ?></p>
								</div>
								<div class="form-group">
									<strong>Travel Dropoff</strong>: 
									<p><?php echo (($editPlaydateRequest->getTravel_dropoff()!=null)?$editPlaydateRequest->getTravel_dropoff():"Not set") ?></p>
								</div>
										
								<div class="form-group">
									<strong>Budget Min / Max</strong>: 
									<p><?php echo (($editPlaydateRequest->getBudget_init()!=null)?$editPlaydateRequest->getBudget_init():"Not set") ?> / <?php echo (($editPlaydateRequest->getBudget_end()!=null)?$editPlaydateRequest->getBudget_end():"Not set") ?></p>
								</div>
								
								<div class="form-group">
									<strong>Information</strong>: 
									<p><?php 
									if($editPlaydateRequest->getInfo() != "null"){
									    echo $editPlaydateRequest->getInfo();
									}else{
									    echo $editPlaydateRequest->getNotes();
									}
									 ?></p>
								</div>	
								
								<div class="form-group">
									<strong>Group Size</strong>: 
									<p><?php echo $editPlaydateRequest->getGroup_size() ?></p>
								</div>	
								
								<div class="form-group">
									<strong>Age Range</strong>: 
									<p><?php 
									$arr = explode(",",$editPlaydateRequest->getAge_range());
									if(!empty($arr)){
									    foreach($arr as $k=>$v){
									        if($v == 1)
									        $arr[$k] = "Under 3";
									         if($v == 2)
									        $arr[$k] = "3 - 5";
									         if($v == 3)
									        $arr[$k] = "5 - 7";
									         if($v == 4)
									        $arr[$k] = "7 or up";
									    }
									}
									$arr = implode (" & ", $arr);
							        echo $arr;
                                    ?></p>
								</div>	
								
								<div class="form-group">
									<strong>Theme of Playdate</strong>: 
									<p><?php $tarr = explode(",",$editPlaydateRequest->getTheme_of_playdate());
									if(!empty($tarr)){
									    foreach($tarr as $k=>$v){
									        if($v == 1)
									        $tarr[$k] = "Anything fun!";
									         if($v == 2)
									        $tarr[$k] = "Academic";
									         if($v == 3)
									        $tarr[$k] = "Creative";
									         if($v == 4)
									        $tarr[$k] = "Active";
									         if($v == 5)
									        $tarr[$k] = "Parent Social";
									        
									    }
									}
									$tarr = implode (" & ", $tarr);
							        echo $tarr;
							        ?></p>
								</div>	
								
								
								<form name="frm_delete" action="/admin/parentrequests.php">
								<input type="hidden" name="action" value="delete" />
								<input type="hidden" name="id_request" value="<?php echo $editPlaydateRequest->getId_request() ?>" />
								
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    								<div class="form-group">
        								<div class="text-left">
        									<button type="submit" class="btn btn-primary" style="margin-top:5px;width:100%" onclick="return confirm('Are you sure you want to remove this parent request and all its information? This operation cannot be undone.');">Delete this request</button>
        								</div>
    								</div>
    							</div>
								
								</form>			
							</div>
							
						
					</div>


				</div>
			</section>
		</div>
	
	</section>
</section>
<!-- END CONTENT -->
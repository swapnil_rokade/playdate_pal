<?php
include_once("BaseEntity.class.php");

class PlaydateRequest extends BaseEntity {
    
    public static $STATUS_REQUEST_OPEN=0;
    public static $STATUS_REQUEST_CLOSED=1;
    
    protected $id_request, $id_parent, $id_group, $type, $location, $travel_pickup, $travel_dropoff, $date, $time_init, $time_end, $budget_init, $budget_end, $extra_kids, $info, $num_supporters, $public_job_board, $status;
    

    /**
    * Constructor
    */ 
    public function __construct( $id_request=-1) {
        $this->id_request = $id_request;
    }
   
	public function readFromRow($row){
	    if(isset($row['id_request'])) { $this->setId_request($row['id_request']); }
	    if(isset($row['id_parent'])) { $this->setId_parent($row['id_parent']); }
	    if(isset($row['id_group'])) { $this->setId_group($row['id_group']); }
	    if(isset($row['type'])) { $this->setType($row['type']);}
	    if(isset($row['location'])) { $this->setLocation($row['location']);}
	    if(isset($row['travel_pickup'])) { $this->setTravel_pickup($row['travel_pickup']);}
	    if(isset($row['travel_dropoff'])) { $this->setTravel_dropoff($row['travel_dropoff']);}
	    if(isset($row['date']) && ($row['date']!="0000-00-00 00:00:00")) $this->date=($this->toStringDateFormat($row['date']));
	    if(isset($row['time_init'])) { $this->setTime_init($row['time_init']);}
	    if(isset($row['time_end'])) { $this->setTime_end($row['time_end']);}
	    if(isset($row['budget_init'])) { $this->setBudget_init($row['budget_init']);}
	    if(isset($row['budget_end'])) { $this->setBudget_end($row['budget_end']);}
	    if(isset($row['extra_kids'])) { $this->setExtra_kids($row['extra_kids']);}
	    if(isset($row['info'])) { $this->setInfo($row['info']);}
	    if(isset($row['num_supporters'])) { $this->setNum_supporters($row['num_supporters']);}
	    if(isset($row['public_job_board'])) { $this->setPublic_job_board($row['public_job_board']);}
	    if(isset($row['status'])) { $this->setStatus($row['status']);}
	    
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_request()
    {
        return $this->id_request;
    }

    /**
     * @param mixed $id_request
     */
    public function setId_request($id_request)
    {
        $this->id_request = $id_request;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getTravel_pickup()
    {
        return $this->travel_pickup;
    }

    /**
     * @param mixed $travel_pickup
     */
    public function setTravel_pickup($travel_pickup)
    {
        $this->travel_pickup = $travel_pickup;
    }

    /**
     * @return mixed
     */
    public function getTravel_dropoff()
    {
        return $this->travel_dropoff;
    }

    /**
     * @param mixed $travel_dropoff
     */
    public function setTravel_dropoff($travel_dropoff)
    {
        $this->travel_dropoff = $travel_dropoff;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTime_init()
    {
        return $this->time_init;
    }

    /**
     * @param mixed $time_init
     */
    public function setTime_init($time_init)
    {
        $this->time_init = $time_init;
    }

    /**
     * @return mixed
     */
    public function getTime_end()
    {
        return $this->time_end;
    }

    /**
     * @param mixed $time_end
     */
    public function setTime_end($time_end)
    {
        $this->time_end = $time_end;
    }

    /**
     * @return mixed
     */
    public function getBudget_init()
    {
        return $this->budget_init;
    }

    /**
     * @param mixed $budget_init
     */
    public function setBudget_init($budget_init)
    {
        $this->budget_init = $budget_init;
    }

    /**
     * @return mixed
     */
    public function getBudget_end()
    {
        return $this->budget_end;
    }

    /**
     * @param mixed $budget_end
     */
    public function setBudget_end($budget_end)
    {
        $this->budget_end = $budget_end;
    }

    /**
     * @return mixed
     */
    public function getExtra_kids()
    {
        return $this->extra_kids;
    }

    /**
     * @param mixed $extra_kids
     */
    public function setExtra_kids($extra_kids)
    {
        $this->extra_kids = $extra_kids;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getNum_supporters()
    {
        return $this->num_supporters;
    }

    /**
     * @param mixed $num_supporters
     */
    public function setNum_supporters($num_supporters)
    {
        $this->num_supporters = $num_supporters;
    }

    /**
     * @return mixed
     */
    public function getPublic_job_board()
    {
        return $this->public_job_board;
    }

    /**
     * @param mixed $public_job_board
     */
    public function setPublic_job_board($public_job_board)
    {
        $this->public_job_board = $public_job_board;
    }
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function getId_group()
    {
        return $this->id_group;
    }

    /**
     * @param mixed $id_group
     */
    public function setId_group($id_group)
    {
        $this->id_group = $id_group;
    }



	
	
} 
?>
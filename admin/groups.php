<?php 
/**
 * PLAYDATE - ADMIN - GROUPS
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "groups";
$menu_option = "groups-all";
$main_section = "groups/groups-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    
    case "delete": {
        $menu_section = "groups";
        $menu_option = "groups-all";
        $main_section = "groups/groups-list.php"; //Section to load as main content
        
        //delete Group
        $delGroup = new Group();
        $delGroup->readFromRow($_REQUEST);
        $deleted = GroupDAO::deleteGroup($delGroup->getId_group());
        if($deleted) {
            //ok with message
            $successMessage = "Group has been deleted";

            $groups = GroupDAO::getGroupsList(1, 1000, null, null);
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting group. Please, try again";
            
            $menu_section = "groups";
            $menu_option = "groups-all";
            $main_section = "groups/groups-list.php"; //Section to load as main content
            
            $groups = GroupDAO::getGroupsList(1, 1000, null, null);
        }
        
        break;
    }
    
    case "view": {
        //view group
        $editGroup = new Group();
        $editGroup->readFromRow($_REQUEST);
        
        if($editGroup->getId_group()>0) {
            $editGroup= GroupDAO::getGroup($editGroup->getId_group());
        }
        
        if(($editGroup!=null) && ($editGroup->getId_group()>0)) {
            $menu_section = "groups";
            $main_section = "groups/groups-view.php"; //Section to load as main content
        } else {
            //group not found
            header("Location: /admin/groups.php");
        }
        break;
    }
    
    /*
    case "edit": {
        //view parent
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        if(($editPlaydate!=null) && ($editPlaydate->getId_playdate()>0)) {
            $menu_section = "playdates";
            $main_section = "playdates/playdates-edit.php"; //Section to load as main content
        } else {
            //playdate not found
            header("Location: /admin/playdates.php");
        }
        break;
    }
    */
    /*
    case "update-basic": {
        $updated = false;
        
        $menu_section = "playdates";
        $menu_option = "playdates-all";
        $main_section = "playdates/playdates-view.php"; //Section to load as main content
        
        //update playdate
        $updPlaydate = new Playdate();
        $updPlaydate->readFromRow($_REQUEST);
        
        if($updPlaydate->getId_playdate()>0) {
            $updPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate()); //read all playdate info
            
            $updPlaydate->readFromRow($_REQUEST); //change only this form basic info
            
            $updPlaydate = PlaydateDAO::updatePlaydateBasics($updPlaydate);
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Playdate has been updated";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating playdate. Please, try again";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
        }
        
        break;
    }
    */
    
    default: {
        
        $menu_section = "groups";
        $menu_option = "groups-all";
        $main_section = "groups/groups-list.php"; //Section to load as main content
        
        $groups = GroupDAO::getGroupsList(1, 1000, null, null);
        
        break;
    }
}



include("template.php");
?>
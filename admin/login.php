<?php 
/**
 * PLAYATE ADMIN - Login Page
 */

include_once("adm-common.php");

include_once("../classes/all_classes.php");
include_once("connection.php");

$pageTitle = "Login Page";
$errorMessage = null;

if(isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        
        case "login": {
            $administrator = null;
            if(isset($_REQUEST["inputEmail"]) && ($_REQUEST["inputEmail"]!="") && isset($_REQUEST["inputPassword"]) && ($_REQUEST["inputPassword"]!="")) {
                
                $administrator = AdministratorDAO::getAdministratorByEmailPassword($_REQUEST['inputEmail'], $_REQUEST['inputPassword']);
                if ($administrator == null) {
                    $administrator = AdministratorDAO::getAdministratorByUsernamePassword($_REQUEST['inputEmail'], $_REQUEST['inputPassword']);
                }
                
                if ($administrator != null) {
                    // usuario activo, validacion ok
                    
                    $_SESSION[ADM_SESSION_ID] = $administrator->getId_administrator();
                    $_SESSION[ADM_SESSION_NAME] = $administrator->getFullName();
                    $_SESSION[ADM_SESSION_PICTURE] = $administrator->getPicture();
                    
                    $dest_redirect = "/admin/";
                    if (isset($_REQUEST["backurl"])) {
                        $dest_redirect = $_REQUEST["backurl"];
                    }
                    header("Location:" . $dest_redirect);
                }
            }
            
            if($administrator==null) {
                $errorMessage = "User and password combination is not correct. Try again.";
            }
            break;
        }
    }
}
?>
<!DOCTYPE html>
<html class=" ">
    <head>
		<?php include("parts/adm-header.php") ?>
		
    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body class=" login_page">
    
        <div class="fullscreen-bg">
            <video poster="http://playdate.airtouchmedia.com/video/video-home.jpg" autoplay="" muted="" playsinline="" preload="metadata" loop="" tabindex="0" class="fullscreen-bg__video">
               <source src="http://playdate.airtouchmedia.com/video/video-home.webm" type="video/webm; codecs=&quot;vp8, vorbis&quot;">
               <source src="http://playdate.airtouchmedia.com/video/video-home.ogv" type="video/ogg; codecs=&quot;theora, vorbis&quot;">
                <source src="https://playdate.airtouchmedia.com/video/video-home.mp4" type="video/mp4">
            </video>
        </div>
        

        <div class="login-wrapper">
            <div id="login" class="login loginpage col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-0 col-xs-12">
                <h1><a href="/admin/" title="Login Page" tabindex="-1">Pal by project Playdate</a></h1>

                <form name="loginform" id="loginform" action="/admin/login.php" method="post">
                	<input type="hidden" name="action" value="login" />
                	
                	<?php if($errorMessage!=null) { ?>
                	<div class="alert alert-error alert-dismissible fade in">
                    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <strong>Warning:</strong> <?php echo $errorMessage ?>
                    </div>
                	<?php } ?>
                	
                    <p>
                        <label for="user_login">Username or Email<br />
                            <input type="text" id="user_login" class="input" value="" size="20" name="inputEmail" placeholder="Username or Email" /></label>
                    </p>
                    <p>
                        <label for="user_pass">Password<br />
                            <input type="password" id="user_pass" class="input" value="" size="20" name="inputPassword" placeholder="Password" /></label>
                    </p>
                    <p class="forgetmenot">
                        <label class="icheck-label form-label" for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever" class="skin-square-orange" checked> Remember me</label>
                    </p>
					<p class="submit">
                        <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-purple btn-block" value="Sign In" />
                    </p>
                </form>

                <p id="nav">
                <?php /*
                    <a class="pull-left" href="#" title="Password Lost and Found">Forgot password?</a>
                    <a class="pull-right" href="ui-register.html" title="Sign Up">Sign Up</a>
                */?>
                </p>

            </div>
        </div>

        <?php include("parts/adm-libs.php"); ?> 

    </body>
</html>
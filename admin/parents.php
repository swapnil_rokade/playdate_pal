<?php 
/**
 * PLAYDATE - ADMIN 
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "parents";
$menu_option = "parents-all";
$main_section = "parents/parent-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    case "add-credit": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update administrator
        $updParent = new ParentPd();
        $updParent ->readFromRow($_REQUEST);
        
        if($updParent ->getId_parent()>0) {
            $updParent = ParentDAO::getParent($updParent->getId_parent()); //read all parent info
            
            //Almacenamos info de purchase
            $newPurchase = new Purchase();
            $newPurchase->readFromRow($_REQUEST);
            $newPurchase->setAmount($newPurchase->getAmount()*100); //they send dollars
            PurchaseDAO::createPurchase($newPurchase);
            
            $parent = ParentDAO::getParent($updParent ->getId_parent());
            $prevCredit = $parent->getCredit();
            //Incrementamos el saldo
            ParentDAO::updateParentCredit($parent->getId_parent(), $prevCredit+($newPurchase->getAmount()));
            
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Parent credit has been increased";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error increasing parent credit. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "pull-credit": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //parent info
        $updParent = new ParentPd();
        $updParent ->readFromRow($_REQUEST);
        
        if($updParent ->getId_parent()>0) {
            $updParent = ParentDAO::getParent($updParent->getId_parent()); //read all parent info
            
            //Almacenamos info de purchase
            $newPayment = new Payment();
            $newPayment->readFromRow($_REQUEST);
            $newPayment->setAmount_playdate($newPayment->getAmount_playdate()*100); //they send dollars
            $newPayment->setAmount_addons(0);
            $newPayment->setId_parent($updParent->getId_parent());
            $newPayment->setId_playdate(null); //no playdate
            $newPayment->setName("[Charge] ".$newPayment->getComment());
            
            
            PaymentDAO::createParentPayment($newPayment);
            
            //Realizamos la ejecucion de pagos pendientes de este padre con este playdate (solo ser� el pago actual)
            $payments = PaymentDAO::getPaymentListByParent($updParent->getId_parent());
            if($payments!=null) {
                foreach($payments as $auxPay) {
                    if($auxPay->getStatus() == Payment::$STATUS_PENDING) {
                        //Execute this pending payment
                        PaymentDAO::executeParentPayment($auxPay->getId_payment(), $auxPay->getComment());
                    }
                }
            }
            
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Parent credit has been pulled";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error increasing parent credit. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "delete": {
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //delete Parent
        $delParent = new ParentPd();
        $delParent->readFromRow($_REQUEST);
        $deleted = ParentDAO::deleteParent($delParent);
        if($deleted) {
            //ok with message
            $successMessage = "Parent has been deleted";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting parent. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "edit": {
        //view parent
        $editParent = new ParentPd();
        $editParent->readFromRow($_REQUEST);
        
        if($editParent->getId_parent()>0) {
            $editParent= ParentDAO::getParent($editParent->getId_parent());
        }
        
        if(($editParent!=null) && ($editParent->getId_parent()>0)) {
            $menu_section = "parents";
            $main_section = "parents/parent-edit.php"; //Section to load as main content
        } else {
            //parent not found
            header("Location: /admin/parents.php");
        }
        break;
    }
    
    case "update": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update administrator
        $updParent = new ParentPd();
        $updParent ->readFromRow($_REQUEST);
        
        if($updParent ->getId_parent()>0) {
            $updParent = ParentDAO::getParent($updParent->getId_parent()); //read all parent info
            
            $picture = $updParent->getPicture();
            
            $updParent->readFromRow($_REQUEST); //change only this form parent info
            
            //TODO - SUBIR FOTO SI LLEGA FICHERO
            
            $updParent->setPicture($picture);
            
            $updParent = ParentDAO::updateParent($updParent);
            
            if(isset($_REQUEST["password"]) && ($_REQUEST["password"]!="")) {
                ParentDAO::updateParentPassword($updParent->getId_parent(), $_REQUEST["password"]);
            }
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Parent has been updated";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating parent. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "updateavailability": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update administrator
        $updParent = new ParentPd();
        $updParent->readFromRow($_REQUEST);
        
        if($updParent ->getId_parent()>0) {
            $updParent = ParentDAO::getParent($updParent->getId_parent()); //read all parent info
            $updParent ->readFromRow($_REQUEST); //change only this form parent info
            $updParent = ParentDAO::updateParent($updParent);
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Parent availability has been updated";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating parent availability. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "updatechildren": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update children
        $updChildren = new Children();
        $updChildren->readFromRow($_REQUEST);
        
        if($updChildren->getId_children()>0) {
            $updChildren = ChildrenDAO::getChildren($updChildren->getId_children()); //read all children info
            $updChildren->readFromRow($_REQUEST); //change only this form info
            $updChildren = ChildrenDAO::updateChildren($updChildren);
            
            if(isset($_POST["remove_kid"]) && ($_POST["remove_kid"]=="1")) {
                //Delete kid, if possible
                ChildrenDAO::deleteChildren($updChildren->getId_children());
            }
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Children has been updated";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating children info. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "updateemergency": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update emergency
        $updEmergency = new Emergency();
        $updEmergency->readFromRow($_REQUEST);
        
        if($updEmergency->getId_emergency()>0) {
            $updEmergency = EmergencyDAO::getEmergency($updEmergency->getId_emergency()); //read all info
            $updEmergency->readFromRow($_REQUEST); //change only this form info
            $updEmergency = EmergencyDAO::updateEmergency($updEmergency);
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Emergency contact has been updated";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating emergency contact. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    default: {
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        $parents = ParentDAO::getParentsList(1,1000,null,null);
        
        break;
    }
}



include("template.php");
?>
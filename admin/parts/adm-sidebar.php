<?php 
/**
 * PLAYDATE ADMIN - TOPBAR (welcome, alerts, etc.)
 */

if(!isset($sessionAdministrator)) {
    $sessionAdministrator = AdministratorDAO::getAdministrator($_SESSION[ADM_SESSION_ID]);
}

$admpicture = $sessionAdministrator->getPicture();
if(($admpicture==null) || ($admpicture=="")) {
    $admpicture = "data/profile/profile-crm.jpg";
}
?>
            <!-- SIDEBAR - START --> 
            <div class="page-sidebar ">

                <!-- MAIN MENU - START -->
                <div class="page-sidebar-wrapper" id="main-menu-wrapper"> 

                    <!-- USER INFO - START -->
                    <div class="profile-info row">

                        <div class="profile-image col-md-4 col-sm-4 col-xs-4">
                            <a href="/admin/administrators.php?action=edit&id_administrator=<?php echo $sessionAdministrator->getId_administrator() ?>">
                                <img src="<?php echo $admpicture ?>" class="img-responsive img-circle">
                            </a>
                        </div>

                        <div class="profile-details col-md-8 col-sm-8 col-xs-8">

                            <h3>
                                <a href="ui-profile.html"><?php echo $sessionAdministrator->getFullName() ?></a>

                                <!-- Available statuses: online, idle, busy, away and offline -->
                                <span class="profile-status online"></span>
                            </h3>

                            <p class="profile-title">Administrator</p>

                        </div>

                    </div>
                    <!-- USER INFO - END -->



                    <ul class='wraplist'>	


                        <li class="<?php echo(($menu_section=="home")?"open":""); ?>"> 
                            <a href="/admin/index.php">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="<?php echo(($menu_section=="parentrequests")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-calendar"></i>
                                <span class="title">Parent Requests</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "parentrequests-all")?"active":"") ?>" href="/admin/parentrequests.php" >All Parent Requests</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php echo(($menu_section=="playdates")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-calendar"></i>
                                <span class="title">Playdates</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "playdates-all")?"active":"") ?>" href="/admin/playdates.php" >All Playdates</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php echo(($menu_section=="administrators")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-users"></i>
                                <span class="title">Administrators</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "administrators-all")?"active":"") ?>" href="/admin/administrators.php" >All Administrators</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "administrators-new")?"active":"") ?>" href="/admin/administrators.php?action=new" >Add Administrator</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php echo(($menu_section=="invitations")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-star"></i>
                                <span class="title">Invitations</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "invitations-all")?"active":"") ?>" href="/admin/invitations.php" >All Invitations</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "invitations-new")?"active":"") ?>" href="/admin/invitations.php?action=new" >Add Invitation</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php echo(($menu_section=="requests")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-star"></i>
                                <span class="title">Request for Access</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "requests-all")?"active":"") ?>" href="/admin/requests.php" >All Requests</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php echo(($menu_section=="parents")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-users"></i>
                                <span class="title">Parents</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "parents-all")?"active":"") ?>" href="/admin/parents.php" >All Parents</a>
                                </li>
                                <?php /*?>
                                <li>
                                    <a class="" href="parents-add.html" >Add Parent</a>
                                </li>
                                */?>
                            </ul>
                        </li>
                        
                        <li class="<?php echo(($menu_section=="specialists")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-users"></i>
                                <span class="title">Specialists</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "specialists-all")?"active":"") ?>" href="/admin/specialists.php" >All Specialists</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "specialists-new")?"active":"") ?>" href="/admin/specialists.php?action=new" >Add Specialist</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo(($menu_section=="groups")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-users"></i>
                                <span class="title">Groups</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "groups-all")?"active":"") ?>" href="/admin/groups.php" >All Groups</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo(($menu_section=="contacts")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-envelope"></i>
                                <span class="title">Contacts</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "contacts-all")?"active":"") ?>" href="/admin/contacts.php" >All Contacts</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "contacts-unread")?"active":"") ?>" href="/admin/contacts.php?action=unread" >Unread Contacts</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo(($menu_section=="contactsspecialist")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-envelope"></i>
                                <span class="title">Contacts Specialist</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "contactsspecialist-all")?"active":"") ?>" href="/admin/contactsspecialist.php" >All Contacts</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "contactsspecialist-unread")?"active":"") ?>" href="/admin/contactsspecialist.php?action=unread" >Unread Contacts</a>
                                </li>
                            </ul>
                        </li>
                        
                        
                        <li class="<?php echo(($menu_section=="contactshost")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-envelope"></i>
                                <span class="title">Contacts Spec. Hosts</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "contactshost-all")?"active":"") ?>" href="/admin/contactshost.php" >All Contacts</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "contactshost-unread")?"active":"") ?>" href="/admin/contactshost.php?action=unread" >Unread Contacts</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo(($menu_section=="contactsrequest")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-envelope"></i>
                                <span class="title">Contacts Request Playdate</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "contactsrequest-all")?"active":"") ?>" href="/admin/contactsrequest.php" >All Contacts</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "contactsrequest-unread")?"active":"") ?>" href="/admin/contactsrequest.php?action=unread" >Unread Contacts</a>
                                </li>
                            </ul>
                        </li>
                        
                        
                        <li class="<?php echo(($menu_section=="neighborhoods")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-envelope"></i>
                                <span class="title">Neighborhoods</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "neighborhoods-all")?"active":"") ?>" href="/admin/neighborhoods.php" >All Cities</a>
                                </li>
                            </ul>
                        </li>
                        
                        
                        <li class="<?php echo(($menu_section=="playpacks")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-tags"></i>
                                <span class="title">Playpacks</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "playpacks-all")?"active":"") ?>" href="/admin/playpacks.php" >All Playpacks</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "playpacks-new")?"active":"") ?>" href="/admin/playpacks.php?action=new" >Add Playpack</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo(($menu_section=="discount-code")?"open":""); ?>"> 
                            <a href="javascript:;">
                                <i class="fa fa-tags"></i>
                                <span class="title">Discount Code</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="<?php echo(($menu_option == "discounts-all")?"active":"") ?>" href="/admin/discounts.php" >All Discounts</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "discount-new")?"active":"") ?>" href="/admin/discounts.php?action=new" >Add Discount Code</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "discount-type")?"active":"") ?>" href="/admin/discounts.php?action=type" >Add Discount Type</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "discount-type-all")?"active":"") ?>" href="/admin/discounts.php?action=discount-type" >All Discount Types</a>
                                </li>
                                <li>
                                    <a class="<?php echo(($menu_option == "discount-usage-all")?"active":"") ?>" href="/admin/discounts.php?action=discount-usage" >All Discount Usage</a>
                                </li>
                                
                            </ul>
                        </li>

                        <?php /*?>
                        <li class=""> 
                            <a href="javascript:;">
                                <i class="fa fa-calendar"></i>
                                <span class="title">Playdates</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="playdates.html" >All Playdates</a>
                                </li>
                                <li>
                                    <a class="" href="playdates-add.html" >Add Playdate</a>
                                </li>
                                <li>
                                    <a class="" href="playdates-edit.html" >Edit Playdate</a>
                                </li>
                            </ul>
                        </li>
                        */?>
                    </ul>

                </div>
                <!-- MAIN MENU - END -->



                <div class="project-info">

                    <div class="block1">
                        <div class="data">
                            <span class='title'>Playdates</span>
                            <span class='total'><?php echo PlaydateDAO::countPlaydates() ?></span>
                        </div>
                        <div class="graph">
                            <span class="sidebar_orders">...</span>
                        </div>
                    </div>

                    <div class="block2">
                        <div class="data">
                            <span class='title'>Parents</span>
                            <span class='total'><?php echo ParentDAO::countParents() ?></span>
                        </div>
                        <div class="graph">
                            <span class="sidebar_visitors">...</span>
                        </div>
                    </div>

                </div>



            </div>
            <!--  SIDEBAR - END -->
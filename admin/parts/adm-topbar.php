<?php 
/**
 * PLAYDATE ADMIN - TOPBAR (welcome, alerts, etc.)
 */

$sessionAdministrator = AdministratorDAO::getAdministrator($_SESSION[ADM_SESSION_ID]);

$admpicture = $sessionAdministrator->getPicture();
if(($admpicture==null) || ($admpicture=="")) {
    $admpicture = "data/profile/profile-crm.jpg";
}

$numNotifications = ContactDAO::countUnreadContactsByType(Contact::$TYPE_REQUEST);
?>

        <!-- START TOPBAR -->
        <div class='page-topbar '>
            <div class='logo-area'>

            </div>
            <div class='quick-area'>
                <div class='pull-left'>
                    <ul class="info-menu left-links list-inline list-unstyled">
                        <li class="sidebar-toggle-wrap">
                            <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                        
                        <?php 
                        //LIST OF MESSAGES
                        $numMessages = ContactDAO::countUnreadContacts();
                        ?>
                        <li class="message-toggle-wrapper">
                            <a href="#" data-toggle="dropdown" class="toggle">
                                <i class="fa fa-envelope"></i>
                                <span class="badge badge-primary"><?php echo $numMessages ?></span>
                            </a>
                            <ul class="dropdown-menu messages animated fadeIn">
                            	<li class="total">
                                    <span class="small">
                                        You have <strong><?php echo $numMessages ?></strong> new messages.
                                        <?php if(false && ($numMessages>0)) { //disabled by now?>
                                        <a href="javascript:;" class="pull-right">Mark all as Read</a>
                                        <?php } ?>
                                    </span>
                                </li>
								<?php 
								//LIST OF NEW MESSAGES
								?>
                                <li class="list">
                                    <ul class="dropdown-menu-list list-unstyled ps-scrollbar">
                                    	<?php 
                                    	$messages = ContactDAO::getUnreadContactList();
                                    	foreach($messages as $auxMessage) {
                                    	    $link = "/admin/contacts.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	    $desc = "New message";
                                    	    $icon = "/admin/data/profile/message-contact.png";
                                    	    switch ($auxMessage->getType()) {
                                    	        case Contact::$TYPE_CONTACT: {
                                    	            $link = "/admin/contacts.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $auxMessage->getMessage(30);
                                    	            $icon = "/admin/data/profile/message-contact.png";
                                    	            break;
                                    	        }
                                    	        
                                    	        case Contact::$TYPE_HOST: {
                                    	            $link = "/admin/contactshost.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $auxMessage->getAvailability(30);
                                    	            $icon = "/admin/data/profile/message-host.png";
                                    	            break;
                                    	        }
                                    	        
                                    	        case Contact::$TYPE_SPECIALIST: {
                                    	            $link = "/admin/contactsspecialist.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $auxMessage->getExperience(30);
                                    	            $icon = "/admin/data/profile/message-specialist.png";
                                    	            break;
                                    	        }
                                    	        
                                    	        case Contact::$TYPE_REQUEST: {
                                    	            $msgparent = ParentDAO::getParent($auxMessage->getReq_id_parent());
                                    	            $link = "/admin/contactsrequest.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $msgparent->getName()." Request Playdate for ".$auxMessage->getReq_date();
                                    	            $icon = "/admin/data/profile/message-request.png";
                                    	            break;
                                    	        }
                                    	    }
                                    	    
                                    	    
                                    	?>
                                        <li class="unread status-available">
                                            <a href="<?php echo $link ?>">
                                                <div class="user-img">
                                                    <img src="<?php echo $icon ?>" alt="user-image" class="img-circle img-inline">
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong><?php echo $auxMessage->getName() ?></strong>
                                                        <span class="time small"><?php echo Utils::dateTimeFormat($auxMessage->getIdate()) ?></span>
                                                        <span class="profile-status available pull-right"></span>
                                                    </span>
                                                    <span class="desc small">
                                                        <?php echo $desc ?>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                
                                <?php if(false && ($numMessages>0)) { ?>
                                <li class="external">
                                    <a href="javascript:;">
                                        <span>Read All Messages</span>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>

                        </li>
                        <?php 
                        //NOTIFICATIONS
                        ?>
                        <li class="notify-toggle-wrapper">
                            <a href="#" data-toggle="dropdown" class="toggle">
                                <i class="fa fa-bell"></i>
                                <span class="badge badge-orange"><?php echo $numNotifications ?></span>
                            </a>
                            <ul class="dropdown-menu notifications animated fadeIn">
                                <li class="total">
                                    <span class="small">
                                        You have <strong><?php echo $numNotifications ?></strong> new notifications.
                                        <?php if(false && $numNotifications>0) { ?>
                                        <a href="javascript:;" class="pull-right">Mark all as Read</a>
                                        <?php } ?>
                                    </span>
                                </li>
                                <?php 
								//LIST OF NEW MESSAGES
								?>
                                <li class="list">
                                    <ul class="dropdown-menu-list list-unstyled ps-scrollbar">
                                    	<?php 
                                    	$messages = ContactDAO::getUnreadContactListByType(Contact::$TYPE_REQUEST);
                                    	foreach($messages as $auxMessage) {
                                    	    $link = "/admin/contactsrequest.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	    $desc = "New message";
                                    	    $icon = "/admin/data/profile/message-contact.png";
                                    	    switch ($auxMessage->getType()) {
                                    	        case Contact::$TYPE_CONTACT: {
                                    	            $link = "/admin/contacts.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $auxMessage->getMessage(30);
                                    	            $icon = "/admin/data/profile/message-contact.png";
                                    	            break;
                                    	        }
                                    	        
                                    	        case Contact::$TYPE_HOST: {
                                    	            $link = "/admin/contactshost.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $auxMessage->getAvailability(30);
                                    	            $icon = "/admin/data/profile/message-host.png";
                                    	            break;
                                    	        }
                                    	        
                                    	        case Contact::$TYPE_SPECIALIST: {
                                    	            $link = "/admin/contactsspecialist.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $auxMessage->getExperience(30);
                                    	            $icon = "/admin/data/profile/message-specialist.png";
                                    	            break;
                                    	        }
                                    	        
                                    	        case Contact::$TYPE_REQUEST: {
                                    	            $msgparent = ParentDAO::getParent($auxMessage->getReq_id_parent());
                                    	            $link = "/admin/contactsrequest.php?action=view&id_contact=".$auxMessage->getId_contact();
                                    	            $desc = $msgparent->getName()." Request Playdate for ".$auxMessage->getReq_date();
                                    	            $icon = "/admin/data/profile/message-request.png";
                                    	            break;
                                    	        }
                                    	    }
                                    	    
                                    	    
                                    	?>
                                        <li class="unread status-available">
                                            <a href="<?php echo $link ?>">
                                                <div class="user-img">
                                                    <img src="<?php echo $icon ?>" alt="user-image" class="img-circle img-inline">
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong><?php echo $auxMessage->getName() ?></strong>
                                                        <span class="time small"><?php echo Utils::dateTimeFormat($auxMessage->getIdate()) ?></span>
                                                        <span class="profile-status available pull-right"></span>
                                                    </span>
                                                    <span class="desc small">
                                                        <?php echo $desc ?>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php if(false && ($numNotifications>0)) {?>
                                <li class="external">
                                    <a href="javascript:;">
                                        <span>Read All Notifications</span>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php /*?>
                        <li class="hidden-sm hidden-xs searchform">
                            <div class="input-group">
                                <span class="input-group-addon input-focus">
                                    <i class="fa fa-search"></i>
                                </span>
                                <form action="search-page.html" method="post">
                                    <input type="text" class="form-control animated fadeIn" placeholder="Search & Enter">
                                    <input type='submit' value="">
                                </form>
                            </div>
                        </li>
                        */?>
                    </ul>
                </div>		
                <div class='pull-right'>
                    <ul class="info-menu right-links list-inline list-unstyled">
                        <li class="profile">
                            <a href="#" data-toggle="dropdown" class="toggle">
                                <img src="<?php echo $admpicture ?>" alt="user-image" class="img-circle img-inline">
                                <span><?php echo $sessionAdministrator->getFullname() ?><i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="dropdown-menu profile animated fadeIn">
                                <?php /*?>
                                <li>
                                    <a href="#settings">
                                        <i class="fa fa-wrench"></i>
                                        Settings
                                    </a>
                                </li>
                                */?>
                                <li>
                                    <a href="/admin/administrators.php?action=edit&id_administrator=<?php echo $sessionAdministrator->getId_administrator() ?>">
                                        <i class="fa fa-user"></i>
                                        Profile
                                    </a>
                                </li>
                                <?php /*?>
                                <li>
                                    <a href="#help">
                                        <i class="fa fa-info"></i>
                                        Help
                                    </a>
                                </li>
                                */?>
                                <li class="last">
                                    <a href="/admin/logout.php">
                                        <i class="fa fa-lock"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php /*?>
                        <li class="chat-toggle-wrapper">
                            <a href="#" data-toggle="chatbar" class="toggle_chat">
                                <i class="fa fa-comments"></i>
                                <span class="badge badge-warning">9</span>
                                <i class="fa fa-times"></i>
                            </a>
                        </li>
                        */?>
                    </ul>			
                </div>		
            </div>

        </div>
        <!-- END TOPBAR -->
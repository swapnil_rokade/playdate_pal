<?php 
$LIB_VERSION="1.0";
?>
        <!-- CORE JS FRAMEWORK - START --> 
        <script src="assets/js/jquery-1.11.2.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script> 
        <script src="assets/js/jquery.easing.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script> 
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script> 
        <script src="assets/plugins/pace/pace.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>  
        <script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script> 
        <script src="assets/plugins/viewport/viewportchecker.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>  
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <script src="assets/plugins/rickshaw-chart/vendor/d3.v3.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/plugins/jquery-ui/smoothness/jquery-ui.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/plugins/rickshaw-chart/js/Rickshaw.All.js?v=<?php echo $LIB_VERSION ?>"></script>
        <script src="assets/plugins/sparkline-chart/jquery.sparkline.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/plugins/easypiechart/jquery.easypiechart.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/plugins/morris-chart/js/raphael-min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/plugins/morris-chart/js/morris.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <?php /* 
        <script src="assets/plugins/jvectormap/jquery-jvectormap-2.0.1.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        */?>
        <script src="assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
        <script src="assets/plugins/icheck/icheck.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script> 
        <?php /* <script src="assets/js/crm-dashboard.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script> */ ?>

        <!-- DATA TABLES -->
        <script src="assets/plugins/datatables/js/jquery.dataTables.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/plugins/datatables/js/jquery.dataTables.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
		<script src="assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
		<script src="assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
		<script src="assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
		<!-- /DATA TABLES -->
        
        
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
        
        <!-- CORE TEMPLATE JS - START --> 
        <script src="assets/js/scripts.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        <!-- Sidebar Graph - START --> 
        <script src="assets/plugins/sparkline-chart/jquery.sparkline.min.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <script src="assets/js/chart-sparkline.js?v=<?php echo $LIB_VERSION ?>" type="text/javascript"></script>
        <!-- Sidebar Graph - END --> 

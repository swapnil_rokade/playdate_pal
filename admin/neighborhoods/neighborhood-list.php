<?php 
/**
 * PLAYDATE ADMIN - PART - NEIGHBORHOODS
 * 
 */
if(!isset($neighborhoods)) {$neighborhoods=array();}
if((!isset($city)) || ($city==null)) {
    header("Location: /admin/neighborhoods.php");
}

if(!isset($newNeighborhood)) {
    $newNeighborhood = new Neighborhood();
}

?>
<!-- START NEIGHBORHOODS LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Cities</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/neighborhoods.php">Neighborhoods</a></li>
						<li><a href="/admin/neighborhoods.php?action=city&id_city=<?php echo $city->getId_city() ?>"><?php echo $city->getName() ?></a></li>
						<li class="active"><strong>All Neighborhoods</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<!-- cities -->
		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Neighborhoods in <strong><?php echo $city->getName() ?></strong></h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->
							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($neighborhoods as $auxNeigh) {
									?>
									<tr>
										<td><?php echo $auxNeigh->getId_neighborhood() ?></td>
										<td><?php echo $auxNeigh->getName() ?></td>
										<td><a href="/admin/neighborhoods.php?action=neighborhood&id_neighborhood=<?php echo $auxNeigh->getId_neighborhood() ?>" class="bt-edit"
											title="Edit"><img src="assets/images/bt-edit.png" /></a>
											<a
											title="Delete" href="/admin/neighborhoods.php?action=deleteneighborhood&id_neighborhood=<?php echo $auxNeigh->getId_neighborhood() ?>" onclick="return confirm('Are you sure you want to remove this neighborhood and all his information? This operation cannot be undone.');" class="bt-delete"><img
												src="assets/images/bt-delete.png" /></a>
											</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->

						</div>
					</div>
				</div>
			</section>
			<!--  new city -->
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Create new neighborhood in city: <strong><?php echo $city->getName() ?></strong></h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/neighborhoods.php" method="post">
							<input type="hidden" name="action" value="createneighborhood" />
							<input type="hidden" name="id_city" value="<?php echo $city->getId_city()?>" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
									<label class="form-label" for="field-1">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $newNeighborhood->getName()?>" class="form-control" id="field-1" name="name"  placeholder="Name" />
									</div>
								</div>
							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="button" class="btn" onclick="top.location='/admin/neighborhoods.php'">Cancel</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
			
		</div>






	</section>
</section>
<!-- END NEIGHBORHOODS LIST CONTENT -->
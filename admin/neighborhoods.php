<?php 
/**
 * PLAYDATE - ADMIN - NEIGHBORHOODS
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "neighborhoods";
$menu_option = "neighborhoods-all";
$main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
    
    case "createcity": {
        $menu_section = "neighborhoods";
        $menu_option = "neighborhoods-all";
        $main_section = "neighborhoods/city-list.php"; //Section to load as main content
        
        //delete City
        $newCity = new City();
        $newCity->readFromRow($_REQUEST);
        $newCity = NeighborhoodDAO::createCity($newCity);
        if(($newCity!=null) && ($newCity->getId_city()>0)) {
            //ok with message
            $successMessage = "City has been created";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/city-list.php"; //Section to load as main content
            
            $cities = NeighborhoodDAO::getCityList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error creating city. Please, try again";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/city-list.php"; //Section to load as main content
            
            $cities = NeighborhoodDAO::getCityList();
        }
        break;
    }
    
    case "deletecity": {
        $menu_section = "neighborhoods";
        $menu_option = "neighborhoods-all";
        $main_section = "neighborhoods/city-list.php"; //Section to load as main content
        
        //delete City
        $delCity = new City();
        $delCity->readFromRow($_REQUEST);
        $deleted = NeighborhoodDAO::deleteCity($delCity);
        if($deleted) {
            //ok with message
            $successMessage = "City has been deleted";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/city-list.php"; //Section to load as main content
            
            $cities = NeighborhoodDAO::getCityList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting city. Please, try again";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/city-list.php"; //Section to load as main content
            
            $cities = NeighborhoodDAO::getCityList();
        }
        
        break;
    }
    
    case "createneighborhood": {
        $menu_section = "neighborhoods";
        $menu_option = "neighborhoods-all";
        $main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content
        
        //create
        $newNeighborhood = new Neighborhood();
        $newNeighborhood->readFromRow($_REQUEST);
        $newNeighborhood = NeighborhoodDAO::createNeighborhood($newNeighborhood);
        $city = NeighborhoodDAO::getCity($newNeighborhood->getId_city());
        if(($newNeighborhood!=null) && ($newNeighborhood->getId_neighborhood()>0)) {
            //ok with message
            $successMessage = "Neighborhood has been created";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content
            
            $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByCity($newNeighborhood->getId_city());
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error creating neighborhood. Please, try again";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content
            
            $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByCity($newNeighborhood->getId_city());
        }
        break;
    }
    case "deleteneighborhood": {
        $menu_section = "neighborhoods";
        $menu_option = "neighborhoods-all";
        $main_section = "neighborhoods/neighborhoods-list.php"; //Section to load as main content
        
        //delete Neighborhood
        $delNeighborhood = new Neighborhood();
        $delNeighborhood->readFromRow($_REQUEST);
        $delNeighborhood = NeighborhoodDAO::getNeighborhood($delNeighborhood->getId_neighborhood());
        $city = NeighborhoodDAO::getCity($delNeighborhood->getId_city());
        $deleted = NeighborhoodDAO::deleteNeighborhood($delNeighborhood);
        if($deleted) {
            //ok with message
            $successMessage = "Neighborhood has been deleted";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content
            
            $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByCity($delNeighborhood->getId_city());
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting neighborhood. Please, try again";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content
            
            $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByCity($delNeighborhood->getId_city());
        }
        
        break;
    }
    
    case "createzipcode": {
        $menu_section = "neighborhoods";
        $menu_option = "neighborhoods-all";
        $main_section = "neighborhoods/zipcode-list.php"; //Section to load as main content
        
        if(isset($_REQUEST["multizipcode"])) {
            $multicodes = explode(",", $_REQUEST["multizipcode"]);
            if(count($multicodes)>0) {
                foreach($multicodes as $auxCode) {
                    $newZipcode = new Zipcode();
                    $newZipcode->readFromRow($_REQUEST);
                    $newZipcode->setZipcode(trim($auxCode));
                    
                    $newZipcode = NeighborhoodDAO::createZipcode($newZipcode);
                }
                
                $successMessage = "Zipcodes has been created";
                
                $menu_section = "neighborhoods";
                $menu_option = "neighborhoods-all";
                $main_section = "neighborhoods/zipcode-list.php"; //Section to load as main content
                
                $neighborhood = NeighborhoodDAO::getNeighborhood($newZipcode->getId_neighborhood());
                $city = NeighborhoodDAO::getCity($neighborhood ->getId_city());
                $zipcodes = NeighborhoodDAO::getZipcodesListByNeighborhood($newZipcode->getId_neighborhood());
            }
            
        } 
        break;
    }
    case "deletezipcode": {
        $menu_section = "neighborhoods";
        $menu_option = "neighborhoods-all";
        $main_section = "neighborhoods/zipcode-list.php"; //Section to load as main content
        
        //delete Zipcode
        $delZipcode = new Zipcode();
        $delZipcode->readFromRow($_REQUEST);
        $delZipcode = NeighborhoodDAO::getZipcode($delZipcode->getId_zipcode());
        $neighborhood = NeighborhoodDAO::getNeighborhood($delZipcode->getId_neighborhood());
        $city = NeighborhoodDAO::getCity($neighborhood->getId_city());
        
        $deleted = NeighborhoodDAO::deleteZipcode($delZipcode);
        if($deleted) {
            //ok with message
            $successMessage = "Zipcode has been deleted";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/zipcode-list.php"; //Section to load as main content
            
            $zipcodes = NeighborhoodDAO::getZipcodesListByNeighborhood($delZipcode->getId_neighborhood());
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting zipcode. Please, try again";
            
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/zipcode-list.php"; //Section to load as main content
            
            $zipcodes = NeighborhoodDAO::getZipcodesListByNeighborhood($delZipcode->getId_neighborhood());
        }
        
        break;
    }
    
    case "city": {
        $city = new City();
        $city->readFromRow($_REQUEST);
        $city = NeighborhoodDAO::getCity($city->getId_city());
        if(($city!=null) && ($city->getId_city()>0)) {
            $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByCity($city->getId_city());
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content
        } else {
            $city = null;
            $cities = NeighborhoodDAO::getCityList();
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/city-list.php"; //Section to load as main content
        }
        
        break;
    }
    
    case "neighborhood": {
        $neighborhood = new Neighborhood();
        $neighborhood->readFromRow($_REQUEST);
        $neighborhood = NeighborhoodDAO::getNeighborhood($neighborhood->getId_neighborhood());
        $city = NeighborhoodDAO::getCity($neighborhood->getId_neighborhood());
        if(($neighborhood!=null) && ($neighborhood->getId_neighborhood()>0)) {
            $zipcodes = NeighborhoodDAO::getZipcodesListByNeighborhood($neighborhood->getId_neighborhood());
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/zipcode-list.php"; //Section to load as main content
        } else {
            $neighborhoods = NeighborhoodDAO::getNeighborhoodsListByCity($city->getId_city());
            $menu_section = "neighborhoods";
            $menu_option = "neighborhoods-all";
            $main_section = "neighborhoods/neighborhood-list.php"; //Section to load as main content
        }
        
        break;
    }
    
    
    default: {
        
        $menu_section = "neighborhoods";
        $menu_option = "neighborhoods-all";
        $main_section = "neighborhoods/city-list.php"; //Section to load as main content
        
        $cities = NeighborhoodDAO::getCityList();
        
        break;
    }
}



include("template.php");
?>
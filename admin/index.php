<?php 
/**
 * PLAYDATE - ADMIN 
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "home";

$main_section = "home/home-content.php"; //Section to load as main content
include("template.php");
?>

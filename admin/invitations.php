<?php 
/**
 * PLAYDATE - ADMIN - INVITATIONS SECTION 
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "invitations";
$menu_option = "invitations-all";
$main_section = "invitations/invitation-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {

    case "new": {
        $menu_section = "invitations";
        $menu_option = "invitations-new";
        $main_section = "invitations/invitation-new.php"; //Section to load as main content
        
        break;
    }
    
    case "create": {
        //Create new Invitation
        $newInvitation = new Invitation();
        $newInvitation->readFromRow($_REQUEST);
        $newInvitation = InvitationDAO::createInvitationForEmail($newInvitation->getEmail());
        if(($newInvitation!=null) && ($newInvitation->getId_invitation()>0)) {
            //ok
            $successMessage = "New invitation has been created";
            
            //SEND MAIL WITH INVITATION CODE
            MailUtils::sendPlaydateInvitationCode($newInvitation->getEmail(), "PAL by Playdate Project", $newInvitation->getCode());
            
            $menu_section = "invitations";
            $main_section = "invitations/invitation-edit.php"; //Section to load as main content
            
            $editInvitation = $newInvitation;
            
            
        } else {
            //error - go to new, with error message
            $errorMessage = "Error creating invitation. Please, review info and try again";
            
            $menu_section = "invitations";
            $menu_option = "invitations-new";
            $main_section = "invitations/invitation-new.php"; //Section to load as main content
        }
        break;
    }
    
    case "delete": {
        $menu_section = "invitations";
        $menu_option = "invitations-all";
        $main_section = "invitations/invitation-list.php"; //Section to load as main content
        
        //delete Parent
        $delInvitation = new Invitation();
        $delInvitation->readFromRow($_REQUEST);
        $deleted = InvitationDAO::deleteInvitation($delInvitation->getId_invitation());
        if($deleted) {
            //ok with message
            $successMessage = "Invitation has been deleted";
            
            $menu_section = "invitations";
            $menu_option = "invitations-all";
            $main_section = "invitations/invitation-list.php"; //Section to load as main content
            
            $invitations = InvitationDAO::getInvitationsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting invitation. Please, try again";
            
            $menu_section = "invitations";
            $menu_option = "invitations-all";
            $main_section = "invitations/invitation-list.php"; //Section to load as main content
            
            $invitations = InvitationDAO::getInvitationsList();
        }
        
        break;
    }
    
    case "edit": {
        //view invitation
        $editInvitation = new Invitation();
        $editInvitation->readFromRow($_REQUEST);
        
        if($editInvitation->getId_invitation()>0) {
            $editInvitation= InvitationDAO::getInvitation($editInvitation->getId_invitation());
        }
        
        if(($editInvitation!=null) && ($editInvitation->getId_invitation()>0)) {
            $menu_section = "invitations";
            $main_section = "invitations/invitation-edit.php"; //Section to load as main content
        } else {
            //invitation not found
            header("Location: /admin/invitations.php");
        }
        break;
    }
    
    case "update": {
        $updated = false;
        
        $menu_section = "invitations";
        $menu_option = "invitations-all";
        $main_section = "invitations/invitation-list.php"; //Section to load as main content
        
        //update invitation
        $updInvitation = new Invitation();
        $updInvitation->readFromRow($_REQUEST);
        
        if($updInvitation->getId_invitation()>0) {
            $updInvitation = InvitationDAO::getInvitation($updInvitation->getId_invitation()); //read all invitation info
            
            $updInvitation->readFromRow($_REQUEST); //change only this form info
            
            $updInvitation = InvitationDAO::updateInvitation($updInvitation);
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Invitation has been updated";
            
            $menu_section = "invitations";
            $menu_option = "invitations-all";
            $main_section = "invitations/invitation-list.php"; //Section to load as main content
            
            $invitations = InvitationDAO::getInvitationsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating invitation. Please, try again";
            
            $menu_section = "invitations";
            $menu_option = "invitations-all";
            $main_section = "invitations/invitation-list.php"; //Section to load as main content
            
            $invitations = InvitationDAO::getInvitationList();
        }
        
        break;
    }
    
    default: {
        
        $menu_section = "invitations";
        $menu_option = "invitations-all";
        $main_section = "invitations/invitation-list.php"; //Section to load as main content
        
        $invitations = InvitationDAO::getInvitationsList();
        
        break;
    }
}

include("template.php");
?>
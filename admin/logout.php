<?php 
/**
 * PLAYATE ADMIN - Logout Page
 */
include_once("adm-common.php");

session_destroy();

header("Location: /admin/login.php");
?>
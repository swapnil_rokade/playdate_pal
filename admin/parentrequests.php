<?php 
/**
 * PLAYDATE - ADMIN - PARENT REQUESTS
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "parentrequests";
$menu_option = "parentrequests-all";
$main_section = "parentrequests/parentrequests-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    
    case "delete": {
        $menu_section = "parentrequests";
        $menu_option = "parentrequests-all";
        $main_section = "parentrequests/parentrequests-list.php"; //Section to load as main content
        
        //delete Request
        $delPlaydateRequest = new PlaydateRequest();
        $delPlaydateRequest->readFromRow($_REQUEST);
        $deleted = PlaydateRequestDAO::deletePlaydateRequest($delPlaydateRequest);
        if($deleted) {
            //ok with message
            $successMessage = "Parent request has been deleted";

            $parentrequests = PlaydateRequestDAO::getPlaydateRequestsList(1, 1000, null, null);
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting parent request. Please, try again";
            
            $menu_section = "parentrequests";
            $menu_option = "parentrequests-all";
            $main_section = "parentrequests/parentrequests-list.php"; //Section to load as main content
            
            $parentrequests = PlaydateRequestDAO::getPlaydateRequestsList(1, 1000, null, null);
        }
        
        break;
    }
    
    case "view": {
        //view parent request
        $editPlaydateRequest = new PlaydateRequest();
        $editPlaydateRequest->readFromRow($_REQUEST);
        
        if($editPlaydateRequest->getId_request()>0) {
            $editPlaydateRequest= PlaydateRequestDAO::getPlaydateRequest($editPlaydateRequest->getId_request());
        }
        
        if(($editPlaydateRequest!=null) && ($editPlaydateRequest->getId_request()>0)) {
            $menu_section = "parentrequests";
            $main_section = "parentrequests/parentrequests-view.php"; //Section to load as main content
        } else {
            //parent request not found
            header("Location: /admin/parentrequests.php");
        }
        break;
    }
    
    
    default: {
        
        $menu_section = "parentrequests";
        $menu_option = "parentrequests-all";
        $main_section = "parentrequests/parentrequests-list.php"; //Section to load as main content
        
        $parentrequests = PlaydateRequestDAO::getPlaydateRequestsList(1, 1000, null, null);
        
        break;
    }
}



include("template.php");
?>
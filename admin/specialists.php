<?php 
/**
 * PLAYDATE - ADMIN - SPECIALISTS SECTION 
 */
require_once('../vendor/autoload.php');

use Stripe\Transfer;

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "specialists";
$menu_option = "specialists-all";
$main_section = "specialists/specialist-list.php"; //Section to load as main content

$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {

    case "pay-specialist": {
        $updated = false;
        
        $menu_section = "specialists";
        $menu_option = "specialists-all";
        $main_section = "specialists/specialist-list.php"; //Section to load as main content
        
        //pay specialist
        $updSpec = new Specialist();
        $updSpec ->readFromRow($_REQUEST);
        
        if($updSpec->getId_specialist()>0) {
            $updSpec = SpecialistDAO::getSpecialist($updSpec->getId_specialist()); //read all info
            
            if(($updSpec->getPay_stripeid()==null)||($updSpec->getPay_stripeid()=="")) {
                $updated = false;
                $errorMessage = "This specialist has not connected payment account";
            } else {
            
                //pay amount to specialist
                //Generate specialist payment here
                $amountToPay = 0; //in $
                $comment = "Specialist payment by Admin";
                if(isset($_REQUEST["amount"]) && is_numeric($_REQUEST["amount"])) {
                    $amountToPay = $_REQUEST["amount"];
                }
                if(isset($_REQUEST["description"]) && ($_REQUEST["description"]!=null)) {
                    $comment = $_REQUEST["description"];
                }
                
                if($amountToPay>0) {
                    
                    try {
                        /*
                        //Payment in Stripe
                        \Stripe\Stripe::setApiKey($STRIPE_CONNECT_SECRET);
                        
                        \Stripe\Payout::create([
                            "amount" => $amountToPay,
                            "currency" => "usd",
                        ], ["stripe_account" => $updSpec->getPay_stripeid()]);
                        */
                    
                        //Payment registered
                        $newSpecPayment = new Payment();
                        $newSpecPayment->setId_specialist($updSpec->getId_specialist());
                        $newSpecPayment->setId_playdate(null);
                        $newSpecPayment->setName($comment);
                        $newSpecPayment->setAmount_playdate($amountToPay*100);
                        $newSpecPayment->setAmount_addons(0);
                        $newSpecPayment->setAmount_paid(0);
                        $newSpecPayment->setStatus(Payment::$STATUS_PENDING);
                        
                        PaymentDAO::createSpecialistPayment($newSpecPayment);
                        $successMessage = "Pending payment to specialist has been created";
                        
                        $updated = true;
                    } catch(Exception $e) {
                        $updated = false;
                        $errorMessage = "Error processing payment: ".$e->getMessage();
                    }
                } else {
                    $updated = false;
                    $errorMessage = "Amount must be more than $0";
                }
            }
            
        }
        if($updated) {
            //ok with message
            if((!isset($successMessage)) || ($successMessage==null) || ($successMessage=="")) {
                $successMessage = "Payment to specialist has been created.";
            }
            
            $menu_section = "specialists";
            $main_section = "specialists/specialist-edit.php"; //Section to load as main content
            
            $editSpecialist = SpecialistDAO::getSpecialist($updSpec->getId_specialist());
            
            
            
        } else {
            //error - go to list, with error message
            if(!isset($errorMessage) || ($errorMessage==null)) {
                $errorMessage = "Error executing payment. Please, try again";
            }
            
            $menu_section = "specialists";
            $main_section = "specialists/specialist-edit.php"; //Section to load as main content
            
            $editSpecialist = SpecialistDAO::getSpecialist($updSpec->getId_specialist());
        }
        
        break;
    }
    
    case "delete-pay": {
        $payment = new Payment();
        $payment->readFromRow($_REQUEST);
        if($payment->getId_payment()!=null) {
            $payment = PaymentDAO::getPayment($payment->getId_payment());
            if($payment!=null) {
                //Eliminamos el payment
                PaymentDAO::deletePayment($payment->getId_payment());
                //ok with message
                $successMessage = "Payment order has been deleted";
                if($payment->getId_parent()!=null) {
                    $successMessage = "Charge order has been deleted";
                } else if($payment->getId_specialist()!=null) {
                    $successMessage = "Payment order has been deleted";
                }
                
            }
            
        }
        
        $editSpecialist = new Specialist();
        $editSpecialist->readFromRow($_REQUEST);
        
        if($editSpecialist->getId_specialist()>0) {
            $editSpecialist = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
        }
        
        $menu_section = "specialists";
        $main_section = "specialists/specialist-edit.php"; //Section to load as main content
        
        break;
    }
    
    case "execute-pay": {
        $payment = new Payment();
        $payment->readFromRow($_REQUEST);
        if($payment->getId_payment()!=null) {
            $payment = PaymentDAO::getPayment($payment->getId_payment());
            if($payment!=null) {
                //Execute el payment
                $editSpecialist = new Specialist();
                $editSpecialist->readFromRow($_REQUEST);
                
                if($editSpecialist->getId_specialist()>0) {
                    $editSpecialist = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
                }
                
                if($editSpecialist->getId_specialist()>0) {
                    
                    if(($editSpecialist->getPay_stripeid()==null)||($editSpecialist->getPay_stripeid()=="")) {
                        $updated = false;
                        $errorMessage = "This specialist has not connected payment account";
                    } else {
                        
                        //pay amount to specialist
                        //Generate specialist payment here
                        $amountToPay = $payment->getAmount_playdate()+$payment->getAmount_addons(); //in $
                        
                        if($amountToPay>0) {
                            try {
                                //Payment in Stripe
                                \Stripe\Stripe::setApiKey($STRIPE_CONNECT_SECRET);
                                $transferDescription = "Payment to ".$editSpecialist->getFullName()." - ".$payment->getName();
                                // Create a Transfer to a connected account
                                $orderId = "PAYMENT".$payment->getId_payment();
                                $transfer = \Stripe\Transfer::create([
                                    "amount" => $amountToPay,
                                    "currency" => "usd",
                                    "description" => $transferDescription,
                                    "destination" => $editSpecialist->getPay_stripeid(),
                                    "transfer_group" => $orderId,
                                ]);
                                
                                PaymentDAO::updatePaymentStatus($payment->getId_payment(), $amountToPay, Payment::$STATUS_COMPLETED);
                                
                                $successMessage = "Payment to specialist has been executed";
                                
                                $updated = true;
                            } catch(Exception $e) {
                                $updated = false;
                                $errorMessage = "Error processing payment: ".$e->getMessage();
                            }
                        } else {
                            $updated = false;
                            $errorMessage = "Amount must be more than $0";
                        }
                    }
                }
                if($updated) {
                    //ok with message
                    if((!isset($successMessage)) || ($successMessage==null) || ($successMessage=="")) {
                        $successMessage = "Payment to specialist has been executed.";
                    }
                    
                    $menu_section = "specialists";
                    $main_section = "specialists/specialist-edit.php"; //Section to load as main content
                    
                    $editSpecialist = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
                    
                    
                    
                } else {
                    //error - go to list, with error message
                    if(!isset($errorMessage) || ($errorMessage==null)) {
                        $errorMessage = "Error executing payment. Please, try again";
                    }
                    
                    $menu_section = "specialists";
                    $main_section = "specialists/specialist-edit.php"; //Section to load as main content
                    
                    $editSpecialist = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
                }
                
                
            }
            
        }
        
        $menu_section = "specialists";
        $main_section = "specialists/specialist-edit.php"; //Section to load as main content
        
        break;
    }
    
    case "new": {
        $menu_section = "specialists";
        $menu_option = "specialists-new";
        $main_section = "specialists/specialist-new.php"; //Section to load as main content
        
        break;
    }
    
    case "create": {
        /*
        $menu_section = "specialists";
        $menu_option = "specialists-all";
        $main_section = "specialists/specialists-list.php"; //Section to load as main content
        */
        
        //Create new Specialist
        $newSpecialist = new Specialist();
        $newSpecialist->readFromRow($_REQUEST);
        
        
        $newSpecialist = SpecialistDAO::createSpecialist($newSpecialist);
        if(($newSpecialist!=null) && ($newSpecialist->getId_specialist()>0)) {
            
            //Welcome mail to specialist
            MailUtils::sendSpecialistWelcome($newSpecialist->getEmail(), $newSpecialist->getName());
            
            
            //ok
            $successMessage = "New specialist has been created. You can add more info";
            /*
            $menu_section = "specialists";
            $menu_option = "specialists-all";
            $main_section = "specialists/specialist-list.php"; //Section to load as main content
            
            $specialists = SpecialistDAO::getSpecialistsList();
            */
            
            $menu_section = "specialists";
            $main_section = "specialists/specialist-edit.php"; //Section to load as main content
            
            $editSpecialist = $newSpecialist;
            
            
        } else {
            //error - go to new, with error message
            $errorMessage = "Error creating specialist. Please, review info and try again";
            
            $menu_section = "specialists";
            $menu_option = "specialists-new";
            $main_section = "specialists/specialist-new.php"; //Section to load as main content
        }
        break;
    }
    
    case "delete": {
        $menu_section = "specialists";
        $menu_option = "specialists-all";
        $main_section = "specialists/specialist-list.php"; //Section to load as main content
        
        //delete Parent
        $delSpecialist = new Specialist();
        $delSpecialist ->readFromRow($_REQUEST);
        $deleted = SpecialistDAO::deleteSpecialist($delSpecialist);
        if($deleted) {
            //ok with message
            $successMessage = "Specialist has been deleted";
            
            $menu_section = "specialists";
            $menu_option = "specialists-all";
            $main_section = "specialists/specialist-list.php"; //Section to load as main content
            
            $specialists = SpecialistDAO::getSpecialistsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting specialist. Please, try again";
            
            $menu_section = "specialists";
            $menu_option = "specialists-all";
            $main_section = "specialists/specialist-list.php"; //Section to load as main content
            
            $specialists = SpecialistDAO::getSpecialistsList();
        }
        
        break;
    }
    
    case "edit": {
        //view specialist
        $editSpecialist = new Specialist();
        $editSpecialist->readFromRow($_REQUEST);
        
        if($editSpecialist->getId_specialist()>0) {
            
            
            $editSpecialist= SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
        }
        
        if(($editSpecialist!=null) && ($editSpecialist->getId_specialist()>0)) {
            $menu_section = "specialists";
            $main_section = "specialists/specialist-edit.php"; //Section to load as main content
        } else {
            //specialist not found
            header("Location: /admin/specialists.php");
        }
        break;
    }
    
    case "update": {
        $updated = false;
        
        $menu_section = "specialists";
        $menu_option = "specialists-all";
        $main_section = "specialists/specialist-list.php"; //Section to load as main content
        
        //update administrator
        $updSpecialist = new Specialist();
        $updSpecialist->readFromRow($_REQUEST);
        
        if($updSpecialist->getId_specialist()>0) {
            $updSpecialist = SpecialistDAO::getSpecialist($updSpecialist->getId_specialist()); //read all specialist info
            
            $picture = $updSpecialist->getPicture();
            
            $updSpecialist->readFromRow($_REQUEST); //change only this form info
            
            //TODO - SUBIR FOTO SI LLEGA FICHERO
            
            //si hay fichero, subimos
            if(count($_FILES)>0) {
                //echo(":::::::::FILES::::::::");die();
                //$currentDir = getcwd();
                $currentDir = dirname( dirname(__FILE__) );
                $uploadDirectory = "/uploads/";
                
                $errors = []; // Store all foreseen and unforseen errors here
                
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                
                $fileName = $_FILES['formfile']['name'];
                $fileSize = $_FILES['formfile']['size'];
                $fileTmpName  = $_FILES['formfile']['tmp_name'];
                $fileType = $_FILES['formfile']['type'];
                
                $tmpArr = explode('.',$fileName);
                $tmpExt = end($tmpArr);
                $fileExtension = strtolower($tmpExt);
                
                $uploadPath = $currentDir . $uploadDirectory . basename($fileName);
                
                $i=0;
                while (file_exists($uploadPath)) {
                    $i++;
                    $uploadPath = $currentDir . $uploadDirectory .$i."_".basename($fileName);
                }
                
                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }
                
                if ($fileSize > 6000000) {
                    $errors[] = "This file is more than 6MB. Sorry, it has to be less than or equal to 2MB";
                }
                
                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                    
                    if ($didUpload) {
                        $updSpecialist->setPicture($uploadDirectory.basename($fileName)); //Actualizamos la ruta
                    } else {
                        echo "<!-- An error occurred somewhere. Try again or contact the admin -->";
                    }
                } else {
                    foreach ($errors as $error) {
                        //echo $error . "These are the errors" . "\n";
                    }
                }
            } //hay fichero
            
            
            $updSpecialist = SpecialistDAO::updateSpecialist($updSpecialist);
            
            if(isset($_REQUEST["password"]) && ($_REQUEST["password"]!="")) {
                SpecialistDAO::updateSpecialistPassword($updSpecialist->getId_specialist(), $_REQUEST["password"]);
            }
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Specialist has been updated";
            
            $menu_section = "specialists";
            $menu_option = "specialists-all";
            $main_section = "specialists/specialist-list.php"; //Section to load as main content
            
            $specialists = SpecialistDAO::getSpecialistsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating specialist. Please, try again";
            
            $menu_section = "specialists";
            $menu_option = "specialists-all";
            $main_section = "specialists/specialist-list.php"; //Section to load as main content
            
            $specialists = SpecialistDAO::getSpecialistsList();
        }
        
        break;
    }
    
    default: {
        
        $menu_section = "specialists";
        $menu_option = "specialists-all";
        $main_section = "specialists/specialist-list.php"; //Section to load as main content
        
        $specialists = SpecialistDAO::getSpecialistsList();
        
        break;
    }
}

include("template.php");
?>
<?php
/**
 * PLAYDATE ADMIN - Common things
 */

$serverName = null;
if(isset($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME']!=null)) {
    $serverName = $_SERVER['SERVER_NAME'];
}


if($serverName=="playdate.airtouchmedia.com") { //TEST
    $DOCROOT = "/var/www/vhosts/playdate.airtouchmedia.com/httpdocs/";
} else if($serverName=="www.playdatepal.com") { //PROD
    //$DOCROOT = "/var/www/vhosts/playdate.airtouchmedia.com/httpdocs/";
    $DOCROOT = $_SERVER['DOCUMENT_ROOT'];
} else if($serverName=="playdatepal.com") { //PROD
    //$DOCROOT = "/var/www/vhosts/playdate.airtouchmedia.com/httpdocs/";
    $DOCROOT = $_SERVER['DOCUMENT_ROOT'];
} else if($serverName=="playdate.local") { //LOCAL
    $DOCROOT = "/PROYECTOS/PLAYDATES/workspace/Playdate/www";
} 
else if($serverName == "ht.3dchow.com"){
    $DOCROOT = $_SERVER['DOCUMENT_ROOT'];
}
else { //LOCAL
    //$DOCROOT = "/PROYECTOS/PLAYDATES/workspace/Playdate/www";
    $DOCROOT = $_SERVER['DOCUMENT_ROOT'];
}


require $DOCROOT.'/mail/PHPMailer/Exception.php';
require $DOCROOT.'/mail/PHPMailer/PHPMailer.php';
require $DOCROOT.'/mail/PHPMailer/SMTP.php';


//Stripe connect
$STRIPE_AUTHORIZE_URI = 'https://connect.stripe.com/express/oauth/authorize';
$STRIPE_TOKEN_URI = 'https://connect.stripe.com/oauth/token';
$PLAYDATE_STRIPE_URI = 'https://www.playdatepal.com/stripe-connect.php';

/* STRIPE DATOS LIVE BUY PARENTS */
$STRIPE_SECRET="sk_live_TtpUhAcQjr3wQdICKW5N352X";
$STRIPE_PK="pk_live_yBW7nuwSbCpMzGVTIJAdwxE3";
$STRIPE_CLIENTID="ca_DsJrSoJiMJwSHjm4JTjdUgykqMYoF3ar"; //Stripe Connect


/* STRIPE DATA TEST PLAYDATES */
/*
 $STRIPE_CONNECT_SECRET="sk_test_NFJJMJlmy2x70nlKvmelIfaJ";
 $STRIPE_CONNECT_CLIENTID="ca_DsJr640bivSXTFjzAT8VpWcJQGSBDkkk"; //Stripe Connect
 */

/* STRIPE DATA LIVE */
$STRIPE_CONNECT_SECRET="sk_live_TtpUhAcQjr3wQdICKW5N352X";
$STRIPE_CONNECT_CLIENTID="ca_DsJrSoJiMJwSHjm4JTjdUgykqMYoF3ar"; //Stripe Connect


define("ADM_SESSION_ID", "admin_id");
define("ADM_SESSION_NAME", "admin_name");
define("ADM_SESSION_PICTURE", "admin_picture");

if(session_id()=="") {
	session_start();
}

$isAdmin = (isset($_SESSION[ADM_SESSION_ID]) && ($_SESSION[ADM_SESSION_ID] != ""));
$isConnected = $isAdmin; //add here any other profiles allowed to access
$backUrl = null;  //address to go back after login

$menu_section = "home";
$menu_option = null;
?>

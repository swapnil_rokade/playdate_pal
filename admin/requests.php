<?php 
/**
 * PLAYDATE - REQUEST INVITATIONS 
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "parents";
$menu_option = "parents-all";
$main_section = "parents/parent-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    case "delete": {
        $menu_section = "requests";
        $menu_option = "requests-all";
        $main_section = "requests/request-list.php"; //Section to load as main content
        
        //delete request invitation
        $elem = new InvitationRequest();
        $elem->readFromRow($_REQUEST);
        if($elem->getId_request_invitation()!=null) {
            InvitationDAO::deleteRequestInvitation($elem->getId_request_invitation());
            $successMessage = "Request for invitation has been deleted.";
        } else {
            $errorMessage = "Request for invitation couldn't be deleted.";
        }
        $requests = InvitationDAO::getRequestInvitationsList(1,100,null,null);
        break;
    }
    
    case "accept": {
        $menu_section = "requests";
        $menu_option = "requests-all";
        $main_section = "requests/request-list.php"; //Section to load as main content
        
        //delete request invitation
        $elem = new InvitationRequest();
        $elem->readFromRow($_REQUEST);
        if(($elem->getId_request_invitation()!=null) && ($elem->getId_request_invitation()>0)) {
            InvitationDAO::updateRequestInvitationStatus($elem->getId_request_invitation(), Invitation::$STATUS_ACCEPTED);
            
            $reqInv = InvitationDAO::getInvitationRequest($elem->getId_request_invitation());
            
            //Generate access code
            $newInvitation = new Invitation();
            $newInvitation->setEmail($reqInv->getEmail());
            $newInvitation = InvitationDAO::createInvitationForEmail($newInvitation->getEmail());
            
            //Send Mail
            MailUtils::sendInvitationAcceptedCode($newInvitation->getEmail(), $newInvitation->getCode());
            
            $successMessage = "Request for invitation has been accepted. Generated code: ".$newInvitation->getCode();
        } else {
            $errorMessage = "Request for invitation couldn't be accepted.";
        }
        $requests = InvitationDAO::getRequestInvitationsList(1,100,null,null);
        break;
    }
    
    case "decline": {
        $menu_section = "requests";
        $menu_option = "requests-all";
        $main_section = "requests/request-list.php"; //Section to load as main content
        
        //delete request invitation
        $elem = new InvitationRequest();
        $elem->readFromRow($_REQUEST);
        if(($elem->getId_request_invitation()!=null) && ($elem->getId_request_invitation()>0)) {
            InvitationDAO::updateRequestInvitationStatus($elem->getId_request_invitation(), Invitation::$STATUS_DECLINED);
            $successMessage = "Request for invitation has been declined.";
        } else {
            $errorMessage = "Request for invitation couldn't be declined.";
        }
        $requests = InvitationDAO::getRequestInvitationsList(1,100,null,null);
        break;
    }
    
    
    default: {
        
        $menu_section = "requests";
        $menu_option = "requests-all";
        $main_section = "requests/request-list.php"; //Section to load as main content
        
        $requests = InvitationDAO::getRequestInvitationsList(1,100,null,null);
        
        break;
    }
}



include("template.php");
?>
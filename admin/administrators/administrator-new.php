<?php
/**
 * PLAYDATE ADMIN - ADMINISTRATOR NEW FORM 
 */

if(!isset($newAdmin)) {
    $newAdmin = new Administrator();
}

?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Add Administrator</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/administrators.php">Administrators</a></li>
						<li class="active"><strong>Add Administrator</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/administrators.php" method="post">
							<input type="hidden" name="action" value="create" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>
                	
							

								<div class="form-group">
									<label class="form-label" for="field-1">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" id="field-1" name="name" placeholder="Name" />
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="field-5">Last Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" name="lastname" placeholder="Last Name" />
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Email</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" name="email" placeholder="Email" />
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Username</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" name="username" placeholder="Username" />
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Password</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="password" value="" class="form-control" name="password" placeholder="Password">
									</div>
								</div>

							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>






	</section>
</section>
<!-- END CONTENT -->
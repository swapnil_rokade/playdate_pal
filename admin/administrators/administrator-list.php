<?php 
/**
 * PLAYDATE ADMIN - PART - ADMINISTRATOR LIST
 * 
 */
if(!isset($administrators)) {$administrators=array();}

?>
<!-- START ADMINISTRATORS LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Administrators</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/administrators.php">Administrators</a></li>
						<li class="active"><strong>All Administrators</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All Administrators</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Last Login</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($administrators as $auxAdmin) {
									?>
									<tr>
										<td><?php echo $auxAdmin->getId_administrator() ?></td>
										<td><?php echo $auxAdmin->getFullName() ?></td>
										<td><?php echo $auxAdmin->getEmail() ?></td>
										<td><?php echo $auxAdmin->getUsername() ?></td>
										<td><?php echo $auxAdmin->getLastlogin() ?></td>
										<td><a href="/admin/administrators.php?action=edit&id_administrator=<?php echo $auxAdmin->getId_administrator() ?>" class="bt-edit"
											title="Edit"><img src="assets/images/bt-edit.png" /></a>
											<?php if($auxAdmin->getId_administrator() != $_SESSION[ADM_SESSION_ID]) { 
											 //An administrator cannot delete himself
											?>
											<a
											title="Delete" href="/admin/administrators.php?action=delete&id_administrator=<?php echo $auxAdmin->getId_administrator() ?>" onclick="return confirm('Are you sure you want to remove this administrator? This operation cannot be undone.');" class="bt-delete"><img
												src="assets/images/bt-delete.png" /></a>
												
											<?php } ?>	
											</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END ADMINISTRATORS LIST CONTENT -->
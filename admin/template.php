<?php 
/**
 * PLAYDATE - ADMIN TEMPLATE 
 */

if(!isset($main_section) || ($main_section==null)) {
    $main_section = "home/home-content.php"; //Section to load as main content
}
?>
<!DOCTYPE html>
<html class=" ">
    <head>
        
        <?php include("parts/adm-header.php")?>
		<style>
		.DTTT_button_collection {display:none;}
		</style>
    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body class=" ">
        <?php include("parts/adm-topbar.php") ?>
        
        <!-- START CONTAINER -->
        <div class="page-container row-fluid">

			<!-- START LEFT SIDEBAR -->
			<?php include("parts/adm-sidebar.php") ?>
			<!-- END LEFT SIDEBAR -->
            
            <!-- START CONTENT -->
            <?php include($main_section); ?>
            <!-- END CONTENT -->
            
            <!-- START CHAT SIDEBAR -->
            <?php include("parts/adm-chat-sidebar.php"); ?>
            <!-- END CHAT SIDEBAR -->
            
            
            </div>
        <!-- END CONTAINER -->
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

		<?php include("parts/adm-libs.php") ?>

        <!-- General section box modal start -->
        <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated bounceInDown">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Section Settings</h4>
                    </div>
                    <div class="modal-body">

                        Nothing to configure in this component. Thank you!

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#discount-usage').DataTable();
                $('#discount-type').DataTable();
                $('#discount-list').DataTable();
            });
        </script>
        <!-- modal end -->
    </body>
</html>

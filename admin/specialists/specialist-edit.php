<?php
/**
 * PLAYDATE ADMIN - SPECIALIST EDIT FORM 
 */
if (! isset($editSpecialist)) {
    header("Location: /admin/specialists.php");
}

?>
<style>
.income {color:green;}
.expense {color:red;}
.completed {color:green;}
.partial {color:red;}
.pending {color:orange;}
</style>

<!-- START CONTENT -->
<section id="main-content" class=" ">

		<section class="wrapper main-wrapper" style=''>

			<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
				<div class="page-title">

					<div class="pull-left">
						<h1 class="title">Edit Specialist</h1>
					</div>

					<div class="pull-right hidden-xs">
						<ol class="breadcrumb">
							<li><a href="/admin"><i class="fa fa-home"></i>Home</a></li>
							<li><a href="/admin/specialists.php">Specialists</a></li>
							<li class="active"><strong>Edit Specialist</strong></li>
						</ol>
					</div>

				</div>
			</div>
			<div class="clearfix"></div>
			
			<!-- personal info -->
			<form action="/admin/specialists.php" method="post" autocomplete="off" enctype="multipart/form-data">
					<input type="hidden" name="action" value="update" /> 
					<input type="hidden" name="id_specialist" value="<?php echo $editSpecialist->getId_specialist() ?>" />
			
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			
				<section class="box ">
					<header class="panel_header">
						<h2 class="title pull-left">Personal Info</h2>
						<div class="actions panel_actions pull-right">
							<i class="box_toggle fa fa-chevron-down"></i> <i
								class="box_close fa fa-times"></i>
						</div>
					</header>
					<div class="content-body">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div
									class="alert alert-error alert-dismissible fade in">
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">x</span>
									</button>
									<strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div
									class="alert alert-success alert-dismissible fade in">
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true">x</span>
									</button>
									<strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>
								<div class="form-group">
									<label class="form-label" for="field-5">Profile</label> <span
										class="desc"></span> <select class="form-control"
										name="profile">
										<option value="<?php echo Specialist::$PROFILE_LEAD ?>"
											<?php echo (($editSpecialist->getProfile()==Specialist::$PROFILE_LEAD)?"selected=\"selected\"":"") ?>>Lead</option>
										<option value="<?php echo Specialist::$PROFILE_SUPPORTER ?>"
											<?php echo (($editSpecialist->getProfile()==Specialist::$PROFILE_SUPPORTER)?"selected=\"selected\"":"") ?>>Support</option>
									</select>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">US Citizen</label> <span
										class="desc"></span> <select class="form-control"
										name="us_citizen">
										<option value="1"
											<?php echo (($editSpecialist->getUs_citizen()==1)?"selected=\"selected\"":"") ?>>Yes</option>
										<option value="0"
											<?php echo (($editSpecialist->getUs_citizen()==0)?"selected=\"selected\"":"") ?>>No</option>
									</select>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-1">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $editSpecialist->getName()?>"
											class="form-control" id="field-1" name="name"
											placeholder="Name" required="required"/>
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="field-1">Last Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $editSpecialist->getLastname()?>"
											class="form-control" id="field-1" name="lastname"
											placeholder="Last Name" required="required"/>
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="field-5">Username</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $editSpecialist->getUsername()?>"
											class="form-control" name="username" placeholder="Username">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Email</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $editSpecialist->getEmail() ?>"
											class="form-control" name="email" placeholder="Email" required="required">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">New Password</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="password" value="" class="form-control"
											name="password" placeholder="New Password">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Cell Phone</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $editSpecialist->getCellphone() ?>"
											class="form-control" name="cellphone"
											placeholder="Cell Phone">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Work Phone</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $editSpecialist->getPhone() ?>"
											class="form-control" name="phone" placeholder="Work Phone">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-6">About Specialist</label>
									<span class="desc"></span>
									<div class="controls">
										<textarea class="form-control autogrow" cols="5" rows="4"
											id="field-6" name="about_me"><?php echo $editSpecialist->getAbout_me() ?></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="form-label" for="field-1">New Profile Image</label>
									<span class="desc"></span>
									<div class="controls">
										<input type="file" class="form-control" id="field-5" name="formfile" />
									</div>
									<div>
									<?php if(($editSpecialist->getPicture()!=null) && ($editSpecialist->getPicture()!="")) { ?>
										<img src="<?php echo $editSpecialist->getPicture() ?>" />
									<?php } ?>
									</div>
								</div>
								

							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save All</button>
									<button type="button" class="btn"
										onclick="top.location='/admin/specialists.php'">Cancel</button>
								</div>
							</div>


						</div>


					</div>
				</section>
			</div>
			<!-- /personal info -->
			<!-- experience -->
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<section class="box ">
					<header class="panel_header">
						<h2 class="title pull-left">Experience</h2>
						<div class="actions panel_actions pull-right">
							<i class="box_toggle fa fa-chevron-down"></i> <i
								class="box_setting fa fa-cog" data-toggle="modal"
								href="#section-settings"></i> <i class="box_close fa fa-times"></i>
						</div>
					</header>
					<div class="content-body">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
								<div class="row">
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Under 1 Year Old</label>
										<span class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getExp_range1() ?>"
												class="form-control" id="field-1" name="exp_range1">
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">2-3 Years Old</label>
										<span class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getExp_range2() ?>"
												class="form-control" id="field-1" name="exp_range2">
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">4+ Years Old</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getExp_range3() ?>"
												class="form-control" id="field-1" name="exp_range3">
										</div>
									</div>
								</div>
							</div>
							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save All</button>
									<button type="button" class="btn"
										onclick="top.location='/admin/specialists.php'">Cancel</button>
								</div>
							</div>

						</div>
					</div>
				</section>
			</div>
			<!-- /experience -->
			<!-- languages -->
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<section class="box ">
					<header class="panel_header">
						<h2 class="title pull-left">Languages</h2>
						<div class="actions panel_actions pull-right">
							<i class="box_toggle fa fa-chevron-down"></i> <i
								class="box_setting fa fa-cog" data-toggle="modal"
								href="#section-settings"></i> <i class="box_close fa fa-times"></i>
						</div>
					</header>
					<div class="content-body">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
								<div class="row">
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Language 1</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getLanguage_1() ?>"
												class="form-control" id="field-1" name="language_1"
												placeholder="Language">
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Language 2</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getLanguage_2() ?>"
												class="form-control" id="field-1" name="language_2"
												placeholder="Language">
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Language 3</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getLanguage_3() ?>"
												class="form-control" id="field-1" name="language_3"
												placeholder="Language">
										</div>
									</div>
								</div>
							</div>
							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save All</button>
									<button type="button" class="btn"
										onclick="top.location='/admin/specialists.php'">Cancel</button>
								</div>
							</div>

						</div>
					</div>
				</section>
			</div>
			<!-- /languages -->
			<!-- additional -->
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<section class="box ">
					<header class="panel_header">
						<h2 class="title pull-left">Additional</h2>
						<div class="actions panel_actions pull-right">
							<i class="box_toggle fa fa-chevron-down"></i> <i
								class="box_setting fa fa-cog" data-toggle="modal"
								href="#section-settings"></i> <i class="box_close fa fa-times"></i>
						</div>
					</header>
					<div class="content-body">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
								<div class="row">
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Additional 1</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getAdditional_1() ?>"
												class="form-control" id="field-1" name="additional_1"
												placeholder="Additional Info">
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Additional 2</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getAdditional_2() ?>"
												class="form-control" id="field-1" name="additional_2"
												placeholder="Additional Info">
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Additional 3</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text"
												value="<?php echo $editSpecialist->getAdditional_3() ?>"
												class="form-control" id="field-1" name="additional_3"
												placeholder="Additional Info">
										</div>
									</div>
								</div>
							</div>
							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save All</button>
									<button type="button" class="btn"
										onclick="top.location='/admin/specialists.php'">Cancel</button>
								</div>
							</div>

						</div>
					</div>
				</section>
			</div>
			<!-- /additional -->
			<!-- certificate -->
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<section class="box ">
					<header class="panel_header">
						<h2 class="title pull-left">Certifications</h2>
						<div class="actions panel_actions pull-right">
							<i class="box_toggle fa fa-chevron-down"></i> <i
								class="box_setting fa fa-cog" data-toggle="modal"
								href="#section-settings"></i> <i class="box_close fa fa-times"></i>
						</div>
					</header>
					<div class="content-body">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
								<div class="row">
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Undergraduate Degree
											in Nursing</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_nursing" class="form-control">
												<option value="0"
													<?php echo (($editSpecialist->getCert_nursing()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
													<?php echo (($editSpecialist->getCert_nursing()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Red Cross Special
											Education</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_redcross" class="form-control">
												<option value="0"
													<?php echo (($editSpecialist->getCert_redcross()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
													<?php echo (($editSpecialist->getCert_redcross()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Teaching Second
											Language</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_language" class="form-control">
												<option value="0"
													<?php echo (($editSpecialist->getCert_language()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
													<?php echo (($editSpecialist->getCert_language()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
									
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Teaching Professional</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_teaching" class="form-control">
												<option value="0"
												<?php echo (($editSpecialist->getCert_teaching()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
												<?php echo (($editSpecialist->getCert_teaching()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
									
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">CPR Certified</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_cpcr" class="form-control">
												<option value="0"
												<?php echo (($editSpecialist->getCert_cpcr()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
												<?php echo (($editSpecialist->getCert_cpcr()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
									
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">First Aid Certified</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_firstaid" class="form-control">
												<option value="0"
												<?php echo (($editSpecialist->getCert_firstaid()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
												<?php echo (($editSpecialist->getCert_firstaid()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
									
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Child Development Training</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_child_dev" class="form-control">
												<option value="0"
												<?php echo (($editSpecialist->getCert_child_dev()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
												<?php echo (($editSpecialist->getCert_child_dev()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
									
									<div class="form-group col-lg-4">
										<label class="form-label" for="field-1">Approved Partner</label> <span class="desc"></span>
										<div class="controls">
											<select name="cert_partner" class="form-control">
												<option value="0"
												<?php echo (($editSpecialist->getCert_partner()==0)?"selected=\"selected\"":"") ?>>No</option>
												<option value="1"
												<?php echo (($editSpecialist->getCert_partner()==1)?"selected=\"selected\"":"") ?>>Yes</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save All</button>
									<button type="button" class="btn"
										onclick="top.location='/admin/specialists.php'">Cancel</button>
								</div>
							</div>

						</div>
					</div>
				</section>
			</div>
			<!-- /certificate -->
			</form>
			<!--  pay something -->
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Create Payment to Specialist</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
				<form action="/admin/specialists.php" method="post">
					<input type="hidden" name="action" value="pay-specialist" />
					<input type="hidden" name="id_specialist" value="<?php echo $editSpecialist->getId_specialist() ?>" />
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<div class="row children-row">
								<div class="form-group col-lg-2">
									<label class="form-label" for="field-1">Amount ($)</label> <span class="desc"></span>
										<div class="controls">
											<input type="text" value="" class="form-control" id="field-1" name="amount">
										</div>
								</div>

								<div class="form-group col-lg-4">
									<label class="form-label" for="field-1">Description</label> <span class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" id="field-1" name="description">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
            						<div class="text-left">
            							<button type="submit" class="btn btn-primary">Create Payment to Specialist</button>
            						</div>
            					</div>
							</div>
    						
						</div>
					</div>
				</form>
				</div>
			</section>
		</div>
		<!--  /pay something -->
		
		<!-- payments -->
		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Payments to <strong><?php echo $editSpecialist->getFullName() ?></strong></h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							
							<!-- ********************************************** -->
							<table id="payments"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Status</th>
										<th>Description</th>
										<th>Amount Playdate</th>
										<th>Amount Addons</th>
										<th>Amount Paid</th>
										<th>Date</th>
										<th>Actions</th>
									</tr>
								</thead>

								<tbody>
									<?php
									$payments = PaymentDAO::getPaymentListBySpecialist($editSpecialist->getId_specialist());
									
									foreach($payments as $auxPay) {
									    $status = "Pending";
									    if($auxPay->getStatus()==Payment::$STATUS_COMPLETED) {
									        $status = "Completed";
									    } else if($auxPay->getStatus()==Payment::$STATUS_PARTIAL) {
									        $status = "Partial";
									    } else if($auxPay->getStatus()==Payment::$STATUS_PENDING) {
									        $status = "Pending";
									    } else if($auxPay->getStatus()==Payment::$STATUS_ROLLBACK) {
									        $status = "Rolled back";
									    }
									    
									    
									?>
									<tr>
										<td class="<?php echo strtolower($status)?>"><?php echo $status ?></td>
										<td><?php echo $auxPay->getName() ?></td>
										<td><?php echo Utils::moneyFormat($auxPay->getAmount_playdate()) ?></td>
										<td><?php echo Utils::moneyFormat($auxPay->getAmount_addons()) ?></td>
										<td><?php echo Utils::moneyFormat($auxPay->getAmount_paid()) ?></td>
										<td><?php echo $auxPay->getIdate() ?></td>
										<td>
											<?php 
											if($auxPay->getStatus()!=Payment::$STATUS_COMPLETED) {
											?>
											<a href="/admin/specialists.php?action=execute-pay&id_specialist=<?php echo $editSpecialist->getId_specialist()?>&id_payment=<?php echo $auxPay->getId_payment()?>" onclick="return confirm('Are you sure you want to execute this order?');">[Execute]</a>
											<?php 
											}
											?>
										 	<a href="/admin/specialists.php?action=delete-pay&id_specialist=<?php echo $editSpecialist->getId_specialist()?>&id_payment=<?php echo $auxPay->getId_payment()?>"  onclick="return confirm('Are you sure you want to delete this order?');">[Delete]</a>
										 </td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- /payments -->
		
		</section>
</section>
<!-- END CONTENT -->
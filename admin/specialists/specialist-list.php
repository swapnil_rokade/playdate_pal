<?php
/**
 * PLAYDATE ADMIN - PART - SPECIALIST LIST
 * 
 */
if (! isset($specialists)) {
    $specialists = array();
}

?>
<!-- START SPECIALISTS CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Specialists</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/specialists.php">Specialists</a></li>
						<li class="active"><strong>All Specialists</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All Specialists</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>


							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Profile</th>
										<th>Name</th>
										<th>Username</th>
										<th>Email</th>
										<th>Phone</th>
										<th>Added On</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
								<?php 
								foreach ($specialists as $auxSpecialist) {
								    $profilename = "Lead";
								    switch ($auxSpecialist->getProfile()) {
								        case Specialist::$PROFILE_LEAD: {
								            $profilename = "Lead";
								            break;
								        }
								        case Specialist::$PROFILE_SUPPORTER: {
								            $profilename = "Supporter";
								            break;
								        }
								        case Specialist::$PROFILE_NURSE: {
								            $profilename = "Nurse";
								            break;
								        }
								        
								    }
								?>
									<tr>
										<td><?php echo $auxSpecialist->getId_specialist() ?></td>
										<td><?php echo $profilename ?></td>
										<td><?php echo $auxSpecialist->getFullName() ?></td>
										<td><?php echo $auxSpecialist->getUsername() ?></td>
										<td><?php echo $auxSpecialist->getEmail() ?></td>
										<td><?php echo $auxSpecialist->getPhone() ?></td>
										<td><?php echo Utils::dateFormat($auxSpecialist->getIdate()) ?></td>
										<td><a href="/admin/specialists.php?action=edit&id_specialist=<?php echo $auxSpecialist->getId_specialist() ?>" class="bt-edit"
											title="Edit"><img src="assets/images/bt-edit.png" /></a><a
											title="Delete" href="/admin/specialists.php?action=delete&id_specialist=<?php echo $auxSpecialist->getId_specialist() ?>" class="bt-delete" onclick="return confirm('Are you sure you want to remove this specialist and all his information? This operation cannot be undone.');" ><img
												src="assets/images/bt-delete.png" /></a></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->

						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END SPECIALISTS CONTENT -->
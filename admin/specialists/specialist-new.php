<?php
/**
 * PLAYDATE ADMIN - SPECIALIST NEW FORM 
 */

if(!isset($newSpecialist)) {
    $newSpecialist = new Specialist();
    $newSpecialist->setUs_citizen(1); //by default, us citizen
    $newSpecialist->setProfile(Specialist::$PROFILE_LEAD);
}

?>
<!-- START SPECIALIST NEW CONTENT -->
<section id="main-content" class=" ">
	<form action="/admin/specialists.php" method="post" autocomplete="off">
		<input type="hidden" name="action" value="create" /> 
		
		<section class="wrapper main-wrapper" style=''>

			<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
				<div class="page-title">

					<div class="pull-left">
						<h1 class="title">New Specialist</h1>
					</div>

					<div class="pull-right hidden-xs">
						<ol class="breadcrumb">
							<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
							<li><a href="/admin/specialists.php">Specialists</a></li>
							<li class="active"><strong>New Specialist</strong></li>
						</ol>
					</div>

				</div>
			</div>
			<div class="clearfix"></div>
			<!-- personal info -->
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<section class="box ">
					<header class="panel_header">
						<h2 class="title pull-left">Personal Info</h2>
						<div class="actions panel_actions pull-right">
							<i class="box_toggle fa fa-chevron-down"></i> <i
								class="box_close fa fa-times"></i>
						</div>
					</header>
					<div class="content-body">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
                                	<label class="form-label" for="field-5">Profile</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="us_citizen">
                                    	<option value="<?php echo Specialist::$PROFILE_SUPPORTER ?>"<?php echo (($newSpecialist->getProfile()==Specialist::$PROFILE_SUPPORTER)?"selected=\"selected\"":"") ?>>Support</option>
                                    	<option value="<?php echo Specialist::$PROFILE_LEAD ?>"<?php echo (($newSpecialist->getProfile()==Specialist::$PROFILE_LEAD)?"selected=\"selected\"":"") ?>>Lead</option>
                                    </select>
                                </div>
								
								<div class="form-group">
                                	<label class="form-label" for="field-5">US Citizen</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="us_citizen">
                                    	<option value="0"<?php echo (($newSpecialist->getUs_citizen()==0)?"selected=\"selected\"":"") ?>>No</option>
                                    	<option value="1" <?php echo (($newSpecialist->getUs_citizen()==1)?"selected=\"selected\"":"") ?>>Yes</option>
                                    </select>
                                </div>

								<div class="form-group">
									<label class="form-label" for="field-1">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $newSpecialist->getName()?>"
											class="form-control" id="field-1" name="name"
											placeholder="Name" required="required"/>
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="field-1">Last Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $newSpecialist->getLastname()?>"
											class="form-control" id="field-1" name="lastname"
											placeholder="Last Name" required="required" />
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="field-5">Username</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $newSpecialist->getUsername()?>"
											class="form-control" name="username" placeholder="Username">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Email</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $newSpecialist->getEmail() ?>"
											class="form-control" name="email" placeholder="Email" required="required">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Password</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="password" value="" class="form-control"
											name="password" placeholder="New Password">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Cell Phone</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $newSpecialist->getCellphone() ?>"
											class="form-control" name="cellphone"
											placeholder="Cell Phone">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Work Phone</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $newSpecialist->getPhone() ?>"
											class="form-control" name="phone" placeholder="Work Phone">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-6">About Specialist</label>
									<span class="desc"></span>
									<div class="controls">
										<textarea class="form-control autogrow" cols="5" rows="4"
											id="field-6" name="about_me"><?php echo $newSpecialist->getAbout_me() ?></textarea>
									</div>
								</div>
							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save All</button>
									<button type="button" class="btn"
										onclick="top.location='/admin/specialists.php'">Cancel</button>
								</div>
							</div>


						</div>


					</div>
				</section>
			</div>
			<!-- /personal info -->
		</section>
	</form>
</section>
<!-- END SPECIALIST NEW CONTENT -->
<?php 
/**
 * PLAYDATE ADMIN - PART - PLAYPACK LIST
 * 
 */
if(!isset($playpacks)) {$playpacks=array();}

?>
<!-- START PLAYPACKS LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Playpacks</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/playpacks.php">Playpacks</a></li>
						<li class="active"><strong>All Playpacks</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All playpacks</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Type</th>
										<th>Name</th>
										<th>Amount</th>
										<th>Help</th>
										<th>Creation Date</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($playpacks as $auxPlaypack) {
									?>
									<tr>
										<td><?php echo $auxPlaypack->getId_playpack() ?></td>
										<td><?php echo $auxPlaypack->getType() ?></td>
										<td><?php echo $auxPlaypack->getName() ?></td>
										<td><?php echo $auxPlaypack->getAmount() ?></td>
										<td><?php echo $auxPlaypack->getHelp() ?></td>
										<td><?php echo Utils::dateFormat($auxPlaypack->getIdate()) ?></td>
										<td><a href="/admin/playpacks.php?action=edit&id_playpack=<?php echo $auxPlaypack->getId_playpack() ?>" class="bt-edit"
											title="Edit"><img src="assets/images/bt-edit.png" /></a>
											<a
											title="Delete" href="/admin/playpacks.php?action=delete&id_playpack=<?php echo $auxPlaypack->getId_playpack() ?>" onclick="return confirm('Are you sure you want to remove this playpack? This operation cannot be undone.');" class="bt-delete"><img
												src="assets/images/bt-delete.png" /></a>
											</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END PLAYPACKS LIST CONTENT -->
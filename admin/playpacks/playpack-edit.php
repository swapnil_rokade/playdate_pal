<?php
/**
 * PLAYDATE ADMIN - PLAYPACK EDIT FORM 
 */
if (! isset($editPlaypack)) {
    header("Location: /admin/playpacks.php");
}
?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Edit Playpack</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/playpacks.php">Playpacks</a></li>
						<li class="active"><strong>Edit Playpack</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Playpack Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/playpacks.php" method="post">
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="id_playpack" value="<?php echo $editPlaypack->getId_playpack() ?>" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
									<label class="form-label" for="name">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaypack->getName()?>" class="form-control" id="name" name="name"  placeholder="Name" />
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="type">Type</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="type" class="form-control">
											<option value="<?php echo Playpack::$TYPE_1CHILD ?>" <?php echo(($editPlaypack->getType()==Playpack::$TYPE_1CHILD)?"selected=\"selected\"":"") ?>>Playpack for 1 child</option>
											<option value="<?php echo Playpack::$TYPE_2CHILD ?>" <?php echo(($editPlaypack->getType()==Playpack::$TYPE_2CHILD)?"selected=\"selected\"":"") ?>>Playpack for 2 child</option>
										</select>
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="amount">Amount ($)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaypack->getAmount()?>" class="form-control" name="amount" placeholder="">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="amount">Savings Description</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editPlaypack->getDescription()?>" class="form-control" name="description" placeholder="Save $-">
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="type">Icon</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="icon" class="form-control">
											<option value="<?php echo Playpack::$ICON_LITTLE_PACK ?>" <?php echo(($editPlaypack->getIcon()==Playpack::$ICON_LITTLE_PACK)?"selected=\"selected\"":"") ?>>Little Playpack</option>
											<option value="<?php echo Playpack::$ICON_MEDIUM_PACK ?>" <?php echo(($editPlaypack->getIcon()==Playpack::$ICON_MEDIUM_PACK)?"selected=\"selected\"":"") ?>>Medium Playpack</option>
											<option value="<?php echo Playpack::$ICON_BIG_PACK ?>" <?php echo(($editPlaypack->getIcon()==Playpack::$ICON_BIG_PACK)?"selected=\"selected\"":"") ?>>Big Playpack</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="help">Help</label>
									<span class="desc"></span>
									<div class="controls">
										<textarea class="form-control autogrow" cols="5" rows="4"
											id="help" name="help"><?php echo $editPlaypack->getHelp() ?></textarea>
									</div>
								</div>
								
							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="button" class="btn" onclick="top.location='/admin/playpacks.php'">Cancel</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>

	</section>
</section>
<!-- END CONTENT -->
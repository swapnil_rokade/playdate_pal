<?php
/**
 * PLAYDATE ADMIN - PART - REQUEST INVITATION LIST
 * 
 */
if (! isset($requests)) {
    $requests = array();
}

?>
<!-- START CONTACT LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Contacts</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/requests.php">Request Invitation</a></li>
						<li class="active">All Requests</li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All Requests</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->

							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Status</th>
										<th>Id</th>
										<th>Name</th>
										<th>Last Name</th>
										<th>Username</th>
										<th>Email</th>
										<th>Zipcode</th>
										<th>Date</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($requests as $auxRequest) {
									    $reqStatus = Invitation::$STATUS_CREATED;
									    $statusName = "Pending";
									    $strColor="orange";
									    switch ($auxRequest->getStatus()) {
									        
									        case Invitation::$STATUS_ACCEPTED: {
									            $statusName = "Accepted";
									            $strColor="green";
									            break;
									        }
									        
									        case Invitation::$STATUS_DECLINED: {
									            $statusName = "Declined";
									            $strColor="red";
									            break;
									        }
									        
									    }
 									?>
									<tr>
										<td style="color:<?php echo $strColor?>"><?php echo $statusName ?></td>
										<td><?php echo $auxRequest->getId_request_invitation() ?></td>
										<td><?php echo $auxRequest->getName() ?></td>
										<td><?php echo $auxRequest->getLastname() ?></td>
										<td><?php echo $auxRequest->getUsername() ?></td>
										<td><?php echo $auxRequest->getEmail() ?></td>
										<td><?php echo $auxRequest->getZipcode() ?></td>
										<td><?php echo Utils::dateTimeFormat($auxRequest->getIdate()) ?></td>
										<td>
											<?php if($auxRequest->getStatus()==Invitation::$STATUS_CREATED) { ?>
											<a href="/admin/requests.php?action=accept&id_request=<?php echo $auxRequest->getId_request_invitation() ?>"><li class="fa fa-check"></li></a> &nbsp; 
											<a href="/admin/requests.php?action=decline&id_request=<?php echo $auxRequest->getId_request_invitation() ?>"><li class="fa fa-times"></li></a> &nbsp; 
											<?php } ?>
											<a href="/admin/requests.php?action=delete&id_request=<?php echo $auxRequest->getId_request_invitation() ?>"><li class="fa fa-trash"></li></a></td>
									</tr>
									<?php } ?>
									
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>

	</section>
</section>
<!-- END REQUEST LIST CONTENT -->
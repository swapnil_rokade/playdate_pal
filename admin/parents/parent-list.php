<?php 
/**
 * PLAYDATE ADMIN - PART - PARENT LIST
 * 
 */
if(!isset($parents)) {$parents=array();}

?>
<!-- START ADMINISTRATORS LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Parents</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/parents.php">Parents</a></li>
						<li class="active"><strong>All Parents</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All parents</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Phone</th>
										<th>Credit</th>
										<th>Registration Date</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($parents as $auxParent) {
									?>
									<tr>
										<td><?php echo $auxParent->getId_parent() ?></td>
										<td><?php echo $auxParent->getFullName() ?></td>
										<td><?php echo $auxParent->getEmail() ?></td>
										<td><?php echo $auxParent->getUsername() ?></td>
										<td><?php echo $auxParent->getPhone() ?></td>
										<td>$ <?php echo Utils::moneyFormat($auxParent->getCredit()) ?></td>
										<td><?php echo Utils::dateFormat($auxParent->getIdate()) ?></td>
										<td><a href="/admin/parents.php?action=edit&id_parent=<?php echo $auxParent->getId_parent() ?>" class="bt-edit"
											title="Edit"><img src="assets/images/bt-edit.png" /></a>
											<a
											title="Delete" href="/admin/parents.php?action=delete&id_parent=<?php echo $auxParent->getId_parent() ?>" onclick="return confirm('Are you sure you want to remove this parent and all his information? This operation cannot be undone.');" class="bt-delete"><img
												src="assets/images/bt-delete.png" /></a>
											</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END ADMINISTRATORS LIST CONTENT -->
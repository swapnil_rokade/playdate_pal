<?php
/**
 * PLAYDATE ADMIN - PARENT EDIT FORM 
 */
if (! isset($editParent)) {
    header("Location: /admin/parents.php");
}
?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Edit Parent</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/parents.php">Parents</a></li>
						<li class="active"><strong>Edit Parent</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Personal Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/parents.php" method="post">
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="id_parent" value="<?php echo $editParent->getId_parent() ?>" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
									<label class="form-label" for="field-5">Profile</label> <span
										class="desc"></span> <select class="form-control"
										name="profile">
										<option value="<?php echo ParentPd::$PROFILE_STANDARD ?>"
											<?php echo (($editParent->getProfile()==ParentPd::$PROFILE_STANDARD)?"selected=\"selected\"":"") ?>>Standard</option>
										<option value="<?php echo ParentPd::$PROFILE_EXPERIENCED ?>"
											<?php echo (($editParent->getProfile()==ParentPd::$PROFILE_EXPERIENCED)?"selected=\"selected\"":"") ?>>Experienced</option>
									</select>
								</div>
								
								
								<div class="form-group">
									<label class="form-label" for="field-1">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editParent->getName()?>" class="form-control" id="field-1" name="name"  placeholder="Name" />
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="field-1">Last Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editParent->getLastname()?>" class="form-control" id="field-1" name="lastname"  placeholder="Last Name" />
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="field-5">Username</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editParent->getUsername()?>" class="form-control" name="username" placeholder="Username">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Email</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editParent->getEmail() ?>" class="form-control" name="email"  placeholder="Email">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">New Password</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="password" value="" class="form-control" name="password" placeholder="New Password">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Cell Phone</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editParent->getCellphone() ?>" class="form-control" name="cellphone" placeholder="Cell Phone">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-5">Work Phone</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editParent->getPhone() ?>" class="form-control" name="phone" placeholder="Work Phone">
									</div>
								</div>

								<?php /* ?>
								<div class="form-group">
									<label class="form-label" for="field-1">Profile</label> <span
										class="desc"></span> <select class="form-control">
										<option></option>
										<option>Standard</option>
										<option>Bronze</option>
										<option>Gold</option>
										<option>Platinum</option>
									</select>
								</div>
								*/?>
								
								<div class="form-group">
									<label class="form-label" for="field-1">New Profile Image</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="file" class="form-control" id="field-5" name="formfile">
									</div>
								</div>

							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="button" class="btn" onclick="top.location='/admin/parents.php'">Cancel</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>

		<?php /*?>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Neighborhoods</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="#" method="post">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group ">
									<label class="form-label" for="field-1">Neighborhoods</label> <span
										class="desc"></span>
									<div class="controls neighborhood-row">
										<input type="text" value="" class="form-control" id="field-3">
									</div>
								</div>
								<div class="form-group">
									<div class="text-left">
										<button type="button" class="btn btn-primary bt-add"
											id="bt-add-neighborhood">Add Neighborhood</button>
									</div>
								</div>
							</div>


							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="button" class="btn btn-primary">Save</button>
									<button type="button" class="btn">Cancel</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>
		*/?>

		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Children's availability for playdates</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/parents.php" method="post">
							<input type="hidden" name="id_parent" value="<?php echo $editParent->getId_parent() ?>" />
							<input type="hidden" name="action" value="updateavailability" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
									<label class="form-label" for="field-1">Monday</label> <span
										class="desc"></span>
									<div class="row">
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_mon_init() ?>" id="field-31" placeholder="-" name="children_avb_mon_init">
										</div>
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_mon_end() ?>" id="field-31" placeholder="-" name="children_avb_mon_end">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-1">Tuesday</label> <span
										class="desc"></span>
									<div class="row">
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_tue_init() ?>" id="field-31" placeholder="-" name="children_avb_tue_init">
										</div>
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_tue_end() ?>" id="field-31" placeholder="-" name="children_avb_tue_end">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-1">Wednesday</label> <span
										class="desc"></span>
									<div class="row">
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_wed_init() ?>" id="field-31" placeholder="-" name="children_avb_wed_init">
										</div>
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_wed_end() ?>" id="field-31" placeholder="-" name="children_avb_wed_end">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-1">Thursday</label> <span
										class="desc"></span>
									<div class="row">
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_thu_init() ?>" id="field-31" placeholder="-" name="children_avb_thu_init">
										</div>
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_thu_end() ?>" id="field-31" placeholder="-" name="children_avb_thu_end">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-1">Friday</label> <span
										class="desc"></span>
									<div class="row">
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_fri_init() ?>" id="field-31" placeholder="-" name="children_avb_fri_init">
										</div>
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_fri_end() ?>" id="field-31" placeholder="-" name="children_avb_fri_end">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-1">Saturday</label> <span
										class="desc"></span>
									<div class="row">
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_sat_init() ?>" id="field-31" placeholder="-" name="children_avb_sat_init">
										</div>
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_sat_end() ?>" id="field-31" placeholder="-" name="children_avb_sat_end">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="field-1">Sunday</label> <span
										class="desc"></span>
									<div class="row">
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_sun_init() ?>" id="field-31" placeholder="-" name="children_avb_sun_init">
										</div>
										<div class="controls col-lg-6">
											<input type="text" class="form-control" value="<?php echo $editParent->getChildren_avb_sun_end() ?>" id="field-31" placeholder="-" name="children_avb_sun_end">
										</div>
									</div>
								</div>

							</div>
							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="button" class="btn" onclick="top.location='/admin/parents.php'">Cancel</button>
								</div>
							</div>

						</form>
					</div>


				</div>
			</section>
		</div>

		
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Your Children</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
				<?php 
				$children = ChildrenDAO::getChildrenListByParent($editParent->getId_parent());
				foreach($children as $auxChildren) {
				?>
				     <!-- children <?php echo $auxChildren->getId_children() ?> -->
				
					<div class="row">
						<form action="/admin/parents.php" method="post">
							<input type="hidden" name="action" value="updatechildren" />
							<input type="hidden" name="id_parent" value="<?php echo $editParent->getId_parent() ?>" />
							<input type="hidden" name="id_children" value="<?php echo $auxChildren->getId_children() ?>" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							
								<div class="row children-row">

									<div class="form-group col-lg-3">
										<label class="form-label" for="field-5">Gender</label> <span
											class="desc"></span> 
											<select class="form-control" name="genre">
												<option value="boy" <?php echo(($auxChildren->getGenre()=="boy")?"selected=\"selected\"":"") ?>>Male</option>
												<option value="girl" <?php echo(($auxChildren->getGenre()=="girl")?"selected=\"selected\"":"") ?>>Female</option>
											</select>
									</div>

									<div class="form-group col-lg-3">
										<label class="form-label" for="field-1">Name</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text" value="<?php echo $auxChildren->getName() ?>" class="form-control" id="field-1" name="name">
										</div>
									</div>


									<div class="form-group col-lg-3">
										<label class="form-label" for="field-5">Date of Birth</label>
										<span class="desc"></span>
										<div class="controls">
											<input type="text" class="form-control datepicker"
												data-format="mm/dd/yyyy" value="<?php echo $auxChildren->getBirthdate() ?>" name="birthdate">
										</div>
									</div>

									<div class="form-group col-lg-3">
										<label class="form-label" for="field-1">Age</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text" value="<?php echo $auxChildren->getAge() ?>" class="form-control" id="field-1" name="age">
										</div>
									</div>

									<div class="form-group col-lg-12">
										<label class="form-label" for="field-6">Important to know</label>
										<span class="desc"></span>
										<div class="controls">
											<textarea class="form-control autogrow" cols="5" id="field-6" name="notes"><?php echo $auxChildren->getNotes()?></textarea>
										</div>
									</div>
									<div class="form-group col-lg-12">
										
                             			<input type="checkbox" id="remove_kid" value="1" name="remove_kid" style="width:20px">
                             			<label for="remove_kid" style="width:auto; ">Remove this kid (will be removed only if not used in reservations)</label>
                        
									</div>
									
									<?php /* CHILDREN INTERESTS
									<div class="row col-lg-12">
										<div class="form-group col-lg-12">
											<label class="form-label" for="field-6">Interests</label>
										</div>
										<div class="col-lg-3">
											<div class="list-container stem">
												<div class="form-group">
													<input type="checkbox" id="0" class=""
														placeholder="keyword" required=""> <label for="0">Academic</label>
												</div>
												<div class="form-group">
													<input type="checkbox" id="1" checked="checked" class=""
														placeholder="keyword" required=""> <label for="1">STEM</label>
												</div>
											</div>
										</div>
										<div class="col-lg-3">
											<div class="list-container creative">
												<div class="form-group">
													<input type="checkbox" id="2" class=""
														placeholder="keyword" required=""> <label for="2">Creative</label>
												</div>
												<div class="form-group">
													<input type="checkbox" id="0" class=""
														placeholder="keyword" required=""> <label for="3">Performing
														Arts</label>
												</div>
												<div class="form-group">
													<input type="checkbox" id="4" checked="checked" class=""
														placeholder="keyword" required=""> <label for="4">Language
														&amp; Culture</label>
												</div>
											</div>
										</div>
										<div class="col-lg-3">
											<div class="list-container outdoor">
												<div class="form-group">
													<input type="checkbox" id="5" class=""
														placeholder="keyword" required=""> <label for="5">Play</label>
												</div>
												<div class="form-group">
													<input type="checkbox" id="6" class=""
														placeholder="keyword" required=""> <label for="6">Sports
														&amp; Recreation</label>
												</div>
												<div class="form-group">
													<input type="checkbox" checked="checked" id="7" class=""
														placeholder="keyword" required=""> <label for="7">Health
														&amp; Wellness</label>
												</div>
												<div class="form-group">
													<input type="checkbox" id="8" class=""
														placeholder="keyword" required=""> <label for="8">Outdoor</label>
												</div>
											</div>
										</div>
										<div class="col-lg-3">
											<div class="list-container attractions">
												<div class="form-group">
													<input type="checkbox" id="9" class=""
														placeholder="keyword" required=""> <label for="9">NYC
														Museums</label>
												</div>
												<div class="form-group">
													<input type="checkbox" id="10" class=""
														placeholder="keyword" required=""> <label for="10">NYC
														Attractions</label>
												</div>
												<div class="form-group">
													<input type="checkbox" checked="checked" id="11" class=""
														placeholder="keyword" required=""> <label for="11">NYC
														Theater</label>
												</div>
											</div>
										</div>
									</div>
									*/?>

        							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
        								<div class="text-left">
        									<button type="submit" class="btn btn-primary">Save</button>
        								</div>
        							</div>

								</div>
							</div>
                            <?php /* ?>
							<div class="form-group col-lg-8 col-md-8 col-sm-9 col-xs-12">
								<div class="text-left">
									<button type="button" class="btn btn-primary bt-add"
										id="bt-add-child">Add Child</button>
								</div>
							</div>
                            */?>
						</form>
						
					</div>
					<div class="row"></div>
					<!-- /children <?php echo $auxChildren->getId_children() ?> -->
					<?php } ?>							


				</div>
			</section>
		</div>

		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Emergency Contact & authorized for
						pickup</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<?php 
					$emergencys = EmergencyDAO::getEmergencyListByParent($editParent->getId_parent());
					foreach($emergencys as $auxEmergency) {
					?>
				
					<div class="row">
						<form action="/admin/parents.php" method="post">
							<input type="hidden" name="id_parent" value="<?php echo $editParent->getId_parent() ?>" />
							<input type="hidden" name="id_emergency" value="<?php echo $auxEmergency->getId_emergency() ?>" />
							<input type="hidden" name="action" value="updateemergency" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="row children-row">

									<div class="form-group col-lg-3">
										<label class="form-label" for="field-1">Name</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text" value="<?php echo $auxEmergency->getName()?>" class="form-control" id="field-1" name="name">
										</div>
									</div>

									<div class="form-group col-lg-3">
										<label class="form-label" for="field-1">Mail</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text" value="<?php echo $auxEmergency->getMail() ?>" class="form-control" id="field-1" name="mail">
										</div>
									</div>



									<div class="form-group col-lg-3">
										<label class="form-label" for="field-1">Phone Number</label> <span
											class="desc"></span>
										<div class="controls">
											<input type="text" value="<?php echo $auxEmergency->getPhone()?>" class="form-control" id="field-1" name="phone">
										</div>
									</div>

									<div class="form-group col-lg-3">
										<label class="form-label" for="field-1">Relation to Children</label>
										<span class="desc"></span>
										<div class="controls">
											<input type="text" value="<?php echo $auxEmergency->getRelation() ?>" class="form-control" id="field-1" name="relation">
										</div>
									</div>

        							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
        								<div class="text-left">
        									<button type="submit" class="btn btn-primary">Save Contact</button>
        								</div>
        							</div>
								</div>

							</div>

							<?php /*?>
							<div class="form-group col-lg-8 col-md-8 col-sm-9 col-xs-12">
								<div class="text-left">
									<button type="button" class="btn btn-primary bt-add"
										id="bt-add-authorized">Add Person</button>
								</div>
							</div>
                            */?>
							
						</form>
					</div>
					<?php } ?>

				</div>
			</section>
		</div>

	<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Add Credit to Parent - Actual Credit: $ <strong><?php echo Utils::moneyFormat($editParent->getCredit()) ?></strong></h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
				<form action="/admin/parents.php" method="post">
					<input type="hidden" name="action" value="add-credit" />
					<input type="hidden" name="id_parent" value="<?php echo $editParent->getId_parent() ?>" />
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<div class="row children-row">
								<div class="form-group col-lg-2">
									<label class="form-label" for="field-1">Amount ($)</label> <span class="desc"></span>
										<div class="controls">
											<input type="text" value="" class="form-control" id="field-1" name="amount">
										</div>
								</div>

								<div class="form-group col-lg-4">
									<label class="form-label" for="field-1">Description</label> <span class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" id="field-1" name="description">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
            						<div class="text-left">
            							<button type="submit" class="btn btn-primary">Add Credit</button>
            						</div>
            					</div>
							</div>
    						
						</div>
					</div>
				</form>
				</div>
			</section>
		</div>


		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Pull Credit from Parent - Actual Credit: $ <strong><?php echo Utils::moneyFormat($editParent->getCredit()) ?></strong></h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
				<form action="/admin/parents.php" method="post">
					<input type="hidden" name="action" value="pull-credit" />
					<input type="hidden" name="id_parent" value="<?php echo $editParent->getId_parent() ?>" />
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<div class="row children-row">
								<div class="form-group col-lg-2">
									<label class="form-label" for="field-1">Amount ($)</label> <span class="desc"></span>
										<div class="controls">
											<input type="text" value="" class="form-control" id="field-1" name="amount_playdate">
										</div>
								</div>

								<div class="form-group col-lg-4">
									<label class="form-label" for="field-1">Description</label> <span class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" id="field-1" name="comment">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
            						<div class="text-left">
            							<button type="submit" class="btn btn-primary">Pull Credit</button>
            						</div>
            					</div>
							</div>
    						
						</div>
					</div>
				</form>
				</div>
			</section>
		</div>


	</section>
</section>
<!-- END CONTENT -->
<?php
/**
 * PLAYDATE ADMIN - PART - CONTACT LIST
 * 
 */
if (! isset($contacts)) {
    $contacts = array();
}

?>
<!-- START CONTACT LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Contacts</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/contacts.php">Contact</a></li>
						<li class="active">All Contacts</li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All Contacts</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">



							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Is Read</th>
										<th>Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Message</th>
										<th>Date</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($contacts as $auxContact) {
 									?>
									<tr>
										<td><?php echo $auxContact->getId_contact() ?></td>
										<td><?php echo (($auxContact->getIs_read()==1)?"YES":"NO") ?></td>
										<td><?php echo $auxContact->getName() ?></td>
										<td><?php echo $auxContact->getLast_name() ?></td>
										<td><?php echo $auxContact->getEmail() ?></td>
										<td>"<?php echo  $auxContact->getMessage(100) ?>"</td>
										<td><?php echo Utils::dateTimeFormat($auxContact->getIdate()) ?></td>
										<td><a href="/admin/contacts.php?action=view&id_contact=<?php echo $auxContact->getId_contact() ?>" class="bt-edit" title="View message"><img
												src="assets/images/bt-see.png" /></a><a
											title="Delete" href="/admin/contacts.php?action=delete&id_contact=<?php echo $auxContact->getId_contact() ?>" onclick="return confirm('Are you sure you want to remove this contact? This operation cannot be undone.');" class="bt-delete"><img
												src="assets/images/bt-delete.png" /></a></td>
									</tr>
									<?php } ?>
									
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END CONTACT LIST CONTENT -->

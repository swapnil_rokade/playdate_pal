<?php
/**
 * PLAYDATE ADMIN - CONTACT DETAIL CONTENT 
 */
if (! isset($viewContact)) {
    header("Location: /admin/contacts.php");
}

// $editSpecialist = new Specialist();
?>
<!-- START CONTACT DETAIL CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">View Contact</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="index.html"><i class="fa fa-home"></i>Home</a></li>
						<li class="active"><a href="/admin/contacts.php">Contact</a></li>
						<li class="active">View Contact</li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">View contact</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/contacts.php" method="get">
							<input type="hidden" name="id_contact" value="<?php echo $viewContact->getId_contact() ?>" />
							<input type="hidden" name="action" value="markunread" />
							
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
									<p>
										<strong>Date</strong>
									</p>
									<p><?php echo Utils::dateTimeFormat($viewContact->getIdate()) ?></p>
								</div>

								<div class="form-group">
									<p>
										<strong>Name</strong>
									</p>
									<p><?php echo $viewContact->getName() ?></p>
								</div>

								<div class="form-group">
									<p>
										<strong>Last Name</strong>
									</p>
									<p><?php echo $viewContact->getLast_name() ?></p>
								</div>

								<div class="form-group">
									<p>
										<strong>Email</strong>
									</p>
									<p><?php echo $viewContact->getEmail() ?></p>
								</div>

								<div class="form-group">
									<p>
										<strong>Message</strong>
									</p>
									<p><?php echo $viewContact->getMessage() ?></p>
								</div>
							</div>


							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Mark as UNREAD</button>
									<button type="button" class="btn" onclick="top.location='/admin/contacts.php?action=unread'">Back to Unread Contacts</button>
									<button type="button" class="btn" onclick="top.location='/admin/contacts.php'">Back to All Contacts</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>






	</section>
</section>
<!-- START CONTACT DETAIL CONTENT -->
<?php 
/**
 * PLAYDATE - ADMIN - PLAYPACKS
 */

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "playpacks";
$menu_option = "playpacks-all";
$main_section = "playpacks/playpack-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    case "new": {
        $menu_section = "playpacks";
        $menu_option = "playpacks-new";
        $main_section = "playpacks/playpack-new.php"; //Section to load as main content
        
        break;
    }
    
    case "create": {
        //Create new Playpack
        $newPlaypack = new Playpack();
        $newPlaypack->readFromRow($_REQUEST);
        $newPlaypack = PlaypackDAO::createPlaypack($newPlaypack);
        if(($newPlaypack!=null) && ($newPlaypack->getId_playpack()>0)) {
            //ok
            $successMessage = "New playpack has been succesfully created";
            
             $menu_section = "playpacks";
             $menu_option = "playpacks-all";
             $main_section = "playpacks/playpack-list.php"; //Section to load as main content
             
             $playpacks = PlaypackDAO::getPlaypackList();
             
        } else {
            //error - go to new, with error message
            $errorMessage = "Error creating playpack. Please, review info and try again";
            
            $menu_section = "playpacks";
            $menu_option = "playpacks-new";
            $main_section = "playpacks/playpack-new.php"; //Section to load as main content
        }
        break;
    }
    
    case "delete": {
        $menu_section = "playpacks";
        $menu_option = "playpacks-all";
        $main_section = "playpacks/playpack-list.php"; //Section to load as main content
        
        //delete element
        $delPlaypack = new Playpack();
        $delPlaypack->readFromRow($_REQUEST);
        $deleted = PlaypackDAO::deletePlaypack($delPlaypack->getId_playpack());
        if($deleted) {
            //ok with message
            $successMessage = "Playpack has been deleted";
            
            $menu_section = "playpacks";
            $menu_option = "playpacks-all";
            $main_section = "playpacks/playpack-list.php"; //Section to load as main content
            
            $playpacks = PlaypackDAO::getPlaypackList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting playpack. Please, try again";
            
            $menu_section = "playpacks";
            $menu_option = "playpacks-all";
            $main_section = "playpacks/playpack-list.php"; //Section to load as main content
            
            $playpacks = PlaypackDAO::getPlaypackList();
        }
        
        break;
    }
    
    case "edit": {
        //view playpack
        $editPlaypack = new Playpack();
        $editPlaypack->readFromRow($_REQUEST);
        
        if($editPlaypack->getId_playpack()>0) {
            $editPlaypack= PlaypackDAO::getPlaypack($editPlaypack->getId_playpack());
        }
        
        if(($editPlaypack!=null) && ($editPlaypack->getId_playpack()>0)) {
            $menu_section = "playpacks";
            $main_section = "playpacks/playpack-edit.php"; //Section to load as main content
        } else {
            //playpack not found
            header("Location: /admin/playpacks.php");
        }
        break;
    }
    
    case "update": {
        $updated = false;
        
        $menu_section = "playpacks";
        $menu_option = "playpacks-all";
        $main_section = "playpacks/playpack-list.php"; //Section to load as main content
        
        //update playpacks
        $updPlaypack = new Playpack();
        $updPlaypack->readFromRow($_REQUEST);
        
        if($updPlaypack->getId_playpack()>0) {
            $updPlaypack = PlaypackDAO::getPlaypack($updPlaypack->getId_playpack()); //read all info
            
            $updPlaypack->readFromRow($_REQUEST); //change only this form parent info
            
            $updPlaypack= PlaypackDAO::updatePlaypack($updPlaypack);
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Playpack has been updated";
            
            $menu_section = "playpacks";
            $menu_option = "playpacks-all";
            $main_section = "playpacks/playpack-list.php"; //Section to load as main content
            
            $playpacks = PlaypackDAO::getPlaypackList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating playpack. Please, try again";
            
            $menu_section = "playpacks";
            $menu_option = "playpacks-all";
            $main_section = "playpacks/playpack-list.php"; //Section to load as main content
            
            $playpacks = PlaypackDAO::getPlaypackList();
        }
        
        break;
    }
    
    default: {
        
        $menu_section = "playpacks";
        $menu_option = "playpacks-all";
        $main_section = "playpacks/playpack-list.php"; //Section to load as main content
        
        $playpacks = PlaypackDAO::getPlaypackList();
        
        break;
    }
}

include("template.php");
?>
<?php
/**
 * PLAYDATE ADMIN - DISCOUNT EDIT FORM 
 */
if (! isset($editDiscountType)) {
    header("Location: /admin/discounts.php");
}
?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Edit Discount Type</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/discounts.php">Discounts</a></li>
						<li class="active"><strong>Edit Discount Type</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Discount Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/discounts.php" method="post">
							<input type="hidden" name="action" value="update-discount-type" />
							<input type="hidden" name="id_discount_type" value="<?php echo $editDiscountType->getId_discount_type() ?>" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
									<label class="form-label" for="name">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editDiscountType->getName()?>" class="form-control" id="name" name="name"  placeholder="Name" />
									</div>
								</div>
                                
                                <div class="form-group">
									<label class="form-label" for="type">Type</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="type" class="form-control">
											<option value="Restricted" <?php echo(($editDiscountType->getType()=='Restricted')?"selected=\"selected\"":"") ?>>Restricted</option>
											<option value="Public" <?php echo(($editDiscountType->getType()=='Public')?"selected=\"selected\"":"") ?>>Public</option>
                                            <option value="Private" <?php echo(($editDiscountType->getType()=='Private')?"selected=\"selected\"":"") ?>>Private</option>
                                            <option value="Other" <?php echo(($editDiscountType->getType()=='Other')?"selected=\"selected\"":"") ?>>Other</option>
                                        </select>
									</div>
								</div>

                                <div class="form-group">
									<label class="form-label" for="discount">Discount</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="discount" class="form-control">
											<option value="10" <?php echo(($editDiscountType->getDiscount()==10)?"selected=\"selected\"":"") ?>>10%</option>
											<option value="20" <?php echo(($editDiscountType->getDiscount()==20)?"selected=\"selected\"":"") ?>>20%</option>
											<option value="30" <?php echo(($editDiscountType->getDiscount()==30)?"selected=\"selected\"":"") ?>>30%</option>
											<option value="40" <?php echo(($editDiscountType->getDiscount()==40)?"selected=\"selected\"":"") ?>>40%</option>
											<option value="50" <?php echo(($editDiscountType->getDiscount()==50)?"selected=\"selected\"":"") ?>>50%</option>
											<option value="60" <?php echo(($editDiscountType->getDiscount()==60)?"selected=\"selected\"":"") ?>>60%</option>
											<option value="70" <?php echo(($editDiscountType->getDiscount()==70)?"selected=\"selected\"":"") ?>>70%</option>
											<option value="80" <?php echo(($editDiscountType->getDiscount()==80)?"selected=\"selected\"":"") ?>>80%</option>
											<option value="90" <?php echo(($editDiscountType->getDiscount()==90)?"selected=\"selected\"":"") ?>>90%</option>
											<option value="100" <?php echo(($editDiscountType->getDiscount()==100)?"selected=\"selected\"":"") ?>>100%</option>                                       
                                        </select>
									</div>
								</div>
								
							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="button" class="btn" onclick="top.location='/admin/discounts.php?action=discount-type'">Cancel</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>

	</section>
</section>
<!-- END CONTENT -->

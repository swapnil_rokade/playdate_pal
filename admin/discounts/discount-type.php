<?php
/**
 * PLAYDATE ADMIN - DISCOUNT NEW FORM 
 */

?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Add Discount Type</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/discounts.php">Discount</a></li>
						<li class="active"><strong>Add Discount Type</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/discounts.php" method="post">
							<input type="hidden" name="action" value="type-create" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>
                	
							    <div class="form-group">
									<label class="form-label" for="discount-code">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="" class="form-control" id="name" name="name"  placeholder="Name" />
                                        <span class="help-block">Add new Discount Type Name.</span>
									</div>
								</div>

                                <div class="form-group">
									<label class="form-label" for="type">Type</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="type" class="form-control">
											<option value="Restricted" selected=\"selected\"":"">Restricted</option>
											<option value="Public">Public</option>
                                            <option value="Private">Private</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <span class="help-block">Select any suitable Discount Code type.</span>
									</div>
								</div>
                                <div class="form-group">
									<label class="form-label" for="discount">Discount</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="discount" class="form-control">
											<option value="10" selected=\"selected\"":"">10%</option>
											<option value="20">20%</option>
											<option value="30">30%</option>
											<option value="40">40%</option>
											<option value="50">50%</option>
											<option value="60">60%</option>
											<option value="70">70%</option>
											<option value="80">80%</option>
											<option value="90">90%</option>
											<option value="100">100%</option>                                       
                                        </select>
                                        <span class="help-block">Select any suitable Discount Code type.</span>
									</div>
								</div>
							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>






	</section>
</section>
<!-- END CONTENT -->

<?php 
/**
 * PLAYDATE ADMIN - PART - DISCOUNT LIST
 * 
 */
if(!isset($discount_types)) {$discount_types=array();}

?>
<!-- START DISCOUNT LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Discounts</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/discounts.php">Discounts</a></li>
						<li class="active"><strong>All Discounts</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All Discounts</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->


							<table id="discount-list"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Sr. No.</th>
										<th>Discount Code</th>
										<th>Name</th>
										<th>Start Date</th>
										<th>End Date</th>
                                        <th>Type</th>
                                        <th>Description</th>                            
										<th>Creation Date</th>
                                        <th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
                                     $i=0;   
									foreach($discount_types as $auxDiscount) {
                                        $editDiscountType= DiscountDAO::getDiscountType($auxDiscount->getType());
									?>
									<tr>
                                        <td><?php echo $i+1; ?></td>
										<td><?php echo $auxDiscount->getdiscountCode() ?></td>
										<td><?php echo $auxDiscount->getName() ?></td>
										<td><?php echo $auxDiscount->getStartDate() ?></td>
										<td><?php echo $auxDiscount->getEndDate() ?></td>
										<td><?php echo $editDiscountType->getType() ?></td>
                                        <td><?php echo $auxDiscount->getDescription() ?></td>    
										<td><?php echo Utils::dateFormat($auxDiscount->getIdate()) ?></td>
										<td><a href="/admin/discounts.php?action=edit&id_discount=<?php echo $auxDiscount->getId_discount() ?>" class="bt-edit"
											title="Edit"><img src="assets/images/bt-edit.png" /></a>
											<a
											title="Delete" href="/admin/discounts.php?action=delete&id_discount=<?php echo $auxDiscount->getId_discount() ?>" onclick="return confirm('Are you sure you want to remove this discount? This operation cannot be undone.');" class="bt-delete"><img
												src="assets/images/bt-delete.png" /></a>
											</td>
									</tr>
									<?php 
                                         $i++;                           
                                    } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END DISCOUNTS LIST CONTENT -->

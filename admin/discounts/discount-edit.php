<?php
/**
 * PLAYDATE ADMIN - DISCOUNT EDIT FORM 
 */
if (! isset($editDiscount)) {
    header("Location: /admin/discounts.php");
}
if(!isset($discount_types)) {$discount_types=array();}
?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Edit Discount</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/discounts.php">Discounts</a></li>
						<li class="active"><strong>Edit Discount</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Discount Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<form action="/admin/discounts.php" method="post">
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="id_discount" value="<?php echo $editDiscount->getId_discount() ?>" />
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

                                <div class="form-group">
									<label class="form-label" for="discount-code">Discount Code*</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editDiscount->getdiscountCode()?>" class="form-control" id="discount-code" name="discount_code" required="true" placeholder="Discount Code" />
									</div>
								</div>
                                <div class="form-group">
									<label class="form-label" for="name">Name</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editDiscount->getName()?>" class="form-control" id="name" name="name"  placeholder="Name" />
									</div>
								</div>
                                <div class="form-group">
									<label class="form-label" for="start-date">Start Date (MM/DD/YYYY)</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text" value="<?php echo $editDiscount->getStartDate();?>" class="form-control datepicker" id="start-date" name="start_date"  placeholder="DD/MM/YYYY" />
									</div>
								</div>

                                <div class="form-group">
									<label class="form-label" for="end-date">End Date (MM/DD/YYYY)</label>
									<div class="controls">
										<input type="text" value="<?php echo $editDiscount->getEndDate();?>" class="form-control datepicker" id="end-date" name="end_date"  placeholder="DD/MM/YYYY" />
									</div>
								</div>            
                            
								<div class="form-group">
									<label class="form-label" for="type">Type</label> <span
										class="desc"></span>
									<div class="controls">
										<select name="type" class="form-control">
                                            <?php foreach($discount_types as $auxDiscountType) { ?>
							                <option value="<?php echo $auxDiscountType->getId_discount_type(); ?>" <?php echo(($editDiscount->getType()==$auxDiscountType->getId_discount_type())?"selected=\"selected\"":"") ?>><?php echo $auxDiscountType->getName();?></option>	
								            <?php } ?>                            
										</select>
                                        <!--span class="help-block">Select any suitable Discount type.</span-->
									</div>
								</div>


                                <div class="form-group">
									<label class="form-label" for="field-6">Description</label>
									<span class="desc"></span>
									<div class="controls">
										<textarea class="form-control autogrow" cols="5" rows="4" name="description"><?php echo $editDiscount->getDescription() ?></textarea>
									</div>
								</div>
								
							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="button" class="btn" onclick="top.location='/admin/discounts.php'">Cancel</button>
								</div>
							</div>
						</form>
					</div>


				</div>
			</section>
		</div>

	</section>
</section>
<!-- END CONTENT -->

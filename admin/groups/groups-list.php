<?php 
/**
 * PLAYDATE ADMIN - PART - GROUP LIST
 * 
 */
if(!isset($groups)) {$groups=array();}

?>
<!-- START GROUPS LIST CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">Groups</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/groups.php">Groups</a></li>
						<li class="active"><strong>All groups</strong></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">All groups</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>

							<!-- ********************************************** -->


							<table id="example"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Description</th>
										<th>Created</th>
										<th>&nbsp;</th>
									</tr>
								</thead>

								<tbody>
									<?php 
									foreach($groups as $auxGroup) {
									?>
									<tr>
										<td><?php echo $auxGroup->getId_group() ?></td>
										<td><?php echo $auxGroup->getName() ?></td>
										<td><?php echo Utils::getLimitedText($auxGroup->getDescription(), 120) ?></td>
										<td><?php echo Utils::dateFormat($auxGroup->getIdate()) ?></td>
										<td>
										<?php /*?>
										<a href="/admin/parentrequests.php?action=view&id_request=<?php echo $auxRequest->getId_request() ?>" class="bt-view"
											title="View"><img src="assets/images/bt-see.png" /></a> */?>
											<a
											title="Delete" href="/admin/groups.php?action=delete&id_group=<?php echo $auxGroup->getId_group() ?>" onclick="return confirm('Are you sure you want to remove this group and all its information? This operation cannot be undone.');" class="bt-delete"><img
												src="assets/images/bt-delete.png" /></a>
											</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->




						</div>
					</div>
				</div>
			</section>
		</div>






	</section>
</section>
<!-- END GROUP LIST CONTENT -->
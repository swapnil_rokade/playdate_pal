<?php
/**
 * PLAYDATE ADMIN - PLAYDATE VIEW FORM 
 */
if (! isset($editPlaydate)) {
    header("Location: /admin/parents.php");
}

$leadName = "";
if($editPlaydate->getId_specialist()) {
    $leadName = SpecialistDAO::getSpecialist($editPlaydate->getId_specialist())->getFullName();
}

$numReservations = PlaydateDAO::countPlaydateReservations($editPlaydate->getId_playdate());

$addonsPrice = 0;
if($editPlaydate->getAdd1_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd1_price()*100;
}
if($editPlaydate->getAdd2_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd2_price()*100;
}
if($editPlaydate->getAdd3_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd3_price()*100;
}
if($editPlaydate->getAdd4_price()!=null) {
    $addonsPrice += $editPlaydate->getAdd4_price()*100;
}

$numHours = $editPlaydate->getTime_end()-$editPlaydate->getTime_init();

$pricePerChild = PlaydatePriceDAO::getPlaydatePrice($editPlaydate->getId_playdate());

$payments = PaymentDAO::getPaymentListByPlaydate($editPlaydate->getId_playdate());

$incomes = 0; 
$expenses = 0;

if($payments!=null) {
    foreach($payments as $auxPay) {
        if($auxPay->getId_parent()!=null) {
            if(($auxPay->getStatus()==Payment::$STATUS_COMPLETED) || ($auxPay->getStatus()==Payment::$STATUS_PARTIAL)) {
                $incomes += $auxPay->getAmount_playdate()+$auxPay->getAmount_addons();
            }
        }
        
        if($auxPay->getId_specialist()!=null) {
            $expenses += $auxPay->getAmount_playdate()+$auxPay->getAmount_addons();
        }
    }
}

$playdateStatus = "PUBLISHED";
switch ($editPlaydate->getStatus()) {
    case Playdate::$STATUS_DECLINED: {
        $playdateStatus = "DECLINED";
        break;
    }
    
    case Playdate::$STATUS_FINISHED: {
        $playdateStatus = "FINISHED";
        break;
    }
    
    case Playdate::$STATUS_PUBLISHED: {
        $playdateStatus = "PUBLISHED";
        break;
    }
    
    case Playdate::$STATUS_REQUEST: {
        $playdateStatus = "REQUEST";
        break;
    }
    
    case Playdate::$STATUS_RSVP: {
        $playdateStatus = "IN RSVP";
        break;
    }
}

?>
<style>
.income {color:green;}
.expense {color:red;}
.completed {color:green;}
.partial {color:red;}
.pending {color:orange;}
</style>
<!-- START CONTENT -->
<section id="main-content" class=" ">
	<section class="wrapper main-wrapper" style=''>

		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<div class="page-title">

				<div class="pull-left">
					<h1 class="title">View Playdate</h1>
				</div>

				<div class="pull-right hidden-xs">
					<ol class="breadcrumb">
						<li><a href="/admin/"><i class="fa fa-home"></i>Home</a></li>
						<li><a href="/admin/playdates.php">Playdates</a></li>
						<li class="active"><a href="/admin/playdates.php?action=view&id_playdate=<?php echo $editPlaydate->getId_playdate() ?>"><strong>View Playdate</strong></a></li>
					</ol>
				</div>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Basic Info</h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
							<?php if($errorMessage!=null) { ?>
                            	<div class="alert alert-error alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Warning:</strong> <?php echo $errorMessage ?>
                                </div>
                            <?php } ?>

							<?php if($successMessage!=null) { ?>
                            	<div class="alert alert-success alert-dismissible fade in">
                                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>
                                    <strong>Success:</strong> <?php echo $successMessage ?>
                                </div>
                            <?php } ?>
							
							
								<form action="/admin/playdates.php" method="get">
        							<input type="hidden" name="action" value="edit" />
        							<input type="hidden" name="id_playdate" value="<?php echo $editPlaydate->getId_playdate() ?>" />
    								<div class="form-group">
        								<div class="text-left">
        									<button type="submit" class="btn btn-primary">Edit Basic Playdate Info</button>
        								</div>
    								</div>
    							</form>
								
								<div class="form-group">
									<strong>Name</strong>: 
									<p><?php echo $editPlaydate->getName()?></p>
								</div>
								<div class="form-group">
									<strong>Date</strong>: 
									<p><?php echo Utils::dateFormat($editPlaydate->getDate()) ?>, <?php echo Utils::get12hourFormat($editPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($editPlaydate->getTime_end()) ?></p>
								</div>
								<div class="form-group">
									<strong>Lead</strong>: 
									<p><?php echo $leadName ?></p>
								</div>
								<div class="form-group">
									<strong>Reservations / Max Children</strong>: 
									<p><?php echo $numReservations ?> / <?php echo $editPlaydate->getNum_children() ?></p>
								</div>
								<div class="form-group">
									<strong>Status</strong>: <?php echo $playdateStatus ?> 
								</div>
										
							</div>
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
								<form action="/admin/playdates.php" method="post">
    							<input type="hidden" name="action" value="generate-payments" />
    							<input type="hidden" name="id_playdate" value="<?php echo $editPlaydate->getId_playdate() ?>" />
								<div class="form-group">
    								<div class="text-left">
    									<button type="submit" class="btn btn-primary">RSVP - Generate Payment and Charge Orders</button>
    								</div>
								</div>
								</form>
							</div>
						
					</div>


				</div>
			</section>
		</div>
		
		<?php 
		if($payments != null) {
		?>
		<!-- payments -->
		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Charge and payment orders for <strong><?php echo $editPlaydate->getName() ?></strong> - INCOMES: <span style="color:green">$<?php echo Utils::moneyFormat($incomes) ?></span> - EXPENSES: <span style="color:red">$<?php echo Utils::moneyFormat($expenses) ?></span> - BALANCE: <strong style="<?php echo ((($incomes-$expenses)>0)?"color:green":"color:red") ?>">$<?php echo Utils::moneyFormat(($incomes-$expenses)) ?></strong></h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							
							<!-- ********************************************** -->
							<table id="paymentstable"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Type</th>
										<th>Status</th>
										<th>Parent</th>
										<th>Specialist</th>
										<th>Amount Playdate</th>
										<th>Amount Addons</th>
										<th>Total</th>
										<th>Total Paid</th>
										<th>Comment</th>
										<th>Actions</th>
									</tr>
								</thead>

								<tbody>
								
									<?php 
																		
									foreach($payments as $auxPay) {
									    $typePayment = "Income";
									    $parent = null;
									    $specialist=null;
									    $parentName = "-";
									    if($auxPay->getId_parent()!=null) {
									        $parent = ParentDAO::getParent($auxPay->getId_parent());
									        $parentName = $parent->getFullName();
									        $typePayment = "Income";
									    }
									    
									    $specialistName = "-";
									    if($auxPay->getId_specialist()!=null) {
									        $specialist = SpecialistDAO::getSpecialist($auxPay->getId_specialist());
									        $specialistName = $specialist->getFullName();
									        $typePayment = "Expense";
									    }
									    
									    $status = "Pending";
									    if($auxPay->getStatus()==Payment::$STATUS_COMPLETED) {
									        $status = "Completed";
									    } else if($auxPay->getStatus()==Payment::$STATUS_PARTIAL) {
									        $status = "Partial";
									    } else if($auxPay->getStatus()==Payment::$STATUS_PENDING) {
									        $status = "Pending";
									    } else if($auxPay->getStatus()==Payment::$STATUS_ROLLBACK) {
									        $status = "Rolled back";
									    }
									    
									?>
									<tr>
										<td class="<?php echo strtolower($typePayment)?>"><?php echo $typePayment ?></td>
										<td class="<?php echo strtolower($status)?>"><?php echo $status ?></td>
										<td><?php echo $parentName ?></td>
										<td><?php echo $specialistName ?></td>
										<td>$<?php echo Utils::moneyFormat($auxPay->getAmount_playdate()) ?></td>
										<td>$<?php echo Utils::moneyFormat($auxPay->getAmount_addons()) ?></td>
										<td>$<?php echo Utils::moneyFormat($auxPay->getAmount_playdate()+ $auxPay->getAmount_addons()) ?></td>
										<td>$<?php echo Utils::moneyFormat($auxPay->getAmount_paid()) ?></td>
										<td><?php echo $auxPay->getComment() ?></td>
										<td>
											<?php 
											if($auxPay->getStatus()!=Payment::$STATUS_COMPLETED) {
											?>
											<a href="/admin/playdates.php?action=execute-pay&id_playdate=<?php echo $editPlaydate->getId_playdate()?>&id_payment=<?php echo $auxPay->getId_payment()?>" onclick="return confirm('Are you sure you want to execute this order?');">[Execute]</a>
											<?php 
											}
											?>
										 	<a href="/admin/playdates.php?action=delete-pay&id_playdate=<?php echo $editPlaydate->getId_playdate()?>&id_payment=<?php echo $auxPay->getId_payment()?>"  onclick="return confirm('Are you sure you want to delete this order?');">[Delete]</a>
										 </td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- /payments -->
		<?php } ?>
		
		
		<?php 
		$reservations = PlaydateDAO::getAllParentReservationByPlaydate($editPlaydate->getId_playdate());
				
		foreach($reservations as $auxReservation) {
		     $parent = ParentDAO::getParent($auxReservation->getId_parent());
		     $childrenReservation = PlaydateDAO::getChildrenReservationsByReservation($auxReservation->getId_reservation());
		     
		     $priceForParent = 0;
		     $addonsForParent = 0;
		     $numChild = 0;
		     foreach($childrenReservation as $auxRes) {
		         $numChild +=$auxRes->getNum_children();
		         $priceForParent += ($auxRes->getNum_children() * $pricePerChild);
		         $addonsForParent += ($auxRes->getNum_children() * $addonsPrice);
		     }
		     
		     
		?>
		
		<div class="col-lg-12">
			<section class="box ">
				<header class="panel_header">
					<h2 class="title pull-left">Reservation for <strong><?php echo $parent->getFullName() ?></strong> - NUM KIDS RESERVED: <?php echo $numChild ?> - PLAYDATE TOTAL PRICE: $<?php echo Utils::moneyFormat($priceForParent, 2) ?> - ADDONS TOTAL PRICE: $<?php echo Utils::moneyFormat($addonsForParent, 2) ?></h2>
					<div class="actions panel_actions pull-right">
						<i class="box_toggle fa fa-chevron-down"></i> <i
							class="box_setting fa fa-cog" data-toggle="modal"
							href="#section-settings"></i> <i class="box_close fa fa-times"></i>
					</div>
				</header>
				<div class="content-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							
							<!-- ********************************************** -->
							<table id="childsof<?php echo $parent->getId_parent() ?>"
								class="display table table-hover table-condensed"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Child Name</th>
										<th>Num. of Kids</th>
										<th>Age</th>
										<th>Notes</th>
									</tr>
								</thead>

								<tbody>
								
									<?php 
									foreach($childrenReservation as $auxChildren) {
									    $childName = "Extra-children";
									    $childNotes = "";
									    if($auxChildren->getId_children()!=null) {
									        $child = ChildrenDAO::getChildren($auxChildren->getId_children());
									        $childName = $child->getName();
									        $childNotes = $child->getNotes();
									    }
									?>
									<tr>
										<td><?php echo $childName ?></td>
										<td><?php echo $auxChildren->getNum_children() ?></td>
										<td><?php echo $auxChildren->getAge() ?></td>
										<td><?php echo $childNotes ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- ********************************************** -->
						</div>
					</div>
				</div>
			</section>
		</div>
		<?php } ?>		
		


	</section>
</section>
<!-- END CONTENT -->
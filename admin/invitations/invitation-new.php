<?php
/**
 * PLAYDATE ADMIN - INVITATION NEW FORM 
 */

if(!isset($newInvitation)) {
    $newInvitation = new Invitation();
}

?>
<!-- START SPECIALIST NEW CONTENT -->
<section id="main-content" class=" ">
	<form action="/admin/invitations.php" method="post" autocomplete="off">
		<input type="hidden" name="action" value="create" /> 
		
		<section class="wrapper main-wrapper" style=''>

			<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
				<div class="page-title">

					<div class="pull-left">
						<h1 class="title">New Invitation</h1>
					</div>

					<div class="pull-right hidden-xs">
						<ol class="breadcrumb">
							<li><a href="/admin/index.php"><i class="fa fa-home"></i>Home</a></li>
							<li><a href="/admin/invitations.php">Invitations</a></li>
							<li class="active"><strong>New Invitation</strong></li>
						</ol>
					</div>

				</div>
			</div>
			<div class="clearfix"></div>
			<!-- personal info -->
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<section class="box ">
					<header class="panel_header">
						<h2 class="title pull-left">New Invitation</h2>
						<div class="actions panel_actions pull-right">
							<i class="box_toggle fa fa-chevron-down"></i> <i
								class="box_close fa fa-times"></i>
						</div>
					</header>
					<div class="content-body">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">

								<div class="form-group">
									<label class="form-label" for="field-5">Email</label> <span
										class="desc"></span>
									<div class="controls">
										<input type="text"
											value="<?php echo $newInvitation->getEmail() ?>"
											class="form-control" name="email" placeholder="Email" required="required">
									</div>
								</div>


							</div>

							<div
								class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
								<div class="text-left">
									<button type="submit" class="btn btn-primary">Create Invitation</button>
									<button type="button" class="btn"
										onclick="top.location='/admin/invitations.php'">Cancel</button>
								</div>
							</div>


						</div>


					</div>
				</section>
			</div>
			<!-- /personal info -->
		</section>
	</form>
</section>
<!-- END SPECIALIST NEW CONTENT -->
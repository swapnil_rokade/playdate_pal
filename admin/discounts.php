<?php 
/**
 * PLAYDATE - ADMIN - DISCOUNTS
 */

include_once("../classes/all_classes.php");
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "discount-code";
$menu_option = "discounts-all";
$main_section = "discounts/discount-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
        
    case "new": {
        $menu_section = "discount-code";
        $menu_option = "discount-new";
        $main_section = "discounts/discount-new.php"; //Section to load as main content
        $discount_types = DiscountDAO::getDiscountTypeList();

        break;
    }
    
    case "type": {
        $menu_section = "discount-code";
        $menu_option = "discount-type";
        $main_section = "discounts/discount-type.php"; //Section to load as main content
        break;
    }
    
    case "type-create": {
 
        $newDiscountType = new Discount();
        $newDiscountType->readTypeFromRow($_REQUEST);
        $newDiscountType = DiscountDAO::createDiscountType($newDiscountType);
        
        if(($newDiscountType!=null) && ($newDiscountType->getId_discount_type()>0)) {
            //ok
             $successMessage = "New Discount Type has been succesfully created";
             //$menu_section = "discount-code";
             //$menu_option = "discounts-all";
             //$main_section = "discounts/discount-type-list.php"; //Section to load as main content
             
             $discount_types = DiscountDAO::getDiscountTypeList();
             header("Location: /admin/discounts.php?action=discount-type");   
             
        } else {
            //error - go to new, with error message
            $errorMessage = "Error creating discount type. Please, review info and try again";
            
            $menu_section = "discount-code";
            $menu_option = "discount-new";
            $main_section = "discounts/discount-new.php"; //Section to load as main content
        }
        break;
    }

    case "discount-type": {
 
         $menu_section = "discount-code";
         $menu_option = "discount-type-all";
         $main_section = "discounts/discount-type-list.php"; //Section to load as main content
             
         $discount_types = DiscountDAO::getDiscountTypeList();
         break;
    }

    case "discount-usage": {
 
         $menu_section = "discount-code";
         $menu_option = "discount-usage-all";
         $main_section = "discounts/discount-usage-list.php"; //Section to load as main content
             
         $discount_usage = DiscountDAO::getDiscountUsageList();
         break;
    }

    case "create": {
        //Create new Discount
        $newDiscount = new Discount();
        $newDiscount->readFromRow($_REQUEST);
        $newDiscount = DiscountDAO::createDiscount($newDiscount);
        
        if(($newDiscount!=null) && ($newDiscount->getId_discount()>0)) {
            //ok
             $successMessage = "New Discount has been succesfully created";
             $menu_section = "discount-code";
             $menu_option = "discounts-all";
             $main_section = "discounts/discount-list.php"; //Section to load as main content
             
             $discount_types = DiscountDAO::getDiscountList();
             //header("Location: /admin/discounts.php");   
             
        } else {
            //error - go to new, with error message
            $errorMessage = "Error creating discount. Please, review info and try again";
            
            $menu_section = "discount-code";
            $menu_option = "discount-new";
            $main_section = "discounts/discount-new.php"; //Section to load as main content
        }
        break;
    }
    
    case "delete": {
        $menu_section = "discount-code";
        $menu_option = "discounts-all";
        $main_section = "discounts/discount-list.php"; //Section to load as main content
        
        //delete element
        $delDiscountType = new Discount();
        $delDiscountType->readFromRow($_REQUEST);
        $deleted = DiscountDAO::deleteDiscount($delDiscountType->getId_discount());
        if($deleted) {
            //ok with message
            header("Location: /admin/discounts.php");
            $successMessage = "Discount has been deleted";
            
            $menu_section = "discount-code";
            $menu_option = "discounts-all";
            $main_section = "discounts/discount-list.php"; //Section to load as main content
            
            $discount_types = DiscountDAO::getDiscountList();
            
        } else {
            //error - go to list, with error message
            header("Location: /admin/discounts.php");
            $errorMessage = "Error deleting Discounts. Please, try again";
            
            $menu_section = "discount-code";
            $menu_option = "discounts-all";
            $main_section = "discounts/discount-list.php"; //Section to load as main content
            
            $discount_types = DiscountDAO::getDiscountList();
        }
        
        break;
    }

    case "delete-type": {
        $menu_section = "discount-code";
        $menu_option = "discounts-all";
        $main_section = "discounts/discount-type-list.php"; //Section to load as main content
        
        //delete element
        $delDiscountType = new Discount();
        $delDiscountType->readTypeFromRow($_REQUEST);
        $deleted = DiscountDAO::deleteDiscountType($delDiscountType->getId_discount_type());
        if($deleted) {
            //ok with message
            header("Location: /admin/discounts.php?action=discount-type");
            $successMessage = "Discount type has been deleted";
            
            $menu_section = "discount-code";
            $menu_option = "discounts-all";
            $main_section = "discounts/discount-type-list.php"; //Section to load as main content
            
            $discount_types = DiscountDAO::getDiscountTypeList();
            
        } else {
            //error - go to list, with error message
            header("Location: /admin/discounts.php?action=discount-type");
            $errorMessage = "Error deleting discount types. Please, try again";
            
            $menu_section = "discount-code";
            $menu_option = "discounts-all";
            $main_section = "discounts/discount-type-list.php"; //Section to load as main content
            
            $discount_types = DiscountDAO::getDiscountTypeList();
        }
        
        break;
    }
   
    
    case "edit": {
        //view discount
        $editDiscount = new Discount();
        $editDiscount->readFromRow($_REQUEST);
        if($editDiscount->getId_discount()>0) {
            $editDiscount= DiscountDAO::getDiscount($editDiscount->getId_discount());
        }
        
        if(($editDiscount!=null) && ($editDiscount->getId_discount()>0)) {
            $discount_types = DiscountDAO::getDiscountTypeList(); 
            $menu_section = "discount-code";
            $main_section = "discounts/discount-edit.php"; //Section to load as main content
        } else {
            //discount not found
            header("Location: /admin/discounts.php");
        }
        break;
    }
    case "update": {
        $updated = false;
        
        $menu_section = "discount-code";
        $menu_option = "discounts-all";
        $main_section = "discounts/discount-list.php"; //Section to load as main content
        
        //update discount
        $updDiscount = new Discount();
        $updDiscount->readFromRow($_REQUEST);
        
        if($updDiscount->getId_discount()>0) {
            $updDiscount = DiscountDAO::getDiscount($updDiscount->getId_discount()); //read all info
            
            $updDiscount->readFromRow($_REQUEST); //change only this form parent info
            
            $updDiscount= DiscountDAO::updateDiscount($updDiscount);
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Discount type has been updated";
            
            $discount_types = DiscountDAO::getDiscountList();
            //header("Location: /admin/discounts.php?action=discount-type");
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating discount type. Please, try again";
            
            $menu_section = "discount-code";
            $menu_option = "discounts-all";
            $main_section = "discounts/discount-list.php"; //Section to load as main content
            
            $discount_types = DiscountDAO::getDiscountTypeList();
        }
        
        break;
    }    

    case "edit-type": {
        //view discount type
        $editDiscountType = new Discount();
        $editDiscountType->readTypeFromRow($_REQUEST);
        
        if($editDiscountType->getId_discount_type()>0) {
            $editDiscountType= DiscountDAO::getDiscountType($editDiscountType->getId_discount_type());
        }
        
        if(($editDiscountType!=null) && ($editDiscountType->getId_discount_type()>0)) {
            $menu_section = "discount-code";
            $main_section = "discounts/discount-type-edit.php"; //Section to load as main content
        } else {
            //discount type not found
            header("Location: /admin/discounts.php");
        }
        break;
    }
 
    case "update-discount-type": {
        $updated = false;
        
        $menu_section = "discount-code";
        $menu_option = "discounts-all";
        $main_section = "discounts/discount-type-list.php"; //Section to load as main content
        
        //update discount type
        $updDiscountType = new Discount();
        $updDiscountType->readTypeFromRow($_REQUEST);
        
        if($updDiscountType->getId_discount_type()>0) {
            $updDiscountType = DiscountDAO::getDiscountType($updDiscountType->getId_discount_type()); //read all info
            
            $updDiscountType->readTypeFromRow($_REQUEST); //change only this form parent info
            
            $updDiscountType= DiscountDAO::updateDiscountType($updDiscountType);
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Discount type has been updated";
            
            $discount_types = DiscountDAO::getDiscountTypeList();
            //header("Location: /admin/discounts.php?action=discount-type");
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating discount type. Please, try again";
            
            $menu_section = "discount-code";
            $menu_option = "discounts-all";
            $main_section = "discounts/discount-list.php"; //Section to load as main content
            
            $discount_types = DiscountDAO::getDiscountTypeList();
        }
        
        break;
    }
    
    default: {
        
         $menu_section = "discount-code";
         $menu_option = "discounts-all";
         $main_section = "discounts/discount-list.php"; //Section to load as main content
         
         $discount_types = DiscountDAO::getDiscountList();
        
        break;
    }
}

include("template.php");
?>

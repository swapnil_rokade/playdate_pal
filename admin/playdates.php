<?php 
/**
 * PLAYDATE - ADMIN - PLAYADTES
 */

require_once('../vendor/autoload.php');

use Stripe\Transfer;

include_once("../classes/all_classes.php");

include_once("adm-common.php");
include_once("connection.php");

include_once("checkadmin.php");

$menu_section = "playdates";
$menu_option = "playdates-all";
$main_section = "playdates/playdates-list.php"; //Section to load as main content


$action = null;
if(isset($_REQUEST["action"])) {  
    $action = $_REQUEST["action"];
}

$successMessage = null;
$errorMessage = null;

    
switch ($action) {
    
    case "cancel-reservation": {
        
        $reservation = null;
        
        $id_reservation = null;
        if(isset($_REQUEST["id_reservation"])) {
            $id_reservation = $_REQUEST["id_reservation"];
        }
        
        if(($id_reservation!=null) && is_numeric($id_reservation)) {
            $reservation = PlaydateDAO::getParentReservation($id_reservation);
        }
        
        
        if($reservation!=null) {
            //Refund payment
            $payments = PaymentDAO::getPaymentListByParentPlaydate($reservation->getId_parent(), $reservation->getId_playdate());
            if($payments!=null) {
                foreach($payments as $auxPay) {
                    PaymentDAO::rollbackParentPayment($auxPay->getId_payment()); //Devolvemos dinero
                    PaymentDAO::deletePayment($auxPay->getId_payment()); //Eliminamos pago
                }
            }
            
            $playdate = PlaydateDAO::getPlaydate($reservation->getId_playdate());
            
            //Generate parent payment here
            $newPayment = new Payment();
            $newPayment->setId_parent($reservation->getId_parent());
            $newPayment->setId_playdate($reservation->getId_playdate());
            $newPayment->setName("[Cancellation] ".$playdate->getName());
            $newPayment->setAmount_playdate(500);
            $newPayment->setAmount_addons(0);
            $newPayment->setComment("Cancellation fee.");
            PaymentDAO::createParentPayment($newPayment);
            
            //Realizamos la ejecucion de pagos pendientes de este padre con este playdate (solo será el pago actual)
            $payments = PaymentDAO::getPaymentListByParentPlaydate($reservation->getId_parent(), $reservation->getId_playdate());
            if($payments!=null) {
                foreach($payments as $auxPay) {
                    if($auxPay->getStatus() == Payment::$STATUS_PENDING) {
                        //Execute this pending payment
                        PaymentDAO::executeParentPayment($auxPay->getId_payment(), $auxPay->getComment());
                    }
                }
            }
            
            //mark as cancelled
            PlaydateDAO::updateParentReservationStatus($reservation->getId_reservation(), ParentReservation::$STATUS_CANCELED);
            $successMessage = "Parent reservation has been cancelled.";
            
        } else {
            //Reservation not found
            $errorMessage = "Reservation not been found.";
        }
        
        //Go to playdate view
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        if(($editPlaydate!=null) && ($editPlaydate->getId_playdate()>0)) {
            $menu_section = "playdates";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
        } else {
            //playdate not found
            header("Location: /admin/playdates.php");
        }
        
        break;
    }
    
    case "execute-specialist-pay": {
        $payment = new Payment();
        $payment->readFromRow($_REQUEST);
        if($payment->getId_payment()!=null) {
            $payment = PaymentDAO::getPayment($payment->getId_payment());
            if($payment!=null) {
                //Execute el payment
                $editSpecialist = new Specialist();
                $editSpecialist = SpecialistDAO::getSpecialist($payment->getId_specialist());
                                
                if($editSpecialist->getId_specialist()>0) {
                    
                    if(($editSpecialist->getPay_stripeid()==null)||($editSpecialist->getPay_stripeid()=="")) {
                        $updated = false;
                        $errorMessage = "This specialist has not connected payment account";
                    } else {
                        
                        //pay amount to specialist
                        //Generate specialist payment here
                        $amountToPay = $payment->getAmount_playdate()+$payment->getAmount_addons(); //in $
                        
                        if($amountToPay>0) {
                            try {
                                //Payment in Stripe
                                \Stripe\Stripe::setApiKey($STRIPE_CONNECT_SECRET);
                                $transferDescription = "Payment to ".$editSpecialist->getFullName()." - ".$payment->getName();
                                // Create a Transfer to a connected account
                                $orderId = "PAYMENT".$payment->getId_payment();
                                $transfer = \Stripe\Transfer::create([
                                    "amount" => $amountToPay,
                                    "currency" => "usd",
                                    "description" => $transferDescription,
                                    "destination" => $editSpecialist->getPay_stripeid(),
                                    "transfer_group" => $orderId,
                                ]);
                                
                                PaymentDAO::updatePaymentStatus($payment->getId_payment(), $amountToPay, Payment::$STATUS_COMPLETED);
                                
                                $successMessage = "Payment to specialist has been executed";
                                
                                $updated = true;
                            } catch(Exception $e) {
                                $updated = false;
                                $errorMessage = "Error processing payment: ".$e->getMessage();
                            }
                        } else {
                            $updated = false;
                            $errorMessage = "Amount must be more than $0";
                        }
                    }
                }
                if($updated) {
                    //ok with message
                    if((!isset($successMessage)) || ($successMessage==null) || ($successMessage=="")) {
                        $successMessage = "Payment to specialist has been executed.";
                    }
                    
                    $menu_section = "specialists";
                    $main_section = "specialists/specialist-edit.php"; //Section to load as main content
                    
                    $editSpecialist = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
                    
                    
                    
                } else {
                    //error - go to list, with error message
                    if(!isset($errorMessage) || ($errorMessage==null)) {
                        $errorMessage = "Error executing payment. Please, try again";
                    }
                    
                    $menu_section = "specialists";
                    $main_section = "specialists/specialist-edit.php"; //Section to load as main content
                    
                    $editSpecialist = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
                }
                
                
            }
            
        }
        
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        $menu_section = "playdates";
        $main_section = "playdates/playdates-view.php"; //Section to load as main content
        
        break;
    }
    
    case "execute-pay": {
        $payment = new Payment();
        $payment->readFromRow($_REQUEST);
        if($payment->getId_payment()!=null) {
            $payment = PaymentDAO::getPayment($payment->getId_payment());
            if($payment!=null) {
                //Ejecutamos el payment
                if(PaymentDAO::executeParentPayment($payment->getId_payment())) {
                    //ok with message
                    if($payment->getId_parent()!=null) {
                        $successMessage = "Charge order has been executed";
                    } else if($payment->getId_specialist()!=null) {
                        $successMessage = "Payment order has been executed";
                    }
                } else {
                    $errorMessage = "Error: Payment order was not executed";
                    if($payment->getId_parent()!=null) {
                        $errorMessage = "Error: Charge order was not executed";
                    } else if($payment->getId_specialist()!=null) {
                        $errorMessage = "Error: Payment order was not executed";
                    }
                    
                }
                
            }
            
        }
        
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        $menu_section = "playdates";
        $main_section = "playdates/playdates-view.php"; //Section to load as main content
        
        break;
    }
    
    case "delete-pay": {
        $payment = new Payment();
        $payment->readFromRow($_REQUEST);
        if($payment->getId_payment()!=null) {
            $payment = PaymentDAO::getPayment($payment->getId_payment());
            if($payment!=null) {
                //Eliminamos el payment
                PaymentDAO::deletePayment($payment->getId_payment());
                //ok with message
                $successMessage = "Payment order has been deleted";
                if($payment->getId_parent()!=null) {
                    $successMessage = "Charge order has been deleted";
                } else if($payment->getId_specialist()!=null) {
                    $successMessage = "Payment order has been deleted";
                }
                
            }
            
        }
        
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        $menu_section = "playdates";
        $main_section = "playdates/playdates-view.php"; //Section to load as main content
        
        break;
    }
    
    case "generate-payments": {
        
        //echo(":::::::::");die();
        //Generate all playdate payments
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        if($editPlaydate!=null) {
            //Realizamos la devolución de reservas que pudiera haber hecho
            $payments = PaymentDAO::getPaymentListByPlaydate($editPlaydate->getId_playdate());
            if($payments!=null) {
                foreach($payments as $auxPay) {
                    PaymentDAO::rollbackParentPayment($auxPay->getId_payment());
                    //PaymentDAO::deletePayment($auxPay->getId_payment()); //Eliminamos pago
                }
            }
            
            $numReservations = PlaydateDAO::countPlaydateReservations($editPlaydate->getId_playdate());
            
            $addonsPrice = 0;

            // if($editPlaydate->getAdd1_price()!=null) {
            //     $addonsPrice += $editPlaydate->getAdd1_price()*100;
            // }
            // if($editPlaydate->getAdd2_price()!=null) {
            //     $addonsPrice += $editPlaydate->getAdd2_price()*100;
            // }
            // if($editPlaydate->getAdd3_price()!=null) {
            //     $addonsPrice += $editPlaydate->getAdd3_price()*100;
            // }
            // if($editPlaydate->getAdd4_price()!=null) {
            //     $addonsPrice += $editPlaydate->getAdd4_price()*100;
            // }
            
            $numHours = $editPlaydate->getTime_end()-$editPlaydate->getTime_init();
            $pricePerChild = PlaydatePriceDAO::getPlaydatePrice($editPlaydate->getId_playdate());
            $reservations = PlaydateDAO::getAllParentReservationByPlaydate($editPlaydate->getId_playdate());
           //  echo "<pre>";print_r($reservations);
            $priceForSpecialist = 0;
            $allAddonsPrice = 0;
            
            $minimumAge = null;
            
            foreach($reservations as $auxReservation) {
                
                //Processing parent reservation
                $parent = ParentDAO::getParent($auxReservation->getId_parent());
                $childrenReservation = PlaydateDAO::getChildrenReservationsByReservation($auxReservation->getId_reservation());
                //echo "<pre>";print_r($childrenReservation);
                $priceForParent = 0;
                $addonsForParent = 0;
                $numChild = 0;
                $namedChilds = 0;
                foreach($childrenReservation as $auxRes) {
                  //  echo "here";
                    if(($auxRes->getId_children()!=null) && (is_numeric($auxRes->getId_children()))) {
                        $namedChilds++;
                    }
                    
                    
                    $numChild +=$auxRes->getNum_children();
                    if($minimumAge==null) {
                        $minimumAge = $auxRes->getAge();
                    } else if($auxRes->getAge()<$minimumAge) {
                        $minimumAge = $auxRes->getAge();
                    }
                    
                    if(($auxRes->getId_children()!=null) && (is_numeric($auxRes->getId_children())) && ($namedChilds==2)) {
                        //discount only in second child
                        $priceForParent += ($auxRes->getNum_children() * ($pricePerChild*0.75));
                    } else {
                        $priceForParent += ($auxRes->getNum_children() * $pricePerChild);
                    } 
                        
                    $addonsForParent += ($auxRes->getNum_children() * $addonsPrice);
                    
                    
                }
              //  echo "there";
                $allAddonsPrice += $addonsForParent;
            
                //Generate parent payment here
                 $parentpayments = PaymentDAO::getPaymentListByParentPlaydate($auxReservation->getId_parent(), $auxReservation->getId_playdate());
                if($parentpayments!=null) {
                    foreach($parentpayments as $auxPay) {
                        if($auxPay->getStatus() == Payment::$STATUS_ROLLBACK) {
                            //Execute this pending payment
                            $newPayment = new Payment();
                            $newPayment->setId_parent($parent->getId_parent());
                            $newPayment->setId_playdate($auxReservation->getId_playdate());
                            $newPayment->setName($editPlaydate->getName());
                            $newPayment->setAmount_playdate($auxPay->getAmount_playdate());
                            $newPayment->setAmount_addons($auxPay->getAmount_addons());
                            $newPayment->setDiscount_amount($auxPay->getDiscount_amount());
                            $newPayment->setDiscount_code($auxPay->getDiscount_code());
                            PaymentDAO::createParentPayment($newPayment);
                        }
                    }
                }
            } 
            
            $payments = PaymentDAO::getPaymentListByPlaydate($editPlaydate->getId_playdate());
            if($payments!=null) {
                foreach($payments as $auxPay) {
                    if(($auxPay->getId_parent()!=null) && ($auxPay->getStatus()==Payment::$STATUS_PENDING)) {
                        PaymentDAO::executeParentPayment($auxPay->getId_payment());
                    }
                }
            }
            
            //SPECIALIST ORDERS
            $numKidsInPlaydate = $numReservations;
            $specialistBaseRate = 2500;
            
            if($minimumAge<5) {
                if($numKidsInPlaydate==2) {
                    $specialistBaseRate = 2500;
                } else if(($numKidsInPlaydate>=3) && ($numKidsInPlaydate<5)) {
                    $specialistBaseRate = 3000;
                } else if(($numKidsInPlaydate>=5) && ($numKidsInPlaydate<7)) {
                    $specialistBaseRate = 3200;
                } else if(($numKidsInPlaydate>=7) && ($numKidsInPlaydate<9)) {
                    $specialistBaseRate = 3400;
                } else if(($numKidsInPlaydate>=9) && ($numKidsInPlaydate<13)) {
                    $specialistBaseRate = 3500;
                } else if(($numKidsInPlaydate>=13) && ($numKidsInPlaydate<17)) {
                    $specialistBaseRate = 3600;
                } else if(($numKidsInPlaydate>=17) && ($numKidsInPlaydate<21)) {
                    $specialistBaseRate = 3800;
                } else if(($numKidsInPlaydate>=21)) {
                    $specialistBaseRate = 4000;
                } 
            } else if($minimumAge<8) {
                if($numKidsInPlaydate==2) {
                    $specialistBaseRate = 2500;
                }else if($numKidsInPlaydate==3) {
                    $specialistBaseRate = 3000;
                } else if(($numKidsInPlaydate>=4) && ($numKidsInPlaydate<7)) {
                    $specialistBaseRate = 3500;
                } else if(($numKidsInPlaydate>=7) && ($numKidsInPlaydate<9)) {
                    $specialistBaseRate = 3900;
                } else if(($numKidsInPlaydate>=9) && ($numKidsInPlaydate<16)) {
                    $specialistBaseRate = 4000;
                } else if(($numKidsInPlaydate>=16) && ($numKidsInPlaydate<21)) {
                    $specialistBaseRate = 4200;
                } else if(($numKidsInPlaydate>=21)) {
                    $specialistBaseRate = 4500;
                }
            } else {
                if($numKidsInPlaydate==2) {
                    $specialistBaseRate = 2500;
                }else if($numKidsInPlaydate==3) {
                    $specialistBaseRate = 3000;
                } else if(($numKidsInPlaydate>=4) && ($numKidsInPlaydate<7)) {
                    $specialistBaseRate = 3500;
                } else if(($numKidsInPlaydate>=7) && ($numKidsInPlaydate<10)) {
                    $specialistBaseRate = 3800;
                } else if(($numKidsInPlaydate>=10) && ($numKidsInPlaydate<13)) {
                    $specialistBaseRate = 4000;
                } else if(($numKidsInPlaydate>=13) && ($numKidsInPlaydate<16)) {
                    $specialistBaseRate = 4200;
                } else if(($numKidsInPlaydate>=16) && ($numKidsInPlaydate<19)) {
                    $specialistBaseRate = 4500;
                } else if(($numKidsInPlaydate>=19) && ($numKidsInPlaydate<22)) {
                    $specialistBaseRate = 4700;
                } else if(($numKidsInPlaydate>=22)) {
                    $specialistBaseRate = 5000;
                }
            }
            
            //$priceForSpecialist += ($numKidsInPlaydate * $specialistBaseRate * $numHours);
            $priceForSpecialist += ($specialistBaseRate * $numHours); //specialist price is not for child
            
            //Generate specialist payment here
            $newSpecPayment = new Payment();
            $newSpecPayment->setId_specialist($editPlaydate->getId_specialist());
            $newSpecPayment->setId_playdate($editPlaydate->getId_playdate());
            $newSpecPayment->setName($editPlaydate->getName());
            $newSpecPayment->setAmount_playdate($priceForSpecialist);
            //$newSpecPayment->setAmount_addons($editPlaydate->getAmount_addons());
            $newSpecPayment->setAmount_addons(0);
            
            PaymentDAO::createSpecialistPayment($newSpecPayment);
            
            
            //SUPPORT ORDERS
            //TODO - GET SUPPORTERS AND CALC PAYMENT TO SUPPORTERS
            
            //ok with message
            $successMessage = "Charge and Payment orders have been generated";
            
            //Update Playdate Num Supporters based in $minimumAge
            $numSupporters = 0;
            if($minimumAge<5) {
                if($numKidsInPlaydate>=5) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=9) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=13) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=17) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=21) {
                    $numSupporters++;
                }
            } else if($minimumAge<8) {
                if($numKidsInPlaydate>=6) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=11) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=16) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=21) {
                    $numSupporters++;
                }
            } else if($minimumAge>=8) {
                if($numKidsInPlaydate>=7) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=13) {
                    $numSupporters++;
                }
                if($numKidsInPlaydate>=18) {
                    $numSupporters++;
                }
            }
            
            PlaydateDAO::updatePlaydateMaxSupporters($editPlaydate->getId_playdate(), $numSupporters);
            
            
            //Update Playdate Status to RSVP
            PlaydateDAO::updatePlaydateStatus($editPlaydate->getId_playdate(), Playdate::$STATUS_RSVP);
            
            $editPlaydate = PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
            
            
            $menu_section = "playdates";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
        }
        break;
    }
    
    case "generate-support-payments": {
        
        //echo(":::::::::");die();
        //Generate all playdate support payments
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        if($editPlaydate!=null) {
            
            $numReservations = PlaydateDAO::countPlaydateReservations($editPlaydate->getId_playdate());
            $numHours = $editPlaydate->getTime_end()-$editPlaydate->getTime_init();
            
            $priceForSpecialist = 0;
            
            $minimumAge = null;
            
            $reservations = PlaydateDAO::getAllParentReservationByPlaydate($editPlaydate->getId_playdate());
            
            foreach($reservations as $auxReservation) {
                
                //Processing parent reservation
                $parent = ParentDAO::getParent($auxReservation->getId_parent());
                $childrenReservation = PlaydateDAO::getChildrenReservationsByReservation($auxReservation->getId_reservation());
                
                foreach($childrenReservation as $auxRes) {
                    if($minimumAge==null) {
                        $minimumAge = $auxRes->getAge();
                    } else if($auxRes->getAge()<$minimumAge) {
                        $minimumAge = $auxRes->getAge();
                    }
                }
            }
            
            //SUPPORTERS PAYMENT
            
            $supporters = SpecialistDAO::getSupportersByPlaydate($editPlaydate->getId_playdate());
            $numSupporters = 0;
            if($supporters!=null) {
                $numSupporters = count($supporters);
            }
            
            //echo(":::::[supporters: $numSupporters]::::");die();
            
            $numKidsInPlaydate = $numReservations;
            $specialistBaseRate = 0;
            
            if($minimumAge<5) {
                
                if($numSupporters==1) {
                    if($numKidsInPlaydate<5) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=5) && ($numKidsInPlaydate<13)) {
                        $specialistBaseRate = 2300;
                    } else if(($numKidsInPlaydate>=13) && ($numKidsInPlaydate<17)) {
                        $specialistBaseRate = 2400;
                    } else if(($numKidsInPlaydate>=17)) {
                        $specialistBaseRate = 2500;
                    }
                } else if($numSupporters==2) {
                    if($numKidsInPlaydate<9) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=9) && ($numKidsInPlaydate<18)) {
                        $specialistBaseRate = 2300;
                    } else if(($numKidsInPlaydate>=18)) {
                        $specialistBaseRate = 2500;
                    }
                } else if($numSupporters==3) {
                    if($numKidsInPlaydate<13) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=13)) {
                        $specialistBaseRate = 2300;
                    }
                } else if($numSupporters==4) {
                    if($numKidsInPlaydate<17) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=17)) {
                        $specialistBaseRate = 2300;
                    }
                } else if($numSupporters==5) {
                    if($numKidsInPlaydate<21) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=21)) {
                        $specialistBaseRate = 2300;
                    }
                }
                
            } else if($minimumAge<8) {
                
                if($numSupporters==1) {
                    if($numKidsInPlaydate<7) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=7) && ($numKidsInPlaydate<9)) {
                        $specialistBaseRate = 2300;
                    } else if(($numKidsInPlaydate>=9)) {
                        $specialistBaseRate = 2500;
                    }
                } else if($numSupporters==2) {
                    if($numKidsInPlaydate<11) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=11) && ($numKidsInPlaydate<16)) {
                        $specialistBaseRate = 2300;
                    } else if(($numKidsInPlaydate>=16)) {
                        $specialistBaseRate = 2500;
                    }
                } else if($numSupporters==3) {
                    if($numKidsInPlaydate<16) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=16)) {
                        $specialistBaseRate = 2300;
                    }
                } else if($numSupporters==4) {
                    if($numKidsInPlaydate<21) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=21)) {
                        $specialistBaseRate = 2300;
                    }
                }
                
            } else {
                
                if($numSupporters==1) {
                    if($numKidsInPlaydate<7) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=7) && ($numKidsInPlaydate<9)) {
                        $specialistBaseRate = 2300;
                    } else if(($numKidsInPlaydate>=9)) {
                        $specialistBaseRate = 2500;
                    }
                } else if($numSupporters==2) {
                    if($numKidsInPlaydate<13) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=13) && ($numKidsInPlaydate<16)) {
                        $specialistBaseRate = 2300;
                    } else if(($numKidsInPlaydate>=16)) {
                        $specialistBaseRate = 2500;
                    }
                } else if($numSupporters==3) {
                    if($numKidsInPlaydate<19) {
                        $specialistBaseRate = 0;
                    } else if(($numKidsInPlaydate>=19)) {
                        $specialistBaseRate = 2300;
                    }
                }
                
            }
            

            $priceForSpecialist += ($specialistBaseRate * $numHours); //support base payment * hours
            
            foreach ($supporters as $auxSupport) {
            
                //Generate specialist payment here
                $newSpecPayment = new Payment();
                $newSpecPayment->setId_specialist($auxSupport->getId_specialist());
                $newSpecPayment->setId_playdate($editPlaydate->getId_playdate());
                $newSpecPayment->setName("Support in ".$editPlaydate->getName());
                $newSpecPayment->setComment("Support in ".$editPlaydate->getName());
                $newSpecPayment->setAmount_playdate($priceForSpecialist);
                $newSpecPayment->setAmount_addons(0);
                
                PaymentDAO::createSpecialistPayment($newSpecPayment);
            }
            
            //ok with message
            $successMessage = "Support Payment orders have been generated";
            
            $editPlaydate = PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
            
            $menu_section = "playdates";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
        }
        break;
    }
    
    
    case "delete": {
        $menu_section = "playdates";
        $menu_option = "playdates-all";
        $main_section = "playdates/playdates-list.php"; //Section to load as main content
        
        //delete Playdate
        $delPlaydate = new Playdate();
        $delPlaydate->readFromRow($_REQUEST);
        $deleted = PlaydateDAO::deletePlaydate($delPlaydate);
        if($deleted) {
            //ok with message
            $successMessage = "Playdate has been deleted";

            $playdates = PlaydateDAO::getPlaydatesList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error deleting playdate. Please, try again";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-list.php"; //Section to load as main content
            
            $playdates = PlaydateDAO::getPlaydatesList();
        }
        
        break;
    }
    
    case "view": {
        //view parent
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        if(($editPlaydate!=null) && ($editPlaydate->getId_playdate()>0)) {
            $menu_section = "playdates";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
        } else {
            //playdate not found
            header("Location: /admin/playdates.php");
        }
        break;
    }
    
    case "edit": {
        //view parent
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        if(($editPlaydate!=null) && ($editPlaydate->getId_playdate()>0)) {
            $menu_section = "playdates";
            $main_section = "playdates/playdates-edit.php"; //Section to load as main content
        } else {
            //playdate not found
            header("Location: /admin/playdates.php");
        }
        break;
    }
    
    case "edit-full": {
        //view parent
        $editPlaydate = new Playdate();
        $editPlaydate->readFromRow($_REQUEST);
        
        if($editPlaydate->getId_playdate()>0) {
            $editPlaydate= PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
        }
        
        if(($editPlaydate!=null) && ($editPlaydate->getId_playdate()>0)) {
            $menu_section = "playdates";
            $main_section = "playdates/playdates-edit-full.php"; //Section to load as main content
        } else {
            //playdate not found
            header("Location: /admin/playdates.php");
        }
        break;
    }
    
    case "update-basic": {
        $updated = false;
        
        $menu_section = "playdates";
        $menu_option = "playdates-all";
        $main_section = "playdates/playdates-view.php"; //Section to load as main content
        
        //update playdate
        $updPlaydate = new Playdate();
        $updPlaydate->readFromRow($_REQUEST);
        
        if($updPlaydate->getId_playdate()>0) {
            $updPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate()); //read all playdate info
            
            $updPlaydate->readFromRow($_REQUEST); //change only this form basic info
            
            $updPlaydate = PlaydateDAO::updatePlaydateBasics($updPlaydate);
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Playdate has been updated";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating playdate. Please, try again";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
        }
        
        break;
    }
    
    case "update-full": {
        $updated = false;
        
        $menu_section = "playdates";
        $menu_option = "playdates-all";
        $main_section = "playdates/playdates-view.php"; //Section to load as main content
        
        //update playdate
        $updPlaydate = new Playdate();
        $updPlaydate->readFromRow($_REQUEST);
                   // echo "<pre>";print_r($_POST);exit;

        if($updPlaydate->getId_playdate()>0) {
            $updPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate()); //read all playdate info
            
            $initialStatus = $updPlaydate->getStatus();
            
            $updPlaydate->readFromRow($_REQUEST); //change only this form basic info
            
            //Addedby Priya
            if(isset( $_REQUEST['add1_id_check']) && $_REQUEST['add1_id_check'] == 'on'){
                $updPlaydate->setAdd1_id_check(1);
            }else{
                 $updPlaydate->setAdd1_id_check(0);
            }
            if(isset( $_REQUEST['add2_id_check']) && $_REQUEST['add2_id_check'] == 'on'){
                $updPlaydate->setAdd2_id_check(1);
            }
            else{
                 $updPlaydate->setAdd2_id_check(0);
            }
            if(isset( $_REQUEST['add3_id_check']) && $_REQUEST['add3_id_check'] == 'on'){
                $updPlaydate->setAdd3_id_check(1);
            }else{
                 $updPlaydate->setAdd3_id_check(0);
            }
            if(isset( $_REQUEST['add4_id_check']) && $_REQUEST['add4_id_check'] == 'on'){
                $updPlaydate->setAdd4_id_check(1);
            }else{
                 $updPlaydate->setAdd4_id_check(0);
            }
            
            $newStatus = $updPlaydate->getStatus();
            
            $updPlaydate = PlaydateDAO::updatePlaydate($updPlaydate);
            
            if(($initialStatus==Playdate::$STATUS_PUBLISHED) && ($newStatus==Playdate::$STATUS_DECLINED)) {
                //Playdate cancelled: time to send notifications and rollback parent payments (reservations)
                
                //Enviamos notificacion
                $pdTime =  Utils::dateFormat($updPlaydate->getDate())." ".Utils::get12hourFormat($updPlaydate->getTime_init())." - ".Utils::get12hourFormat($updPlaydate->getTime_end());
                $leadName = "No Lead";
                $leadMail = null;
                if($updPlaydate->getId_specialist()!=null) {
                    $pdSpecialist = SpecialistDAO::getSpecialist($updPlaydate->getId_specialist());
                    if($pdSpecialist!=null) {
                        $leadName = $pdSpecialist->getFullName();
                        $leadMail = $pdSpecialist->getEmail();
                    }
                }
                
                $playdate_address= $updPlaydate->getLoc_address().", ".$updPlaydate->getLoc_city().", ". $updPlaydate->getLoc_state();
                
                
                //Mail Booking Declined to lead
                if($leadMail!=null) {
                    MailUtils::sendPlaydateDeclined($leadMail, $leadName, $updPlaydate->getName(), $pdTime, $leadName, $playdate_address);
                }
                
                //Mail to all parents that have reserved (if any)
                $reservations = PlaydateDAO::getAllParentReservationByPlaydate($updPlaydate->getId_playdate());
                foreach ($reservations as $auxReservation) {
                    if($auxReservation->getStatus()==ParentReservation::$STATUS_RESERVED) { //only if not cancelled
                        $parentReserved = ParentDAO::getParent($auxReservation->getId_parent());
                        MailUtils::sendPlaydateDeclined($parentReserved->getEmail(), $parentReserved->getName(), $updPlaydate->getName(), $pdTime, $leadName, $playdate_address);
                    }
                }
                
                //Rollback parent payments
                $paymentsToRollBack = PaymentDAO::getPaymentListByPlaydate($updPlaydate->getId_playdate());
                foreach($paymentsToRollBack as $auxPaymentToRollback) {
                    if(($auxPaymentToRollback->getId_parent()!=null) && (($auxPaymentToRollback->getStatus()==Payment::$STATUS_PARTIAL)||($auxPaymentToRollback->getStatus()==Payment::$STATUS_COMPLETED))) {
                        //If payment is a parent payment and is partial or completed
                        PaymentDAO::rollbackParentPayment($auxPaymentToRollback->getId_payment(), "Playdate cancelled");
                    }

                }//if playdate was published and now has to be declined
                //DONE!
                
            }
            
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Playdate has been updated";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating playdate. Please, try again";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-view.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
        }
        
        break;
    }
    
    case "add-support": {
        $updated = false;
        
        $menu_section = "playdates";
        $menu_option = "playdates-all";
        $main_section = "playdates/playdates-edit-full.php"; //Section to load as main content
        
        //update playdate
        $updPlaydate = new Playdate();
        $updPlaydate->readFromRow($_REQUEST);
        
        if($updPlaydate->getId_playdate()>0) {
            $specialist = new Specialist();
            $specialist->readFromRow($_REQUEST);
            $specialist = SpecialistDAO::getSpecialist($specialist->getId_specialist());
            if($specialist!=null) {
                SpecialistDAO::createPlaydateSupporter($updPlaydate->getId_playdate(), $specialist->getId_specialist());
                $updated = true;
            } 
            
            
        }
        if($updated) {
            //ok with message
            $successMessage = "Playdate has a new support";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-edit-full.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error adding support. Please, try again";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-edit-full.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
        }
        
        break;
    }
    
    case "remove-support": {
        $updated = false;
        
        $menu_section = "playdates";
        $menu_option = "playdates-all";
        $main_section = "playdates/playdates-edit-full.php"; //Section to load as main content
        
        //update playdate
        $updPlaydate = new Playdate();
        $updPlaydate->readFromRow($_REQUEST);
        
        if($updPlaydate->getId_playdate()>0) {
            $specialist = new Specialist();
            $specialist->readFromRow($_REQUEST);
            $specialist = SpecialistDAO::getSpecialist($specialist->getId_specialist());
            if($specialist!=null) {
                SpecialistDAO::deletePlaydateSupporter($updPlaydate->getId_playdate(), $specialist->getId_specialist());
                $updated = true;
            }
            
            
        }
        if($updated) {
            //ok with message
            $successMessage = "Support has been removed";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-edit-full.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
            
        } else {
            //error - go with error message
            $errorMessage = "Error removing support. Please, try again";
            
            $menu_section = "playdates";
            $menu_option = "playdates-all";
            $main_section = "playdates/playdates-edit-full.php"; //Section to load as main content
            
            $editPlaydate = PlaydateDAO::getPlaydate($updPlaydate->getId_playdate());
        }
        
        break;
    }
    /*
    case "updateavailability": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update administrator
        $updParent = new ParentPd();
        $updParent->readFromRow($_REQUEST);
        
        if($updParent ->getId_parent()>0) {
            $updParent = ParentDAO::getParent($updParent->getId_parent()); //read all parent info
            $updParent ->readFromRow($_REQUEST); //change only this form parent info
            $updParent = ParentDAO::updateParent($updParent);
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Parent availability has been updated";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating parent availability. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "updatechildren": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update children
        $updChildren = new Children();
        $updChildren->readFromRow($_REQUEST);
        
        if($updChildren->getId_children()>0) {
            $updChildren = ChildrenDAO::getChildren($updChildren->getId_children()); //read all children info
            $updChildren->readFromRow($_REQUEST); //change only this form info
            $updChildren = ChildrenDAO::updateChildren($updChildren);
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Children has been updated";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating children info. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    
    case "updateemergency": {
        $updated = false;
        
        $menu_section = "parents";
        $menu_option = "parents-all";
        $main_section = "parents/parent-list.php"; //Section to load as main content
        
        //update emergency
        $updEmergency = new Emergency();
        $updEmergency->readFromRow($_REQUEST);
        
        if($updEmergency->getId_emergency()>0) {
            $updEmergency = EmergencyDAO::getEmergency($updEmergency->getId_emergency()); //read all info
            $updEmergency->readFromRow($_REQUEST); //change only this form info
            $updEmergency = EmergencyDAO::updateEmergency($updEmergency);
            $updated = true;
        }
        if($updated) {
            //ok with message
            $successMessage = "Emergency contact has been updated";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
            
        } else {
            //error - go to list, with error message
            $errorMessage = "Error updating emergency contact. Please, try again";
            
            $menu_section = "parents";
            $menu_option = "parents-all";
            $main_section = "parents/parent-list.php"; //Section to load as main content
            
            $parents = ParentDAO::getParentsList();
        }
        
        break;
    }
    */
    default: {
        
        $menu_section = "playdates";
        $menu_option = "playdates-all";
        $main_section = "playdates/playdates-list.php"; //Section to load as main content
        
        $playdates = PlaydateDAO::getPlaydatesList();
        
        break;
    }
}



include("template.php");
?>
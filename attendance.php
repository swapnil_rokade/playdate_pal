<?php 
/**
 * PLAYDATE - ATTENDANCE PDF DOCUMENT DOWNLOAD 
 */
require_once('vendor/autoload.php');


include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isSpecialist) {
    header("Location: /");
}

$specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);

$specProfile = "Lead";
if($specialist->getProfile()==Specialist::$PROFILE_LEAD) {
    $specProfile = "Lead"; 
} else if($specialist->getProfile()==Specialist::$PROFILE_SUPPORTER) {
    $specProfile = "Supporter";
} else if($specialist->getProfile()==Specialist::$PROFILE_NURSE) {
    $specProfile = "Nurse";
}

//Selected Playdate to show in detail
$selectedPastPlaydate = null;
$selectedPlaydate = null;
$paramPlaydate = new Playdate(); 
$paramPlaydate->readFromRow($_REQUEST); 
if($paramPlaydate->getId_playdate()>0) { 
    $paramPlaydate = PlaydateDAO::getPlaydate($paramPlaydate->getId_playdate());
    
    if(($paramPlaydate!=null) && ($paramPlaydate->getId_playdate()>0)) {
        $selectedPlaydate = $paramPlaydate;
    }
}

if(($selectedPlaydate!=null) && ($specialist!=null) && ($specialist->getId_specialist()==$selectedPlaydate->getId_specialist())) {
    $mpdf = new \mPDF();
    
    $mpdf->WriteHTML("<h1>".$selectedPlaydate->getName()."</h1>");
    $mpdf->WriteHTML("<h2>".Utils::dateFormat($selectedPlaydate->getDate())." | ".Utils::get12hourFormat($selectedPlaydate->getTime_init())." - ".Utils::get12hourFormat($selectedPlaydate->getTime_end())."</h2>");
    $mpdf->WriteHTML("<p>".$selectedPlaydate->getLoc_address()."<br/>".$selectedPlaydate->getLoc_city().", ".$selectedPlaydate->getLoc_state()."</p>");
    $mpdf->WriteHTML('<h2>Playdate Attendance</h2>');
    
    
    $parents = ParentDAO::getParentsListByPlaydate($selectedPlaydate->getId_playdate(), 1, 10, null, null);
    $mpdf->WriteHTML("<hr>");
    foreach ($parents as $auxParent) {
        $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPlaydate->getId_playdate());
        $childrenReservation = PlaydateDAO::getChildrenReservationsByReservation($reservation->getId_reservation());
        $numSpots = 0;
        foreach($childrenReservation as $auxChildren) {
            $numSpots += $auxChildren->getNum_children();
        }
        
        $mpdf->WriteHTML("<h2>Parent: ".$auxParent->getFullName()."</h2>");
        $mpdf->WriteHTML("<p><b>Email</b>: ".$auxParent->getEmail().". <b>Phone(s)</b>: ".$auxParent->getPhone()." | ".$auxParent->getCellphone()."</p>");
        $mpdf->WriteHTML("<table id=\"playpack-credit\" class=\"table table-striped\" style=\"width:100%\">");
        $mpdf->WriteHTML("<thead><tr><th>Child Name</th><th>Num. of Kids</th><th>Age</th><th>Notes</th></tr></thead><tbody>");
        foreach($childrenReservation as $auxChildren) {
            $childName = "Extra-children";
            $childNotes = "";
            if($auxChildren->getId_children()!=null) {
                $child = ChildrenDAO::getChildren($auxChildren->getId_children());
                $childName = $child->getName();
                $childNotes = $child->getNotes();
            }
            $mpdf->WriteHTML("<tr><td>".$childName."</td><td>".$auxChildren->getNum_children()."</td><td>".$auxChildren->getAge()."</td><td>".$childNotes."</td></tr>");
        }
        $mpdf->WriteHTML("</tbody></table><br />");
        
        $mpdf->WriteHTML("<table id=\"playpack-credit\" class=\"table table-striped\" style=\"width:100%\"><thead><tr><th>Emergency Contact</th><th>Email</th><th>Phone</th><th>Relationship</th></tr></thead><tbody>");
        $emergency = EmergencyDAO::getEmergencyListByParent($auxParent->getId_parent());
        foreach($emergency as $auxEmer) {
            $mpdf->WriteHTML("<tr><td>".$auxEmer->getName()."</td><td>".$auxEmer->getMail()."</td><td>".$auxEmer->getPhone()."</td><td>".$auxEmer->getRelation()."</td></tr>");
        }
        $mpdf->WriteHTML("</tbody></table>");
        $mpdf->WriteHTML("<hr>");
    }
    
    $supporters = SpecialistDAO::getSupportersByPlaydate($selectedPlaydate->getId_playdate());
    if(($supporters!=null) && (count($supporters)>0)) {
        $mpdf->WriteHTML('<h2>Supporters</h2>');
        $mpdf->WriteHTML("<table id=\"playpack-credit\" class=\"table table-striped\" style=\"width:100%\"><thead><tr><th>Name</th><th>Email</th><th>Phone</th></tr></thead><tbody>");
        foreach($supporters as $auxSupport) {
            $mpdf->WriteHTML("<tr><td>".$auxSupport->getFullName()."</td><td>".$auxSupport->getEmail()."</td><td>".$auxSupport->getPhone()."</td></tr>");
        }
        $mpdf->WriteHTML("</tbody></table>");
    }
    
    $mpdf->Output();
} else {
    header("Location: /");
}
?>
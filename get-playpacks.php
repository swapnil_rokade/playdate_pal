<?php 
/**
 * PLAYDATE - GET PLAYPACKS SECTION
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");
if((!isset($isParent)) || (!$isParent)) {
    header("Location: /");
}
//Parent connected. Get full info
$parent = ParentDAO::getParent($_SESSION["parent_id"]);

$backUrl = "/get-playpacks.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head> 

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content=""> 
    <meta name="author" content="">

    <title>Get Playpacks - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Get Playpacks</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">




<!-- Featured playdates -->
      <div class="row featured-playdates get-playpacks">
        <div class="col-lg-3">
          <div class="titulo">
            <h2 class="budget">$<?php echo Utils::moneyFormat($parent->getCredit()) ?></h2>
            <p>Your available credit</p>
          </div>
        </div>
        <div class="col-lg-9">
        <form action="/checkout.php" method="post">
          <input type="hidden" name="description" value="Manual amount selection" />
          <div class="row intro-playpacks  row-eq-height">
            <div class="col-lg-4 vcenter">
              <p>Enter the highest possible price for your playdate. If the price drops the remaining credits will be available for you to use on your next playdate. </p>
            </div>
              <div class="col-lg-4 vcenter">
                <input type="number" name="amount" placeholder="$0" id="playpacks" required="required">
              </div>
              <div class="col-lg-4 vcenter">
                <button class="btn btn-md btn-primary btn-block" type="submit">Purchase</button>
              </div>
          </div>
        </form>
        <div class="row row-eq-height row-playpacks">
            <div class="col-lg-12">
              <p class="title">Playpacks for One Child</p>
            </div>
            <?php 
            $playpacks = PlaypackDAO::getPlaypackListByType(Playpack::$TYPE_1CHILD);
            foreach ($playpacks as $auxPlaypack) {
                $hasHelp = (($auxPlaypack->getHelp()!=null) && ($auxPlaypack->getHelp()!=""));
            ?>
            <div class="col-lg-4 col-md-6 vcenter">
              <div title="" class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $auxPlaypack->getIcon() ?>" alt=""/>
                </div>
                <p>$<?php echo $auxPlaypack->getAmount() ?></p>
                <p class="legal"><?php echo $auxPlaypack->getDescription() ?>
                 <?php if($hasHelp) { ?>
                <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="<?php echo $auxPlaypack->getHelp() ?>"><i class="fa fa-question-circle"></i></a>
                <?php } ?>
                </p>
                <div class="reserve">
                	<form action="/checkout.php" method="post">
            			<input type="hidden" name="amount" value="<?php echo $auxPlaypack->getAmount() ?>" />
            			<input type="hidden" name="description" value="<?php echo $auxPlaypack->getName() ?>" />
                  	<button class="btn btn-md btn-primary btn-block" type="submit">Purchase</button>
                  	</form>
                </div>
              </div>
            </div>
			
			<?php } ?>
           
          
        </div>
        <div class="row row-eq-height row-playpacks">
            <div class="col-lg-12">
              <p class="title">Playpacks for Two Children</p>
            </div>
            <?php 
            $playpacks = PlaypackDAO::getPlaypackListByType(Playpack::$TYPE_2CHILD);
            foreach ($playpacks as $auxPlaypack) {
                $hasHelp = (($auxPlaypack->getHelp()!=null) && ($auxPlaypack->getHelp()!=""));
            ?>
            
            <div class="col-lg-4 col-md-6 vcenter">
              <div title="" class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $auxPlaypack->getIcon() ?>" alt=""/>
                </div>
                <p>$<?php echo $auxPlaypack->getAmount() ?></p>
                <p class="legal"><?php echo $auxPlaypack->getDescription() ?>
                 <?php if($hasHelp) { ?>
                <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="<?php echo $auxPlaypack->getHelp() ?>"><i class="fa fa-question-circle"></i></a>
                <?php } ?>
                </p>
                <div class="reserve">
					<form action="/checkout.php" method="post">
            			<input type="hidden" name="amount" value="<?php echo $auxPlaypack->getAmount() ?>" />
            			<input type="hidden" name="description" value="<?php echo $auxPlaypack->getName() ?>" />
                  		<button class="btn btn-md btn-primary btn-block" type="submit">Purchase</button>
                  	</form>                  
                </div>
              </div>
            </div>

			<?php } ?>
          
        </div>
        <div class="row get-playpacks-info">

        <div class="col-lg-12">

          <button class="btn btn-md btn-primary btn-block" type="submit">Purchase history</button>

          <div class="terms">Credits are non-refundable. Playpacks based on signature 3-hour playdates , not including "Just In Time" rates or optional add-on fees (i.e. travel, admissions and additional materials). Visit  <a href="terms-of-service.php" title="Terms and Conditions">Terms and Conditions</a> for more information.</div>
          
        </div>
      </div>
      </div>
    </div>

      



    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

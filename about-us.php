<?php 
/**
 * PLAYDATE - ABOUT US SECTION
 */

include_once("classes/all_classes.php");

include_once("common.php");

include_once("connection.php");

$backUrl = "/about-us.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>About Us - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>About us</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">



    <!-- Featured Specialists -->
    <div class="row intro-row">
        
        <div class="col-lg-6 texto vcenter">
          <h2>PAL is pioneering social care.</h2>
          <p>Social care is a curated child care experience for two or more children facilitated by professional caregivers. Social care promises to always be safe, social, and stimulating without breaking the bank.  </p>
        </div>
        <div class="col-lg-6 imagen">
          <img src="img/img-intro-1.jpg" alt="">
        </div>
    </div>

<!-- How to -->
    <div class="row row-eq-height how-to about-info">
        <div class="col-lg-6 vtop">
          <div class="content-title">
            <h2 class="title-in"><span>&nbsp;</span>Playdates</h2>
            <div class="texto">
              <p><strong>Social care experiences that always include an activity or adventure.</strong></p>
              <p><strong>Playdates can be one-off or recurring, public or private, and with or without parents.</strong></p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 vtop">
          <div class="content-title">
            <h2 class="title-in"><span>Coming Soon</span>Care Shares</h2>
            <div class="texto">
             <p><strong>Consistent social care with structured enrichment.</strong></p>
             <p><strong>Split by up to four parents on the same set schedule.</strong></p>
             <p><strong>Registered nurse care for up to two infants.</strong></p>
            </div>
          </div>
        </div>
    </div>

<!-- Featured playdates -->
      <div class="row featured-playdates mission">
         <div class="col-lg-12">
          <div class="titulo">
            <h2>The PAL Vision</h2>
            <p>The PAL Vision. If we want to see a new generation of people who are empathetic, resilient, and socially-conscious, then we need to start, when they are young, creating space for children to have meaningful social connections. We can only accomplish this with positive role modeling from caregivers that have strong values and are talented enough to inspire kindness, curiosity and a healthy emotional well-being.</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12">
          <div class="box-playdates">
            <div class="img-container">
              <img src="img/img-safety.jpg"/ alt="">
              <span class="cat-mision">Safe</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12">
          <div class="box-playdates">
            <div class="img-container">
              <img src="img/img-affordability.jpg"/ alt="">
              <span class="cat-mision">Affordable</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12">
          <div class="box-playdates last">
            <div class="img-container">
              <img src="img/img-social.jpg"/ alt="">
              <span class="cat-mision">Social</span>
            </div>
          </div>
        </div>
        
        <div class="col-lg-3 col-md-6 col-xs-12">
          <div class="box-playdates last">
            <div class="img-container">
              <img src="img/img-stimulating.jpg"/ alt="">
              <span class="cat-mision">Stimulating</span>
            </div>
          </div>
        </div>
      </div>

      <!-- Social -->
      <div class="row row-eq-height how-to safety">
          <div class="col-lg-12 vtop">
            <div class="content-title">
              <h2 class="title-in">Safe
                <span class="subtitle">Top Quality Care</span>
              </h2>
              <div class="row texto">
              <p><strong>PAL is a marketplace of well-paid and meaningful job opportunities for the best people in child care. With a group care model, we can achieve higher quality care at a lower price point for Parents.</strong></p>
              <div class="measures">
                <p><strong>Playdates Maintain These Safety Measures:</strong></p>
                <ul>
                  <li class="ico1"><span>Comprehensive Vetting Process</span></li>
                  <li class="ico2"><span>Professionally Trained and CPR/ First Aid Certified Lead Specialists </span></li>
                  <li class="ico3"><span>Age Appropriate Adult to Child Ratios</span></li>
                  <li class="ico4"><span>Structured Itineraries for Every Playdate Experience </span></li>
                  <li class="ico5"><span>Experience Options for Younger Children</span></li>
                  <li class="ico6"><span>Performance Evaluations Based on Ratings and Reviews</span></li>
                </ul>
                
              </div>
                
              </div>
            </div>
          </div>
          <div class="col-lg-12 titulo-container">
            <div class="titulo">
              <h2>Suggested Age Group Ratio We Follow</h2>
              <div class="row ranges-container">
                <div class="col-lg-3">
                  <div class="row row-eq-height ">
                    <div class="col-lg-3 vtop specialist-container">
                      <div class="img-container">
                        <img src="img/ico/white/0296-woman.svg" alt=""/>
                        <img class="aditional" src="img/ico/white/0296-woman.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-9 vtop baby-container">
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-12 info">
                      <p class="tit">Under 3</p>
                      <p class="texto">Nurse Care without Parents, coming soon!</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="row row-eq-height ">
                    <div class="col-lg-3 vtop specialist-container">
                      <div class="img-container">
                        <img src="img/ico/white/0296-woman.svg" alt=""/>
                        <img class="aditional" src="img/ico/white/0296-woman.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-9 vtop baby-container">
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-12 info">
                      <p class="tit">3-4 yrs</p>
                      <p class="texto">4 children to 1 adult</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="row row-eq-height ">
                    <div class="col-lg-3 vtop specialist-container">
                      <div class="img-container">
                        <img src="img/ico/white/0296-woman.svg" alt=""/>
                        <img class="aditional" src="img/ico/white/0296-woman.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-9 vtop baby-container">
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-12 info">
                      <p class="tit">5-7 yrs</p>
                      <p class="texto">5 children to 1 adult</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="row row-eq-height ">
                    <div class="col-lg-3 vtop specialist-container">
                      <div class="img-container">
                        <img src="img/ico/white/0296-woman.svg" alt=""/>
                        <img class="aditional" src="img/ico/white/0296-woman.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-9 vtop baby-container">
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                      <div class="img-container">
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                        <img src="img/ico/white/0299-baby2.svg" alt=""/>
                      </div>
                    </div>
                    <div class="col-lg-12 info">
                      <p class="tit">+7 or Under 3 Parent Social</p>
                      <p class="texto">6 children to 1 adult</p>
                    </div>
                  </div>
                </div>
              </div>
              <p class="add-specialist">Additional Support Specialists can be hired with an add-on fee. PAL suggests an extra Specialist for children under 7 for playdates at public attractions.</p>
            </div>
          </div>

      </div>

      <!-- Affordability -->
      <div class="row row-eq-height how-to affordability">
          <div class="col-lg-12 vtop">
            <div class="content-title">
              <h2 class="title-in">Affordable
                <span class="subtitle">Shared Pricing lowers the cost of care</span>
              </h2>
              <div class="row texto">
                <p><strong>Use the PAL Calculator to check rates.</strong></p>               
              </div>
              <div class="row texto calculator">
                  <div class="col-lg-3 col-md-6">
                      <input type="text" name="" placeholder="3" id="numKids"> 
                      <label for="numKids">Numbers of kids<span>25% off second child</span></label>
                  </div>  
                  <div class="col-lg-3 col-md-6">
                      <input type="text" name="" placeholder="5" id="hours"> 
                      <label for="hours">Hours</label>
                  </div>  
                  <div class="col-lg-3 col-md-6">
                      <input type="text" name="" placeholder="$18" id="baseRate"> 
                      <label for="baseRate">Hourly Rate</label>
                  </div>  
                   <div class="col-lg-3 col-md-6">
                      <input type="text" name="" placeholder="$100"  id="youPay"> 
                      <label for="youPay">You Pay</label>
                  </div>
                  <?php /*?> 
                  <div class="col-lg-12 legal">
                    <p>*$25 per hour flat rate after RSVP date has passed.</p>
                  </div>  
                  */?>             
              </div>
            </div>
          </div>
          
      </div>

      <!-- Social -->
      <div class="row row-eq-height how-to social">
          <div class="col-lg-12 vtop">
            <div class="content-title">
              <h2 class="title-in">Social
                <span class="subtitle">Life’s Always Better with Friends</span>
              </h2>
              <div class="row row-eq-height texto collage">
                  <div class="col-lg-4 vcenter">
                      <p>2 + kids at each playdate</p>
                  </div>  
                  <div class="col-lg-4 vcenter withBorder">
                      <img src="img/img-social1.jpg" alt=""/>
                  </div>  
                  <div class="col-lg-4 vcenter">
                      <p>Facilitated group play</p>
                  </div>          
              </div>
              <div class="row row-eq-height texto collage">
                  <div class="col-lg-4 vcenter">
                    <img src="img/img-social2.jpg" alt=""/>                      
                  </div>  
                  <div class="col-lg-4 vcenter withBorder">
                      <p>Social skill building</p>
                  </div>  
                  <div class="col-lg-4 vcenter">
                    <img src="img/img-social3.jpg" alt=""/>
                  </div>          
              </div>
            </div>
          </div>
          
      </div>

      <!-- Stimulating -->
      <div class="row row-eq-height how-to stimulating">
          <div class="col-lg-12 vtop">
            <div class="content-title">
              <h2 class="title-in">Stimulating
                <span class="subtitle">We provide meaningful and well-paid job opportunities for the most talented Specialists in the child care industry.</span>
              </h2>
              <div class="row row-eq-height texto collage">
                  <div class="col-lg-4 vcenter">
                      <p>Structured playdate itineraries</p>
                  </div>  
                  <div class="col-lg-4 vcenter withBorder">
                      <img src="img/img-stimulating1.jpg" alt=""/>
                  </div>  
                  <div class="col-lg-4 vcenter">
                      <p>Local adventures and cultural excursions </p>
                  </div>          
              </div>
              <div class="row row-eq-height texto collage">
                  <div class="col-lg-4 vcenter">
                    <img src="img/img-stimulating2.jpg" alt=""/>                      
                  </div>  
                  <div class="col-lg-4 vcenter withBorder">
                      <p>Activity-based learning</p>
                  </div>  
                  <div class="col-lg-4 vcenter">
                    <img src="img/img-stimulating3.jpg" alt=""/>
                  </div>          
              </div>
            </div>
          </div>
          
      </div>

    </div>

    <!-- TESTIMONIALS -->
    <div class="row playdates-testimonials">
        <div class="col-lg-12">
          <div class="titulo">
            <div class="testimonio">
              <p class="texto">The kids loved it! We moved to NY in April and finding kids for them to play with has been not easy. This was exactly what they needed. The caregivers were outstanding. We have had trouble finding someone our kids were comfortable staying with. My husband and I were thrilled that both kids ran off to have fun and didn't even look back. We will definitely be back and will certainly recommend and support your program anyway that we can.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">My husband and I are thrilled. Our son had a great time. We had a great time - guilt free! This is just an all around a win-win situation.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">This is awesome! Your service was unparalleled by anything else we've experienced.</p>
              <p class="name">Maryam, Playdate Partner Host</p>
            </div>
            <div class="testimonio">
              <p class="texto">Project Playdate is a wonderful service. My husband and I were so grateful to have a night out and to know that our son was in good hands. Now how do we get Project Playdate to come to California?!</p>
              <p class="name">Shenna Deveza, Mom of One, Union Square</p>
            </div>
             <ul class="nav-testimonios"></ul>
          </div>

        </div>
    </div>
    <!-- FIN TESTIMONIALS-->



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

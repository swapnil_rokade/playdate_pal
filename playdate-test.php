<?php
/**
 * PLAYDATE - TEST PLAYDATE DETAILS
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");

$playdate=new Playdate();
$playdate->readFromRow($_REQUEST);
if($playdate->getId_playdate()>0) {
    $playdate= PlaydateDAO::getPlaydate($playdate->getId_playdate());
}

if(($playdate==null) || ($playdate->getId_playdate()<=0)) {
    //Playdate not found - Home
    header("Location: /");
}

$parentcredit=0;
$priceperchild=PlaydatePriceDAO::getPlaydatePrice($playdate->getId_playdate());
$discountsecondchild = ($priceperchild*0.25);
//$priceperchild=PlaydatePriceDAO::getMaxPlaydatePrice($playdate->getId_playdate());
//add addons price
$addonsPrice = 0;
if($playdate->getAdd1_price()!=null) {
    $addonsPrice += $playdate->getAdd1_price()*100;
}
if($playdate->getAdd2_price()!=null) {
    $addonsPrice += $playdate->getAdd2_price()*100;
}
if($playdate->getAdd3_price()!=null) {
    $addonsPrice += $playdate->getAdd3_price()*100;
}
if($playdate->getAdd4_price()!=null) {
    $addonsPrice += $playdate->getAdd4_price()*100;
}
$priceperchild += $addonsPrice;

//Parent connected. Get full info
$parent = null;
if(isset($_SESSION["parent_id"])) {
    $parent = ParentDAO::getParent($_SESSION["parent_id"]);
    $parentcredit=$parent->getCredit();
}

$specialist = null;
if($playdate->getId_specialist()!=null) {
    $specialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
}

$sessionSpecialist = null;
if(isset($_SESSION["spec_id"])) {
    $sessionSpecialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
}


$reserved = false;
$accepted = false;
$declined = false;
$invited = false;

$maxNumSuporters = 100;
$numSuportersSelected = 0;
$supporters = SpecialistDAO::getSupportersByPlaydate($playdate->getId_playdate());
if($supporters!=null) {
    $numSuportersSelected = count($supporters);
}

//BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
if(($playdate->getId_request()!=null) && is_numeric($playdate->getId_request())) {
    $minAge = 100;
    $maxAge = -1;
    $reservations = PlaydateDAO::getChildrenReservations($playdate->getId_playdate());
    foreach ($reservations as $auxReserv) {
        if($auxReserv->getAge()>$maxAge) {
            $maxAge = $auxReserv->getAge();
        }
        
        if($auxReserv->getAge()<$minAge) {
            $minAge = $auxReserv->getAge();
        }
    }
    
    
    if(($maxAge>=0) && ($minAge<100)) {
        $playdate->setAge_init($minAge);
        $playdate->setAge_end($maxAge);
    }
    
}

$sent = false;
$applied = false;

$isCheckout = false;

if(isset($_REQUEST["action"]) && ($_REQUEST["action"]!=null)) {
    
    switch ($_REQUEST["action"]) {
        
        case "checkout": {
             $isCheckout = true;
             break;
        }
        
        case "apply-support": {
            SpecialistDAO::requestPlaydateSupporter($playdate->getId_playdate(), $sessionSpecialist->getId_specialist());
            //Send mail to lead?
            
            $supporterName = $sessionSpecialist->getFullName();
            
            
            //Enviamos notificacion al supporter aceptado
            $pdTime =  Utils::dateFormat($playdate->getDate())." ".Utils::get12hourFormat($playdate->getTime_init())." - ".Utils::get12hourFormat($playdate->getTime_end());
            $leadName = "No Lead";
            $leadMail = null;
            if($playdate->getId_specialist()!=null) {
                $pdSpecialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
                if($pdSpecialist!=null) {
                    $leadName = $pdSpecialist->getFullName();
                    $leadMail = $pdSpecialist->getEmail();
                }
            }
            
            $playdate_address= $playdate->getLoc_address().", ".$playdate->getLoc_city().", ". $playdate->getLoc_state();
            $playdate_pickup = "";
            if($playdate->getPickup_address()!=null) {
                $playdate_pickup = $playdate->getPickup_address();
            }
            $playdate_dropoff="";
            if($playdate->getDropoff_address()!=null) {
                $playdate_dropoff = $playdate->getDropoff_address();
            }
            
            MailUtils::sendPlaydateSupportApplied($leadMail, $leadName, $supporterName, $playdate->getName(), $pdTime, $playdate_address, $playdate_pickup, $playdate_dropoff, "playdate.php?id_playdate=".$playdate->getId_playdate());
            
            
            break;
        }
        
        
        case "accept-support": {
            $specSupporter = new Specialist();
            $specSupporter->readFromRow($_REQUEST);
            $specSupporter = SpecialistDAO::getSpecialist($specSupporter->getId_specialist());
            
            if($specSupporter!=null) {
                SpecialistDAO::acceptPlaydateSupporter($playdate->getId_playdate(), $specSupporter->getId_specialist());
                
                
                
                //Enviamos notificacion al supporter aceptado
                $pdTime =  Utils::dateFormat($playdate->getDate())." ".Utils::get12hourFormat($playdate->getTime_init())." - ".Utils::get12hourFormat($playdate->getTime_end());
                $leadName = "No Lead";
                $leadMail = null;
                if($playdate->getId_specialist()!=null) {
                    $pdSpecialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
                    if($pdSpecialist!=null) {
                        $leadName = $pdSpecialist->getFullName();
                        $leadMail = $pdSpecialist->getEmail();
                    }
                }
                
                $playdate_address= $playdate->getLoc_address().", ".$playdate->getLoc_city().", ". $playdate->getLoc_state();
                $playdate_pickup = "";
                if($playdate->getPickup_address()!=null) {
                    $playdate_pickup = $playdate->getPickup_address();
                }
                $playdate_dropoff="";
                if($playdate->getDropoff_address()!=null) {
                    $playdate_dropoff = $playdate->getDropoff_address();
                }
                
                MailUtils::sendPlaydateSupportConfirmation($specSupporter->getEmail(), $specSupporter->getName(), $playdate->getName(), $pdTime, $playdate_address, $playdate_pickup, $playdate_dropoff, "playdate.php?id_playdate=".$playdate->getId_playdate());
            }
            
            //Recalc supporters
            $supporters = SpecialistDAO::getSupportersByPlaydate($playdate->getId_playdate());
            if($supporters!=null) {
                $numSuportersSelected = count($supporters);
            }
            
            break;
        }
        
        case "decline-support": {
            $specSupporter = new Specialist();
            $specSupporter->readFromRow($_REQUEST);
            $specSupporter = SpecialistDAO::getSpecialist($specSupporter->getId_specialist());
            
            if($specSupporter!=null) {
                SpecialistDAO::declinePlaydateSupporter($playdate->getId_playdate(), $specSupporter->getId_specialist());
            }
            
            //Recalc supporters
            $supporters = SpecialistDAO::getSupportersByPlaydate($playdate->getId_playdate());
            if($supporters!=null) {
                $numSuportersSelected = count($supporters);
            }
            
            break;
        }
        
        
        case "addgroupspecialist": {
            if(($_REQUEST["name"]!=null) && ($_REQUEST["name"]!="") && ($_REQUEST["id_specialist"]!="")) {
                $newgroup = GroupDAO::getGroupByName($_REQUEST["name"]);
                $newspec = new Specialist();
                $newspec->readFromRow($_REQUEST);
                if($newspec->getId_specialist()>0) {
                    $newspec = SpecialistDAO::getSpecialist($newspec->getId_specialist());
                }
                
                if(($newgroup!=null) && ($newgroup->getId_group()>0) && ($newspec!=null) && ($newspec->getId_specialist()>0)) {
                    if($newgroup->getPrivacity()== Group::$PRIVACITY_PUBLIC) {
                        if(GroupDAO::isSpecialistInGroup($newspec->getId_specialist(), $newgroup->getPrivacity())) {
                            //If specialist is already in the group, don't send notification - don't create invitation
                            
                        } else {
                            
                            GroupDAO::createGroupSpecialistInvitation($newgroup->getId_group(), $newspec->getId_specialist());
                        }
                    }
                }
            }
            
            break;
        }
        case "invite-friends": {
            if(!empty($_POST['inviteFriendsPopup'])) {
                $name="Someone";
                if($parent!=null) {
                    $name = $parent->getName();
                } else if($specialist!=null) {
                    $name = $specialist->getName();
                }
                
                $event_place = $playdate->getLoc_address().", ".$playdate->getLoc_city().", ".$playdate->getLoc_state();
                
                foreach($_POST['inviteFriendsPopup'] as $auxfriend) {
                    $friend = ParentDAO::getParentByUsernameOrEmail($auxfriend);
                    if($friend!=null) {
                        MailUtils::sendPlaydateInviteFriend($friend->getEmail(), $auxfriend, $name, $playdate->getName(), Utils::dateFormat($playdate->getDate()), $event_place, Utils::get12hourFormat($playdate->getTime_init()), "playdate.php?id_playdate=".$playdate->getId_playdate());
                    } else if(filter_var($auxfriend, FILTER_VALIDATE_EMAIL)) {
                        MailUtils::sendPlaydateInviteFriend($auxfriend, $auxfriend, $name, $playdate->getName(), Utils::dateFormat($playdate->getDate()), $event_place, Utils::get12hourFormat($playdate->getTime_init()), "playdate.php?id_playdate=".$playdate->getId_playdate());
                    }
                    
                }
                $invited = true;
            }
            
            
            break;
        }
        
        case "cancel": {
            //Cancel reservation
            //Refund reservation, mark as cancelled, payment of cancellation
            $reservation = PlaydateDAO::getParentReservationByPlaydate($parent->getId_parent(), $playdate->getId_playdate());
            if($reservation!=null) {
                //Refund payment
                $payments = PaymentDAO::getPaymentListByParentPlaydate($parent->getId_parent(), $playdate->getId_playdate());
                if($payments!=null) {
                    foreach($payments as $auxPay) {
                        PaymentDAO::rollbackParentPayment($auxPay->getId_payment());
                        PaymentDAO::deletePayment($auxPay->getId_payment()); //Eliminamos pago
                    }
                }
                
                //Generate parent payment here
                $newPayment = new Payment();
                $newPayment->setId_parent($parent->getId_parent());
                $newPayment->setId_playdate($playdate->getId_playdate());
                $newPayment->setName("[Cancellation] ".$playdate->getName());
                $newPayment->setAmount_playdate(500);
                $newPayment->setAmount_addons(0);
                $newPayment->setComment("Cancellation fee.");
                PaymentDAO::createParentPayment($newPayment);
                
                //Realizamos la ejecucion de pagos pendientes de este padre con este playdate (solo ser� el pago actual)
                $payments = PaymentDAO::getPaymentListByParentPlaydate($parent->getId_parent(), $playdate->getId_playdate());
                if($payments!=null) {
                    foreach($payments as $auxPay) {
                        if($auxPay->getStatus() == Payment::$STATUS_PENDING) {
                            //Execute this pending payment
                            PaymentDAO::executeParentPayment($auxPay->getId_payment(), $auxPay->getComment());
                        }
                    }
                }
                
                //mark as cancelled
                PlaydateDAO::updateParentReservationStatus($reservation->getId_reservation(), ParentReservation::$STATUS_CANCELED);
                
            }
            
            break;
        }
        
        case "reserve": {
            //Recibimos orden de reservar playdate
            $previousReservedSpots = PlaydateDAO::countPlaydateReservations($playdate->getId_playdate());
            
            //Obtenemos la reserva actual, si ya existia
            $reservation = PlaydateDAO::getParentReservationByPlaydate($parent->getId_parent(), $playdate->getId_playdate());
            if($reservation==null) {
                //create new reservation
                $reserved = PlaydateDAO::playdateParentReservation($playdate->getId_playdate(), $parent->getId_parent());
                $reservation = PlaydateDAO::getParentReservationByPlaydate($parent->getId_parent(), $playdate->getId_playdate());
            } else {
                $reserved = true;
            }
            
            $totalNumKids = 0;
            //Borramos las antiguas plazas reservadas antes de crear las que nos mandan
            PlaydateDAO::deleteAllReservationChildren($reservation->getId_reservation());
            
            $namedKids = 0;
            
            if(!empty($_POST['selectedchildren'])) {
                foreach($_POST['selectedchildren'] as $auxchild) {
                    $kid = ChildrenDAO::getChildren($auxchild);
                    if($kid!=null) {
                        PlaydateDAO::addChildToParentReservation($reservation->getId_reservation(), $kid->getId_children(), 1, $kid->getAge()); //Added age and numchildren
                        $totalNumKids += 1;
                        $namedKids+=1;
                    }
                }
            }
            
            if(!empty($_POST['num_kids'])) {
                $i=0;
                foreach($_POST['num_kids'] as $auxnum) {
                    $numKids = $auxnum;
                    $age= $_POST['ages'][$i];
                    
                    if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                        PlaydateDAO::addChildToParentReservation($reservation->getId_reservation(), null, $numKids, $age); //Added age and numchildren, without id_children
                        $totalNumKids += $numKids;
                    }
                    $i++;
                }
            }
            
            //Realizamos la devoluci�n de pagos que pudiera haber hecho
            $payments = PaymentDAO::getPaymentListByParentPlaydate($parent->getId_parent(), $playdate->getId_playdate());
            if($payments!=null) {
                foreach($payments as $auxPay) {
                    PaymentDAO::rollbackParentPayment($auxPay->getId_payment());
                    PaymentDAO::deletePayment($auxPay->getId_payment()); //Eliminamos pago
                }
            }
            
            //Bloqueamos el saldo correspondiente a esta reserva
            if($totalNumKids > 0) {
                //$amountToReserve = (PlaydatePriceDAO::getMaxPlaydatePrice($playdate->getId_playdate())*$totalNumKids);
                $amountToReserve = (PlaydatePriceDAO::getPlaydatePrice($playdate->getId_playdate())*$totalNumKids);
                
                if($namedKids>1) {
                    //discount second child
                    $amountToReserve -= (PlaydatePriceDAO::getPlaydatePrice($playdate->getId_playdate())*25/100);
                    
                }
                
                //Generate parent payment here
                $newPayment = new Payment();
                $newPayment->setId_parent($parent->getId_parent());
                $newPayment->setId_playdate($playdate->getId_playdate());
                $newPayment->setName($playdate->getName());
                $newPayment->setAmount_playdate($amountToReserve);
                $newPayment->setAmount_addons(($addonsPrice*$totalNumKids));
                $newPayment->setComment("Reservation.");
                PaymentDAO::createParentPayment($newPayment);
                
                //Realizamos la ejecucion de pagos pendientes de este padre con este playdate
                $payments = PaymentDAO::getPaymentListByParentPlaydate($parent->getId_parent(), $playdate->getId_playdate());
                if($payments!=null) {
                    foreach($payments as $auxPay) {
                        if($auxPay->getStatus() == Payment::$STATUS_PENDING) {
                            //Execute this pending payment
                            PaymentDAO::executeParentPayment($auxPay->getId_payment(), $auxPay->getComment());
                        }
                    }
                }
                
                //Enviamos notificacion
                $pdTime =  Utils::dateFormat($playdate->getDate())." ".Utils::get12hourFormat($playdate->getTime_init())." - ".Utils::get12hourFormat($playdate->getTime_end());
                $leadName = "No Lead";
                $leadMail = null;
                if($playdate->getId_specialist()!=null) {
                    $pdSpecialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
                    if($pdSpecialist!=null) {
                        $leadName = $pdSpecialist->getFullName();
                        $leadMail = $pdSpecialist->getEmail();
                    }
                }
                
                $playdate_address= $playdate->getLoc_address().", ".$playdate->getLoc_city().", ". $playdate->getLoc_state();
                $playdate_pickup = "";
                if($playdate->getPickup_address()!=null) {
                    $playdate_pickup = $playdate->getPickup_address();
                }
                $playdate_dropoff="";
                if($playdate->getDropoff_address()!=null) {
                    $playdate_dropoff = $playdate->getDropoff_address();
                }
                
                //Mail Booking Confirmation
                MailUtils::sendPlaydateBooking($parent->getEmail(), $parent->getName(), $playdate->getName(), $pdTime, $leadName, $playdate_address, $playdate_pickup, $playdate_dropoff, "playdate.php?id_playdate=".$playdate->getId_playdate());
                
                //Mail Specialist Presentation
                MailUtils::sendPlaydateBookingSpecialistPresentation($parent->getEmail(), $parent->getName(), $playdate->getName(), $pdTime, $leadName, $playdate_address, $playdate_pickup, $playdate_dropoff, "playdate.php?id_playdate=".$playdate->getId_playdate());
                
                //If playdate reaches 2 kids in this moment, mail to Lead
                $nowReservedSpots = PlaydateDAO::countPlaydateReservations($playdate->getId_playdate());
                
                if(($previousReservedSpots<2) && ($nowReservedSpots>=2) && ($leadMail!=null)) {
                    //This moment, we reach at least 2 kids -> Mail to Specialist with working confirmation
                    MailUtils::sendPlaydateSpecialistConfirmation($leadMail, $leadName, $playdate->getName(), $pdTime, $playdate_address, $playdate_pickup, $playdate_dropoff, "playdate.php?id_playdate=".$playdate->getId_playdate());
                    
                    //This moment, we reach at least 2 kids -> Mail to Specialist with expectations
                    MailUtils::sendSpecialistExpectations($leadMail, $leadName);
                }
                
            }
            
            break;
        }
        
        case "accept-playdate": {
            //Close Playdate Request
            if(($playdate->getId_request()!=null) && is_numeric($playdate->getId_request())) {
                PlaydateRequestDAO::closePlaydateRequest($playdate->getId_request());
            }
            
            PlaydateDAO::acceptPlaydate($playdate->getId_playdate());
            $playdate = PlaydateDAO::getPlaydate($playdate->getId_playdate());
            
            
            $accepted=true;
            
            //send mail accepted
            $pdTime =  Utils::dateFormat($playdate->getDate())." ".Utils::get12hourFormat($playdate->getTime_init())." - ".Utils::get12hourFormat($playdate->getTime_end());
            $leadName = "No Lead";
            if($playdate->getId_specialist()!=null) {
                $pdSpecialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
                if($pdSpecialist!=null) {
                    $playdate_address= $playdate->getLoc_address().", ".$playdate->getLoc_city().", ". $playdate->getLoc_state();
                    MailUtils::sendItineraryAccepted($pdSpecialist->getEmail(), $pdSpecialist->getFullName(), $playdate->getName(), $pdTime, $playdate_address);
                }
            }
            
            //mark other itineraries as declined
            break;
        }
        
        case "decline-playdate": {
            PlaydateDAO::declinePlaydate($playdate->getId_playdate());
            $playdate = PlaydateDAO::getPlaydate($playdate->getId_playdate());
            $declined=true;
            
            //send mail declined
            $pdTime =  Utils::dateFormat($playdate->getDate())." ".Utils::get12hourFormat($playdate->getTime_init())." - ".Utils::get12hourFormat($playdate->getTime_end());
            $leadName = "No Lead";
            if($playdate->getId_specialist()!=null) {
                $pdSpecialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
                if($pdSpecialist!=null) {
                    $playdate_address= $playdate->getLoc_address().", ".$playdate->getLoc_city().", ". $playdate->getLoc_state();
                    MailUtils::sendItineraryDeclined($pdSpecialist->getEmail(), $pdSpecialist->getFullName(), $playdate->getName(), $pdTime, $playdate_address);
                }
            }
            
            header("Location: /my-dashboard.php#requests");
            break;
        }
        
    }
    
}

$maxNumSuporters = PlaydateDAO::calculateNumSupportNeeded($playdate->getId_playdate());

$openSpots = $playdate->getOpen_spots() - PlaydateDAO::countPlaydateReservations($playdate->getId_playdate());

$isPast = PlaydateDAO::isPastPlaydate($playdate->getId_playdate());
$isRSVP = ($playdate->getStatus()==Playdate::$STATUS_RSVP);
$isAccepted = ($playdate->getStatus()==Playdate::$STATUS_PUBLISHED);
$isDeclined = ($playdate->getStatus()==Playdate::$STATUS_DECLINED);

$hasSupervisedTravel = false;
$travelCar = null;
$travelTaxi = null;
$travelTrain = null;
$travelWalk = null;
$travelOptions = 0;


if(($playdate->getPickup_car()!=null) && ($playdate->getPickup_car()>0) && ($playdate->getDropoff_car()!=null) && ($playdate->getDropoff_car()>0) ) {
    $hasSupervisedTravel = true;
    $travelCar = $playdate->getPickup_car() + $playdate->getDropoff_car();
    $travelOptions++;
}

if(($playdate->getPickup_taxi()!=null) && ($playdate->getPickup_taxi()>0) && ($playdate->getDropoff_taxi()!=null) && ($playdate->getDropoff_taxi()>0) ) {
    $hasSupervisedTravel = true;
    $travelTaxi = $playdate->getPickup_taxi() + $playdate->getDropoff_taxi();
    $travelOptions++;
}

if(($playdate->getPickup_train()!=null) && ($playdate->getPickup_train()>0) && ($playdate->getDropoff_train()!=null) && ($playdate->getDropoff_train()>0) ) {
    $hasSupervisedTravel = true;
    $travelTrain = $playdate->getPickup_train() + $playdate->getDropoff_train();
    $travelOptions++;
}

if(($playdate->getPickup_walk()!=null) && ($playdate->getPickup_walk()>0) && ($playdate->getDropoff_walk()!=null) && ($playdate->getDropoff_walk()>0) ) {
    $hasSupervisedTravel = true;
    $travelWalk = $playdate->getPickup_walk() + $playdate->getDropoff_walk();
    $travelOptions++;
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $playdate->getName() ?> - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">

    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page create-playdate itinerary reserve<?php echo($isConnected?" connected":"") ?>" >

    <?php include("modal/modal-background-check.php") ?>
    
    <?php 
    if($isCheckout) { 
        
        $numKidsCheckout = 0;
        
        $checkoutKids = array();
        if(!empty($_POST['selectedchildren'])) {
            foreach($_POST['selectedchildren'] as $auxchild) {
                $kid = ChildrenDAO::getChildren($auxchild);
                if($kid!=null) {
                    $checkoutKids[] = $kid;
                    $numKidsCheckout++;
                }
            }
        }
        
        
        if(!empty($_POST['num_kids'])) {
            $i=0;
            foreach($_POST['num_kids'] as $auxnum) {
                $numKids = $auxnum;
                $age= $_POST['ages'][$i];
                
                if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                    $numKidsCheckout += $numKids;
                }
                $i++;
            }
        }
        
        
        $pricePerChildCheckout = PlaydatePriceDAO::getPlaydatePriceForNewReservations($playdate->getId_playdate(), $numKidsCheckout);
        
        $totalPrice = 0;
        
        $totalPrice += ($pricePerChildCheckout * $numKidsCheckout);
        $totalPrice += ($addonsPrice * $numKidsCheckout);
        
        if(count($checkoutKids>1)) {
            $totalPrice -= ($pricePerChildCheckout*25/100);
        }
        
    ?>
    <!-- modal checkout  -->
    <div class="modal fade itinerary-modal" id="playdate-checkout" class="itinerary-modal" tabindex="-1" role="dialog">
     <form name="frm_checkout" id="frm_checkout" action="/playdate-test.php" method="post">
       	<input type="hidden" name="action" value="reserve" />
        <input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
        <?php foreach ($checkoutKids as $auxKid) { ?>
        <input type="hidden" name="selectedchildren[]" value="<?php echo $auxKid->getId_children()?>" />
        <?php } ?>
        <!-- extra kids -->
        <?php 
        if(!empty($_POST['num_kids'])) {
            $i=0;
            foreach($_POST['num_kids'] as $auxnum) {
                $numKids = $auxnum;
                $age= $_POST['ages'][$i];
                
                if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
        ?>    
        <input type="hidden" name="num_kids[]" value="<?php echo $numKids ?>" />
        <input type="hidden" name="ages[]" value="<?php echo $age ?>" />
        <?php             
                }
                $i++;
            }
        }
        ?>
        <!-- /extra kids -->
        
        			
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="itinerary-modal-heading">CHECKOUT</h2>
            
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="itinerary-modal-heading noUppercase"><?php echo $playdate->getName() ?></h2>
                    
                    <p class="centered"><span style="padding-right:20px"><?php echo Utils::dateFormat($playdate->getDate()) ?></span> | <span style="padding-left:20px"><?php echo Utils::get12hourFormat($playdate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($playdate->getTime_end()) ?></span> </p>
                    <p class="centered"><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?></p>
                </div>
            </div>

            <div class="checkout-data">
                <div class="row">
                    <div class="col-lg-6 ">
                        <p><strong>Description</strong></p>
                        <!-- named kids -->
                        <?php foreach ($checkoutKids as $auxKid) { ?>
                        <p><?php echo $auxKid->getName() ?>| <?php echo $auxKid->getAge() ?> years old</p>
                        <?php } ?>
                        <!-- /named kids -->
                        
                        
                        <!-- extra kids -->
                        <?php 
                        if(!empty($_POST['num_kids'])) {
                            $i=0;
                            foreach($_POST['num_kids'] as $auxnum) {
                                $numKids = $auxnum;
                                $age= $_POST['ages'][$i];
                                
                                if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                        ?>    
                        <p>Extra Kid | <?php echo $age?> years old</p>        
                        <?php             
                                }
                                $i++;
                            }
                        }
                        ?>
                        <!-- /extra kids -->
                        
                        <!-- addons -->
                        <?php 
                        if($playdate->getAdd1_price()!=null) {
                            $auxAddon = AddonDAO::getAddon($playdate->getAdd1_id());
                        ?>
                        <p><?php echo $auxAddon->getName() ?></p>
                        <?php 
                        }
                        if($playdate->getAdd2_price()!=null) {
                            $auxAddon = AddonDAO::getAddon($playdate->getAdd2_id());
                            ?>
                        <p><?php echo $auxAddon->getName() ?></p>
                        <?php 
                        }
                        if($playdate->getAdd3_price()!=null) {
                            $auxAddon = AddonDAO::getAddon($playdate->getAdd3_id());
                            ?>
                        <p><?php echo $auxAddon->getName() ?></p>
                        <?php 
                        }
                        if($playdate->getAdd4_price()!=null) {
                            $auxAddon = AddonDAO::getAddon($playdate->getAdd4_id());
                            ?>
                        <p><?php echo $auxAddon->getName() ?></p>
                        <?php 
                        }
                        ?>
                        <!-- /addons -->
                        <?php if(count($checkoutKids)>1) { ?>
                        <p>Discount 2nd child</p>
                        <?php } ?>
                    </div>
                    <div class="col-lg-3 quantity">
                        <p><strong>Quantity</strong></p>
                        <!-- named kids -->
                        <?php foreach ($checkoutKids as $auxKid) { ?>
                        <p>1</p>
                        <?php } ?>
                        <!-- /named kids -->
                        
                        <!-- extra kids -->
                        <?php 
                        if(!empty($_POST['num_kids'])) {
                            $i=0;
                            foreach($_POST['num_kids'] as $auxnum) {
                                $numKids = $auxnum;
                                $age= $_POST['ages'][$i];
                                
                                if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                        ?>    
                        <p><?php echo $numKids ?></p>        
                        <?php             
                                }
                                $i++;
                            }
                        }
                        ?>
                        <!-- /extra kids -->
                        
                        <!-- addons -->
                        <?php 
                        if($playdate->getAdd1_price()!=null) {
                        ?>
                        <p><?php echo $numKidsCheckout ?></p>
                        <?php 
                        }
                        if($playdate->getAdd2_price()!=null) {
                        ?>
                        <p><?php echo $numKidsCheckout ?></p>
                        <?php 
                        }
                        if($playdate->getAdd3_price()!=null) {
                        ?>
                        <p><?php echo $numKidsCheckout ?></p>
                        <?php 
                        }
                        if($playdate->getAdd4_price()!=null) {
                        ?>
                        <p><?php echo $numKidsCheckout ?></p>
                        <?php 
                        }
                        ?>
                        <!-- /addons -->
                        <?php if(count($checkoutKids)>1) { ?>
                        <p>25%</p>
                        <?php } ?>
                    </div>
                    <div class="col-lg-3 price">
                        <p><strong>Price</strong></p>
                        
                        <!-- named kids -->
                        <?php foreach ($checkoutKids as $auxKid) { ?>
                        <p>$<?php echo Utils::moneyFormat($pricePerChildCheckout, 2)?></p>
                        <?php } ?>
                        <!-- /named kids -->
                        
                        <!-- extra kids -->
                        <?php 
                        if(!empty($_POST['num_kids'])) {
                            $i=0;
                            foreach($_POST['num_kids'] as $auxnum) {
                                $numKids = $auxnum;
                                $age= $_POST['ages'][$i];
                                
                                if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                        ?>    
                        <p>$<?php echo Utils::moneyFormat(($pricePerChildCheckout*$numKids), 2)?></p>        
                        <?php             
                                }
                                $i++;
                            }
                        }
                        ?>
                        <!-- /extra kids -->
                        
                        <!-- addons -->
                        <?php 
                        if($playdate->getAdd1_price()!=null) {
                        ?>
                        <p>$<?php echo Utils::moneyFormat($playdate->getAdd1_price()*100*$numKidsCheckout); ?></p>
                        <?php 
                        }
                        if($playdate->getAdd2_price()!=null) {
                        ?>
                        <p>$<?php echo Utils::moneyFormat($playdate->getAdd2_price()*100*$numKidsCheckout); ?></p>
                        <?php 
                        }
                        if($playdate->getAdd3_price()!=null) {
                        ?>
                        <p>$<?php echo Utils::moneyFormat($playdate->getAdd3_price()*100*$numKidsCheckout); ?></p>
                        <?php 
                        }
                        if($playdate->getAdd4_price()!=null) {
                        ?>
                        <p>$<?php echo Utils::moneyFormat($playdate->getAdd4_price()*100*$numKidsCheckout); ?></p>
                        <?php 
                        }
                        ?>
                        <!-- /addons -->
                        
                        
                        <?php if(count($checkoutKids)>1) { ?>
                        <p>-$<?php echo Utils::moneyFormat(($pricePerChildCheckout*25/100), 2)?></p>
                        <?php } ?>
                    </div>
                    <div class="col-lg-12 total" style="margin:20px 0px 20px 0px"></div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                    </div>
                    <div class="col-lg-3">Total: <?php echo Utils::moneyFormat($totalPrice, 2)?>
                    </div>
                </div>
                
                <?php 
                /*
                ?>
                <div class="row extra-kids" style="display:none">
                	<div class="col-lg-3"></div>
                	<div class="col-lg-6 addons">
              
                    <!-- SELECT EXTRA KIDS -->
                    <p class="centered">Lower price adding extra kids</p>
              
                      <div class="row row-child-detail">
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="num_kids[]">
                                    <option value="">Kids</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="ages[]">
                                    <option value="">Age</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-2">
                            <a href="javascript:;" class="bt-add-kid-detail"><img src="img/ico/main/0860-plus-circle.svg"/></a>
                          </div>
                        </div>

                	</div>
                 <div class="col-lg-3"></div>
            </div>
            <?php */ ?>
            
            
               
            </div>


            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8">
              <input class="btn btn-md btn-primary bt-save-profile last" type="submit" value="Reserve playdate" />
              </div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row" style="margin-top:20px">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-link-reserved-playdate" href="/help-center.php#collapse23" title="">Pricing and cancellation policy</a>
                <p class="centered">Here at PAL, we rely on our community of parents and Pal specialists to abide by these shared <a href="/help-center.php#collapse26b">expectations</a></p>
              </div>
              <div class="col-lg-2"></div>
            </div>

          </div>
        </div>
      </div>
      </form>
    </div>
    <!-- /modal checkout  -->
    <?php 
    } //if isCheckout 
    ?>
    
    <!-- Playdate reserved -->
    <div class="modal fade itinerary-modal" id="playdate-reserved" class="itinerary-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close bt-current-playdates" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="itinerary-modal-heading">PLAYDATE SUCCESSFULLY RESERVED</h2>
            
            <div class="row row-tabs">
              <h2 class="itinerary-modal-heading noUppercase"><?php $playdate->getName() ?></h2>
              <p><?php echo Utils::dateFormat($playdate->getDate()) ?>   |   <?php echo Utils::get12hourFormat($playdate->getTime_init()) ?> to <?php echo Utils::get12hourFormat($playdate->getTime_end()) ?> h.</p>
              <p><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?></p>
            </div>
            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8">
                <p>Booking will be confirmed when at least 2 open spots are reserved by </p>
                <p class="ppal-date"><?php echo Utils::dateFormat(date('m/d/Y', strtotime('-2 day', strtotime(BaseDAO::toMysqlDateFormat($playdate->getDate()))))); ?></p>
              </div>
              <div class="col-lg-2"></div>
            </div>
            
            <?php /*
            <div class="row row-tabs playdate-option-modal">
              <div class="col-lg-6">
                <p>Reserve More Spots <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="Activity Budget"><i class="fa fa-question-circle"></i></a></p>
                <!--<div class="selectdiv">
                  <label for="inputSpots">
                    <select class="form-control" id="inputSpots">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </label>
                </div>-->
                <div class="row ">
                  <div class="col-lg-5">
                    <div class="selectdiv">
                        <label for="inputKids">
                          <select class="form-control" id="inputKids" name="num_kids[]">
                            <option value="">Kids</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                          </select>
                        </label>
                      </div>
                  </div>
                  <div class="col-lg-5">
                    <div class="selectdiv">
                        <label for="inputKids">
                          <select class="form-control" id="inputKids" name="ages[]">
                            <option value="">Age</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                          </select>
                        </label>
                      </div>
                  </div>
                  <div class="col-lg-2">
                    <a href="javascript:;" class="bt-add-kid"><img src="img/ico/main/0860-plus-circle.svg"/></a>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <p>Invite Friends</p>
                <div class="form-group">
                  <label for="inviteFriends" class="sr-only">Share with friends</label>
                  <input type="text" id="inviteFriends" class="form-control noMarginTop" placeholder="Username or email" required="">
                </div>
                <div class="container-tags">
                  <div class="tag">
                    <span>Chelsea</span>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                  </div>
                  <div class="tag">
                    <span>Soho</span>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                  </div>
                  <div class="tag">
                    <span>Meatpacking</span>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-top" href="my-dashboard.html" title="">Update Playdate</a></div>
              <div class="col-lg-2"></div>
            </div>
            */ ?> 
            
            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-link-reserved-playdate" href="/help-center.php#collapse23" title="">Pricing and Cancellation Policy</a>
                <p>Here at Playdate, we rely on our community of parents and playdate specialists to aboide by these shared <a href="/help-center.php#collapse26b">expectations</a>.</p>
              </div>
              <div class="col-lg-2"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Playdate reserved -->
    
    <!-- Itinerary Accepted -->
    <div class="modal fade itinerary-modal" id="playdate-accepted" class="itinerary-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location.href='/my-dashboard.php'">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="itinerary-modal-heading">ITINERARY ACCEPTED</h2>
            
            <div class="row row-tabs">
              <h2 class="itinerary-modal-heading noUppercase"><?php $playdate->getName() ?></h2>
              <p><?php echo Utils::dateFormat($playdate->getDate()) ?>   |   <?php echo Utils::get12hourFormat($playdate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($playdate->getTime_end()) ?></p>
              <p><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?></p>
            </div>
            <div class="row row-tabs">
              <p>Specialist will reserve their time for two days. At least two children are required to book a spot by</p>
              <p class="ppal-date"><?php echo Utils::dateFormat($playdate->getDate()) ?></p>
            </div>
            <?php /*
            <div class="row row-tabs playdate-option-modal">
              <div class="col-lg-6">
                <p>Reserve More Spots <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="Activity Budget"><i class="fa fa-question-circle"></i></a></p>
                <div class="selectdiv">
                  <label for="inputSpots">
                    <select class="form-control" id="inputSpots">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </label>
                </div>
              </div>
              <div class="col-lg-6">
                <p>Invite Friends</p>
                <div class="form-group">
                  <label for="inviteFriends" class="sr-only">Username or email</label>
                  <input type="text" id="inviteFriends" class="form-control noMarginTop" placeholder="Username or email" required="">
                </div>
                <div class="container-tags">
                  <div class="tag">
                    <span>Chelsea</span>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                  </div>
                  <div class="tag">
                    <span>Soho</span>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                  </div>
                  <div class="tag">
                    <span>Meatpacking</span>
                    <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-top" href="my-dashboard.html" title="">Update Playdate</a></div>
              <div class="col-lg-2"></div>
            </div>
            */?>

          </div>
        </div>
      </div>
    </div>
    <!-- Playdate reserved -->
     <!-- Itinerary changes -->
    <div class="modal fade itinerary-modal" id="playdate-declined" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="itinerary-modal-heading">CHANGE OR DECLINE ITINERARY</h2>
            
            <div class="row row-tabs">
              <h2 class="itinerary-modal-heading noUppercase"><?php $playdate->getName() ?></h2>
              <p><?php echo Utils::dateFormat($playdate->getDate()) ?>   |   <?php echo Utils::get12hourFormat($playdate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($playdate->getTime_end()) ?></p>
              <p><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?></p>
            </div>
            <div class="row row-tabs">
              <p>Submited by</p>
              <p class="ppal-date"><?php echo $specialist->getFullName() ?></p>
            </div>
            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8">
              <form name="frm" name="frm_change_modal" id="frm_change_modal" action="/playdate.php">
              	 <input type="hidden" name="action" value="change-playdate" />
                 <input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                	                 
                 <label for="changeItinerary" class="sr-only">Tell the specialist what could be improved</label>
                 <textarea class="form-control" placeholder="Tell the specialist what could be improved" id="changeItinerary" rows="7" name="message"></textarea>
              </form>
              </div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8">
                <a class="bt-top"  href="javascript:;" onclick="document.getElementById('frm_change_modal').submit();" title="">Request changes</a>
                <form name="frm" name="frm_decline_modal" id="frm_decline_modal" action="/playdate.php">
                	<input type="hidden" name="action" value="decline-playdate" />
                	<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                	<a class="bt-top grey" href="javascript:;" onclick="document.getElementById('frm_decline_modal').submit();" title="">Decline Itinerary</a>
                </form>
              </div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"></div>
              <div class="col-lg-2"></div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Itinerary changes -->


    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead" style="background: url('<?php echo $playdate->getPicture() ?>') no-repeat center; background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1><?php echo $playdate->getName() ?></h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <?php 
        $ratingvalue = RatingDAO::getSpecialistValue($specialist->getId_specialist());
        $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialist->getId_specialist());
        $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialist->getId_specialist());
        ?>
        <div class="col-lg-3 sidebar  my-dashboard">
          <div class="box-playdates">
            <div class="img-container">
              <img src="<?php echo $specialist->getPicture() ?>" alt="">
            </div>
            <div class="info">
              <span class="title"><?php echo $specialist->getFullName() ?></span>
              <span class="subtitle">
              	<?php if($specialist->getProfile()==Specialist::$PROFILE_LEAD) { ?>
              	Lead
              	<?php } else if($specialist->getProfile()==Specialist::$PROFILE_SUPPORTER) { ?>
              	Supporter
              	<?php } else if($specialist->getProfile()==Specialist::$PROFILE_NURSE) { ?>
              	Nurse
              	<?php } ?>
              </span>
            </div>
            <div class="puntuacion">
              <ul>
                <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
              	<li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
              </ul>
              <span class="num"><?php echo $ratingvalue ?>/5</span>
            </div>
            <div class="separator"></div>
            <div class="data certifications">
              <div class="row">
                <div class="col-lg-6">
                  <div class="info check" data-toggle="modal" data-target="#modal-legal"><span>background check</span></div>
                </div>
                 <div class="col-lg-6">
                  <div class="info certificate"><span>approved by PAL</span></div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="info family"><span><?php echo $numFamilies ?> repeat families</span></div>
                </div>
                 <div class="col-lg-6">
                  <div class="info playdate"><span><?php echo $numPlaydates ?> playdates</span></div>
                </div>
              </div>
            </div>
             <div class="separator"></div>
            <div class="data">
              <p><?php echo $specialist->getAbout_me(250) ?></p>
              <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist()?>&slug=<?php echo $specialist->getSlug() ?>" title="" class="bt-profile">View full Profile</a>
            </div>
            <div class="separator"></div>
            <div class="reserve">
            	<form class="form-add-group" method="post" action="/playdate.php?id_playdate=<?php echo $playdate->getId_playdate() ?>">
                  	<input type="hidden" name="action" value="addgroupspecialist" />
                  	<input type="hidden" name="id_specialist" value="<?php echo $specialist->getId_specialist() ?>" />
                  	<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                	<input type="text" id="inputSelectGroup" class="form-control" placeholder="Select group" required="" name="name" style="border:2px solid #878787">
                    <button class="btn btn-md btn-primary bt-titulo" type="submit">Invite to Group</button>
              	</form>
            </div>
            <div class="reserve">
              <a href="mailto:parentconcierge@playdatepal.com" class="bt-reserve">Get in touch with<br>Concierge</a>
            </div>
          </div>
          
           <?php 
           foreach($supporters as $auxSupporter) {
               $ratingvalue = RatingDAO::getSpecialistValue($auxSupporter->getId_specialist());
           ?>
           <div class="box-playdates">
            <div class="img-container">
              <img src="<?php echo $auxSupporter->getPicture() ?>" alt="Picture of <?php echo $auxSupporter->getFullName() ?>">
            </div>
            <div class="info">
              <span class="title"><?php echo $auxSupporter->getFullName() ?></span>
              <span class="subtitle">Supporter</span>
            </div>
            <div class="puntuacion">
              <ul>
                <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
              	<li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
              </ul>
              <span class="num"><?php echo $ratingvalue ?>/5</span>
            </div>
          </div>
		<?php } ?>
		  
		  
		  <?php
		  
		  if(($sessionSpecialist!=null) && ($sessionSpecialist->getId_specialist() == $specialist->getId_specialist())) {
		      $supporters = SpecialistDAO::getSupporterRequestsByPlaydate($playdate->getId_playdate());
		      if(count($supporters)>0) {
		  ?>
          		<p class="titulo">Support Applications</p>
          		<p>You can hire <?php echo $maxNumSuporters ?> supporters</p>
          <?php 
		      }
          
           foreach($supporters as $auxSupporter) {
               $ratingvalue = RatingDAO::getSpecialistValue($auxSupporter->getId_specialist());
           ?>
           <div class="box-playdates">
            <div class="img-container">
              <img src="<?php echo $auxSupporter->getPicture() ?>" alt="Picture of <?php echo $auxSupporter->getFullName() ?>">
            </div>
            <div class="info">
              <span class="title"><a href="/specialist-profile.php?id_specialist=<?php echo $auxSupporter->getId_specialist()?>"><?php echo $auxSupporter->getFullName() ?></a></span>
              <span class="subtitle">Supporter</span>
            </div>
            <div class="puntuacion">
              <ul>
                <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
              	<li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
              </ul>
              <span class="num"><?php echo $ratingvalue ?>/5</span>
            </div>
            
            <div class="separator"></div>
            <?php if(($playdate->getStatus()==Playdate::$STATUS_RSVP) || ($playdate->getStatus()==Playdate::$STATUS_PUBLISHED)) { //only can accept/decline in RSVP status ?>
                <?php if($numSuportersSelected<$maxNumSuporters) { ?> 
                <div class="reserve">
                	<form class="form-add-group" method="post" action="/playdate.php?id_playdate=<?php echo $playdate->getId_playdate() ?>">
                      	<input type="hidden" name="action" value="accept-support" />
                      	<input type="hidden" name="id_specialist" value="<?php echo $auxSupporter->getId_specialist() ?>" />
                      	<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                        <button class="btn btn-md btn-primary bt-titulo" type="submit">Accept Support</button>
                  	</form>
                </div>
                <div class="reserve">
                	<form class="form-add-group" method="post" action="/playdate.php?id_playdate=<?php echo $playdate->getId_playdate() ?>">
                      	<input type="hidden" name="action" value="decline-support" />
                      	<input type="hidden" name="id_specialist" value="<?php echo $auxSupporter->getId_specialist() ?>" />
                      	<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                        <button class="bt-reserve" type="submit">Decline Support</button>
                  	</form>
                </div>
                <?php } else { ?>
                <div class="reserve">
            		<button class="bt-reserve" type="submit" disabled="disabled">Max. supports reached</button>
            	</div>
                <?php } ?>
            <?php } else { ?>
            <div class="reserve">
            	<button class="bt-reserve" type="submit" disabled="disabled">Choose in RSVP</button>
            </div>
            <?php } ?>
          </div>
		<?php }
		  }//connected lead
		?>
		  
		  
		  
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-6 contenido-central">
            <div class="row">

              

              <div class="col-lg-12">
                <div class="itinerary-info">
                  <p class="name"><?php echo $playdate->getName() ?></p>
                  <p class="place"><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?></p>
                  <p class="date"><?php echo Utils::dateFormatShort($playdate->getDate()) ?> | <?php echo Utils::get12hourFormat($playdate->getTime_init()) ?> to <?php echo Utils::get12hourFormat($playdate->getTime_end()) ?></p>
                  <p class="reserve">RSVP by <?php echo Utils::dateFormat(date('m/d/Y', strtotime('-2 day', strtotime(BaseDAO::toMysqlDateFormat($playdate->getDate()))))); ?></p>
                  <p class="age">Ages: <?php echo $playdate->getAge_init() ?> - <?php echo $playdate->getAge_end() ?></p>
                  <p class="participants">Participants: <?php echo $playdate->getNum_children() ?></p>
                  <p class="spots">Open Spot<?php echo(($openSpots!=1)?"s":"") ?>: <?php echo $openSpots ?></p>
                </div>
              </div>

            </div>

            <div class="row row-tabs" style="padding-bottom:20px">
               <div class="col-lg-6">
               <?php 
                $interests = InterestDAO::getInterestsByPlaydate($playdate->getId_playdate());
                foreach ($interests as $auxInterest) {
                    $interestName =$auxInterest->getName();
                    $interestType = "";
                    switch ($auxInterest->getType()) {
                        case Interest::$TYPE_TRAINING: {
                            $interestType=" stem";
                            break;
                        }
                        case Interest::$TYPE_ART: {
                            $interestType=" creative";
                            break;
                        }
                        case Interest::$TYPE_PLAY: {
                            $interestType=" outdoor";
                            break;
                        }
                        case Interest::$TYPE_CULTURAL: {
                            $interestType=" attractions";
                            break;
                        }
                    }
                    
               ?>
                <p class="category <?php echo $interestType ?>"><?php echo $interestName ?></p>
               <?php } ?>
              </div>
              <div class="col-lg-6">
               <!-- <p class="invite-parent-title"><?php echo $playdate->getOpen_spots() ?> Open Spots</p>
                
                <form class="form-invite-parent">
                  <label for="inputInviteParents" class="sr-only">Username or Email</label>
                  <input type="text" id="inputInviteParents" class="form-control noMarginTop" placeholder="Username or Email" required="" >
                  <button class="btn btn-md btn-primary bt-save-profile last" type="button" data-toggle="modal" data-target="#itinerary-accepted">Invite Friends</button>
                </form>-->
              </div>
            </div>

            <div class="row">
               <div class="col-lg-12 description">
                <div class="data">
                  <p><strong>Overview</strong></p>
                  <p><?php echo $playdate->getDescription() ?></p>
                </div>
                <div class="data">
                  <p><strong>Itinerary</strong></p>
                  <!-- act1 -->
                  <?php 
                  if($playdate->getAct1_id()!=null) {
                      $actName=ActivityDAO::getActivity($playdate->getAct1_id())->getName();
                  ?>
                  <p><span class="hour" style="width: 20%;"><?php echo Utils::get12hourFormat($playdate->getAct1_time()) ?></span>
                    <span style="width:75%;"><?php echo $actName ?></span>
                    <span class="desc" style="padding-left: 20%;"><?php echo $playdate->getAct1_note() ?></span>
                  </p>
                  <?php } ?>
                  <!-- /act1 -->
                  <!-- act2 -->
                  <?php 
                  if($playdate->getAct2_id()!=null) {
                      $actName=ActivityDAO::getActivity($playdate->getAct2_id())->getName();
                  ?>
                  <p><span class="hour" style="width: 20%;"><?php echo Utils::get12hourFormat($playdate->getAct2_time()) ?></span>
                    <span style="width:75%;"><?php echo $actName ?></span>
                    <span class="desc" style="padding-left: 20%;"><?php echo $playdate->getAct2_note() ?></span>
                  </p>
                  <?php } ?>
                  <!-- /act2 -->
                  <!-- act3 -->
                  <?php 
                  if($playdate->getAct3_id()!=null) {
                      $actName=ActivityDAO::getActivity($playdate->getAct3_id())->getName();
                  ?>
                  <p><span class="hour" style="width: 20%;"><?php echo Utils::get12hourFormat($playdate->getAct3_time()) ?></span>
                    <span style="width:75%;"><?php echo $actName ?></span>
                    <span class="desc" style="padding-left: 20%;"><?php echo $playdate->getAct3_note() ?></span>
                  </p>
                  <?php } ?>
                  <!-- /act3 -->
                  <!-- act4 -->
                  <?php 
                  if($playdate->getAct4_id()!=null) {
                      $actName=ActivityDAO::getActivity($playdate->getAct4_id())->getName();
                  ?>
                  <p><span class="hour" style="width: 20%;"><?php echo Utils::get12hourFormat($playdate->getAct4_time()) ?></span>
                    <span style="width:75%;"><?php echo $actName ?></span>
                    <span class="desc" style="padding-left: 20%;"><?php echo $playdate->getAct4_note() ?></span>
                  </p>
                  <?php } ?>
                  <!-- /act4 -->
                </div>
                
                <?php 
                if(($playdate->getAdd1_id()!=null) || ($playdate->getAdd2_id()!=null) || ($playdate->getAdd3_id()!=null) || ($playdate->getAdd4_id()!=null) ) {
                ?>
                <div class="data">
                  <?php if($isSpecialist) { ?>
                  <p><strong>Activity Budget <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="The Activity Budget is a per child rate charged to each Parent to cover admissions, meals or additional supplies associated with the experience. You can pay your Specialist in cash or using a mobile payment app."><i class="fa fa-question-circle"></i></a></strong></p>
                  <?php } else { ?>
                  <p><strong>Activity Budget <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="The Activity Budget is a per child rate that will be charged to each Parent. If you prefer to be paid in cash or using a mobile payment app, please leave this section blank and include your budget preferences in the description portion above. Activity Budgets are not necessary for a successful playdate, but should be considered if there may be admissions, meals or additional supplies associated with the experience."><i class="fa fa-question-circle"></i></a></strong></p>
                  <?php } ?>
                  <!-- addon1 -->
                  <?php 
                  if($playdate->getAdd1_id()!=null) {
                      $addName=AddonDAO::getAddon($playdate->getAdd1_id())->getName();
                  ?>
                  <p><span class="hour">$<?php echo $playdate->getAdd1_price()?></span>
                    <span><?php echo $addName ?></span>
                  </p>
                  <?php } ?>
                  <!-- /addon1 -->
                  <!-- addon2 -->
                  <?php 
                  if($playdate->getAdd2_id()!=null) {
                      $addName=AddonDAO::getAddon($playdate->getAdd2_id())->getName();
                  ?>
                  <p><span class="hour">$<?php echo $playdate->getAdd2_price()?></span>
                    <span><?php echo $addName ?></span>
                  </p>
                  <?php } ?>
                  <!-- /addon2 -->
                  <!-- addon3 -->
                  <?php 
                  if($playdate->getAdd3_id()!=null) {
                      $addName=AddonDAO::getAddon($playdate->getAdd3_id())->getName();
                  ?>
                  <p><span class="hour">$<?php echo $playdate->getAdd3_price()?></span>
                    <span><?php echo $addName ?></span>
                  </p>
                  <?php } ?>
                  <!-- /addon3 -->
                  <!-- addon4 -->
                  <?php 
                  if($playdate->getAdd4_id()!=null) {
                      $addName=AddonDAO::getAddon($playdate->getAdd4_id())->getName();
                  ?>
                  <p><span class="hour">$<?php echo $playdate->getAdd4_price()?></span>
                    <span><?php echo $addName ?></span>
                  </p>
                  <?php } ?>
                  <!-- /addon4 -->
                </div>
                <?php 
                } //end if there is Activity Budget
                ?>
                
                <div class="data">
                  <p><strong>Pricing and Cancelation Policy</strong></p>
                  <p>Price will be confirmed the next day from RSVP date.</p>
                  <p>Cancellation Fee of $10 within 48hrs or $5 within a week.</p>
                </div>
               </div>

          </div>

 		  
          <div class="row row-tabs">
           
            <div class="col-lg-12">
              <div class="admin-slider front">
                <div class="all-slides">
                  <div class="slide selected"><img src="<?php echo $playdate->getPicture() ?>"></div>
                  <?php /*
                  <div class="nav">
                    <a href="javascript:;" title="" class="bt-slider-left"><img src="img/fle-slider.png"/></a>
                    <a href="javascript:;" title="" class="bt-slider-right"><img src="img/fle-slider.png"/></a>
                  </div>
				  */?>
                </div>
                
              </div>
            </div>
           
          </div>
          
          
          <?php /* ?>
          <div class="row row-tabs">
              <div class="col-lg-12 location">
                <iframe src="http://maps.google.com/maps?q=address&output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>              
          </div>
            */?> 
          <div class="row row-tabs">
              <div class="col-lg-12 location container-map">
                <iframe src="//maps.google.com/maps?q=<?php echo $playdate->getLoc_address() ." ". $playdate->getLoc_city() ." ". $playdate->getLoc_state()." EEUU"?>&output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>              
          </div>
          <div class="row">
			   <?php $zipCode = ZipcodeDAO::getZipcode($playdate->getLoc_id_zipcode()) ?>
               <div class="col-lg-12 description">
                <div class="data">
                  <p class="address"><?php echo $playdate->getLoc_address() ?></p>
                  <p class="city"><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?> <?php echo (($zipCode!=null)?$zipCode->getZipcode():"") ?></p>
                </div>
               </div>

          </div>
        
        <?php 
          $ratings = null;
          if($isPast) {
             $ratings = RatingDAO::getRatingListByPlaydate($playdate->getId_playdate());
          }
          if(($ratings!=null) && (count($ratings)>0)) {
        ?>
	    <div class="row row-dashboard updates parents">
          <div class="col-lg-12">
            <p class="titulo">Comments</p>
          </div>
          
              <?php 
              foreach($ratings as $auxRating) {
                  if(($auxRating->getComment_playdate() != null) && ($auxRating->getComment_playdate() != "")) {
                      $auxParent = ParentDAO::getParent($auxRating->getId_parent());
                      $pic = $auxParent->getPicture();
                      if(($pic==null) || ($pic=="")) {
                          $pic = "img/img-profile.jpg";
                      }
              ?>
          <div class="row info-updates">
            <div class="col-lg-2 date">
              <img src="<?php echo $pic ?>">
              <span><strong style="color:#878787"><?php echo $auxParent->getUsername() ?></strong></span>
            </div>
            <div class="col-lg-1">
            </div>
            <div class="col-lg-9 texto">
              <p class="day"><strong><?php echo Utils::dateFormat($auxRating->getIdate())?></strong></p>
              <p><?php echo $auxRating->getComment_playdate() ?></p>
            </div>
          </div>
              
              <?php 
                  } 
              }
          ?>
          </div>
          <?php 
          }
          ?>
        </div>
        

		
        <div class="col-lg-3 sidebar-right">
          <div class="row">
          
			  <div class="itinerary-extras">
			  	   <?php 
			  	   if($isPast) {
			  	   ?>
			  	  <div class="form-group" style="padding-bottom:20px">
                    <p class="totalPrice">$<?php echo Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePrice($playdate->getId_playdate())+$addonsPrice), 0); ?></p>
                    <p class="perchild" style="color:#878787">playdate has passed</p>
                  </div>
			  	  <?php } else if($isRSVP) { ?>
			  	  <div class="form-group" style="padding-bottom:20px">
                    <p class="totalPrice">$<?php echo Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePrice($playdate->getId_playdate())+$addonsPrice), 0); ?></p>
                    <p class="perchild" style="color:#878787">playdate RSVP has passed</p>
                  </div>
			  	  <?php } else if($isParent && ($isAccepted || ($playdate->getId_parent()==$parent->getId_parent()))) { ?>
			  	  <!-- price info -->
			  	  <div class="form-group" style="padding-bottom:20px">
                      <p class="totalPrice">$<?php echo Utils::moneyFormat((PlaydatePriceDAO::getPlaydatePrice($playdate->getId_playdate())+$addonsPrice), 0); ?></p>
                      <p class="perchild">or less per child</p>
                      <p class="perchild">25% off second child</p>
                    </div>
                <!-- /price info -->
            	<?php } ?>
                  
                  <?php 
                  if($isSpecialist && ($sessionSpecialist->getId_specialist()!=$specialist->getId_specialist())) {
                      $sent = false;
                      $reqs = SpecialistDAO::getAllSupporterRequestsByPlaydate($playdate->getId_playdate());
                      foreach ($reqs as $auxReq) {
                          if($auxReq->getId_specialist()==$sessionSpecialist->getId_specialist()) {
                              $sent = true;
                          }
                      }
                      
                      if($sent) {
                          ?>
                  <div class="form-group" style="padding-top:10px;">
                  <form name="frm_support" id="frm_support" action="/playdate.php?id_playdate=<?php echo $playdate->getId_playdate() ?>" method="post">
                  	<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                    <input class="btn btn-md btn-primary bt-save-profile last" type="button" value="Already Requested" disabled="disabled" />
                  </form>
                  </div>
                  <?php
                          
                      } else {
                          
                          
                  ?>
                  <div class="form-group" style="padding-top:10px;">
                  <form name="frm_support" id="frm_support" action="/playdate.php?id_playdate=<?php echo $playdate->getId_playdate() ?>" method="post">
                  	<input type="hidden" name="action" value="apply-support" />
        			<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                    <input class="btn btn-md btn-primary bt-save-profile last" type="submit" value="Support - Apply Now" style="display: inline; width: 90%" /> <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="By clicking 'Apply Now', this shows the Lead that you are interested in the position. You will not be confirmed until the Lead approves you."><i class="fa fa-question-circle"></i></a>
                  </form>
                  </div>
                  <?php
                      }
                   } //is supporter ?>
                  
                  
                  <?php 
                  //if(($playdate->getStatus()!=Playdate::$STATUS_REQUEST) || (!$isParent) || ($playdate->getId_parent()!= $parent->getId_parent())) {  
                  if($isParent && ($playdate->getStatus()==Playdate::$STATUS_PUBLISHED) && (!$isPast)) {
                  ?>
                  <!-- open form reserve -->
                  <form name="frm_reserve" id="frm_reserve" action="/playdate-test.php" method="post">
        			  		<?php /*?>
        			  		<input type="hidden" name="action" value="reserve" />
        			  		*/?>
        			  		<input type="hidden" name="action" value="checkout" />
        			  		<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
        			  		<input type="hidden" name="credit" value="<?php echo $parentcredit ?>" />
        			  		<input type="hidden" name="priceperchild" value="<?php echo $priceperchild ?>" />
        			  		<input type="hidden" name=discountsecondchild value="<?php echo $discountsecondchild ?>" />
        			  		
                  <?php } ?>
                  <?php if((!$isPast) && (!$isRSVP) && $isAccepted && $hasSupervisedTravel) {?>
                  <!-- SUPERVISED TRAVEL  -->
                  <div class="supervised-travel">
                    <p>Supervised Travel <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="This is only a suggested budget for Supervised Travel. It will not be included in the per child price of this playdate. Payment via cash or mobile cash app must be facilitated by Parents and Specialists directly to cover the cost of travel and travel time (if time is not already included in Playdate itinerary)."><i class="fa fa-question-circle"></i></a></p>
                    <div class="row">
                      <?php if($travelWalk!=null) { ?>
                      <div class="col-lg-6">
                        <a href="javascript:;" data-value="walk" class="bt-travel"><img src="img/ico/white/0302-walk.svg" width="30">$<?php echo $travelWalk ?></a>
                      </div>
                      <?php }  ?>
                      <?php if($travelTrain!=null) { ?>
                      <div class="col-lg-6">
                        <a href="javascript:;" data-value="train" class="bt-travel"><img src="img/ico/white/0603-train.svg" width="30">$<?php echo $travelTrain ?></a>
                      </div>
                      <?php } ?>
                      
                      <?php if($travelTaxi!=null) { ?>
                      <div class="col-lg-6">
                        <a href="javascript:;" data-value="taxi" class="bt-travel<?php /* selected */ ?>"><img src="img/ico/white/0595-taxi.svg" width="30">$<?php echo $travelTaxi?></a>
                      </div>
                      <?php }  ?>
                      
                      <?php if($travelCar!=null) { ?>
                      <div class="col-lg-6">
                        <a href="javascript:;" data-value="car" class="bt-travel"><img src="img/ico/white/0592-car2.svg" width="30">$<?php echo $travelCar ?></a>
                      </div>
                      <?php }  ?>
                      <?php if(($travelOptions==1) || ($travelOptions==3)) { ?>
                      <div class="col-lg-6"><p style="width:100px;"></p></div>
                      <?php } ?>
                      <input type="hidden" name="travel" id="travel" value="">
                    </div>
                  </div>
                  
                  <!-- /SUPERVISED TRAVEL  -->
                  <?php } ?>
                  
                  <?php if($isParent && ($playdate->getStatus()==Playdate::$STATUS_PUBLISHED) && (!$isPast)) { 
                      $childrenReservations = PlaydateDAO::getChildrenReservationsByParent($playdate->getId_playdate(), $parent->getId_parent());
                      $childids=array();
                      foreach($childrenReservations as $auxRes) {
                          if($auxRes->getId_children()!=null) {
                              $childids[] = $auxRes->getId_children();
                          }
                      }
                      
                  ?>
                  <div class="kids-playdates">
                  <p style="text-align:center;">Children attending playdate <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="You can include your own children by selecting their name(s) below. You can also reserve and pay for additional children by indicating the number of kids and their age(s) below. Please note that to confirm additional spots for non-members, parents must sign a Guest Registration Form. "><i class="fa fa-question-circle"></i></a></p>
                    <!-- SELECT MY CHILDREN -->
                    <div class="row">
                      <div class="col-lg-12">
                      <?php $childs = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                      
                      foreach ($childs as $auxChild) {
                          $selectedKid = in_array($auxChild->getId_children(), $childids);
                          $childAgeInvalid = (($auxChild->getAge()<$playdate->getAge_init()) || ($auxChild->getAge()>$playdate->getAge_end()));
                          
                      ?>
                        <div class="form-group">
                          <input onchange="updatePlaydatePrice()" type="checkbox" id="0" class="form-control" placeholder="keyword" name="selectedchildren[]" value="<?php echo $auxChild->getId_children()?>" <?php echo($selectedKid?"checked=\"checked\"":"")?> <?php echo($childAgeInvalid?"disabled=\"disabled\"":"")?> />
                          <label for="0"><?php echo $auxChild->getName() ?></label>
                        </div>
                      
					  <?php } ?> 
                      </div>
                  	</div>
                    <!-- /SELECT MY CHILDREN -->
                      <!-- SELECT EXTRA KIDS -->
                      <p style="text-align:center; margin-bottom: 15px;">Additional children</p>
                      <?php 
                      foreach ($childrenReservations as $auxReservation) {
                          if($auxReservation->getId_children()==null) {
                      ?>
                      
					   <div class="row row-child-detail">
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="num_kids[]">
                                    <option value="-1">Kids</option>
                                    <option value="1" <?php echo(($auxReservation->getNum_children()==1)?"selected=\"selected\"":"") ?>>1 kid</option>
                                    <option value="2" <?php echo(($auxReservation->getNum_children()==2)?"selected=\"selected\"":"") ?>>2 kids</option>
                                    <option value="3" <?php echo(($auxReservation->getNum_children()==3)?"selected=\"selected\"":"") ?>>3 kids</option>
                                    <option value="4" <?php echo(($auxReservation->getNum_children()==4)?"selected=\"selected\"":"") ?>>4 kids</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="ages[]">
                                    <option value="-1">Age</option>
                                    <?php 
                                    for($i=$playdate->getAge_init(); $i<=$playdate->getAge_end(); $i++) {
                                    ?>
                                    <option value="<?php echo $i ?>" <?php echo(($auxReservation->getAge()==$i)?"selected=\"selected\"":"") ?>><?php echo $i ?> year<?php if($i!=1) {echo('s');} ?></option>
                                    <?php } ?>
                                  </select>
                                </label>
                              </div>
                          </div>
                      </div>                      
                      <?php 
                          }
                      }
                      ?>
                      
                      <div class="row row-child-detail">
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="num_kids[]">
                                    <option value="-1">Kids</option>
                                    <option value="1">1 kid</option>
                                    <option value="2">2 kids</option>
                                    <option value="3">3 kids</option>
                                    <option value="4">4 kids</option>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="selectdiv">
                                <label for="inputKids">
                                  <select class="form-control" id="inputKids" name="ages[]">
                                    <option value="-1">Age</option>
                                    <?php 
                                    for($i=$playdate->getAge_init(); $i<=$playdate->getAge_end(); $i++) {
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?> year<?php if($i!=1) {echo('s');} ?></option>
                                    <?php } ?>
                                  </select>
                                </label>
                              </div>
                          </div>
                          <div class="col-lg-2">
                            <a href="javascript:;" class="bt-add-kid"><img src="img/ico/main/0860-plus-circle.svg"/></a>
                          </div>
                      </div>
                        <!-- SELECT EXTRA KIDS -->
	              </div>
                  <!-- FIN SELECC CHILD-->
                  <?php 
                  $childrenReservations = PlaydateDAO::getChildrenReservationsByParent($playdate->getId_playdate(), $parent->getId_parent());
                  
                  if(($childrenReservations==null) || (count($childrenReservations)==0)) {
                  
                      if($openSpots>0) {
                          //si hay plazas libres permite reservar
                      ?>
                      
                      <div class="form-group" style="padding-top:10px;">
                        <input class="btn btn-md btn-primary bt-save-profile last" type="submit" value="Checkout" />
                      </div>
                      <?php 
                      } 
                      
                  } else { /*
                      ?>
                      <div class="form-group" style="padding-top:10px;width:100%;text-align:center">
                        <a onclick="return confirm('Do you really want to cancel your reservation?');" href="/playdate.php?action=cancel&id_playdate=<?php echo $playdate->getId_playdate()?>">Cancel Reservation</a>
                      </div>
                      <?php*/
                  }
                //if(($playdate->getStatus()!=Playdate::$STATUS_REQUEST) || (!$isParent) || ($playdate->getId_parent()!= $parent->getId_parent())) {  
                if($isParent && ($playdate->getStatus()==Playdate::$STATUS_PUBLISHED) && (!$isPast)) {
                ?>
                <!-- close form reserve -->
                </form>
                <?php } ?>
                <?php //if status=published/accepted, show closing of reservation form 
                } 
                ?>
                </div>
                
                <?php if(($playdate->getStatus()==Playdate::$STATUS_REQUEST) && ($isParent) && ($playdate->getId_parent()== $parent->getId_parent())) {  ?>
                	<div class="col-lg-12 ">
                		<form name="frm_accept" action="/playdate.php" method="post">
                		<input type="hidden" name="action" value="accept-playdate" />
                		<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                		<div class="form-group">
                    		<button class="btn btn-md btn-primary bt-save-profile last" type="submit">Accept Itinerary</button>
                 		</div>
                 		</form>
              		</div>

                	<div class="col-lg-12 ">
                		
                		<div class="form-group">
                    		<button data-toggle="modal" data-target="#playdate-declined" class="btn btn-md btn-primary bt-save-profile last bt-reserve" style="color: #838383; border: 2px solid #838383" type="submit">Decline Itinerary</button>
                 		</div>
                 		
              		</div>
              	
                <?php
                  }
                ?>
                
                <?php if($playdate->getStatus()==Playdate::$STATUS_FINISHED) { ?>
                <div class="row">
                <div class="col-lg-12">
                   <div class="form-group">
                    <button class="btn btn-md btn-primary bt-save-profile last" type="button">Create Similar Playdate</button>
                  </div>
                   <div class="form-group">
                    <button class="btn btn-md btn-primary bt-save-profile last grey" type="button" data-toggle="modal" data-target="#itinerary-changes">Check Feedback</button>
                  </div>
                </div>
                <?php } ?>
                
                <?php if($isParent) { ?>
                
              <div class="col-lg-12">
                <p class="titulo">Lower your rate</p>
                <div class="info-right">
                  <p>2 children: $18/hour/child</p>
                  <p>3 children: $16/hour/child</p>
                  <p>4-5 children: $12/hour/child</p>
                  <p>6 children: $11/hour/child</p>
                  <p>7+ children: $10/hour/child</p>
                </div>
                <div class="info-right">
                  <p>Price drops based on final number of children attending. Book more spots or invite friends to get a lower price</p>
                </div>
              </div>
              <?php } else if($isSpecialist) { ?>
              <div class="col-lg-12">
                <p class="titulo">Increase Your Pay</p>
                <div class="info-right">
                  <p>General = $25-$50/hr</p>
                  <p>Under 2 = $25-$40/hr</p>
                  <p>Ages 3-4 = $25-$34/hr</p>
                  <p>Ages 5-7 = $25-$40/hr</p>
                  <p>Ages 7-8 = $25-$40/hr</p>
                </div>
                <div class="info-right">
                  <p>Support Specialists can make $23-$25/hr</p>
                </div>
              </div>
              <?php } ?>

              <div class="col-lg-12 ">
                  <p class="titulo">
                  <?php if($isParent) { ?>
                  Invite Friends
                  <?php } else if($isSpecialist) { ?>
                  Invite Parents
                  <?php } ?> 
                  </p>
                  <form name="frm_invite" action="/playdate.php?id_playdate=<?php echo $playdate->getId_playdate() ?>" method="post">
                	<input type="hidden" name="action" value="invite-friends" />
                	<input type="hidden" name="id_playdate" value="<?php echo $playdate->getId_playdate() ?>" />
                  	<label for="inputInviteFriends" class="sr-only">Username or email</label>
                  	<input type="text" id="inputInviteFriends" class="form-control noMarginTop input-invite-friend" name="inviteFriendsPopup[]" placeholder="Username or email">
                  	<?php if($invited) { ?>
                  	<p>Invitation has been sent</p>
                  	<?php } ?>
                  	<p class="errorLocation"></p>
                  	<div class="container-tags container-tags-email"></div>
                  	<div class="form-group">
                  	<?php if($isParent) { ?>
                    	<button class="btn btn-md btn-primary bt-save-profile last" type="submit" >Invite Friends</button>
                    <?php } else if($isSpecialist) { ?>
                    	<button class="btn btn-md btn-primary bt-save-profile last" type="submit" >Invite Parents</button>
                    <?php } ?>
                    </div>
                  </form>
              </div>
              <?php /*?>
              <div class="col-lg-12">
                <p class="titulo">NYC attractions</p>
                <div class="info-right">
                  <p><strong>Bronx Zoo Day Trip</strong></p>
                  <p>May 20 | 10am-18pm</p>
                </div>

                <div class="info-right">
                  <p><strong>Color Exploring</strong></p>
                  <p>May 21 | 12pm-14pm</p>
                </div>

                <div class="info-right">
                  <p><strong>Outdoors We Go</strong></p>
                  <p>May 22 | 4pm-8pm</p>
                </div>
              </div>
              */?>
              <div class="col-lg-12">
                <p class="titulo">Led by <?php echo $specialist->getName() ?></p>
                <?php 
                $playdatesSpec = PlaydateDAO::getUpcomingPlaydatesListBySpecialist($specialist->getId_specialist(), 1, 3, null, null);
                
                foreach ($playdatesSpec as $auxPdSpec) {
                ?>
                <div class="info-right">
                  <p><a href="/playdate.php?id_playdate=<?php echo $auxPdSpec->getId_playdate() ?>" style="text-decoration:none;"><strong><?php echo $auxPdSpec->getName() ?></strong></a></p>
                  <p style="color:#878787;"><?php echo Utils::dateFormatShort($auxPdSpec->getDate())?> | <?php echo Utils::get12hourFormat($auxPdSpec->getTime_init())?>-<?php echo Utils::get12hourFormat($auxPdSpec->getTime_end())?></p>
                </div>
				<?php } ?>
                
              </div>
            </div>
            
          </div>
          
          <?php /*?>
          <div class="row row-tabs">
            <div class="col-lg-12">
            
              <p class="titulo">NYC Attractions</p>

              <div class="info-right">
                <p><strong>Bronx Day Trip</strong></p>
                <p>Jan 13 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Botanical Garden</strong></p>
                <p>Jan 35 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Central Park Farm</strong></p>
                <p>Feb 13 I 12pm-2pm</p>
              </div>
              

            </div>
          </div>

          <div class="row row-tabs">
            <div class="col-lg-12">
            
              <p class="titulo">7-8 Years Old</p>

              <div class="info-right">
                <p><strong>Painting and Fun</strong></p>
                <p>Jan 13 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Dance Party</strong></p>
                <p>Jan 35 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Muppets Theater</strong></p>
                <p>Feb 13 I 12pm-2pm</p>
              </div>
              

            </div>
          </div>
          */?>



        </div>
		

        </div>

        <!-- Content -->

      </div>


    



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
    <?php if($reserved) {?>
	<script>$('#playdate-reserved').modal('show');</script>
	<?php } ?>
	<?php if($accepted) {?>
	<script>$('#playdate-accepted').modal('show');</script>
	<?php } ?>
  <span id="x" style="display:none"></span><span id="y" style="display:none"></span>
  
  	<script>
		function updatePlaydatePrice() {
			var numKids = 0;
			 $("#frm_reserve .kids-playdates input[type='checkbox']").each(function(e){ if($(this).is(':checked')) { numKids++;} })
			//calculate price
			$.get( "playdate-price.php?id_playdate=<?php echo $playdate->getId_playdate() ?>&addonsprice=<?php echo $addonsPrice ?>&num_kids="+numKids, function( data ) {
				$( ".totalPrice" ).html( data );
			});
			
		}


		<?php if($isCheckout) { ?>
			$("#playdate-checkout").modal('show');
		<?php } ?>
  	</script>
  
	 

  
  </body>
<script>
  /*$(document).ready(function(){
    var address = $(".address").html()+ " " + $(".city").html();
    var re = / /g;
    address= address.replace(re,"+")
    var urlMap= "";
    $(".container-map").append('<iframe src="https://www.google.com/maps/embed?q='+address+'" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>')
  })*/
  
</script>


</html>

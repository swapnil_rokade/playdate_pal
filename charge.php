<?php
/**
 * PLAYDATES - CHARGE PAGE
 * 
 * Stripe will call this page after charging parents money, so we create charge and update parent credit 
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);


require_once('./stripe-config.php');

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");
if((!isset($isParent)) || (!$isParent)) {
    header("Location: /");
}
//Parent connected. Get full info
$parent = ParentDAO::getParent($_SESSION["parent_id"]);

$token  = $_POST['stripeToken'];
$email  = $_POST['stripeEmail'];

$amount = 0; //minimo = 10$
$description = "Playdate Playpacks";

if(isset($_SESSION['amount_charge']) && ($_SESSION['amount_charge']!=null) && is_numeric($_SESSION['amount_charge'])) {
    $amount = $_SESSION['amount_charge'];
    $_SESSION['amount_charge'] = null;
}else if(isset($_REQUEST['callback_page']) && isset($_REQUEST['amount'])){
    $amount = $_REQUEST['amount']*100;
}

/*
if(isset($_REQUEST["amount"]) && ($_REQUEST["amount"]!=null) && is_numeric($_REQUEST["amount"])) {
    $amount = $_REQUEST["amount"]*100;
}
*/

if(isset($_REQUEST["description"]) && ($_REQUEST["description"]!=null) && ($_REQUEST["description"]!="")) {
    $description = $_REQUEST["description"];
}


$customer = \Stripe\Customer::create(array(
    'email' => $email,
    'source'  => $token
));

$charge = \Stripe\Charge::create(array(
    'customer' => $customer->id,
    'amount'   => $amount,
    'currency' => 'usd'
));

//Charged done. Increase amount
$prevCredit = $parent->getCredit();
if($prevCredit==null) {$prevCredit = 0;}

//Almacenamos info de purchase
$newPurchase = new Purchase();
$newPurchase->setId_parent($parent->getId_parent());
$newPurchase->setAmount($amount);
$newPurchase->setDescription($description);
PurchaseDAO::createPurchase($newPurchase);

//Incrementamos el saldo
ParentDAO::updateParentCredit($parent->getId_parent(), $prevCredit+$amount);

//Enviamos mail confirmación
MailUtils::sendPlaypackConfirm($parent->getEmail(), $parent->getName(), date('m/d/Y h:i:s a', time()), Utils::moneyFormat($amount));

//Admin mail
MailUtils::sendPlaypackConfirmToAdmin($parent->getEmail(), $parent->getName(), date('m/d/Y h:i:s a', time()), Utils::moneyFormat($amount));

if(isset($_REQUEST['callback_page'])){
    $_SESSION['charge-done'] = 'Charge Done';
    header ("Location: ".$_REQUEST['callback_page']);
}else{
    header ("Location: /my-dashboard.php#playpacks");
}
?>

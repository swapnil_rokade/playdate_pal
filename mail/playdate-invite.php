<?php
/**
 * PLAYDATE - SEND INVITATION MAIL
 */

include_once("../common.php");

include_once("../classes/all_classes.php");
include_once("../connection.php");


require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$email = null;
if(isset($_REQUEST['email']) && ($_REQUEST['email']!=null)) {
    $email = $_REQUEST['email'];
}
$parentName = "Juan Carlos";
$invitationCode = "AAAAAA";
$subject = "You�ve Been Invited to PAL!";


if(($email!=null) && ($invitationCode!=null)) {

    $template = file_get_contents("templates/playdate-invitation.html");
    
    $template = str_replace('{{ domain }}', $EMAIL_BASEDOMAIN, $template);
    $template = str_replace('{{ parent_name }}', $parentName, $template);
    $template = str_replace('{{ email }}', $email, $template);
    $template = str_replace('{{ invitation_code }}', $invitationCode, $template);
    
    $mail = new PHPMailer(true);   // Passing `true` enables exceptions
    
    $emailSent = false;
    
    try {
        //Server settings
        $mail->SMTPDebug = 2;                                 // Enable verbose debug output
        //$mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $EMAIL_HOST;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = false;                               // Enable SMTP authentication
        $mail->Username = $EMAIL_USERNAME;                 // SMTP username
        $mail->Password = $EMAIL_PASSWORD;                           // SMTP password
        //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = $EMAIL_SECURE;                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $EMAIL_PORT;                                    // TCP port to connect to
        
        //Recipients
        $mail->setFrom($EMAIL_USERNAME, $EMAIL_FROMNAME);
        $mail->addAddress($email);     // Add a recipient
        $mail->addCc($EMAIL_USERNAME, $EMAIL_FROMNAME);     // Add a recipient
        $mail->addReplyTo($EMAIL_USERNAME, $EMAIL_FROMNAME);
        
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $template;
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        $mail->send();
        
        if(isset($backUrl) && ($backUrl!=null)) {
            header("Location: ".$backUrl);
        }
        
        /*
        $sqlUpdt = "update gpdr_emails set fec_sent=now() where id_email=".$id;
        $linkUpdt = getConnectionMysqli();
        mysqli_query($linkUpdt, $sqlUpdt);
        mysqli_close($linkUpdt);
        */
        
        $emailSent = true;
        
    } catch (Exception $e) {
        echo 'Error sending email: ', $mail->ErrorInfo;
    }
} //if email and code are not null
?>

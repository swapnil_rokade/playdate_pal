<?php 
/**
 * PLAYDATE - GROUPS SECTION 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

include_once("checkprofile.php");

//Get search criteria
$keyword = null;
if(isset($_REQUEST["keyword"]) && ($_REQUEST["keyword"]!="")) {
    $keyword = $_REQUEST["keyword"];
}

//location
$neighborhoods = null;
if(!empty($_POST['id_neighborhood'])) {
    $neighborhoods = array(); 
    foreach($_POST['id_neighborhood'] as $auxlocation) {
        $neighborhoods[] = $auxlocation;
    }
}
 
$selectedInterests = null;
if(!empty($_POST['interests'])) {
    $selectedInterests = array();
    foreach($_POST['interests'] as $interest) {
        $selectedInterests[] = $interest;
    }
}


$action = null;
if(isset($_REQUEST["action"])) {
    $action = $_REQUEST["action"];
}

$allgroups = GroupDAO::getGroupsList(1, 18, null, null);
$groups = $allgroups;

switch ($action) {
    case "search": {
        //Filter contents with criteria
        if($keyword!=null) {
            $filteredgroups = array();
            foreach($groups as $auxGrp) {
                if(strpos(strtolower($auxGrp->getDescription()), strtolower($_REQUEST["keyword"])) !==false) {
                    $filteredgroups[] = $auxGrp;
                } else if(strpos(strtolower($auxGrp->getName()), strtolower($_REQUEST["keyword"])) !==false) {
                    $filteredgroups[] = $auxGrp;
                }
            }
            $groups = $filteredgroups;
        }
        
        //Neighborhoods
        if($neighborhoods!=null) {
            $filteredgroups = array();
            foreach($groups as $auxGrp) {
                $pdNeighList = NeighborhoodDAO::getNeighborhoodsListByGroup($auxGrp->getId_group());
                $included = false;
                foreach ($pdNeighList as $auxNeigh) {
                    if(in_array($auxNeigh->getId_neighborhood(), $neighborhoods)) {
                        $filteredgroups[] = $auxGrp;
                    }
                }
            }
            $groups = $filteredgroups;
        }
        
        //Interests
        if($selectedInterests!=null) {
            $filteredgroups = array();
            foreach($groups as $auxGrp) {
                $pdIntList = InterestDAO::getInterestsByGroup($auxGrp->getId_group());
                $included = false;
                foreach ($pdIntList as $auxInt) {
                    if(in_array($auxInt->getId_interest(), $selectedInterests)) {
                        $filteredgroups[] = $auxGrp;
                    }
                }
            }
            $groups = $filteredgroups;
        }
        
        
        
        
        break;
    }
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Groups - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Groups</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar">
		  <form name="frm_search" action="/groups.php" method="post">
		  <input type="hidden" name="action" value="search" />
          <!-- Search -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#search-container" role="button" aria-expanded="false" aria-controls="search-container">Search</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="search-container">
                
                  <label for="inputSearch" class="sr-only">Search</label>
                  <input type="text" id="inputSearch" class="form-control" placeholder="keyword" name="keyword" value="<?php echo(($keyword!=null)?$keyword:"") ?>">
                
              </div>
            </div>
          </div>


          <!-- Location -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#location-container" role="button" aria-expanded="false" aria-controls="location-container">Location</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="location-container">
                
                  <label for="inputLocation" class="sr-only">Location</label>
                  <input type="text" id="inputLocation" class="form-control inputLocation" placeholder="neighborhood">
                  <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                
                <div class="container-tags">
                  <?php 
                  if(($neighborhoods!=null) && (count($neighborhoods)>0)) {
                      foreach ($neighborhoods as $auxNeigh) {
                          $nei = NeighborhoodDAO::getNeighborhood($auxNeigh);
                          if($nei!=null) {
                  ?>
					<div class="tag">
                    	<span><?php echo $nei->getName()?></span>
                        <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                        <input type="hidden" value="<?php echo $nei->getId_neighborhood() ?>" name="id_neighborhood[]">
                    </div>
                  <?php 
                          }
                      }
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>

           <!-- Categories -->
          <a class="btn btn-primary bt-sidebar open" data-toggle="collapse" href="#categories-container" role="button" aria-expanded="false" aria-controls="categories-container">Categories</a>
          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse show" id="categories-container">
                
				  <?php 
                  	if($selectedInterests==null) {
                  	    //Para evitar error si no hay filtro
                  	    $selectedInterests=array();
                  	} 
                  ?>
                  <div class="list-container stem">
                    <div class="form-group">
                      <input type="checkbox" id="0" class="form-control" placeholder="keyword" name="interests[]" value="1" <?php echo((in_array(1, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="0">Academic</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="1" class="form-control" placeholder="keyword" name="interests[]" value="2" <?php echo((in_array(2, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="1">STEM</label>
                    </div> 
                  </div>
                  
                  <div class="list-container creative">
                    <div class="form-group">
                      <input type="checkbox" id="2" class="form-control" placeholder="keyword" name="interests[]" value="3" <?php echo((in_array(3, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="2">Creative</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="0" class="form-control" placeholder="keyword" name="interests[]" value="4" <?php echo((in_array(4, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="3">Performing Arts</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="4" class="form-control" placeholder="keyword" name="interests[]" value="5" <?php echo((in_array(5, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="4">Language & Culture</label>
                    </div> 
                  </div>

                  <div class="list-container outdoor">
                    <div class="form-group">
                      <input type="checkbox" id="5" class="form-control" placeholder="keyword" name="interests[]" value="6" <?php echo((in_array(6, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="5">Play</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="6" class="form-control" placeholder="keyword" name="interests[]" value="7" <?php echo((in_array(7, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="6">Sports & Recreation</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="7" class="form-control" placeholder="keyword" name="interests[]" value="8" <?php echo((in_array(8, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="7">Health & Wellness</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="8" class="form-control" placeholder="keyword" name="interests[]" value="9" <?php echo((in_array(9, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="8">Outdoor</label>
                    </div>
                  </div>

                  <div class="list-container attractions">
                    <div class="form-group">
                      <input type="checkbox" id="9" class="form-control" placeholder="keyword" name="interests[]" value="10" <?php echo((in_array(10, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="9">NYC Museums</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="10" class="form-control" placeholder="keyword" name="interests[]" value="11" <?php echo((in_array(11, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="10">NYC Attractions</label>
                    </div> 
                    <div class="form-group">
                      <input type="checkbox" id="11" class="form-control" placeholder="keyword" name="interests[]" value="12" <?php echo((in_array(12, $selectedInterests))?"checked=\"checked\"":""  ) ?>>
                      <label for="11">NYC Theater</label>
                    </div> 
                  </div>
                
              </div>
            </div>
          </div>

		  <div class="row">
            <div class="col">
            	<input type="submit" class="bt-save-profile last" value="Apply Filter">
            </div>
          </div>
          <div class="row">
            <div class="col">
            	<input type="button" class="bt-save-profile last" value="Clear Filter" onclick="top.location='/groups.php'">
            </div>
          </div>
		</form>
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <div class="row">
          
          	<?php foreach ($groups as $auxGroup) { 
          	    
          	    $link = "/group-public-profile.php?id_group=".$auxGroup->getId_group()."&slug=".$auxGroup->getSlug();
          	    $ingroup = false;
          	    if($isParent) {
          	        $ingroup = GroupDAO::isParentInGroup($_SESSION["parent_id"], $auxGroup->getId_group());
          	    } else if ($isSpecialist) {
          	        $ingroup = GroupDAO::isSpecialistInGroup($_SESSION["spec_id"], $auxGroup->getId_group());
          	    }
          	    
          	    if($ingroup) {
          	        $link = "/groups-dashboard.php?id_group=".$auxGroup->getId_group()."&slug=".$auxGroup->getSlug();
          	    }
          	    
          	    $interestName = "";
          	    $interestType = "";
          	    
          	    $interests = InterestDAO::getInterestsByGroup($auxGroup->getId_group());
          	    $auxInterest = null;
          	    if(count($interests)>0) {
          	        $auxInterest = $interests[0];
          	        $interestName =$auxInterest->getName();
          	        
          	        switch ($auxInterest->getType()) {
          	            case Interest::$TYPE_TRAINING: {
          	                $interestType=" stem";
          	                break;
          	            }
          	            case Interest::$TYPE_ART: {
          	                $interestType=" creative";
          	                break;
          	            }
          	            case Interest::$TYPE_PLAY: {
          	                $interestType=" outdoor";
          	                break;
          	            }
          	            case Interest::$TYPE_CULTURAL: {
          	                $interestType=" attractions";
          	                break;
          	            }
          	        }
          	    }
          	
          	    $groupMembers = 0;
          	    $groupMembers += GroupDAO::countGroupParents($auxGroup->getId_group());
          	    $groupMembers += GroupDAO::countGroupSpecialists($auxGroup->getId_group());
          	    
          	    $grpPicture = $auxGroup->getPicture();
          	    if(($grpPicture==null) || ($grpPicture=="")) {
          	        $grpPicture = "/img/default-img-group-profile.jpg";
          	    }
          	?>
            <div class="col-lg-4">
              <div class="box-playdates <?php echo $interestType?>">
                <div class="img-container">
                  <a href="<?php echo $link ?>"><img src="<?php echo $grpPicture ?>" alt="" style="height: 195px;"/></a>
                  <span class="cat-box"><?php echo $interestName?></span>
                </div>
                <div class="info" style="height:100px">
                  <a href="<?php echo $link ?>"><span class="title"><?php echo $auxGroup->getName()?></span></a>
                  <span class="subtitle"><?php echo $auxGroup->getZipcode()?></span>
                  <span class="subtitle members"><?php echo $groupMembers ?> members</span>
                </div>
                 <div class="data" style="height:112px">
                  <p><?php echo $auxGroup->getDescription(100)?> </p>
                </div>
                <div class="reserve" style="height:100px">
                  <a href="<?php echo $link ?>" class="bt-reserve">Learn more</a>
                </div>
              </div>
            </div>
            <?php } ?>

            

          </div>

           <div class="row info-box">

            <div class="col-lg-12">
              <div class="titulo noImages">
                <h2>Create a Group</h2>
                <div class="create-step1">                  
                  <p>As a Parent, you can create a group to coordinate care with families in your network or community. As a PAL Partner, you can create a group to facilititate playdate experiences at your location or during one of your classes.</p>
                    <a class="bt-titulo bt-get-started" href="javascript:;" >Get Started</a>
                </div>
                <div class="create-group">
                  <!--<p>Create a New Group</p>-->
                  <?php 
                  $actionFormUrl = "#"; //Action where group should be created
                  if($isSpecialist) {
                      $actionFormUrl = "/specialist-dashboard.php#groups";
                  }
                  
                  if($isParent) {
                      $actionFormUrl = "/my-dashboard.php#groups";
                  }
                  ?>
                  <form class="form-create-group" action="<?php echo $actionFormUrl ?>" method="post">
                    <input type="hidden" name="action" value="creategroup" />
                    <label for="inputGroupName" class="sr-only">Group Name</label>
                    <input type="text" id="inputGroupName" class="form-control" placeholder="Group Name" required="required" name="name" />
                    <?php /*
                    <p class="text-invite">Invite friends</p>
                    <div class="mail-container">
                      <label for="inputInviteUser" class="sr-only">Username or Email</label>
                      <div class="cont-invite">
                        <input type="text" id="inputInviteUser" class="form-control" placeholder="Username or Email" required="" ><a href="javascript:;" class="bt-invite" title=""><img src="img/bt-add.png" alt=""/></a>
                      </div>
                    </div>
                    */?>
                    <button class="btn btn-md btn-primary bt-titulo" type="submit">Create Group</button>  
                  </form>
                </div>
              </div>
              
            </div>

          </div>

        
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

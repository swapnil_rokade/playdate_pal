<?php
/**
 * CHECK IF PARENT PROFILE IS COMPLETED
 */

if($isParent) {
    $completed = ParentDAO::isCompletedProfile($_SESSION["parent_id"]);
    if(!$completed) {
        header("Location: /my-dashboard.php#profile");
    }
}
?>
<?php 
/**
 * PLAYDATE - VIEW PLAYDATE 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$playdate=new Playdate();
$playdate->readFromRow($_REQUEST);
if($playdate->getId_playdate()>0) {
    $playdate= PlaydateDAO::getPlaydate($playdate->getId_playdate());
}

$specialist = null;
if($playdate->getId_specialist()!=null) {
    $specialist = SpecialistDAO::getSpecialist($playdate->getId_specialist());
}

?>
<!DOCTYPE html>
<html lang="en">

  <head> 

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $playdate->getName() ?> - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page create-playdate itinerary reserve<?php echo($isConnected?" connected":"") ?>">
    
    <!-- Itinerary accepted -->
    <div class="modal fade itinerary-modal" id="playdate-reserved" class="itinerary-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="itinerary-modal-heading">PLAYDATE SUCCESSFULLY RESERVED</h2>
            
            <div class="row row-tabs">
              <h2 class="itinerary-modal-heading noUppercase"><?php $playdate->getName() ?></h2>
              <p><?php echo $playdate->getDate() ?>   |   <?php echo $playdate->getTime_init() ?> to <?php echo $playdate->getTime_end() ?> h.</p>
              <p><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?></p>
            </div>
            <div class="row row-tabs">
              <p>Booking will be confirmed by </p>
              <p class="ppal-date">May 20 2018</p>
            </div>
            <div class="row row-tabs">
              <p>when at least <strong>2 open spots</strong> are reserved</p>
            </div>

            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-top" href="#" title="">Invite Friends</a></div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-top" href="my-dashboard.html" title="">Your Reserved Playdates</a></div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"><a class="bt-link-reserved-playdate" href="#" title="">Pricing and Cancellation Policy</a></div>
              <div class="col-lg-2"></div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Itinerary accepted -->

     <!-- Itinerary changes -->
    <div class="modal fade itinerary-modal" id="itinerary-changes" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h2 class="itinerary-modal-heading">CHANGE OR DECLINE ITINERARY</h2>
            
            <div class="row row-tabs">
              <h2 class="itinerary-modal-heading noUppercase">Learn Spanish Sing Along in Soho</h2>
              <p>Jan 13th 2018   |   12pm-2pm</p>
              <p>Soho, NYC</p>
            </div>
            <div class="row row-tabs">
              <p>Submited by</p>
              <p class="ppal-date">Monica Allen</p>
            </div>
            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8">                    
                 <label for="changeItinerary" class="sr-only">Tell the specialist what could be improved</label>
                 <textarea class="form-control" placeholder="Tell the specialist what could be improved" id="changeItinerary" rows="7"></textarea>
              </div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8">
                <a class="bt-top" href="#" title="">Request changes</a>
                <a class="bt-top grey" href="#" title="">Decline Itinerary</a>
              </div>
              <div class="col-lg-2"></div>
            </div>

            <div class="row row-tabs">
              <div class="col-lg-2"></div>
              <div class="col-lg-8"></div>
              <div class="col-lg-2"></div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Itinerary changes -->


    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1><?php echo $playdate->getName() ?></h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
          <div class="box-playdates">
            <div class="img-container">
              <img src="<?php echo $specialist->getPicture() ?>" alt="">
            </div>
            <div class="info">
              <span class="title"><?php echo $specialist->getFullName() ?></span>
              <span class="subtitle">
				<?php if($specialist->getProfile()==Specialist::$PROFILE_LEAD) { ?>
              	Lead
              	<?php } else if($specialist->getProfile()==Specialist::$PROFILE_SUPPORTER) { ?>
              	Supporter
              	<?php } else if($specialist->getProfile()==Specialist::$PROFILE_NURSE) { ?>
              	Nurse
              	<?php } ?>
			  </span>
            </div>
            <div class="puntuacion">
              <ul>
                <li class="active"></li>
                <li class="active"></li>
                <li class="active"></li>
                <li class="active"></li>
                <li class="active"></li>
              </ul>
              <span class="num">5/5</span>
            </div>
            <div class="data">
              <p><?php echo $specialist->getAbout_me(250) ?></p>
              <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist()?>&slug=<?php echo $specialist->getSlug() ?>" title="" class="bt-profile">View full Profile</a>
            </div>
            <div class="reserve">
              <a href="#" class="bt-reserve">Get in touch</a>
            </div>
          </div>

		  <?php /* ?>
          <div class="box-playdates">
            <div class="img-container">
              <img src="img/img-box-specialist-2.jpg" alt="">
            </div>
            <div class="info">
              <span class="title">Lucy Tello</span>
              <span class="subtitle">Supporter</span>
            </div>
            <div class="puntuacion">
              <ul>
                <li class="active"></li>
                <li class="active"></li>
                <li class="active"></li>
                <li class="active"></li>
                <li class="active"></li>
              </ul>
              <span class="num">5/5</span>
            </div>
          </div>
           */?>
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-6">
            <div class="row">

              

              <div class="col-lg-6">
                <div class="itinerary-info">
                  <p class="name"><?php echo $playdate->getName() ?></p>
                  <p class="place"><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?></p>
                  <p class="date"><?php echo $playdate->getDate() ?> <br/> <?php echo $playdate->getTime_init() ?> to <?php echo $playdate->getTime_end() ?> h.</p>
                  <p class="reserve">RSVP by <?php echo Utils::dateFormat(date('d/m/Y', strtotime('-2 day', strtotime(BaseDAO::toMysqlDateFormat($playdate->getDate()))))); ?></p>
                  <p class="age">Ages: <?php echo $playdate->getAge_init() ?> - <?php echo $playdate->getAge_end() ?></p>
                  <p class="participants">Participants: <?php echo $playdate->getNum_children() ?></p>
                  <p class="spots">Open Spots: <?php echo $playdate->getOpen_spots() ?></p>
                </div>
              </div>

              <div class="col-lg-6 ">
                <div class="itinerary-extras">

                  <div class="form-group">
                    <p class="totalPrice">$60-$80</p>
                  </div>
                  
                  <?php 
                    $addons = AddonDAO::getAddonListByPlaydate($playdate->getId_playdate());
                    foreach ($addons as $auxAddon) {
                        $price = "";
                        if($auxAddon->getPrice()>0) {
                            $price = " ($ ".round($auxAddon->getPrice()/100,2).")"; 
                        }
                  ?>
              
                  <div class="form-group">
                    <input type="checkbox" id="<?php echo $auxAddon->getId_addon()?>" class="form-control" placeholder="keyword"  >
                    <label for="1">+ <?php echo $auxAddon->getName()?><?php echo $price ?></label>
                  </div> 
                  <?php } ?>
                  <div class="form-group">
                    <button class="btn btn-md btn-primary bt-save-profile grey" type="button" onclick="javascript:;">Create Similar Playdate</button>
                  </div>
                
                </div>
              </div>
            </div>

            <div class="row row-tabs">
               <div class="col-lg-6">
               <?php 
                $interests = InterestDAO::getInterestsByPlaydate($playdate->getId_playdate());
                foreach ($interests as $auxInterest) {
                    $interestName =$auxInterest->getName();
                    $interestType = "";
                    switch ($auxInterest->getType()) {
                        case Interest::$TYPE_TRAINING: {
                            $interestType=" stem";
                            break;
                        }
                        case Interest::$TYPE_ART: {
                            $interestType=" creative";
                            break;
                        }
                        case Interest::$TYPE_PLAY: {
                            $interestType=" outdoor";
                            break;
                        }
                        case Interest::$TYPE_CULTURAL: {
                            $interestType=" attractions";
                            break;
                        }
                    }
                    
               ?>
                <p class="category <?php echo $interestType ?>"><?php echo $interestName ?></p>
               <?php } ?>
              </div>
              
            </div>

            <div class="row">

               <div class="col-lg-12 description">
                <div class="data">
                  <p><strong>Description</strong></p>
                  <p><?php echo $playdate->getDescription() ?></p>
                </div>
                <div class="data">
                  <p><strong>Itinerary</strong></p>
                  <?php 
                  $itinerary = ItineraryDAO::getItineraryListByPlaydate($playdate->getId_playdate());
                  foreach ($itinerary as $auxItinerary) {
                  ?>
                  <p><?php echo $auxItinerary->getHour() ?> h. - <?php echo $auxItinerary->getActivity() ?></p>
                  <?php } ?>
                </div>
                <div class="data">
                  <p>Pricing and Cancelation Policy</p>
                  <p>Price will be confirmed the next day from RSVP date.</p>
                  <p>Cancellation Fee of $10 within 48hrs or $5 within a week.</p>
                </div>
               </div>

          </div>

 		  <?php /* ?>
          <div class="row row-tabs">
            
           
            <div class="col-lg-12">
              <div class="admin-slider front">
                <div class="all-slides">
                  <div class="slide selected"><img src="img/box-home-1.jpg"></div>
                  <div class="slide"><img src="img/box-home-2.jpg"></div>
                  <div class="slide"><img src="img/box-home-3.jpg"></div>
                  <div class="slide"><img src="img/box-home-4.jpg"></div>
                  <div class="nav">
                    <a href="javascript:;" title="" class="bt-slider-left"><img src="img/fle-slider.png"/></a>
                    <a href="javascript:;" title="" class="bt-slider-right"><img src="img/fle-slider.png"/></a>
                  </div>
                </div>
                
              </div>
            </div>
           
          </div>
          */ ?>
          
          <?php /* ?>
          <div class="row row-tabs">
              <div class="col-lg-12 location">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.77558625915!2d-73.99660068459471!3d40.72295647933074!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25985f6c449b7%3A0xfde5ebd54379bf7c!2s23+Prince+St%2C+New+York%2C+NY+10012%2C+EE.+UU.!5e0!3m2!1ses!2ses!4v1524389184500" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>              
          </div>
            */?> 
            
          <div class="row">
			   <?php $zipCode = ZipcodeDAO::getZipcode($playdate->getLoc_id_zipcode()) ?>
               <div class="col-lg-12 description">
                <div class="data">
                  <p><?php echo $playdate->getLoc_address() ?></p>
                  <p><?php echo $playdate->getLoc_city() ?>, <?php echo $playdate->getLoc_state() ?>, <?php echo $zipCode->getZipcode() ?></p>
                </div>
               </div>

          </div>
        </div>

        <div class="col-lg-3">
          <div class="row">
            <div class="col-lg-12">

              <p class="titulo">Playdates near you</p>

              <div class="info-right">
                <p><strong>Bronx Zoo Day Trip</strong></p>
                <p>May 20 | 10am-18pm</p>
              </div>

              <div class="info-right">
                <p><strong>Color Exploring</strong></p>
                <p>May 21 | 12pm-14pm</p>
              </div>

              <div class="info-right">
                <p><strong>Outdoors We Go</strong></p>
                <p>May 22 | 4pm-8pm</p>
              </div>
              

            </div>
          </div>
          
          <?php /*?>
          <div class="row row-tabs">
            <div class="col-lg-12">
            
              <p class="titulo">NYC Attractions</p>

              <div class="info-right">
                <p><strong>Bronx Day Trip</strong></p>
                <p>Jan 13 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Botanical Garden</strong></p>
                <p>Jan 35 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Central Park Farm</strong></p>
                <p>Feb 13 I 12pm-2pm</p>
              </div>
              

            </div>
          </div>

          <div class="row row-tabs">
            <div class="col-lg-12">
            
              <p class="titulo">7-8 Years Old</p>

              <div class="info-right">
                <p><strong>Painting and Fun</strong></p>
                <p>Jan 13 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Dance Party</strong></p>
                <p>Jan 35 I 12pm-2pm</p>
              </div>

              <div class="info-right">
                <p><strong>Muppets Theater</strong></p>
                <p>Feb 13 I 12pm-2pm</p>
              </div>
              

            </div>
          </div>
          */?>
          <div class="row row-tabs">
            <div class="col-lg-12">
            
              <p class="titulo">Led by <?php echo $specialist->getName() ?> </p>

              <div class="info-right">
                <p><strong>Color Exploring</strong></p>
                <p>May 21 | 12pm-14pm</p>
              </div>
              
           </div>
          </div>



        </div>


        </div>

        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

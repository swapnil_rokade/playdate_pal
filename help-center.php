<?php 
/**
 * PLAYDATE - HOME 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$backUrl = "/help-center.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=1.2" rel="stylesheet">

  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>FAQ's</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row help-center">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar">
          <div class="row">
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type general">
                <img src="img/ico/white/0003-home3.svg" alt="" />
                <span class="texto">General</span>
              </a>
            </div>
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type account">
                <img src="img/ico/white/0287-user.svg" alt="" />
                <span class="texto">Account</span>
              </a>
            </div>            
          </div>
          <div class="row">
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type parents">
                <img src="img/ico/white/0299-baby2.svg" alt="" />
                <span class="texto">Parents</span>
              </a>
            </div>
            <div class="col-lg-6 bt-container">
              <a href="javascript:;" class="bt-help-type providers">
                <img src="img/ico/white/0215-reading.svg" alt="" />
                <span class="texto">Specialists</span>
              </a>
            </div>
            
          </div>
        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9 all-faqs">

          <div class="col-lg-12">
            <div class="no-results"><img src="img/cactus.png"/>
              <p>No faqs available</p>
            </div>
          </div>


          <div id="accordion">

            <div class="card general">
              <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Where is PAL available?
                  </button>
                </h5>
              </div>
              <div id="collapseOne" class="collapse  show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                  <p>Currently PAL is only available in the New York City area. We will be expanding to additional cities in the near future. You can <a href="/playdates.php">nominate your city</a> by visiting the “Playdates” page and clicking “nominate” under the Location section.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading2b">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse2b" aria-expanded="false" aria-controls="collapse2b">
                    How do I sign up?
                  </button>
                </h5>
              </div>
              <div id="collapse2b" class="collapse" aria-labelledby="heading2b" data-parent="#accordion">
                <div class="card-body">
                  <p>Parents - PAL is an invite-only community. You will need to receive an access code to register. You can <a href="/parent-sign-up.php">request an invitation</a>. When requesting an invitation, you will need to verify your identity by submitting additional information to <a href="mailto:parentconcierge@playdatepal.com">parentconcierge@playdatepal.com</a> and completing an interview. It's free to sign up!</p>
                  <p>Specialists - If you are interested in being a Specialist, you can <a href="/apply-specialist.php">submit basic information</a> about yourself including your name, location and why you want to join our network. You will be required to pass our comprehensive vetting process to join to the PAL community. </p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading2">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    What is a PAL Group?
                  </button>
                </h5>
              </div>
              <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                <div class="card-body">
                  <p>A PAL group is a private community of Parents with shared child care needs administered by Parents or third party Partners. Group administrators are authorized to manage group participation and child care planning.</p>
                  <p>Once logged in, visit the Group page. You can <a href="/groups.php">join a group</a> by requesting an invite to any group of interest. You can also <a href="/groups.php">create a group</a> by scrolling to the bottom of the page.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading3">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                    What is the difference between a Lead and Support Specialist?
                  </button>
                </h5>
              </div>
              <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                <div class="card-body">
                  A Support Specialist has successfully completed the PAL vetting process. This includes a background check, personality test, reference check, online application and online interview. </p>
                  <p>All Lead Specialists, in addition to completing the vetting process, has received his or her First Aid and CPR Certifications and has pursued continued learning as a child care provider. A Lead Specialist has also completed at least 10 hours in a Support Specialist role, received consistent positive feedback from Parents and approval from another pre-approved Lead Specialist. Every playdate is required to have at least one Lead Specialist to facilitate the experience.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading4">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                    Can you help me find a place to host my Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                <div class="card-body">
                  <p>Third party hosts can be found on the <a href="/groups.php">Groups</a> page. Once logged in, you can request an invite to a group to inquire about a playdate you are interested in planning at their site.</p>
                  <p>We also recommend considering the following: a public attraction, local kid-friendly restaurant, local playground, in-home, or in-school (if approved) playdate location.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading5">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                    What is the adult to child ratio?
                  </button>
                </h5>
              </div>
              <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                <div class="card-body">
                  <img src="img/img-suggested-ratio.jpg" width="100%" />
                  <p>Under 3 - Nurse Care without Parents, coming soon!<br>3-4 yrs - 4 children to 1 adult<br>5-7 yrs - 5 children to 1 adult<br>+7 or Under 3 Parent Social - 6 children to 1 adult<br></p>
                  <p>*Additional Support Specialists can be hired with an add-on fee. PAL suggests an extra Specialist for children under 7 for playdates at public attractions.</p>
                  <p>These are suggested ratios. Parents or PAL Specialists can request an extra Support Specialist at an additional cost of $23 per hour split among all Parents within the playdate.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading6">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                    What is the vetting process for PAL Specialists?
                  </button>
                </h5>
              </div>
              <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                <div class="card-body">
                  <p>All PAL Specialists must complete the following:</p>
                  <ul>
                    <li>Online Application </li>
                    <li>Day100 Personality Test </li>
                    <li>Day100 Reference Check</li>
                    <li>Video or In-Person Interview </li>
                    <li>Background Check</li>
                  </ul>
                  <p>After passing these 5 steps, Specialists are welcome to make a profile and apply for Support roles. They will be unable to design their own playdates or apply for Lead Specialist positions.</p>
                  <p>In addition to this vetting process, all PAL Leads must:</p>
                  <ul>
                    <li>Complete at least 10 hours in a Support Specialist role </li>
                    <li>Receive First Aid and CPR Certifications </li>
                    <li>Show proof of continued learning in education or child care</li>
                    <li>Receive consistent positive feedback from parents</li>
                    <li>Receive approval from another pre-approved Lead Specialist</li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading8">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                    I have a business that provides kids enrichment or hosts playdates, how do I become a partner?
                  </button>
                </h5>
              </div>
              <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                <div class="card-body">
                  <p>Contact us directly at <a href="mailto:partner@playdatepal.com">partner@playdatepal.com</a>. You can also visit our <a href="/contact-us.php">Get In Touch</a> page for more information.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading11">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                    How do you address no-shows or reliability concerns?
                  </button>
                </h5>
              </div>
              <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                <div class="card-body">
                  <p>Not showing up to an accepted booking, especially without communication, is not acceptable conduct on PAL. If a Parent or Specialist is a no-show for a booking, it should be reported as soon as possible by contacting PAL Support at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>. Our team takes action in regards to every reported no-show.</p>
                  <p>We review all cancellations and no-shows in detail and use all the information available to us to determine if a cancellation or no-show will warrant action taken on the members’ account. Each situation merits its own review, but we abide by the following guidelines in our review process to determine which reliability concerns are problematic.</p>
                  <ul>
                    <li>When did the member cancel? The closer to the date and time of the job, the more concerning it is, with no-shows being the most serious.</li>
                    <li>Is this a first-time booking? The fewer times the Parent and Specialist have worked together, the more concerning the cancellation is — cancelling on your first booking with a Parent or Specialist is the most serious.</li>
                    <li>What was the reason given? We evaluate cancellations and no-shows based on the reason provided by the member.</li>
                  </ul>
                  <p>To protect the member experience on PAL, Playdate Labs Inc. reserves the right, in its full discretion, to take corrective action on members' accounts. This Reliability Policy is in place in order to maintain the quality of the PAL platform and to ensure that members can trust the reliability of others.</p>
                  <p>While we recognize that life does happen, members deemed to be unreliable may have action taken on their account, including permanent account closure. These members would be removed from PAL for being in violation for our <a href="/terms-of-service.php">Terms of Service and Community Guidelines</a></p>.
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading12">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                    What if the hours worked by a Specialist do not line up with the hours booked online?
                  </button>
                </h5>
              </div>
              <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                <div class="card-body">
                  <p>A Specialist and/or Parent should contact PAL Support directly at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>. We will facilitate a refund if overpaid or an invoice if underpaid.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading23">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapse23">
                    What is your cancellation policy? 
                  </button>
                </h5>
              </div>
              <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#accordion">
                <div class="card-body">
                  <p>Parents - There is a cancellation fee of $10 within 48 hours of the playdate. There is a cancellation fee of $5 within the week.</p>
                  <p>Specialists - If you have to cancel a job you committed to, we strongly recommend finding a replacement before cancelling on the families. We understand that things happen, but if we see consistent cancellations, your profile and participation will be under review and potentially suspended.</p>
                  <p>Both Parents and Specialists are expected to uphold their booking commitments. However, we do understand that life can lead to unexpected circumstances which may cause a member to cancel a booking. If either member needs to cancel in case of an emergency, they should immediately notify our Concierge Service. If you are a Parent, email <a href="mailto:parentconcierge@playdatepal.com">parentconcierge@playdatepal.com</a>. If you are a Specialist, email <a href="mailto:talentconcierge@playdatepal.com">talentconcierge@playdatepal.com</a>.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading26">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse26" aria-expanded="false" aria-controls="collapse26">
                    How do I invite my friends to join PAL?
                  </button>
                </h5>
              </div>
              <div id="collapse26" class="collapse" aria-labelledby="heading26" data-parent="#accordion">
                <div class="card-body">
                  <p>Parents - Once logged in, go to “My Dashboard” and click the “Invite Friends” tab. Input the emails of friends you’d like to invite. They will be contacted and interviewed by our Parent Concierge service in order to receive an exclusive access code to the site. You can also invite your friends to specific playdates, if they are already PAL members, by inputting their emails directly on a playdate page.</p>
                  <p>Specialists - You can <a href="/specialist-dashboard.php">nominate a Specialist</a> by clicking “My Dashboard”, choosing the “Specialists” tab and scrolling to the bottom of the page. Input an email address to invite your friends to start the application process! You can also invite your favorite families by clicking “Get In Touch” at the bottom of your home screen. Fill out a contact form letting our PAL Support Team know that you would like to refer a family to the PAL platform. They will be contacted and interviewed by our Parent Concierge service in order to receive an exclusive access code.</p>
                </div>
              </div>
            </div>

            <div class="card general">
              <div class="card-header" id="heading26b">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse26b" aria-expanded="false" aria-controls="collapse26b">
                    Expectations of the PAL Community
                  </button>
                </h5>
              </div>
              <div id="collapse26b" class="collapse" aria-labelledby="heading26b" data-parent="#accordion">
                <div class="card-body">
                  <p>Here at PAL, we rely on our community of parents and playdate specialists to abide by these shared expectations:
                    <ul>
                      <li>Be respectful of everyone in our community</li>
                      <li>Be prompt for bookings - start and end times</li>
                      <li>Adhere to booking and payment agreements upon arranging</li>
                      <li>Provide safe, clean and supportive environments for children, parents, and playdate specialists</li>
                      <li>Follow local, state, and federal laws</li>
                      <li>Special booking circumstances should be previously discussed, arranged, and approved by both parties. Examples of such circumstances include:
                        <ul>
                          <li>Additional attendants</li>
                          <li>Additional compensation</li>
                          <li>Special care requirements</li>
                          <li>Travel</li>
                        </ul>
                      </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>


            <!-- FIN GENERAL -->

            <!-- ACCOUNT -->

            <div class="card account">
              <div class="card-header" id="heading14">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                    What information do I need to Create an Account?                  </button>
                </h5>
              </div>
              <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
                <div class="card-body">
                  <p>Account information required includes name, photo, neighborhood, and basic information around your child care needs. Your credit card will not be filed.</p>
                  <p>We respect your privacy and do not post any information publicly without your permission.</p>
                  <p>Ready to get started on PAL? </p>
                  <p>If you are a Parent, you can <a href="/parent-sign-up.php">request an invitation</a>.</p>
                  <p>If you are a potential Specialist, you can <a href="/apply-specialist.php">apply to become a Specialist</a> by submitting your information.</p>
                </div>
              </div>
            </div>


            <div class="card account">
              <div class="card-header" id="heading32">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse32" aria-expanded="false" aria-controls="collapse32">
                    How do I delete my account?
                  </button>
                </h5>
              </div>
              <div id="collapse32" class="collapse" aria-labelledby="heading32" data-parent="#accordion">
                <div class="card-body">
                  <p>Parents - Contact Parent Concierge at  <a href="mailto:parentconcierge@playdatepal.com">parentconcierge@playdatepal.com</a> and our team will assist you.</p>
                  <p>Specialists - Contact Talent Concierge at <a href="mailto:talentconcierge@playdatepal.com">talentconcierge@playdatepal.com</a> and our team will assist you. </p>
                </div>
              </div>
            </div>

            <div class="card account">
              <div class="card-header" id="heading9">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                    How do I report someone’s account?
                  </button>
                </h5>
              </div>
              <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
                <div class="card-body">
                  <p>Contact PAL Support directly at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a> with any suspicious activity that may violate our <a href="/terms-of-service.php">Terms of Service</a>. Once PAL Support is notified, we will take any action that may be necessary and follow up with you as soon as possible. Reports are completely confidential. The member you report will not be notified.</p>
                </div>
              </div>
            </div>

            <div class="card account">
              <div class="card-header" id="heading10">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                    Why was my account suspended?
                  </button>
                </h5>
              </div>
              <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
                <div class="card-body">
                  <p>To protect the member experience on PAL, Playdate Labs, Inc. reserves the right, in its full discretion, to take corrective action on members' accounts. Our Reliability Policy is in place in order to maintain the quality of the PAL platform and to ensure that members can trust the reliability of others. While we recognize that life does happen, members deemed to be unreliable may have action taken on their account, including permanent account closure. These members would be removed from PAL for being in violation for our <a href="/terms-of-service.php">Terms of Service and Community Guidelines</a>.</p>
                </div>
              </div>
            </div>

            <!-- FIN ACCOUNT -->

            <!-- PARENTS -->

            <div class="card parents">
              <div class="card-header" id="heading33">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse33" aria-expanded="false" aria-controls="collapse33">
                    How much does PAL cost?
                  </button>
                </h5>
              </div>
              <div id="collapse33" class="collapse" aria-labelledby="heading33" data-parent="#accordion">
                <div class="card-body">
                  <ul>
                    <li>2 children: $18/hour/child</li>
                    <li>3 children: $16/hour/child</li>
                    <li>4-5 children: $12/hour/child</li>
                    <li>6 children: $11/hour/child</li>
                    <li>7+ children: $10/hour/child</li>
                  </ul>
                  <p>PAL costs are dependent on the number of kids, activity budget, age group, and hours of care. The price drops for each Parent as more children join the playdate. Playdates with 3rd party hosts will require activity budgets that can influence the final price listed for your playdate. Other add-ons may include supervised travel, additional supplies, or meals.</p> 
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading16">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                    How do I join a Private PAL Group?
                  </button>
                </h5>
              </div>
              <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordion">
                <div class="card-body">
                  <p>To gain access to a private PAL Group, you must be accepted by the Group administrator. Once logged in, you can request an invitation.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading15">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                    How do I request a Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
                <div class="card-body">
                  <p>Once logged in, provide your basic child care needs by <a href="/parent-request-playdate.php">requesting a playdate</a>. This will post to our private job board and notify our Lead PAL Specialists available for care in your area. Check your dashboard to see if a Specialist has applied for your job opening. Once you approve an itinerary and hire a Specialist, the playdate will open up to other families within your private Group(s).</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading17">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                    What if I cannot find a Specialist to work my Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordion">
                <div class="card-body">
                  <p>Keep your playdate request open and email PAL Support at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>. We will do our best to assist you. We recommend a lead time of at least one week to find a Specialist available for your job. </p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading18">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
                    What if I cannot find another family to match my Playdate by the time it expires?
                  </button>
                </h5>
              </div>
              <div id="collapse18" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                <div class="card-body">
                  <p>Your request expires 48 hours before the playdate. You have the option of using that 48 hours to find a second child to join your playdate. Your Specialist will reserve their time for you. If you are unable to find a match, you agree to ‘Pay Social, Go Solo’. You can pay the rate of two kids for one by editing the playdate and reserving spots for 2 children. </p>
                  <p>You also have the option of cancelling your request. You can cancel an open request without having to pay a fee by emailing our team at <a href="mailto:support@playdatepal.com">support@playdatepal.com</a>. </p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading20">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse20" aria-expanded="false" aria-controls="collapse20">
                    Is there a discount for siblings?
                  </button>
                </h5>
              </div>
              <div id="collapse20" class="collapse" aria-labelledby="heading20" data-parent="#accordion">
                <div class="card-body">
                  <p>The second child in the family will receive a 25% discount. </p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading22">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapse22">
                    How do I pay for a Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#accordion">
                <div class="card-body">
                  <p>Once logged in, you can <a href="/get-playpacks.php">purchase a playpack</a>. Playpack credits will be added to your queue and can then be used towards any playdate reservation (excluding Signature Events). Credits will be used from your account only after the RSVP date has passed at the correct rate per hour. If you cancel before the RSVP date has passed, you will be charged a $5 fee. If you cancel after the RSVP date has passed, you will be charged a $10 fee.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading24">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse24" aria-expanded="false" aria-controls="collapse24">
                    Does my Play Pack expire?
                  </button>
                </h5>
              </div>
              <div id="collapse24" class="collapse" aria-labelledby="heading24" data-parent="#accordion">
                <div class="card-body">
                  <p>Your Playpack will expire in a year past the purchase date.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading25">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse25" aria-expanded="false" aria-controls="collapse25">
                    How do I book a Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse25" class="collapse" aria-labelledby="heading25" data-parent="#accordion">
                <div class="card-body">
                  <p>You can book a playdate in three different ways:</p>
                  <ul>
                    <li>By logging in and reserving a spot in any open playdate in the PAL marketplace</li>
                    <li>By checking your private Group Dashboards to see if you’ve been invited to any private playdates</li>
                    <li>Finally, you can book a playdate by logging in and <a href="/parent-request-playdate.php">requesting a playdate</a> that is custom to your schedule and child care needs. This will post on our private job board and then you will receive detailed itineraries from our Specialists available for hire.</li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading27">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse27" aria-expanded="false" aria-controls="collapse27">
                    How do I invite my favorite child care provider to join Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse27" class="collapse" aria-labelledby="heading27" data-parent="#accordion">
                <div class="card-body">
                  <p>Go to My Dashboard and click the “Specialists” tab. You can nominate your favorite child care professional by inputting their email here.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading29">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse29" aria-expanded="false" aria-controls="collapse29">
                    Can I book travel with Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse29" class="collapse" aria-labelledby="heading29" data-parent="#accordion">
                <div class="card-body">
                  <p>Supervised travel is not available for all open playdates, and is offered on a first come first serve basis when it is available. For open playdates, Specialists or Supports will receive an additional $15 each way for their time. Parents must also book and pay for the cost of travel. If you are designing a private playdate, supervised travel can be included in the playdate itinerary at no additional cost for time. Parents are still required to book and pay for travel accommodations. We also recommend chatting with other parents in your group or playdate to facilitate a carpool or "walkpool" with others attending your playdate.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading30">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse30" aria-expanded="false" aria-controls="collapse30">
                   Is my credit card information secure?
                  </button>
                </h5>
              </div>
              <div id="collapse30" class="collapse" aria-labelledby="heading30" data-parent="#accordion">
                <div class="card-body">
                  <p>We take security very seriously and have put measures in place to prevent the loss, misuse and alteration of any information under our control. All purchases are transmitted over secure internet connections using SSL (Secure Sockets Layer) encryption technology.</p>
                </div>
              </div>
            </div>

            <div class="card parents">
              <div class="card-header" id="heading31">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse31" aria-expanded="false" aria-controls="collapse31">
                    How do I post a review about a Specialist or PAL experience?
                  </button>
                </h5>
              </div>
              <div id="collapse31" class="collapse" aria-labelledby="heading31" data-parent="#accordion">
                <div class="card-body">
                  <p>After you have completed a playdate, you will be asked for feedback when you log back in. If you have completed at least one booking with a Specialist on PAL, you have the ability to post a review to their profile. A public review will be visible to all users on PAL, including other Parents and the Specialist themselves. You also have the option to submit a private review, which will only be visible to the PAL Support Team.</p>
                  <p>To post a review, simply navigate to the Specialists' profile and click on the ‘Review’ button available there. This will take you to a form where you can fill out the star count, a description and indicate the visibility setting.</p>
                </div>
              </div>
            </div>

            <!-- FIN PARENTS -->

            <!-- SPECIALISTS -->

            <div class="card providers">
              <div class="card-header" id="heading43">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse43" aria-expanded="false" aria-controls="collapse43">
                    How do I find PAL jobs?
                  </button>
                </h5>
              </div>
              <div id="collapse43" class="collapse" aria-labelledby="heading43" data-parent="#accordion">
                <div class="card-body">
                  <p>On PAL, there are three ways to book a job:</p>
                  <ul>
                    <li>By continuously checking the Job Board for open playdate requests, private playdate requests (if you are a Lead) and direct requests from Parents and Lead Specialists. Then, apply to the jobs you'd like to be considered for. Apply to all of the jobs that work for you to increase your chances of being booked! Please read job posts thoroughly before applying, as details cannot be negotiated. </li>
                    <li>Apply to private booking requests - view Parent requests searching for a Lead Specialist in their area. Submit an itinerary providing details on drop off and pick up, activities, meal or snack time and fun! Refer to the <a href="https://bamboo-magnolia-2mxk.squarespace.com" target="_blank">Knowledge Center</a> on how to build the best playdate itineraries.</li>
                    <li>Create a playdate - As a Lead, you can post your own playdates to the marketplace and build a following of families. You can also post your playdate to the job board if you need to find a Support Specialist to help you run it.</li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading41">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse41" aria-expanded="false" aria-controls="collapse41">
                    Can I Negotiate a Booking?
                  </button>
                </h5>
              </div>
              <div id="collapse41" class="collapse" aria-labelledby="heading41" data-parent="#accordion">
                <div class="card-body">
                  <p>After accepting a job, PAL Specialists are expected to adhere to all booking details and terms. The terms of a booking include the payment method, hourly rate, number of children, time, and location of the booking. Specialists have the option to decline booking requests when they are not the right fit. If a Specialist finds they cannot agree to all terms of the booking, we ask that they politely decline the request.</p>
                  <p>We routinely advise Specialists not to negotiate booking terms after accepting a job. Repeated reports of these negotiations may result in a profile suspension or permanent termination of the PAL Specialists’ account.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading44">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse44" aria-expanded="false" aria-controls="collapse44">
                    Why am I unable to apply for certain jobs?
                  </button>
                </h5>
              </div>
              <div id="collapse44" class="collapse" aria-labelledby="heading44" data-parent="#accordion">
                <div class="card-body">
                  <p>Certain jobs are only available to PAL Specialists in Lead roles, not Support roles. You can refer to the <a href="https://bamboo-magnolia-2mxk.squarespace.com" target="_blank">Knowledge Center</a> to learn more about how to become a PAL Lead Specialist.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading46">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse46" aria-expanded="false" aria-controls="collapse46">
                    How do I hire a Support Specialist to help me run a playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse46" class="collapse" aria-labelledby="heading46" data-parent="#accordion">
                <div class="card-body">
                  <p>Only Lead Specialists can hire Support. You can do so by creating a playdate and publishing it to the Job Board.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading34">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse34" aria-expanded="false" aria-controls="collapse34">
                    How do I add a certification to my profile?
                  </button>
                </h5>
              </div>
              <div id="collapse34" class="collapse" aria-labelledby="heading34" data-parent="#accordion">
                <div class="card-body">
                  <p>To add a certification to your profile, please upload evidence of this certification to your dashboard. Our team will do diligence to approve this certification before it becomes visible to parents.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading45">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse45" aria-expanded="false" aria-controls="collapse45">
                    How do I follow up with parents after a Playdate?
                  </button>
                </h5>
              </div>
              <div id="collapse45" class="collapse" aria-labelledby="heading45" data-parent="#accordion">
                <div class="card-body">
                  <p>You can follow up with Parents after your playdate by sending them a direct thank you email. Contact <a href="mailto:talentconcierge@playdatepal.com">talentconcierge@playdatepal.com</a> for assistance.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading37">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse37" aria-expanded="false" aria-controls="collapse37">
                    When do I get paid?
                  </button>
                </h5>
              </div>
              <div id="collapse37" class="collapse" aria-labelledby="heading37" data-parent="#accordion">
                <div class="card-body">
                  <p>Payment will be processed directly into your bank account typically within 2-3 days after the playdate has passed. If a Parent indicates that the hours booked do not match your time worked, you may experience a delay in the transaction.</p>
                </div>
              </div>
            </div>

            <div class="card providers">
              <div class="card-header" id="heading38">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapse38" aria-expanded="false" aria-controls="collapse38">
                    What pay can I expect?
                  </button>
                </h5>
              </div>
              <div id="collapse38" class="collapse" aria-labelledby="heading38" data-parent="#accordion">
                <div class="card-body">
                  <p>As a Lead Specialist, you will receive anywhere between $25-$50 per hour, depending on the number and age of the children attending. You will receive more per hour every time another child signs up. As a Support Specialist, you can make anywhere between $23-$25 per hour. This also depends on the number and age of children attending.</p>
                </div>
              </div>
            </div>

            

            

             

            
            
          </div>
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>
<script type="text/javascript">
 var url = document.location.toString();
  if ( url.match('#') ) {
      $(".collapse").removeClass("show")
      $('#'+url.split('#')[1]).addClass("show")
      $(".collapse").parent().find(".card-header button").attr("aria-expanded", "false");
      $('#'+url.split('#')[1]).parent().find(".card-header button").attr("aria-expanded", "true");

      $("html, body").delay(500).animate({scrollTop: $('#'+url.split('#')[1]).offset().top -100 }, 2000);
  }


</script>
</html>

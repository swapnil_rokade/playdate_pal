<?php
include_once("BaseEntity.class.php");

class PlaydateRequest extends BaseEntity {
    
    public static $STATUS_REQUEST_OPEN=0;
    public static $STATUS_REQUEST_CLOSED=1;
    
    protected $id_request, $id_parent, $id_group, $type, $location, $travel_pickup, $travel_dropoff, $date, $time_init, $time_end, $budget_init, $budget_end, $extra_kids, $info, $num_supporters, $public_job_board, $status,
    $playdate_type,$playdate_date,$playdate_time,$group_size, $age_range, $no_of_playdate_per_week, $which_day_of_week, $location_in_mind, $preferred_location, $preferred_location_type, $theme_of_playdate, $pay_extra_bucks_for, $notes, $like_to_invite_guest,$information_of_guests, $playdate_lead_by_specialist,
    $specialist_first_name,$specialist_last_name,$other_details,$how_did_you_hear_about_us,$first_name,$last_name,$email_address,
    $private_friend_name,$private_friend_email;
    

    /**
    * Constructor
    */ 
    public function __construct( $id_request=-1) {
        $this->id_request = $id_request;
    }
   
	public function readFromRow($row){
	    if(isset($row['id_request'])) { $this->setId_request($row['id_request']); }
	    if(isset($row['id_parent'])) { $this->setId_parent($row['id_parent']); }
	    if(isset($row['id_group'])) { $this->setId_group($row['id_group']); }
	    if(isset($row['type'])) { $this->setType($row['type']);}
	    if(isset($row['location'])) { $this->setLocation($row['location']);}
	    if(isset($row['travel_pickup'])) { $this->setTravel_pickup($row['travel_pickup']);}
	    if(isset($row['travel_dropoff'])) { $this->setTravel_dropoff($row['travel_dropoff']);}
	    if(isset($row['date']) && ($row['date']!="0000-00-00 00:00:00")) $this->date=($this->toStringDateFormat($row['date']));
	    if(isset($row['time_init'])) { $this->setTime_init($row['time_init']);}
	    if(isset($row['time_end'])) { $this->setTime_end($row['time_end']);}
	    if(isset($row['budget_init'])) { $this->setBudget_init($row['budget_init']);}
	    if(isset($row['budget_end'])) { $this->setBudget_end($row['budget_end']);}
	    if(isset($row['extra_kids'])) { $this->setExtra_kids($row['extra_kids']);}
	    if(isset($row['info'])) { $this->setInfo($row['info']);}
	    if(isset($row['num_supporters'])) { $this->setNum_supporters($row['num_supporters']);}
	    if(isset($row['public_job_board'])) { $this->setPublic_job_board($row['public_job_board']);}
	    if(isset($row['status'])) { $this->setStatus($row['status']);}
        
        if(isset($row['playdate_type'])) { $this->setPlaydate_type($row['playdate_type']); }
	    if(isset($row['playdate_date'])) { $this->setPlaydateDate($row['playdate_date']);}
	    if(isset($row['playdate_time'])) { $this->setPlaydateTime($row['playdate_time']);}
	    if(isset($row['group_size'])) { $this->setGroup_size($row['group_size']);}
	    if(isset($row['age_range'])) { $this->setAge_range($row['age_range']);}
	    if(isset($row['no_of_playdate_per_week'])) { $this->setNo_of_playdate($row['no_of_playdate_per_week']);}
	    if(isset($row['which_day_of_week'])) { $this->setWhich_day_week($row['which_day_of_week']);}
	    if(isset($row['location_in_mind'])) { $this->setLocation_in_mind($row['location_in_mind']);}
	    if(isset($row['preferred_location'])) { $this->setPreferred_location($row['preferred_location']);}
	    if(isset($row['preferred_location_type'])) { $this->setPreferred_location_type($row['preferred_location_type']);}
	    if(isset($row['theme_of_playdate'])) { $this->setTheme_of_playdate($row['theme_of_playdate']);}
	    if(isset($row['pay_extra_bucks_for'])) { $this->setPay_extra($row['pay_extra_bucks_for']);}
	    if(isset($row['notes'])) { $this->setNotes($row['notes']);}
        if(isset($row['like_to_invite_guest'])) { $this->setLike_to_invite($row['like_to_invite_guest']);}
        if(isset($row['information_of_guests'])) { $this->setInformation_of_guest($row['information_of_guests']);}
        if(isset($row['playdate_lead_by_specialist'])) { $this->setPlaydate_lead_by_specialist($row['playdate_lead_by_specialist']);}
        if(isset($row['specialist_first_name'])) { $this->setSpecialist_first_name($row['specialist_first_name']);}
        if(isset($row['specialist_last_name'])) { $this->setSpecialist_last_name($row['specialist_last_name']);}
        if(isset($row['other_details'])) { $this->setOther_details($row['other_details']);}
        if(isset($row['private_friend_name'])) { $this->setPrivate_friend_name($row['private_friend_name']);}
        if(isset($row['private_friend_email'])) { $this->setPrivate_friend_email($row['private_friend_email']);}

		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
    }
    
    /**
     * @return mixed
     */
    public function getId_request()
    {
        return $this->id_request;
    }

    /**
     * @param mixed $id_request
     */
    public function setId_request($id_request)
    {
        $this->id_request = $id_request;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getTravel_pickup()
    {
        return $this->travel_pickup;
    }

    /**
     * @param mixed $travel_pickup
     */
    public function setTravel_pickup($travel_pickup)
    {
        $this->travel_pickup = $travel_pickup;
    }

    /**
     * @return mixed
     */
    public function getTravel_dropoff()
    {
        return $this->travel_dropoff;
    }

    /**
     * @param mixed $travel_dropoff
     */
    public function setTravel_dropoff($travel_dropoff)
    {
        $this->travel_dropoff = $travel_dropoff;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTime_init()
    {
        return $this->time_init;
    }

    /**
     * @param mixed $time_init
     */
    public function setTime_init($time_init)
    {
        $this->time_init = $time_init;
    }

    /**
     * @return mixed
     */
    public function getTime_end()
    {
        return $this->time_end;
    }

    /**
     * @param mixed $time_end
     */
    public function setTime_end($time_end)
    {
        $this->time_end = $time_end;
    }

    /**
     * @return mixed
     */
    public function getBudget_init()
    {
        return $this->budget_init;
    }

    /**
     * @param mixed $budget_init
     */
    public function setBudget_init($budget_init)
    {
        $this->budget_init = $budget_init;
    }

    /**
     * @return mixed
     */
    public function getBudget_end()
    {
        return $this->budget_end;
    }

    /**
     * @param mixed $budget_end
     */
    public function setBudget_end($budget_end)
    {
        $this->budget_end = $budget_end;
    }

    /**
     * @return mixed
     */
    public function getExtra_kids()
    {
        return $this->extra_kids;
    }

    /**
     * @param mixed $extra_kids
     */
    public function setExtra_kids($extra_kids)
    {
        $this->extra_kids = $extra_kids;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getNum_supporters()
    {
        return $this->num_supporters;
    }

    /**
     * @param mixed $num_supporters
     */
    public function setNum_supporters($num_supporters)
    {
        $this->num_supporters = $num_supporters;
    }

    /**
     * @return mixed
     */
    public function getPublic_job_board()
    {
        return $this->public_job_board;
    }

    /**
     * @param mixed $public_job_board
     */
    public function setPublic_job_board($public_job_board)
    {
        $this->public_job_board = $public_job_board;
    }
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function getId_group()
    {
        return $this->id_group;
    }

    /**
     * @param mixed $id_group
     */
    public function setId_group($id_group)
    {
        $this->id_group = $id_group;
    }

    /**
     * @return mixed
     */
    public function getPlaydateDate()
    {
        return $this->playdate_date;
    }

    /**
     * @param mixed $playdate_date
     */
    public function setPlaydateDate($playdate_date)
    {
        $this->playdate_date = $playdate_date;
    }

    /**
     * @return mixed
     */
    public function getPlaydateTime()
    {
        return $this->playdate_time;
    }

    /**
     * @param mixed $playdate_time
     */
    public function setPlaydateTime($playdate_time)
    {
        $this->playdate_time = $playdate_time;
    }

    /**
     * @return mixed
     */
    public function getGroup_size()
    {
        return $this->group_size;
    }

    /**
     * @param mixed $group_size
     */
    public function setGroup_size($group_size)
    {
        $this->group_size = $group_size;
    }

    /**
     * @return mixed
     */
    public function getAge_range()
    {
        return $this->age_range;
    }

    /**
     * @param mixed $age_range
     */
    public function setAge_range($age_range)
    {
        $this->age_range = $age_range;
    }

    /**
     * @return mixed
     */
    public function getNo_of_playdate()
    {
        return $this->no_of_playdate_per_week;
    }

    /**
     * @param mixed $no_of_playdate_per_week
     */
    public function setNo_of_playdate($no_of_playdate_per_week)
    {
        $this->no_of_playdate_per_week = $no_of_playdate_per_week;
    }

    /**
     * @return mixed
     */
    public function getWhich_day_week()
    {
        return $this->which_day_of_week;
    }

    /**
     * @param mixed $which_day_of_week
     */
    public function setWhich_day_week($which_day_of_week)
    {
        $this->which_day_of_week = $which_day_of_week;
    }

    /**
     * @return mixed
     */
    public function getLocation_in_mind()
    {
        return $this->location_in_mind;
    }

    /**
     * @param mixed $location_in_mind
     */
    public function setLocation_in_mind($location_in_mind)
    {
        $this->location_in_mind = $location_in_mind;
    }

    /**
     * @return mixed
     */
    public function getPreferred_location()
    {
        return $this->preferred_location;
    }

    /**
     * @param mixed $preferred_location
     */
    public function setPreferred_location($preferred_location)
    {
        $this->preferred_location = $preferred_location;
    }

    /**
     * @return mixed
     */
    public function getPreferred_location_type()
    {
        return $this->preferred_location_type;
    }

    /**
     * @param mixed $preferred_location_type
     */
    public function setPreferred_location_type($preferred_location_type)
    {
        $this->preferred_location_type = $preferred_location_type;
    }

    /**
     * @return mixed
     */
    public function getTheme_of_playdate()
    {
        return $this->theme_of_playdate;
    }

    /**
     * @param mixed $theme_of_playdate
     */
    public function setTheme_of_playdate($theme_of_playdate)
    {
        $this->theme_of_playdate = $theme_of_playdate;
    }

    /**
     * @return mixed
     */
    public function getPay_extra()
    {
        return $this->pay_extra_bucks_for;
    }

    /**
     * @param mixed $pay_extra_bucks_for
     */
    public function setPay_extra($pay_extra_bucks_for)
    {
        $this->pay_extra_bucks_for = $pay_extra_bucks_for;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function getLike_to_invite()
    {
        return $this->like_to_invite_guest;
    }

    /**
     * @param mixed $like_to_invite_guest
     */
    public function setLike_to_invite($like_to_invite_guest)
    {
        $this->like_to_invite_guest = $like_to_invite_guest;
    }

    /**
     * @return mixed
     */
    public function getInformation_of_guest()
    {
        return $this->information_of_guests;
    }

    /**
     * @param mixed $information_of_guests
     */
    public function setInformation_of_guest($information_of_guests)
    {
        $this->information_of_guests = $information_of_guests;
    }

    /**
     * @return mixed
     */
    public function getPlaydate_lead_by_specialist()
    {
        return $this->playdate_lead_by_specialist;
    }

    /**
     * @param mixed $playdate_lead_by_specialist
     */
    public function setPlaydate_lead_by_specialist($playdate_lead_by_specialist)
    {
        $this->playdate_lead_by_specialist = $playdate_lead_by_specialist;
    }
    
    /**
     * @return mixed
     */
    public function getOther_details()
    {
        return $this->other_details;
    }

    /**
     * @param mixed $other_details
     */
    public function setOther_details($other_details)
    {
        $this->other_details = $other_details;
    }
    
    /**
     * @return mixed
     */
    public function getPlaydate_type()
    {
        return $this->playdate_type;
    }

    /**
     * @param mixed $playdate_type
     */
    public function setPlaydate_type($playdate_type)
    {
        $this->playdate_type = $playdate_type;
    }

    /**
     * @return mixed
     */
    public function getPrivate_friend_name()
    {
        return $this->private_friend_name;
    }

    /**
     * @param mixed $private_friend_name
     */
    public function setPrivate_friend_name($private_friend_name)
    {
        $this->private_friend_name = $private_friend_name;
    }
    /**
     * @return mixed
     */
    public function getPrivate_friend_email()
    {
        return $this->private_friend_email;
    }

    /**
     * @param mixed $private_friend_email
     */
    public function setPrivate_friend_email($private_friend_email)
    {
        $this->private_friend_email = $private_friend_email;
    }

	
	
} 
?>
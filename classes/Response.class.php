<?php
include_once("BaseEntity.class.php");

class Response extends BaseEntity {
    protected $error_code, $description, $data;
   
   /**
    * Constructor
    */ 
    public function __construct( $error_code=0, $description="OK") {
    	$this->error_code = $error_code;
    	$this->description = $description;
    }
   
	public function getError_code() {
		return $this->error_code;
	}
	public function setError_code($error_code) {
		$this->error_code = $error_code;
	}
	
	public function getDescription() {
		return $this->description;
	}
	public function setDescription($description) {
		$this->description = $description;
	}

	public function getData() {
		return $this->data;
	}
	public function setData($newData) {
		$this->data = $newData;
	}
}
?>
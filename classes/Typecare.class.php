<?php
include_once("BaseEntity.class.php");

class Typecare extends BaseEntity {
	
    protected $id_typecare, $name, $description, $order, $idate, $udate;
    
    
    /**
    * Constructor
    */ 
    public function __construct( $id_typecare=-1) {
        $this->id_typecare = $id_typecare;
    }
   
	public function readFromRow($row){
		if(isset($row['id_typecare'])) $this->setId_typecare($row['id_typecare']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['description'])) $this->setDescription($row['description']);
		if(isset($row['order'])) $this->setOrder($row['order']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
    /**
     * @return mixed
     */
    public function getId_typecare()
    {
        return $this->id_typecare;
    }

    /**
     * @param mixed $id_typecare
     */
    public function setId_typecare($id_typecare)
    {
        $this->id_typecare = $id_typecare;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
	
}
?>
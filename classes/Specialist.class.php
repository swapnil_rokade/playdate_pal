<?php
include_once("BaseEntity.class.php");

class Specialist extends BaseEntity {
    
    public static $STATUS_DRAFT=0;
    public static $STATUS_VISIBLE=1;
    
    public static $PROFILE_SUPPORTER=0;
    public static $PROFILE_LEAD=1;
    public static $PROFILE_NURSE=2;
    
    public static $SUPPORTER_REQUESTED=0;
    public static $SUPPORTER_ACCEPTED=1;
    public static $SUPPORTER_DECLINED=2;
    
    protected $id_specialist, $name, $lastname, $username, $email, $password, $cellphone, $phone, $exp_range1, $exp_range2, $exp_range3, $about_me, $additional_1, $additional_2, $additional_3, $additional_4, $language_1, $language_2, $language_3, $picture, $idate, $udate, $slug, $status, $profile, $us_citizen;
    
    protected $cert_nursing, $cert_redcross, $cert_language;
    
    protected $cert_teaching, $cert_cpcr, $cert_firstaid, $cert_child_dev, $cert_partner;
    
    protected $pay_bankname, $pay_routing, $pay_account, $pay_stripeid;
    
    
    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getExp_range1()
    {
        return $this->exp_range1;
    }

    /**
     * @return mixed
     */
    public function getExp_range2()
    {
        return $this->exp_range2;
    }

    /**
     * @return mixed
     */
    public function getExp_range3()
    {
        return $this->exp_range3;
    }

    /**
     * @return mixed
     */
    public function getAbout_me($charlimit=10000)
    {   
        $retorno = $this->about_me;
        
        if ($charlimit && (strlen($retorno) > $charlimit)) {
            $retorno = $this->truncateString($retorno,$charlimit);
        }
        return $retorno;

    }

    /**
     * @return mixed
     */
    public function getAdditional_1()
    {
        return $this->additional_1;
    }

    /**
     * @return mixed
     */
    public function getAdditional_2()
    {
        return $this->additional_2;
    }

    /**
     * @return mixed
     */
    public function getAdditional_3()
    {
        return $this->additional_3;
    }

    /**
     * @return mixed
     */
    public function getAdditional_4()
    {
        return $this->additional_4;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $cellphone
     */
    public function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $exp_range1
     */
    public function setExp_range1($exp_range1)
    {
        $this->exp_range1 = $exp_range1;
    }

    /**
     * @param mixed $exp_range2
     */
    public function setExp_range2($exp_range2)
    {
        $this->exp_range2 = $exp_range2;
    }

    /**
     * @param mixed $exp_range3
     */
    public function setExp_range3($exp_range3)
    {
        $this->exp_range3 = $exp_range3;
    }

    /**
     * @param mixed $aboutme
     */
    public function setAbout_me($aboutme)
    {
        $this->about_me = $aboutme;
    }

    /**
     * @param mixed $additional_1
     */
    public function setAdditional_1($additional_1)
    {
        $this->additional_1 = $additional_1;
    }

    /**
     * @param mixed $additional_2
     */
    public function setAdditional_2($additional_2)
    {
        $this->additional_2 = $additional_2;
    }

    /**
     * @param mixed $additional_3
     */
    public function setAdditional_3($additional_3)
    {
        $this->additional_3 = $additional_3;
    }

    /**
     * @param mixed $additional_4
     */
    public function setAdditional_4($additional_4)
    {
        $this->additional_4 = $additional_4;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
    * Constructor
    */ 
    public function __construct( $id_specialist=-1) {
        $this->id_specialist = $id_specialist;
    }
   
	public function getId_specialist() {
		return $this->id_specialist;
	}
	public function setId_specialist($newId_spec) {
		$this->id_specialist = $newId_spec;
	}
	
   
	
	/**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getMD5() {
		return md5($this->getId_specialist().$this->getEmail().$this->getName().$this->getLastname());
	}

	public function readFromRow($row){
	    if(isset($row['id_specialist'])) { $this->setId_specialist($row['id_specialist']);}
	    if(isset($row['name'])) { $this->setName($row['name']);}
	    if(isset($row['lastname'])) { $this->setLastname($row['lastname']);}
	    if(isset($row['username'])) { $this->setUsername($row['username']);}
	    if(isset($row['email'])) { $this->setEmail($row['email']);}
	    if(isset($row['password'])) { $this->setPassword($row['password']);}
	    if(isset($row['cellphone'])) { $this->setCellphone($row['cellphone']);}
	    if(isset($row['phone'])) { $this->setPhone($row['phone']);}
	    if(isset($row['exp_range1'])) { $this->setExp_range1($row['exp_range1']);}
	    if(isset($row['exp_range2'])) { $this->setExp_range2($row['exp_range2']);}
	    if(isset($row['exp_range3'])) { $this->setExp_range3($row['exp_range3']);}
	    if(isset($row['about_me'])) { $this->setAbout_me($row['about_me']);}
	    if(isset($row['additional_1'])) { $this->setAdditional_1($row['additional_1']);}
	    if(isset($row['additional_2'])) { $this->setAdditional_2($row['additional_2']);}
	    if(isset($row['additional_3'])) { $this->setAdditional_3($row['additional_3']);}
	    if(isset($row['additional_4'])) { $this->setAdditional_4($row['additional_4']);}
	    if(isset($row['language_1'])) { $this->setLanguage_1($row['language_1']);}
	    if(isset($row['language_2'])) { $this->setLanguage_2($row['language_2']);}
	    if(isset($row['language_3'])) { $this->setLanguage_3($row['language_3']);}
	    
	    if(isset($row['picture'])) { $this->setPicture($row['picture']);}
	    if(isset($row['slug'])) { $this->setSlug($row['slug']);}
	    
	    if(isset($row['profile'])) { $this->setProfile($row['profile']);}
	    
	    if(isset($row['us_citizen'])) { $this->setUs_citizen($row['us_citizen']);}
	    
	    if(isset($row['cert_nursing'])) { $this->setCert_nursing($row['cert_nursing']);}
	    if(isset($row['cert_redcross'])) { $this->setCert_redcross($row['cert_redcross']);}
	    if(isset($row['cert_language'])) { $this->setCert_language($row['cert_language']);}
	    
	    if(isset($row['cert_teaching'])) { $this->setCert_teaching($row['cert_teaching']);}
	    if(isset($row['cert_cpcr'])) { $this->setCert_cpcr($row['cert_cpcr']);}
	    if(isset($row['cert_firstaid'])) { $this->setCert_firstaid($row['cert_firstaid']);}
	    if(isset($row['cert_child_dev'])) { $this->setCert_child_dev($row['cert_child_dev']);}
	    if(isset($row['cert_partner'])) { $this->setCert_partner($row['cert_partner']);}
	    
	    if(isset($row['pay_bankname'])) { $this->setPay_bankname($row['pay_bankname']);}
	    if(isset($row['pay_routing'])) { $this->setPay_routing($row['pay_routing']);}
	    if(isset($row['pay_account'])) { $this->setPay_account($row['pay_account']);}
	    if(isset($row['pay_stripeid'])) { $this->setPay_stripeid($row['pay_stripeid']);}
	    
	    if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
	/**
	 * Read full specialist name
	 */
	public function getFullName() {
	    $fullname = $this->getName();
	    
	    if($this->getLastname()!="") {
	        $fullname.= " ".$this->getLastname();
	    }
	    
	    return $fullname;
	    
	}
    /**
     * @return mixed
     */
    public function getUs_citizen()
    {
        return $this->us_citizen;
    }

    /**
     * @param mixed $us_citizen
     */
    public function setUs_citizen($us_citizen)
    {
        $this->us_citizen = $us_citizen;
    }
    /**
     * @return mixed
     */
    public function getLanguage_1()
    {
        return $this->language_1;
    }

    /**
     * @param mixed $language_1
     */
    public function setLanguage_1($language_1)
    {
        $this->language_1 = $language_1;
    }

    /**
     * @return mixed
     */
    public function getLanguage_2()
    {
        return $this->language_2;
    }

    /**
     * @param mixed $language_2
     */
    public function setLanguage_2($language_2)
    {
        $this->language_2 = $language_2;
    }

    /**
     * @return mixed
     */
    public function getLanguage_3()
    {
        return $this->language_3;
    }

    /**
     * @param mixed $language_3
     */
    public function setLanguage_3($language_3)
    {
        $this->language_3 = $language_3;
    }
    /**
     * @return mixed
     */
    public function getCert_nursing()
    {
        return $this->cert_nursing;
    }

    /**
     * @param mixed $cert_nursing
     */
    public function setCert_nursing($cert_nursing)
    {
        $this->cert_nursing = $cert_nursing;
    }

    /**
     * @return mixed
     */
    public function getCert_redcross()
    {
        return $this->cert_redcross;
    }

    /**
     * @param mixed $cert_redcross
     */
    public function setCert_redcross($cert_redcross)
    {
        $this->cert_redcross = $cert_redcross;
    }

    /**
     * @return mixed
     */
    public function getCert_language()
    {
        return $this->cert_language;
    }

    /**
     * @param mixed $cert_language
     */
    public function setCert_language($cert_language)
    {
        $this->cert_language = $cert_language;
    }
    /**
     * @return mixed
     */
    public function getCert_teaching()
    {
        return $this->cert_teaching;
    }

    /**
     * @param mixed $cert_teaching
     */
    public function setCert_teaching($cert_teaching)
    {
        $this->cert_teaching = $cert_teaching;
    }

    /**
     * @return mixed
     */
    public function getCert_cpcr()
    {
        return $this->cert_cpcr;
    }

    /**
     * @param mixed $cert_cpcr
     */
    public function setCert_cpcr($cert_cpcr)
    {
        $this->cert_cpcr = $cert_cpcr;
    }

    /**
     * @return mixed
     */
    public function getCert_firstaid()
    {
        return $this->cert_firstaid;
    }

    /**
     * @param mixed $cert_firstaid
     */
    public function setCert_firstaid($cert_firstaid)
    {
        $this->cert_firstaid = $cert_firstaid;
    }

    /**
     * @return mixed
     */
    public function getCert_child_dev()
    {
        return $this->cert_child_dev;
    }

    /**
     * @param mixed $cert_child_dev
     */
    public function setCert_child_dev($cert_child_dev)
    {
        $this->cert_child_dev = $cert_child_dev;
    }

    /**
     * @return mixed
     */
    public function getCert_partner()
    {
        return $this->cert_partner;
    }

    /**
     * @param mixed $cert_partner
     */
    public function setCert_partner($cert_partner)
    {
        $this->cert_partner = $cert_partner;
    }
    /**
     * @return mixed
     */
    public function getPay_bankname()
    {
        return $this->pay_bankname;
    }

    /**
     * @param mixed $pay_bankname
     */
    public function setPay_bankname($pay_bankname)
    {
        $this->pay_bankname = $pay_bankname;
    }

    /**
     * @return mixed
     */
    public function getPay_routing()
    {
        return $this->pay_routing;
    }

    /**
     * @param mixed $pay_routing
     */
    public function setPay_routing($pay_routing)
    {
        $this->pay_routing = $pay_routing;
    }

    /**
     * @return mixed
     */
    public function getPay_account()
    {
        return $this->pay_account;
    }

    /**
     * @param mixed $pay_account
     */
    public function setPay_account($pay_account)
    {
        $this->pay_account = $pay_account;
    }

    /**
     * @return mixed
     */
    public function getPay_stripeid()
    {
        return $this->pay_stripeid;
    }

    /**
     * @param mixed $pay_stripeid
     */
    public function setPay_stripeid($pay_stripeid)
    {
        $this->pay_stripeid = $pay_stripeid;
    }

	
} 
?>
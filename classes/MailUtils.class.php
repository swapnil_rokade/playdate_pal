<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MailUtils {
    
    function MailUtils() {
    }
    
    public static function sendMail($email_address, $email_subject, $email_content) {
        $retorno = false;
        
        $mail = new PHPMailer(true);   // Passing `true` enables exceptions
        try {
            $sysConfig = new Config();
            
            //Server settings
            $mail->SMTPDebug = 2;                                 // Enable verbose debug output
            //$mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $sysConfig->getMailHost();  // Specify main and backup SMTP servers
            $mail->SMTPAuth = false;                               // Enable SMTP authentication
            $mail->Username = $sysConfig->getMailUsername();                 // SMTP username
            $mail->Password = $sysConfig->getMailPassword();                           // SMTP password
            //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->SMTPSecure = $sysConfig->getMailSecure();                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $sysConfig->getMailPort();                                    // TCP port to connect to
            
            //Recipients
            $mail->setFrom($sysConfig->getMailUsername(), $sysConfig->getMailFromname());
            $mail->addAddress($email_address);     // Add a recipient
            $mail->addCc($sysConfig->getMailUsername(), $sysConfig->getMailFromname());     // Add a recipient
            $mail->addReplyTo($sysConfig->getMailUsername(), $sysConfig->getMailFromname());
            
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $email_subject;
            $mail->Body    = $email_content;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            
            $mail->send();
            
            $emailSent = true;
            
        } catch (Exception $e) {
            echo 'Error sending email: ', $mail->ErrorInfo;
        }
        
        
        return $retorno;
    }
    
    public static function sendPlaydateInvitationCode($email, $parentName, $invitationCode) {
        
        $mailSubject = "You Have Been Invited to PAL!";
        
        $emailSent = false;
        
        if(($email!=null) && ($invitationCode!=null)) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-invitation.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ parent_name }}', $parentName, $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ invitation_code }}', $invitationCode, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent; 
        
    }
    
    public static function sendInvitationAcceptedCode($email, $invitationCode) {
        
        $mailSubject = "Request for Access Code Approved!";
        
        $emailSent = false;
        
        if(($email!=null) && ($invitationCode!=null)) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-request-invitation-accepted.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ invitation_code }}', $invitationCode, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendInvitationDeclined($email) {
        
        $mailSubject = "Request for Invite Code Denied";
        
        $emailSent = false;
        
        if(($email!=null) && ($invitationCode!=null)) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-request-invitation-denied.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ invitation_code }}', $invitationCode, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    /**
     * 
     * @param string $email
     * @return boolean
     */
    public static function sendNominateSpecialist($email) {
        
        $mailSubject = "You Have Been Nominated!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-nominate-specialist.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendRequestAccess($name, $email) {
        
        $mailSubject = "Request for Access Code Received";
        
        $emailSent = false;
        
        if($name!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-request-code.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ name }}', $name, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendRequestGroup($email, $name, $group) {
        
        $mailSubject = "Request to Join Group Received!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-request-group.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ group }}', $group, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendRequestGroupToAdmin($email, $name, $user_name, $group) {
        
        $mailSubject = "A user has requested access to your group";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-request-group-admin.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ user_name }}', $user_name, $template);
            $template = str_replace('{{ group }}', $group, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    
    public static function sendGroupAccept($email, $name, $group, $id_group="#") {
        
        $mailSubject = "Request to Join Group Approved";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-group-accept.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ group }}', $group, $template);
            $template = str_replace('{{ group_id }}', $id_group, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendGroupDenied($email, $name, $group) {
        
        $mailSubject = "Request to Join Group Denied";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-group-denied.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ group }}', $group, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    public static function sendGroupInvite($email, $name, $name_invite, $group) {
        
        $mailSubject = "You've Been Invited to the PAL Group ".$group;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-group-invite.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ name_invite }}', $name_invite, $template);
            $template = str_replace('{{ group }}', $group, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendPlaydateInviteFriend($email, $name, $friend_name, $event_name, $event_date, $event_place, $event_time,$event_end_time, $event_link) {
        
        $mailSubject = "You've Been Invited to join a Playdate with  ".$friend_name;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-invite-friend.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ friend-name }}', $friend_name, $template);
            $template = str_replace('{{ event-name }}', $event_name, $template);
            $template = str_replace('{{ event-date }}', $event_date, $template);
            $template = str_replace('{{ event-place }}', $event_place, $template);
            $template = str_replace('{{ event-time }}', $event_time, $template);
            $template = str_replace('{{ event-end-time }}', $event_end_time, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendPlaydateBooking($email, $name, $playdate_name, $playdate_date, $playdate_lead, $playdate_address, $playdate_pickup, $playdate_dropoff, $event_link="") {
        
        $mailSubject = "Playdate Confirmation: ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-booking.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-lead }}', $playdate_lead, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            $template = str_replace('{{ playdate-pickup }}', $playdate_pickup, $template);
            $template = str_replace('{{ playdate-dropoff }}', $playdate_dropoff, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }

    public static function sendPlaydateBookingToAdmin($email, $name, $playdate_name, $playdate_date, $playdate_lead, $playdate_address, $playdate_pickup, $playdate_dropoff,$child_name) {
        
        $mailSubject = "A Parent has reserved a playdate : ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-booking-admin.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-lead }}', $playdate_lead, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            $template = str_replace('{{ playdate-pickup }}', $playdate_pickup, $template);
            $template = str_replace('{{ playdate-dropoff }}', $playdate_dropoff, $template);
            $template = str_replace('{{ child-name }}', $child_name, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    /**
     * Notification for declined template (to parents and specialists)
     * Sent from ADMIN portal, when changing playdate status from PUBLISHED to DECLINED
     * 
     * @param string $email
     * @param string $name
     * @param string $playdate_name
     * @param string $playdate_date
     * @param string $playdate_lead
     * @param string $playdate_address
     * @return boolean
     */
    public static function sendPlaydateDeclined($email, $name, $playdate_name, $playdate_date, $playdate_lead, $playdate_address) {
        
        $mailSubject = "Uh oh! Due to low enrollment, your Playdate has been cancelled: ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-declined.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-lead }}', $playdate_lead, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendPlaydateNewJob($email, $name, $playdate_name, $playdate_date, $playdate_lead) {
        
        $mailSubject = "There is a new job request for Supports!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-supporters-new-job.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-lead }}', $playdate_lead, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendPlaydateBookingSpecialistPresentation($email, $name, $playdate_name, $playdate_date, $playdate_lead, $playdate_address, $playdate_pickup, $playdate_dropoff, $event_link="") {
        
        $mailSubject = "Hi ".$name." my name is ".$playdate_lead." and I am your PAL Specialist!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-booking-spec-presentation.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-lead }}', $playdate_lead, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            $template = str_replace('{{ playdate-pickup }}', $playdate_pickup, $template);
            $template = str_replace('{{ playdate-dropoff }}', $playdate_dropoff, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendPlaydateSpecialistConfirmation($email, $name, $playdate_name, $playdate_date, $playdate_address, $playdate_pickup, $playdate_dropoff, $event_link="") {
        
        $mailSubject = "Playdate Confirmation: ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-specialist-confirmed.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            $template = str_replace('{{ playdate-pickup }}', $playdate_pickup, $template);
            $template = str_replace('{{ playdate-dropoff }}', $playdate_dropoff, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendPlaydateSupportConfirmation($email, $name, $playdate_name, $playdate_date, $playdate_address, $playdate_pickup, $playdate_dropoff, $event_link="") {
        
        $mailSubject = "Playdate Confirmation: ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-support-confirmed.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            $template = str_replace('{{ playdate-pickup }}', $playdate_pickup, $template);
            $template = str_replace('{{ playdate-dropoff }}', $playdate_dropoff, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    
    public static function sendPlaydateSupportApplied($email, $name, $support_name, $playdate_name, $playdate_date, $playdate_address, $playdate_pickup, $playdate_dropoff, $event_link="") {
        
        $mailSubject = "Great news! A Support Specialist has applied to your Playdate: ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-support-applied.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ support_name }}', $support_name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            $template = str_replace('{{ playdate-pickup }}', $playdate_pickup, $template);
            $template = str_replace('{{ playdate-dropoff }}', $playdate_dropoff, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    /**
     * Mail to Support when a lead creates a playdate and select him as supporter
     * @param string $email
     * @param string $name - supporter mail
     * @param string $support_name
     * @param string $playdate_name
     * @param number $id_playdate
     * @return boolean
     */
    public static function sendPlaydateSupportSuggest($email, $name, $support_name, $playdate_name, $id_playdate) {
        
        $mailSubject = "Hi Pal! A Lead Specialist wants you to apply as Support!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-support-suggest.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ support_name }}', $support_name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ id_playdate }}', $id_playdate, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    /**
     * Notification when specialist add update
     * @param string $email
     * @param string $name
     * @param string $playdate_name
     * @param string $playdate_date
     * @param string $playdate_lead
     * @param string $event_link
     * @return boolean
     */
    public static function sendPlaydateSpecialistUpdate($email, $name, $playdate_name, $playdate_date, $playdate_update, $playdate_lead, $event_link="") {
        
        $mailSubject = "You Have Received a Comment on Your Playdate : ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-specialist-update.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-update }}', $playdate_update, $template);
            $template = str_replace('{{ playdate-lead }}', $playdate_lead, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    /**
     * Notification when parent add update
     * @param string $email
     * @param string $name
     * @param string $playdate_name
     * @param string $playdate_date
     * @param string $playdate_update
     * @param string $playdate_lead
     * @param string $event_link
     * @return boolean
     */
    public static function sendPlaydateParentUpdate($email, $name, $playdate_name, $playdate_date, $playdate_update, $playdate_lead, $event_link="") {
        
        $mailSubject = "You Have Received a Comment on Your Playdate : ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-parent-update.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-update }}', $playdate_update, $template);
            $template = str_replace('{{ playdate-lead }}', $playdate_lead, $template);
            $template = str_replace('{{ event-link }}', $event_link, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and not null
        
        return $emailSent;
        
    }
    
    public static function sendPlaydateParentRequest($email, $name) {
        
        $mailSubject = "Your Playdate Request Has Been Submitted";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-parent-request.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }
    
    public static function sendParentWelcome($email, $name) {
        
        $mailSubject = "Thank you for joining the PAL community!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-parent-welcome.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }

    public static function sendNewParentWelcome($email, $name,$invitationCode,$parentId) {
        
        $mailSubject = "Thank you for joining the PAL community!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-new-parent-welcome.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ invitation_code }}', $invitationCode, $template);
            $template = str_replace('{{ parent_id }}', $parentId, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }

    //no matched parent
    public static function sendNewParentNoData($email, $name) {
        
        $mailSubject = "Your access request is pending";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-new-parent-no-match.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
                        
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }

    public static function sendNomatchData($email, $body) {
        
        $mailSubject = "Your access request is pending";//"Non verified parent details!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $first_name = $body["first_name"];
            $last_name = $body["last_name"];
			$dob = 	$body["date_of_birth"];
			$postal_code =	$body["postal_code"];
			$parent_email =	$body["email"];


            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/non-verified-parent-data.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ first_name }}', $first_name, $template);
            $template = str_replace('{{ last_name }}', $last_name, $template);
            $template = str_replace('{{ dob }}', $dob, $template);
            $template = str_replace('{{ postal_code }}', $postal_code, $template);
            $template = str_replace('{{ parent_email }}', $parent_email, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }
    
    public static function sendMatchData($email, $body) {
        
        $mailSubject = "New parent has registered!";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $first_name = $body["first_name"];
            $last_name = $body["last_name"];
			$postal_code =	$body["postal_code"];
			$parent_email =	$body["email"];


            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/verified-parent-data.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ first_name }}', $first_name, $template);
            $template = str_replace('{{ last_name }}', $last_name, $template);
            $template = str_replace('{{ postal_code }}', $postal_code, $template);
            $template = str_replace('{{ parent_email }}', $parent_email, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }

    public static function sendSpecialistWelcome($email, $name) {
        
        $mailSubject = "Congratulations! You have been selected to join the PAL Community.";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-specialist-welcome.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }
    
    public static function sendSpecialistExpectations($email, $name) {
        
        $mailSubject = "Playdate Expectations";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-specialist-expectations.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email is not null
        
        return $emailSent;
        
    }
    
    public static function sendItineraryAccepted($email, $name, $playdate_name, $playdate_date, $playdate_address) {
        
        $mailSubject = "Itinerary Approved: ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-itinerary-accepted.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendItineraryDeclined($email, $name, $playdate_name, $playdate_date, $playdate_address) {
        
        $mailSubject = "Itinerary Declined: ".$playdate_name." | ".$playdate_date;
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-itinerary-declined.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ playdate-name }}', $playdate_name, $template);
            $template = str_replace('{{ playdate-date }}', $playdate_date, $template);
            $template = str_replace('{{ playdate-address }}', $playdate_address, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    
    public static function sendPlaypackConfirm($email, $name, $date, $order) {
        
        $mailSubject = "PAL Purchase Confirmation";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-playpack-confirm.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ date }}', $date, $template);
            $template = str_replace('{{ order }}', $order, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }

    public static function sendPlaypackConfirmToAdmin($email, $name, $date, $order) {
        
        $mailSubject = "A Parent has purchased credit";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-playpack-confirm-admin.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ date }}', $date, $template);
            $template = str_replace('{{ order }}', $order, $template);
            
            $emailSent = MailUtils::sendMail("sujata.ambatkar@gmail.com", $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
    
    public static function sendPasswordReset($email, $name, $newpassword) {
        
        $mailSubject = "Your password has been reset";
        
        $emailSent = false;
        
        if($email!=null) {
            $sysConfig = new Config();
            
            $template = file_get_contents($sysConfig->getDocumentRoot()."/mail/templates/playdate-password-reset.html");
            
            $template = str_replace('{{ domain }}', $sysConfig->getMailBasedomain(), $template);
            $template = str_replace('{{ email }}', $email, $template);
            $template = str_replace('{{ name }}', $name, $template);
            $template = str_replace('{{ newpassword }}', $newpassword, $template);
            
            $emailSent = MailUtils::sendMail($email, $mailSubject, $template);
            
        } //if email and code are not null
        
        return $emailSent;
        
    }
}
?>
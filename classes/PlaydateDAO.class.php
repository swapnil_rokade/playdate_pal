<?php
include_once ("BaseDAO.class.php");

class PlaydateDAO extends BaseDAO {

    function PlaydateDAO() {
	}

	/**
	 * TEST if playdate is past
	 * @param mixed $id_playdate
	 * @return boolean
	 */
	public static function isPastPlaydate($id_playdate) {
	    $link = getConnection();
	    $sql = "select count(1) FROM pd_playdates WHERE id_playdate=$id_playdate AND date<CURRENT_DATE";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	
	public static function countPlaydates() {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_playdates", $link) or die("Error counting Playdates");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}
	
	public static function countPlaydatesBySpecialist($id_specialist) {
	    $link = getConnection();
	    $sql = "SELECT COUNT(1) FROM pd_playdates WHERE id_specialist=".$id_specialist;
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	public static function countFamiliesRepeatBySpecialist($id_specialist) {
	    $link = getConnection();
	    $sql = "SELECT count(1) FROM (select a.id_parent, a.id_specialist, count(1) as veces FROM (select r.id_playdate, r.id_parent, p.id_specialist FROM pd_playdate_parent_reservation r, pd_playdates p WHERE r.id_playdate=p.id_playdate AND id_specialist=".$id_specialist.") a GROUP BY id_parent, id_specialist) w WHERE w.veces>1";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	
	public static function countPlaydatesSearch($query) {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_playdates WHERE name like '%$query%' or description like '%$query%'", $link) or die("Error counting playdates");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	
	public static function getPlaydatesList($page=1, $elemsPerPage=1000, $sortfield=null, $sorttype=null) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT * FROM pd_playdates ";
		//Ordenacion
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
		    $sql .= " order by id_playdate DESC";
		    
		}

		//Start and range
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first
		$max = $elemsPerPage; # elements per page

		//paging
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
		    $newItem = new Playdate(-1);
		    $newItem->readFromRow($row);
		    $retorno[] = $newItem;
		}
		mysql_close($link);
		return $retorno;
	}

	public static function getPlaydatesListByParentRequest($id_parent, $page=1, $elemsPerPage=1000, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_playdates p, pd_playdate_requests pr WHERE p.id_request=pr.id_request AND pr.id_parent=".$id_parent;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_playdate DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}

	/**
	 * 
	 * @param mixed $id_specialist
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Playdate[]
	 */
	public static function getUpcomingPlaydatesListBySpecialist($id_specialist, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdates WHERE id_specialist=$id_specialist and status in(".Playdate::$STATUS_PUBLISHED.", ".Playdate::$STATUS_RSVP.") and date >= CURRENT_DATE()";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_playdate DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * Find upcoming playdates where specialist is supporter
	 * @param mixed $id_specialist
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Playdate[]
	 */
	public static function getUpcomingPlaydatesListBySupporter($id_specialist, $page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    $sql = "SELECT p.* FROM pd_playdates p, pd_playdate_supporters s WHERE p.id_playdate=s.id_playdate AND s.status=1 AND s.id_specialist=".$id_specialist."  AND p.status in(".Playdate::$STATUS_PUBLISHED.", ".Playdate::$STATUS_RSVP.") and p.date >= CURRENT_DATE()";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_playdate DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	/**
	 *
	 * @param mixed $id_specialist
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Playdate[]
	 */
	public static function getUpcomingPlaydatesListBySpecialistWithSupportRequests($id_specialist, $page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT DISTINCT pd.* FROM pd_playdates pd, pd_playdate_supporters ps WHERE pd.id_playdate=ps.id_playdate AND ps.status=".Specialist::$SUPPORTER_REQUESTED." AND pd.id_specialist=$id_specialist and pd.status=".Playdate::$STATUS_PUBLISHED." and pd.date >= CURRENT_DATE()";
	    //$sql = "SELECT DISTINCT pd.* FROM pd_playdates pd, pd_playdate_supporters ps WHERE pd.id_playdate=ps.id_playdate AND ps.status IN(".Specialist::$SUPPORTER_REQUESTED.", ".Specialist::$SUPPORTER_DECLINED.") AND pd.id_specialist=$id_specialist and pd.status=".Playdate::$STATUS_PUBLISHED." and pd.date >= CURRENT_DATE()";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by pd.".$sortfield;
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by pd.id_playdate DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getUpcomingPlaydatesList($page, $elemsPerPage, $sortfield, $sorttype, $onlyPublic=true) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdates WHERE status=".Playdate::$STATUS_PUBLISHED." and date >= CURRENT_DATE()";
	    if($onlyPublic) {
	        $sql .= " AND privacy_type=".Playdate::$PRIVACITY_PUBLIC;
	    }
	    
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by date ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //echo("::::[$sql]::::::"); die();
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * 
	 */
	public static function getUpcomingFeaturedPlaydatesList($page, $elemsPerPage, $sortfield, $sorttype, $onlyPublic=true) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdates WHERE featured=".Playdate::$FEATURED_YES." AND status=".Playdate::$STATUS_PUBLISHED." and date >= CURRENT_DATE()";
	    if($onlyPublic) {
	        $sql .= " AND privacy_type=".Playdate::$PRIVACITY_PUBLIC;
	    }
	    
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by date ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //echo("::::[$sql]::::::"); die();
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * 
	 * @param number $page
	 * @param number $elemsPerPage
	 * @param number $sortfield
	 * @param number $sorttype
	 * @param boolean $onlyPublic
	 * @return Playdate[]
	 */
	public static function getUpcomingPlaydatesFindingSupportersList($page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    
	    $playdatesRetorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdates p WHERE p.status=".Playdate::$STATUS_PUBLISHED." and (p.num_supporters>0 OR p.num_children>4 OR p.open_spots>4) and p.date >= CURRENT_DATE()";
	    
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by date ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //echo("::::[$sql]::::::"); die();
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $playdatesRetorno[] = $newItem;
	    }
	    mysql_close($link);
	    
	    //Filtramos para mostrar solo los que necesiten supporters
	    foreach($playdatesRetorno as $auxPlaydate) {
	        $supplist = SpecialistDAO::getSupportersByPlaydate($auxPlaydate->getId_playdate());
	        if(($supplist==null) || (count($supplist)<$auxPlaydate->getNum_supporters())) {
	            $retorno[] = $auxPlaydate;
	        }
	    }
	    
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param number $page
	 * @param number $elemsPerPage
	 * @param number $sortfield
	 * @param number $sorttype
	 * @param boolean $onlyPublic
	 * @return Playdate[]
	 */
	public static function getUpcomingPlaydatesFindingSupportersListBySpecialist($id_specialist, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    
	    $playdatesRetorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdates p WHERE p.status=".Playdate::$STATUS_PUBLISHED." and (p.num_supporters>0 OR p.num_children>4 OR p.open_spots>4 OR p.id_playdate IN (SELECT id_playdate FROM pd_playdate_supporters s WHERE s.id_specialist=".$id_specialist.")) and p.date >= CURRENT_DATE()";
	    
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by date ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //echo("::::[$sql]::::::"); die();
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $playdatesRetorno[] = $newItem;
	    }
	    mysql_close($link);
	    
	    //Filtramos para mostrar solo los que necesiten supporters
	    foreach($playdatesRetorno as $auxPlaydate) {
	        $supplist = SpecialistDAO::getSupportersByPlaydate($auxPlaydate->getId_playdate());
	        if(($supplist==null) || (count($supplist)<$auxPlaydate->getNum_supporters())) {
	            $retorno[] = $auxPlaydate;
	        }
	    }
	    
	    
	    return $retorno;
	}
	
	
	
	/**
	 *
	 * @param mixed $id_parent
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Playdate[]
	 */
	public static function getUpcomingPlaydatesListByParent($id_parent, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_playdates p, pd_playdate_parent_reservation r WHERE p.status=".Playdate::$STATUS_PUBLISHED." AND p.id_playdate=r.id_playdate AND r.id_parent=$id_parent AND r.status=".ParentReservation::$STATUS_RESERVED." and p.date >= CURRENT_DATE()";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by p.".$sortfield;
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.date ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 *
	 * @param mixed $id_group
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Playdate[]
	 */
	public static function getUpcomingPlaydatesListByGroup($id_group, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT DISTINCT p.* FROM pd_playdates p, pd_group_playdates r WHERE ((p.id_playdate=r.id_playdate AND r.id_group=$id_group) OR (p.privacy_type=".Playdate::$PRIVACITY_GROUP." AND p.privacy_group_id=".$id_group."))  AND p.date >= CURRENT_DATE() AND p.status=".Playdate::$STATUS_PUBLISHED;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by p.".$sortfield;
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.date ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 *
	 * @param mixed $id_group
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Playdate[]
	 */
	public static function getPastPlaydatesListByGroup($id_group, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    //$sql = "SELECT p.* FROM pd_playdates p, pd_group_playdates r WHERE p.id_playdate=r.id_playdate AND r.id_group=$id_group and p.date < CURRENT_DATE() AND p.status IN(".Playdate::$STATUS_PUBLISHED.",".Playdate::$STATUS_RSVP.",".Playdate::$STATUS_FINISHED.")";
	    $sql = "SELECT DISTINCT p.* FROM pd_playdates p, pd_group_playdates r WHERE ((p.id_playdate=r.id_playdate AND r.id_group=$id_group) OR (p.privacy_type=".Playdate::$PRIVACITY_GROUP." AND p.privacy_group_id=".$id_group."))  AND p.date < CURRENT_DATE() AND p.status IN(".Playdate::$STATUS_PUBLISHED.",".Playdate::$STATUS_RSVP.",".Playdate::$STATUS_FINISHED.")";
	    
	    
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by p.".$sortfield;
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.date ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 *
	 * @param mixed $id_parent
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Playdate[]
	 */
	public static function getPastPlaydatesListByParent($id_parent, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_playdates p, pd_playdate_parent_reservation r WHERE p.id_playdate=r.id_playdate AND r.id_parent=$id_parent AND r.status=".ParentReservation::$STATUS_RESERVED." AND p.date < CURRENT_DATE() AND p.status IN(".Playdate::$STATUS_PUBLISHED.",".Playdate::$STATUS_RSVP.",".Playdate::$STATUS_FINISHED.")";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by p.".$sortfield;
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.date DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * Parents with playdate reservation
	 * @param mixed $id_playdate
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return ParentReservation[]
	 */
	public static function getParentReservationByPlaydate($id_parent, $id_playdate, $onlyReserved=true) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_parent_reservation WHERE id_parent=".$id_parent." AND id_playdate= ".$id_playdate;
	    if($onlyReserved) {
	        $sql .=" AND status=".ParentReservation::$STATUS_RESERVED;
	    }
	    //results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newReservation = new ParentReservation(-1);
	        $newReservation->readFromRow($row);
	        $retorno = $newReservation;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * Get Parent Reservation
	 * @param number $id_reservation
	 * @return NULL|ParentReservation
	 */
	public static function getParentReservation($id_reservation) {
	    $retorno = null;
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_parent_reservation WHERE id_reservation=".$id_reservation;
	    
	    //results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newReservation = new ParentReservation(-1);
	        $newReservation->readFromRow($row);
	        $retorno = $newReservation;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	/**
	 * Parents with playdate reservation
	 * @param mixed $id_playdate
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return ParentReservation[]
	 */
	public static function getAllParentReservationByPlaydate($id_playdate, $onlyReserved=true) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_parent_reservation WHERE id_playdate= ".$id_playdate;
	    if($onlyReserved) {
	        $sql .= " AND status=".ParentReservation::$STATUS_RESERVED;
	    }
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newReservation = new ParentReservation(-1);
	        $newReservation->readFromRow($row);
	        $retorno[] = $newReservation;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	
	public static function getPastPlaydatesListBySpecialist($id_specialist, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdates WHERE id_specialist=$id_specialist and date < CURRENT_DATE() AND status IN(".Playdate::$STATUS_PUBLISHED.",".Playdate::$STATUS_RSVP.",".Playdate::$STATUS_FINISHED.")";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_playdate DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function getSubmittedPlaydatesListBySpecialist($id_specialist, $page=1, $elemsPerPage=1000, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    //$sql = "SELECT * FROM pd_playdates WHERE id_specialist=$id_specialist and status=".Playdate::$STATUS_REQUEST." and id_request is not null and date >= CURRENT_DATE()";
	    $sql = "SELECT * FROM pd_playdates WHERE id_specialist=$id_specialist and id_request is not null and date >= CURRENT_DATE()";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_playdate DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //echo("::[$sql]::");
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playdate(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	
	
	public static function getPlaydatesSearch($playdateSearch, $page, $elemsPerPage, $sortfield, $sorttype) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_playdates s WHERE 1=1";
		
		//Adding query parameters
		/*
		$userSearch = new ParentPd();
		
		if(($userSearch->getPerfil()!=null)&&($userSearch->getPerfil()!="")) {
			$sql.=" AND perfil='".$userSearch->getPerfil()."'";
		}
		*/
		
		//order
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		}

		//paging values
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first element
		$max = $elemsPerPage; # num elements

		//paging limits
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
		    $newItem = new Playdate(-1);
		    $newItem->readFromRow($row);
		    $retorno[] = $newItem;
		}
		mysql_close($link);
		return $retorno;
	}
	
	
	public static function getPlaydate($id_playdate) {
		$retorno = array ();
		
		if(is_numeric($id_playdate) && ($id_playdate>=0)) {
			$link = getConnection();
	
			$sql = "SELECT s.* FROM pd_playdates s WHERE id_playdate=$id_playdate";
	
			//reading results
			$result = mysql_query($sql, $link);
			if ($row = mysql_fetch_assoc($result)) {
			    $newItem = new Playdate(-1);
			    $newItem->readFromRow($row);
			    $retorno = $newItem;
			}
			mysql_close($link);
		}
		return $retorno;
	}

	

	/**
	 * Playdate creation
	 * @param Playdate $newPlaydate
	 * @return mixed
	 */
	public static function createPlaydate($newPlaydate) {
		$retorno = null;

		if (isset ($newPlaydate) && ($newPlaydate->getName() != "")) {
			//Save playdate
			$link = getConnection();

			//$newPlaydate = new Playdate();
			
			$sql_frm = "INSERT INTO pd_playdates (name, loc_address, loc_city, loc_state, loc_id_zipcode, ". 
                        "date, time_init, time_end, age_init, age_end, description, picture, num_children, min_kids, ".
                        "invite_parents, privacy_type, type, num_supporters, privacy_group_id, open_spots, id_request, id_specialist, idate, status, slug, id_parent, ".
                        "pickup_address, pickup_walk, pickup_train, pickup_car, pickup_taxi, dropoff_address, dropoff_walk, dropoff_train, dropoff_car, dropoff_taxi, ".
                        "act1_id, act1_note, act1_time, act2_id, act2_note, act2_time, act3_id, act3_note, act3_time, act4_id, act4_note, act4_time, ". 
                        "add1_id, add1_price, add2_id, add2_price, add3_id, add3_price, add4_id, add4_price,add1_id_check,add2_id_check,add3_id_check,add4_id_check,type_of_playdate,date_1,date_2,date_3,date_4,date_5,date_6,date_7,date_8) " .
			"VALUES (".
			         "'".mysql_real_escape_string($newPlaydate->getName())."', ".
			         "'".mysql_real_escape_string($newPlaydate->getLoc_address())."', ".
			         "'".mysql_real_escape_string($newPlaydate->getLoc_city())."', ".
			         "'".mysql_real_escape_string($newPlaydate->getLoc_state())."', ".
			         "".(($newPlaydate->getLoc_id_zipcode()!=null)?mysql_real_escape_string($newPlaydate->getLoc_id_zipcode()):"null").", ".
			         "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate())."', ".
			         "".(($newPlaydate->getTime_init()!=null)?mysql_real_escape_string($newPlaydate->getTime_init()):"null").", ".
			         "".(($newPlaydate->getTime_end()!=null)?mysql_real_escape_string($newPlaydate->getTime_end()):"null").", ".
			         "".(($newPlaydate->getAge_init()!=null)?mysql_real_escape_string($newPlaydate->getAge_init()):"null").", ".
			         "".(($newPlaydate->getAge_end()!=null)?mysql_real_escape_string($newPlaydate->getAge_end()):"null").", ".
			         "'".mysql_real_escape_string($newPlaydate->getDescription())."', ".
			         "'".mysql_real_escape_string($newPlaydate->getPicture())."', ".
			         //"".(($newPlaydate->getNum_children()!=null)?mysql_real_escape_string($newPlaydate->getNum_children()):"null").", ".
			         "".(($newPlaydate->getOpen_spots()!=null)?mysql_real_escape_string($newPlaydate->getOpen_spots()):"null").", ". //we insert open spots in number of children, because "number of children" not longer exist
			         "".(($newPlaydate->getMin_kids()!=null)?mysql_real_escape_string($newPlaydate->getMin_kids()):"1").", ". //20181221 min kids feature
			         "'".mysql_real_escape_string($newPlaydate->getInvite_parents())."', ".
			         "".(($newPlaydate->getPrivacy_type()!=null)?mysql_real_escape_string($newPlaydate->getPrivacy_type()):"null").", ".
			         "".(($newPlaydate->getType()!=null)?mysql_real_escape_string($newPlaydate->getType()):"null").", ".
			         "".(($newPlaydate->getNum_supporters()!=null)?mysql_real_escape_string($newPlaydate->getNum_supporters()):"null").", ".
			         "".(($newPlaydate->getPrivacy_group_id()!=null)?mysql_real_escape_string($newPlaydate->getPrivacy_group_id()):"null").", ".
			         "".(($newPlaydate->getOpen_spots()!=null)?mysql_real_escape_string($newPlaydate->getOpen_spots()):"null").", ".
			         "".(($newPlaydate->getId_request()!=null)?mysql_real_escape_string($newPlaydate->getId_request()):"null").", ".
			         "".(($newPlaydate->getId_specialist()!=null)?mysql_real_escape_string($newPlaydate->getId_specialist()):"null").", ".
			         "now(),".
			         "".mysql_real_escape_string($newPlaydate->getStatus()).", ".
			         "'".mysql_real_escape_string($newPlaydate->getSlug())."', ".
			         "".(($newPlaydate->getId_parent()!=null)?mysql_real_escape_string($newPlaydate->getId_parent()):"null").", ".
			         "".(($newPlaydate->getPickup_address()!=null)?("'".mysql_real_escape_string($newPlaydate->getPickup_address())."'"):"null").", ".
			         "".(($newPlaydate->getPickup_walk()!=null)?mysql_real_escape_string($newPlaydate->getPickup_walk()):"null").", ".
			         "".(($newPlaydate->getPickup_train()!=null)?mysql_real_escape_string($newPlaydate->getPickup_train()):"null").", ".
			         "".(($newPlaydate->getPickup_car()!=null)?mysql_real_escape_string($newPlaydate->getPickup_car()):"null").", ".
			         "".(($newPlaydate->getPickup_taxi()!=null)?mysql_real_escape_string($newPlaydate->getPickup_taxi()):"null").", ".
			         "".(($newPlaydate->getDropoff_address()!=null)?("'".mysql_real_escape_string($newPlaydate->getDropoff_address())."'"):"null").", ".
			         "".(($newPlaydate->getDropoff_walk()!=null)?mysql_real_escape_string($newPlaydate->getDropoff_walk()):"null").", ".
			         "".(($newPlaydate->getDropoff_train()!=null)?mysql_real_escape_string($newPlaydate->getDropoff_train()):"null").", ".
			         "".(($newPlaydate->getDropoff_car()!=null)?mysql_real_escape_string($newPlaydate->getDropoff_car()):"null").", ".
			         "".(($newPlaydate->getDropoff_taxi()!=null)?mysql_real_escape_string($newPlaydate->getDropoff_taxi()):"null").", ".
			         "".(($newPlaydate->getAct1_id()!=null)?mysql_real_escape_string($newPlaydate->getAct1_id()):"null").", ".
			         "".(($newPlaydate->getAct1_note()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct1_note())."'"):"null").", ".
			         "".(($newPlaydate->getAct1_time()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct1_time())."'"):"null").", ".
			         "".(($newPlaydate->getAct2_id()!=null)?mysql_real_escape_string($newPlaydate->getAct2_id()):"null").", ".
			         "".(($newPlaydate->getAct2_note()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct2_note())."'"):"null").", ".
			         "".(($newPlaydate->getAct2_time()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct2_time())."'"):"null").", ".
			         "".(($newPlaydate->getAct3_id()!=null)?mysql_real_escape_string($newPlaydate->getAct3_id()):"null").", ".
			         "".(($newPlaydate->getAct3_note()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct3_note())."'"):"null").", ".
			         "".(($newPlaydate->getAct3_time()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct3_time())."'"):"null").", ".
			         "".(($newPlaydate->getAct4_id()!=null)?mysql_real_escape_string($newPlaydate->getAct4_id()):"null").", ".
			         "".(($newPlaydate->getAct4_note()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct4_note())."'"):"null").", ".
			         "".(($newPlaydate->getAct4_time()!=null)?("'".mysql_real_escape_string($newPlaydate->getAct4_time())."'"):"null").", ".
			         "".(($newPlaydate->getAdd1_id()!=null)?mysql_real_escape_string($newPlaydate->getAdd1_id()):"null").", ".
			         "".(($newPlaydate->getAdd1_price()!=null)?mysql_real_escape_string($newPlaydate->getAdd1_price()):"null").", ".
			         "".(($newPlaydate->getAdd2_id()!=null)?mysql_real_escape_string($newPlaydate->getAdd2_id()):"null").", ".
			         "".(($newPlaydate->getAdd2_price()!=null)?mysql_real_escape_string($newPlaydate->getAdd2_price()):"null").", ".
			         "".(($newPlaydate->getAdd3_id()!=null)?mysql_real_escape_string($newPlaydate->getAdd3_id()):"null").", ".
			         "".(($newPlaydate->getAdd3_price()!=null)?mysql_real_escape_string($newPlaydate->getAdd3_price()):"null").", ".
			         "".(($newPlaydate->getAdd4_id()!=null)?mysql_real_escape_string($newPlaydate->getAdd4_id()):"null").", ".
					 "".(($newPlaydate->getAdd4_price()!=null)?mysql_real_escape_string($newPlaydate->getAdd4_price()):"null").",".
					 "".(($newPlaydate->getAdd1_id_check()!=null)?mysql_real_escape_string($newPlaydate->getAdd1_id_check()):"0").",".
			         "".(($newPlaydate->getAdd2_id_check()!=null)?mysql_real_escape_string($newPlaydate->getAdd2_id_check()):"0").",".
			         "".(($newPlaydate->getAdd3_id_check()!=null)?mysql_real_escape_string($newPlaydate->getAdd3_id_check()):"0").",".
			         "".(($newPlaydate->getAdd4_id_check()!=null)?mysql_real_escape_string($newPlaydate->getAdd4_id_check()):"0").",".
					 "".(($newPlaydate->getType_of_Playdate()!=null)?mysql_real_escape_string($newPlaydate->getType_of_Playdate()):"1").",".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_1())."',".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_2())."',".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_3())."',".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_4())."',".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_5())."',".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_6())."',".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_7())."',".
					 "'".BaseDAO::toMysqlDateFormat($newPlaydate->getDate_8())."'".
			")";

			//echo("::[$sql_frm]::"); die();
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			//Obtenemos el id del nuevo playdate
			$link = getConnection();
			$sql_frm = "SELECT * FROM pd_playdates WHERE id_specialist=".$newPlaydate->getId_specialist()." AND idate>date_sub(now(), interval 30 second) order by id_playdate desc limit 1";
			$result_frm = mysql_query($sql_frm, $link);
			$newPlaydate->readFromRow(mysql_fetch_assoc($result_frm));

			if (is_numeric($newPlaydate->getId_playdate()) && ($newPlaydate->getId_playdate()>0)) {
			    $retorno = $newPlaydate;
			}
			mysql_close($link);
		}
		return $retorno;
	}

	/**
	 * Playdate reservation
	 * @param mixed $newPlaydate
	 * @return mixed
	 */
	public static function playdateParentReservation($id_playdate, $id_parent, $num_spots=1) {
	    $retorno = false;
	    
	    if (($id_playdate>0) && ($id_parent>0) && (is_numeric($num_spots))) {
	        //Save reservation
	        $link = getConnection();
	        
	        //$newPlaydate = new Playdate();
	        
	        $sql_frm = "INSERT INTO pd_playdate_parent_reservation (id_playdate, id_parent, num_spots, idate) " .
	   	        "VALUES (".
	   	        "".$id_playdate.", ".
	   	        "".$id_parent.", ".
	   	        "".$num_spots.", ".
	   	        "now()".
	   	        ")  ON DUPLICATE KEY update udate=now();";
	        
	        //echo("[$sql_frm]"); die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    return $retorno;
	}
	
	public static function countPlaydateReservations($id_playdate) {
	    $link = getConnection();
	    $sql = "select IFNULL(sum(num_children), 0) as reservations from pd_reservation_children c, pd_playdate_parent_reservation p WHERE c.id_reservation=p.id_reservation AND p.status<>".ParentReservation::$STATUS_CANCELED." AND p.id_playdate=".$id_playdate;
	    //echo(":::::[$sql]::::");die();
	    $rs = mysql_query($sql, $link) or die("Error counting playdate reservations");
	    list ($total) = mysql_fetch_row($rs);
	    return $total;
	}
	
	/**
	 * 
	 * @param mixed $id_playdate
	 * @return ChildrenReservation[]
	 */
	public static function getChildrenReservations($id_playdate) {
	    $retorno = array ();
	    $link = getConnection();
	    $sql = "select c.* FROM pd_reservation_children c, pd_playdate_parent_reservation p WHERE c.id_reservation=p.id_reservation AND p.id_playdate=".$id_playdate;

	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new ChildrenReservation(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    
	    return $retorno;
	}

	/**
	 *
	 * @param mixed $id_playdate
	 * @return ChildrenReservation[]
	 */
	public static function getChildrenReservationsByParent($id_playdate, $id_parent, $onlyReserved=true) {
	    $retorno = array ();
	    $link = getConnection();
	    $sql = "select c.* FROM pd_reservation_children c, pd_playdate_parent_reservation p WHERE c.id_reservation=p.id_reservation AND p.id_playdate=".$id_playdate." AND p.id_parent=".$id_parent;
	    if($onlyReserved) {
	        $sql.=" AND p.status=".ParentReservation::$STATUS_RESERVED;
	    }
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new ChildrenReservation(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    
	    return $retorno;
	}
	
	
	/**
	 *
	 * @param mixed $id_playdate
	 * @return ChildrenReservation[]
	 */
	public static function getChildrenReservationsByReservation($id_reservation) {
	    $retorno = array ();
	    $link = getConnection();
	    $sql = "select c.* FROM pd_reservation_children c WHERE c.id_reservation=".$id_reservation;
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new ChildrenReservation(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    
	    return $retorno;
	}
	
	
	
	/**
	 * 
	 * @param Playdate $editPlaydate
	 * @return NULL|array|Playdate
	 */
	public static function updatePlaydate($editPlaydate) {
		$retorno = null;
		if (isset ($editPlaydate) && (is_numeric($editPlaydate->getId_playdate()))) {
			
			//Updating playdate info
			$link = getConnection();
			
			//$editPlaydate = new Playdate();
			
			$sql_frm = "UPDATE pd_playdates SET name='" . mysql_real_escape_string($editPlaydate->getName()) . "', " .
			"loc_address='" . mysql_real_escape_string($editPlaydate->getLoc_address()) . "', " .
			"loc_city='" . mysql_real_escape_string($editPlaydate->getLoc_city()) . "', " .
			"loc_state='" . mysql_real_escape_string($editPlaydate->getLoc_state()) . "', " .
			"loc_id_zipcode=" .(($editPlaydate->getLoc_id_zipcode()!=null)?(mysql_real_escape_string($editPlaydate->getLoc_id_zipcode())):"null"). ", " .
			"id_neighborhood=" .(($editPlaydate->getId_neighborhood()!=null)?(mysql_real_escape_string($editPlaydate->getId_neighborhood())):"null"). ", " .
			"date='" . BaseDAO::toMysqlDateFormat($editPlaydate->getDate()) . "', " .
			"time_init='" . $editPlaydate->getTime_init() . "', " .
			"time_end='" . $editPlaydate->getTime_end() . "', " .
			"age_init='" . $editPlaydate->getAge_init() . "', " .
			"age_end='" . $editPlaydate->getAge_end() . "', " .
			"featured=" . $editPlaydate->getFeatured() . ", ".
			"description='" . mysql_real_escape_string($editPlaydate->getDescription()) . "', " .
			"picture='" . mysql_real_escape_string($editPlaydate->getPicture()) . "', " .
			"num_children='" . $editPlaydate->getNum_children() . "', " .
			"min_kids='" . $editPlaydate->getMin_kids() . "', " .
			"act1_id=" .(($editPlaydate->getAct1_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct1_id())."'"):"null"). ", " .
			"act1_note=" .(($editPlaydate->getAct1_note()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct1_note())."'"):"null"). ", " .
			"act1_time=" .(($editPlaydate->getAct1_time()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct1_time())."'"):"null"). ", " .
			"act2_id=" .(($editPlaydate->getAct2_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct2_id())."'"):"null"). ", " .
			"act2_note=" .(($editPlaydate->getAct2_note()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct2_note())."'"):"null"). ", " .
			"act2_time=" .(($editPlaydate->getAct2_time()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct2_time())."'"):"null"). ", " .
			"act3_id=" .(($editPlaydate->getAct3_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct3_id())."'"):"null"). ", " .
			"act3_note=" .(($editPlaydate->getAct3_note()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct3_note())."'"):"null"). ", " .
			"act3_time=" .(($editPlaydate->getAct3_time()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct3_time())."'"):"null"). ", " .
			"act4_id=" .(($editPlaydate->getAct4_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct4_id())."'"):"null"). ", " .
			"act4_note=" .(($editPlaydate->getAct4_note()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct4_note())."'"):"null"). ", " .
			"act4_time=" .(($editPlaydate->getAct4_time()!=null)?("'".mysql_real_escape_string($editPlaydate->getAct4_time())."'"):"null"). ", " .
			"add1_id=" .(($editPlaydate->getAdd1_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd1_id())."'"):"null"). ", " .
			"add1_price=" .(($editPlaydate->getAdd1_price()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd1_price())."'"):"null"). ", " .
			"add2_id=" .(($editPlaydate->getAdd2_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd2_id())."'"):"null"). ", " .
			"add2_price=" .(($editPlaydate->getAdd2_price()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd2_price())."'"):"null"). ", " .
			"add3_id=" .(($editPlaydate->getAdd3_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd3_id())."'"):"null"). ", " .
			"add3_price=" .(($editPlaydate->getAdd3_price()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd3_price())."'"):"null"). ", " .
			"add4_id=" .(($editPlaydate->getAdd4_id()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd4_id())."'"):"null"). ", " .
			"add4_price=" .(($editPlaydate->getAdd4_price()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd4_price())."'"):"null"). ", " .
			"add1_id_check=" .(($editPlaydate->getAdd1_id_check()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd1_id_check())."'"):"0"). ", " .
			"add2_id_check=" .(($editPlaydate->getAdd2_id_check()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd2_id_check())."'"):"0"). ", " .
			"add3_id_check=" .(($editPlaydate->getAdd3_id_check()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd3_id_check())."'"):"0"). ", " .
			"add4_id_check=" .(($editPlaydate->getAdd4_id_check()!=null)?("'".mysql_real_escape_string($editPlaydate->getAdd4_id_check())."'"):"0"). ", " .
			"invite_parents='" . mysql_real_escape_string($editPlaydate->getInvite_parents()) . "', " .
			"privacy_type='" . $editPlaydate->getPrivacy_type() . "', ";
			
			if($editPlaydate->getPrivacy_group_id()!=null) {
			    $sql_frm .= "privacy_group_id='" . $editPlaydate->getPrivacy_group_id() . "', ";
			} else {
			    $sql_frm .= "privacy_group_id=null, ";
			}
			
			if($editPlaydate->getType()!=null) {
			    $sql_frm .= "type='" . $editPlaydate->getType() . "', ";
			} else {
			    $sql_frm .= "type=null, ";
			}
			
			$sql_frm .= "open_spots='" . $editPlaydate->getOpen_spots() . "', " ;
			 			
			if($editPlaydate->getId_specialist()!=null) {
			     $sql_frm .= "id_specialist='" . $editPlaydate->getId_specialist() . "', ";
			} else {
			     $sql_frm .= "id_specialist=null, ";
			}
			
			if($editPlaydate->getId_parent()!=null) {
			    $sql_frm .= "id_parent='" . $editPlaydate->getId_parent() . "', ";
			} else {
			    $sql_frm .= "id_parent=null, ";
			}
			
			$sql_frm .= "status='" . $editPlaydate->getStatus() . "', " .
			"slug='" . mysql_real_escape_string($editPlaydate->getSlug()) . "', udate=now() " .
			"WHERE id_playdate=" . $editPlaydate->getId_playdate();

			//echo("::::::[$sql_frm]:::::::"); die();
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
		}

		return $retorno;
	}

	/**
	 *
	 * @param Playdate $editPlaydate
	 * @return NULL|array|Playdate
	 */
	public static function updatePlaydateBasics($editPlaydate) {
	    $retorno = null;
	    if (isset ($editPlaydate) && (is_numeric($editPlaydate->getId_playdate()))) {
	        
	        //Updating playdate info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_playdates SET name='" . mysql_real_escape_string($editPlaydate->getName()) . "', " .
	   	        "date='" . BaseDAO::toMysqlDateFormat($editPlaydate->getDate()) . "', " .
	   	        "description='" . mysql_real_escape_string($editPlaydate->getDescription()) . "', ".
	   	        "featured=" . $editPlaydate->getFeatured() . ", ".
	   	        "open_spots=" . $editPlaydate->getOpen_spots() . ", ".
	   	        "num_children=" . $editPlaydate->getNum_children() . " ".
	            "WHERE id_playdate=" . $editPlaydate->getId_playdate();
	        
	        //echo("::::::[$sql_frm]:::::::"); die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
	    }
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param Playdate $editPlaydate
	 * @return NULL|array|Playdate
	 */
	public static function updatePlaydateFull($editPlaydate) {
	    $retorno = null;
	    if (isset ($editPlaydate) && (is_numeric($editPlaydate->getId_playdate()))) {
	        
	        //Updating playdate info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_playdates SET name='" . mysql_real_escape_string($editPlaydate->getName()) . "', " .
	   	        "date='" . BaseDAO::toMysqlDateFormat($editPlaydate->getDate()) . "', " .
	   	        "description='" . mysql_real_escape_string($editPlaydate->getDescription()) . "', ".
	   	        "featured=" . $editPlaydate->getFeatured() . ", ".
	   	        "open_spots=" . $editPlaydate->getOpen_spots() . ", ".
	   	        "num_children=" . $editPlaydate->getNum_children() . " ".
	   	        "WHERE id_playdate=" . $editPlaydate->getId_playdate();
	        
	        //echo("::::::[$sql_frm]:::::::"); die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = PlaydateDAO::getPlaydate($editPlaydate->getId_playdate());
	    }
	    
	    return $retorno;
	}
	
	
	
	public static function getUpdatesListByPlaydate($id_playdate, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_playdate_updates p WHERE p.id_playdate= ".$id_playdate;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.idate ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUpdate = new PlaydateUpdate(-1);
	        $newUpdate->readFromRow($row);
	        $retorno[] = $newUpdate;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function deletePlaydate($deletePlaydate) {
		$retorno = false;

		if (isset ($deletePlaydate) && (is_numeric($deletePlaydate->getId_playdate()))) {
			//Remove specialist info
			$link = getConnection();
			$sql_frm = "DELETE FROM pd_playdates WHERE id_playdate=" . $deletePlaydate->getId_playdate();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = true;
		}

		return $retorno;
	}

	public static function acceptPlaydate($id_playdate) {
	    $retorno = false;
	    if (is_numeric($id_playdate)) {
	        //change status
	        $link = getConnection();
	        $sql_frm = "UPDATE pd_playdates SET status=".Playdate::$STATUS_PUBLISHED.", udate=now() WHERE id_playdate=".$id_playdate;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    //addon - decline all other playdates with the same request
	    $playdate = PlaydateDAO::getPlaydate($id_playdate);
	    if(($playdate!=null) && ($playdate->getId_request()!=null) && is_numeric($playdate->getId_request())) {
    	    $link2 = getConnection();
    	    $sql_frm2 = "UPDATE pd_playdates SET status=".Playdate::$STATUS_DECLINED.", udate=now() WHERE id_request=".$playdate->getId_request()." AND id_playdate<>".$id_playdate;
    	    $result_frm2 = mysql_query($sql_frm2, $link2);
    	    mysql_close($link2);
	    }
	    
	    return $retorno;
	}
	
	public static function declinePlaydate($id_playdate) {
	    $retorno = false;
	    
	    if (is_numeric($id_playdate)) {
	        //change status
	        $link = getConnection();
	        $sql_frm = "UPDATE pd_playdates SET status=".Playdate::$STATUS_DECLINED.", udate=now() WHERE id_playdate=".$id_playdate;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
	public static function addPlaydateToGroup($id_playdate, $id_group) {
	    if(is_numeric($id_playdate) && is_numeric($id_group)) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_group_playdates(id_group, id_playdate, idate) VALUES (".$id_group.", ".$id_playdate.", now())";
	        //echo("::::[$sql]:::::"); die();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	
	public static function addChildToParentReservation($id_reservation, $id_children, $num_children=1, $age="null") {
	    if(is_numeric($id_reservation)) {
	        if($id_children==null) {
	            $id_children ="null";
	        }
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_reservation_children (id_reservation, id_children, num_children, age, idate) VALUES (".$id_reservation.", ".$id_children.", ".$num_children.", ".$age.", now())";
	        //echo("::::[$sql]:::::"); die();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function deleteAllReservationChildren($id_reservation) {
	    if(is_numeric($id_reservation)) {
	        $link1 = getConnection();
	        $sql = "DELETE FROM pd_reservation_children WHERE id_reservation=".$id_reservation;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * 
	 * @param number $id_playdate
	 * @param number $status
	 */
	public static function updatePlaydateStatus($id_playdate, $status) {
	    if(is_numeric($id_playdate)) {
	        $link1 = getConnection();
	        $sql = "UPDATE pd_playdates SET status=".$status.", udate=now() WHERE id_playdate=".$id_playdate;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 *
	 * @param number $id_reservation
	 * @param number $status
	 */
	public static function updateParentReservationStatus($id_reservation, $status) {
	    if(is_numeric($id_reservation)) {
	        $link1 = getConnection();
	        $sql = "UPDATE pd_playdate_parent_reservation SET status=".$status.", udate=now() WHERE id_reservation=".$id_reservation;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	/**
	 *
	 * @param number $id_playdate
	 * @param number $numSupporters
	 */
	public static function updatePlaydateMaxSupporters($id_playdate, $numSupporters) {
	    if(is_numeric($id_playdate)) {
	        $link1 = getConnection();
	        $sql = "UPDATE pd_playdates SET num_supporters=".$numSupporters." WHERE id_playdate=".$id_playdate;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	
	public static function calculateNumSupportNeeded($id_playdate) {
	    $retorno = 0;
	    /**
	     * age < 3   - 2 kids for adult
	     * 3-4 years - 4 kids for adult
	     * 5-7 years - 5 kids for adult
	     * +8 years  - 6 kids for adult 
	     * */
	    
	    $adultsNeeded = 0;
	    
	    //get reservations and kids
	    $reservations = PlaydateDAO::getChildrenReservations($id_playdate);
	    
	    $kids0to2years = 0; //range 1
	    $kids3to4years = 0; //range 2
	    $kids5to7years = 0; //range 3
	    $kids8years = 0;    //range 4

	    $range = 0;  //range is decided by less age kid
	    
	    foreach ($reservations as $auxRes) {
	        if($auxRes->getAge()>=8) { $kids8years+=$auxRes->getNum_children(); if($range==0) {$range=4;}}
	        if(($auxRes->getAge()>=5) && ($auxRes->getAge()<=7) ) { $kids5to7years+=$auxRes->getNum_children(); if($range==0) {$range=3;}}
	        if(($auxRes->getAge()>=3) && ($auxRes->getAge()<=4) ) { $kids3to4years+=$auxRes->getNum_children(); if($range==0) {$range=2;}}
	        if($auxRes->getAge()<3) { $kids0to2years+=$auxRes->getNum_children(); $range=1; }
	    }
	    
	    $totalKids = ($kids0to2years+$kids3to4years+$kids5to7years+$kids8years);
	    
	    //calculate using range (minimum age)
	    if($range==1) {
	        $adultsNeeded =  (ceil($totalKids/2));
	    }
	    
	    if($range==2) {
	        $adultsNeeded =  (ceil($totalKids/4));
	    }
	    
	    if($range==3) {
	        $adultsNeeded =  (ceil($totalKids/5));
	    }
	    
	    if($range==4) {
	        $adultsNeeded =  (ceil($totalKids/6));
	    }
	    
	    //echo("::[RANGE: $range]::[TOTAL KIDS: $totalKids]:::[ADULTS: $adultsNeeded]::");die();
	    
	    $retorno = ($adultsNeeded-1);
	    if($retorno<0) {
	        $retorno=0;
	    }
	    
	    return $retorno;
	}
}
?>
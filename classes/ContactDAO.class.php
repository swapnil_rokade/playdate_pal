<?php
include_once("BaseDAO.class.php");

class ContactDAO extends BaseDAO {

    public function  ContactDAO() {
    }
    
    public static function countContacts() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_contacts";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	public static function countUnreadContacts() {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_contacts WHERE is_read=0";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	public static function countContactsByType($typeContact) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_contacts WHERE type='".$typeContact."'";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	public static function countUnreadContactsByType($typeContact) {
	    $link = getConnection();
	    $sql = "select count(1) FROM pd_contacts WHERE is_read=0 AND type='".$typeContact."'";
	    
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	
	public static function getContactList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="", $isRead=null) {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_contacts";
		
		if($isRead!=null) {
		    $sql.=" WHERE is_read=".$isRead;
		}

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by idate desc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Contact();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	public static function getUnreadContactList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_contacts WHERE is_read=0";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by idate desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Contact();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	public static function getUnreadContactListByType($contactType, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $link = getConnection();
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_contacts WHERE type='".mysql_real_escape_string($contactType)."' AND is_read=0";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by idate desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Contact();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	public static function getReadContactList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_contacts WHERE is_read=1";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by idate desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Contact();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	public static function getContactsByType($type, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $link = getConnection();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_contacts WHERE type='".mysql_real_escape_string($type)."'";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by idate desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Contact();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	
	public static function getContact($id_contact) {
		$retorno = null;
		if(is_numeric($id_contact)) {
			$sql = "SELECT * FROM pd_contacts where id_contact=$id_contact";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Contact();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    

	/**
	 * 
	 * @param Contact $newContact
	 * @return NULL|Contact
	 */
	public static function createContact($newContact) {
		$retorno = null;

		//$newContact = new Contact();
		
		if(isset($newContact) && ($newContact->getName()!="")) {	
		    //Almacenamos contact info 
			$link = getConnection();
			
			$sql_frm = "INSERT INTO pd_contacts (type, name, last_name, email, message, zipcode, experience, company_name, company_web, company_address, availability, req_date, req_time, req_kids, req_age, req_id_parent, idate) " .
			 			"VALUES (".
			 			"'".mysql_real_escape_string($newContact->getType())."', ".
			 			"'".mysql_real_escape_string($newContact->getName())."', ".
			 			"'".mysql_real_escape_string($newContact->getLast_name())."', ".
			 			"'".mysql_real_escape_string($newContact->getEmail())."', ".
			 			"'".mysql_real_escape_string($newContact->getMessage())."', ".
			 			"'".mysql_real_escape_string($newContact->getZipcode())."', ".
			 			"'".mysql_real_escape_string($newContact->getExperience())."', ".
			 			"'".mysql_real_escape_string($newContact->getCompany_name())."', ".
			 			"'".mysql_real_escape_string($newContact->getCompany_web())."', ".
			 			"'".mysql_real_escape_string($newContact->getCompany_address())."', ".
			 			"'".mysql_real_escape_string($newContact->getAvailability())."', ".
			 			"'".mysql_real_escape_string($newContact->getReq_date())."', ".
			 			"'".mysql_real_escape_string($newContact->getReq_time())."', ".
			 			"'".mysql_real_escape_string($newContact->getReq_kids())."', ".
			 			"'".mysql_real_escape_string($newContact->getReq_age())."', ".
			 			"'".mysql_real_escape_string($newContact->getReq_id_parent())."', ".
			 			"now()".
			 			")";
			
			
			//echo(":::::[$sql_frm]::::::"); die();
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			
			//Obtenemos el id de ese contacto			
			$link = getConnection();
			$sql_frm = "SELECT * FROM pd_contacts WHERE name='".mysql_real_escape_string($newContact->getName())."' order by id_contact desc limit 1";
			$result_frm = mysql_query($sql_frm, $link);
			
			$newContact->readFromRow(mysql_fetch_assoc($result_frm));
			
			if(is_numeric($newContact->getId_contact())) {
				$retorno = $newContact;
			}
			mysql_close($link);
		}
		return $retorno;	
	}    

	
	public static function markContactRead($idContact) {
	    $retorno = false;
	    
	    if(is_numeric($idContact)) {
	        //Marcamos como leido
	        $link = getConnection();
	        $sql_frm = "UPDATE pd_contacts SET is_read=1, read_date=now(), udate=now() WHERE id_contact=".$idContact;
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	        
	    }
	    
	    return $retorno;
	}

	public static function markContactUnread($idContact) {
	    $retorno = false;
	    
	    if(is_numeric($idContact)) {
	        //Marcamos como leido
	        $link = getConnection();
	        $sql_frm = "UPDATE pd_contacts SET is_read=0, read_date=null, udate=now() WHERE id_contact=".$idContact;
	        //echo(":::[$sql_frm]:::"); die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	        
	    }
	    
	    return $retorno;
	}
	
	
	public static function deleteContact($idContact) {
		$retorno = false;
		
		if(is_numeric($idContact->getId_contact())) {	
			//Eliminamos contacto
			$link = getConnection();
			$sql_frm = "DELETE FROM pd_contacts WHERE id_contact=".$idContact->getId_contact();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			$retorno = true;
			
		}
		
		return $retorno;	
	}

}
?>

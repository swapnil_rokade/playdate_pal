<?php
include_once("BaseEntity.class.php");

class Expense extends BaseEntity {
	
    protected $id_expense, $id_playdate, $id_parent, $name, $amount_playdate, $amount_addons, $idate;
    
    public function readFromRow($row){
        if(isset($row['id_expense'])) $this->setId_expense($row['id_expense']);
        if(isset($row['id_playdate'])) $this->setId_playdate($row['id_playdate']);
        if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
        if(isset($row['name'])) $this->setName($row['name']);
        if(isset($row['amount_playdate'])) $this->setAmount_playdate($row['amount_playdate']);
        if(isset($row['amount_addons'])) $this->setAmount_addons($row['amount_addons']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
	}
    /**
     * @return mixed
     */
    public function getId_expense()
    {
        return $this->id_expense;
    }

    /**
     * @param mixed $id_expense
     */
    public function setId_expense($id_expense)
    {
        $this->id_expense = $id_expense;
    }

    /**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAmount_playdate()
    {
        return $this->amount_playdate;
    }

    /**
     * @param mixed $amount_playdate
     */
    public function setAmount_playdate($amount_playdate)
    {
        $this->amount_playdate = $amount_playdate;
    }

    /**
     * @return mixed
     */
    public function getAmount_addons()
    {
        return $this->amount_addons;
    }

    /**
     * @param mixed $amount_addons
     */
    public function setAmount_addons($amount_addons)
    {
        $this->amount_addons = $amount_addons;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    
	
	
}
?>
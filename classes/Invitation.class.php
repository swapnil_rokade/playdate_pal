<?php
include_once("BaseEntity.class.php");

class Invitation extends BaseEntity {
	
    protected $id_invitation, $code, $email, $idate, $udate, $status;
    
    public static $STATUS_CREATED=0;
    public static $STATUS_ACCEPTED=1;
    public static $STATUS_DECLINED=2;

    public static $INVITATION_CODE_LENGTH = 8;
   
    /**
    * Constructor
    */ 
    public function __construct( $id_invitation=-1) {
        $this->id_invitation = $id_invitation;
    }
   
    
	public function readFromRow($row){
		if(isset($row['id_invitation'])) $this->setId_invitation($row['id_invitation']);
		if(isset($row['code'])) $this->setCode($row['code']);
		if(isset($row['email'])) $this->setEmail($row['email']);
		if(isset($row['status'])) $this->setStatus($row['status']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_invitation()
    {
        return $this->id_invitation;
    }

    /**
     * @param mixed $id_invitation
     */
    public function setId_invitation($id_invitation)
    {
        $this->id_invitation = $id_invitation;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

	
}
?>
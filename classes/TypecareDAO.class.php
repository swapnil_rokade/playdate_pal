<?php
include_once("BaseDAO.class.php");

class TypecareDAO extends BaseDAO {

    public function  TypecareDAO() {
    }
    
    public static function getTypecareList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_typecares t";

		//Order items
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by t.order asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Typecare();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	
	public static function getTypecare($id_typecare) {
		$retorno = null;
		if(is_numeric($id_typecare)) {
			$sql = "SELECT * FROM pd_typecares where id_typecare=$id_typecare";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Typecare();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    

	/*
	public static function createCategoria($newCategoria) {
		$retorno = null;

		if(isset($newCategoria) && ($newCategoria->getNombre()!="")) {	
			//Almacenamos la información de categoria
			$link = getConnection();
			$sql_frm = "INSERT INTO fm_categorias (id_categoria_padre, nombre, descripcion, estado, orden, slug, ifecha) " .
				"values (".(is_numeric($newCategoria->getId_categoria_padre())?$newCategoria->getId_categoria_padre():"null").",'".$newCategoria->getNombre()."','".$newCategoria->getDescripcion()."','".$newCategoria->getEstado()."', ".$newCategoria->getOrden().", '".$newCategoria->getSlug()."', now())";
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			
			//Obtenemos el id de esa categoria			
			$link = getConnection();
			$sql_frm = "SELECT * FROM fm_categorias WHERE nombre='".$newCategoria->getNombre()."' order by id_categoria desc limit 1";
			$result_frm = mysql_query($sql_frm, $link);
			
			$newCategoria->readFromRow(mysql_fetch_assoc($result_frm));
			
			if(is_numeric($newCategoria->getId_categoria())) {
				$retorno = $newCategoria;
			}
			mysql_close($link);
		}
		return $retorno;	
	}    

	public static function updateCategoria($editCategoria) {
		$retorno = null;
		if(isset($editCategoria) && ($editCategoria->getNombre()!="") && (is_numeric($editCategoria->getId_categoria()))) {	
			//Almacenamos la información de categoria
			$link = getConnection();
			$sql_frm = "UPDATE fm_categorias SET id_categoria_padre=".$editCategoria->getId_categoria_padre().", " .
						 "nombre='".$editCategoria->getNombre()."', " .
						 "descripcion='".$editCategoria->getDescripcion()."', " .
						 "estado='".$editCategoria->getEstado()."', " .
						 "orden='".$editCategoria->getOrden()."', " .
						 "slug='".$editCategoria->getSlug()."', " .
						 "ufecha=now() " .
						"WHERE id_categoria=".$editCategoria->getId_categoria();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			
			$retorno = CategoriaDAO::getCategoria($editCategoria->getId_categoria());
		}
		
		return $retorno;	
	}    

	public static function deleteCategoria($deleteCategoria) {
		$retorno = false;

		if(isset($deleteCategoria) && (is_numeric($deleteCategoria->getId_categoria()))) {	
			//Eliminamos la información de categoria
			$link = getConnection();
			$sql_frm = "DELETE FROM fm_categorias WHERE id_categoria=".$deleteCategoria->getId_categoria();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			
			$retorno = true;
			
		}
		
		return $retorno;	
	}
	*/

}
?>
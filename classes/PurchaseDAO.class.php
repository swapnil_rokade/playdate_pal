<?php
include_once("BaseDAO.class.php");

class PurchaseDAO extends BaseDAO {

    public function  PurchaseDAO() {
    }
    
    
	public static function getPurchaseListByParent($id_parent, $date_init=null, $date_end=null) {
	    $retorno = array();
	    
	    $sql = "SELECT * FROM pd_purchases where id_parent=".$id_parent;
	    
	    if($date_init!=null) {
	        $sql.=" AND idate>='".BaseDAO::toMysqlDateFormat($date_init)."'";
	    }
	    if($date_end!=null) {
	        $sql.=" AND idate<='".BaseDAO::toMysqlDateFormat($date_end)."'";
	    }
	    
        $sql .= " order by id_purchase desc";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Purchase();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 * @param Purchase $newPurchase
	 */
	public static function createPurchase($newPurchase) {
	    if(is_numeric($newPurchase->getId_parent()) && is_numeric($newPurchase->getAmount())) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_purchases (id_parent, amount, description, idate) VALUES (".$newPurchase->getId_parent().", ".$newPurchase->getAmount().", '".mysql_real_escape_string($newPurchase->getDescription())."', now())";
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
}
?>
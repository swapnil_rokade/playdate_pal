<?php
class Utils {
	
    function Utils() {
    }
    
    public static function get12hourFormat($hh24) {
        $retorno = $hh24." am";
        
        if($hh24==0) {
            $retorno = "0 am";
        } else if($hh24==12 ) {
            $retorno = "12 pm";
        } else if($hh24==24){
            $retorno = "12 am";
        }
        else if($hh24>12 && $hh24<24){
            $retorno = ($hh24%12)." pm";
            if($has_dots = strpos($hh24, ':') !== false) {
                $retorno = ($hh24%12).":30 pm";
            } else {
                $retorno = ($hh24%12).":00 pm";
            }
        }
        return $retorno;        
    }
    
    public static function get12hourFormatInput($hh24) {
        $retorno = $hh24;
        
        if(($hh24==0)||($hh24==24)) {
            if($has_dots = strpos($hh24, ':') !== false) {
                $retorno = "12:30AM";
            } else {
                $retorno = "12:00AM";
            }
        } else if($hh24==12) {
            if($has_dots = strpos($hh24, ':') !== false) {
                $retorno = "12:30PM";
            } else { 
                $retorno = "12:00PM";
            }
        } else if($hh24>12 ){
            if($has_dots = strpos($hh24, ':') !== false) {
                $retorno = ($hh24%12).":30PM";
            } else {
                $retorno = ($hh24%12).":00PM";
            }
        } else if($hh24<12 ){
            if($has_dots = strpos($hh24, ':') !== false) {
                $retorno = ($hh24%12).":30AM";
            } else {
                $retorno = ($hh24%12).":00AM";
            }
        }
        return $retorno;
    }
    
    public static function moneyFormat($moneyString, $decimals=2) {
        $retorno = $moneyString;
        $retorno = number_format($moneyString/100, $decimals, ".", ",");
        return $retorno;
    }
    
    public static function moneyFormatField($moneyString) {
        $retorno = $moneyString;
        $retorno = number_format($moneyString/100, 2, ".", "");
        return $retorno;
    }
    
    /*
    public static function dateFormat($datestring) {
        $retorno = $datestring;
        $auxdate = date_create(BaseDAO::toMysqlDateFormat($datestring));
        $retorno = date_format($auxdate, 'M jS, Y');
        return $retorno;
    }
    */
    
    public static function dateFormat($datestring) {
        $retorno = $datestring;
        $var1 = BaseDAO::toMysqlDateFormat($datestring);
        $auxdate = date_create($var1, timezone_open("America/New_York"));
        $retorno = date_format($auxdate, 'M jS, Y');
        return $retorno;
    }
    
    public static function dateTimeFormat($datestring) {
        $retorno = $datestring;
        $auxdate = date_create(BaseDAO::toMysqlDateFormat($datestring));
        $retorno = date_format($auxdate, 'M jS, Y'). " - ". Utils::timeFormatShort($datestring);
        return $retorno;
    }
    
    
    public static function dateFormatShort($datestring) {
        $retorno = $datestring;
        $auxdate = date_create(BaseDAO::toMysqlDateFormat($datestring));
        $retorno = date_format($auxdate, 'M j');
        return $retorno;
    }
    
    public static function timeFormatShort($datestring) {
        $retorno = $datestring;
        $auxdate = date_create(BaseDAO::toMysqlDateFormat($datestring));
        $retorno = date_format($auxdate, 'h:ia');
        return $retorno;
    }
    
    
    public static function getLimitedText($sourceText, $charlimit=9999) {
		$retorno = $sourceText;
		if($charlimit && (strlen($retorno)>$charlimit)) {
			$retorno=substr($retorno,0,($charlimit-3))."...";
		}
		return $retorno;    	
    }
    
	public static function toStringDateFormat($fecha){
		$txtfecha = $fecha;
		
		if($fecha!="") {
			if(strpos($fecha,"/")==false) {
				if(strpos($fecha,":")==false) {
				    /*
					ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);
				    $txtfecha=$mifecha[3]."/".$mifecha[2]."/".$mifecha[1];
				    */
				    
				    $txtfecha = preg_replace('#(\d{4})-(\d{2})-(\d{2})#', '$3/$2/$1', $fecha);
				    
				} else {
					/*
					ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})", $fecha, $mifecha);
				    $txtfecha=$mifecha[3]."/".$mifecha[2]."/".$mifecha[1].", ".$mifecha[4].":".$mifecha[5].":".$mifecha[6];
				    */
				    $txtfecha = preg_replace('#(\d{4})-(\d{2})-(\d{2}) (\d{1,2}):(\d{1,2}):(\d{1,2})#', '$3/$2/$1, $4:$5:$6', $fecha);
				    
				}
			}
		} 
	
	    return $txtfecha;
	}
		
	public static function toTextDateFormat($fecha){
		if($fecha!="") {
			$meses=array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
			
    		if(strpos($fecha,":")==false) {
			    preg_match("#([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})#", $fecha, $mifecha);
				$txtfecha=$mifecha[1]. " de ".$meses[(-1+($mifecha[2]))]." de ".$mifecha[3];			
			} else {
			    preg_match( "#([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4}), ([0-9,:]{5,8})#", $fecha, $mifecha);
				$txtfecha=$mifecha[1]. " de ".$meses[(-1+($mifecha[2]))]." de ".$mifecha[3];			
			}
			
			//$txtfecha = preg_replace('#(\d{2})/(\d{2})/(\d{4}), (\d{1,2}):(\d{1,2}):(\d{1,2})#', '$1 de $2 de $3', $fecha);
		}
	    return $txtfecha;
	}
  
	public static function truncateString($phrase, $maxlong = 150) {
	    $phrase = substr(trim($phrase), 0, $maxlong);
	    $pos = strrpos($phrase, " ");
	    $phrase = substr($phrase, 0, $pos);
	    if ((substr($phrase, -1, 1) == ",") or (substr($phrase, -1, 1) == ".")) {
	        $phrase = substr($phrase, 0, -1);
	    }
	    if ($pos === false) {
	        $phrase = $phrase;
	    } else {
	        $phrase = $phrase . "...";
	    }
	    return $phrase;
	}
	
	/**
	 * Calculate User Friendly URL. Can add an extension as param
	 * @param mixed $string
	 * @param mixed $ext
	 * @return string
	 */
	public static function create_slug($string, $ext=null){
	    $replace = '-';
	    $string = strtolower($string);
	    
	    //replace / and . with white space
	    $string = preg_replace("/[\/\.]/", " ", $string);
	    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	    
	    //remove multiple dashes or whitespaces
	    $string = preg_replace("/[\s-]+/", " ", $string);
	    
	    //convert whitespaces and underscore to $replace
	    $string = preg_replace("/[\s_]/", $replace, $string);
	    
	    //limit the slug size
	    $string = substr($string, 0, 100);
	    
	    //slug is generated
	    return ($ext) ? $string.$ext : $string;
	}
	
	public static function generateNewPassword() {
	    return substr(str_shuffle("0123456789abcdefghjklmnopqrstuvwxyz"), 0, 6);
	}
	
}
?>
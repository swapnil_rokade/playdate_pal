<?php
/**
 * Superclase de los objetos de acceso a datos del portal
 */
class Config {

    //CONFIG ZONE LOCAL - CAN BE CHANGED IN CREATOR
    //protected $documentRoot = "/PROYECTOS/PLAYDATES/workspace/Playdate/www";
    protected $documentRoot = "../";
    protected $mailBasedomain = "https://playdate.airtouchmedia.com";
    protected $mailFromname =   "PAL by Project Playdate";
    protected $mailHost =       "playdate.airtouchmedia.com";
    protected $mailUsername=    "noreply@playdate.airtouchmedia.com";
    protected $mailPassword =   "Play!4132";
    protected $mailPort = 25;
    protected $mailSecure = false;
    
    
	function Config() {
	    $serverName = null;
	    if(isset($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME']!=null)) {
	        $serverName = $_SERVER['SERVER_NAME'];
	    }
	   // echo $serverName; exit;
	    
	    if($serverName=="playdate.airtouchmedia.com") { //TEST
	        $this->setDocumentRoot("/var/www/vhosts/playdate.airtouchmedia.com/httpdocs/");
	        $this->setMailBasedomain("https://playdate.airtouchmedia.com");
	        $this->setMailFromname("PAL by Project Playdate");
	        $this->setMailHost("playdate.airtouchmedia.com");
	        $this->setMailUsername("noreply@playdate.airtouchmedia.com");
	        $this->setMailPassword("Play!4132");
	        $this->setMailPort(25);
	        $this->setMailSecure(false);
	    }
	    
	    if(($serverName=="playdatepal.com")||($serverName=="www.playdatepal.com")) { //PROD
	        //$this->setDocumentRoot("/var/www/vhosts/playdate.airtouchmedia.com/httpdocs/");
	        $this->setDocumentRoot($_SERVER['DOCUMENT_ROOT']);
	        $this->setMailBasedomain("https://www.playdatepal.com");
	        $this->setMailFromname("PAL by Project Playdate");
	        $this->setMailHost("playdatepal.com");
	        $this->setMailUsername("noreply@playdatepal.com");
	        $this->setMailPassword("Play!4132");
	        $this->setMailPort(25);
	        $this->setMailSecure(false);
        }
        
        if($serverName=="ht.3dchow.com") { //Devel Server
	        $this->setDocumentRoot($_SERVER['DOCUMENT_ROOT']);
	        $this->setMailBasedomain("http://ht.3dchow.com");
	        $this->setMailFromname("PAL by Project Playdate");
	        $this->setMailHost("smtp.gmail.com");
	        $this->setMailUsername("pagrawal@webcubator.co");
            $this->setMailPassword("9730570284p");
            // $this->setMailUsername("noreply@playdatepal.com");
	        // $this->setMailPassword("Play!4132");
	        $this->setMailPort(587);
	        $this->setMailSecure(false);
	    }
	    if($serverName=="playdate.wwwaz1-ls10.a2hosted.com") { //Devel Server
	        $this->setDocumentRoot($_SERVER['DOCUMENT_ROOT']);
	        $this->setMailBasedomain("http://playdate.wwwaz1-ls10.a2hosted.com/");
	        $this->setMailFromname("PAL by Project Playdate");
	        $this->setMailHost("smtp.gmail.com");
	        $this->setMailUsername("pagrawal@webcubator.co");
            $this->setMailPassword("9730570284p");
            // $this->setMailUsername("noreply@playdatepal.com");
	        // $this->setMailPassword("Play!4132");
	        $this->setMailPort(587);
	        $this->setMailSecure(false);
	    }
    }
    /**
     * @return string
     */
    public function getDocumentRoot()
    {
        return $this->documentRoot;
    }

    /**
     * @param string $documentRoot
     */
    public function setDocumentRoot($documentRoot)
    {
        $this->documentRoot = $documentRoot;
    }

    /**
     * @return string
     */
    public function getMailBasedomain()
    {
        return $this->mailBasedomain;
    }

    /**
     * @param string $mailBasedomain
     */
    public function setMailBasedomain($mailBasedomain)
    {
        $this->mailBasedomain = $mailBasedomain;
    }

    /**
     * @return string
     */
    public function getMailFromname()
    {
        return $this->mailFromname;
    }

    /**
     * @param string $mailFromname
     */
    public function setMailFromname($mailFromname)
    {
        $this->mailFromname = $mailFromname;
    }

    /**
     * @return string
     */
    public function getMailHost()
    {
        return $this->mailHost;
    }

    /**
     * @param string $mailHost
     */
    public function setMailHost($mailHost)
    {
        $this->mailHost = $mailHost;
    }

    /**
     * @return string
     */
    public function getMailUsername()
    {
        return $this->mailUsername;
    }

    /**
     * @param string $mailUsername
     */
    public function setMailUsername($mailUsername)
    {
        $this->mailUsername = $mailUsername;
    }

    /**
     * @return string
     */
    public function getMailPassword()
    {
        return $this->mailPassword;
    }

    /**
     * @param string $mailPassword
     */
    public function setMailPassword($mailPassword)
    {
        $this->mailPassword = $mailPassword;
    }
    /**
     * @return number
     */
    public function getMailPort()
    {
        return $this->mailPort;
    }

    /**
     * @param number $mailPort
     */
    public function setMailPort($mailPort)
    {
        $this->mailPort = $mailPort;
    }

    /**
     * @return boolean
     */
    public function getMailSecure()
    {
        return $this->mailSecure;
    }

    /**
     * @param boolean $mailSecure
     */
    public function setMailSecure($mailSecure)
    {
        $this->mailSecure = $mailSecure;
    }

}
?>
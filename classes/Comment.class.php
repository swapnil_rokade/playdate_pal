<?php
include_once("BaseEntity.class.php");

class Comment extends BaseEntity {
	
    protected $id_comment, $id_parent, $id_playdate, $id_specialist, $comment, $idate, $udate;
    
    /**
    * Constructor
    */ 
    public function __construct( $id_comment=-1) {
        $this->id_comment = $id_comment;
    }
   
    
	public function readFromRow($row){
	    if(isset($row['id_comment'])) $this->setId_comment($row['id_comment']);
	    if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
	    if(isset($row['id_specialist'])) $this->setId_specialist($row['id_specialist']);
	    if(isset($row['id_playdate'])) $this->setId_playdate($row['id_playdate']);
	    if(isset($row['comment'])) $this->setComment($row['comment']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_comment()
    {
        return $this->id_comment;
    }

    /**
     * @param mixed $id_comment
     */
    public function setId_comment($id_comment)
    {
        $this->id_comment = $id_comment;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * 
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param mixed $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    
    /**
     * 
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }
    
    /**
     * @param  mixed $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getId_specialist()
    {
        return $this->id_specialist;
    }

    /**
     * @param mixed $id_specialist
     */
    public function setId_specialist($id_specialist)
    {
        $this->id_specialist = $id_specialist;
    }
    /**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }
	
}
?>
<?php
include_once("BaseEntity.class.php");

class ParentPd extends BaseEntity {
    
    public static $STATUS_INACTIVE=0;
    public static $STATUS_ACTIVE=1;
    
    public static $PROFILE_STANDARD=0;
    public static $PROFILE_EXPERIENCED=1;
    
    
    protected $id_parent, $name, $lastname, $username, $email, $zipcode, $password, $cellphone, $phone;
    
    protected $children_avb_mon_init, $children_avb_mon_end, $children_avb_tue_init, $children_avb_tue_end; 
    protected $children_avb_wed_init, $children_avb_wed_end, $children_avb_thu_init, $children_avb_thu_end; 
    protected $children_avb_fri_init, $children_avb_fri_end, $children_avb_sat_init, $children_avb_sat_end; 
    protected $children_avb_sun_init, $children_avb_sun_end; 
    protected $picture, $idate, $udate, $status, $slug;
    
    protected $credit;
    
    protected $profile=0;
    
   
    /**
     * @return mixed
     */
    public function getChildren_avb_mon_init() 
    {
        return $this->children_avb_mon_init;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_mon_end()
    {
        return $this->children_avb_mon_end;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_tue_init()
    {
        return $this->children_avb_tue_init;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_tue_end()
    {
        return $this->children_avb_tue_end;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_wed_init()
    {
        return $this->children_avb_wed_init;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_wed_end()
    {
        return $this->children_avb_wed_end;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_thu_init()
    {
        return $this->children_avb_thu_init;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_thu_end()
    {
        return $this->children_avb_thu_end;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_fri_init()
    {
        return $this->children_avb_fri_init;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_fri_end()
    {
        return $this->children_avb_fri_end;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_sat_init()
    {
        return $this->children_avb_sat_init;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_sat_end()
    {
        return $this->children_avb_sat_end;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_sun_init()
    {
        return $this->children_avb_sun_init;
    }

    /**
     * @return mixed
     */
    public function getChildren_avb_sun_end()
    {
        return $this->children_avb_sun_end;
    }

    /**
     * @param mixed $children_avb_mon_init
     */
    public function setChildren_avb_mon_init($children_avb_mon_init)
    {
        $this->children_avb_mon_init = $children_avb_mon_init;
    }

    /**
     * @param mixed $children_avb_mon_end
     */
    public function setChildren_avb_mon_end($children_avb_mon_end)
    {
        $this->children_avb_mon_end = $children_avb_mon_end;
    }

    /**
     * @param mixed $children_avb_tue_init
     */
    public function setChildren_avb_tue_init($children_avb_tue_init)
    {
        $this->children_avb_tue_init = $children_avb_tue_init;
    }

    /**
     * @param mixed $children_avb_tue_end
     */
    public function setChildren_avb_tue_end($children_avb_tue_end)
    {
        $this->children_avb_tue_end = $children_avb_tue_end;
    }

    /**
     * @param mixed $children_avb_wed_init
     */
    public function setChildren_avb_wed_init($children_avb_wed_init)
    {
        $this->children_avb_wed_init = $children_avb_wed_init;
    }

    /**
     * @param mixed $children_avb_wed_end
     */
    public function setChildren_avb_wed_end($children_avb_wed_end)
    {
        $this->children_avb_wed_end = $children_avb_wed_end;
    }

    /**
     * @param mixed $children_avb_thu_init
     */
    public function setChildren_avb_thu_init($children_avb_thu_init)
    {
        $this->children_avb_thu_init = $children_avb_thu_init;
    }

    /**
     * @param mixed $children_avb_thu_end
     */
    public function setChildren_avb_thu_end($children_avb_thu_end)
    {
        $this->children_avb_thu_end = $children_avb_thu_end;
    }

    /**
     * @param mixed $children_avb_fri_init
     */
    public function setChildren_avb_fri_init($children_avb_fri_init)
    {
        $this->children_avb_fri_init = $children_avb_fri_init;
    }

    /**
     * @param mixed $children_avb_fri_end
     */
    public function setChildren_avb_fri_end($children_avb_fri_end)
    {
        $this->children_avb_fri_end = $children_avb_fri_end;
    }

    /**
     * @param mixed $children_avb_sat_init
     */
    public function setChildren_avb_sat_init($children_avb_sat_init)
    {
        $this->children_avb_sat_init = $children_avb_sat_init;
    }

    /**
     * @param mixed $children_avb_sat_end
     */
    public function setChildren_avb_sat_end($children_avb_sat_end)
    {
        $this->children_avb_sat_end = $children_avb_sat_end;
    }

    /**
     * @param mixed $children_avb_sun_init
     */
    public function setChildren_avb_sun_init($children_avb_sun_init)
    {
        $this->children_avb_sun_init = $children_avb_sun_init;
    }

    /**
     * @param mixed $children_avb_sun_end
     */
    public function setChildren_avb_sun_end($children_avb_sun_end)
    {
        $this->children_avb_sun_end = $children_avb_sun_end;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $cellphone
     */
    public function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
    * Constructor
    */ 
    public function __construct( $id_parent=-1) {
        $this->id_parent = $id_parent;
    }
   
	public function getId_parent() {
		return $this->id_parent;
	}
	public function setId_parent($newId_parent) {
		$this->id_parent = $newId_parent;
	}
	
   
	
	/**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getMD5() {
		return md5($this->getId_parent().$this->getEmail().$this->getName().$this->getLastname());
	}

	public function readFromRow($row){
	    if(isset($row['id_parent'])) { $this->setId_parent($row['id_parent']);}
	    if(isset($row['name'])) { $this->setName($row['name']);}
	    if(isset($row['lastname'])) { $this->setLastname($row['lastname']);}
	    if(isset($row['username'])) { $this->setUsername($row['username']);}
	    if(isset($row['email'])) { $this->setEmail($row['email']);}
	    if(isset($row['zipcode'])) { $this->setZipcode($row['zipcode']);}
	    if(isset($row['password'])) { $this->setPassword($row['password']);}
	    if(isset($row['cellphone'])) { $this->setCellphone($row['cellphone']);}
	    if(isset($row['phone'])) { $this->setPhone($row['phone']);}
	    
	    if(isset($row['children_avb_mon_init'])) { $this->setChildren_avb_mon_init($row['children_avb_mon_init']);}
	    if(isset($row['children_avb_mon_end'])) { $this->setChildren_avb_mon_end($row['children_avb_mon_end']);}
	    
	    if(isset($row['children_avb_tue_init'])) { $this->setChildren_avb_tue_init($row['children_avb_tue_init']);}
	    if(isset($row['children_avb_tue_end'])) { $this->setChildren_avb_tue_end($row['children_avb_tue_end']);}
	    
	    if(isset($row['children_avb_wed_init'])) { $this->setChildren_avb_wed_init($row['children_avb_wed_init']);}
	    if(isset($row['children_avb_wed_end'])) { $this->setChildren_avb_wed_end($row['children_avb_wed_end']);}
	    
	    if(isset($row['children_avb_thu_init'])) { $this->setChildren_avb_thu_init($row['children_avb_thu_init']);}
	    if(isset($row['children_avb_thu_end'])) { $this->setChildren_avb_thu_end($row['children_avb_thu_end']);}
	    
	    if(isset($row['children_avb_fri_init'])) { $this->setChildren_avb_fri_init($row['children_avb_fri_init']);}
	    if(isset($row['children_avb_fri_end'])) { $this->setChildren_avb_fri_end($row['children_avb_fri_end']);}
	    
	    if(isset($row['children_avb_sat_init'])) { $this->setChildren_avb_sat_init($row['children_avb_sat_init']);}
	    if(isset($row['children_avb_sat_end'])) { $this->setChildren_avb_sat_end($row['children_avb_sat_end']);}
	    
	    if(isset($row['children_avb_sun_init'])) { $this->setChildren_avb_sun_init($row['children_avb_sun_init']);}
	    if(isset($row['children_avb_sun_end'])) { $this->setChildren_avb_sun_end($row['children_avb_sun_end']);}
	    
	    
	    if(isset($row['picture'])) { $this->setPicture($row['picture']);}
	    if(isset($row['slug'])) { $this->setSlug($row['slug']);}
	    
	    if(isset($row['status'])) { $this->setStatus($row['status']);}
	    
	    if(isset($row['credit'])) { $this->setCredit($row['credit']);}
	    
	    if(isset($row['profile'])) { $this->setProfile($row['profile']);}
	    
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
	/**
	 * Read full parent  name
	 */
	public function getFullName() {
	    $fullname = $this->getName();
	    
	    if($this->getLastname()!="") {
	        $fullname.= " ".$this->getLastname();
	    }
	    
	    return $fullname;
	    
	}
    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }
    /**
     * @return mixed
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param mixed $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }
    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }



	
} 
?>
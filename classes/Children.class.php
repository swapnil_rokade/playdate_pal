<?php
include_once("BaseEntity.class.php");

class Children extends BaseEntity {
	
    protected $id_children, $id_parent, $name, $age, $birthdate, $notes, $genre, $idate, $udate;
    
    /**
    * Constructor
    */ 
    public function __construct( $id_children=-1) {
        $this->id_children = $id_children;
    }
   
    
    public function readFromRow($row, $prefixChild=""){
        //echo("LEYENDO CON PREFIJO [$prefixChild] NOS DA ID [".$row[$prefixChild.'id_children']."]");
	    if(isset($row[($prefixChild.'id_children')])) {$this->setId_children($row[$prefixChild.'id_children']); };
	    if(isset($row[$prefixChild.'id_parent'])) $this->setId_parent($row[$prefixChild.'id_parent']);
	    if(isset($row[$prefixChild.'name'])) $this->setName($row[$prefixChild.'name']);
	    if(isset($row[$prefixChild.'age'])) $this->setAge($row[$prefixChild.'age']);
	    if(isset($row[$prefixChild.'birthdate'])) $this->setBirthdate($row[$prefixChild.'birthdate']);
	    if(isset($row[$prefixChild.'notes'])) $this->setNotes($row[$prefixChild.'notes']);
	    if(isset($row[$prefixChild.'genre'])) $this->setGenre($row[$prefixChild.'genre']);
	    if(isset($row[$prefixChild.'idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row[$prefixChild.'idate']));
	    if(isset($row[$prefixChild.'udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row[$prefixChild.'udate']));
	}
    /**
     * @return mixed
     */
    public function getId_children()
    {
        return $this->id_children;
    }

    /**
     * @param mixed $id_children
     */
    public function setId_children($id_children)
    {
        $this->id_children = $id_children;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param mixed $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }


	
    	
}
?>
<?php
include_once ("BaseDAO.class.php");

class SpecialistDAO extends BaseDAO {

    function SpecialistDAO() {
	}

	public static function countSpecialists() {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_specialists", $link) or die("Error counting Specialists");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function countSpecialistsSearch($query) {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_specialists WHERE name like '%$query%' or lastname like '%$query%' or about_me like '%$query%' or email like '%$query%'", $link) or die("Error counting specialists");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function getSpecialistsList($page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT * FROM pd_specialists ";
		//Ordenacion
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
		    $sql .= " order by RAND()";
		    
		}

		//Start and range
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first
		$max = $elemsPerPage; # elements per page

		//paging
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
			$newUser = new Specialist(-1);
			$newUser->readFromRow($row);
			$retorno[] = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}

	public static function getSpecialistsLeadList($page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_specialists WHERE profile=".Specialist::$PROFILE_LEAD;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by RAND()";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getSupportersList($page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_specialists WHERE profile=".Specialist::$PROFILE_SUPPORTER;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by RAND()";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getSpecialistListByGroupMembers($id_group, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_specialists p, pd_group_specialists r WHERE p.id_specialist=r.id_specialist AND r.id_group= ".$id_group;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.name ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getSpecialistListByGroupAdmin($id_group, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_specialists p, pd_group_specialists r WHERE r.is_admin=1 AND p.id_specialist=r.id_specialist AND r.id_group= ".$id_group;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.name ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getSpecialistsListByParent($id_parent, $page=1, $elemsPerPage=10, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT DISTINCT s.* FROM pd_specialists s, pd_playdates p, pd_playdate_parent_reservation r WHERE s.id_specialist=p.id_specialist AND p.id_playdate=r.id_playdate AND r.id_parent=".$id_parent;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by RAND()";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getSpecialistsListByGroup($id_group, $page=1, $elemsPerPage=10, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT DISTINCT s.* FROM pd_specialists s, pd_playdates p, pd_group_playdates g WHERE s.id_specialist=p.id_specialist AND p.id_playdate=g.id_playdate AND g.id_group=".$id_group;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by RAND()";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	/**
	 * 
	 * @param number $id_request
	 * @param number $page
	 * @param number $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return Specialist[]
	 */
	public static function getSpecialistsListByRequest($id_request, $page=1, $elemsPerPage=10, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT DISTINCT s.* FROM pd_specialists s, pd_request_specialist r WHERE s.id_specialist=r.id_specialist AND r.id_request=".$id_request;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getSpecialistsSearch($userSearch, $page, $elemsPerPage, $sortfield, $sorttype) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_specialists s WHERE 1=1";
		
		//Adding query parameters
		/*
		$userSearch = new Specialist();
		
		if(($userSearch->getPerfil()!=null)&&($userSearch->getPerfil()!="")) {
			$sql.=" AND perfil='".$userSearch->getPerfil()."'";
		}
		*/
		
		//order
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		}

		//paging values
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first element
		$max = $elemsPerPage; # num elements

		//paging limits
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
			$newUser = new Specialist(-1);
			$newUser->readFromRow($row);
			$retorno[] = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}
	

	public static function getSpecialist($id_specialist) {
		$retorno = array ();
		
		if(is_numeric($id_specialist) && ($id_specialist>=0)) {
			$link = getConnection();
	
			$sql = "SELECT s.* FROM pd_specialists s WHERE id_specialist=$id_specialist";
	
			//reading results
			$result = mysql_query($sql, $link);
			if ($row = mysql_fetch_assoc($result)) {
				$newUser = new Specialist(-1);
				$newUser->readFromRow($row);
				$retorno = $newUser;
			}
			mysql_close($link);
		}
		return $retorno;
	}

	/*
	public static function getUsuarioByActivation($activateKey) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM fm_usuarios s WHERE activacion='$activateKey'";

		//Obtenemos los resultados
		$result = mysql_query($sql, $link);
		if ($row = mysql_fetch_assoc($result)) {
			$newUser = new Usuario(-1);
			$newUser->readFromRow($row);
			$retorno = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}
    */
	
	public static function getSpecialistByEmailPassword($email, $password) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_specialists s WHERE email='".mysql_real_escape_string($email)."' and password=md5('".mysql_real_escape_string($password)."')";
        
		//reading results
		$result = mysql_query($sql, $link);
		if ($row = mysql_fetch_assoc($result)) {
			$newUser = new Specialist(-1);
			$newUser->readFromRow($row);
			$retorno = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}
	
	public static function getSpecialistByUsernamePassword($username, $password) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT s.* FROM pd_specialists s WHERE username='".mysql_real_escape_string($username)."' and password=md5('".mysql_real_escape_string($password)."')";
	    
	    //reading results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	
	public static function getSpecialistByEmail($email) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_specialists s WHERE email='$email'";

		//reading results
		$result = mysql_query($sql, $link);
		if ($row = mysql_fetch_assoc($result)) {
			$newUser = new Specialist(-1);
			$newUser->readFromRow($row);
			$retorno = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}

	public static function getSpecialistByUsernameOrEmail($usernameoremail) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT s.* FROM pd_specialists s WHERE lower(email)='".strtolower($usernameoremail)."' or lower(username)='".strtolower($usernameoremail)."'";
	    
	    //reading results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newUser = new Specialist(-1);
	        $newUser->readFromRow($row);
	        $retorno = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	/**
	 * Create Specialist
	 * @param Specialist $newSpecialist
	 * @return mixed
	 */
	public static function createSpecialist($newSpecialist) {
	    $retorno = null;
	    
	    if (($newSpecialist!=null) && ($newSpecialist->getEmail() != "")) {
	        //Save specialist basic info
	        $link = getConnection();
	        
	        $sql_frm = "INSERT INTO pd_specialists (profile, name, lastname, username, email, password, us_citizen, about_me, cellphone, phone, idate) " .
	   	        "VALUES (".
	   	        "'".$newSpecialist->getProfile()."', ".
	   	        "'".mysql_real_escape_string($newSpecialist->getName())."', ".
	   	        "'".mysql_real_escape_string($newSpecialist->getLastname())."', ".
	   	        "'".mysql_real_escape_string($newSpecialist->getUsername())."', ".
	   	        "'".mysql_real_escape_string($newSpecialist->getEmail())."', ".
	   	        "md5('".mysql_real_escape_string($newSpecialist->getPassword())."'), ".
	   	        (($newSpecialist->getUs_citizen()!=null)?$newSpecialist->getUs_citizen():"null").", " .
	   	        (($newSpecialist->getAbout_me()!=null)?("'".mysql_real_escape_string($newSpecialist->getAbout_me())."'"):"null").", " .
	   	        (($newSpecialist->getCellphone()!=null)?("'".$newSpecialist->getCellphone()."'"):"null").", " .
	   	        (($newSpecialist->getPhone()!=null)?("'".$newSpecialist->getPhone()."'"):"null").", " .
	   	        "now()".
	   	        ")";
	        
	   	   //echo(":::::[$sql_frm]::::::"); die();
	   	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        //Obtenemos el id del nuevo specialist
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM pd_specialists WHERE email='" . mysql_real_escape_string($newSpecialist->getEmail()) . "' order by id_specialist desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        $newSpecialist->readFromRow(mysql_fetch_assoc($result_frm));
	        
	        if (is_numeric($newSpecialist->getId_specialist()) && ($newSpecialist->getId_specialist()>0)) {
	            $retorno = $newSpecialist;
	        }
	        mysql_close($link);
	    }
	    return $retorno;
	}
	
	
	

	/**
	 * Update Specialist info
	 * @param Specialist $editSpecialist
	 * @return NULL|Specialist
	 */
	public static function updateSpecialist($editSpecialist) {
		$retorno = null;
		if (isset ($editSpecialist) && (is_numeric($editSpecialist->getId_specialist()))) {
			
			//Updating specialist info
			$link = getConnection();
			
			$sql_frm = "UPDATE pd_specialists SET name='" . $editSpecialist->getName() . "', " .
			"lastname='" . mysql_real_escape_string($editSpecialist->getLastname()) . "', " .
			"username='" . mysql_real_escape_string($editSpecialist->getUsername()) . "', " .
			"email='" . mysql_real_escape_string($editSpecialist->getEmail()) . "', " .
			"cellphone='" . mysql_real_escape_string($editSpecialist->getCellphone()) . "', " .
			"phone='" . mysql_real_escape_string($editSpecialist->getPhone()) . "', " .
			"profile='" . mysql_real_escape_string($editSpecialist->getProfile()) . "', " .
			"exp_range1='" . mysql_real_escape_string($editSpecialist->getExp_range1()) . "', " .
			"exp_range2='" . mysql_real_escape_string($editSpecialist->getExp_range2()) . "', " .
			"exp_range3='" . mysql_real_escape_string($editSpecialist->getExp_range3()) . "', " .
			"about_me='" . mysql_real_escape_string($editSpecialist->getAbout_me()) . "', " .
			"additional_1='" . mysql_real_escape_string($editSpecialist->getAdditional_1()) . "', " .
			"additional_2='" . mysql_real_escape_string($editSpecialist->getAdditional_2()) . "', " .
			"additional_3='" . mysql_real_escape_string($editSpecialist->getAdditional_3()) . "', " .
			"additional_4='" . mysql_real_escape_string($editSpecialist->getAdditional_4()) . "', " .
			"language_1='" . mysql_real_escape_string($editSpecialist->getLanguage_1()) . "', " .
			"language_2='" . mysql_real_escape_string($editSpecialist->getLanguage_2()) . "', " .
			"language_3='" . mysql_real_escape_string($editSpecialist->getLanguage_3()) . "', " .
			"cert_nursing=". (($editSpecialist->getCert_nursing()!=null)?$editSpecialist->getCert_nursing():"null").", " .
			"cert_redcross=". (($editSpecialist->getCert_redcross()!=null)?$editSpecialist->getCert_redcross():"null").", " .
			"cert_language=". (($editSpecialist->getCert_language()!=null)?$editSpecialist->getCert_language():"null").", " .
			"cert_teaching=". (($editSpecialist->getCert_teaching()!=null)?$editSpecialist->getCert_teaching():"null").", " .
			"cert_cpcr=". (($editSpecialist->getCert_cpcr()!=null)?$editSpecialist->getCert_cpcr():"null").", " .
			"cert_firstaid=". (($editSpecialist->getCert_firstaid()!=null)?$editSpecialist->getCert_firstaid():"null").", " .
			"cert_child_dev=". (($editSpecialist->getCert_child_dev()!=null)?$editSpecialist->getCert_child_dev():"null").", " .
			"cert_partner=". (($editSpecialist->getCert_partner()!=null)?$editSpecialist->getCert_partner():"null").", " .
			"picture='" . mysql_real_escape_string($editSpecialist->getPicture()) . "', " .
			"us_citizen=". (($editSpecialist->getUs_citizen()!=null)?$editSpecialist->getUs_citizen():"null").", " .
			"slug='" . mysql_real_escape_string($editSpecialist->getSlug()) . "', udate=now() " .
			"WHERE id_specialist=" . $editSpecialist->getId_specialist();

			//echo("::::::[$sql_frm]:::::::");
			//die();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
		}
 
		return $retorno;
	}

	/**
	 *
	 * @param Specialist $editSpecialist
	 * @return NULL|Specialist
	 */
	public static function updateSpecialistPassword($id_specialist, $newPassword) {
	    $retorno = null;
	    if (($id_specialist!=null) && (is_numeric($id_specialist))) {
	        
	        //Updating specialist info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_specialists SET password=md5('". mysql_real_escape_string($newPassword) . "'), udate=now() WHERE id_specialist=" . $id_specialist;
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = SpecialistDAO::getSpecialist($id_specialist);
	    }
	    
	    return $retorno;
	}
	
	
	/**
	 *
	 * @param Specialist $editSpecialist
	 * @return NULL|Specialist
	 */
	public static function updateSpecialistPayment($id_specialist, $pay_bankname, $pay_routing, $pay_account, $pay_stripeid) {
	    $retorno = null;
	    if (($id_specialist!=null) && (is_numeric($id_specialist))) {
	        
	        //Updating specialist info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_specialists SET pay_bankname='". mysql_real_escape_string($pay_bankname) . "', pay_routing='". mysql_real_escape_string($pay_routing) . "', pay_account='". mysql_real_escape_string($pay_account) . "', pay_stripeid='". mysql_real_escape_string($pay_stripeid) . "', udate=now() WHERE id_specialist=" . $id_specialist;
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = SpecialistDAO::getSpecialist($id_specialist);
	    }
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param Specialist $editSpecialist
	 * @return NULL|Specialist
	 */
	public static function updateSpecialistStripeInfo($id_specialist, $pay_stripeid) {
	    $retorno = null;
	    if (($id_specialist!=null) && (is_numeric($id_specialist))) {
	        
	        //Updating specialist info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_specialists SET pay_stripeid='". mysql_real_escape_string($pay_stripeid) . "', udate=now() WHERE id_specialist=" . $id_specialist;
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = SpecialistDAO::getSpecialist($id_specialist);
	    }
	    
	    return $retorno;
	}
	
	
	
	public static function changePassword($editSpecialist, $newpassword) {
		$retorno = null;
		if (isset ($editSpecialist) && (is_numeric($editSpecialist->getId_specialist()))) {
			//Update password info
			$link = getConnection();
			$sql_frm = "UPDATE pd_specialists SET password=md5('" . $newpassword . "') WHERE id_specialist=" . $editSpecialist->getId_specialist();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = SpecialistDAO::getSpecialist($editSpecialist->getId_specialist());
		}

		return $retorno;
	}

	
	/*
	public static function activateUsuario($editUsuario) {
		$retorno = null;
		if (isset ($editUsuario) && (is_numeric($editUsuario->getId_usuario())) && ($editUsuario->getId_usuario() > 0)) {
			//Almacenamos la información de usuario
			$link = getConnection();

			$sql_frm = "UPDATE fm_usuarios SET flg_activo=1, ufecha=now() WHERE id_usuario=".$editUsuario->getId_usuario();

			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = UsuarioDAO::getUsuario($editUsuario->getId_usuario());
		}

		return $retorno;
	}

    
	
	public static function activateUsuarioByKey($activateKey) {
		$retorno = null;

		$usuario=UsuarioDAO :: getUsuarioByActivation($activateKey);

		if (!is_null($usuario)) {
			$retorno = UsuarioDAO::activateUsuario($usuario);
		}

		return $retorno;
	}
	

	public static function deactivateUsuario($editUsuario) {
		$retorno = null;
		if (isset ($editUsuario) && ($editUsuario->getUsuario() != "") && (is_numeric($editUsuario->getId_usuario()))) {
			//Actualizamos la información de usuario
			$link = getConnection();

			$sql_frm = "UPDATE fm_usuarios SET flg_activo=0, ufecha=now() WHERE id_usuario=" . $editUsuario->getId_usuario();

			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = UsuarioDAO::getUsuario($editUsuario->getId_usuario());
		}

		return $retorno;
	}

    */
	public static function deleteSpecialist($deleteSpecialist) {
		$retorno = false;

		if (isset ($deleteSpecialist) && (is_numeric($deleteSpecialist->getId_specialist()))) {
			//Remove specialist info
			$link = getConnection();
			$sql_frm = "DELETE FROM pd_specialists WHERE id_specialist=" . $deleteSpecialist->getId_specialist();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = true;
		}

		return $retorno;
	}

	
	public static function checkMail($specialist) {
		//1 - Test if mail exists
		$retorno = new Response(); // OK if not found
		$link = getConnection();
		$sql = "SELECT count(1) FROM pd_specialists WHERE email='" . $specialist->getEmail() . "'";
		//Read results
		$result = mysql_query($sql, $link);
		list ($total) = mysql_fetch_row($result);
		mysql_close($link);

		if ($total > 0) {
			$retorno->setError_code(-5);
			$retorno->setDescription("This email address belongs to a specialist");
		}

		return $retorno;
	}
	
	/**
	 * 
	 * @param number $id_playdate
	 * @param string $sortfield
	 * @param string $sorttype
	 * @return Specialist[]
	 */
	public static function getSupportersByPlaydate($id_playdate, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT i.* FROM pd_specialists i, pd_playdate_supporters p where i.id_specialist=p.id_specialist and p.id_playdate=$id_playdate and p.status=".Specialist::$SUPPORTER_ACCEPTED;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Specialist();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 *
	 * @param number $id_playdate
	 * @param string $sortfield
	 * @param string $sorttype
	 * @return Specialist[]
	 */
	public static function getSupporterRequestsByPlaydate($id_playdate, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    //$sql = "SELECT i.* FROM pd_specialists i, pd_playdate_supporters p where i.id_specialist=p.id_specialist and p.id_playdate=$id_playdate and p.status IN(".Specialist::$SUPPORTER_REQUESTED.", ".Specialist::$SUPPORTER_DECLINED.")";
	    $sql = "SELECT i.* FROM pd_specialists i, pd_playdate_supporters p where i.id_specialist=p.id_specialist and p.id_playdate=$id_playdate and p.status=".Specialist::$SUPPORTER_REQUESTED;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Specialist();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 *
	 * @param number $id_playdate
	 * @param string $sortfield
	 * @param string $sorttype
	 * @return Specialist[]
	 */
	public static function getAllSupporterRequestsByPlaydate($id_playdate, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    //$sql = "SELECT i.* FROM pd_specialists i, pd_playdate_supporters p where i.id_specialist=p.id_specialist and p.id_playdate=$id_playdate and p.status IN(".Specialist::$SUPPORTER_REQUESTED.", ".Specialist::$SUPPORTER_DECLINED.")";
	    $sql = "SELECT i.* FROM pd_specialists i, pd_playdate_supporters p where i.id_specialist=p.id_specialist and p.id_playdate=".$id_playdate;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Specialist();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 * 
	 * @param number $id_playdate
	 */
	public static function deleteAllPlaydateSupporters($id_playdate) {
	    if(is_numeric($id_playdate)) {
	        $sql = "DELETE FROM pd_playdate_supporters WHERE id_playdate=".$id_playdate;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	    }
	}
	
	
    /**
     * 
     * @param number $id_playdate
     * @param number $id_specialist
     */
    public static function deletePlaydateSupporter($id_playdate, $id_specialist) {
	    if(is_numeric($id_playdate)) {
	        $sql = "DELETE FROM pd_playdate_supporters WHERE id_playdate=".$id_playdate." AND id_specialist=".$id_specialist;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	    }
	}
	/**
	 * @param number $id_playdate
	 * @param number $id_specialist
	 */
	public static function createPlaydateSupporter($id_playdate, $id_specialist) {
	    if(is_numeric($id_specialist) && is_numeric($id_playdate)) {
	        $sql = "INSERT INTO pd_playdate_supporters (id_playdate, id_specialist, status, idate) VALUES (".$id_playdate.", ".$id_specialist.", ".Specialist::$SUPPORTER_ACCEPTED.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	

	/**
	 * @param number $id_playdate
	 * @param number $id_specialist
	 */
	public static function requestPlaydateSupporter($id_playdate, $id_specialist) {
	    if(is_numeric($id_specialist) && is_numeric($id_playdate)) {
	        $sql = "INSERT INTO pd_playdate_supporters (id_playdate, id_specialist, status, idate) VALUES (".$id_playdate.", ".$id_specialist.", ".Specialist::$SUPPORTER_REQUESTED.", now())";
	        //echo("::::[$sql]:::::");die();
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * @param number $id_playdate
	 * @param number $id_specialist
	 */
	public static function acceptPlaydateSupporter($id_playdate, $id_specialist) {
	    if(is_numeric($id_specialist) && is_numeric($id_playdate)) {
	        $sql = "UPDATE pd_playdate_supporters SET status=".Specialist::$SUPPORTER_ACCEPTED." WHERE id_playdate=".$id_playdate." AND id_specialist=".$id_specialist;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * @param number $id_playdate
	 * @param number $id_specialist
	 */
	public static function declinePlaydateSupporter($id_playdate, $id_specialist) {
	    if(is_numeric($id_specialist) && is_numeric($id_playdate)) {
	        $sql = "UPDATE pd_playdate_supporters SET status=".Specialist::$SUPPORTER_DECLINED." WHERE id_playdate=".$id_playdate." AND id_specialist=".$id_specialist;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
}
?>
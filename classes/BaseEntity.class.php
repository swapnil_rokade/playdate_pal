<?php
class BaseEntity {
	
    function BaseEntity() {
    }
    
    public static function getLimitedText($sourceText, $charlimit=9999) {
		$retorno = $sourceText;
		if($charlimit && (strlen($retorno)>$charlimit)) {
			$retorno=substr($retorno,0,($charlimit-3))."...";
		}
		return $retorno;    	
    }
    
	public function getId_tienda() {
		return 1;
	}
  
	/*
	public static function toStringDateFormat($fecha){
		$txtfecha = $fecha;
		
		if($fecha!="") {
			if(strpos($fecha,"/")==false) {
				if(strpos($fecha,":")==false) {
				    $txtfecha = preg_replace('#(\d{4})-(\d{2})-(\d{2})#', '$3/$2/$1', $fecha);
				    
				} else {
					$txtfecha = preg_replace('#(\d{4})-(\d{2})-(\d{2}) (\d{1,2}):(\d{1,2}):(\d{1,2})#', '$3/$2/$1, $4:$5:$6', $fecha);
				    
				}
			}
		} 
	
	    return $txtfecha;
	}
	*/
	
	/**
	 * to String, in american format
	 * @param mixed $fecha
	 * @return mixed
	 */
	public static function toStringDateFormat($fecha){
	    $txtfecha = $fecha;
	    
	    if($fecha!="") {
	        if(strpos($fecha,"/")==false) {
	            if(strpos($fecha,":")==false) {
	                $txtfecha = preg_replace('#(\d{4})-(\d{2})-(\d{2})#', '$2/$3/$1', $fecha);
	                
	            } else {
	                $txtfecha = preg_replace('#(\d{4})-(\d{2})-(\d{2}) (\d{1,2}):(\d{1,2}):(\d{1,2})#', '$2/$3/$1, $4:$5:$6', $fecha);
	                
	            }
	        }
	    }
	    
	    return $txtfecha;
	}
	
	public static function toTextDateFormat($fecha){
		if($fecha!="") {
			$meses=array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
			
    		if(strpos($fecha,":")==false) {
			    preg_match("#([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})#", $fecha, $mifecha);
				$txtfecha=$mifecha[1]. " de ".$meses[(-1+($mifecha[2]))]." de ".$mifecha[3];			
			} else {
			    preg_match( "#([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4}), ([0-9,:]{5,8})#", $fecha, $mifecha);
				$txtfecha=$mifecha[1]. " de ".$meses[(-1+($mifecha[2]))]." de ".$mifecha[3];			
			}
			
			//$txtfecha = preg_replace('#(\d{2})/(\d{2})/(\d{4}), (\d{1,2}):(\d{1,2}):(\d{1,2})#', '$1 de $2 de $3', $fecha);
		}
	    return $txtfecha;
	}
  
	public function truncateString($phrase, $maxlong = 150) {
	    if(strlen($phrase)>=$maxlong) {
    	    $phrase = substr(trim($phrase), 0, $maxlong);
    	    $pos = strrpos($phrase, " ");
    	    $phrase = substr($phrase, 0, $pos);
    	    if ((substr($phrase, -1, 1) == ",") or (substr($phrase, -1, 1) == ".")) {
    	        $phrase = substr($phrase, 0, -1);
    	    }
    	    if ($pos === false) {
    	        $phrase = $phrase;
    	    } else {
    	        $phrase = $phrase . "...";
    	    }
	    }
	    return $phrase;
	}
}
?>
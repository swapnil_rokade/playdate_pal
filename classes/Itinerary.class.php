<?php
include_once("BaseEntity.class.php");

class Itinerary extends BaseEntity {
	
    protected $id_itinerary, $id_playdate, $hour, $activity, $description, $idate, $udate;
    
    
   
        /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
    * Constructor
    */ 
    public function __construct( $id_itinerary=-1) {
        $this->id_itinerary = $id_itinerary;
    }
   
	public function readFromRow($row){
		if(isset($row['id_itinerary'])) $this->setId_itinerary($row['id_itinerary']);
		if(isset($row['id_playdate'])) $this->setId_playdate($row['id_playdate']);
		if(isset($row['activity'])) $this->setActivity($row['activity']);
		if(isset($row['description'])) $this->setDescription($row['description']);
		if(isset($row['hour'])) $this->setHour($row['hour']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}

	/**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }
    /**
     * @return mixed
     */
    public function getId_itinerary()
    {
        return $this->id_itinerary;
    }

    /**
     * @param mixed $id_itinerary
     */
    public function setId_itinerary($id_itinerary)
    {
        $this->id_itinerary = $id_itinerary;
    }

    /**
     * @return mixed
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * @param mixed $hour
     */
    public function setHour($hour)
    {
        $this->hour = $hour;
    }

    /**
     * @return mixed
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * @param mixed $activity
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @param mixed $activity
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    
}
?>
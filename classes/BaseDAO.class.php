<?php
/**
 * Superclase de los objetos de acceso a datos del portal
 */
class BaseDAO {
	
	public static $USE_GMT_TIME = "+2:00";
	
    function BaseDAO() {
    }

    static function create_slug($string, $ext='.html'){
		$replace = '-';
		$string = strtolower($string);
	
		//replace / and . with white space
		$string = preg_replace("/[\/\.]/", " ", $string);
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	
		//remove multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
	
		//convert whitespaces and underscore to $replace
		$string = preg_replace("/[\s_]/", $replace, $string);
	
		//limit the slug size
		$string = substr($string, 0, 100);
	
		//slug is generated
		return ($ext) ? $string.$ext : $string;
	}

	/*
	public static function toMysqlDateFormat($fecha){
		$txtfecha = null;
		if($fecha!="") {

		    $txtfecha = preg_replace('#(\d{2})/(\d{2})/(\d{4})#', '$3-$2-$1', $fecha);
		}
	    return $txtfecha;
	}
	*/
	public static function toMysqlDateFormat($fecha){
	    $txtfecha = null;
	    if($fecha!="") {
	        /*
	         preg_match('/([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})/', $fecha, $mifecha);
	         $txtfecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1];
	         */
	        $txtfecha = preg_replace('#(\d{2})/(\d{2})/(\d{4})#', '$3-$1-$2', $fecha);
	    }
	    return $txtfecha;
	}
    
	public static function toMysqlDateTimeSecondsFormat($fecha){
		if($fecha!="") {
		 	$pos = strpos($fecha, ":");
    		if ($pos == false) {
				$txtfecha = BaseDAO::toMysqlDateFormat($fecha);	
    		} else {
    			if (strpos($fecha, ',') !== false) {
			    	preg_match('#([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4}), ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})#', $fecha, $mifecha);
    			} else {
    				preg_match('#([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})#', $fecha, $mifecha);
    			}
			    $txtfecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]." ".$mifecha[4].":".$mifecha[5].":".$mifecha[6];
        	}
		}
	    return $txtfecha;
	} 
        
	public static function toMysqlDateTimeFormat($fecha){
		if($fecha!="") {
		 	$pos = strpos($fecha, ":");
    		if ($pos == false) {
				$txtfecha = BaseDAO::toMysqlDateFormat($fecha);	
    		} else {
    			if (strpos($fecha, ',') !== false) {
			    	preg_match('#([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4}), ([0-9]{1,2}):([0-9]{1,2})#', $fecha, $mifecha);
    			} else {
    				preg_match('#([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4}) ([0-9]{1,2}):([0-9]{1,2})#', $fecha, $mifecha);
    			}
			    $txtfecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]." ".$mifecha[4].":".$mifecha[5];
        	}
		}
	    return $txtfecha;
	} 
}
?>
<?php
include_once("BaseDAO.class.php");

class InvitationDAO extends BaseDAO {

    public function  InvitationDAO() {
    }
    
    public static function countInvitations() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_invitations";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	public static function getInvitationsList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_invitations";

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by id_invitation desc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Invitation();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	
	public static function getInvitation($id_invitation) {
		$retorno = null;
		if(is_numeric($id_invitation)) {
			$sql = "SELECT * FROM pd_invitations where id_invitation=$id_invitation";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Invitation();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    


	/**
	 * 
	 * @param number $id_request_invitation
	 * @return NULL|InvitationRequest
	 */
	public static function getInvitationRequest($id_request_invitation) {
	    $retorno = null;
	    if(is_numeric($id_request_invitation)) {
	        $sql = "SELECT * FROM pd_request_invitation where id_request_invitation=$id_request_invitation";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new InvitationRequest();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	
	public static function getInvitationByCode($codeParam) {
		$retorno = null;
		
		if($codeParam!=null) {
	        $link1 = getConnection();
	        
			$sql = "SELECT * FROM pd_invitations where code='".mysql_real_escape_string($codeParam)."'";
			//$sql = "SELECT * FROM pd_invitations where code='".$codeParam."'";
			
			//echo(":::[$sql]:::");die();
			//Read results
	        $result = mysql_query($sql, $link1);
	        if($row = mysql_fetch_assoc($result)) {
				$newItem = new Invitation();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	/**
	 * 
	 * @param mixed $email
	 * @return NULL|Invitation
	 */
	public static function createInvitationForEmail($email) {
	    $retorno = null;
	    if($email!=null) {
	        $link1 = getConnection();
	        $newCode = InvitationDAO::generateInvitationCode();
	        $sql = "INSERT INTO pd_invitations (email, code, idate) VALUES ('".mysql_real_escape_string($email)."', '".$newCode."', now())";
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	        $retorno = InvitationDAO::getInvitationByCode($newCode);
	    }
	    return $retorno;
	}
	

	/**
	 * Create Request Parent - Request for invitation for parent
	 * @param ParentPd $newParent
	 * @return boolean
	 */
	public static function createRequestInvitation($newParent) {
	    $retorno = false;
	    
	    if (($newParent!=null) && ($newParent->getEmail() != "")) {
	        //Save parent basic info
	        $link = getConnection();
	        
	        $sql_frm = "INSERT INTO pd_request_invitation (name, lastname, username, email, zipcode, password, idate) " .
	   	        "VALUES (".
	   	        "'".mysql_real_escape_string($newParent->getName())."', ".
	   	        "'".mysql_real_escape_string($newParent->getLastname())."', ".
	   	        "'".mysql_real_escape_string($newParent->getUsername())."', ".
	   	        "'".mysql_real_escape_string($newParent->getEmail())."', ".
	   	        "'".mysql_real_escape_string($newParent->getZipcode())."', ".
	   	        "md5('".mysql_real_escape_string($newParent->getPassword())."'), ".
	   	        "now()".
	   	        ")";
	        
	        //echo("::::[$sql_frm]::::"); die();
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;

	    }
	    return $retorno;
	}
	
	
	public static function generateInvitationCode() {
	    return substr(str_shuffle("0123456789ABCDEFGHJKLMNOPQRSTUVWXYZ"), 0, Invitation::$INVITATION_CODE_LENGTH); 
	}
	
	public static function deleteInvitation($invitationId) {
	    $retorno = false;
	    
	    if (($invitationId!=null) && (is_numeric($invitationId))) {
	        //Remove invitation
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_invitations WHERE id_invitation=" . $invitationId;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	/**
	 * 
	 * @param number $id_request_invitation
	 * @return boolean
	 */
	public static function deleteRequestInvitation($id_request_invitation) {
	    $retorno = false;
	    
	    if (($id_request_invitation!=null) && (is_numeric($id_request_invitation))) {
	        //Remove request invitation
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_request_invitation WHERE id_request_invitation=" . $id_request_invitation;
	        
	        //echo(":::::[$sql_frm]:::::::");die();
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	/**
	 * 
	 * @param number $id_request_invitation
	 * @param number $status
	 */
	public static function updateRequestInvitationStatus($id_request_invitation, $status) {
	    if(is_numeric($id_request_invitation)) {
	        $link1 = getConnection();
	        $sql = "UPDATE pd_request_invitation SET status=".$status.", udate=now() WHERE id_request_invitation=".$id_request_invitation;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * 
	 * @param number $id_request_invitation
	 * @param number $status
	 */
	public static function updateRequestInvitationStatusEmail($firstname,$lastname,$email, $status,$username,$password) {
		$link1 = getConnection();
		$sql = "UPDATE pd_request_invitation SET status=".$status.", udate=now(), username='".mysql_real_escape_string($username)."',password=md5('".mysql_real_escape_string($password)."') WHERE email='".mysql_real_escape_string($email)."' AND name='".mysql_real_escape_string($firstname)."' AND lastname='".mysql_real_escape_string($lastname)."'";
		$result = mysql_query($sql, $link1);
		mysql_close($link1);
	}
	
	
	/**
	 * Update Invitation
	 * @param Invitation $editInvitation
	 * @return NULL|Invitation
	 */
	public static function updateInvitation($editInvitation) {
	    $retorno = null;
	    if (isset ($editInvitation) && (is_numeric($editInvitation->getId_invitation()))) {
	        
	        //Updating invitation info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_invitations SET code='".mysql_real_escape_string($editInvitation->getCode())."', " .
	   	        "email='" . mysql_real_escape_string($editInvitation->getEmail()) . "', " .
	   	        "udate=now() " .
	   	        "WHERE id_invitation=" . $editInvitation->getId_invitation();
	        
	        //echo("::::::[$sql_frm]:::::::");
	        //die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = InvitationDAO::getInvitation($editInvitation->getId_invitation());
	    }
	    
	    return $retorno;
	}
	
	
	/**
	 * 
	 * @param number $page
	 * @param number $elementsPerPage
	 * @param string $sortfield
	 * @param string $sorttype
	 * @return ParentPd[]
	 */
	public static function getRequestInvitationsList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_request_invitation";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request_invitation desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new InvitationRequest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
}
?>
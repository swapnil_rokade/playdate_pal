<?php
include_once("BaseEntity.class.php");

class Activity extends BaseEntity {
	
    protected $id_activity, $name, $order, $idate, $udate;
    
    
   
        /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

        /**
    * Constructor
    */ 
    public function __construct( $id_activity=-1) {
        $this->id_activity = $id_activity;
    }
   
	public function readFromRow($row){
	    if(isset($row['id_activity'])) $this->setId_activity($row['id_activity']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['order'])) $this->setOrder($row['order']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_activity()
    {
        return $this->id_activity;
    }

    /**
     * @param mixed $id_activity
     */
    public function setId_activity($id_activity)
    {
        $this->id_activity = $id_activity;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }


	
}
?>
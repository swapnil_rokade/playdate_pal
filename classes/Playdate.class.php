<?php
include_once("BaseEntity.class.php");

class Playdate extends BaseEntity {
    
    public static $STATUS_REQUEST=0;  //borrador o respuesta a una request
    public static $STATUS_PUBLISHED=1; //publicado visible
    public static $STATUS_FINISHED=2; //cerrado
    public static $STATUS_DECLINED=3; //cerrado
    public static $STATUS_RSVP=4; //alcanzado RSVP
    
    public static $PRIVACITY_PUBLIC=1;  //public
    public static $PRIVACITY_PRIVATE=2;  //privated
    public static $PRIVACITY_GROUP=3;  //group
    
    public static $FEATURED_NO=0;  //no featured
    public static $FEATURED_YES=1;  //featured
    
    
    protected $id_playdate, $name, $loc_address, $loc_city, $loc_state, $loc_id_zipcode, $id_neighborhood, $date, $time_init, $time_end;
    protected $age_init, $age_end, $description, $picture, $num_children, $invite_parents, $privacy_type, $privacy_group_id, $open_spots, $idate, $udate, $status, $slug;
    
    protected $id_specialist, $id_parent, $id_request, $type, $num_supporters;
    
    protected $pickup_address, $pickup_walk, $pickup_train, $pickup_car, $pickup_taxi, $dropoff_address, $dropoff_walk, $dropoff_train, $dropoff_car, $dropoff_taxi;
    
    protected $act1_id, $act1_note, $act1_time, $act2_id, $act2_note, $act2_time, $act3_id, $act3_note, $act3_time, $act4_id, $act4_note, $act4_time;
    protected $add1_id, $add1_price, $add1_id_check, $add2_id, $add2_price, $add2_id_check,$add3_id, $add3_price, $add3_id_check, $add4_id, $add4_price, $add4_id_check;
    
    protected $featured=0;
    
    protected $min_kids=1; //20181221 new feature min kids
    
    protected $type_of_playdate, $date_1,$date_2,$date_3,$date_4,$date_5,$date_6,$date_7,$date_8;
    /**
    * Constructor
    */ 
    public function __construct( $id_playdate=-1) {
        $this->id_playdate = $id_playdate;
    }
   
	public function readFromRow($row){
	    if(isset($row['id_playdate'])) { $this->setId_playdate($row['id_playdate']);}
	    if(isset($row['name'])) { $this->setName($row['name']);}
	    if(isset($row['loc_address'])) { $this->setLoc_address($row['loc_address']);}
	    if(isset($row['loc_city'])) { $this->setLoc_city($row['loc_city']);}
	    if(isset($row['loc_state'])) { $this->setLoc_state($row['loc_state']);}
	    if(isset($row['loc_id_zipcode'])) { $this->setLoc_id_zipcode($row['loc_id_zipcode']);}
	    if(isset($row['id_neighborhood'])) { $this->setId_neighborhood($row['id_neighborhood']);}
	    if(isset($row['date']) && ($row['date']!="0000-00-00 00:00:00")) $this->date=($this->toStringDateFormat($row['date']));
	    if(isset($row['time_init'])) { $this->setTime_init($row['time_init']);}
	    if(isset($row['time_end'])) { $this->setTime_end($row['time_end']);}
	    if(isset($row['age_init'])) { $this->setAge_init($row['age_init']);}
	    if(isset($row['age_end'])) { $this->setAge_end($row['age_end']);}
	    if(isset($row['description'])) { $this->setDescription($row['description']);}
	    if(isset($row['picture'])) { $this->setPicture($row['picture']);}
	    if(isset($row['num_children'])) { $this->setNum_children($row['num_children']);$this->setOpen_spots($row['num_children']);}
	    if(isset($row['num_supporters'])) { $this->setNum_supporters($row['num_supporters']);}
	    if(isset($row['invite_parents'])) { $this->setInvite_parents($row['invite_parents']);}
	    if(isset($row['privacy_type'])) { $this->setPrivacy_type($row['privacy_type']);}
	    if(isset($row['privacy_group_id'])) { $this->setPrivacy_group_id($row['privacy_group_id']);}
	    if(isset($row['open_spots'])) { $this->setOpen_spots($row['open_spots']);$this->setNum_children($row['open_spots']);}
	    if(isset($row['id_specialist'])) { $this->setId_specialist($row['id_specialist']);}
	    if(isset($row['id_parent'])) { $this->setId_parent($row['id_parent']);}
	    if(isset($row['id_request'])) { $this->setId_request($row['id_request']);}
	    if(isset($row['status'])) { $this->setStatus($row['status']);}
	    if(isset($row['slug'])) { $this->setSlug($row['slug']);}
	    if(isset($row['type'])) { $this->setType($row['type']);}
	    
	    if(isset($row['pickup_address'])) { $this->setPickup_address($row['pickup_address']);}
	    if(isset($row['pickup_walk'])) { $this->setPickup_walk($row['pickup_walk']);}
	    if(isset($row['pickup_train'])) { $this->setPickup_train($row['pickup_train']);}
	    if(isset($row['pickup_car'])) { $this->setPickup_car($row['pickup_car']);}
	    if(isset($row['pickup_taxi'])) { $this->setPickup_taxi($row['pickup_taxi']);}
	    
	    if(isset($row['dropoff_address'])) { $this->setDropoff_address($row['dropoff_address']);}
	    if(isset($row['dropoff_walk'])) { $this->setDropoff_walk($row['dropoff_walk']);}
	    if(isset($row['dropoff_train'])) { $this->setDropoff_train($row['dropoff_train']);}
	    if(isset($row['dropoff_car'])) { $this->setDropoff_car($row['dropoff_car']);}
	    if(isset($row['dropoff_taxi'])) { $this->setDropoff_taxi($row['dropoff_taxi']);}
	    
	    if(isset($row['act1_id'])) { $this->setAct1_id($row['act1_id']);}
	    if(isset($row['act1_note'])) { $this->setAct1_note($row['act1_note']);}
	    if(isset($row['act1_time'])) { $this->setAct1_time($row['act1_time']);}
	    if(isset($row['act2_id'])) { $this->setAct2_id($row['act2_id']);}
	    if(isset($row['act2_note'])) { $this->setAct2_note($row['act2_note']);}
	    if(isset($row['act2_time'])) { $this->setAct2_time($row['act2_time']);}
	    if(isset($row['act3_id'])) { $this->setAct3_id($row['act3_id']);}
	    if(isset($row['act3_note'])) { $this->setAct3_note($row['act3_note']);}
	    if(isset($row['act3_time'])) { $this->setAct3_time($row['act3_time']);}
	    if(isset($row['act4_id'])) { $this->setAct4_id($row['act4_id']);}
	    if(isset($row['act4_note'])) { $this->setAct4_note($row['act4_note']);}
	    if(isset($row['act4_time'])) { $this->setAct4_time($row['act4_time']);}
	    
	    if(isset($row['add1_id'])) { $this->setAdd1_id($row['add1_id']);}
        if(isset($row['add1_price'])) { $this->setAdd1_price($row['add1_price']);}
        if(isset($row['add1_id_check'])) { $this->setAdd1_id_check($row['add1_id_check']);}
	    if(isset($row['add2_id'])) { $this->setAdd2_id($row['add2_id']);}
        if(isset($row['add2_price'])) { $this->setAdd2_price($row['add2_price']);}
        if(isset($row['add2_id_check'])) { $this->setAdd2_id_check($row['add2_id_check']);}
	    if(isset($row['add3_id'])) { $this->setAdd3_id($row['add3_id']);}
        if(isset($row['add3_price'])) { $this->setAdd3_price($row['add3_price']);}
        if(isset($row['add3_id_check'])) { $this->setAdd3_id_check($row['add3_id_check']);}
	    if(isset($row['add4_id'])) { $this->setAdd4_id($row['add4_id']);}
	    if(isset($row['add4_price'])) { $this->setAdd4_price($row['add4_price']);}
        if(isset($row['add4_id_check'])) { $this->setAdd4_id_check($row['add4_id_check']);}
        
	    if(isset($row['featured'])) { $this->setFeatured($row['featured']);}
	    
        if(isset($row['min_kids'])) { $this->setMin_kids($row['min_kids']);}
        
        if(isset($row['type_of_playdate'])) { $this->setType_of_Playdate($row['type_of_playdate']);}
        if(isset($row['date_1'])) { $this->setDate_1($row['date_1']);}
        if(isset($row['date_2'])) { $this->setDate_2($row['date_2']);}
        if(isset($row['date_3'])) { $this->setDate_3($row['date_3']);}
        if(isset($row['date_4'])) { $this->setDate_4($row['date_4']);}
        if(isset($row['date_5'])) { $this->setDate_5($row['date_5']);}
        if(isset($row['date_6'])) { $this->setDate_6($row['date_6']);}
        if(isset($row['date_7'])) { $this->setDate_7($row['date_7']);}
        if(isset($row['date_8'])) { $this->setDate_8($row['date_8']);}
	    
	    if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
	/**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLoc_address()
    {
        return $this->loc_address;
    }

    /**
     * @param mixed $loc_address
     */
    public function setLoc_address($loc_address)
    {
        $this->loc_address = $loc_address;
    }

    /**
     * @return mixed
     */
    public function getLoc_city()
    {
        return $this->loc_city;
    }

    /**
     * @param mixed $loc_city
     */
    public function setLoc_city($loc_city)
    {
        $this->loc_city = $loc_city;
    }

    /**
     * @return mixed
     */
    public function getLoc_state()
    {
        return $this->loc_state;
    }

    /**
     * @param mixed $loc_state
     */
    public function setLoc_state($loc_state)
    {
        $this->loc_state = $loc_state;
    }

    /**
     * @return mixed
     */
    public function getLoc_id_zipcode()
    {
        return $this->loc_id_zipcode;
    }

    /**
     * @param mixed $loc_id_zipcode
     */
    public function setLoc_id_zipcode($loc_id_zipcode)
    {
        $this->loc_id_zipcode = $loc_id_zipcode;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTime_init()
    {
        return $this->time_init;
    }

    /**
     * @param mixed $time_init
     */
    public function setTime_init($time_init)
    {
        $this->time_init = $time_init;
    }

    /**
     * @return mixed
     */
    public function getTime_end()
    {
        return $this->time_end;
    }

    /**
     * @param mixed $time_end
     */
    public function setTime_end($time_end)
    {
        $this->time_end = $time_end;
    }

    /**
     * @return mixed
     */
    public function getAge_init()
    {
        return $this->age_init;
    }

    /**
     * @param mixed $age_init
     */
    public function setAge_init($age_init)
    {
        $this->age_init = $age_init;
    }

    /**
     * @return mixed
     */
    public function getAge_end()
    {
        return $this->age_end;
    }

    /**
     * @param mixed $age_end
     */
    public function setAge_end($age_end)
    {
        $this->age_end = $age_end;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getNum_children()
    {
        return $this->num_children;
    }

    /**
     * @param mixed $num_children
     */
    public function setNum_children($num_children)
    {
        $this->num_children = $num_children;
    }

    /**
     * @return mixed
     */
    public function getInvite_parents()
    {
        return $this->invite_parents;
    }

    /**
     * @param mixed $invite_parents
     */
    public function setInvite_parents($invite_parents)
    {
        $this->invite_parents = $invite_parents;
    }

    /**
     * @return mixed
     */
    public function getPrivacy_type()
    {
        return $this->privacy_type;
    }

    /**
     * @param mixed $privacy_type
     */
    public function setPrivacy_type($privacy_type)
    {
        $this->privacy_type = $privacy_type;
    }

    /**
     * @return mixed
     */
    public function getPrivacy_group_id()
    {
        return $this->privacy_group_id;
    }

    /**
     * @param mixed $privacy_group_id
     */
    public function setPrivacy_group_id($privacy_group_id)
    {
        $this->privacy_group_id = $privacy_group_id;
    }

    /**
     * @return mixed
     */
    public function getOpen_spots()
    {
        return $this->open_spots;
    }

    /**
     * @param mixed $open_spots
     */
    public function setOpen_spots($open_spots)
    {
        $this->open_spots = $open_spots;
    }

    /**
     * @return mixed
     */
    public function getId_specialist()
    {
        return $this->id_specialist;
    }

    /**
     * @param mixed $id_specialist
     */
    public function setId_specialist($id_specialist)
    {
        $this->id_specialist = $id_specialist;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }
    
    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }
    
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
    /**
     * @return mixed
     */
    public function getId_request()
    {
        return $this->id_request;
    }

    /**
     * @param mixed $id_request
     */
    public function setId_request($id_request)
    {
        $this->id_request = $id_request;
    }
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    /**
     * @return mixed
     */
    public function getNum_supporters()
    {
        return $this->num_supporters;
    }

    /**
     * @param mixed $num_supporters
     */
    public function setNum_supporters($num_supporters)
    {
        $this->num_supporters = $num_supporters;
    }
    /**
     * @return mixed
     */
    public function getPickup_address()
    {
        return $this->pickup_address;
    }

    /**
     * @param mixed $pickup_address
     */
    public function setPickup_address($pickup_address)
    {
        $this->pickup_address = $pickup_address;
    }

    /**
     * @return mixed
     */
    public function getPickup_walk()
    {
        return $this->pickup_walk;
    }

    /**
     * @param mixed $pickup_walk
     */
    public function setPickup_walk($pickup_walk)
    {
        $this->pickup_walk = $pickup_walk;
    }

    /**
     * @return mixed
     */
    public function getPickup_train()
    {
        return $this->pickup_train;
    }

    /**
     * @param mixed $pickup_train
     */
    public function setPickup_train($pickup_train)
    {
        $this->pickup_train = $pickup_train;
    }

    /**
     * @return mixed
     */
    public function getPickup_car()
    {
        return $this->pickup_car;
    }

    /**
     * @param mixed $pickup_car
     */
    public function setPickup_car($pickup_car)
    {
        $this->pickup_car = $pickup_car;
    }

    /**
     * @return mixed
     */
    public function getPickup_taxi()
    {
        return $this->pickup_taxi;
    }

    /**
     * @param mixed $pickup_taxi
     */
    public function setPickup_taxi($pickup_taxi)
    {
        $this->pickup_taxi = $pickup_taxi;
    }

    /**
     * @return mixed
     */
    public function getDropoff_address()
    {
        return $this->dropoff_address;
    }

    /**
     * @param mixed $dropoff_address
     */
    public function setDropoff_address($dropoff_address)
    {
        $this->dropoff_address = $dropoff_address;
    }

    /**
     * @return mixed
     */
    public function getDropoff_walk()
    {
        return $this->dropoff_walk;
    }

    /**
     * @param mixed $dropoff_walk
     */
    public function setDropoff_walk($dropoff_walk)
    {
        $this->dropoff_walk = $dropoff_walk;
    }

    /**
     * @return mixed
     */
    public function getDropoff_train()
    {
        return $this->dropoff_train;
    }

    /**
     * @param mixed $dropoff_train
     */
    public function setDropoff_train($dropoff_train)
    {
        $this->dropoff_train = $dropoff_train;
    }

    /**
     * @return mixed
     */
    public function getDropoff_car()
    {
        return $this->dropoff_car;
    }

    /**
     * @param mixed $dropoff_car
     */
    public function setDropoff_car($dropoff_car)
    {
        $this->dropoff_car = $dropoff_car;
    }

    /**
     * @return mixed
     */
    public function getDropoff_taxi()
    {
        return $this->dropoff_taxi;
    }

    /**
     * @param mixed $dropoff_taxi
     */
    public function setDropoff_taxi($dropoff_taxi)
    {
        $this->dropoff_taxi = $dropoff_taxi;
    }
    /**
     * @return mixed
     */
    public function getAct1_id()
    {
        return $this->act1_id;
    }

    /**
     * @param mixed $act1_id
     */
    public function setAct1_id($act1_id)
    {
        $this->act1_id = $act1_id;
    }

    /**
     * @return mixed
     */
    public function getAct1_note()
    {
        return $this->act1_note;
    }

    /**
     * @param mixed $act1_note
     */
    public function setAct1_note($act1_note)
    {
        $this->act1_note = $act1_note;
    }

    /**
     * @return mixed
     */
    public function getAct1_time()
    {
        return $this->act1_time;
    }

    /**
     * @param mixed $act1_time
     */
    public function setAct1_time($act1_time)
    {
        $this->act1_time = $act1_time;
    }

    /**
     * @return mixed
     */
    public function getAct2_id()
    {
        return $this->act2_id;
    }

    /**
     * @param mixed $act2_id
     */
    public function setAct2_id($act2_id)
    {
        $this->act2_id = $act2_id;
    }

    /**
     * @return mixed
     */
    public function getAct2_note()
    {
        return $this->act2_note;
    }

    /**
     * @param mixed $act2_note
     */
    public function setAct2_note($act2_note)
    {
        $this->act2_note = $act2_note;
    }

    /**
     * @return mixed
     */
    public function getAct2_time()
    {
        return $this->act2_time;
    }

    /**
     * @param mixed $act2_time
     */
    public function setAct2_time($act2_time)
    {
        $this->act2_time = $act2_time;
    }

    /**
     * @return mixed
     */
    public function getAct3_id()
    {
        return $this->act3_id;
    }

    /**
     * @param mixed $act3_id
     */
    public function setAct3_id($act3_id)
    {
        $this->act3_id = $act3_id;
    }

    /**
     * @return mixed
     */
    public function getAct3_note()
    {
        return $this->act3_note;
    }

    /**
     * @param mixed $act3_note
     */
    public function setAct3_note($act3_note)
    {
        $this->act3_note = $act3_note;
    }

    /**
     * @return mixed
     */
    public function getAct3_time()
    {
        return $this->act3_time;
    }

    /**
     * @param mixed $act3_time
     */
    public function setAct3_time($act3_time)
    {
        $this->act3_time = $act3_time;
    }

    /**
     * @return mixed
     */
    public function getAct4_id()
    {
        return $this->act4_id;
    }

    /**
     * @param mixed $act4_id
     */
    public function setAct4_id($act4_id)
    {
        $this->act4_id = $act4_id;
    }

    /**
     * @return mixed
     */
    public function getAct4_note()
    {
        return $this->act4_note;
    }

    /**
     * @param mixed $act4_note
     */
    public function setAct4_note($act4_note)
    {
        $this->act4_note = $act4_note;
    }

    /**
     * @return mixed
     */
    public function getAct4_time()
    {
        return $this->act4_time;
    }

    /**
     * @param mixed $act4_time
     */
    public function setAct4_time($act4_time)
    {
        $this->act4_time = $act4_time;
    }

    /**
     * @return mixed
     */
    public function getAdd1_id()
    {
        return $this->add1_id;
    }

    /**
     * @param mixed $add1_id
     */
    public function setAdd1_id($add1_id)
    {
        $this->add1_id = $add1_id;
    }

    /**
     * @return mixed
     */
    public function getAdd1_price()
    {
        return $this->add1_price;
    }

    /**
     * @param mixed $add1_price
     */
    public function setAdd1_price($add1_price)
    {
        $this->add1_price = $add1_price;
    }

    /**
     * @return mixed
     */
    public function getAdd1_id_check()
    {
        return $this->add1_id_check;
    }

    /**
     * @param mixed $add1_id_check
     */
    public function setAdd1_id_check($add1_id_check)
    {
        $this->add1_id_check = $add1_id_check;
    }

    /**
     * @return mixed
     */
    public function getAdd2_id()
    {
        return $this->add2_id;
    }

    /**
     * @param mixed $add2_id
     */
    public function setAdd2_id($add2_id)
    {
        $this->add2_id = $add2_id;
    }

    /**
     * @return mixed
     */
    public function getAdd2_price()
    {
        return $this->add2_price;
    }

    /**
     * @param mixed $add2_price
     */
    public function setAdd2_price($add2_price)
    {
        $this->add2_price = $add2_price;
    }

    /**
     * @return mixed
     */
    public function getAdd2_id_check()
    {
        return $this->add2_id_check;
    }

    /**
     * @param mixed $add2_id_check
     */
    public function setAdd2_id_check($add2_id_check)
    {
        $this->add2_id_check = $add2_id_check;
    }

    /**
     * @return mixed
     */
    public function getAdd3_id()
    {
        return $this->add3_id;
    }

    /**
     * @param mixed $add3_id
     */
    public function setAdd3_id($add3_id)
    {
        $this->add3_id = $add3_id;
    }

    /**
     * @return mixed
     */
    public function getAdd3_price()
    {
        return $this->add3_price;
    }

    /**
     * @param mixed $add3_price
     */
    public function setAdd3_price($add3_price)
    {
        $this->add3_price = $add3_price;
    }

    /**
     * @return mixed
     */
    public function getAdd3_id_check()
    {
        return $this->add3_id_check;
    }

    /**
     * @param mixed $add3_id_check
     */
    public function setAdd3_id_check($add3_id_check)
    {
        $this->add3_id_check = $add3_id_check;
    }

    /**
     * @return mixed
     */
    public function getAdd4_id()
    {
        return $this->add4_id;
    }

    /**
     * @param mixed $add4_id
     */
    public function setAdd4_id($add4_id)
    {
        $this->add4_id = $add4_id;
    }

    /**
     * @return mixed
     */
    public function getAdd4_price()
    {
        return $this->add4_price;
    }

    /**
     * @param mixed $add4_price
     */
    public function setAdd4_price($add4_price)
    {
        $this->add4_price = $add4_price;
    }

    /**
     * @return mixed
     */
    public function getAdd4_id_check()
    {
        return $this->add4_id_check;
    }

    /**
     * @param mixed $add4_id_check
     */
    public function setAdd4_id_check($add4_id_check)
    {
        $this->add4_id_check = $add4_id_check;
    }

    /**
     * @return mixed
     */
    public function getId_neighborhood()
    {
        return $this->id_neighborhood;
    }

    /**
     * @param mixed $id_neighborhood
     */
    public function setId_neighborhood($id_neighborhood)
    {
        $this->id_neighborhood = $id_neighborhood;
    }
    /**
     * @return number
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * @param number $featured
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;
    }
    /**
     * @return number
     */
    public function getMin_kids()
    {
        return $this->min_kids;
    }

    /**
     * @param number $min_kids
     */
    public function setMin_kids($min_kids)
    {
        $this->min_kids = $min_kids;
    }

	public function getType_of_Playdate()
    {
        return $this->type_of_playdate;
    }

    /**
     * @param number $type_of_playdate
     */
    public function setType_of_Playdate($type_of_playdate)
    {
        $this->type_of_playdate = $type_of_playdate;
    }

    public function getDate_1()
    {
        return $this->date_1;
    }

    /**
     * @param number $date_1
     */
    public function setDate_1($date_1)
    {
        $this->date_1 = $date_1;
    }

    public function getDate_2()
    {
        return $this->date_2;
    }

    /**
     * @param number $date_2
     */
    public function setDate_2($date_2)
    {
        $this->date_2 = $date_2;
    }

    public function getDate_3()
    {
        return $this->date_3;
    }

    /**
     * @param number $date_3
     */
    public function setDate_3($date_3)
    {
        $this->date_3 = $date_3;
    }

    public function getDate_4()
    {
        return $this->date_4;
    }

    /**
     * @param number $date_4
     */
    public function setDate_4($date_4)
    {
        $this->date_4 = $date_4;
    }

    public function getDate_5()
    {
        return $this->date_5;
    }

    /**
     * @param number $date_5
     */
    public function setDate_5($date_5)
    {
        $this->date_5 = $date_5;
    }

    public function getDate_6()
    {
        return $this->date_6;
    }

    /**
     * @param number $date_6
     */
    public function setDate_6($date_6)
    {
        $this->date_6 = $date_6;
    }

    public function getDate_7()
    {
        return $this->date_7;
    }

    /**
     * @param number $date_7
     */
    public function setDate_7($date_7)
    {
        $this->date_7 = $date_7;
    }

    public function getDate_8()
    {
        return $this->date_8;
    }

    /**
     * @param number $date_8
     */
    public function setDate_8($date_8)
    {
        $this->date_8 = $date_8;
    }
} 
?>
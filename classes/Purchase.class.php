<?php
include_once("BaseEntity.class.php");

class Purchase extends BaseEntity {
	
    protected $id_purchase, $id_parent, $amount, $description, $idate;
    
    public function readFromRow($row){
        if(isset($row['id_purchase'])) $this->setId_purchase($row['id_purchase']);
        if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
        if(isset($row['amount'])) $this->setAmount($row['amount']);
		if(isset($row['description'])) $this->setDescription(stripslashes($row['description']));
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
	}
    /**
     * @return mixed
     */
    public function getId_purchase()
    {
        return $this->id_purchase;
    }

    /**
     * @param mixed $id_purchase
     */
    public function setId_purchase($id_purchase)
    {
        $this->id_purchase = $id_purchase;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param mixed $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

	
	
}
?>
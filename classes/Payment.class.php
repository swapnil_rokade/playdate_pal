<?php
include_once("BaseEntity.class.php");

class Payment extends BaseEntity {
    
    public static $STATUS_PENDING = 0;
    public static $STATUS_COMPLETED = 1;
    public static $STATUS_PARTIAL = 2;
    public static $STATUS_ROLLBACK = 3;
	
    protected $id_payment, $id_playdate, $id_parent, $id_specialist, $name, $amount_playdate=0, $amount_addons=0, $amount_paid=0,$discount_amount=0,$discount_code, $comment, $status, $date, $idate;
    
    public function Payment() {
        //set default values
        $this->status=Payment::$STATUS_PENDING;
        $this->amount_playdate=0;
        $this->amount_addons=0;
        $this->amount_paid=0;
    }
    
    
    public function readFromRow($row){
        if(isset($row['id_payment'])) $this->setId_payment($row['id_payment']);
        if(isset($row['id_playdate'])) $this->setId_playdate($row['id_playdate']);
        if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
        if(isset($row['id_specialist'])) $this->setId_specialist($row['id_specialist']);
        if(isset($row['name'])) $this->setName($row['name']);
        if(isset($row['comment'])) $this->setComment($row['comment']);
        if(isset($row['status'])) $this->setStatus($row['status']);
        if(isset($row['amount_playdate'])) $this->setAmount_playdate($row['amount_playdate']);
        if(isset($row['amount_addons'])) $this->setAmount_addons($row['amount_addons']);
        if(isset($row['amount_paid'])) $this->setAmount_paid($row['amount_paid']);
        if(isset($row['discount_amount'])) $this->setDiscount_amount($row['discount_amount']);
        if(isset($row['discount_code'])) $this->setDiscount_code($row['discount_code']);
        if(isset($row['date']) && ($row['date']!="0000-00-00 00:00:00")) $this->date=($this->toStringDateFormat($row['date']));
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
	}
	
	
    /**
     * @return mixed
     */
	public function getId_payment()
    {
        return $this->id_payment;
    }

    /**
     * @param mixed $id_payment
     */
    public function setId_payment($id_payment)
    {
        $this->id_payment = $id_payment;
    }

    /**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAmount_playdate()
    {
        return $this->amount_playdate;
    }

    /**
     * @param mixed $amount_playdate
     */
    public function setAmount_playdate($amount_playdate)
    {
        $this->amount_playdate = $amount_playdate;
    }

    /**
     * @return mixed
     */
    public function getAmount_addons()
    {
        return $this->amount_addons;
    }

    /**
     * @param mixed $amount_addons
     */
    public function setAmount_addons($amount_addons)
    {
        $this->amount_addons = $amount_addons;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }
    /**
     * @return mixed
     */
    public function getId_specialist()
    {
        return $this->id_specialist;
    }

    /**
     * @param mixed $id_specialist
     */
    public function setId_specialist($id_specialist)
    {
        $this->id_specialist = $id_specialist;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
    /**
     * @return mixed
     */
    public function getAmount_paid()
    {
        return $this->amount_paid;
    }

    /**
     * @param mixed $amount_paid
     */
    public function setAmount_paid($amount_paid)
    {
        $this->amount_paid = $amount_paid;
    }

/**
     * @return mixed
     */
    public function getDiscount_amount()
    {
        return $this->discount_amount;
    }

    /**
     * @param mixed $discount_amount
     */
    public function setDiscount_amount($discount_amount)
    {
        $this->discount_amount = $discount_amount;
    }

    /**
     * @return mixed
     */
    public function getDiscount_code()
    {
        return $this->discount_code;
    }

    /**
     * @param mixed $discount_code
     */
    public function setDiscount_code($discount_code)
    {
        $this->discount_code = $discount_code;
    }

    
	
	
}
?>

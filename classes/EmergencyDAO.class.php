<?php
include_once("BaseDAO.class.php");

class EmergencyDAO extends BaseDAO {

    public function  EmergencyDAO() {
    }
    
    public static function countEmergency() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_emergency";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	/**
	 * 
	 * @param number $id_parent
	 * @param number $page
	 * @param number $elementsPerPage
	 * @param string $sortfield
	 * @param string $sorttype
	 * @return Emergency[]
	 */
	public static function getEmergencyListByParent($id_parent, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
		$link = getConnection();
		
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_emergency WHERE id_parent=".mysql_real_escape_string($id_parent);

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by id_emergency asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		//echo("::::[$sql]::::");
		
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Emergency();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	
	public static function getEmergency($id_emergency) {
		$retorno = null;
		if(is_numeric($id_emergency)) {
			$sql = "SELECT * FROM pd_emergency where id_emergency=$id_emergency";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Emergency();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    


	public static function createEmergency($newEmergency) {
	    $retorno = false;
	    if (($newEmergency!=null)&&(is_numeric($newEmergency->getId_parent()))) {
	        //Save parent basic info
	        $link = getConnection();
	        
	        //$newChildren = new Children(); 
	        
	        $sql_frm = "INSERT INTO pd_emergency (id_parent, name, mail, phone, relation, idate) " .
	   	        "VALUES (".
	   	        mysql_real_escape_string($newEmergency->getId_parent()).", ".
	   	        "'".mysql_real_escape_string($newEmergency->getName())."', ".
	   	        "'".mysql_real_escape_string($newEmergency->getMail())."', ".
	   	        "'".mysql_real_escape_string($newEmergency->getPhone())."', ".
	   	        "'".mysql_real_escape_string($newEmergency->getRelation())."', ".
	   	        "now()".
	   	        ")";
	        
	        //echo("::::[$sql_frm]:::::"); die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;

	    }
	    return $retorno;
	}
	
	public static function updateEmergency($editEmergency) {
	    $retorno = null;
	    if (isset ($editEmergency) && (is_numeric($editEmergency->getId_emergency()))) {
	        
	        //Updating parent info
	        $link = getConnection();
	        
	        //$editEmergency= new Emergency();
	        
	        $sql_frm = "UPDATE pd_emergency SET name='" . $editEmergency->getName() . "', " .
	   	        "mail='" . mysql_real_escape_string($editEmergency->getMail()) . "', " .
	   	        "phone='" . mysql_real_escape_string($editEmergency->getPhone()) . "', " .
	   	        "relation='" . mysql_real_escape_string($editEmergency->getRelation()) . "', " .
	   	        "udate=now() " .
	   	        "WHERE id_emergency=" . $editEmergency->getId_emergency();
	        
	        //echo("::::::[$sql_frm]:::::::");
	        //die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = EmergencyDAO::getEmergency($editEmergency->getId_emergency());
	    }
	    
	    return $retorno;
	}
	
	
}
?>
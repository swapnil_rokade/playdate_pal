<?php
include_once("BaseDAO.class.php");

class ChildrenDAO extends BaseDAO {

    public function  ChildrenDAO() {
    }
    
    public static function countChildren() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_children";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	public static function getChildrenListByParent($id_parent, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		$link = getConnection();
		
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_children WHERE id_parent=".mysql_real_escape_string($id_parent);

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by id_children asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Children();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	public static function getChildrenListByRequest($id_request, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $link = getConnection();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT c.* FROM pd_children c, pd_request_children r WHERE c.id_children=r.id_children AND r.id_request=".$id_request;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_children asc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Children();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 * 
	 * @param number $id_children
	 * @return Children
	 */
	public static function getChildren($id_children) {
		$retorno = null;
		if(is_numeric($id_children)) {
			$sql = "SELECT * FROM pd_children where id_children=$id_children";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Children();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    


	public static function createChildren($newChildren) {
	    $retorno = false;
	    if (($newChildren!=null)&&(is_numeric($newChildren->getId_parent()))) {
	        //Save parent basic info
	        $link = getConnection();
	        
	        //$newChildren = new Children(); 
	        
	        $sql_frm = "INSERT INTO pd_children (id_parent, name, age, birthdate, notes, genre, idate) " .
	   	        "VALUES (".
	   	        mysql_real_escape_string($newChildren->getId_parent()).", ".
	   	        "'".mysql_real_escape_string($newChildren->getName())."', ".
	   	        "'".mysql_real_escape_string($newChildren->getAge())."', ".
	   	        "'".mysql_real_escape_string($newChildren->getBirthdate())."', ".
	   	        "'".mysql_real_escape_string($newChildren->getNotes())."', ".
	   	        "'".mysql_real_escape_string($newChildren->getGenre())."', ".
	   	        "now()".
	   	        ")";
	        
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;

	    }
	    return $retorno;
	}
	
	public static function updateChildren($editChildren) {
	    $retorno = null;
	    if (isset ($editChildren) && (is_numeric($editChildren->getId_children()))) {
	        
	        //Updating parent info
	        $link = getConnection();
	        
	        //$editChildren= new Children();
	        
	        $sql_frm = "UPDATE pd_children SET name='" . $editChildren->getName() . "', " .
	   	        "age='" . mysql_real_escape_string($editChildren->getAge()) . "', " .
	   	        "birthdate='" . mysql_real_escape_string($editChildren->getBirthdate()) . "', " .
	   	        "notes='" . mysql_real_escape_string($editChildren->getNotes()) . "', " .
	   	        "genre='" . mysql_real_escape_string($editChildren->getGenre()) . "', " .
	   	        "udate=now() " .
	   	        "WHERE id_children=" . $editChildren->getId_children();
	        
	        //echo("::::::[$sql_frm]:::::::");
	        //die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = ChildrenDAO::getChildren($editChildren->getId_children());
	    }
	    
	    return $retorno;
	}
	
	/**
	 * Delete children
	 * @param number $id_children
	 * @return boolean
	 */
	public static function deleteChildren($id_children) {
	    $retorno = false;
	    
	    if (($id_children!=null) && (is_numeric($id_children))) {
	        //Remove children
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_children WHERE id_children=" . $id_children;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
}
?>
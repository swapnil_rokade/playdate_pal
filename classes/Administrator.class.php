<?php
include_once("BaseEntity.class.php");

class Administrator extends BaseEntity {
    
    protected $id_administrator, $name, $lastname, $username, $email, $password, $picture, $lastlogin, $idate, $udate;
   
   	public function readFromRow($row){
	    if(isset($row['id_administrator'])) { $this->setId_administrator($row['id_administrator']);}
	    if(isset($row['name'])) { $this->setName($row['name']);}
	    if(isset($row['lastname'])) { $this->setLastname($row['lastname']);}
	    if(isset($row['username'])) { $this->setUsername($row['username']);}
	    if(isset($row['email'])) { $this->setEmail($row['email']);}
	    if(isset($row['password'])) { $this->setPassword($row['password']);}
	    if(isset($row['picture'])) { $this->setPicture($row['picture']);}
	    if(isset($row['lastlogin']) && ($row['lastlogin']!="0000-00-00 00:00:00")) $this->setLastlogin($this->toStringDateFormat($row['lastlogin']));
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
	/**
	 * Read full specialist name
	 */
	public function getFullName() {
	    $fullname = $this->getName();
	    
	    if($this->getLastname()!="") {
	        $fullname.= " ".$this->getLastname();
	    }
	    
	    return $fullname;
	    
	}
    /**
     * @return mixed
     */
    public function getId_administrator()
    {
        return $this->id_administrator;
    }

    /**
     * @param mixed $id_administrator
     */
    public function setId_administrator($id_administrator)
    {
        $this->id_administrator = $id_administrator;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getLastlogin()
    {
        return $this->lastlogin;
    }

    /**
     * @param mixed $lastlogin
     */
    public function setLastlogin($lastlogin)
    {
        $this->lastlogin = $lastlogin;
    }

    /**
     * @return Mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Mixed $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return Mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }


   
		
} 
?>
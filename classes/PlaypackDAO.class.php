<?php
include_once("BaseDAO.class.php");

class PlaypackDAO extends BaseDAO {

    public function  PlaypackDAO() {
    }
    
	public static function getPlaypackList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_playpacks";

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by amount asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Playpack();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	
	public static function getPlaypackListByType($type, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_playpacks WHERE type=".$type;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by amount asc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    //echo(":::[$sql]:::"); die();
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playpack();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function getPlaypack($id_playpack) {
	    $retorno = null;
	    if(is_numeric($id_playpack)) {
	        $sql = "SELECT * FROM pd_playpacks where id_playpack=$id_playpack";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Playpack();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	/**
	 * 
	 * @param Playpack $newPlaypack
	 * @return NULL|Playpack
	 */
	public static function createPlaypack($newPlaypack) {
	    $retorno = null;
	    if($newPlaypack->getName()!=null) {
	        $link1 = getConnection();
	        
	        $sql_frm = "INSERT INTO pd_playpacks (name, type, help, description, amount, icon, idate) " .
	   	        "VALUES (".
	   	        "'".mysql_real_escape_string($newPlaypack->getName())."', ".
	   	        "".(($newPlaypack->getType()!=null)?mysql_real_escape_string($newPlaypack->getType()):Playpack::$TYPE_1CHILD).", ".
	   	        "".(($newPlaypack->getHelp()!=null)?("'".mysql_real_escape_string($newPlaypack->getHelp())."'"):"null").", ".
	   	        "".(($newPlaypack->getDescription()!=null)?("'".mysql_real_escape_string($newPlaypack->getDescription())."'"):"null").", ".
	   	        "".(($newPlaypack->getAmount()!=null)?mysql_real_escape_string($newPlaypack->getAmount()):"0").", ".
	   	        "".(($newPlaypack->getIcon()!=null)?("'".mysql_real_escape_string($newPlaypack->getIcon())."'"):("'".Playpack::$ICON_LITTLE_PACK."'")).", ".
	   	        "now()".
	   	        ")";
	        
	        //echo("::[$sql_frm]:::"); die();
	        
	        $result = mysql_query($sql_frm, $link1);
	        mysql_close($link1);
	        
	        
	        //Obtenemos el id de ese item
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM pd_playpacks WHERE name='".mysql_real_escape_string($newPlaypack->getName())."' order by id_playpack desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        
	        $newPlaypack->readFromRow(mysql_fetch_assoc($result_frm));
	        
	        if(is_numeric($newPlaypack->getId_playpack()) && ($newPlaypack->getId_playpack()>0)) {
	            $retorno = $newPlaypack;
	        }
	        
	        
	    }
	    return $retorno;
	}
	
	
	/**
	 * 
	 * @param mixed id_playpack
	 * @return boolean
	 */
	public static function deletePlaypack($id_playpack) {
	    $retorno = false;
	    
	    if (($id_playpack!=null) && (is_numeric($id_playpack))) {
	        //Remove info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_playpacks WHERE id_playpack=" . $id_playpack;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
	/**
	 *
	 * @param Playpack $editPlaypack
	 * @return NULL|array|Playdate
	 */
	public static function updatePlaypack($editPlaypack) {
	    $retorno = null;
	    if (($editPlaypack!=null) && (is_numeric($editPlaypack->getId_playpack()))) {
	        
	        //Updating info
	        $link = getConnection();
	        
	        //$editPlaydate = new Playdate();
	        
	        $sql_frm = "UPDATE pd_playpacks SET name='" . $editPlaypack->getName() . "', " .
	   	        "type=" .(($editPlaypack->getType()!=null)?(mysql_real_escape_string($editPlaypack->getType())):Playpack::$TYPE_1CHILD). ", " .
	   	        "help=" .(($editPlaypack->getHelp()!=null)?("'".mysql_real_escape_string($editPlaypack->getHelp())."'"):"null"). ", " .
	   	        "description=" .(($editPlaypack->getDescription()!=null)?("'".mysql_real_escape_string($editPlaypack->getDescription())."'"):"null"). ", " .
	   	        "amount=" .(($editPlaypack->getAmount()!=null)?(mysql_real_escape_string($editPlaypack->getAmount())):0). ", " .
	   	        "icon=" .(($editPlaypack->getIcon()!=null)?("'".mysql_real_escape_string($editPlaypack->getIcon())."'"):("'".Playpack::$ICON_LITTLE_PACK."'")). ", " .
	   	        "udate=now() " .
	   	        "WHERE id_playpack=" . $editPlaypack->getId_playpack();
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = PlaypackDAO::getPlaypack($editPlaypack->getId_playpack());
	    }
	    
	    return $retorno;
	}
	
	
}
?>
<?php
include_once("BaseEntity.class.php");

class Group extends BaseEntity {
	
    public static $PRIVACITY_PUBLIC = 0;
    public static $PRIVACITY_PRIVATE = 1;
    
    
    protected $id_group, $name, $zipcode, $description, $picture, $privacity, $slug, $idate, $udate;
    
    /**
     * Constructor
     */
    public function __construct( $id_group=-1) {
        $this->id_group = $id_group;
    }
    
    
	public function readFromRow($row){
		if(isset($row['id_group'])) $this->setId_group($row['id_group']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['zipcode'])) $this->setZipcode($row['zipcode']);
		if(isset($row['description'])) $this->setDescription(stripslashes($row['description']));
		if(isset($row['picture'])) $this->setPicture(stripslashes($row['picture']));
		if(isset($row['privacity'])) $this->setPrivacity($row['privacity']);
		if(isset($row['slug'])) $this->setSlug($row['slug']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_group()
    {
        return $this->id_group;
    }

    /**
     * @param mixed $id_group
     */
    public function setId_group($id_group)
    {
        $this->id_group = $id_group;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getDescription($maxchars=null)
    {
        if($maxchars==null) {
            return $this->description;
        } else {
            return $this->truncateString($this->description, $maxchars);
        }
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getPrivacity()
    {
        return $this->privacity;
    }

    /**
     * @param mixed $status
     */
    public function setPrivacity($privacity)
    {
        $this->privacity = $privacity;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
	
}
?>
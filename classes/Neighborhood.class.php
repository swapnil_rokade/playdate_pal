<?php
include_once("BaseEntity.class.php");

class Neighborhood extends BaseEntity {
	
    protected $id_neighborhood, $id_city, $name, $idate, $udate;
    
    /**
    * Constructor
    */ 
    public function __construct( $id_neighborhood=-1) {
        $this->id_neighborhood = $id_neighborhood;
    }
   
    
	public function readFromRow($row){
	    if(isset($row['id_neighborhood'])) $this->setId_neighborhood($row['id_neighborhood']);
		if(isset($row['id_city'])) $this->setId_city($row['id_city']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_city()
    {
        return $this->id_city;
    }

    /**
     * @param mixed $id_city
     */
    public function setId_city($id_city)
    {
        $this->id_city = $id_city;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return Mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getId_neighborhood()
    {
        return $this->id_neighborhood;
    }

    /**
     * @param mixed $id_neighborhood
     */
    public function setId_neighborhood($id_neighborhood)
    {
        $this->id_neighborhood = $id_neighborhood;
    }

	
}
?>
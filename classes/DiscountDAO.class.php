<?php
include_once("BaseDAO.class.php");

class DiscountDAO extends BaseDAO {

    public function  DiscountDAO() {
    }
    
	public static function getDiscountTypeList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM discount_types";

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Discount();
			$newItem->readTypeFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}   

    public static function getDiscountUsageList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT dd.discount_code,dd.idate,pp.name,pp.lastname FROM discount_details dd
                LEFT JOIN pd_parents pp 
                ON pp.id_parent = dd.user_id";

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Discount();
			$newItem->readUsageFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	} 

    public static function getDiscountList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM discounts";

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Discount();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	} 

	
	public static function getPlaypackListByType($type, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_playpacks WHERE type=".$type;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by amount asc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    //echo(":::[$sql]:::"); die();
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Playpack();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function getDiscountType($id_discount_type) {
	    $retorno = null;
	    if(is_numeric($id_discount_type)) {
	        $sql = "SELECT * FROM discount_types where id_discount_type=$id_discount_type";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Discount();
	            $newItem->readTypeFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
    public static function getDiscountDetails($discount_code,$user_id) {
	    $retorno = null;
	    if(isset($discount_code)) {
	        $sql = "SELECT * FROM discount_details WHERE discount_code='$discount_code' AND user_id = $user_id";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Discount();
	            $newItem->readDetailsRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}

    public static function getDiscount($id_discount) {
	    $retorno = null;
	    if(is_numeric($id_discount)) {
	        $sql = "SELECT * FROM discounts where id_discount=$id_discount";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Discount();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
    
    public static function getDiscountByCode($discount_code) {
	    $retorno = null;
	    if(isset($discount_code)) {
	        $sql = "SELECT * FROM discounts where discount_code='".$discount_code."' AND (end_date = 0000-00-00 OR end_date > now())order by id_discount desc limit 1";
            
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Discount();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}

    /**
	 * 
	 * @param Discount Type $newDiscount
	 * @return NULL|Discount Type
	 */
	public static function createDiscount($newDiscount) {
	    $retorno = null;
	    if($newDiscount->getName()!=null) {
	        $link1 = getConnection();
	        
	        $sql_frm = "INSERT INTO discounts (discount_code, name, start_date, end_date, type, description, idate) " .
	   	        "VALUES (".
	   	        "'".mysql_real_escape_string($newDiscount->getdiscountCode())."', ".
                "".(($newDiscount->getName()!=null)?("'".mysql_real_escape_string($newDiscount->getName())."'"):"null").", ".
                "'".BaseDAO::toMysqlDateFormat($newDiscount->getStartDate())."', ".
                "'".BaseDAO::toMysqlDateFormat($newDiscount->getEndDate())."', ".
	   	        "".(($newDiscount->getType()!=null)?("'".mysql_real_escape_string($newDiscount->getType())."'"):"null").", ".
                "".(($newDiscount->getDescription()!=null)?("'".mysql_real_escape_string($newDiscount->getDescription())."'"):"null").", ".
	   	        "now()".
	   	        ")";
	        
	        
	        
	        $result = mysql_query($sql_frm, $link1);
	        mysql_close($link1);
	        
	        
	        //We get the id of that item
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM discounts WHERE discount_code='".mysql_real_escape_string($newDiscount->getdiscountCode())."' order by id_discount desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
 
	        $newDiscount->readFromRow(mysql_fetch_assoc($result_frm));
	        if(is_numeric($newDiscount->getId_discount()) && ($newDiscount->getId_discount()>0)) {
	            $retorno = $newDiscount;
	        }
	        
	        
	    }
	    return $retorno;
	}

	/**
	 * 
	 * @param Discount Type $newDiscountType
	 * @return NULL|Discount Type
	 */
	public static function createDiscountType($newDiscountType) {
	    $retorno = null;
	    if($newDiscountType->getName()!=null) {
	        $link1 = getConnection();
	        
	        $sql_frm = "INSERT INTO discount_types (name, type, discount, status, idate) " .
	   	        "VALUES (".
	   	        "'".mysql_real_escape_string($newDiscountType->getName())."', ".
	   	        "".(($newDiscountType->getType()!=null)?("'".mysql_real_escape_string($newDiscountType->getType())."'"):"null").", ".
	   	        "".(($newDiscountType->getDiscount()!=null)?("'".mysql_real_escape_string($newDiscountType->getDiscount())."'"):"null").", ".
	   	        "".("1").", ".
	   	        "now()".
	   	        ")";
	        
	        //echo("::[$sql_frm]:::"); die();
	        
	        $result = mysql_query($sql_frm, $link1);
	        mysql_close($link1);
	        
	        
	        //We get the id of that item
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM discount_types WHERE name='".mysql_real_escape_string($newDiscountType->getName())."' order by id_discount_type desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        
	        $newDiscountType->readTypeFromRow(mysql_fetch_assoc($result_frm));
	        if(is_numeric($newDiscountType->getId_discount_type()) && ($newDiscountType->getId_discount_type()>0)) {
	            $retorno = $newDiscountType;
	        }
	        
	        
	    }
	    return $retorno;
	}
	
	
	/**
	 * 
	 * @param mixed id_discount_type
	 * @return boolean
	 */
	public static function deleteDiscountType($id_discount_type) {
	    $retorno = false;
	    
	    if (($id_discount_type!=null) && (is_numeric($id_discount_type))) {
	        //Remove info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM discount_types WHERE id_discount_type=" . $id_discount_type;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
    /**
	 * 
	 * @param mixed id_discount
	 * @return boolean
	 */
	public static function deleteDiscount($id_discount) {
	    $retorno = false;
	    
	    if (($id_discount!=null) && (is_numeric($id_discount))) {
	        //Remove info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM discounts WHERE id_discount=" . $id_discount;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param Discount Type $editDiscountType
	 * @return NULL|array|Discount Type
	 */
	public static function updateDiscountType($updDiscountType) {
	    $retorno = null;
	    if (($updDiscountType!=null) && (is_numeric($updDiscountType->getId_discount_type()))) {
	        
	        //Updating info
	        $link = getConnection();
	        
	        //$editPlaydate = new Playdate();
	        
	        $sql_frm = "UPDATE discount_types SET name='" . $updDiscountType->getName() . "', " .
	   	        "type='" .(($updDiscountType->getType()!=null)?(mysql_real_escape_string($updDiscountType->getType())):"null"). "', " .
	   	        "discount=" .(($updDiscountType->getDiscount()!=null)?("'".mysql_real_escape_string($updDiscountType->getDiscount())."'"):"null"). ", " .
	   	        "udate=now() " .
	   	        "WHERE id_discount_type=" . $updDiscountType->getId_discount_type();
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = DiscountDAO::getDiscountType($updDiscountType->getId_discount_type());
	    }
	    
	    return $retorno;
	}
	
    /**
	 *
	 * @param Discount Type $updDiscount
	 * @return NULL|array|Discount
	 */
	public static function updateDiscount($updDiscount) {
	    $retorno = null;
	    if (($updDiscount!=null) && (is_numeric($updDiscount->getId_discount()))) {
	        
	        //Updating info
	        $link = getConnection();
	        
	        //$editPlaydate = new Playdate();
    	        

	        $sql_frm = "UPDATE discounts SET discount_code='" . $updDiscount->getdiscountCode() . "', " .
	   	        "name='" .(($updDiscount->getName()!=null)?(mysql_real_escape_string($updDiscount->getName())):"null"). "', " .
                "start_date='" .(($updDiscount->getStartDate()!=null)?(BaseDAO::toMysqlDateFormat($updDiscount->getStartDate())):"null"). "', " .
                "end_date='" .(($updDiscount->getEndDate()!=null)?(BaseDAO::toMysqlDateFormat($updDiscount->getEndDate())):"null"). "', " .
	   	        "type=" .(($updDiscount->getType()!=null)?("'".mysql_real_escape_string($updDiscount->getType())."'"):"null"). ", " .
                "description=" .(($updDiscount->getDescription()!=null)?("'".mysql_real_escape_string($updDiscount->getDescription())."'"):"null"). ", " .
	   	        "udate=now() " .
	   	        "WHERE id_discount=" . $updDiscount->getId_discount();
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = DiscountDAO::getDiscount($updDiscount->getId_discount());
	    }
	    
	    return $retorno;
	}

	/**
	 * 
	 * @param Discount Detail $newDiscount
	 * @return NULL|Discount Detail
	 */
	public static function createDiscountDetails($discountDetails) {
	    $retorno = null;
	    if($discountDetails->getdiscountCode()!=null) {
	        $link1 = getConnection();
	        
	        $sql_frm = "INSERT INTO discount_details (discount_code, user_id, user_type, idate) " .
	   	        "VALUES (".
	   	        "'".mysql_real_escape_string($discountDetails->getdiscountCode())."', ".
                "".mysql_real_escape_string($discountDetails->get_userId()).", ".
	   	        "'Parent'".
                "now()".
	   	        ")";
	        
	        $result = mysql_query($sql_frm, $link1);
	        mysql_close($link1);
	    }
	    return $retorno;
	}
	
}
?>

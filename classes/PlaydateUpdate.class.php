<?php
include_once("BaseEntity.class.php");

class PlaydateUpdate extends BaseEntity {
	
    protected $id_update, $id_playdate, $content, $idate, $udate;
    

    /**
    * Constructor
    */ 
    public function __construct( $id_update=-1) {
        $this->id_update = $id_update;
    }
   
    
	public function readFromRow($row){
		if(isset($row['id_update'])) $this->setId_update($row['id_update']);
		if(isset($row['id_playdate'])) $this->setId_playdate($row['id_playdate']);
		if(isset($row['content'])) $this->setContent($row['content']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}

    /**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getId_update()
    {
        return $this->id_update;
    }

    /**
     * @param mixed $id_update
     */
    public function setId_update($id_update)
    {
        $this->id_update = $id_update;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }


    
}
?>
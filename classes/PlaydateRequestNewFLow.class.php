<?php
include_once("BaseEntity.class.php");

class PlaydateRequest extends BaseEntity {
    
    public static $STATUS_REQUEST_OPEN=0;
    public static $STATUS_REQUEST_CLOSED=1;
    
    protected $id_request, $id_parent, $playdate_type, $date, $time, $group_size, $age_range, $no_of_playdate_per_week, $which_day_of_week, $location_in_mind, $preferred_location, $preferred_location_type, $theme_of_playdate, $pay_extra_bucks_for, $notes, $like_to_invite_guest,$information_of_guests, $playdate_lead_by_specialist,
    $specialist_first_name,$specialist_last_name,$other_details,$how_did_you_hear_about_us,$first_name,$last_name,$email_address,$status;
    

    /**
    * Constructor
    */ 
    public function __construct( $id_request=-1) {
        $this->id_request = $id_request;
    }
   
	public function readFromRow($row){
	    if(isset($row['id_request'])) { $this->setId_request($row['id_request']); }
	    if(isset($row['id_parent'])) { $this->setId_parent($row['id_parent']); }
	    if(isset($row['playdate_type'])) { $this->setPlaydate_type($row['playdate_type']); }
	    if(isset($row['date'])) { $this->setDate($row['date']);}
	    if(isset($row['time'])) { $this->setTime($row['time']);}
	    if(isset($row['group_size'])) { $this->setGroup_size($row['group_size']);}
	    if(isset($row['age_range'])) { $this->setAge_range($row['age_range']);}
	    if(isset($row['no_of_playdate_per_week'])) { $this->setNo_of_playdate($row['no_of_playdate_per_week']);}
	    if(isset($row['which_day_of_week'])) { $this->setWhich_day_week($row['which_day_of_week']);}
	    if(isset($row['location_in_mind'])) { $this->setLocation_in_mind($row['location_in_mind']);}
	    if(isset($row['preferred_location'])) { $this->setPreferred_location($row['preferred_location']);}
	    if(isset($row['preferred_location_type'])) { $this->setPreferred_location_type($row['preferred_location_type']);}
	    if(isset($row['theme_of_playdate'])) { $this->setTheme_of_playdate($row['theme_of_playdate']);}
	    if(isset($row['pay_extra_bucks_for'])) { $this->setPay_extra($row['pay_extra_bucks_for']);}
	    if(isset($row['notes'])) { $this->setNotes($row['notes']);}
        if(isset($row['like_to_invite_guest'])) { $this->setLike_to_invite($row['like_to_invite_guest']);}
        if(isset($row['information_of_guests'])) { $this->setInformation_of_guest($row['information_of_guests']);}
        if(isset($row['playdate_lead_by_specialist'])) { $this->setPlaydate_lead_by_specialist($row['playdate_lead_by_specialist']);}
        if(isset($row['specialist_first_name'])) { $this->setSpecialist_first_name($row['specialist_first_name']);}
        if(isset($row['specialist_last_name'])) { $this->setSpecialist_last_name($row['specialist_last_name']);}
        if(isset($row['other_details'])) { $this->setOther_details($row['other_details']);}
        if(isset($row['how_did_you_hear_about_us'])) { $this->setHear_about_us($row['how_did_you_hear_about_us']);}
        if(isset($row['first_name'])) { $this->setFirst_name($row['first_name']);}
        if(isset($row['last_name'])) { $this->setLast_name($row['last_name']);}
        if(isset($row['email_address'])) { $this->setLast_name($row['email_address']);}
        if(isset($row['status'])) { $this->setStatus(1);}
		//$this->creation_date= date('Y-m-d H:i:s');
		//$this->updation_date= date('Y-m-d H:i:s');
	}
    /**
     * @return mixed
     */
    public function getId_request()
    {
        return $this->id_request;
    }

    /**
     * @param mixed $id_request
     */
    public function setId_request($id_request)
    {
        $this->id_request = $id_request;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getGroup_size()
    {
        return $this->group_size;
    }

    /**
     * @param mixed $group_size
     */
    public function setGroup_size($group_size)
    {
        $this->group_size = $group_size;
    }

    /**
     * @return mixed
     */
    public function getAge_range()
    {
        return $this->age_range;
    }

    /**
     * @param mixed $age_range
     */
    public function setAge_range($age_range)
    {
        $this->age_range = $age_range;
    }

    /**
     * @return mixed
     */
    public function getNo_of_playdate()
    {
        return $this->no_of_playdate_per_week;
    }

    /**
     * @param mixed $no_of_playdate_per_week
     */
    public function setNo_of_playdate($no_of_playdate_per_week)
    {
        $this->no_of_playdate_per_week = $no_of_playdate_per_week;
    }

    /**
     * @return mixed
     */
    public function getWhich_day_week()
    {
        return $this->which_day_of_week;
    }

    /**
     * @param mixed $which_day_of_week
     */
    public function setWhich_day_week($which_day_of_week)
    {
        $this->which_day_of_week = $which_day_of_week;
    }

    /**
     * @return mixed
     */
    public function getLocation_in_mind()
    {
        return $this->location_in_mind;
    }

    /**
     * @param mixed $location_in_mind
     */
    public function setLocation_in_mind($location_in_mind)
    {
        $this->location_in_mind = $location_in_mind;
    }

    /**
     * @return mixed
     */
    public function getPreferred_location()
    {
        return $this->preferred_location;
    }

    /**
     * @param mixed $preferred_location
     */
    public function setPreferred_location($preferred_location)
    {
        $this->preferred_location = $preferred_location;
    }

    /**
     * @return mixed
     */
    public function getPreferred_location_type()
    {
        return $this->preferred_location_type;
    }

    /**
     * @param mixed $preferred_location_type
     */
    public function setPreferred_location_type($preferred_location_type)
    {
        $this->preferred_location_type = $preferred_location_type;
    }

    /**
     * @return mixed
     */
    public function getTheme_of_playdate()
    {
        return $this->theme_of_playdate;
    }

    /**
     * @param mixed $theme_of_playdate
     */
    public function setTheme_of_playdate($theme_of_playdate)
    {
        $this->theme_of_playdate = $theme_of_playdate;
    }

    /**
     * @return mixed
     */
    public function getPay_extra()
    {
        return $this->pay_extra_bucks_for;
    }

    /**
     * @param mixed $pay_extra_bucks_for
     */
    public function setPay_extra($pay_extra_bucks_for)
    {
        $this->pay_extra_bucks_for = $pay_extra_bucks_for;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function getLike_to_invite()
    {
        return $this->like_to_invite_guest;
    }

    /**
     * @param mixed $like_to_invite_guest
     */
    public function setLike_to_invite($like_to_invite_guest)
    {
        $this->like_to_invite_guest = $like_to_invite_guest;
    }

    /**
     * @return mixed
     */
    public function getInformation_of_guest()
    {
        return $this->information_of_guests;
    }

    /**
     * @param mixed $information_of_guests
     */
    public function setInformation_of_guest($information_of_guests)
    {
        $this->information_of_guests = $information_of_guests;
    }

    /**
     * @return mixed
     */
    public function getPlaydate_lead_by_specialist()
    {
        return $this->playdate_lead_by_specialist;
    }

    /**
     * @param mixed $playdate_lead_by_specialist
     */
    public function setPlaydate_lead_by_specialist($playdate_lead_by_specialist)
    {
        $this->playdate_lead_by_specialist = $playdate_lead_by_specialist;
    }
    /**
     * @return mixed
     */
    public function getSpecialist_first_name()
    {
        return $this->specialist_first_name;
    }

    /**
     * @param mixed $specialist_first_name
     */
    public function setSpecialist_first_name($specialist_first_name)
    {
        $this->specialist_first_name = $specialist_first_name;
    }
    /**
     * @return mixed
     */
    public function getSpecialist_last_name()
    {
        return $this->specialist_last_name;
    }

    /**
     * @param mixed $specialist_last_name
     */
    public function setSpecialist_last_name($specialist_last_name)
    {
        $this->specialist_last_name = $specialist_last_name;
    }
    /**
     * @return mixed
     */
    public function getOther_details()
    {
        return $this->other_details;
    }

    /**
     * @param mixed $other_details
     */
    public function setOther_details($other_details)
    {
        $this->other_details = $other_details;
    }
    /**
     * @return mixed
     */
    public function getHear_about_us()
    {
        return $this->how_did_you_hear_about_us;
    }

    /**
     * @param mixed $how_did_you_hear_about_us
     */
    public function setHear_about_us($how_did_you_hear_about_us)
    {
        $this->how_did_you_hear_about_us = $how_did_you_hear_about_us;
    }
    /**
     * @return mixed
     */
    public function getFirst_name()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirst_name($first_name)
    {
        $this->first_name = $first_name;
    }
    /**
     * @return mixed
     */
    public function getLast_name()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLast_name($last_name)
    {
        $this->last_name = $last_name;
    }
    /**
     * @return mixed
     */
    public function getEmail_address()
    {
        return $this->email_address;
    }

    /**
     * @param mixed $email_address
     */
    public function setEmail_address($email_address)
    {
        $this->email_address = $email_address;
    }
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function getPlaydate_type()
    {
        return $this->playdate_type;
    }

    /**
     * @param mixed $playdate_type
     */
    public function setPlaydate_type($playdate_type)
    {
        $this->playdate_type = $playdate_type;
    }



	
	
} 
?>
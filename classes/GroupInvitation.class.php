<?php
include_once("BaseEntity.class.php");


/**
 * All group info, and invitation info (id_parent - id_specialist, invitation status)
 */
class GroupInvitation extends BaseEntity {
	
    public static $PRIVACITY_PUBLIC = 0;
    public static $PRIVACITY_PRIVATE = 1;
    
    public static $STATUS_SENT = 0;
    public static $STATUS_ACCEPTED = 1;
    public static $STATUS_REJECTED = 2;
    
    
    protected $id_invitation, $id_group, $name, $zipcode, $description, $picture, $privacity, $slug, $idate, $udate, $id_parent, $id_specialist, $status;
    
    /**
     * Constructor
     */
    public function __construct( $id_invitation=-1) {
        $this->id_invitation = $id_invitation;
    }
    
    
	public function readFromRow($row){
	    if(isset($row['id_invitation'])) $this->setId_invitation($row['id_invitation']);
	    if(isset($row['id_group'])) $this->setId_group($row['id_group']);
	    if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
	    if(isset($row['id_specialist'])) $this->setId_specialist($row['id_specialist']);
	    if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['zipcode'])) $this->setZipcode($row['zipcode']);
		if(isset($row['description'])) $this->setDescription(stripslashes($row['description']));
		if(isset($row['picture'])) $this->setPicture(stripslashes($row['picture']));
		if(isset($row['privacity'])) $this->setPrivacity($row['privacity']);
		if(isset($row['status'])) $this->setStatus($row['status']);
		if(isset($row['slug'])) $this->setSlug($row['slug']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_group()
    {
        return $this->id_group;
    }

    /**
     * @param mixed $id_group
     */
    public function setId_group($id_group)
    {
        $this->id_group = $id_group;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getDescription($maxchars=10000)
    {
        return $this->truncateString($this->description, $maxchars);
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getPrivacity()
    {
        return $this->privacity;
    }

    /**
     * @param mixed $status
     */
    public function setPrivacity($privacity)
    {
        $this->privacity = $privacity;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function getId_invitation()
    {
        return $this->id_invitation;
    }

    /**
     * @param mixed $id_request
     */
    public function setId_invitation($id_invitation)
    {
        $this->id_invitation = $id_invitation;
    }
    /**
     * @return mixed
     */
    public function getId_specialist()
    {
        return $this->id_specialist;
    }

    /**
     * @param mixed $id_specialist
     */
    public function setId_specialist($id_specialist)
    {
        $this->id_specialist = $id_specialist;
    }



	
}
?>
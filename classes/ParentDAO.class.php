<?php
include_once ("BaseDAO.class.php");

class ParentDAO extends BaseDAO {

    function ParentDAO() {
	}

	public static function isCompletedProfile($id_parent) {
	    $retorno = true;
	    $parent = ParentDAO::getParent($id_parent);
	     
	    //Must have phone
	    $retorno = ($retorno && ($parent->getCellphone()!=null));
	    
	    //Must have kids
	    $kids = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
	    $retorno = ($retorno && (count($kids)>0));
	    
	    $emerg = EmergencyDAO::getEmergencyListByParent($parent->getId_parent());
	    $retorno = ($retorno && (count($emerg)>0));
	    
	    return $retorno;
	}
	
	public static function countParents() {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_parents", $link) or die("Error counting Parents");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function countParentsSearch($query) {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_parents WHERE name like '%$query%' or lastname like '%$query%' or email like '%$query%'", $link) or die("Error counting parents");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function getParentsList($page=1, $elemsPerPage=1000, $sortfield=null, $sorttype=null) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT * FROM pd_parents ";
		//Ordenacion
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
		    $sql .= " order by id_parent DESC";
		    
		}

		//Start and range
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first
		$max = $elemsPerPage; # elements per page

		//paging
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
		    $newUser = new ParentPd(-1);
			$newUser->readFromRow($row);
			$retorno[] = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}

	/**
	 * Parents with playdate reservation
	 * @param mixed $id_playdate
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return ParentPd[]
	 */
	public static function getParentsListByPlaydate($id_playdate, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_parents p, pd_playdate_parent_reservation r WHERE p.id_parent=r.id_parent AND r.id_playdate= ".$id_playdate;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.name ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new ParentPd(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * Parents with playdate reservation
	 * @param mixed $id_playdate
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return ParentPd[]
	 */
	public static function getParentsListByGroup($id_group, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_parents p, pd_group_parents r WHERE p.id_parent=r.id_parent AND r.id_group= ".$id_group;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.name ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new ParentPd(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * Parents with playdate reservation
	 * @param mixed $id_playdate
	 * @param mixed $page
	 * @param mixed $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return ParentPd[]
	 */
	public static function getParentsListByGroupAdmin($id_group, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT p.* FROM pd_parents p, pd_group_parents r WHERE r.is_admin=1 AND p.id_parent=r.id_parent AND r.id_group= ".$id_group;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.name ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new ParentPd(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getParentsListBySpecialist($id_specialist, $page=1, $elemsPerPage=20, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT distinct p.* FROM pd_playdate_parent_reservation r, pd_playdates pd, pd_parents p WHERE p.id_parent=r.id_parent AND r.id_playdate=pd.id_playdate AND pd.id_specialist=$id_specialist";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by p.name ASC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //echo(":::[$sql]::"); die();
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newUser = new ParentPd(-1);
	        $newUser->readFromRow($row);
	        $retorno[] = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function getParentsSearch($userSearch, $page, $elemsPerPage, $sortfield, $sorttype) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_parents s WHERE 1=1";
		
		//Adding query parameters
		/*
		$userSearch = new ParentPd();
		
		if(($userSearch->getPerfil()!=null)&&($userSearch->getPerfil()!="")) {
			$sql.=" AND perfil='".$userSearch->getPerfil()."'";
		}
		*/
		
		//order
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		}

		//paging values
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first element
		$max = $elemsPerPage; # num elements

		//paging limits
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
		    $newUser = new ParentPd(-1);
			$newUser->readFromRow($row);
			$retorno[] = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}
	

	public static function getParent($id_parent) {
		$retorno = array ();
		
		if(is_numeric($id_parent) && ($id_parent>=0)) {
			$link = getConnection();
	
			$sql = "SELECT s.* FROM pd_parents s WHERE id_parent=$id_parent";
	
			//reading results
			$result = mysql_query($sql, $link);
			if ($row = mysql_fetch_assoc($result)) {
			    $newUser = new ParentPd(-1);
				$newUser->readFromRow($row);
				$retorno = $newUser;
			}
			mysql_close($link);
		}
		return $retorno;
	}

	/*
	public static function getUsuarioByActivation($activateKey) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM fm_usuarios s WHERE activacion='$activateKey'";

		//Obtenemos los resultados
		$result = mysql_query($sql, $link);
		if ($row = mysql_fetch_assoc($result)) {
			$newUser = new Usuario(-1);
			$newUser->readFromRow($row);
			$retorno = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}
    */
	
	public static function getParentByEmailPassword($email, $password) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT s.* FROM pd_parents s WHERE email='".mysql_real_escape_string($email)."' and password=md5('".$password."')";
	    
	    //reading results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newUser = new ParentPd(-1);
	        $newUser->readFromRow($row);
	        $retorno = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getParentByUsernamePassword($username, $password) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT s.* FROM pd_parents s WHERE username='".mysql_real_escape_string($username)."' and password=md5('".$password."')";
	    
	    //reading results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newUser = new ParentPd(-1);
	        $newUser->readFromRow($row);
	        $retorno = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getParentByUsernameOrEmail($parentInfo) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT s.* FROM pd_parents s WHERE username='".mysql_real_escape_string($parentInfo)."' or email='".mysql_real_escape_string($parentInfo)."'";
	    
	    //reading results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newUser = new ParentPd(-1);
	        $newUser->readFromRow($row);
	        $retorno = $newUser;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	
	public static function getParentByEmail($email) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM fm_parent s WHERE email='$email'";

		//reading results
		$result = mysql_query($sql, $link);
		if ($row = mysql_fetch_assoc($result)) {
		    $newUser = new ParentPd(-1);
			$newUser->readFromRow($row);
			$retorno = $newUser;
		}
		mysql_close($link);
		return $retorno;
	}

	
	/**
	 * Create Parent - Basic register (more info when updating)
	 * @param ParentPd $newParent
	 * @return mixed
	 */
	public static function createParent($newParent) {
		$retorno = null;

		if (($newParent!=null) && ($newParent->getEmail() != "")) {
			//Save parent basic info
			$link = getConnection();

			$sql_frm = "INSERT INTO pd_parents (name, lastname, username, email, zipcode, password, idate) " .
			 			"VALUES (".
			 			"'".mysql_real_escape_string($newParent->getName())."', ".
			 			"'".mysql_real_escape_string($newParent->getLastname())."', ".
			 			"'".mysql_real_escape_string($newParent->getUsername())."', ".
			 			"'".mysql_real_escape_string($newParent->getEmail())."', ".
			 			"'".mysql_real_escape_string($newParent->getZipcode())."', ".
			 			"md5('".mysql_real_escape_string($newParent->getPassword())."'), ".
			 			"now()".
			 			")";
			
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			//Obtenemos el id del nuevo usuario
			$link = getConnection();
			$sql_frm = "SELECT * FROM pd_parents WHERE email='" . mysql_real_escape_string($newParent->getEmail()) . "' order by id_parent desc limit 1";
			$result_frm = mysql_query($sql_frm, $link);
			$newParent->readFromRow(mysql_fetch_assoc($result_frm));

			if (is_numeric($newParent->getId_parent()) && ($newParent->getId_parent()>0)) {
				$retorno = $newParent;
			}
			mysql_close($link);
		}
		return $retorno;
	}

	/**
	 * Create Parent - Basic register (more info when updating)
	 * @param ParentPd $newParent
	 * @return mixed
	 */
	public static function createNewParent($newParent) {
		$retorno = null;

		if (($newParent!=null) && ($newParent->getEmail() != "")) {
			//Save parent basic info
			$link = getConnection();

			$sql_frm = "INSERT INTO pd_parents (name, lastname, username, email, zipcode, password, idate) " .
			 			"VALUES (".
			 			"'".mysql_real_escape_string($newParent->getName())."', ".
			 			"'".mysql_real_escape_string($newParent->getLastname())."', ".
			 			"'".mysql_real_escape_string($newParent->getUsername())."', ".
			 			"'".mysql_real_escape_string($newParent->getEmail())."', ".
			 			"'".mysql_real_escape_string($newParent->getZipcode())."', ".
			 			"md5('".mysql_real_escape_string($newParent->getPassword())."'), ".
			 			"now()".
			 			")";
			
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			//Obtenemos el id del nuevo usuario
			$link = getConnection();
			$sql_frm = "SELECT * FROM pd_parents WHERE email='" . mysql_real_escape_string($newParent->getEmail()) . "' order by id_parent desc limit 1";
			$result_frm = mysql_query($sql_frm, $link);
			$newParent->readFromRow(mysql_fetch_assoc($result_frm));

			if (is_numeric($newParent->getId_parent()) && ($newParent->getId_parent()>0)) {
				$retorno = $newParent;
			}
			mysql_close($link);
		}
		return $retorno;
	}
	
	/**
	 * 
	 * @param ParentPd $editParent
	 * @return NULL|array|ParentPd
	 */
	public static function updateParent($editParent) {
	    $retorno = null;
		if (isset ($editParent) && (is_numeric($editParent->getId_parent()))) {
		    
			//Updating parent info
			$link = getConnection();			
			
			$sql_frm = "UPDATE pd_parents SET name='" . mysql_real_escape_string($editParent->getName()) . "', " .
			"lastname='" . mysql_real_escape_string($editParent->getLastname()) . "', " .
			"username='" . mysql_real_escape_string($editParent->getUsername()) . "', " .
			"email='" . mysql_real_escape_string($editParent->getEmail()) . "', " .
			"zipcode='" . mysql_real_escape_string($editParent->getZipcode()) . "', " .
			"cellphone='" . mysql_real_escape_string($editParent->getCellphone()) . "', " .
			"phone='" . mysql_real_escape_string($editParent->getPhone()) . "', ".
			"children_avb_mon_init=".(($editParent->getChildren_avb_mon_init()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_mon_init()) . "'"):"null").", " .
			"children_avb_mon_end=".(($editParent->getChildren_avb_mon_end()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_mon_end()) . "'"):"null").", " .
			"children_avb_tue_init=".(($editParent->getChildren_avb_tue_init()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_tue_init()) . "'"):"null").", " .
			"children_avb_tue_end=".(($editParent->getChildren_avb_tue_end()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_tue_end()) . "'"):"null").", " .
			"children_avb_wed_init=".(($editParent->getChildren_avb_wed_init()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_wed_init()) . "'"):"null").", " .
			"children_avb_wed_end=".(($editParent->getChildren_avb_wed_end()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_wed_end()) . "'"):"null").", " .
			"children_avb_thu_init=".(($editParent->getChildren_avb_thu_init()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_thu_init()) . "'"):"null").", " .
			"children_avb_thu_end=".(($editParent->getChildren_avb_thu_end()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_thu_end()) . "'"):"null").", " .
			"children_avb_fri_init=".(($editParent->getChildren_avb_fri_init()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_fri_init()) . "'"):"null").", " .
			"children_avb_fri_end=".(($editParent->getChildren_avb_fri_end()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_fri_end()) . "'"):"null").", " .
			"children_avb_sat_init=".(($editParent->getChildren_avb_sat_init()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_sat_init()) . "'"):"null").", " .
			"children_avb_sat_end=".(($editParent->getChildren_avb_sat_end()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_sat_end()) . "'"):"null").", " .
			"children_avb_sun_init=".(($editParent->getChildren_avb_sun_init()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_sun_init()) . "'"):"null").", " .
			"children_avb_sun_end=".(($editParent->getChildren_avb_sun_end()!=null)?("'" . mysql_real_escape_string($editParent->getChildren_avb_sun_end()) . "'"):"null").", " .
			"picture='" . mysql_real_escape_string($editParent->getPicture()) . "', " .
			"status='" . mysql_real_escape_string($editParent->getStatus()) . "', " .
			"profile=".(($editParent->getProfile()!=null)?$editParent->getProfile():"null").", " .
			"slug='" . mysql_real_escape_string($editParent->getSlug()) . "', udate=now() " .
			"WHERE id_parent=" . $editParent->getId_parent();
			
			//echo("::::::[$sql_frm]:::::::");
			//die();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = ParentDAO::getParent($editParent->getId_parent());
		}

		return $retorno;
	}

	
	/**
	 *
	 * @param ParentPd $editParent
	 * @return NULL|array|ParentPd
	 */
	public static function updateParentProfile($id_parent, $newProfile) {
	    $retorno = null;
	    if (is_numeric($editParent->getId_parent())) {
	        
	        //Updating parent info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_parents SET profile=".$newProfile.", udate=now() WHERE id_parent=" . $id_parent;
	        
	        //echo("::::::[$sql_frm]:::::::");
	        //die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = ParentDAO::getParent($id_parent);
	    }
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param ParentPd $editParent
	 * @return NULL|ParentPd
	 */
	public static function updateParentPassword($id_parent, $newPassword) {
	    $retorno = null;
	    if (($id_parent!=null) && (is_numeric($id_parent))) {
	        
	        //Updating parent info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_parents SET password=md5('". mysql_real_escape_string($newPassword) . "'), udate=now() WHERE id_parent=" . $id_parent;
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = ParentDAO::getParent($id_parent);
	    }
	    
	    return $retorno;
	}
	
	/**
	 * Update Parent Credit
	 * @param mixed $id_parent
	 * @param mixed $newCredit
	 * @return NULL|array|ParentPd
	 */
	public static function updateParentCredit($id_parent, $newCredit) {
	    $retorno = null;
	    
	    if (($id_parent!=null) && is_numeric($id_parent) && is_numeric($newCredit)) {
	        //Updating parent info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_parents SET credit=".$newCredit.", udate=now() WHERE id_parent=".$id_parent;
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = ParentDAO::getParent($id_parent);
	    }
	    
	    return $retorno;
	}
	
	public static function changePassword($editParent, $newpassword) {
		$retorno = null;
		if (isset ($editParent) && (is_numeric($editParent->getId_parent()))) {
			//Update password info
			$link = getConnection();
			$sql_frm = "UPDATE pd_parents SET password=md5('" . $newpassword . "') WHERE id_parent=" . $editParent->getId_parent();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = ParentDAO::getParent($editParent->getId_parent());
		}

		return $retorno;
	}

	
	/*
	public static function activateUsuario($editUsuario) {
		$retorno = null;
		if (isset ($editUsuario) && (is_numeric($editUsuario->getId_usuario())) && ($editUsuario->getId_usuario() > 0)) {
			//Almacenamos la información de usuario
			$link = getConnection();

			$sql_frm = "UPDATE fm_usuarios SET flg_activo=1, ufecha=now() WHERE id_usuario=".$editUsuario->getId_usuario();

			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = UsuarioDAO::getUsuario($editUsuario->getId_usuario());
		}

		return $retorno;
	}

    
	
	public static function activateUsuarioByKey($activateKey) {
		$retorno = null;

		$usuario=UsuarioDAO :: getUsuarioByActivation($activateKey);

		if (!is_null($usuario)) {
			$retorno = UsuarioDAO::activateUsuario($usuario);
		}

		return $retorno;
	}
	

	public static function deactivateUsuario($editUsuario) {
		$retorno = null;
		if (isset ($editUsuario) && ($editUsuario->getUsuario() != "") && (is_numeric($editUsuario->getId_usuario()))) {
			//Actualizamos la información de usuario
			$link = getConnection();

			$sql_frm = "UPDATE fm_usuarios SET flg_activo=0, ufecha=now() WHERE id_usuario=" . $editUsuario->getId_usuario();

			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = UsuarioDAO::getUsuario($editUsuario->getId_usuario());
		}

		return $retorno;
	}

    */
	public static function deleteParent($deleteParent) {
		$retorno = false;

		if (isset ($deleteParent) && (is_numeric($deleteParent->getId_parent()))) {
			//Remove specialist info
			$link = getConnection();
			$sql_frm = "DELETE FROM pd_parents WHERE id_parent=" . $deleteParent->getId_parent();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = true;
		}

		return $retorno;
	}

	
	public static function checkMail($parent) {
		//1 - Test if mail exists
		$retorno = new Response(); // OK if not found
		$link = getConnection();
		$sql = "SELECT count(1) FROM pd_parents WHERE email='" . $parent->getEmail() . "'";
		//Read results
		$result = mysql_query($sql, $link);
		list ($total) = mysql_fetch_row($result);
		mysql_close($link);

		if ($total > 0) {
			$retorno->setError_code(-5);
			$retorno->setDescription("This email address belongs to a parent");
		}

		return $retorno;
	}
}
?>
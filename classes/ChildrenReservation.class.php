<?php
include_once("BaseEntity.class.php");

class ChildrenReservation extends BaseEntity {
	
    protected $id_reservation_children, $id_reservation, $id_children, $num_children, $age, $idate, $udate;
    

    /**
    * Constructor
    */ 
    public function __construct( $id_reservation_children=-1) {
        $this->id_reservation_children = $id_reservation_children;
    }
   
    
	public function readFromRow($row){
	    if(isset($row['id_reservation_children'])) $this->setId_reservation_children($row['id_reservation_children']);
	    if(isset($row['id_reservation'])) $this->setId_reservation($row['id_reservation']);
	    if(isset($row['id_children'])) $this->setId_children($row['id_children']);
	    if(isset($row['num_children'])) $this->setNum_children($row['num_children']);
	    if(isset($row['age'])) $this->setAge($row['age']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_reservation_children()
    {
        return $this->id_reservation_children;
    }

    /**
     * @param mixed $id_reservation_children
     */
    public function setId_reservation_children($id_reservation_children)
    {
        $this->id_reservation_children = $id_reservation_children;
    }

    /**
     * @return mixed
     */
    public function getId_reservation()
    {
        return $this->id_reservation;
    }

    /**
     * @param mixed $id_reservation
     */
    public function setId_reservation($id_reservation)
    {
        $this->id_reservation = $id_reservation;
    }

    /**
     * @return mixed
     */
    public function getId_children()
    {
        return $this->id_children;
    }

    /**
     * @param mixed $id_children
     */
    public function setId_children($id_children)
    {
        $this->id_children = $id_children;
    }

    /**
     * @return mixed
     */
    public function getNum_children()
    {
        return $this->num_children;
    }

    /**
     * @param mixed $num_children
     */
    public function setNum_children($num_children)
    {
        $this->num_children = $num_children;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }

	
	   
}
?>
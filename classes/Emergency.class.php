<?php
include_once("BaseEntity.class.php");

class Emergency extends BaseEntity {
	
    protected $id_emergency, $id_parent, $name, $mail, $phone, $relation, $idate, $udate;
    
    /**
    * Constructor
    */ 
    public function __construct( $id_emergency=-1) {
        $this->id_emergency = $id_emergency;
    }
   
    
    public function readFromRow($row, $prefixElement=""){
        if(isset($row[($prefixElement.'id_emergency')])) {$this->setId_emergency($row[$prefixElement.'id_emergency']); };
        if(isset($row[$prefixElement.'id_parent'])) $this->setId_parent($row[$prefixElement.'id_parent']);
        if(isset($row[$prefixElement.'name'])) $this->setName($row[$prefixElement.'name']);
        if(isset($row[$prefixElement.'mail'])) $this->setMail($row[$prefixElement.'mail']);
        if(isset($row[$prefixElement.'phone'])) $this->setPhone($row[$prefixElement.'phone']);
        if(isset($row[$prefixElement.'relation'])) $this->setRelation($row[$prefixElement.'relation']);
        if(isset($row[$prefixElement.'idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row[$prefixElement.'idate']));
        if(isset($row[$prefixElement.'udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row[$prefixElement.'udate']));
	}
    /**
     * @return mixed
     */
    public function getId_emergency()
    {
        return $this->id_emergency;
    }

    /**
     * @param mixed $id_emergency
     */
    public function setId_emergency($id_emergency)
    {
        $this->id_emergency = $id_emergency;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getRelation()
    {
        return $this->relation;
    }

    /**
     * @param mixed $relation
     */
    public function setRelation($relation)
    {
        $this->relation = $relation;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous mixed
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param mixed
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }

	
		
    	
}
?>
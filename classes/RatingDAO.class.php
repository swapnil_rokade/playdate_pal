<?php
include_once("BaseDAO.class.php");

class RatingDAO extends BaseDAO {

    public function  RatingDAO() {
    }
    
    public static function countRatings() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_ratings";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	public static function getRatingListBySpecialist($id_specialist, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
		
		$link = getConnection();
		
		if (($page==null)|| (0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_ratings WHERE id_specialist=".$id_specialist;

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by id_rating desc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Rating();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	public static function getRatingListByPlaydate($id_playdate, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $link = getConnection();
	    
	    if (($page==null)|| (0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_ratings WHERE id_playdate=".$id_playdate;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_rating desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Rating();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getRating($id_rating) {
		$retorno = null;
		if(is_numeric($id_rating)) {
			$sql = "SELECT * FROM pd_ratings where id_rating=$id_rating";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Rating();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    

	public static function getRatingByParentPlaydate($id_parent, $id_playdate) {
	    $retorno = null;
	    if(is_numeric($id_parent) && is_numeric($id_playdate)) {
	        $sql = "SELECT * FROM pd_ratings where id_parent=$id_parent AND id_playdate=$id_playdate";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Rating();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	
	

	/**
	 * 
	 * @param Rating $newRating
	 * @return NULL|Rating
	 */
	public static function createRating($newRating) {
	    $retorno = null;
	    
	    if (($newRating!=null) && ($newRating->getId_parent()!=null) && ($newRating->getId_playdate()!=null) && ($newRating->getId_specialist()!=null)) {
	        //Save playdate
	        $link = getConnection();
	        
	        $sql_frm = "INSERT INTO pd_ratings (id_parent, id_specialist, id_playdate, value, comment_specialist, comment_playdate, idate) " .
	   	        "VALUES (".
	   	        "".mysql_real_escape_string($newRating->getId_parent()).", ".
	   	        "".mysql_real_escape_string($newRating->getId_specialist()).", ".
	   	        "".mysql_real_escape_string($newRating->getId_playdate()).", ".
	   	        "".(($newRating->getValue()!=null)?mysql_real_escape_string($newRating->getValue()):"0").", ".
	   	        "".(($newRating->getComment_specialist()!=null)?("'".mysql_real_escape_string($newRating->getComment_specialist())."'"):"null").", ".
	   	        "".(($newRating->getComment_playdate()!=null)?("'".mysql_real_escape_string($newRating->getComment_playdate())."'"):"null").", ".
	   	        "now()".
	   	        ")";
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        
	        
	        //Obtenemos el id del nuevo rating
	        $link2 = getConnection();
	        $sql2 = "SELECT * FROM pd_ratings WHERE id_parent=".$newRating->getId_parent()." AND id_playdate=".$newRating->getId_playdate()." order by id_rating desc limit 1";
	        //echo("<pre>::::[$sql2]::::::</pre>");//die();
	        $result2 = mysql_query($sql2, $link2);
	        
	        $newRating->readFromRow(mysql_fetch_assoc($result2));
	        
	        if (is_numeric($newRating->getId_rating()) && ($newRating->getId_rating()>0)) {
	            $retorno = $newRating;
	        }
	        mysql_close($link2);
	        
	    }
	    return $retorno;
	}
	
	
	/**
	 * 
	 * @param Rating $editRating
	 * @return NULL|Rating
	 */
	public static function updateRating($editRating) {
	    $retorno = null;
	    if (($editRating!=null) && (is_numeric($editRating->getId_rating()))) {
	        
	        //Updating rating info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_ratings SET " .
	   	        "value=" .(($editRating->getValue()!=null)?mysql_real_escape_string($editRating->getValue()):"null"). ", " .
	   	        "comment_playdate=" .(($editRating->getComment_playdate()!=null)?("'".mysql_real_escape_string($editRating->getComment_playdate())."'"):"null"). ", " .
	   	        "comment_specialist=" .(($editRating->getComment_specialist()!=null)?("'".mysql_real_escape_string($editRating->getComment_specialist())."'"):"null"). ", " .
	   	        "udate=now() " .
	   	        "WHERE id_rating=" . $editRating->getId_rating();
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = RatingDAO::getRating($editRating->getId_rating());
	    }
	    
	    return $retorno;
	}
	
	public static function deleteRating($id_rating) {
	    $retorno = false;
	    
	    if (($id_rating!=null) && (is_numeric($id_rating))) {
	        //Remove rating
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_ratings WHERE id_rating=" . $id_rating;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
	public static function getSpecialistValue($id_specialist) {
	    $link = getConnection();
	    $sql = "SELECT IFNULL(info.suma DIV info.votos, 0) as value FROM (select sum(value) as suma, count(1) as votos FROM pd_ratings WHERE id_specialist=".$id_specialist.") info;";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	
	
	
}
?>
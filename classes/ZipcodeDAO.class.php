<?php
include_once("BaseDAO.class.php");

class ZipcodeDAO extends BaseDAO {

    public function  ZipcodeDAO() {
    }
    
	public static function getZipcodeList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_zipcodes";

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by type asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Zipcode();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	
	public static function getZipcode($id_zipcode) {
		$retorno = null;
		if(is_numeric($id_zipcode)) {
			$sql = "SELECT * FROM pd_zipcodes where id_zipcode=$id_zipcode";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Zipcode();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    

}
?>
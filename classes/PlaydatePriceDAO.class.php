<?php
include_once ("BaseDAO.class.php");

class PlaydatePriceDAO extends BaseDAO {

    function PlaydatePriceDAO() {
	}

	
	public static function getPlaydatePrice($id_playdate) {
	    $retorno = null; //$ cents
	    if(($id_playdate!=null) && is_numeric($id_playdate)) {
	        $playdate = PlaydateDAO::getPlaydate($id_playdate);
	        
	        //Precio según número de horas y niños apuntados
	        $timeInit = $playdate->getTime_init();
	        $timeEnd = $playdate->getTime_end();
	        
	        $hours = $timeEnd-$timeInit;
	        //$numChilds = PlaydateDAO::countPlaydateReservations($id_playdate);
			$numChilds = $playdate->getMin_kids();
			
	        $baseRate = 1800;
	        if($numChilds==1) {
	            $baseRate = 1800;
	        } else if($numChilds==2) {
	            $baseRate = 1800;
	        } else if($numChilds==3) {
	            $baseRate = 1600;
	        } else if($numChilds==4) {
	            $baseRate = 1200;
	        } else if($numChilds==5) {
	            $baseRate = 1200;
	        } else if($numChilds==6) {
	            $baseRate = 1100;
	        } else if($numChilds==7) {
	            $baseRate = 1000;
	        } else if($numChilds==8) {
	            $baseRate = 1000;
	        } else if($numChilds>8) {
	            $baseRate = 1000;
	        }
	        
	        $retorno = $baseRate*$hours;
	        
	    }
	    
	    return $retorno;
	}
	
	/**
	 * Calculate playdate price that will be if there are new num_kids reservations
	 * @param number $id_playdate
	 * @param number $num_kids
	 * @return NULL|number
	 */
	public static function getPlaydatePriceForNewReservations($id_playdate, $num_kids=0) {
	    $retorno = null; //$ cents
	    if(($id_playdate!=null) && is_numeric($id_playdate)) {
	        $playdate = PlaydateDAO::getPlaydate($id_playdate);
	        
	        //Precio según número de horas y niños apuntados
	        $timeInit = $playdate->getTime_init();
	        $timeEnd = $playdate->getTime_end();
	        
	        $hours = $timeEnd-$timeInit;
	        $numChilds = PlaydateDAO::countPlaydateReservations($id_playdate) + $num_kids;
	        
	        $baseRate = 1800;
	        if($numChilds==1) {
	            $baseRate = 1800;
	        } else if($numChilds==2) {
	            $baseRate = 1800;
	        } else if($numChilds==3) {
	            $baseRate = 1600;
	        } else if($numChilds==4) {
	            $baseRate = 1200;
	        } else if($numChilds==5) {
	            $baseRate = 1200;
	        } else if($numChilds==6) {
	            $baseRate = 1100;
	        } else if($numChilds==7) {
	            $baseRate = 1000;
	        } else if($numChilds==8) {
	            $baseRate = 1000;
	        } else if($numChilds>8) {
	            $baseRate = 1000;
	        }
	        
	        $retorno = $baseRate*$hours;
	        
	    }
	    
	    return $retorno;
	}
	
	public static function getMaxPlaydatePrice($id_playdate) {
	    $retorno = null; //$ cents
	    if(($id_playdate!=null) && is_numeric($id_playdate)) {
	        $playdate = PlaydateDAO::getPlaydate($id_playdate);
	        
	        //Precio según número de horas y niños apuntados
	        $timeInit = $playdate->getTime_init();
	        $timeEnd = $playdate->getTime_end();
	        
	        $hours = $timeEnd-$timeInit;
	        
	        $numChilds = $playdate->getMin_kids();
	        if(($numChilds==null) || (!is_numeric($numChilds)) || ($numChilds<=0)) {
	           //if not maximum child number specified, we count reservations
	           $numChilds = PlaydateDAO::countPlaydateReservations($id_playdate);
	        }
	        
	        $baseRate = 1800;
	        if($numChilds==1) {
	            $baseRate = 1800;
	        } else if($numChilds==2) {
	            $baseRate = 1800;
	        } else if($numChilds==3) {
	            $baseRate = 1600;
	        } else if($numChilds==4) {
	            $baseRate = 1200;
	        } else if($numChilds==5) {
	            $baseRate = 1200;
	        } else if($numChilds==6) {
	            $baseRate = 1100;
	        } else if($numChilds==7) {
	            $baseRate = 1000;
	        } else if($numChilds==8) {
	            $baseRate = 1000;
	        }
	        
	        //TODO - REVIEW: Max playdate price would be with only 2 kids, so...
	        $baseRate = 1800;
	        
	        $retorno = $baseRate*$hours;
	        
	    }
	    
	    return $retorno;
	}
	
	/*
	public static function addChildToParentReservation($id_reservation, $id_children, $num_children=1, $age="null") {
	    if(is_numeric($id_reservation)) {
	        if($id_children==null) {
	            $id_children ="null";
	        }
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_reservation_children (id_reservation, id_children, num_children, age, idate) VALUES (".$id_reservation.", ".$id_children.", ".$num_children.", ".$age.", now())";
	        //echo("::::[$sql]:::::"); die();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function deleteAllReservationChildren($id_reservation) {
	    if(is_numeric($id_reservation)) {
	        $link1 = getConnection();
	        $sql = "DELETE FROM pd_reservation_children WHERE id_reservation=".$id_reservation;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	*/
	
}
?>
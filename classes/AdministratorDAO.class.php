<?php
include_once ("BaseDAO.class.php");

class AdministratorDAO extends BaseDAO {

    function AdministratorDAO() {
	}

	public static function countAdministrators() {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_administrators", $link) or die("Error counting Administrators");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function countAdministratorsSearch($query) {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_administrators WHERE name like '%$query%' or lastname like '%$query%' or email like '%$query%'", $link) or die("Error counting administrators");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function getAdministratorsList($page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT * FROM pd_administrators ";
		//Ordenacion
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
		    $sql .= " order by RAND()";
		    
		}

		//Start and range
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first
		$max = $elemsPerPage; # elements per page

		//paging
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
			$newItem = new Administrator(-1);
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}
		mysql_close($link);
		return $retorno;
	}

	
	public static function getAdministratorsSearch($userSearch, $page, $elemsPerPage, $sortfield, $sorttype) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_administrators s WHERE 1=1";
		
		//Adding query parameters
		/*
		$userSearch = new Administrator();
		
		if(($userSearch->getPerfil()!=null)&&($userSearch->getPerfil()!="")) {
			$sql.=" AND perfil='".$userSearch->getPerfil()."'";
		}
		*/
		
		//order
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		}

		//paging values
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first element
		$max = $elemsPerPage; # num elements

		//paging limits
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
			$newItem = new Administrator(-1);
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}
		mysql_close($link);
		return $retorno;
	}
	

	public static function getAdministrator($id_administrator) {
		$retorno = null;
		
		if(is_numeric($id_administrator) && ($id_administrator>=0)) {
			$link = getConnection();
	
			$sql = "SELECT s.* FROM pd_administrators s WHERE id_administrator=$id_administrator";
	
			//reading results
			$result = mysql_query($sql, $link);
			if ($row = mysql_fetch_assoc($result)) {
				$newItem= new Administrator(-1);
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}
			mysql_close($link);
		}
		return $retorno;
	}

	public static function getAdministratorByEmailPassword($email, $password) {
		$retorno = null;
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_administrators s WHERE email='".mysql_real_escape_string($email)."' and password=md5('".mysql_real_escape_string($password)."')";
        //echo("::::[$sql]::::");
		//reading results
		$result = mysql_query($sql, $link);
		if ($row = mysql_fetch_assoc($result)) {
			$newItem = new Administrator(-1);
			$newItem->readFromRow($row);
			$retorno = $newItem;
		}
		mysql_close($link);
		return $retorno;
	}
	
	public static function getAdministratorByUsernamePassword($username, $password) {
	    $retorno = null;
	    $link = getConnection();
	    
	    $sql = "SELECT s.* FROM pd_administrators s WHERE username='".mysql_real_escape_string($username)."' and password=md5('".mysql_real_escape_string($password)."')";
	    
	    //reading results
	    $result = mysql_query($sql, $link);
	    if ($row = mysql_fetch_assoc($result)) {
	        $newItem = new Administrator(-1);
	        $newItem->readFromRow($row);
	        $retorno = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	
	public static function getAdministratorByEmail($email) {
		$retorno = null;
		$link = getConnection();

		$sql = "SELECT s.* FROM pd_administrators s WHERE email='$email'";

		//reading results
		$result = mysql_query($sql, $link);
		if ($row = mysql_fetch_assoc($result)) {
			$newItem = new Administrator(-1);
			$newItem->readFromRow($row);
			$retorno = $newItem;
		}
		mysql_close($link);
		return $retorno;
	}

	/**
	 * CREATE ADMINISTRATOR
	 * 
	 */
	public static function createAdministrator($newAdministrator) {
	    $retorno = null;
	    
	    if (($newAdministrator!=null) && ($newAdministrator->getEmail() != "")) {
	        //Save parent basic info
	        $link = getConnection();
	        
	        //$newAdministrator = new Administrator();
	        
	        $sql_frm = "INSERT INTO pd_administrators (name, lastname, username, email, password, picture, idate) " .
	   	        "VALUES (".
	   	        "'".mysql_real_escape_string($newAdministrator->getName())."', ".
	   	        "'".mysql_real_escape_string($newAdministrator->getLastname())."', ".
	   	        "'".mysql_real_escape_string($newAdministrator->getUsername())."', ".
	   	        "'".mysql_real_escape_string($newAdministrator->getEmail())."', ".
	   	        "md5('".mysql_real_escape_string($newAdministrator->getPassword())."'), ".
	   	        "'".mysql_real_escape_string($newAdministrator->getPicture())."', ".
	   	        "now()".
	   	        ")";
	        
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        //Obtenemos el id del nuevo administrador
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM pd_administrators WHERE email='" . mysql_real_escape_string($newAdministrator->getEmail()) . "' order by id_administrator desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        $newAdministrator->readFromRow(mysql_fetch_assoc($result_frm));
	        
	        if (is_numeric($newAdministrator->getId_administrator()) && ($newAdministrator->getId_administrator()>0)) {
	            $retorno = $newAdministrator;
	        }
	        mysql_close($link);
	    }
	    return $retorno;
	}

	/**
	 * 
	 * @param Administrator $editAdministrator
	 * @return NULL|Administrator
	 */
	public static function updateAdministrator($editAdministrator) {
		$retorno = null;
		if (($editAdministrator!=null) && (is_numeric($editAdministrator->getId_administrator()))) {
			
			//Updating administrator info
			$link = getConnection();
			
			$sql_frm = "UPDATE pd_administrators SET name='" . $editAdministrator->getName() . "', " .
			"lastname='" . mysql_real_escape_string($editAdministrator->getLastname()) . "', " .
			"username='" . mysql_real_escape_string($editAdministrator->getUsername()) . "', " .
			"email='" . mysql_real_escape_string($editAdministrator->getEmail()) . "', " .
			"picture='" . mysql_real_escape_string($editAdministrator->getPicture()) . "', " .
			"udate=now() " .
			"WHERE id_administrator=" . $editAdministrator->getId_administrator();

			//echo("::::::[$sql_frm]:::::::");
			//die();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = AdministratorDAO::getAdministrator($editAdministrator->getId_administrator());
		}

		return $retorno;
	}

	/**
	 *
	 * @param Administrator $editAdministrator
	 * @return NULL|Administrator
	 */
	public static function updateAdministratorPassword($id_administrator, $newPassword) {
	    $retorno = null;
	    if (($id_administrator!=null) && (is_numeric($id_administrator))) {
	        
	        //Updating administrator info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_administrators SET password=md5('". mysql_real_escape_string($newPassword) . "'), udate=now() WHERE id_administrator=" . $id_administrator;
	        
	        //echo("::::::[$sql_frm]:::::::");
	        //die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = AdministratorDAO::getAdministrator($id_administrator);
	    }
	    
	    return $retorno;
	}
	
	
	
	
	/**
	 * Update Administrator Password
	 * @param Administrator $editAdministrator
	 * @param mixed $newpassword
	 * @return NULL|Administrator
	 */
	public static function changePassword($editAdministrator, $newpassword) {
		$retorno = null;
		if (($editAdministrator!=null) && (is_numeric($editAdministrator->getId_administrator()))) {
			//Update password
			$link = getConnection();
			$sql_frm = "UPDATE pd_administrators SET password=md5('" . $newpassword . "') WHERE id_administrator=" . $editAdministrator->getId_administrator();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = AdministratorDAO::getAdministrator($editAdministrator->getId_administrator());
		}

		return $retorno;
	}


	/**
	 * Delete an Administrator
	 * @param Administrator $deleteAdministrator
	 * @return boolean
	 */
	public static function deleteAdministrator($deleteAdministrator) {
		$retorno = false;

		if (($deleteAdministrator!=null) && (is_numeric($deleteAdministrator->getId_administrator()))) {
			//Remove administrator info
			$link = getConnection();
			$sql_frm = "DELETE FROM pd_administrators WHERE id_administrator=" . $deleteAdministrator->getId_administrator();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = true;
		}

		return $retorno;
	}

	/**
	 * Check if mail is already in administrators table
	 * @param Administrator $administrator
	 * @return Response
	 */
	public static function checkMail($administrator) {
		//1 - Test if mail exists
		$retorno = new Response(); // OK if not found
		$link = getConnection();
		$sql = "SELECT count(1) FROM pd_administrators WHERE email='" . mysql_real_escape_string($administrator->getEmail()) . "'";
		//Read results
		$result = mysql_query($sql, $link);
		list ($total) = mysql_fetch_row($result);
		mysql_close($link);

		if ($total > 0) {
			$retorno->setError_code(-5);
			$retorno->setDescription("This email address belongs to an administrator");
		}

		return $retorno;
	}
}
?>
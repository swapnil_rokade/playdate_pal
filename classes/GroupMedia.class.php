<?php
include_once("BaseEntity.class.php");

class GroupMedia extends BaseEntity {
	
    protected $id_media, $id_group, $id_parent, $id_specialist, $description, $picture, $idate, $udate;
    
    /**
    * Constructor
    */ 
    public function __construct( $id_media=-1) {
        $this->id_media = $id_media;
    }
   
    
	public function readFromRow($row){
	    if(isset($row['id_media'])) $this->setId_media($row['id_media']);
	    if(isset($row['id_group'])) $this->setId_group($row['id_group']);
	    if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
	    if(isset($row['id_specialist'])) $this->setId_specialist($row['id_specialist']);
	    if(isset($row['description'])) $this->setDescription($row['description']);
	    if(isset($row['picture'])) $this->setPicture($row['picture']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_media()
    {
        return $this->id_media;
    }

    /**
     * @param mixed $id_media
     */
    public function setId_media($id_media)
    {
        $this->id_media = $id_media;
    }

    /**
     * @return mixed
     */
    public function getId_group()
    {
        return $this->id_group;
    }

    /**
     * @param mixed $id_group
     */
    public function setId_group($id_group)
    {
        $this->id_group = $id_group;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getId_specialist()
    {
        return $this->id_specialist;
    }

    /**
     * @param mixed $id_specialist
     */
    public function setId_specialist($id_specialist)
    {
        $this->id_specialist = $id_specialist;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param mixed $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param mixed $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }

	
    
}
?>
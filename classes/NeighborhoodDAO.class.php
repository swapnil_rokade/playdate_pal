<?php
include_once("BaseDAO.class.php");

class NeighborhoodDAO extends BaseDAO {

    public function  NeighborhoodDAO() {
    }
    
	public static function getCityList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_cities";

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by name asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new City();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	
	public static function getNeighborhoodsListByCity($id_city, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_neighborhoods WHERE id_city=".$id_city;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    //echo(":::[$sql]:::"); die();
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Neighborhood();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getZipcodesListByNeighborhood($id_neighborhood, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_zipcodes WHERE id_neighborhood=".$id_neighborhood;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by zipcode asc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    //echo(":::[$sql]::::::"); die();
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Zipcode();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getCity($id_city) {
	    $retorno = null;
	    if(is_numeric($id_city)) {
	        $sql = "SELECT * FROM pd_cities where id_city=$id_city";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new City();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	public static function getNeighborhood($id_neighborhood) {
	    $retorno = null;
	    if(is_numeric($id_neighborhood)) {
	        $sql = "SELECT * FROM pd_neighborhoods where id_neighborhood=$id_neighborhood";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Neighborhood();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	public static function getZipcode($id_zipcode) {
		$retorno = null;
		if(is_numeric($id_zipcode)) {
			$sql = "SELECT * FROM pd_zipcodes where id_zipcode=$id_zipcode";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Zipcode();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}
	
	/**
	 * 
	 * @param City $newCity
	 * @return NULL|City
	 */
	public static function createCity($newCity) {
	    $retorno = null;
	    if($newCity->getName()!=null) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_cities (name, idate) VALUES ('".mysql_real_escape_string($newCity->getName())."', now())";
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	        
	        //Obtenemos el id de ese item
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM pd_cities WHERE name='".mysql_real_escape_string($newCity->getName())."' order by id_city desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        
	        $newCity->readFromRow(mysql_fetch_assoc($result_frm));
	        
	        if(is_numeric($newCity->getId_city())) {
	            $retorno = $newCity;
	        }
	        
	        
	    }
	    return $retorno;
	}
	
	/**
	 *
	 * @param Neighborhood $newNeighborhood
	 * @return NULL|Neighborhood
	 */
	public static function createNeighborhood($newNeighborhood) {
	    $retorno = null;
	    if($newNeighborhood->getName()!=null) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_neighborhoods (id_city, name, idate) VALUES (".$newNeighborhood->getId_city().",'".mysql_real_escape_string($newNeighborhood->getName())."', now())";
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	        
	        //Obtenemos el id de ese item
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM pd_neighborhoods WHERE name='".mysql_real_escape_string($newNeighborhood->getName())."' order by id_neighborhood desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        
	        $newNeighborhood->readFromRow(mysql_fetch_assoc($result_frm));
	        
	        if(is_numeric($newNeighborhood->getId_neighborhood())) {
	            $retorno = $newNeighborhood;
	        }
	        
	    }
	    return $retorno;
	}

	/**
	 *
	 * @param Zipcode $newZipcode
	 * @return NULL|Zipcode
	 */
	public static function createZipcode($newZipcode) {
	    $retorno = null;
	    if($newZipcode->getZipcode()!=null) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_zipcodes (id_neighborhood, zipcode, idate) VALUES (".$newZipcode->getId_neighborhood().",'".mysql_real_escape_string($newZipcode->getZipcode())."', now())";
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	        
	        //Obtenemos el id de ese item
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM pd_zipcodes WHERE zipcode='".mysql_real_escape_string($newZipcode->getZipcode())."' order by id_zipcode desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        
	        $newZipcode->readFromRow(mysql_fetch_assoc($result_frm));
	        
	        if(is_numeric($newZipcode->getId_zipcode())) {
	            $retorno = $newZipcode;
	        }
	        
	    }
	    return $retorno;
	}
	/**
	 * 
	 * @param City $deleteCity
	 * @return boolean
	 */
	public static function deleteCity($deleteCity) {
	    $retorno = false;
	    
	    if (isset ($deleteCity) && (is_numeric($deleteCity->getId_city()))) {
	        //Remove info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_cities WHERE id_city=" . $deleteCity->getId_city();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param Neighborhood $deleteNeighborhood
	 * @return boolean
	 */
	public static function deleteNeighborhood($deleteNeighborhood) {
	    $retorno = false;
	    
	    if (isset ($deleteNeighborhood) && (is_numeric($deleteNeighborhood->getId_neighborhood()))) {
	        //Remove info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_neighborhoods WHERE id_neighborhood=" . $deleteNeighborhood->getId_neighborhood();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param Zipcode $deleteZipcode
	 * @return boolean
	 */
	public static function deleteZipcode($deleteZipcode) {
	    $retorno = false;
	    
	    if (isset ($deleteZipcode) && (is_numeric($deleteZipcode->getId_zipcode()))) {
	        //Remove info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_zipcodes WHERE id_zipcode=" . $deleteZipcode->getId_zipcode();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
	public static function getNeighborhoodsListByParent($id_parent) {
	    $retorno = array();
	    
	    $sql = "SELECT n.* FROM pd_neighborhoods n, pd_parent_neighborhoods r WHERE r.id_neighborhood=n.id_neighborhood AND id_parent=".$id_parent;
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Neighborhood();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function deleteAllParentNeighborhood($id_parent) {
	    if(is_numeric($id_parent)) {
	        $sql = "DELETE FROM pd_parent_neighborhoods WHERE id_parent=".$id_parent;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createParentNeighborhood($id_parent, $id_neighborhood) {
	    if(is_numeric($id_parent) && is_numeric($id_neighborhood)) {
	        $sql = "INSERT INTO pd_parent_neighborhoods (id_parent, id_neighborhood, idate) VALUES (".$id_parent.", ".$id_neighborhood.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	

	public static function getNeighborhoodsListBySpecialist($id_specialist) {
	    $retorno = array();
	    
	    $sql = "SELECT n.* FROM pd_neighborhoods n, pd_specialist_neighborhoods r WHERE r.id_neighborhood=n.id_neighborhood AND id_specialist=".$id_specialist;
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Neighborhood();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function deleteAllSpecialistNeighborhood($id_specialist) {
	    if(is_numeric($id_specialist)) {
	        $sql = "DELETE FROM pd_specialist_neighborhoods WHERE id_specialist=".$id_specialist;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createSpecialistNeighborhood($id_specialist, $id_neighborhood) {
	    if(is_numeric($id_specialist) && is_numeric($id_neighborhood)) {
	        $sql = "INSERT INTO pd_specialist_neighborhoods (id_specialist, id_neighborhood, idate) VALUES (".$id_specialist.", ".$id_neighborhood.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	public static function getNeighborhoodsListByGroup($id_group) {
	    $retorno = array();
	    
	    $sql = "SELECT n.* FROM pd_neighborhoods n, pd_group_neighborhoods r WHERE r.id_neighborhood=n.id_neighborhood AND id_group=".$id_group;
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Neighborhood();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function deleteAllGroupNeighborhood($id_group) {
	    if(is_numeric($id_group)) {
	        $sql = "DELETE FROM pd_group_neighborhoods WHERE id_group=".$id_group;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createGroupNeighborhood($id_group, $id_neighborhood) {
	    if(is_numeric($id_group) && is_numeric($id_neighborhood)) {
	        $sql = "INSERT INTO pd_group_neighborhoods (id_group, id_neighborhood, idate) VALUES (".$id_group.", ".$id_neighborhood.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * 
	 * @param mixed $id_request
	 * @return Neighborhood[]
	 */
	public static function getNeighborhoodsListByRequest($id_request) {
	    $retorno = array();
	    
	    $sql = "SELECT n.* FROM pd_neighborhoods n, pd_request_neighborhoods r WHERE r.id_neighborhood=n.id_neighborhood AND id_request=".$id_request;
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Neighborhood();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
    public static function deleteAllRequestNeighborhood($id_request) {
	    if(is_numeric($id_request)) {
	        $sql = "DELETE FROM pd_request_neighborhoods WHERE id_request=".$id_request;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createRequestNeighborhood($id_request, $id_neighborhood) {
	    if(is_numeric($id_request) && is_numeric($id_neighborhood)) {
	        $sql = "INSERT INTO pd_request_neighborhoods (id_request, id_neighborhood, idate) VALUES (".$id_request.", ".$id_neighborhood.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function getNeighborhoodsListByPlaydate($id_playdate) {
	    $retorno = array();
	    
	    $sql = "SELECT n.* FROM pd_neighborhoods n, pd_playdate_neighborhoods r WHERE r.id_neighborhood=n.id_neighborhood AND id_playdate=".$id_playdate;
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Neighborhood();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function deleteAllPlaydateNeighborhood($id_playdate) {
	    if(is_numeric($id_request)) {
	        $sql = "DELETE FROM pd_playdate_neighborhoods WHERE id_playdate=".$id_playdate;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createPlaydateNeighborhood($id_playdate, $id_neighborhood) {
	    if(is_numeric($id_playdate) && is_numeric($id_neighborhood)) {
	        $sql = "INSERT INTO pd_playdate_neighborhoods (id_playdate, id_neighborhood, idate) VALUES (".$id_playdate.", ".$id_neighborhood.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
}
?>
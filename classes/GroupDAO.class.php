<?php
include_once("BaseDAO.class.php");

class GroupDAO extends BaseDAO {

    public function  GroupDAO() {
    }

    public static function createGroup($newGroup) {
        $retorno = null;
        if($newGroup->getName() != null) {
            $link1 = getConnection();
            $sql = "INSERT INTO pd_groups (name, idate) VALUES ('".mysql_real_escape_string($newGroup->getName())."', now())";
            $result = mysql_query($sql, $link1);
            mysql_close($link1);
            
            //Obtenemos el id del nuevo grupo
            $link2 = getConnection();
            $sql2 = "SELECT * FROM pd_groups WHERE name='".mysql_real_escape_string($newGroup->getName())."' order by id_group desc limit 1";

            $result2 = mysql_query($sql2, $link2);
            
            $newGroup->readFromRow(mysql_fetch_assoc($result2));
            
            if (is_numeric($newGroup->getId_group()) && ($newGroup->getId_group()>0)) {
                $retorno = $newGroup;
            }
            mysql_close($link2);
            
        }
        
        return $retorno;
    }
    
    
    public static function countGroups() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_groups";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	/**
	 * TEST if parent is in group
	 * @param mixed $id_parent
	 * @param mixed $id_group
	 * @return boolean
	 */
	public static function isParentInGroup($id_parent, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_parents WHERE id_parent=$id_parent AND id_group=$id_group";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}

	/**
	 * TEST if parent is admin in group
	 * @param mixed $id_parent
	 * @param mixed $id_group
	 * @return boolean
	 */
	public static function isParentAdminInGroup($id_parent, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_parents WHERE id_parent=$id_parent AND id_group=$id_group AND is_admin=1";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	public static function isParentInGroupRequest($id_parent, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_parent_requests WHERE id_parent=$id_parent AND id_group=$id_group";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	public static function isParentInGroupRequestDeclined($id_parent, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_parent_requests WHERE id_parent=$id_parent AND id_group=$id_group AND status=".GroupRequest::$STATUS_REJECTED;
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	public static function isSpecialistInGroup($id_specialist, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_specialists WHERE id_specialist=$id_specialist AND id_group=$id_group";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	/**
	 * TEST if specialist is admin in group
	 * @param mixed $id_specialist
	 * @param mixed $id_group
	 * @return boolean
	 */
	public static function isSpecialistAdminInGroup($id_specialist, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_specialists WHERE id_specialist=$id_specialist AND id_group=$id_group AND is_admin=1";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	public static function isSpecialistInGroupRequests($id_specialist, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_specialist_requests WHERE id_specialist=$id_specialist AND id_group=$id_group";
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	public static function isSpecialistInGroupRequestsDeclined($id_specialist, $id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_specialist_requests WHERE id_specialist=$id_specialist AND id_group=$id_group AND status=".GroupRequest::$STATUS_REJECTED;
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return ($total>0);
	}
	
	public static function countGroupParents($id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_parents WHERE id_group=".$id_group;
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	public static function countGroupSpecialists($id_group) {
	    $link = getConnection();
	    $sql = "select count(*) FROM pd_group_specialists WHERE id_group=".$id_group;
	    $rs = mysql_query($sql, $link);
	    list($total) = mysql_fetch_row($rs);
	    mysql_close($link);
	    return $total;
	}
	
	
	public static function getGroupsList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_groups";

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by name asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Group();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	public static function getGroupsByParent($id_parent, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT g.* FROM pd_groups g, pd_group_parents gp WHERE g.id_group = gp.id_group AND gp.id_parent=".$id_parent;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Group();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	public static function getGroupInvitationsByParent($id_parent, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT g.*, gp.id_invitation, gp.id_parent, gp.status FROM pd_groups g, pd_group_parents_invitations gp WHERE g.id_group = gp.id_group AND gp.id_parent=".$id_parent;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new GroupInvitation();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getWaitingGroupInvitationsByParent($id_parent, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT g.*, gp.id_invitation, gp.id_parent, gp.status FROM pd_groups g, pd_group_parents_invitations gp WHERE g.id_group = gp.id_group AND gp.status=".GroupInvitation::$STATUS_SENT." AND gp.id_parent=".$id_parent;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new GroupInvitation();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getWaitingGroupInvitationsBySpecialist($id_specialist, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT g.*, gp.id_invitation, gp.id_specialist, gp.status FROM pd_groups g, pd_group_specialist_invitations gp WHERE g.id_group = gp.id_group AND gp.status=".GroupInvitation::$STATUS_SENT." AND gp.id_specialist=".$id_specialist;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new GroupInvitation();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function getGroupsBySpecialist($id_specialist, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT g.* FROM pd_groups g, pd_group_specialists gs WHERE g.id_group = gs.id_group AND gs.id_specialist=".$id_specialist;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Group();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getParentRequestsByGroup($id_group, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT g.*, r.* FROM pd_groups g, pd_group_parent_requests r WHERE g.id_group = r.id_group AND r.id_group=$id_group AND r.status=".GroupRequest::$STATUS_SENT;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new GroupRequest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getSpecialistRequestsByGroup($id_group, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT g.*, r.* FROM pd_groups g, pd_group_specialist_requests r WHERE g.id_group = r.id_group AND r.id_group=$id_group AND r.status=".GroupRequest::$STATUS_SENT;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by name asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new GroupRequest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function updateGroupPrivacity($id_group, $newPrivacity) {
	    $link = getConnection();
	    $sql = "UPDATE pd_groups SET privacity=".$newPrivacity." WHERE id_group=".$id_group;
	    //echo("::::::[$sql]::::"); die();
	    $rs = mysql_query($sql, $link);
	    mysql_close($link);
	    return true;
	}
	
	public static function acceptParentRequest($id_request) {
	    $link = getConnection();
	    $sql = "UPDATE pd_group_parent_requests SET status=".GroupRequest::$STATUS_ACCEPTED." WHERE id_request=".$id_request;
	    $rs = mysql_query($sql, $link);
	    mysql_close($link);
	    return true;
	}
	public static function acceptSpecialistRequest($id_request) {
	    $link = getConnection();
	    $sql = "UPDATE pd_group_specialist_requests SET status=".GroupRequest::$STATUS_ACCEPTED." WHERE id_request=".$id_request;
	    $rs = mysql_query($sql, $link);
	    mysql_close($link);
	    return true;
	}
	
	public static function declineParentRequest($id_request) {
	    $link = getConnection();
	    $sql = "UPDATE pd_group_parent_requests SET status=".GroupRequest::$STATUS_REJECTED." WHERE id_request=".$id_request;
	    $rs = mysql_query($sql, $link);
	    mysql_close($link);
	    return true;
	}
	public static function declineSpecialistRequest($id_request) {
	    $link = getConnection();
	    $sql = "UPDATE pd_group_specialist_requests SET status=".GroupRequest::$STATUS_REJECTED." WHERE id_request=".$id_request;
	    $rs = mysql_query($sql, $link);
	    mysql_close($link);
	    return true;
	}
	
	
	public static function getInterestsByRequest($id_request, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT i.* FROM pd_interests i, pd_request_interests p where i.id_interest=p.id_interest and p.id_request=$id_request";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by type asc";
	    }
	    
	    //echo("::[$sql]:"); die();
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getInterestsIdByRequest($id_request, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT q.* FROM pd_playdate_requests r, pd_request_interests q where r.id_request=q.id_request and r.id_request=".$id_request;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by type asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem->getId_interest();
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getGroup($id_group) {
		$retorno = null;
		if(is_numeric($id_group)) {
			$sql = "SELECT * FROM pd_groups where id_group=$id_group";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Group();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}

	
	
	/**
	 *
	 * @param number $id_request
	 * @return NULL|GroupRequest
	 */
	public static function getGroupParentRequest($id_request) {
	    $retorno = null;
	    if(is_numeric($id_request)) {
	        $sql = "SELECT * FROM pd_group_parent_requests where id_request=$id_request";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new GroupRequest();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
    
    /**
     * 
     * @param number $id_request
     * @return NULL|GroupRequest
     */
    public static function getGroupSpecialistRequest($id_request) {
	    $retorno = null;
	    if(is_numeric($id_request)) {
	        $sql = "SELECT * FROM pd_group_specialist_requests where id_request=$id_request";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new GroupRequest();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	/**
	 * 
	 * @param string $name
	 * @return NULL|Group
	 */
	public static function getGroupByName($name) {
	    $retorno = null;
	    if(($name!=null) && ($name!="")) {
	        $link1 = getConnection();
	        $sql = "SELECT * FROM pd_groups where lower(name)=lower('".mysql_real_escape_string($name)."')";
	        
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new Group();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	
	public static function getGroupParentInvitation($id_invitation) {
	    $retorno = null;
	    if(is_numeric($id_invitation)) {
	        $sql = "SELECT g.*, gp.id_invitation, gp.id_parent, gp.status FROM pd_groups g, pd_group_parents_invitations gp WHERE g.id_group = gp.id_group AND gp.id_invitation=".$id_invitation;
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new GroupInvitation();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	public static function getGroupSpecialistInvitation($id_invitation) {
	    $retorno = null;
	    if(is_numeric($id_invitation)) {
	        $sql = "SELECT g.*, gp.id_invitation, gp.id_specialist, gp.status FROM pd_groups g, pd_group_specialist_invitations gp WHERE g.id_group = gp.id_group AND gp.id_invitation=".$id_invitation;
	        //echo(":::[$sql]::::"); die();
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new GroupInvitation();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	
	public static function deleteAllRequestInterests($id_request) {
	    if(is_numeric($id_request)) {
	        $sql = "DELETE FROM pd_request_interests WHERE id_request=".$id_request;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	    }
	}
	
	public static function createGroupParentRequest($id_group, $id_parent) {
	    if(is_numeric($id_parent) && is_numeric($id_group)) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_group_parent_requests (id_group, id_parent, idate) VALUES (".$id_group.", ".$id_parent.", now())";
	        
	        //echo(":::[$sql]:::"); die();
	        
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	        //Remove declined invitations to join this group
	        $linkDeleteInv = getConnection();
	        $sqlDeleteInv = "DELETE pd_group_parents_invitations WHERE id_parent=".$id_parent." AND id_group=".$id_group." AND status=".GroupInvitation::$STATUS_REJECTED;
	        $result = mysql_query($sqlDeleteInv, $linkDeleteInv);
	        mysql_close($linkDeleteInv);
	    }
	}
	
	public static function createGroupParentInvitation($id_group, $id_parent) {
	    if(is_numeric($id_parent) && is_numeric($id_group)) {
	        $sql = "INSERT INTO pd_group_parents_invitations (id_group, id_parent, idate) VALUES (".$id_group.", ".$id_parent.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createGroupSpecialistInvitation($id_group, $id_specialist) {
	    if(is_numeric($id_specialist) && is_numeric($id_group)) {
	        $sql = "INSERT INTO pd_group_specialist_invitations (id_group, id_specialist, idate) VALUES (".$id_group.", ".$id_specialist.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function acceptGroupParentInvitation($id_invitation) {
	    if(is_numeric($id_invitation)) {
	        $sql = "UPDATE pd_group_parents_invitations SET status=".GroupInvitation::$STATUS_ACCEPTED.", udate=now() WHERE id_invitation=".$id_invitation;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function declineGroupParentInvitation($id_invitation) {
	    
	    if(is_numeric($id_invitation)) {
	        $sql = "UPDATE pd_group_parents_invitations SET status=".GroupInvitation::$STATUS_REJECTED.", udate=now() WHERE id_invitation=".$id_invitation;
	        //echo(":::::[$sql]::::"); die();
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	public static function acceptGroupSpecialistInvitation($id_invitation) {
	    if(is_numeric($id_invitation)) {
	        $sql = "UPDATE pd_group_specialist_invitations SET status=".GroupInvitation::$STATUS_ACCEPTED.", udate=now() WHERE id_invitation=".$id_invitation;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function declineGroupSpecialistInvitation($id_invitation) {
	    
	    if(is_numeric($id_invitation)) {
	        $sql = "UPDATE pd_group_specialist_invitations SET status=".GroupInvitation::$STATUS_REJECTED.", udate=now() WHERE id_invitation=".$id_invitation;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	public static function parentJoinGroup($id_group, $id_parent, $is_admin=false) {
	    if(is_numeric($id_parent) && is_numeric($id_group)) {
	        $isAdminValue = 0;
	        if($is_admin) {
	            $isAdminValue = 1;
	        }
	        $sql = "INSERT INTO pd_group_parents (id_group, id_parent, is_admin, idate) VALUES (".$id_group.", ".$id_parent.", ".$isAdminValue.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function parentRemoveGroup($id_group, $id_parent) {
	    if(is_numeric($id_parent) && is_numeric($id_group)) {
	        $sql = "DELETE pd_group_parents WHERE id_group=".$id_group." AND id_parent=".$id_parent;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function specialistRemoveGroup($id_group, $id_specialist) {
	    if(is_numeric($id_parent) && is_numeric($id_group)) {
	        $sql = "DELETE pd_group_specialists WHERE id_group=".$id_group." AND id_specialist=".$id_specialist;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	public static function createGroupSpecialistRequest($id_group, $id_specialist) {
	    if(is_numeric($id_specialist) && is_numeric($id_group)) {
	        $sql = "INSERT INTO pd_group_specialist_requests (id_group, id_specialist, idate) VALUES (".$id_group.", ".$id_specialist.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function specialistJoinGroup($id_group, $id_specialist, $is_admin) {
	    if(is_numeric($id_specialist) && is_numeric($id_group)) {
	        
	        $isAdminValue = 0;
	        if($is_admin) {
	            $isAdminValue = 1;
	        }
	        $sql = "INSERT INTO pd_group_specialists (id_group, id_specialist, is_admin, idate) VALUES (".$id_group.", ".$id_specialist.", ".$isAdminValue.", now())";
	        
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * Update group info
	 * @param Group $editGroup
	 * @return NULL|Group
	 */
	public static function updateGroup($editGroup) {
	    $retorno = null;
	    if (is_numeric($editGroup->getId_group())) {
	        
	        //Updating group info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_groups SET name='" . mysql_real_escape_string($editGroup->getName()) . "', " .
	   	        "description=" .(($editGroup->getDescription()!=null)?("'".mysql_real_escape_string($editGroup->getDescription())."'"):"null"). ", " .
	   	        "zipcode=" .(($editGroup->getZipcode()!=null)?("'".mysql_real_escape_string($editGroup->getZipcode())."'"):"null"). ", " .
	   	        "picture=". (($editGroup->getPicture()!=null)?("'" . $editGroup->getPicture() . "'"):"null") . ", " .
	   	        "privacity=". (($editGroup->getPrivacity()!=null)?("'" . $editGroup->getPrivacity() . "'"):"null") . ", " .
	   	        "slug=". (($editGroup->getSlug()!=null)?("'" . $editGroup->getSlug() . "'"):"null") . ", " .
	   	        "udate=now() " .
	   	        "WHERE id_group=" . $editGroup->getId_group();
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = GroupDAO::getGroup($editGroup->getId_group());
	    }
	    
	    return $retorno;
	}
	
	/**
	 * Group Media creation
	 * @param GroupMedia $newMedia
	 * @return mixed
	 */
	public static function createMedia($newMedia) {
	    $retorno = null;
	    
	    if (isset ($newMedia) && ($newMedia->getPicture() != "") && ($newMedia->getId_group() != null)&& ($newMedia->getId_group() > 0)) {
	        //Save media
	        $link = getConnection();
	        
	        $sql_frm = "INSERT INTO pd_group_media (id_group, id_parent, id_specialist, description, picture, idate) " .
	   	        "VALUES (".
	   	        "".mysql_real_escape_string($newMedia->getId_group()).", ".
	   	        "".(($newMedia->getId_parent()!=null)?mysql_real_escape_string($newMedia->getId_parent()):"null").", ".
	   	        "".(($newMedia->getId_specialist()!=null)?mysql_real_escape_string($newMedia->getId_specialist()):"null").", ".
	   	        "".(($newMedia->getDescription()!=null)?("'".mysql_real_escape_string($newMedia->getDescription())."'"):"null").", ".
	   	        "".(($newMedia->getPicture()!=null)?("'".mysql_real_escape_string($newMedia->getPicture())."'"):"null").", ".
	   	        "now())";
	        
	        //echo("::[$sql_frm]::"); die();
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        //Obtenemos el id del nuevo media (creado en los �ltimos 15 segundos en este grupo)
	        $link = getConnection();
	        $sql_frm = "SELECT * FROM pd_group_media WHERE id_group=" . $newMedia->getId_group() . " AND idate > date_sub(now(), interval 15 second) order by id_media desc limit 1";
	        $result_frm = mysql_query($sql_frm, $link);
	        $newMedia->readFromRow(mysql_fetch_assoc($result_frm));
	        
	        if (is_numeric($newMedia->getId_media()) && ($newMedia->getId_media()>0)) {
	            $retorno = $newMedia;
	        }
	        mysql_close($link);
	    }
	    return $retorno;
	}
	
	/**
	 * Update Group Media
	 * @param GroupMedia $editMedia
	 * @return NULL|GroupMedia
	 */
	public static function updateMedia($editMedia) {
	    $retorno = null;
	    if (isset ($editMedia) && (is_numeric($editMedia->getId_media()))) {
	        
	        //Updating media info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_group_media SET " .
	   	        "description=" .(($editMedia->getDescription()!=null)?("'".mysql_real_escape_string($editMedia->getDescription())."'"):"null"). ", " .
	   	        "udate=now() " .
	   	        "WHERE id_media=" . $editMedia->getId_media();
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = GroupDAO::getMedia($editMedia->getId_media());
	    }
	    
	    return $retorno;
	}
	
	/**
	 * Get GroupMedia info
	 * @param number $id_media
	 * @return NULL|GroupMedia
	 */
	public static function getMedia($id_media) {
	    $retorno = null;
	    if(is_numeric($id_group)) {
	        $sql = "SELECT * FROM pd_group_media where id_media=$id_media";
	        $link1 = getConnection();
	        //Read results
	        $result = mysql_query($sql, $link1);
	        while($row = mysql_fetch_assoc($result)) {
	            $newItem = new GroupMedia();
	            $newItem->readFromRow($row);
	            $retorno = $newItem;
	        }
	        
	        mysql_close($link1);
	        
	    }
	    return $retorno;
	}
	
	/**
	 * Remove media info
	 * @param number $id_media
	 * @return boolean
	 */
	public static function deleteMedia($id_media) {
	    $retorno = false;
	    
	    if (($id_media!=null) && (is_numeric($id_media))) {
	        //Remove media info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_group_media WHERE id_media=" . $id_media;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
	/**
	 * Remove group
	 * @param number $id_group
	 * @return boolean
	 */
	public static function deleteGroup($id_group) {
	    $retorno = false;
	    
	    if (($id_group!=null) && (is_numeric($id_group))) {
	        //Remove group
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_groups WHERE id_group=" . $id_group;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
	/**
	 * Get Media List by Group
	 * @param number $id_group
	 * @param number $page
	 * @param number $elementsPerPage
	 * @param string $sortfield
	 * @param string $sorttype
	 * @return GroupMedia[]
	 */
	public static function getMediaListByGroup($id_group, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_group_media WHERE id_group=".$id_group;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by idate desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new GroupMedia();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
}
?>
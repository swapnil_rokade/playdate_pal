<?php
include_once ("BaseEntity.class.php");

class DiskUtil extends BaseEntity {

	/**
	 * Constructor
	 */
	public function __construct() {
	}

	public function createDir($dirPath) {

	}

	/**
	 * Elimina un directorio
	 * 
	 */
	public static function delDir($path) {
		if(is_dir($path)) {
			$dh = opendir($path);
			$path = DiskUtil :: formatForOS($path);
			while (($file = readdir($dh)) !== false) {
				$osfile = DiskUtil :: formatForOS($file);
				if (is_dir("$path/$file") && ($file != '.' && $file != '..')) {
					DiskUtil :: deldir("${path}/${osfile}");
				} else
					if ($file != '.' && $file != '..') {
						unlink("${path}/${osfile}");
					}
			}
			closedir($dh);
			return rmdir($path);
		}
	}

	public static function duplicateDir($dirPathSrc, $dirPathDest) {

	}

	/**
	 * Format a variable for operating-system usage. This method is recursive array safe.
	 *
	 * @param var        The variable to format
	 * @param absolute   Allow absolute paths (default=true) (optional)
	 *
	 * return var formatted variable
	 */
	function formatForOS($var, $absolute = true) {
		if (is_array($var)) {
			foreach ($var as $k => $v) {
				$var[$k] = DiskUtil :: formatForOS($v);
			}
		} else {
			static $cached;
			if ($cached == null)
				$cached = array ();

			if (isset ($cached[$var]))
				return $cached[$var];
			$orgVar = $var;

			$clean_array = array ();

			//if we're supporting absolute paths and the first charater is a slash and , then
			//an absolute path is passed
			$absolutepathused = ($absolute && substr($var, 0, 1) == '/');

			// Split the path at possible path delimiters.
			// Setting PREG_SPLIT_NOEMPTY eliminates double delimiters on the fly.
			$dirty_array = preg_split('#[/\\\\]#', $var, -1, PREG_SPLIT_NO_EMPTY);

			// now walk the path and do the relevant things
			foreach ($dirty_array as $current) {
				if ($current == '.') {
					// current path element is a dot, so we don't do anything
				}
				elseif ($current == '..') {
					// current path element is .., so we remove the last path in case of relative paths
					if (!$absolutepathused) {
						array_pop($clean_array);
					}
				} else {
					// current path element is valid, so we add it to the path
					$clean_array[] = $current;
				}
			}

			// build the path
			// should we use DIRECTORY_SEPARATOR here?
			$var = implode('/', $clean_array);
			//if an absolute path was passed to the function, we need to make it absolute again
			if ($absolutepathused) {
				$var = '/' . $var;
			}

			// Prepare var
			// needed for magic_quotes_runtime = 0
			$var = addslashes($var);

			$cached[$orgVar] = $var;
		}

		return $var;
	}

	/**
	 * Recursiveley create a directory path
	 *
	 * @param path        The path we wish to generate
	 * @param mode        The (UNIX) mode we wish to create the files with
	 *
	 * @return boolean The return code from mkdir()
	 */
	function mkdirs($path, $mode = null) {
		if (is_dir($path)) {
			return true;
		}

		$pPath = DiskUtil :: formatForOS(dirname($path));
		if (DiskUtil :: mkdirs($pPath, $mode) === false) {
			return false;
		}

		if ($mode) {
			return mkdir($path, $mode);
		} else {
			return mkdir($path);
		}
	}

	/**
	 * Read a file's contents and return them as a string. This method also
	 * opens and closes the file.
	 *
	 * @param filename    The file to read
	 * @param absolute   Allow absolute paths (default=false) (optional)
	 *
	 * @return string     The file's contents or false upon failure
	 */
	function readFile($filename, $absolute = false) {
		if (!strlen($filename)) {
			return false; //pn_exit ('DiskUtil::readFile: filename is empty');
		}

		return file_get_contents(DiskUtil :: formatForOS($filename, $absolute));
	}

	/**
	 * Get the basename of a filename
	 *
	 * @param filename     The filename to process
	 *
	 * @return string     The file's basename
	 */
	function getBasename($filename) {
		if (!$filename) {
			return false; //pn_exit('DiskUtil::getBasename: filename is empty');
		}

		return basename($filename);
	}

	/**
	 * Get the file's extension
	 *
	 * @param filename    The filename to process
	 * @param keepDot     whether or not to return the '.' with the extension
	 *
	 * @return string The file's extension
	 */
	function getExtension($filename, $keepDot = false) {
		if (!$filename) {
			return false; //pn_exit('DiskUtil::getExtension: filename is empty');
		}

		$p = strrpos($filename, '.');
		if ($p !== false) {
			if ($keepDot) {
				return substr($filename, $p);
			} else {
				return substr($filename, $p +1);
			}
		}

		return '';
	}

	/**
	 * Recursiveley generate a file listing
	 *
	 * @param rootPath      The root-path we wish to start at
	 * @param recurse       whether or not to recurse directories (optional) (default=true)
	 * @param relativePath  whether or not to list relative (vs abolute) paths (optional) (default=true)
	 * @param extension     The file extension to scan for (optional) (default=null)
	 * @param directories   Include directories in listing (optional) (default=true)
	 *
	 * @return boolean The return code from mkdir()
	 */
	function getFiles($rootPath, $recurse = true, $relativePath = true, $extension = null, $directories = true) {
		$files = array ();

		if (!file_exists($rootPath) || !is_dir($rootPath) || !is_readable($rootPath)) {
			return $files;
		}

		$el = ($extension ? strlen($extension) : 0);
		$dh = opendir($rootPath);
		while (($file = readdir($dh)) !== false) {
			if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn' && $file != 'index.html') {
				$path = "$rootPath/$file";

				if (!$directories && is_dir($path)) {
					continue;
				}

				if (!$extension || substr($file, - $el) == $extension) {
					if ($relativePath) {
						$files[] = $file;
					} else {
						$files[] = $path;
					}
				}

				if ($recurse && is_dir($path)) {
					$files = array_merge((array) $files, (array) DiskUtil :: getFiles($path, $recurse, $relativePath, $extension));
				}
			}
		}

		closedir($dh);
		return $files;
	}

	/**
	 * Upload a file
	 *
	 * @param key           The filename key to use in accessing the file data
	 * @param destination   The destination where the file should end up
	 * @param newName       The new name to give the file (optional) (default='')
	 *
	 * @return mixed        The return code from the fclose() call
	 */
	function uploadFile($key, $destination, $newName = '') {
		if (!$key) {
			return "DiskUtil::uploadFile called with invalid field key ...";
		}

		if (!$destination) {
			return "DiskUtil::uploadFile called with invalid destination ...";
		}

		$msg = '';
		if ($_FILES[$key]['name']) {
			$uploadfile = $_FILES[$key]['tmp_name'];
			$origfile = $_FILES[$key]['name'];
			$uploaddest = DiskUtil :: formatForOS("$destination/$origfile");

			if ($newName) {
				$uploaddest = DiskUtil :: formatForOS("$destination/$newName");
			}

			$rc = move_uploaded_file($uploadfile, $uploaddest);
			if (!$rc) {
				switch ($_FILES[$key]['error']) {
					case 1 :
						$msg = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
						break;
					case 2 :
						$msg = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form.';
						break;
					case 3 :
						$msg = 'The uploaded file was only partially uploaded.';
						break;
					case 4 :
						$msg = 'No file was uploaded.';
						break;
					case 5 :
						$msg = 'Uploaded file size 0 bytes';
						break;
				}
			}
		}

		return $msg;
	}

	/**
	* Copy a file, or recursively copy a folder and its contents
	*
	* @author      Aidan Lister <aidan@php.net>
	* @version     1.0.1
	* @link        http://aidanlister.com/repos/v/function.copyr.php
	* @param       string   $source    Source path
	* @param       string   $dest      Destination path
	* @return      bool     Returns TRUE on success, FALSE on failure
	*/
	public static function copyr($source, $dest) {
		// Check for symlinks
		if (is_link($source)) {
			return symlink(readlink($source), $dest);
		}

		// Simple copy for a file
		if (is_file($source)) {
			return copy($source, $dest);
		}

		// Make destination directory
		if (!is_dir($dest)) {
			mkdir($dest);
		}

		// Loop through the folder
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			// Skip pointers
			if ($entry == '.' || $entry == '..') {
				continue;
			}

			// Deep copy directories
			DiskUtil :: copyr("$source/$entry", "$dest/$entry");
		}

		// Clean up
		$dir->close();
		return true;
	}

}
?>
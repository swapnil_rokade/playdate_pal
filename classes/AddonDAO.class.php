<?php
include_once("BaseDAO.class.php");

class AddonDAO extends BaseDAO {

    public function  AddonDAO() {
    }
    
	
	public static function getAddon($id_addon) {
		$retorno = null;
		if(is_numeric($id_addon)) {
		    $sql = "SELECT * FROM pd_addons where id_addon=$id_addon";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Addon();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}


	public static function getAddonList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    if ((0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_addons t";
	    
	    //Order items
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by t.order asc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Addon();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
}
?>
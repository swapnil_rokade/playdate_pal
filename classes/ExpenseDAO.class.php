<?php
include_once("BaseDAO.class.php");

class ExpenseDAO extends BaseDAO {

    public function  ExpenseDAO() {
    }
    
    /**
     * 
     * @param mixed $id_parent
     * @param mixed $date_init
     * @param mixed $date_end
     * @return Expense[]
     */
    public static function getExpenseListByParent($id_parent, $date_init=null, $date_end=null) {
	    $retorno = array();
	    
	    $sql = "SELECT * FROM pd_expenses where id_parent=".$id_parent;
	    
	    if($date_init!=null) {
	        $sql.=" AND idate>='".BaseDAO::toMysqlDateFormat($date_init)."'";
	    }
	    if($date_end!=null) {
	        $sql.=" AND idate<='".BaseDAO::toMysqlDateFormat($date_end)."'";
	    }
	    
        $sql .= " order by id_expense desc";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Expense();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 * @param Expense $newExpense
	 */
	public static function createExpense($newExpense) {
	    if(is_numeric($newExpense->getId_parent()) && is_numeric($newExpense->getAmount())) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_expenses (id_parent, id_playdate, name, amount_playdate, amount_addons, idate) VALUES ("
	            .$newExpense->getId_parent().", ".$newExpense->getId_playdate().", '".$newExpense->getName()."', ".$newExpense->getAmount_playdate().", ".$newExpense->getAmount_addons().",  now())";
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
}
?>
<?php
include_once("BaseEntity.class.php");

class ChildrenRequest extends BaseEntity {
	
    protected $id_request_children, $id_request, $id_children, $num_children, $age, $idate, $udate;
    

    /**
    * Constructor
    */ 
    public function __construct( $id_request_children=-1) {
        $this->id_request_children = $id_request_children;
    }
   
    
	public function readFromRow($row){
	    if(isset($row['id_request_children'])) $this->setId_request_children($row['id_request_children']);
	    if(isset($row['id_request'])) $this->setId_request($row['id_request']);
	    if(isset($row['id_children'])) $this->setId_children($row['id_children']);
	    if(isset($row['num_children'])) $this->setNum_children($row['num_children']);
	    if(isset($row['age'])) $this->setAge($row['age']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
	public function getId_request_children()
    {
        return $this->id_request_children;
    }

    /**
     * @param mixed $id_request_children
     */
    public function setId_request_children($id_request_children)
    {
        $this->id_request_children = $id_request_children;
    }

    /**
     * @return mixed
     */
    public function getId_request()
    {
        return $this->id_request;
    }

    /**
     * @param mixed $id_request
     */
    public function setId_request($id_request)
    {
        $this->id_request = $id_request;
    }

    /**
     * @return mixed
     */
    public function getId_children()
    {
        return $this->id_children;
    }

    /**
     * @param mixed $id_children
     */
    public function setId_children($id_children)
    {
        $this->id_children = $id_children;
    }

    /**
     * @return mixed
     */
    public function getNum_children()
    {
        return $this->num_children;
    }

    /**
     * @param mixed $num_children
     */
    public function setNum_children($num_children)
    {
        $this->num_children = $num_children;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }

	
	   
}
?>
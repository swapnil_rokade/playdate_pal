<?php
include_once("BaseEntity.class.php");

class InvitationRequest extends BaseEntity {
    
    public static $STATUS_INACTIVE=0;
    public static $STATUS_ACTIVE=1;
    
    public static $PROFILE_STANDARD=0;
    public static $PROFILE_EXPERIENCED=1;
    
    
    protected $id_request_invitation, $name, $lastname, $username, $email, $zipcode, $password, $cellphone, $phone;
    
    protected $picture, $idate, $udate, $status, $slug;
    
    
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $cellphone
     */
    public function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
    * Constructor
    */ 
    public function __construct( $id_request_invitation=-1) {
        $this->id_request_invitation = $id_request_invitation;
    }
   
	public function getId_request_invitation() {
	    return $this->id_request_invitation;
	}
	public function setId_request_invitation($newId_request_invitation) {
	    $this->id_request_invitation = $newId_request_invitation;
	}
	
   
	/**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	public function readFromRow($row){
	    if(isset($row['id_request'])) { $this->setId_request_invitation($row['id_request']);}
	    if(isset($row['id_request_invitation'])) { $this->setId_request_invitation($row['id_request_invitation']);}
	    if(isset($row['name'])) { $this->setName($row['name']);}
	    if(isset($row['lastname'])) { $this->setLastname($row['lastname']);}
	    if(isset($row['username'])) { $this->setUsername($row['username']);}
	    if(isset($row['email'])) { $this->setEmail($row['email']);}
	    if(isset($row['zipcode'])) { $this->setZipcode($row['zipcode']);}
	    if(isset($row['password'])) { $this->setPassword($row['password']);}
	    if(isset($row['cellphone'])) { $this->setCellphone($row['cellphone']);}
	    if(isset($row['phone'])) { $this->setPhone($row['phone']);}
	    
	    if(isset($row['status'])) { $this->setStatus($row['status']);}
	    
	    if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
	/**
	 * Read full name
	 */
	public function getFullName() {
	    $fullname = $this->getName();
	    
	    if($this->getLastname()!="") {
	        $fullname.= " ".$this->getLastname();
	    }
	    
	    return $fullname;
	    
	}
	
    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }
    
    
	
} 
?>
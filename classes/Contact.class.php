<?php
include_once("BaseEntity.class.php");

class Contact extends BaseEntity {
	
    public static $TYPE_CONTACT = "contact";
    public static $TYPE_SPECIALIST = "specialist";
    public static $TYPE_HOST = "host";
    public static $TYPE_REQUEST = "request";
    
    
    protected $id_contact, $type, $name, $last_name, $email, $message, $zipcode, $experience, $company_name, $company_web, $company_address, $availability, $is_read, $read_date, $idate, $udate;
    
    protected $req_date, $req_time, $req_kids, $req_age, $req_id_parent;
    
   /**
    * Constructor
    */ 
    public function __construct( $id_contact=-1) {
        $this->id_contact = $id_contact;
    }
   
    
    
	public function readFromRow($row){
		if(isset($row['id_contact'])) $this->setId_contact($row['id_contact']);
		if(isset($row['type'])) $this->setType($row['type']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['last_name'])) $this->setLast_name($row['last_name']);
		if(isset($row['email'])) $this->setEmail($row['email']);
		if(isset($row['message'])) $this->setMessage($row['message']);
		
		if(isset($row['zipcode'])) $this->setZipcode($row['zipcode']);
		if(isset($row['experience'])) $this->setExperience($row['experience']);
		
		if(isset($row['company_name'])) $this->setCompany_name($row['company_name']);
		if(isset($row['company_web'])) $this->setCompany_web($row['company_web']);
		if(isset($row['company_address'])) $this->setCompany_address($row['company_address']);
		if(isset($row['availability'])) $this->setAvailability($row['availability']);
		
		if(isset($row['is_read'])) $this->setIs_read($row['is_read']);
		if(isset($row['read_date']) && ($row['read_date']!="0000-00-00 00:00:00")) $this->read_date=($this->toStringDateFormat($row['read_date']));
		
		if(isset($row['req_date'])) $this->setReq_date($row['req_date']);
		if(isset($row['req_time'])) $this->setReq_time($row['req_time']);
		if(isset($row['req_kids'])) $this->setReq_kids($row['req_kids']);
		if(isset($row['req_age'])) $this->setReq_age($row['req_age']);
		if(isset($row['req_id_parent'])) $this->setReq_id_parent($row['req_id_parent']);
		
		
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
    /**
     * @return mixed
     */
    public function getId_contact()
    {
        return $this->id_contact;
    }

    /**
     * @param mixed $id_contact
     */
    public function setId_contact($id_contact)
    {
        $this->id_contact = $id_contact;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLast_name()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLast_name($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getMessage($charlimit=null)
    {
        $retorno = $this->message;
        
        if (($charlimit!=null) && is_numeric($charlimit) && (strlen($retorno) > $charlimit)) {
            $retorno = $this->truncateString($retorno,$charlimit);
        }
        return $retorno;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getExperience($charlimit=null)
    {
        $retorno = $this->experience;
        
        if (($charlimit!=null) && is_numeric($charlimit) && (strlen($retorno) > $charlimit)) {
            $retorno = $this->truncateString($retorno,$charlimit);
        }
        
        return $retorno;
    }

    /**
     * @param mixed $experience
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    }

    /**
     * @return mixed
     */
    public function getCompany_name()
    {
        return $this->company_name;
    }

    /**
     * @param mixed $company_name
     */
    public function setCompany_name($company_name)
    {
        $this->company_name = $company_name;
    }

    /**
     * @return mixed
     */
    public function getCompany_web()
    {
        return $this->company_web;
    }

    /**
     * @param mixed $company_web
     */
    public function setCompany_web($company_web)
    {
        $this->company_web = $company_web;
    }

    /**
     * @return mixed
     */
    public function getCompany_address()
    {
        return $this->company_address;
    }

    /**
     * @param mixed $company_address
     */
    public function setCompany_address($company_address)
    {
        $this->company_address = $company_address;
    }

    /**
     * @return mixed
     */
    public function getAvailability($charlimit=null)
    {
        $retorno = $this->availability;
        
        if (($charlimit!=null) && is_numeric($charlimit) && (strlen($retorno) > $charlimit)) {
            $retorno = $this->truncateString($retorno,$charlimit);
        }
        
        return $retorno;
    }

    /**
     * @param mixed $availability
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getIs_read()
    {
        return $this->is_read;
    }

    /**
     * @param mixed $read
     */
    public function setIs_read($is_read)
    {
        $this->is_read = $is_read;
    }

    /**
     * @return mixed
     */
    public function getRead_date()
    {
        return $this->read_date;
    }

    /**
     * @param mixed $read_date
     */
    public function setRead_date($read_date)
    {
        $this->read_date = $read_date;
    }
    /**
     * @return mixed
     */
    public function getReq_date()
    {
        return $this->req_date;
    }

    /**
     * @param mixed $req_date
     */
    public function setReq_date($req_date)
    {
        $this->req_date = $req_date;
    }

    /**
     * @return mixed
     */
    public function getReq_time()
    {
        return $this->req_time;
    }

    /**
     * @param mixed $req_time
     */
    public function setReq_time($req_time)
    {
        $this->req_time = $req_time;
    }

    /**
     * @return mixed
     */
    public function getReq_kids()
    {
        return $this->req_kids;
    }

    /**
     * @param mixed $req_kids
     */
    public function setReq_kids($req_kids)
    {
        $this->req_kids = $req_kids;
    }

    /**
     * @return mixed
     */
    public function getReq_age()
    {
        return $this->req_age;
    }

    /**
     * @param mixed $req_age
     */
    public function setReq_age($req_age)
    {
        $this->req_age = $req_age;
    }

    /**
     * @return mixed
     */
    public function getReq_id_parent()
    {
        return $this->req_id_parent;
    }

    /**
     * @param mixed $req_id_parent
     */
    public function setReq_id_parent($req_id_parent)
    {
        $this->req_id_parent = $req_id_parent;
    }


	
}
?>
<?php
include_once("BaseDAO.class.php");

class PaymentDAO extends BaseDAO {

    public function  PaymentDAO() {
    }
    
    /**
     * 
     * @param number $id_payment
     * @return NULL|Payment
     */
    public static function getPayment($id_payment) {
        $retorno = null;
        
        $sql = "SELECT * FROM pd_payments where id_payment=".$id_payment;
        
        $link = getConnection();
        //Obtenemos los resultados
        $result = mysql_query($sql, $link);
        if($row = mysql_fetch_assoc($result)) {
            $newItem = new Payment();
            $newItem->readFromRow($row);
            $retorno = $newItem;
        }
        mysql_close($link);
        return $retorno;
    }    
    
    /**
     * 
     * @param number $id_parent
     * @param mixed $date_init
     * @param mixed $date_end
     * @return Payment[]
     */
    public static function getPaymentListByParent($id_parent, $date_init=null, $date_end=null) {
	    $retorno = array();
	    
	    $sql = "SELECT * FROM pd_payments where id_parent=".$id_parent;
	    
	    if($date_init!=null) {
	        $sql.=" AND idate>='".BaseDAO::toMysqlDateFormat($date_init)."'";
	    }
	    if($date_end!=null) {
	        $sql.=" AND idate<='".BaseDAO::toMysqlDateFormat($date_end)."'";
	    }
	    
        $sql .= " order by id_payment desc";
	    
        //echo("::::[$sql]::::::::");die();
        
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Payment();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    

	/**
	 *
	 * @param number $id_parent
	 * @param number $id_playdate
	 * @return Payment[]
	 */
	public static function getPaymentListByParentPlaydate($id_parent, $id_playdate) {
	    $retorno = array();
	    
	    $sql = "SELECT * FROM pd_payments where id_parent=".$id_parent." AND id_playdate=".$id_playdate;
	    
	    //echo("::::[$sql]::::::::");die();
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Payment();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 *
	 * @param number $id_parent
	 * @param mixed $date_init
	 * @param mixed $date_end
	 * @return Payment[]
	 */
	public static function getPaymentListByPlaydate($id_playdate, $date_init=null, $date_end=null) {
	    $retorno = array();
	    
	    $sql = "SELECT * FROM pd_payments where id_playdate=".$id_playdate;
	    
	    if($date_init!=null) {
	        $sql.=" AND idate>='".BaseDAO::toMysqlDateFormat($date_init)."'";
	    }
	    if($date_end!=null) {
	        $sql.=" AND idate<='".BaseDAO::toMysqlDateFormat($date_end)."'";
	    }
	    
	    $sql .= " order by id_parent desc";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Payment();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	/**
	 *
	 * @param number $id_specialist
	 * @param mixed $date_init
	 * @param mixed $date_end
	 * @return Payment[]
	 */
	public static function getPaymentListBySpecialist($id_specialist, $date_init=null, $date_end=null) {
	    $retorno = array();
	    
	    $sql = "SELECT * FROM pd_payments where id_specialist=".$id_specialist;
	    
	    if($date_init!=null) {
	        $sql.=" AND idate>='".BaseDAO::toMysqlDateFormat($date_init)."'";
	    }
	    if($date_end!=null) {
	        $sql.=" AND idate<='".BaseDAO::toMysqlDateFormat($date_end)."'";
	    }
	    
	    $sql .= " order by id_payment desc";
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Payment();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	/**
	 * @param Payment $newPayment
	 */
	public static function createParentPayment($newPayment) {
	    if(is_numeric($newPayment->getId_parent()) && is_numeric($newPayment->getAmount_playdate())) {
	        $link1 = getConnection();
	        
	        if($newPayment->getId_playdate()==null) { 
    	        $sql = "INSERT INTO pd_payments (id_parent, id_playdate, name, comment, amount_playdate, amount_addons, amount_paid,discount_amount,discount_code, status, idate) VALUES ("
    	            .$newPayment->getId_parent().", null, '".mysql_real_escape_string($newPayment->getName())."', '".mysql_real_escape_string($newPayment->getComment())."', ".$newPayment->getAmount_playdate().", ".$newPayment->getAmount_addons().",  ".$newPayment->getAmount_paid().", ".$newPayment->getDiscount_amount().",  ".$newPayment->getDiscount_code().", ".$newPayment->getStatus().",  now())";
	        } else {
	            $sql = "INSERT INTO pd_payments (id_parent, id_playdate, name, comment, amount_playdate, amount_addons, amount_paid, discount_amount,discount_code, status, idate) VALUES ("
	                .$newPayment->getId_parent().", ".$newPayment->getId_playdate().", '".mysql_real_escape_string($newPayment->getName())."', '".mysql_real_escape_string($newPayment->getComment())."', ".$newPayment->getAmount_playdate().", ".$newPayment->getAmount_addons().",  ".$newPayment->getAmount_paid().", ".$newPayment->getDiscount_amount().",  ".$newPayment->getDiscount_code().", ".$newPayment->getStatus().",  now())";
	        }
	            
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * @param Payment $newPayment
	 */
	public static function createSpecialistPayment($newPayment) {
	    if(is_numeric($newPayment->getId_specialist()) && is_numeric($newPayment->getAmount_playdate())) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_payments (id_specialist, id_playdate, name, comment, amount_playdate, amount_addons, amount_paid, status, idate) VALUES ("
	            .$newPayment->getId_specialist().", ".(($newPayment->getId_playdate()!=null)?$newPayment->getId_playdate():"null").", '".mysql_real_escape_string($newPayment->getName())."', '".mysql_real_escape_string($newPayment->getComment())."', ".$newPayment->getAmount_playdate().", ".$newPayment->getAmount_addons().",  ".$newPayment->getAmount_paid().",  ".$newPayment->getStatus().",  now())";
	            $result = mysql_query($sql, $link1);
	            mysql_close($link1);
	    }
	}
	
	
	public static function deletePayment($id_payment) {
	    $retorno = false;
	    
	    if (($id_payment!=null) && (is_numeric($id_payment))) {
	        //Remove specialist info
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_payments WHERE id_payment=" . $id_payment;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	

	
	public static function executeParentPayment($id_payment, $comment=null) {
	    $retorno = false;
	    
	    if (($id_payment!=null) && (is_numeric($id_payment))) {
	        $payment = PaymentDAO::getPayment($id_payment);
	        
	        //CHARGE TO PARENT
	        if(($payment->getId_parent()!=null) && ( ($payment->getStatus()==Payment::$STATUS_PENDING) || ($payment->getStatus()==Payment::$STATUS_PARTIAL)) ) {
	            //get parent
	            $parent = ParentDAO::getParent($payment->getId_parent());
	            
	            //set amount paid
	            $total_amount = ($payment->getAmount_playdate()+$payment->getAmount_addons()) - $payment->getDiscount_amount();
	            $amount_to_pay = $total_amount -$payment->getAmount_paid();
	            $newStatus = Payment::$STATUS_COMPLETED;
	            
	            if($parent->getCredit()<$amount_to_pay) {
	                $amount_to_pay = $parent->getCredit();
	                $newStatus = Payment::$STATUS_PARTIAL;
	                if($comment!=null) {
	                    $comment .= " No enough credit.";
	                } else {
	                    $comment = "No enough credit.";
	                }
	                $payment->setComment($comment);
	            } else {
	                if($comment!=null) {
	                    $comment .= " Completed.";
	                } else {
	                    $comment = "Completed.";
	                }
	                
	                $newStatus = Payment::$STATUS_COMPLETED;
	                $payment->setComment($comment); 
	            }
	            
	            $payment->setAmount_paid($payment->getAmount_paid()+$amount_to_pay);
	            $payment->setStatus($newStatus);
	            
	            $newCredit = ($parent->getCredit()- $amount_to_pay);
	            
	            ParentDAO::updateParentCredit($parent->getId_parent(), $newCredit);
	            
    	        $link = getConnection();
    	        $sql_frm = "UPDATE pd_payments SET amount_paid=".$payment->getAmount_paid().", comment='".mysql_real_escape_string($payment->getComment())."', status=".$newStatus.", udate=now()  WHERE id_payment=" . $id_payment;
    	        $result_frm = mysql_query($sql_frm, $link);
    	        mysql_close($link);
    	        $retorno = true;
	        } else if(($payment->getId_specialist()!=null) && ( ($payment->getStatus()==Payment::$STATUS_PENDING) || ($payment->getStatus()==Payment::$STATUS_PARTIAL)) ) {
	            //PAYMENT TO SPECIALIST

	        }
	    }
	    
	    return $retorno;
	}
	
	/**
	 * Rollback parent playdate - Cash back
	 * @param number $id_payment
	 * @param string $auxComment Optional additional comment for the operation
	 * @return boolean
	 */
	public static function rollbackParentPayment($id_payment, $auxComment="") {
	    $retorno = false;
	    
	    if (($id_payment!=null) && (is_numeric($id_payment))) {
	        $payment = PaymentDAO::getPayment($id_payment);
	        
	        //RETURN TO PARENT
	        if(($payment->getId_parent()!=null) && ( ($payment->getStatus()==Payment::$STATUS_COMPLETED) || ($payment->getStatus()==Payment::$STATUS_PARTIAL)) ) {
	            //get parent
	            $parent = ParentDAO::getParent($payment->getId_parent());
	            
	            $newStatus = Payment::$STATUS_ROLLBACK;
	            $operationComment = "Rolled back";
	            if(($auxComment!=null) && ($auxComment!="")) {
	                $operationComment = "Rolled back: ".$auxComment;
	            }
	            $payment->setComment($operationComment);
	            
	            $newCredit = ($parent->getCredit()+ $payment->getAmount_paid());
	            
	            ParentDAO::updateParentCredit($parent->getId_parent(), $newCredit);
	            
	            $link = getConnection();
	            $sql_frm = "UPDATE pd_payments SET comment='".mysql_real_escape_string($payment->getComment())."', status=".$newStatus.", udate=now()  WHERE id_payment=" . $id_payment;
	            $result_frm = mysql_query($sql_frm, $link);
	            mysql_close($link);
	            $retorno = true;
	        } 
	    }
	    
	    return $retorno;
	}
	
	/**
	 *
	 * @param number $id_playdate
	 * @param number $status
	 */
	public static function updatePaymentStatus($id_payment, $amountPaid, $newStatus) {
	    if(is_numeric($id_payment)) {
	        $link1 = getConnection();
	        $sql_frm = "UPDATE pd_payments SET amount_paid=".$amountPaid.", status=".$newStatus.", udate=now()  WHERE id_payment=" . $id_payment;
	        //echo("::::[$sql_frm]:::::");die();
	        $result = mysql_query($sql_frm, $link1);
	        mysql_close($link1);
	    }
	}
	
}
?>
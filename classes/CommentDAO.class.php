<?php
include_once("BaseDAO.class.php");

class CommentDAO extends BaseDAO {

    public function  CommentDAO() {
    }
    
    public static function countComment() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_playdate_comments";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	public static function getCommentListByPlaydate($id_playdate, $page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $link = getConnection();
	    
	    if (($page==null)|| (0 == $page)||($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elementsPerPage * ($page - 1); # primer registro a mostrar
	    $max = $elementsPerPage; # elementos a mostrar como maximo
	    
	    $sql = "SELECT * FROM pd_playdate_comments WHERE id_playdate=".$id_playdate;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_comment desc";
	    }
	    
	    //paging
	    $sql .=" LIMIT $start, $max";
	    
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Comment();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getComment($id_comment) {
		$retorno = null;
		if(is_numeric($id_rating)) {
			$sql = "SELECT * FROM pd_playdate_comments where id_comment=$id_comment";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Comment();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    


	/**
	 * 
	 * @param Comment $newComment
	 * @return NULL|Rating
	 */
	public static function createComment($newComment) {
	    $retorno = null;
	    
	    if (($newComment!=null) && ($newComment->getId_playdate()!=null)) {
	        //Save comment
	        $link = getConnection();
	        
	        $sql_frm = "INSERT INTO pd_playdate_comments (id_parent, id_specialist, id_playdate, comment, idate) " .
	   	        "VALUES (".
	   	        "".(($newComment->getId_parent()!=null)?mysql_real_escape_string($newComment->getId_parent()):"null").", ".
	   	        "".(($newComment->getId_specialist()!=null)?mysql_real_escape_string($newComment->getId_specialist()):"null").", ".
	   	        "".mysql_real_escape_string($newComment->getId_playdate()).", ".
	   	        "".(($newComment->getComment()!=null)?("'".mysql_real_escape_string($newComment->getComment())."'"):"null").", ".
	   	        "now()".
	   	        ")";
	        
	        //echo("<pre>::::[$sql_frm]::::::</pre>");die();
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        
	        
	        //Obtenemos el id del nuevo comment
	        $link2 = getConnection();
	        $sql2 = "SELECT * FROM pd_playdate_comments WHERE id_playdate=".$newComment->getId_playdate()." order by id_comment desc limit 1";
	        //echo("<pre>::::[$sql2]::::::</pre>");//die();
	        $result2 = mysql_query($sql2, $link2);
	        
	        $newComment->readFromRow(mysql_fetch_assoc($result2));
	        
	        if (is_numeric($newComment->getId_comment()) && ($newComment->getId_comment()>0)) {
	            $retorno = $newComment;
	        }
	        mysql_close($link2);
	        
	    }
	    return $retorno;
	}
	
	
	/**
	 * 
	 * @param Comment $editComment
	 * @return NULL|Rating
	 */
	public static function updateComment($editComment) {
	    $retorno = null;
	    if (($editComment!=null) && (is_numeric($editComment->getId_comment()))) {
	        
	        //Updating comment info
	        $link = getConnection();
	        
	        $sql_frm = "UPDATE pd_playdate_comments SET " .
	   	        "comment=" .(($editComment->getComment()!=null)?("'".mysql_real_escape_string($editComment->getComment())."'"):"null"). ", " .
	   	        "udate=now() " .
	   	        "WHERE id_comment=" . $editComment->getId_comment();
	        
	        //echo("::::::[$sql_frm]:::::::");die();
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = CommentDAO::getComment($editComment->getId_comment());
	    }
	    
	    return $retorno;
	}
	
	public static function deleteComment($id_comment) {
	    $retorno = false;
	    
	    if (($id_comment!=null) && (is_numeric($id_comment))) {
	        //Remove comment
	        $link = getConnection();
	        $sql_frm = "DELETE FROM pd_playdate_comments WHERE id_comment=" . $id_comment;
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	
	
}
?>
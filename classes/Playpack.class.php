<?php
include_once("BaseEntity.class.php");

class Playpack extends BaseEntity {
	
    public static $TYPE_1CHILD = 1;
    public static $TYPE_2CHILD = 2;
    
    public static $ICON_LITTLE_PACK = "/img/ico/gray/0343-tags.svg";
    public static $ICON_MEDIUM_PACK = "/img/ico/gray/0333-bag2.svg";
    public static $ICON_BIG_PACK = "/img/ico/gray/0336-cart-full.svg";
    
    protected $id_playpack, $name, $help, $type, $amount, $description, $icon, $idate, $udate;
    
    /**
    * Constructor
    */ 
    public function __construct( $id_playpack=-1) {
        $this->id_playpack = $id_playpack;
    }
   
	public function readFromRow($row){
		if(isset($row['id_playpack'])) $this->setId_playpack($row['id_playpack']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['help'])) $this->setHelp($row['help']);
		if(isset($row['type'])) $this->setType($row['type']);
		if(isset($row['amount'])) $this->setAmount($row['amount']);
		if(isset($row['icon'])) $this->setIcon($row['icon']);
		if(isset($row['description'])) $this->setDescription($row['description']);
		
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
	
    /**
     * @return mixed
     */
    public function getId_playpack()
    {
        return $this->id_playpack;
    }

    /**
     * @param mixed $id_playpack
     */
    public function setId_playpack($id_playpack)
    {
        $this->id_playpack = $id_playpack;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * @param mixed $help
     */
    public function setHelp($help)
    {
        $this->help = $help;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }
    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }




    
	
}
?>
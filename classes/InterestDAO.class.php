<?php
include_once("BaseDAO.class.php");

class InterestDAO extends BaseDAO {

    public function  InterestDAO() {
    }
    
    public static function countInterests() {
		$link = getConnection();
		$sql = "select count(*) FROM pd_interests";
		$rs = mysql_query($sql, $link);
		list($total) = mysql_fetch_row($rs);
		mysql_close($link);
		return $total;
	}

	public static function getInterestsList($page=1, $elementsPerPage=1000, $sortfield="", $sorttype="") {
		$retorno = array();
 
		if ((0 == $page)||($page == "")) {
			$page = 1; 
		}
		
		$start = $elementsPerPage * ($page - 1); # primer registro a mostrar
		$max = $elementsPerPage; # elementos a mostrar como maximo
		
		$sql = "SELECT * FROM pd_interests";

		//Ordenacion
		if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
			$sql .= " order by $sortfield";
			if((strcasecmp($sorttype, "ASC")==0)) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
			$sql .= " order by type asc";
		}

		//paging
		$sql .=" LIMIT $start, $max";
		
		$link = getConnection();
		//Obtenemos los resultados
		$result = mysql_query($sql, $link);	
		while($row = mysql_fetch_assoc($result)) {
			$newItem = new Interest();
			$newItem->readFromRow($row);
			$retorno[] = $newItem;
		}	
		mysql_close($link);
		return $retorno;	
	}    

	public static function getInterestsByPlaydate($id_playdate, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT i.* FROM pd_interests i, pd_playdate_interests p where i.id_interest=p.id_interest and p.id_playdate=$id_playdate";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by type asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	public static function getInterestsByGroup($id_group, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT i.* FROM pd_interests i, pd_group_interests p where i.id_interest=p.id_interest and p.id_group=$id_group";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by type asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}    
	
	public static function getInterestsByRequest($id_request, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    //$sql = "SELECT q.* FROM pd_playdate_requests r, pd_request_interests q where r.id_request=q.id_request and r.id_request=".$id_request;
	    $sql = "SELECT i.* FROM pd_interests i, pd_request_interests p where i.id_interest=p.id_interest and p.id_request=$id_request";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by type asc";
	    }
	    
	    //echo("::[$sql]:"); die();
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getInterestsByChildren($id_children, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT i.* FROM pd_interests i, pd_children_interests p where i.id_interest=p.id_interest and p.id_children=$id_children";
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by type asc";
	    }
	    
	    //echo("::[$sql]:"); die();
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	public static function getInterestsIdByRequest($id_request, $sortfield="", $sorttype="") {
	    $retorno = array();
	    
	    $sql = "SELECT q.* FROM pd_playdate_requests r, pd_request_interests q where r.id_request=q.id_request and r.id_request=".$id_request;
	    
	    //Ordenacion
	    if(isset($sortfield) && ($sortfield!=null) && ($sortfield!="")) {
	        $sql .= " order by $sortfield";
	        if((strcasecmp($sorttype, "ASC")==0)) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by type asc";
	    }
	    
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem->getId_interest();
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getInterestsIdByChildren($id_children) {
	    $retorno = array();
	    
	    $sql = "SELECT * FROM pd_children_interests where id_children=".$id_children;
	    
	    $link = getConnection();
	    //Obtenemos los resultados
	    $result = mysql_query($sql, $link);
	    while($row = mysql_fetch_assoc($result)) {
	        $newItem = new Interest();
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem->getId_interest();
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getInterest($id_interest) {
		$retorno = null;
		if(is_numeric($id_interest)) {
			$sql = "SELECT * FROM pd_interests where id_interest=$id_interest";
			$link1 = getConnection();
			//Read results
			$result = mysql_query($sql, $link1);
			while($row = mysql_fetch_assoc($result)) {
				$newItem = new Interest();
				$newItem->readFromRow($row);
				$retorno = $newItem;
			}	
			
			mysql_close($link1);
			
		}
		return $retorno;	
	}    

	
	public static function deleteAllRequestInterests($id_request) {
	    if(is_numeric($id_request)) {
	        $sql = "DELETE FROM pd_request_interests WHERE id_request=".$id_request;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	        
	    }
	}
	
	public static function createRequestInterest($id_request, $id_interest) {
	    if(is_numeric($id_interest) && is_numeric($id_request)) {
	        $sql = "INSERT INTO pd_request_interests (id_request, id_interest, idate) VALUES (".$id_request.", ".$id_interest.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createPlaydateInterest($id_playdate, $id_interest) {
	    if(is_numeric($id_interest) && is_numeric($id_playdate)) {
	        $sql = "INSERT INTO pd_playdate_interests (id_playdate, id_interest, idate) VALUES (".$id_playdate.", ".$id_interest.", now())";
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function deleteAllChildrenInterests($id_children) {
	    if(is_numeric($id_children)) {
	        $sql = "DELETE FROM pd_children_interests WHERE id_children=".$id_children;
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function createChildrenInterest($id_children, $id_interest) {
	    
	    if(is_numeric($id_interest) && is_numeric($id_children)) {
	        
	        $sql = "INSERT INTO pd_children_interests (id_children, id_interest, idate) VALUES (".$id_children.", ".$id_interest.", now())";
	        //echo(":::::[$sql]::::"); die();
	        $link1 = getConnection();
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	
	
	/*
	public static function createCategoria($newCategoria) {
		$retorno = null;

		if(isset($newCategoria) && ($newCategoria->getNombre()!="")) {	
			//Almacenamos la información de categoria
			$link = getConnection();
			$sql_frm = "INSERT INTO fm_categorias (id_categoria_padre, nombre, descripcion, estado, orden, slug, ifecha) " .
				"values (".(is_numeric($newCategoria->getId_categoria_padre())?$newCategoria->getId_categoria_padre():"null").",'".$newCategoria->getNombre()."','".$newCategoria->getDescripcion()."','".$newCategoria->getEstado()."', ".$newCategoria->getOrden().", '".$newCategoria->getSlug()."', now())";
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			
			//Obtenemos el id de esa categoria			
			$link = getConnection();
			$sql_frm = "SELECT * FROM fm_categorias WHERE nombre='".$newCategoria->getNombre()."' order by id_categoria desc limit 1";
			$result_frm = mysql_query($sql_frm, $link);
			
			$newCategoria->readFromRow(mysql_fetch_assoc($result_frm));
			
			if(is_numeric($newCategoria->getId_categoria())) {
				$retorno = $newCategoria;
			}
			mysql_close($link);
		}
		return $retorno;	
	}    

	public static function updateCategoria($editCategoria) {
		$retorno = null;
		if(isset($editCategoria) && ($editCategoria->getNombre()!="") && (is_numeric($editCategoria->getId_categoria()))) {	
			//Almacenamos la información de categoria
			$link = getConnection();
			$sql_frm = "UPDATE fm_categorias SET id_categoria_padre=".$editCategoria->getId_categoria_padre().", " .
						 "nombre='".$editCategoria->getNombre()."', " .
						 "descripcion='".$editCategoria->getDescripcion()."', " .
						 "estado='".$editCategoria->getEstado()."', " .
						 "orden='".$editCategoria->getOrden()."', " .
						 "slug='".$editCategoria->getSlug()."', " .
						 "ufecha=now() " .
						"WHERE id_categoria=".$editCategoria->getId_categoria();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			
			$retorno = CategoriaDAO::getCategoria($editCategoria->getId_categoria());
		}
		
		return $retorno;	
	}    

	public static function deleteCategoria($deleteCategoria) {
		$retorno = false;

		if(isset($deleteCategoria) && (is_numeric($deleteCategoria->getId_categoria()))) {	
			//Eliminamos la información de categoria
			$link = getConnection();
			$sql_frm = "DELETE FROM fm_categorias WHERE id_categoria=".$deleteCategoria->getId_categoria();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);
			
			$retorno = true;
			
		}
		
		return $retorno;	
	}
	*/

}
?>
<?php
include_once("BaseEntity.class.php");

class Rating extends BaseEntity {
	
    protected $id_rating, $id_parent, $id_specialist, $id_playdate, $value, $comment_specialist, $comment_playdate, $idate, $udate;
    
    /**
    * Constructor
    */ 
    public function __construct( $id_rating=-1) {
        $this->id_rating = $id_rating;
    }
   
    
	public function readFromRow($row){
		if(isset($row['id_rating'])) $this->setId_rating($row['id_rating']);
		if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
		if(isset($row['id_specialist'])) $this->setId_specialist($row['id_specialist']);
		if(isset($row['id_playdate'])) $this->setId_playdate($row['id_playdate']);
		if(isset($row['value'])) $this->setValue($row['value']);
		if(isset($row['comment_specialist'])) $this->setComment_specialist($row['comment_specialist']);
		if(isset($row['comment_playdate'])) $this->setComment_playdate($row['comment_playdate']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_rating()
    {
        return $this->id_rating;
    }

    /**
     * @param mixed $id_rating
     */
    public function setId_rating($id_rating)
    {
        $this->id_rating = $id_rating;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getId_specialist()
    {
        return $this->id_specialist;
    }

    /**
     * @param mixed $id_specialist
     */
    public function setId_specialist($id_specialist)
    {
        $this->id_specialist = $id_specialist;
    }

    /**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getComment_specialist()
    {
        return $this->comment_specialist;
    }

    /**
     * @param mixed $comment_specialist
     */
    public function setComment_specialist($comment_specialist)
    {
        $this->comment_specialist = $comment_specialist;
    }

    /**
     * @return mixed
     */
    public function getComment_playdate()
    {
        return $this->comment_playdate;
    }

    /**
     * @param mixed $comment_playdate
     */
    public function setComment_playdate($comment_playdate)
    {
        $this->comment_playdate = $comment_playdate;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param mixed
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param mixed
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }

	
    
}
?>
<?php
include_once("BaseEntity.class.php");

class Interest extends BaseEntity {
	
    public static $TYPE_TRAINING = 1;
    public static $TYPE_ART = 2;
    public static $TYPE_PLAY = 3;
    public static $TYPE_CULTURAL = 4;
    
    protected $id_interest, $name, $type, $description, $idate, $udate, $slug;
    
    
   
   /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId_interest()
    {
        return $this->id_interest;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $id_interest
     */
    public function setId_interest($id_interest)
    {
        $this->id_interest = $id_interest;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @param mixed $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
    * Constructor
    */ 
    public function __construct( $id_interest=-1) {
    	$this->id_interest = $id_interest;
    }
   
	public function readFromRow($row){
		if(isset($row['id_interest'])) $this->setId_interest($row['id_interest']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['type'])) $this->setType($row['type']);
		if(isset($row['description'])) $this->setDescription(stripslashes($row['description']));
		if(isset($row['slug'])) $this->setSlug($row['slug']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}	
}
?>
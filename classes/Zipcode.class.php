<?php
include_once("BaseEntity.class.php");

class Zipcode extends BaseEntity {
	
    protected $id_zipcode, $id_neighborhood, $zipcode, $idate, $udate;
    
    
    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getId_zipcode()
    {
        return $this->id_zipcode;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param mixed $id_zipcode
     */
    public function setId_zipcode($id_zipcode)
    {
        $this->id_zipcode = $id_zipcode;
    }

    /**
     * @param mixed $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @param mixed $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }

    /**
    * Constructor
    */ 
    public function __construct( $id_zipcode=-1) {
        $this->id_zipcode = $id_zipcode;
    }
   
	public function readFromRow($row){
		if(isset($row['id_zipcode'])) $this->setId_zipcode($row['id_zipcode']);
		if(isset($row['id_neighborhood'])) $this->setId_neighborhood($row['id_neighborhood']);
		if(isset($row['zipcode'])) $this->setZipcode($row['zipcode']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_neighborhood()
    {
        return $this->id_neighborhood;
    }

    /**
     * @param mixed $id_neighborhood
     */
    public function setId_neighborhood($id_neighborhood)
    {
        $this->id_neighborhood = $id_neighborhood;
    }
	
}
?>
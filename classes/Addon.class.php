<?php
include_once("BaseEntity.class.php");

class Addon extends BaseEntity {
	
    protected $id_addon, $name, $order, $idate, $udate;
    
    
   
        /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

        /**
    * Constructor
    */ 
    public function __construct( $id_addon=-1) {
        $this->id_addon = $id_addon;
    }
   
	public function readFromRow($row){
		if(isset($row['id_addon'])) $this->setId_addon($row['id_addon']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['order'])) $this->setOrder($row['order']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_addon()
    {
        return $this->id_addon;
    }

    /**
     * @param mixed $id_addon
     */
    public function setId_addon($id_addon)
    {
        $this->id_addon = $id_addon;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }


	
}
?>
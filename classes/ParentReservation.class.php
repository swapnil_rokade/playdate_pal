<?php
include_once("BaseEntity.class.php");

class ParentReservation extends BaseEntity {
	
    public static $STATUS_RESERVED = 0;
    public static $STATUS_CANCELED = 1;
    
    protected $id_reservation, $id_parent, $id_playdate, $num_spots, $idate, $udate, $status;
    

    /**
    * Constructor
    */ 
    public function __construct( $id_reservation=-1) {
        $this->id_reservation = $id_reservation;
    }
   
    
	public function readFromRow($row){
		if(isset($row['id_reservation'])) $this->setId_reservation($row['id_reservation']);
		if(isset($row['id_parent'])) $this->setId_parent($row['id_parent']);
		if(isset($row['id_playdate'])) $this->setId_playdate($row['id_playdate']);
		if(isset($row['num_spots'])) $this->setNum_spots($row['num_spots']);
		if(isset($row['status'])) $this->setStatus($row['status']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
    /**
     * @return mixed
     */
    public function getId_reservation()
    {
        return $this->id_reservation;
    }

    /**
     * @param mixed $id_reservation
     */
    public function setId_reservation($id_reservation)
    {
        $this->id_reservation = $id_reservation;
    }

    /**
     * @return mixed
     */
    public function getId_parent()
    {
        return $this->id_parent;
    }

    /**
     * @param mixed $id_parent
     */
    public function setId_parent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getId_playdate()
    {
        return $this->id_playdate;
    }

    /**
     * @param mixed $id_playdate
     */
    public function setId_playdate($id_playdate)
    {
        $this->id_playdate = $id_playdate;
    }

    /**
     * @return mixed
     */
    public function getNum_spots()
    {
        return $this->num_spots;
    }

    /**
     * @param mixed $num_spots
     */
    public function setNum_spots($num_spots)
    {
        $this->num_spots = $num_spots;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    
}
?>
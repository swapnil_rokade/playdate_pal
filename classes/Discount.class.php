<?php
include_once("BaseEntity.class.php");
class Discount extends BaseEntity {
	
    protected $id_discount_type, $name, $help, $type, $amount, $description, $icon, $idate, $udate;

    /**
    * Constructor
    */ 
    public function __construct( $id_discount_type=-1) {
        $this->id_discount_type = $id_discount_type;
    }
   
	public function readTypeFromRow($row){
        if(isset($row['id_discount_type'])) $this->setId_discount_type($row['id_discount_type']);
		if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['type'])) $this->setType($row['type']);
		if(isset($row['discount'])) $this->setDiscount($row['discount']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
    public function readUsageFromRow($row){
        if(isset($row['name'])) $this->setName($row['name']);
		if(isset($row['lastname'])) $this->setLastName($row['lastname']);
		if(isset($row['discount_code'])) $this->setDiscountCode($row['discount_code']);
        if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
	}

    public function readFromRow($row){

        if(isset($row['id_discount'])) $this->setId_discount($row['id_discount']);
        if(isset($row['discount_code'])) $this->setdiscountCode($row['discount_code']);
		if(isset($row['name'])) $this->setName($row['name']);
        if(isset($row['start_date']) && ($row['start_date']!="0000-00-00 00:00:00")) $this->start_date=($this->toStringDateFormat($row['start_date']));
        if(isset($row['end_date']) && ($row['end_date']!="0000-00-00 00:00:00")) $this->end_date=($this->toStringDateFormat($row['end_date']));
        //if(isset($row['start_date'])) $this->setStartDate($row['start_date']);
        //if(isset($row['end_date'])) $this->setEndDate($row['end_date']);
		if(isset($row['type'])) $this->setType($row['type']);
		if(isset($row['description'])) $this->setDescription($row['description']);
		if(isset($row['idate']) && ($row['idate']!="0000-00-00 00:00:00")) $this->idate=($this->toStringDateFormat($row['idate']));
		if(isset($row['udate']) && ($row['udate']!="0000-00-00 00:00:00")) $this->udate=($this->toStringDateFormat($row['udate']));
	}
	
    public function readDetailsRow($row){

        if(isset($row['user_id'])) $this->set_userId($row['user_id']);
        if(isset($row['discount_code'])) $this->setdiscountCode($row['discount_code']);
	}
    

    /**
     * @param mixed $id_discount
     */
    public function setId_discount($id_discount)
    {
        $this->id_discount = $id_discount;
    }

    

    /**
     * @return mixed
     */
    public function getId_discount()
    {
        return $this->id_discount;
    }

    /**
     * @param mixed $discount_code
     */
    public function setdiscountCode($discount_code)
    {
        $this->discount_code = $discount_code;
    }

    /**
     * @return mixed
     */
    public function getdiscountCode()
    {
        return $this->discount_code;
    }

     /**
     * @param mixed $discount_code
     */
    public function set_userId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function get_userId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $id_discount_type
     */
    public function setId_discount_type($id_discount_type)
    {
        $this->id_discount_type = $id_discount_type;
    }

    /**
     * @return mixed
     */
    public function getId_discount_type()
    {
        return $this->id_discount_type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastname;
    }
    
    /**
     * @return mixed
     */
    public function getTypeName()
    {
        return $this->discount-type;
    }

    
    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }


    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }
    
    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @return mixed
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * @param mixed $help
     */
    public function setHelp($help)
    {
        $this->help = $help;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getIdate()
    {
        return $this->idate;
    }

    /**
     * @param Ambigous <unknown, mixed> $idate
     */
    public function setIdate($idate)
    {
        $this->idate = $idate;
    }

    /**
     * @return mixed
     */
    public function getUdate()
    {
        return $this->udate;
    }

    /**
     * @param Ambigous <unknown, mixed> $udate
     */
    public function setUdate($udate)
    {
        $this->udate = $udate;
    }
    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }
    
    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }
    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }




    
	
}
?>

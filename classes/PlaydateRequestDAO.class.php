<?php
include_once ("BaseDAO.class.php");

class PlaydateRequestDAO extends BaseDAO {

    function PlaydateRequestDAO() {
	}

	public static function countPlaydateRequests() {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_playdate_requests", $link) or die("Error counting Requests");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function countPlaydateRequestsSearch($query) {
		$link = getConnection();
		$rs = mysql_query("SELECT COUNT(*) FROM pd_playdate_requests WHERE info like '%$query%' or location like '%$query%'", $link) or die("Error counting playdate requests");
		list ($total) = mysql_fetch_row($rs);
		return $total;
	}

	public static function getPlaydateRequestsList($page, $elemsPerPage, $sortfield, $sorttype) {
		$retorno = array ();
		$link = getConnection();

		$sql = "SELECT * FROM pd_playdate_requests ";
		//Ordenacion
		if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
			$sql .= " order by $sortfield";
			if (strcasecmp($sorttype, "ASC")) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		} else {
		    $sql .= " order by id_request DESC";
		    
		}

		//Start and range
		if ((0 == $page) || ($page == "")) {
			$page = 1;
		}

		$start = $elemsPerPage * ($page -1); # first
		$max = $elemsPerPage; # elements per page

		//paging
		$sql .= " LIMIT $start, $max";

		//results
		$result = mysql_query($sql, $link);
		while ($row = mysql_fetch_assoc($result)) {
		    $newItem = new PlaydateRequest(-1);
		    $newItem->readFromRow($row);
		    $retorno[] = $newItem;
		}
		mysql_close($link);
		return $retorno;
	}

	
	public static function getPlaydateRequestsListByParent($id_parent, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_requests WHERE id_parent=".mysql_real_escape_string($id_parent)."";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getOpenPlaydateRequestsListByParent($id_parent, $page=1, $elemsPerPage=20, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_requests WHERE status=".PlaydateRequest::$STATUS_REQUEST_OPEN." AND id_parent=".mysql_real_escape_string($id_parent)."";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	//Priya
	public static function getNewOpenPlaydateRequestsListByParent($id_parent, $page=1, $elemsPerPage=20, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM request_playdate WHERE status=1 AND id_parent=".mysql_real_escape_string($id_parent)."";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	       // $newItem = new PlaydateRequest(-1);
	       // $newItem->readFromRow($row);
	        $retorno[] = $row;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	public static function getOpenPlaydateRequestsList($page=1, $elemsPerPage=20, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_requests WHERE status=".PlaydateRequest::$STATUS_REQUEST_OPEN;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 * 
	 * @param number $page
	 * @param number $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return PlaydateRequest[]
	 */
	public static function getOpenPlaydateRequestsJobBoardList($page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_requests WHERE status=".PlaydateRequest::$STATUS_REQUEST_OPEN." AND public_job_board=1";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 *
	 * @param number $page
	 * @param number $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return PlaydateRequest[]
	 */
	public static function getUpcomingOpenPlaydateRequestsJobBoardList($page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT r.* FROM pd_playdate_requests r WHERE r.status=".PlaydateRequest::$STATUS_REQUEST_OPEN." AND (r.public_job_board=1 OR r.id_request NOT IN (select id_request FROM pd_request_specialist)) and r.date >= CURRENT_DATE()";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	/**
	 *
	 * @param number $page
	 * @param number $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return PlaydateRequest[]
	 */
	public static function getUpcomingOpenPlaydateRequestsJobBoardListForSpecialist($id_specialist, $page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT r.* FROM pd_playdate_requests r WHERE r.status=".PlaydateRequest::$STATUS_REQUEST_OPEN." AND (r.public_job_board=1 OR r.id_request NOT IN (select id_request FROM pd_request_specialist) OR r.id_request IN (select id_request FROM pd_request_specialist WHERE id_specialist=".$id_specialist.")) and r.date >= CURRENT_DATE()";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	/**
	 *
	 * @param number $page
	 * @param number $elemsPerPage
	 * @param mixed $sortfield
	 * @param mixed $sorttype
	 * @return PlaydateRequest[]
	 */
	public static function getOpenPlaydateRequestsListBySpecialist($id_specialist, $page=1, $elemsPerPage=100, $sortfield=null, $sorttype=null) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT r.* FROM pd_playdate_requests r, pd_request_specialist s WHERE r.id_request=s.id_request AND s.id_specialist=".$id_specialist." AND status=".PlaydateRequest::$STATUS_REQUEST_OPEN;
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function getPlaydateRequestsListBySpecialist($id_specialist, $page, $elemsPerPage, $sortfield, $sorttype) {
	    $retorno = array ();
	    $link = getConnection();
	    
	    $sql = "SELECT * FROM pd_playdate_requests WHERE id_specialist=".mysql_real_escape_string($id_specialist)."";
	    //Ordenacion
	    if (isset ($sortfield) && ($sortfield != null) && ($sortfield != "")) {
	        $sql .= " order by $sortfield";
	        if (strcasecmp($sorttype, "ASC")) {
	            $sql .= " ASC";
	        } else {
	            $sql .= " DESC";
	        }
	    } else {
	        $sql .= " order by id_request DESC";
	        
	    }
	    
	    //Start and range
	    if ((0 == $page) || ($page == "")) {
	        $page = 1;
	    }
	    
	    $start = $elemsPerPage * ($page -1); # first
	    $max = $elemsPerPage; # elements per page
	    
	    //paging
	    $sql .= " LIMIT $start, $max";
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new PlaydateRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    return $retorno;
	}
	
	
	public static function getPlaydateRequest($id_request) {
		$retorno = array ();
		
		if(is_numeric($id_request) && ($id_request>=0)) {
			$link = getConnection();
	
			$sql = "SELECT s.* FROM pd_playdate_requests s WHERE id_request=".mysql_real_escape_string($id_request);
	
			//reading results
			$result = mysql_query($sql, $link);
			if ($row = mysql_fetch_assoc($result)) {
			    $newItem = new PlaydateRequest(-1);
			    $newItem->readFromRow($row);
			    $retorno = $newItem;
			}
			mysql_close($link);
		}
		return $retorno;
	}

	

	/**
	 * 
	 * @param PlaydateRequest $newPlaydateRequest
	 * @return NULL|PlaydateRequest
	 */
	public static function createPlaydateRequest($newPlaydateRequest) {
		$retorno = null;

		if (isset ($newPlaydateRequest) && ($newPlaydateRequest->getId_parent() != "")) {
			//Save playdate
			$link = getConnection();

			$sql_frm = "INSERT INTO pd_playdate_requests (id_parent, type, id_group, location, travel_pickup, travel_dropoff, date, time_init, time_end, budget_init, budget_end, extra_kids, info, num_supporters, public_job_board, status, idate) " .
			"VALUES (".
			         "".mysql_real_escape_string($newPlaydateRequest->getId_parent()).", ".
			         "".(($newPlaydateRequest->getType()!=null)?mysql_real_escape_string($newPlaydateRequest->getType()):"null").", ".
			         "".(($newPlaydateRequest->getId_group()!=null)?mysql_real_escape_string($newPlaydateRequest->getId_group()):"null").", ".
			         "'".mysql_real_escape_string($newPlaydateRequest->getLocation())."', ".
			         "'".mysql_real_escape_string($newPlaydateRequest->getTravel_pickup())."', ".
			         "'".mysql_real_escape_string($newPlaydateRequest->getTravel_dropoff())."', ".
			         "'".BaseDAO::toMysqlDateFormat($newPlaydateRequest->getDate())."', ".
			         "".mysql_real_escape_string($newPlaydateRequest->getTime_init()).", ".
			         "".mysql_real_escape_string($newPlaydateRequest->getTime_end()).", ".
			         "".(($newPlaydateRequest->getBudget_init()!=null)?mysql_real_escape_string($newPlaydateRequest->getBudget_init()):"null").", ".
			         "".(($newPlaydateRequest->getBudget_end()!=null)?mysql_real_escape_string($newPlaydateRequest->getBudget_end()):"null").", ".
			         "".(($newPlaydateRequest->getExtra_kids()!=null)?mysql_real_escape_string($newPlaydateRequest->getExtra_kids()):"null").", ".
			         "'".mysql_real_escape_string($newPlaydateRequest->getInfo())."', ".
			         "".(($newPlaydateRequest->getNum_supporters()!=null)?mysql_real_escape_string($newPlaydateRequest->getNum_supporters()):"null").", ".
			         "".(($newPlaydateRequest->getPublic_job_board()!=null)?mysql_real_escape_string($newPlaydateRequest->getPublic_job_board()):0).", ".
			         "".(($newPlaydateRequest->getStatus()!=null)?mysql_real_escape_string($newPlaydateRequest->getStatus()):0).", ".
			         "now()".
			")";

			//echo("<pre>::::[$sql_frm]::::::</pre>");die();
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			
			
			//Obtenemos el id del nuevo request
			$link2 = getConnection();
			$sql2 = "SELECT * FROM pd_playdate_requests WHERE id_parent=".$newPlaydateRequest->getId_parent()." and idate>date_sub(now(), interval 30 second) order by id_request desc limit 1";
			//echo("<pre>::::[$sql2]::::::</pre>");//die();
			$result2 = mysql_query($sql2, $link2);
			
			$newPlaydateRequest->readFromRow(mysql_fetch_assoc($result2));
			
			if (is_numeric($newPlaydateRequest->getId_request()) && ($newPlaydateRequest->getId_request()>0)) {
			    $retorno = $newPlaydateRequest;
			} 
			mysql_close($link2);

		}
		return $retorno;
	}

	//Priya
	public static function createNewPlaydateRequest($newPlaydateRequest) {
		$retorno = null;

		if (isset ($newPlaydateRequest) && ($newPlaydateRequest->getId_parent() != "")) {
			//Save playdate
			$link = getConnection();

			$sql_frm = "INSERT INTO request_playdate (id_parent, playdate_type, date, time, group_size, age_range, no_of_playdate_per_week, which_day_of_week, location_in_mind, preferred_location, preferred_location_type, theme_of_playdate, pay_extra_bucks_for, notes, like_to_invite_guest, information_of_guests, playdate_lead_by_specialist,specialist_first_name,specialist_last_name,other_details,how_did_you_hear_about_us,first_name,last_name,email_address,status,creation_date,updation_date) " .
			"VALUES (".
			         "".mysql_real_escape_string($newPlaydateRequest->getId_parent()).", ".
			         "".mysql_real_escape_string($newPlaydateRequest->getPlaydate_type()).", ".
					 "".(($newPlaydateRequest->getDate()!=null || $newPlaydateRequest->getDate()!="")?mysql_real_escape_string($newPlaydateRequest->getDate()):"null").", ".
					 "".(($newPlaydateRequest->getTime()!=null || $newPlaydateRequest->getTime()!="")?mysql_real_escape_string($newPlaydateRequest->getTime()):"null").", ".
			         "'".(($newPlaydateRequest->getGroup_size()!=null || $newPlaydateRequest->getGroup_size()!="")?mysql_real_escape_string($newPlaydateRequest->getGroup_size()):"null")."', ".
			         "'".(($newPlaydateRequest->getAge_range()!=null || $newPlaydateRequest->getAge_range()!="")?mysql_real_escape_string($newPlaydateRequest->getAge_range()):"null")."', ".
			         "'".(($newPlaydateRequest->getNo_of_playdate()!=null || $newPlaydateRequest->getNo_of_playdate()!="")?mysql_real_escape_string($newPlaydateRequest->getNo_of_playdate()):"null")."', ".
			         "'".(($newPlaydateRequest->getWhich_day_week()!=null || $newPlaydateRequest->getWhich_day_week()!="")?mysql_real_escape_string($newPlaydateRequest->getWhich_day_week()):"null")."', ".
			         "".(($newPlaydateRequest->getLocation_in_mind()!=null || $newPlaydateRequest->getLocation_in_mind()!="")?mysql_real_escape_string($newPlaydateRequest->getLocation_in_mind()):"null").", ".
			         "".(($newPlaydateRequest->getPreferred_location()!=null || $newPlaydateRequest->getPreferred_location()!="")?mysql_real_escape_string($newPlaydateRequest->getPreferred_location()):"null").", ".
			         "".(($newPlaydateRequest->getPreferred_location_type()!=null || $newPlaydateRequest->getPreferred_location_type()!="")?mysql_real_escape_string($newPlaydateRequest->getPreferred_location_type()):"null").", ".
					 "".(($newPlaydateRequest->getTheme_of_playdate()!=null || $newPlaydateRequest->getTheme_of_playdate()!="")?mysql_real_escape_string($newPlaydateRequest->getTheme_of_playdate()):"null").", ".
					 "".(($newPlaydateRequest->getPay_extra()!=null || $newPlaydateRequest->getPay_extra()!="")?mysql_real_escape_string($newPlaydateRequest->getPay_extra()):"null").", ".
					 "".(($newPlaydateRequest->getNotes()!=null || $newPlaydateRequest->getNotes()!="")?mysql_real_escape_string($newPlaydateRequest->getNotes()):"null").", ".
					 "".(($newPlaydateRequest->getLike_to_invite()!=null || $newPlaydateRequest->getLike_to_invite()!="")?mysql_real_escape_string($newPlaydateRequest->getLike_to_invite()):"null").", ".
					 "".(($newPlaydateRequest->getInformation_of_guest()!=null || $newPlaydateRequest->getInformation_of_guest()!="")?mysql_real_escape_string($newPlaydateRequest->getInformation_of_guest()):"null").", ".
					 "".(($newPlaydateRequest->getPlaydate_lead_by_specialist()!=null || $newPlaydateRequest->getPlaydate_lead_by_specialist()!="")?mysql_real_escape_string($newPlaydateRequest->getPlaydate_lead_by_specialist()):"null").", ".
					 "".(($newPlaydateRequest->getSpecialist_first_name()!=null || $newPlaydateRequest->getSpecialist_first_name()!="")?mysql_real_escape_string($newPlaydateRequest->getSpecialist_first_name()):"null").", ".
					 "".(($newPlaydateRequest->getSpecialist_last_name()!=null || $newPlaydateRequest->getSpecialist_last_name()!="")?mysql_real_escape_string($newPlaydateRequest->getSpecialist_last_name()):"null").", ".
					 "".(($newPlaydateRequest->getOther_details()!=null || $newPlaydateRequest->getOther_details()!="")?mysql_real_escape_string($newPlaydateRequest->getOther_details()):"null").", ".
					 "".(($newPlaydateRequest->getHear_about_us()!=null || $newPlaydateRequest->getHear_about_us()!="")?mysql_real_escape_string($newPlaydateRequest->getHear_about_us()):"null").", ".
					 "".(($newPlaydateRequest->getFirst_name()!=null || $newPlaydateRequest->getFirst_name()!="")?mysql_real_escape_string($newPlaydateRequest->getFirst_name()):"null").", ".
					 "".(($newPlaydateRequest->getLast_name()!=null || $newPlaydateRequest->getLast_name()!="")?mysql_real_escape_string($newPlaydateRequest->getLast_name()):"null").", ".
					 "".(($newPlaydateRequest->getEmail_address()!=null || $newPlaydateRequest->getEmail_address()!="")?mysql_real_escape_string($newPlaydateRequest->getEmail_address()):"null").", ".
			         "".(($newPlaydateRequest->getStatus()!=null || $newPlaydateRequest->getStatus()!="")?mysql_real_escape_string($newPlaydateRequest->getStatus()):0).", ".
					 "".date('Y-m-d H:i:s').",".
					 "".date('Y-m-d H:i:s')."".
			")";

			//echo("<pre>::::[$sql_frm]::::::</pre>");die();
			
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			
			
			//Obtenemos el id del nuevo request
			$link2 = getConnection();
			$sql2 = "SELECT * FROM request_playdate WHERE id_parent=".$newPlaydateRequest->getId_parent()." and creation_date>date_sub(now(), interval 30 second) order by id_request desc limit 1";
			//echo("<pre>::::[$sql2]::::::</pre>");//die();
			$result2 = mysql_query($sql2, $link2);
			
			$newPlaydateRequest->readFromRow(mysql_fetch_assoc($result2));
			
			if (is_numeric($newPlaydateRequest->getId_request()) && ($newPlaydateRequest->getId_request()>0)) {
			    $retorno = $newPlaydateRequest;
			} 
			mysql_close($link2);

		}
		return $retorno;
	}

	
	/**
	 * 
	 * @param PlaydateRequest $editPlaydateRequest
	 * @return NULL|PlaydateRequest
	 */
	public static function updatePlaydateRequest($editPlaydateRequest) {
		$retorno = null;
		if (isset ($editPlaydateRequest) && (is_numeric($editPlaydateRequest->getId_request()))) {
			
			//Updating playdate info
			$link = getConnection();
			
			$sql_frm = "UPDATE pd_playdate_requests SET location='" . mysql_real_escape_string($editPlaydateRequest->getLocation()) . "', " .
			"type=" .(($editPlaydateRequest->getType()!=null)?mysql_real_escape_string($editPlaydateRequest->getType()):"null"). ", " .
			"id_group=" .(($editPlaydateRequest->getId_group()!=null)?mysql_real_escape_string($editPlaydateRequest->getId_group()):"null"). ", " .
			"location=" .(($editPlaydateRequest->getLocation()!=null)?("'".mysql_real_escape_string($editPlaydateRequest->getLocation())."'"):"null"). ", " .
			"travel_pickup=" .(($editPlaydateRequest->getTravel_pickup()!=null)?("'".mysql_real_escape_string($editPlaydateRequest->getTravel_pickup())."'"):"null"). ", " .
			"travel_dropoff=" .(($editPlaydateRequest->getTravel_dropoff()!=null)?("'".mysql_real_escape_string($editPlaydateRequest->getTravel_dropoff())."'"):"null"). ", " .
			"date=". (($editPlaydateRequest->getDate()!=null)?("'" . BaseDAO::toMysqlDateFormat($editPlaydateRequest->getDate()) . "'"):"null"). ", " .
			"time_init=". (($editPlaydateRequest->getTime_init()!=null)?("'" . $editPlaydateRequest->getTime_init() . "'"):"null") . ", " .
			"time_end=". (($editPlaydateRequest->getTime_end()!=null)?("'" . $editPlaydateRequest->getTime_end() . "'"):"null") . ", " .
			"budget_init=". (($editPlaydateRequest->getBudget_init()!=null)?("'" . $editPlaydateRequest->getBudget_init() . "'"):"null") . ", " .
			"budget_end=". (($editPlaydateRequest->getBudget_end()!=null)?("'" . $editPlaydateRequest->getBudget_end() . "'"):"null") . ", " .
			"extra_kids=". (($editPlaydateRequest->getExtra_kids()!=null)?("'" . $editPlaydateRequest->getExtra_kids() . "'"):"null") . ", " .
			"info=". (($editPlaydateRequest->getInfo()!=null)?("'" . mysql_real_escape_string($editPlaydateRequest->getInfo()) . "'"):"null") . ", " .
			"num_supporters=". (($editPlaydateRequest->getNum_supporters()!=null)?("'" . $editPlaydateRequest->getNum_supporters() . "'"):"null") . ", " .
			"public_job_board=". (($editPlaydateRequest->getPublic_job_board()!=null)?("'" . $editPlaydateRequest->getPublic_job_board() . "'"):"0") . ", " .
			"status=". (($editPlaydateRequest->getStatus()!=null)?("'" . $editPlaydateRequest->getStatus() . "'"):PlaydateRequest::$STATUS_REQUEST_OPEN) . ", " .
			"udate=now() " .
			"WHERE id_request=" . $editPlaydateRequest->getId_request();

			//echo("::::::[$sql_frm]:::::::");die();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = PlaydateRequestDAO::getPlaydateRequest($editPlaydateRequest->getId_request());
		}

		return $retorno;
	}

	
	
	public static function addChildToParentRequest($id_request, $id_children, $num_children=1, $age="null") {
	    if(is_numeric($id_request)) {
	        if($id_children==null) {
	            $id_children ="null";
	        }
	        
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_request_children (id_request, id_children, num_children, age, idate) VALUES (".$id_request.", ".$id_children.", ".$num_children.", ".$age.", now())";
	        //echo(":::[".$sql."]:::");
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	public static function deleteAllRequestChildren($id_request) {
	    if(is_numeric($id_request)) {
	        $link1 = getConnection();
	        $sql = "DELETE FROM pd_request_children WHERE id_request=".$id_request;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 *
	 * @param mixed $id_request
	 * @return ChildrenRequest[]
	 */
	public static function getChildrenRequests($id_request) {
	    $retorno = array ();
	    $link = getConnection();
	    $sql = "select c.* FROM pd_request_children c WHERE c.id_request=".$id_request;
	    
	    //results
	    $result = mysql_query($sql, $link);
	    while ($row = mysql_fetch_assoc($result)) {
	        $newItem = new ChildrenRequest(-1);
	        $newItem->readFromRow($row);
	        $retorno[] = $newItem;
	    }
	    mysql_close($link);
	    
	    return $retorno;
	}
	
	
	/**
	 * 
	 * @param number $id_request
	 * @param number $id_specialist
	 */
	public static function addSpecialistToParentRequest($id_request, $id_specialist) {
	    if(is_numeric($id_request) && is_numeric($id_specialist)) {
	        $link1 = getConnection();
	        $sql = "INSERT INTO pd_request_specialist (id_request, id_specialist, idate) VALUES (".$id_request.", ".$id_specialist.", now())";
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	       // echo("<pre>::::[$sql]::::::</pre>"); die();
	    }
	}
	
	/**
	 * 
	 * @param number $id_request
	 */
	public static function deleteAllRequestSpecialist($id_request) {
	    if(is_numeric($id_request)) {
	        $link1 = getConnection();
	        $sql = "DELETE FROM pd_request_specialist WHERE id_request=".$id_request;
	        $result = mysql_query($sql, $link1);
	        mysql_close($link1);
	    }
	}
	
	/**
	 * Close Playdate Request By ID
	 * @param number $id_request
	 * @return boolean
	 */
	public static function closePlaydateRequest($id_request) {
	    $retorno = false;
	    
	    if (($id_request!=null) && (is_numeric($id_request))) {
	        //set request as closed
	        $link = getConnection();
	        $sql_frm = "UPDATE pd_playdate_requests SET status=".PlaydateRequest::$STATUS_REQUEST_CLOSED." WHERE id_request=" . $id_request;
	        //echo(":::[$sql_frm]:::::");die();
	        
	        $result_frm = mysql_query($sql_frm, $link);
	        mysql_close($link);
	        
	        $retorno = true;
	    }
	    
	    return $retorno;
	}
	
	public static function deletePlaydateRequest($deletePlaydateRequest) {
		$retorno = false;

		if (isset ($deletePlaydateRequest) && (is_numeric($deletePlaydateRequest->getId_request()))) {
			//Remove specialist info
			$link = getConnection();
			$sql_frm = "DELETE FROM pd_playdate_requests WHERE id_request=" . $deletePlaydateRequest->getId_request();
			$result_frm = mysql_query($sql_frm, $link);
			mysql_close($link);

			$retorno = true;
		}

		return $retorno;
	}

}
?>
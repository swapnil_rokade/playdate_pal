<?php 
/**
 * PLAYDATE - GROUP PUBLIC PROFILE 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$group = null;
if(isset($_REQUEST["id_group"]) && ($_REQUEST["id_group"]!=null)) {
    $group = GroupDAO::getGroup($_REQUEST["id_group"]);
}

if($group==null) {
    header("Location: /groups.php");
}

$groupMembers = 0; 
$groupMembers += GroupDAO::countGroupParents($group->getId_group());
$groupMembers += GroupDAO::countGroupSpecialists($group->getId_group());

if(isset($_REQUEST["action"])) {
    
    switch ($_REQUEST["action"]) {
        
        case "invite_parent": {
            if(isset($_SESSION["parent_id"]) && ($_SESSION["parent_id"]!=null)) {
                $parentGrp = ParentDAO::getParent($_SESSION["parent_id"]);
                if(($parentGrp!=null) && ($parentGrp->getId_parent()>0)) {
                    if($group->getPrivacity()==Group::$PRIVACITY_PRIVATE) {
                        //If private - we create request 
                        GroupDAO::createGroupParentRequest($group->getId_group(), $parentGrp->getId_parent());
                        
                        //Request sent - confirmation
                        MailUtils::sendRequestGroup($parentGrp->getEmail(), $parentGrp->getName(), $group->getName());
                        
                        //Notification to all group admins parents
                        $grpAdmins = ParentDAO::getParentsListByGroupAdmin($group->getId_group(), 1, 10, null, null);
                        if($grpAdmins!=null) {
                            foreach($grpAdmins as $auxAdm) {
                                MailUtils::sendRequestGroupToAdmin($auxAdm->getEmail(), $auxAdm->getName(), $parentGrp->getName(), $group->getName());
                            }
                        }
                        
                        //Notification to all group admins specialists
                        $grpAdmins = SpecialistDAO::getSpecialistListByGroupAdmin($group->getId_group(), 1, 10, null, null);
                        if($grpAdmins!=null) {
                            foreach($grpAdmins as $auxAdm) {
                                MailUtils::sendRequestGroupToAdmin($auxAdm->getEmail(), $auxAdm->getName(), $parentGrp->getName(), $group->getName());
                            }
                        }
                        
                        
                    } else if($group->getPrivacity()==Group::$PRIVACITY_PUBLIC) {
                        //If public, connect directly
                        GroupDAO::parentJoinGroup($group->getId_group(), $parentGrp->getId_parent(), false);
                        MailUtils::sendGroupAccept($parentGrp->getEmail(), $parentGrp->getName(), $group->getName(), $group->getId_group());
                        
                        header("Location: /groups-dashboard.php?id_group=".$group->getId_group());
                    }
                }
            }
            break;
        }
        
        case "invite_specialist": {
            if(isset($_SESSION["spec_id"]) && ($_SESSION["spec_id"]!=null)) {
                $specialistGrp = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
                if(($specialistGrp!=null) && ($specialistGrp->getId_specialist()>0)) {
                    if($group->getPrivacity()==Group::$PRIVACITY_PRIVATE) {
                        //If private, create request
                        GroupDAO::createGroupSpecialistRequest($group->getId_group(), $specialistGrp->getId_specialist());
                    } else if($group->getPrivacity()==Group::$PRIVACITY_PUBLIC) {
                        //If public, join directly
                        GroupDAO::specialistJoinGroup($group->getId_group(), $specialistGrp->getId_specialist(), false);
                        header("Location: /groups-dashboard.php?id_group=".$group->getId_group());
                    }
                }
            }
            break;
        }
    
    }
}

$grpPicture = $group->getPicture();
if(($grpPicture==null) || ($grpPicture=="")) {
    $grpPicture = "/img/default-img-group-profile.jpg";
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $group->getName() ?> - Group Profile - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page submit-itinerary public-profile<?php echo($isConnected?" connected":"") ?>">
    
    <?php include ('components/menu.php'); ?>


    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1><?php echo $group->getName() ?></h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <div class="col-lg-3 sidebar  my-dashboard">
          <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $grpPicture ?>" alt="<?php echo $group->getName() ?>">
                </div>
              </div>

        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">

          <div class="row">

            

            <div class="col-lg-4">
              <p class="titulo">Profile</p>
              <div class="data">
                <p><strong class="big"><?php echo $group->getName() ?></strong></p>
                <p><?php echo $group->getZipcode() ?></p>
              </div>       
            </div>
            <div class="col-lg-4">
              <p class="titulo">Members</p>
              <div class="data">
                <p><strong class="big"><?php echo $groupMembers ?></strong></p>
              </div>       
            </div>
            <?php 
            if($isConnected) {
                $ingroup = false;
                $requested = false;
                $declined = false;
                if($isParent) {
                    $action = "invite_parent";
                    $ingroup = GroupDAO::isParentInGroup($_SESSION["parent_id"], $group->getId_group());
                    if(!$ingroup) {
                        $requested = GroupDAO::isParentInGroupRequest($_SESSION["parent_id"], $group->getId_group());
                        $declined = GroupDAO::isParentInGroupRequestDeclined($_SESSION["parent_id"], $group->getId_group());
                    }
                    
                } else if($isSpecialist) {
                    $action = "invite_specialist";
                    $ingroup = GroupDAO::isSpecialistInGroup($_SESSION["spec_id"], $group->getId_group());
                    if(!$ingroup) {
                        $requested = GroupDAO::isSpecialistInGroupRequests($_SESSION["spec_id"], $group->getId_group());
                        $declined = GroupDAO::isSpecialistInGroupRequestsDeclined($_SESSION["spec_id"], $group->getId_group());
                    }
                    
                }
                if(!$ingroup) {
                    if($requested) { //requested - disable button
                        $message = "Invitation requested";
                        if($declined) {$message = "Request declined";}
            ?>
            <div class="col-lg-4">
              <button class="btn btn-md btn-primary bt-save-profile disabled" style="background-color:#878787; border-color:#878787" type="button" disabled="disabled"><?php echo $message ?></button>
            </div>
            <?php 
                    } else { // not requested - form
                        $strBtnJoin = "Request Invite";
                        if($group->getPrivacity()==Group::$PRIVACITY_PRIVATE) {
                            $strBtnJoin = "Request Invite";
                        } else if($group->getPrivacity()==Group::$PRIVACITY_PUBLIC) {
                            $strBtnJoin = "Join Group";
                        }
                        
            ?>
            <div class="col-lg-4">
               <form name="frm_req" action="/group-public-profile.php" method="post">
               <input type="hidden" name="action" value="<?php echo $action?>" />
               <input type="hidden" name="id_group" value="<?php echo $group->getId_group()?>" />
              <button class="btn btn-md btn-primary bt-save-profile" type="submit"><?php echo $strBtnJoin ?></button>
              </form>
            </div>
            <?php 
                    }
            
                } //if not ingroup
            } 
            ?>
          </div>

          <div class="row row-tabs">

            <div class="col-lg-8">
              <p class="titulo">About Us</p>
              <div class="data">
                <p><?php echo $group->getDescription() ?></p>
              </div>       
            </div>
          </div>


        </div>
        <!-- Content -->
      </div>

    </div>

    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

  </body>

</html>

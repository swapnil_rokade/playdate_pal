[<?php
/**
 * PLAYDATE - GROUPS SUGGESTIONS
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$sql = "select id_group as id_location, name from pd_groups";

if($isSpecialist) {
    $specialistSearch = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);
    if($specialistSearch!=null) {
        $sql = "select g.id_group as id_location, g.name from pd_groups g, pd_group_specialists s WHERE g.id_group=s.id_group AND s.id_specialist=".$specialistSearch->getId_specialist();
    }
}

if($isParent) {
    $parentSearch = ParentDAO::getParent($_SESSION["parent_id"]);
    if($parentSearch!=null) {
        $sql = "select g.id_group as id_location, g.name from pd_groups g, pd_group_parents p WHERE g.id_group=p.id_group AND p.id_parent=".$parentSearch->getId_parent();
    }
}



$link = getConnection();
//Obtenemos los resultados
$result = mysql_query($sql, $link);
$i=0;
$total=mysql_num_rows($result);
while($row = mysql_fetch_assoc($result)) {
    $i++;
    $id_location = $row['id_location'];
    $name = $row['name'];
    
    
?>{
    "id_location":<?php echo $id_location ?>,
    "name": "<?php echo $name ?>"
}<?php echo(($i<$total)?",":"" ); ?>
<?php   
}
mysql_close($link);
?>]
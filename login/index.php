<?php
/***
 * PLAYDATE - Login
 * 
 * @param user
 * @param password
 * @param operatiom   (session.login | session.logout)
 **/
include_once("../classes/all_classes.php");
include ("../common.php");
include_once("../connection.php");

$welcomeShow = false;
$_SESSION['welcome'] = 0;

if (isset($_REQUEST['action']) && ($_REQUEST['action'] == "reset-password") && isset($_REQUEST['inputEmailRecovery']) && ($_REQUEST['inputEmailRecovery'] != "")) {
    //RESET PASSWORD
    $resetparent = null;
    $resetspecialist = null;
    
    $resetparent = ParentDAO::getParentByUsernameOrEmail($_REQUEST['inputEmailRecovery']);
    
    if ($resetparent == null) { // No parent. Let's try specialist
        
        $resetspecialist = SpecialistDAO::getSpecialistByUsernameOrEmail($_REQUEST['inputEmailRecovery']);
        
        
        if ($resetspecialist != null) {
            //usuario activo, reset password
            $newpassword = Utils::generateNewPassword();
            SpecialistDAO::changePassword($resetspecialist, $newpassword);
            
            MailUtils::sendPasswordReset($resetspecialist->getEmail(), $resetspecialist->getName(), $newpassword);

            header("Location: /?reset=1");
        }
    } else { // Parent found

        $newpassword = Utils::generateNewPassword();
        ParentDAO::changePassword($resetparent, $newpassword);
        
        MailUtils::sendPasswordReset($resetparent->getEmail(), $resetparent->getName(), $newpassword);
        
        header("Location: /?reset=1");
    }

} else  if (isset($_REQUEST['inputEmail']) && ($_REQUEST['inputEmail'] != "")) {
    $parent = null;
    $specialist = null;
    $_SESSION['login'] = 1;
    $parent = ParentDAO::getParentByEmailPassword($_REQUEST['inputEmail'], $_REQUEST['inputPassword']);
    if ($parent == null) { 
        $parent = ParentDAO::getParentByUsernamePassword($_REQUEST['inputEmail'], $_REQUEST['inputPassword']);
    } 
    
    if ($parent == null) { // No parent. Let's try specialist
        
        $specialist = SpecialistDAO::getSpecialistByEmailPassword($_REQUEST['inputEmail'], $_REQUEST['inputPassword']);
        if ($specialist == null) {
            $specialist = SpecialistDAO::getSpecialistByUsernamePassword($_REQUEST['inputEmail'], $_REQUEST['inputPassword']);
        }
    
        if ($specialist != null) {
            // usuario activo, validacion ok
            $welcomeShow = true;
            
            $_SESSION['spec_id'] = $specialist->getId_specialist();
            $_SESSION['spec_name'] = $specialist->getFullName();
            $_SESSION['spec_profile'] = $specialist->getProfile();
            $_SESSION['welcome'] = 1;
            
            $dest_redirect = "/";
            if (isset($_REQUEST["backurl"])) {
                $dest_redirect = $_REQUEST["backurl"];
            }
            header("Location:" . $dest_redirect);
        }
    } else { // Parent found
        $welcomeShow = true;
        
        $_SESSION['parent_id'] = $parent->getId_parent();
        $_SESSION['parent_name'] = $parent->getFullName();
        $_SESSION['welcome'] = 1;
        
        //$dest_redirect = "/";
        $completed = ParentDAO::isCompletedProfile($parent->getId_parent());
		if(!$completed) {
		    $_SESSION['checkLogin'] = 1;
            $dest_redirect = "/my-dashboard.php#nav-profile";
        }
        else{
            $dest_redirect = "/welcome.php";
        }
        if (isset($_REQUEST["backurl"])) {
            $dest_redirect = $_REQUEST["backurl"];
        }
        
        
        
        header("Location:" . $dest_redirect);
    }
    
    if (($specialist == null) && ($parent == null)) {
        header("Location: /?error=1");
    } 
}
?>

<?php 
/**
 * PLAYDATE - SPECIALIST DASHBOARD 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isSpecialist) {
    header("Location: /");
}

$specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);

$specProfile = "Lead";
if($specialist->getProfile()==Specialist::$PROFILE_LEAD) {
    $specProfile = "Lead"; 
} else if($specialist->getProfile()==Specialist::$PROFILE_SUPPORTER) {
    $specProfile = "Supporter";
} else if($specialist->getProfile()==Specialist::$PROFILE_NURSE) {
    $specProfile = "Nurse";
}

//Selected Playdate to show in detail
$selectedPastPlaydate = null;
$selectedPlaydate = null;
$paramPlaydate = new Playdate(); 
$paramPlaydate->readFromRow($_REQUEST); 
if($paramPlaydate->getId_playdate()>0) { 
    $paramPlaydate = PlaydateDAO::getPlaydate($paramPlaydate->getId_playdate());
    
    if(($paramPlaydate!=null) && ($paramPlaydate->getId_playdate()>0)) {
        $selectedPlaydate = $paramPlaydate;
    }
}

if(isset($_REQUEST["id_past_playdate"]) && ($_REQUEST["id_past_playdate"]!=null)) {
    $paramPastPlaydate = PlaydateDAO::getPlaydate($_REQUEST["id_past_playdate"]);
    if(($paramPastPlaydate!=null) && ($paramPastPlaydate->getId_playdate()>0)) {
        $selectedPastPlaydate = $paramPastPlaydate;
    }
}

$nominated = false;
$pay_updated = false;
$parentinvited = false;
$passwordchanged = false;


if(isset($_REQUEST['pay_updated']) && ($_REQUEST['pay_updated']==1)) {
    $pay_updated = true;
}

$earned = 0;
$payments = PaymentDAO::getPaymentListBySpecialist($specialist->getId_specialist());
foreach ($payments as $auxPay) {
    $earned += $auxPay->getAmount_playdate();
}

$invited = false;

if(isset($_REQUEST["action"])) { 
    switch ($_REQUEST["action"]) {
        
        case "invite-friends": {
            if(!empty($_POST['inviteFriendsPopup']) && ($selectedPlaydate!=null)) {
                $name="Someone";
                if($specialist!=null) {
                    $name = $specialist->getName();
                }
                
                $event_place = $selectedPlaydate->getLoc_address().", ".$selectedPlaydate->getLoc_city().", ".$selectedPlaydate->getLoc_state();
                
                foreach($_POST['inviteFriendsPopup'] as $auxfriend) {
                    $friend = ParentDAO::getParentByUsernameOrEmail($auxfriend);
                    if($friend!=null) {
                        MailUtils::sendPlaydateInviteFriend($friend->getEmail(), $auxfriend, $name, $selectedPlaydate->getName(), Utils::dateFormat($selectedPlaydate->getDate()), $event_place, Utils::get12hourFormat($selectedPlaydate->getTime_init()), Utils::get12hourFormat($selectedPlaydate->getTime_end()), "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
                    } else if(filter_var($auxfriend, FILTER_VALIDATE_EMAIL)) {
                        MailUtils::sendPlaydateInviteFriend($auxfriend, $auxfriend, $name, $selectedPlaydate->getName(), Utils::dateFormat($selectedPlaydate->getDate()), $event_place, Utils::get12hourFormat($selectedPlaydate->getTime_init()),Utils::get12hourFormat($selectedPlaydate->getTime_end()), "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
                    }
                    
                }
                $invited = true;
            }
            
            
            break;
        }
        
        case "update_payment": {
            $updtSpec = new Specialist();
            $updtSpec->readFromRow($_REQUEST);
            $updtSpec = SpecialistDAO::updateSpecialistPayment($specialist->getId_specialist(), $updtSpec->getPay_bankname(), $updtSpec->getPay_routing(), $updtSpec->getPay_account(), null);
            $pay_updated = ($updtSpec!=null);
            break;
        }
        
        case "nominate": {
            if(isset($_REQUEST["nominate_email"]) && ($_REQUEST["nominate_email"]!=null) && ($_REQUEST["nominate_email"]!="")) {
                MailUtils::sendNominateSpecialist($_REQUEST["nominate_email"]);
                $nominated = true;
            }
            
            break;
        }
        
        case "invite-parent": {
            if(isset($_REQUEST["invite_email"]) && ($_REQUEST["invite_email"]!=null) && ($_REQUEST["invite_email"]!="")) {
                $invitation = InvitationDAO::createInvitationForEmail($_REQUEST["invite_email"]);
                if($invitation!=null) {
                    MailUtils::sendPlaydateInvitationCode($_REQUEST["invite_email"], $specialist->getFullName(), $invitation->getCode());
                    $parentinvited = true;
                }
            }
            
            break;
        }
        
        case "addgroup": {
            if(isset($_REQUEST["name"]) && ($_REQUEST["name"]!=null) && ($_REQUEST["name"]!="")) {
                $newgroup = GroupDAO::getGroupByName($_REQUEST["name"]);
                
                if(($newgroup!=null) && ($newgroup->getId_group()>0)) {
                    if($newgroup->getPrivacity()==Group::$PRIVACITY_PRIVATE) {
                        //Si el grupo es privado, creamos solicitud
                        GroupDAO::createGroupSpecialistRequest($newgroup->getId_group(), $specialist->getId_specialist());
                    } else if($newgroup->getPrivacity()==Group::$PRIVACITY_PUBLIC) {
                        //Si el grupo es público, unimos al especialista directamente
                        GroupDAO::specialistJoinGroup($newgroup->getId_group(), $specialist->getId_specialist(), false);
                    }
                }
            }
            
            break;
        }
        
        case "updateprofile": {
            $editSpecialist = $specialist;
            $editSpecialist->readFromRow($_REQUEST);
             
            //si hay fichero, subimos
            if(count($_FILES)>0) {
                $currentDir = getcwd();
                $uploadDirectory = "/uploads/";
                
                $errors = []; // Store all foreseen and unforseen errors here
                
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
                
                $fileName = $_FILES['formfile']['name'];
                $fileSize = $_FILES['formfile']['size'];
                $fileTmpName  = $_FILES['formfile']['tmp_name'];
                $fileType = $_FILES['formfile']['type'];
                
                $tmpArr = explode('.',$fileName);
                $tmpExt = end($tmpArr);
                $fileExtension = strtolower($tmpExt);
                
                $uploadPath = $currentDir . $uploadDirectory . basename($fileName);
                
                $i=0;
                while (file_exists($uploadPath)) {
                    $i++;
                    $uploadPath = $currentDir . $uploadDirectory .$i."_".basename($fileName);
                }
                
                if (! in_array($fileExtension,$fileExtensions)) {
                    $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                }
                
                if ($fileSize > 6000000) {
                    $errors[] = "This file is more than 6MB. Sorry, it has to be less than or equal to 2MB";
                }
                
                if (empty($errors)) {
                    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                    
                    if ($didUpload) {
                        $editSpecialist->setPicture($uploadDirectory.basename($fileName)); //Actualizamos la ruta
                    } else {
                        echo "<!-- An error occurred somewhere. Try again or contact the admin -->";
                    }
                } else {
                    foreach ($errors as $error) {
                        //echo $error . "These are the errors" . "\n";
                    }
                }
            } //hay fichero
            
            //Insertamos las localizaciones
            NeighborhoodDAO::deleteAllSpecialistNeighborhood($editSpecialist->getId_specialist());
            if(!empty($_POST['id_neighborhood'])) {
                foreach($_POST['id_neighborhood'] as $auxlocation) {
                    NeighborhoodDAO::createSpecialistNeighborhood($editSpecialist->getId_specialist(), $auxlocation);
                }
            }
            
            SpecialistDAO::updateSpecialist($editSpecialist);
            
            if(isset($_POST['new-password']) && ($_POST['new-password']!=null) && ($_POST['new-password']!="")) {
                SpecialistDAO::changePassword($editSpecialist, $_POST['new-password']);
                $passwordchanged = true;
            }
            
            break;
        }
        
        case "creategroup": {
            $newGroup = new Group();
            $newGroup->readFromRow($_REQUEST);
            if(($newGroup->getName()!=null) && ($newGroup->getName()!="")) {
                $newGroup = GroupDAO::createGroup($newGroup);
                if($newGroup->getId_group()>0) {
                    GroupDAO::specialistJoinGroup($newGroup->getId_group(), $specialist->getId_specialist(), true); //El creador es administrador
                    header("Location: /groups-dashboard.php?id_group=".$newGroup->getId_group()."#profile");
                }
            }
            break;
        }
        
        case "invitationaccept": {
            $invitation = new GroupInvitation();
            $invitation->readFromRow($_REQUEST);
            if($invitation->getId_invitation()>0) {
                $invitation = GroupDAO::getGroupSpecialistInvitation($invitation->getId_invitation());
                
                if(($invitation!=null) && ($invitation->getId_invitation()>0) && ($invitation->getId_specialist()==$specialist->getId_specialist())) {
                    //Aceptamos invitacion, unimos a grupo
                    GroupDAO::acceptGroupSpecialistInvitation($invitation->getId_invitation());
                    GroupDAO::specialistJoinGroup($invitation->getId_group(), $specialist->getId_specialist(), false);
                    
                    header("Location: /groups-dashboard.php?id_group=".$invitation->getId_group());
                }
            }
            break;
        }
        case "invitationdecline": {
            $invitation = new GroupInvitation();
            $invitation->readFromRow($_REQUEST);
            if($invitation->getId_invitation()>0) {
                $invitation = GroupDAO::getGroupSpecialistInvitation($invitation->getId_invitation());
                
                if(($invitation!=null) && ($invitation->getId_invitation()>0) && ($invitation->getId_specialist()==$specialist->getId_specialist())) {
                    //Declinamos invitacion
                    GroupDAO::declineGroupSpecialistInvitation($invitation->getId_invitation());
                }
            }
            break;
        }
        
        case "createupdateselected": {
            $comment = new Comment();
            $comment->readFromRow($_REQUEST);
            $comment->setId_playdate($selectedPlaydate->getId_playdate());
            $comment->setId_specialist($specialist->getId_specialist());
            
            CommentDAO::createComment($comment);
            
            //Mail to all parents in this playdate
            
            //Enviamos notificacion
            $pdTime =  Utils::dateFormat($selectedPlaydate->getDate())." ".Utils::get12hourFormat($selectedPlaydate->getTime_init())." - ".Utils::get12hourFormat($selectedPlaydate->getTime_end());
            $leadName = "No Lead";
            $leadMail = null;
            if($selectedPlaydate->getId_specialist()!=null) {
                $pdSpecialist = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                if($pdSpecialist!=null) {
                    $leadName = $pdSpecialist->getFullName();
                    $leadMail = $pdSpecialist->getEmail();
                }
            }
            
            $allReservations = PlaydateDAO::getAllParentReservationByPlaydate($selectedPlaydate->getId_playdate());
            foreach($allReservations as $auxRes) {
                $parentName = "";
                $parentEmail = null;
                
                if($auxRes->getId_parent()!=null) {
                    $auxPar = ParentDAO::getParent($auxRes->getId_parent());
                    if($auxPar!=null) {
                        $parentName = $auxPar->getName();
                        $parentEmail = $auxPar->getEmail();
                    }
                }
                
                if($parentEmail!=null) {
                    MailUtils::sendPlaydateSpecialistUpdate($parentEmail, $parentName, $selectedPlaydate->getName(), $pdTime, $comment->getComment(), $leadName, "playdate.php?id_playdate=".$selectedPlaydate->getId_playdate());
                }
            }
            
            break;
        }
        
        case "createupdatepast": {
            $comment = new Comment();
            $comment->readFromRow($_REQUEST);
            $comment->setId_playdate($selectedPastPlaydate->getId_playdate());
            $comment->setId_specialist($specialist->getId_specialist());
            
            CommentDAO::createComment($comment);
            break;
        }
        
    }
    
}
?>
<!DOCTYPE html>
<html lang="en">

  <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Specialist Dashboard - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
    <?php if($selectedPastPlaydate!=null || $selectedPlaydate!=null) { ?>
        <style type="text/css">
            .my-dashboard .tab-content, .my-dashboard .nav-tabs{display: none;}
        </style>
    <?php } ?>
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page specialist-dashboard<?php echo($isConnected?" connected":"") ?>">

    <!-- DATOS INCOMPLETOS -->
    <div class="modal fade itinerary-modal" id="incomplete-data" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            
            <div class="row">
              <h2 class="itinerary-modal-heading noUppercase">Hey, PAL! Not so fast! Please complete all mandatory fields.</h2>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- DATOS INCOMPLETOS -->
    
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Specialist Dashboard</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="modal fade" id="paymentInfo" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="payment-information" method="post" action="/specialist-dashboard.php#playpacks" role="form" novalidate="true">
            	<input type="hidden" name="action" value="update_payment" />
                <input type="hidden" name="id_specialist" value="<?php echo $specialist->getId_specialist()?>" />
                <h2 class="form-last-playdate-heading">Payment Information</h2>
                <div class="controls">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="pay_bankname">Bank Name</label>
                                <input id="pay_bankname" type="text" name="pay_bankname" class="form-control" placeholder="Bank Name" required="required" data-error="Insert your bank name." value="<?php echo $specialist->getPay_bankname() ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="pay_routing">Routing Number</label>
                                <input id="pay_routing" type="text" name="pay_routing" class="form-control" placeholder="Routing Number" required="false" data-error="Insert your bank routing number."  value="<?php echo $specialist->getPay_routing() ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="pay_account">Account Number</label>
                                <input id="pay_account" type="text" name="pay_account" class="form-control" placeholder="Account Number" required="required" data-error="Insert your bank account number."  value="<?php echo $specialist->getPay_account() ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="submit" class="btn btn-send" value="Save Changes">
                        </div>
                    </div>                                                    
                      
                  </div>

              </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <?php 
        $ratingvalue = RatingDAO::getSpecialistValue($specialist->getId_specialist());
        $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialist->getId_specialist());
        $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialist->getId_specialist());
        ?>
        <div class="col-lg-3 sidebar  my-dashboard">
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $specialist->getPicture() ?>" alt="">
                </div>
                <div class="info">
                  <span class="title"><?php echo $specialist->getFullName() ?></span>
                  <span class="subtitle">
					<?php echo $specProfile ?>
				 </span>
                </div>
                <div class="puntuacion">
              <ul>
                <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
              	<li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
              </ul>
              <span class="num"><?php echo $ratingvalue ?>/5</span>
            </div>
                <div class="separator"></div>
                <div class="data certifications">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="info check"><span>background check</span></div>
                    </div>
                     <div class="col-lg-6">
                      <div class="info certificate"><span>approved by PAL</span></div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="info family"><span><?php echo $numFamilies ?> repeat families</span></div>
                    </div>
                     <div class="col-lg-6">
                      <div class="info playdate"><span><?php echo $numPlaydates ?><br/>playdates</span></div>
                    </div>
                  </div>
                </div>
              </div>


        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9 my-dashboard">
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-current-tab" data-toggle="tab" href="#nav-current" role="tab" aria-controls="nav-home" aria-selected="true">Current Playdates</a>
              <a class="nav-item nav-link" id="nav-request-tab" data-toggle="tab" href="#nav-request" role="tab" aria-controls="nav-request" aria-selected="false">Requests</a>
              <a class="nav-item nav-link" id="nav-past-tab" data-toggle="tab" href="#nav-past" role="tab" aria-controls="nav-past" aria-selected="false">Past</a>
              <a class="nav-item nav-link" id="nav-groups-tab" data-toggle="tab" href="#nav-groups" role="tab" aria-controls="nav-groups" aria-selected="false">Groups</a>
              <a class="nav-item nav-link" id="nav-specialists-tab" data-toggle="tab" href="#nav-specialists" role="tab" aria-controls="nav-specialists" aria-selected="false">Specialists</a>
              <a class="nav-item nav-link" id="nav-parents-tab" data-toggle="tab" href="#nav-parents" role="tab" aria-controls="nav-parents" aria-selected="false">Parents</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>              
              <a class="nav-item nav-link" id="nav-playpacks-tab" data-toggle="tab" href="#nav-playpacks" role="tab" aria-controls="nav-playpacks" aria-selected="false">Playdate History</a>
            </div>
          </nav>

          <div class="tab-content" id="nav-tabContent">

            <div class="tab-pane fade show active" id="nav-current" role="tabpanel" aria-labelledby="nav-current-tab">
              <div class="row row-tabs">
              
              	<?php 
              	$upcomingPlaydates = PlaydateDAO::getUpcomingPlaydatesListBySpecialist($specialist->getId_specialist(), 1, 8, null, null);
              	
              	$upPdSup = PlaydateDAO::getUpcomingPlaydatesListBySupporter($specialist->getId_specialist());
              	foreach ($upPdSup as $auxSup) {
              	    $upcomingPlaydates[] = $auxSup;
              	}
              	
              	foreach ($upcomingPlaydates as $auxPlaydate) {
              	    
              	    //TODO - De momento elegimos el primero para mostrar el detalle posterior
              	    $link = "/specialist-dashboard.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
              	    
              	    $interestName = "";
              	    $interestType = "";
              	    
              	    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
              	    $auxInterest = null;
              	    if(count($interests)>0) {
              	        $auxInterest = $interests[0];
              	        $interestName =$auxInterest->getName();
              	        
              	        switch ($auxInterest->getType()) {
              	            case Interest::$TYPE_TRAINING: {
              	                $interestType=" stem";
              	                break;
              	            }
              	            case Interest::$TYPE_ART: {
              	                $interestType=" creative";
              	                break;
              	            }
              	            case Interest::$TYPE_PLAY: {
              	                $interestType=" outdoor";
              	                break;
              	            }
              	            case Interest::$TYPE_CULTURAL: {
              	                $interestType=" attractions";
              	                break;
              	            }
              	        }
              	    }
              	    
              	    $openSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
              	    
              	    //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
              	    if(($auxPlaydate->getId_request()!=null) && is_numeric($auxPlaydate->getId_request())) {
              	        $minAgePd = 100;
              	        $maxAgePd = -1;
              	        $reservations = PlaydateDAO::getChildrenReservations($auxPlaydate->getId_playdate());
              	        foreach ($reservations as $auxReserv) {
              	            if($auxReserv->getAge()>$maxAgePd) {
              	                $maxAgePd = $auxReserv->getAge();
              	            }
              	            
              	            if($auxReserv->getAge()<$minAgePd) {
              	                $minAgePd = $auxReserv->getAge();
              	            }
              	        }
              	        
              	        
              	        if(($maxAgePd>=0) && ($minAgePd<100)) {
              	            $auxPlaydate->setAge_init($minAgePd);
              	            $auxPlaydate->setAge_end($maxAgePd);
              	        }
              	        
              	    }
              	    
              	?>
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?>">
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate()) ?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($auxPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="info">
                      <span class="title"><a href="<?php echo $link ?>"><?php echo ($auxPlaydate->getName()) ?></a></span>
                      <span class="subtitle"><?php echo($auxPlaydate->getLoc_state()) ?></span>
                    </div>
                     <div class="data">
                      <div  class="age">Ages: <span><?php echo $auxPlaydate->getAge_init() ?>-<?php echo $auxPlaydate->getAge_end() ?></span></div>
                      <div class="participants">Participants: <span><?php echo $auxPlaydate->getNum_children() ?> / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                      <?php if($specialist!=null) { ?>
                  		<div class="specialists">Specialist <span><?php echo $specialist->getFullName() ?></span></div>
                  	   <?php } ?>
                    </div>
                  </div>
                </div>
				        <?php } ?>
				        <?php 
                  if(($upcomingPlaydates==null) || (count($upcomingPlaydates)<=0)) {
                  ?>
                    <div class="col-lg-12">
                      <div class="no-results"><img src="img/cactus.png"/>
                        <p>You have no upcoming playdates reserved. </p>
                      </div>
                    </div>
                <?php   
                  }
                ?>
              </div>
             
               <?php if($selectedPlaydate!=null) {
              
                  $interestName = "";
                  $interestType = "";
                  
                  $interests = InterestDAO::getInterestsByPlaydate($selectedPlaydate->getId_playdate());
                  $auxInterest = null;
                  if(count($interests)>0) {
                      $auxInterest = $interests[0];
                      $interestName =$auxInterest->getName();
                      
                      switch ($auxInterest->getType()) {
                          case Interest::$TYPE_TRAINING: {
                              $interestType=" stem";
                              break;
                          }
                          case Interest::$TYPE_ART: {
                              $interestType=" creative";
                              break;
                          }
                          case Interest::$TYPE_PLAY: {
                              $interestType=" outdoor";
                              break;
                          }
                          case Interest::$TYPE_CULTURAL: {
                              $interestType=" attractions";
                              break;
                          }
                      }
                  }
                  
                  $openSpots = $selectedPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($selectedPlaydate->getId_playdate());
                  //$specialist = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                  
                  //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
                  if(($selectedPlaydate->getId_request()!=null) && is_numeric($selectedPlaydate->getId_request())) {
                      $minAgePd = 100;
                      $maxAgePd = -1;
                      $reservations = PlaydateDAO::getChildrenReservations($selectedPlaydate->getId_playdate());
                      foreach ($reservations as $auxReserv) {
                          if($auxReserv->getAge()>$maxAgePd) {
                              $maxAgePd = $auxReserv->getAge();
                          }
                          
                          if($auxReserv->getAge()<$minAgePd) {
                              $minAgePd = $auxReserv->getAge();
                          }
                      }
                      
                      
                      if(($maxAgePd>=0) && ($minAgePd<100)) {
                          $selectedPlaydate->setAge_init($minAgePd);
                          $selectedPlaydate->setAge_end($maxAgePd);
                      }
                      
                  }
                  
              ?>
              
              <div id="selected-playdate" class="row row-current-dashboard row-eq-height">
                <div class="col-lg-4 vcenter" >
                  <div class="box-playdates <?php echo $interestType ?> no-text">
                    <div class="img-container">
                      <a href="/playdate.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>"><img src="<?php echo $selectedPlaydate->getPicture() ?>" alt="" /></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                  <div class="info-current-dashboard">
                    <div class="info">
                      <span class="title"><a href="/playdate.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>"><?php echo $selectedPlaydate->getName() ?></a></span>
                      <span class="subtitle"><?php echo $selectedPlaydate->getLoc_city() ?></span>
                    </div>
                     <div class="data">
                      <p  class="place">
                        <span class="address"><?php echo $selectedPlaydate->getLoc_address() ?></span>
                        <span class="city"><?php echo $selectedPlaydate->getLoc_city() ?>, <?php echo $selectedPlaydate->getLoc_state() ?></span>
                      </p>
                    </div>
                    <div class="data">
                      <span class="date"><?php echo(Utils::dateFormat($selectedPlaydate->getDate())) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($selectedPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($selectedPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="data">
                      <div class="participants"><span><?php echo $selectedPlaydate->getNum_children() ?> / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                  <a href="javascript:;" class="bt-upload-update" id="updatebtn">Upload Update</a>
                  <form class="form-invite" action="/specialist-dashboard.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>" method="post">
                  <input type="hidden" name="action" value="invite-friends" />
                  <input type="hidden" name="id_playdate" value="<?php echo $selectedPlaydate->getId_playdate() ?>" />
                  <label for="inputEmail" class="sr-only">Username or Email</label>
                  <input type="text" id="inputEmail" class="form-control" placeholder="Username or Email" name="inviteFriendsPopup[]" required="required" >
                  <?php if($invited) { ?>
                  	<p style="color:#838383;width:100%;text-align:center">Invitation has been sent</p>
                  <?php } ?>
                  <button class="btn btn-md btn-primary btn-block" type="submit">Invite</button>
                </form>
                </div>
              </div>

              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Reserved By</p>
                  <div class="container-specialist-tags row">
                  	<?php $parents = ParentDAO::getParentsListByPlaydate($selectedPlaydate->getId_playdate(), 1, 10, null, null) ?>
                  	<?php 
                  	foreach ($parents as $auxParent) {
                  	     $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPlaydate->getId_playdate());
                  	     
                  	     if($reservation!=null) {
                      	     $childrenReservation = PlaydateDAO::getChildrenReservationsByReservation($reservation->getId_reservation());
                      	     $numSpots = 0;
                      	     foreach($childrenReservation as $auxChildren) {
                      	         $numSpots += $auxChildren->getNum_children();
                      	     }
                  	?>
                    <div class="specialist-tag">
                      <img src="<?php echo (($auxParent->getPicture()==null)?"/img/img-profile.jpg":$auxParent->getPicture()) ?>">
                      <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getName() ?><span><?php echo $numSpots ?> spot<?php if($numSpots!=1) {echo("s");}?></span></a>
                    </div>
                    <?php 
                  	     } //if reservation is not null (will be null if cancelled or not found)
                  	}
                    ?>
                  </div>
                </div>
              </div>
              <div class="row row-dashboard">
                   <div class="col-lg-8">
                  		<p class="titulo">Playdate Attendance</p>
                    </div>
                    <div class="col-lg-4">
                  		<p><a target="_blank" href="/attendance.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>" style="text-decoration:none;" class="bt-upload-update" id="download">Download Attendance</a></p>
                    </div>
              </div>
			 <div class="row row-dashboard">
                <div class="col-lg-12">
                  <div class="container-specialist-tags row">
                  
                  	<?php $parents = ParentDAO::getParentsListByPlaydate($selectedPlaydate->getId_playdate(), 1, 10, null, null) ?>
                  	<?php foreach ($parents as $auxParent) {
                  	     $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPlaydate->getId_playdate());
                  	     
                  	     if($reservation!=null) {
                      	     $childrenReservation = PlaydateDAO::getChildrenReservationsByReservation($reservation->getId_reservation());
                      	     $numSpots = 0;
                      	     foreach($childrenReservation as $auxChildren) {
                      	         $numSpots += $auxChildren->getNum_children();
                      	     }
                  	?>
                            <div class="specialist-tag col-lg-8">
                              <img src="<?php echo (($auxParent->getPicture()==null)?"/img/img-profile.jpg":$auxParent->getPicture()) ?>">
                              <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getFullName() ?><span>Email: <?php echo $auxParent->getEmail() ?> / Phone: <?php echo $auxParent->getPhone() ?> | <?php echo $auxParent->getCellphone() ?></span><span><?php echo $numSpots ?> spot<?php if($numSpots!=1) {echo("s");}?></span></a>
                              
                            </div>
                            <div class="col-lg-9">
                                <table id="playpack-credit" class="table table-striped" style="width:100%">
                                  <thead>
                                      <tr>
                                          <th>Child Name</th>
        								  <th>Num. of Kids</th>
        								  <th>Age</th>
        								  <th>Notes</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  <?php 
                                  foreach($childrenReservation as $auxChildren) {
                                      $childName = "Extra-children";
                                      $childNotes = "";
                                      if($auxChildren->getId_children()!=null) {
                                          $child = ChildrenDAO::getChildren($auxChildren->getId_children());
                                          $childName = $child->getName();
                                          $childNotes = $child->getNotes();
                                      }
                                  ?>
                                      <tr>
                                            <td><?php echo $childName ?></td>
            								<td><?php echo $auxChildren->getNum_children() ?></td>
            								<td><?php echo $auxChildren->getAge() ?></td>
            								<td><?php echo $childNotes ?></td>
                                      </tr>
                                  <?php } ?>
                                  </tbody>
                                </table>
                              </div>
                            
                              <!-- emergency contacts -->
                              <div class="col-lg-9">
                                <table id="playpack-credit" class="table table-striped" style="width:100%">
                                  <thead>
                                      <tr>
                                          <th>Emergency Contact</th>
        								  <th>Email</th>
        								  <th>Phone</th>
        								  <th>Relationship</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  <?php 
                                  $emergency = EmergencyDAO::getEmergencyListByParent($auxParent->getId_parent());
                                  foreach($emergency as $auxEmer) {
                                  ?>
                                      <tr>
                                            <td><?php echo $auxEmer->getName() ?></td>
            								<td><?php echo $auxEmer->getMail() ?></td>
            								<td><?php echo $auxEmer->getPhone() ?></td>
            								<td><?php echo $auxEmer->getRelation() ?></td>
                                      </tr>
                                  <?php } ?>
                                  </tbody>
                                </table>
                              </div>
                              <!-- /emergency contacts -->
                    <?php 
                  	         } //if reservation is not null (will be null if cancelled or not found)
                  	     } 
                  	     
                  	?>
                  </div>
                </div>
              </div>
              
              <?php 
              $supporters = SpecialistDAO::getSupportersByPlaydate($selectedPlaydate->getId_playdate()); 
              if(($supporters!=null) && (count($supporters)>0)) {
              ?>
              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Supporters</p>
                  <div class="container-specialist-tags row">
                  	
                    <div class="col-lg-9">
                        <table id="playpack-credit" class="table table-striped" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Name</th>
								  <th>Email</th>
								  <th>Phone</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php 
                          foreach($supporters as $auxSupport) {
                          ?>
                              <tr>
                                    <td><?php echo $auxSupport->getFullName() ?></td>
    								<td><?php echo $auxSupport->getEmail() ?></td>
    								<td><?php echo $auxSupport->getPhone() ?></td>
                              </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
              <?php 
              }
              ?>
				
			  <?php /* 
              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Invited Friends</p>
                  <div class="row">
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">jonh@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">tom@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">chris@gmail.com</a>
                    </div>
                    <div class="col-lg-4">
                      <a href="mailto:jonh@gmail.com">gabrielle@gmail.com</a>
                    </div>
                  </div>
                </div>
              </div>
              */ ?>

               <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Specialists</p>
                  <div class="container-specialist-tags row">
                  	  <?php 
                  	  $selectedPlaydateLead = SpecialistDAO::getSpecialist($selectedPlaydate->getId_specialist());
                  	  ?>
                  
                      <a href="/specialist-profile.php?id_specialist=<?php echo $selectedPlaydateLead->getId_specialist() ?>&slug=<?php echo $selectedPlaydateLead->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $selectedPlaydateLead->getPicture()?>">
                        <span><?php echo $selectedPlaydateLead->getFullName() ?> | Lead</span>
                      </div></a>
                      
                      <?php 
                      $supporters = SpecialistDAO::getSupportersByPlaydate($selectedPlaydate->getId_playdate());
                      foreach($supporters as $auxSupporter) {
                      ?>
                      <a href="/specialist-profile.php?id_specialist=<?php echo $auxSupporter->getId_specialist() ?>&slug=<?php echo $auxSupporter->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $auxSupporter->getPicture()?>">
                        <span><?php echo $auxSupporter->getFullName() ?> | Supporter</span>
                      </div></a>
                      <?php  
                      }
                      ?>
                      
                  </div>
                </div>
              </div>

              <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Comments</p>
                </div>
                <?php $updates = CommentDAO::getCommentListByPlaydate($selectedPlaydate->getId_playdate()); ?>
                <?php foreach($updates as $auxUpdate) {  
                    $username = "Unknown";
                    if(($auxUpdate->getId_parent()!=null) && is_numeric($auxUpdate->getId_parent())) {
                        $auxParent = ParentDAO::getParent($auxUpdate->getId_parent());
                        $username = $auxParent->getName();
                        $pic = $auxParent->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    } else if(($auxUpdate->getId_specialist()!=null) && is_numeric($auxUpdate->getId_specialist())) {
                    
                        $auxSpecialist = SpecialistDAO::getSpecialist($auxUpdate->getId_specialist());
                        $username = $auxSpecialist->getName();
                        $pic = $auxSpecialist->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    }
                        
                ?>
                <div class="row info-updates">
                    <div class="col-lg-2 date">
                      <img src="<?php echo $pic ?>">
                      <span><strong><?php echo $username ?></strong></span>
                    </div>
                    <div class="col-lg-10 texto">
                      <p class="day"><strong><?php echo Utils::dateFormat($auxUpdate->getIdate())?></strong></p>
                      <p style="color:#878787"><?php echo $auxUpdate->getComment() ?></p>
                    </div>
                </div>
                
                <?php  
                    }
                ?>
              </div>
              
              
              
              <div class="row row-tabs">
                <div class="col-lg-12">
                <form class="form-comment" id="form-comment" method="post" enctype="multipart/form-data" action="specialist-dashboard.php?id_playdate=<?php echo $selectedPlaydate->getId_playdate() ?>">
                    <input type="hidden" name="action" value="createupdateselected" />
                    <input type="hidden" name="id_playdate" value="<?php echo $selectedPlaydate->getId_playdate() ?>" />    
                    <p class="titulo uppercase">New comment</p>
                    <div class="row">
                      <div class="col-lg-12">                    
                         <label for="comment" class="sr-only">New comment</label>
                         <textarea class="form-control" placeholder="Comment here to send Parents updates on your playdate plans, reminders and other details. Your comment will be visible to every participant in this playdate." id="comment" rows="3" name="comment"></textarea>
                      </div>
                    </div>
                    <div class="row row-tabs bt-send">
                  		<div class="col-lg-12">
                     		<button class="btn btn-md btn-primary bt-save-profile" type="submit">Upload Comment</button>
                  		</div>
                	</div>
                </form>
                                  
                </div>
              </div>
              <?php } //fin if selected playdate ?>
              
              
            </div>

            <div class="tab-pane fade" id="nav-request" role="tabpanel" aria-labelledby="nav-request-tab">
              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo uppercase">Open</p>
                </div>
                
                <?php 
                $openRequests = PlaydateRequestDAO::getOpenPlaydateRequestsListBySpecialist($specialist->getId_specialist());
                foreach ($openRequests as $oreq) {
                    
                    $parent = ParentDAO::getParent($oreq->getId_parent());
                    
                    // $interestName = "";
                    // $interestType = "";
                    
                    // $interests = InterestDAO::getInterestsByRequest($oreq->getId_request());
                    // $auxInterest = null;
                    // if(count($interests)>0) {
                    //     $auxInterest = $interests[0];
                    //     $interestName =$auxInterest->getName();
                        
                    //     switch ($auxInterest->getType()) {
                    //         case Interest::$TYPE_TRAINING: {
                    //             $interestType=" stem";
                    //             break;
                    //         }
                    //         case Interest::$TYPE_ART: {
                    //             $interestType=" creative";
                    //             break;
                    //         }
                    //         case Interest::$TYPE_PLAY: {
                    //             $interestType=" outdoor";
                    //             break;
                    //         }
                    //         case Interest::$TYPE_CULTURAL: {
                    //             $interestType=" attractions";
                    //             break;
                    //         }
                    //     }
                    // }
                    
                ?>
                
                <div class="col-lg-4">
                  <div class="box-playdates <?php //echo $interestType ?>">
                    <div class="time">
                      <span class="date"><?php 
                      // if($oreq->getPlaydateDate()){
                      // $new_date_format = date('M d, Y', strtotime($oreq->getPlaydateDate()));
                      // echo $new_date_format; 
                      // }
                      echo Utils::dateFormat($oreq->getDate()) ?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($oreq->getTime_init()) ?> - <?php echo Utils::get12hourFormat($oreq->getTime_end()) ?></span>
                    </div>
                    <div class="info">
                      <span class="title">Request Pending</span>
                      <span class="subtitle"><?php //echo $oreq->getLocation() ?></span>
                    </div>
                    <div class="data">
                      <div class="specialists">Parent: <span><?php echo $parent->getName() ?></span></div>
                    </div>
                    <div class="reserve">
                      <a href="/specialist-submit-itinerary.php?id_request=<?php echo $oreq->getId_request() ?>" class="bt-reserve">Prepare Itinerary</a>
                    </div>
                  </div>
                </div>
				<?php } ?>
                
                <div class="col-lg-12">
                  <p class="titulo uppercase">Submitted</p>
                </div>
                
                <?php 
                $submittedPlaydates = PlaydateDAO::getSubmittedPlaydatesListBySpecialist($specialist->getId_specialist());
                foreach ($submittedPlaydates as $subPd) {
                    
                    $parent = ParentDAO::getParent($subPd->getId_parent());
                    
                    $interestName = "";
                    $interestType = "";
                    
                    $interests = InterestDAO::getInterestsByPlaydate($subPd->getId_playdate());
                    $auxInterest = null;
                    if(count($interests)>0) {
                        $auxInterest = $interests[0];
                        $interestName =$auxInterest->getName();
                        
                        switch ($auxInterest->getType()) {
                            case Interest::$TYPE_TRAINING: {
                                $interestType=" stem";
                                break;
                            }
                            case Interest::$TYPE_ART: {
                                $interestType=" creative";
                                break;
                            }
                            case Interest::$TYPE_PLAY: {
                                $interestType=" outdoor";
                                break;
                            }
                            case Interest::$TYPE_CULTURAL: {
                                $interestType=" attractions";
                                break;
                            }
                        }
                    }
                    
                    $auxReq = PlaydateRequestDAO::getPlaydateRequest($subPd->getId_request());
                ?>
                
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?>">
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($subPd->getDate()) ?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($subPd->getTime_init()) ?> - <?php echo Utils::get12hourFormat($subPd->getTime_end()) ?></span>
                    </div>
                    <div class="info">
                      <span class="title"><?php echo $subPd->getName() ?></span>
                      <span class="subtitle"><?php echo $subPd->getLoc_city() ?>, <?php echo $subPd->getLoc_state() ?></span>
                    </div>
                    <div class="data">
                      <div class="specialists">Parent: <span><?php echo $parent->getName() ?></span></div>
                    </div>
                    <?php if($subPd->getStatus()==Playdate::$STATUS_REQUEST) { ?>
                    <div class="reserve">
                      <a href="/playdate.php?id_playdate=<?php echo $subPd->getId_playdate() ?>" class="bt-reserve">View Playdate</a>
                    </div>
                    <?php } else if($subPd->getStatus()==Playdate::$STATUS_PUBLISHED) { ?>
                    <div class="reserve">
                      <a href="/playdate.php?id_playdate=<?php echo $subPd->getId_playdate() ?>" class="bt-reserve">Accepted</a>
                    </div>
                    <?php } else if($subPd->getStatus()==Playdate::$STATUS_DECLINED) { ?>
					<div class="reserve">
                      <a href="javascript:;" class="bt-reserve declined">Declined</a>
                    </div>                    
                    <?php } ?>
                  </div>
                </div>
				<?php } ?>
				
				<div class="col-lg-12">
                  <p class="titulo uppercase">Support Applications  <a style="display:inline-block;" href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Use our Suggested Adult to Child Ratio below to hire the appropriate number of Support Specialists. Suggested Age Group Ratios are: Under 3: 2 children to 1 adult. 3-4 years: 4 children to 1 adult. 5-7 years: 5 children to 1 adult. 8+ years: 6 children to 1 adult. If you go above this ratio, you will need to get approval from parents to pay an addition $23/hour that can be split among all Parents and added into the 'Add-Ons' section at a price per child. If you go below this ratio there is no additional compensation for you or your team. We strongly suggest keeping this as your minimum team size."><i class="fa fa-question-circle"></i></a></p>
                </div>
                
                <?php 
                $supportPlaydates = PlaydateDAO::getUpcomingPlaydatesListBySpecialistWithSupportRequests($specialist->getId_specialist());
                foreach ($supportPlaydates as $subPd) {
                    
                    $lead = SpecialistDAO::getSpecialist($subPd->getId_specialist());
                    
                    $interestName = "";
                    $interestType = "";
                    
                    $interests = InterestDAO::getInterestsByPlaydate($subPd->getId_playdate());
                    $auxInterest = null;
                    if(count($interests)>0) {
                        $auxInterest = $interests[0];
                        $interestName =$auxInterest->getName();
                        
                        switch ($auxInterest->getType()) {
                            case Interest::$TYPE_TRAINING: {
                                $interestType=" stem";
                                break;
                            }
                            case Interest::$TYPE_ART: {
                                $interestType=" creative";
                                break;
                            }
                            case Interest::$TYPE_PLAY: {
                                $interestType=" outdoor";
                                break;
                            }
                            case Interest::$TYPE_CULTURAL: {
                                $interestType=" attractions";
                                break;
                            }
                        }
                        
                        $supportCandidates = SpecialistDAO::getSupporterRequestsByPlaydate($subPd->getId_playdate());
                        $numCandidates = count($supportCandidates);
                        $candidates = "";
                        $i=0;
                        foreach($supportCandidates as $auxCand) {
                            if($i>0) {
                                $candidates .= ", ";
                            }
                            $candidates .= "<a style=\"display:inline\" href=\"/specialist-profile.php?id_specialist=".$auxCand->getId_specialist()."\">".$auxCand->getName()."</a>";
                            $i++;
                            
                        }
                        
                        
                    }
                    
                ?>
                
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?>">
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($subPd->getDate()) ?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($subPd->getTime_init()) ?> - <?php echo Utils::get12hourFormat($subPd->getTime_end()) ?></span>
                    </div>
                    <div class="info">
                      <span class="title"><?php echo $subPd->getName() ?></span>
                      <span class="subtitle"><?php echo $subPd->getLoc_city() ?>, <?php echo $subPd->getLoc_state() ?></span>
                    </div>
                    <div class="data">
                      <div class="specialists">Application<?php if($numCandidates>1) {echo("s");}?>: <span><?php echo $candidates ?></span></div>
                    </div>
                    <div class="reserve">
                      <a href="/playdate.php?id_playdate=<?php echo $subPd->getId_playdate() ?>" class="bt-reserve">View Playdate</a>
                    </div>
                  </div>
                </div>
				<?php } ?>
                
                
              </div>
              
            </div>

            <div class="tab-pane fade rate-playdate" id="nav-past" role="tabpanel" aria-labelledby="nav-past-tab">
              <div class="row row-tabs">

              <?php 
              	$pastPlaydates = PlaydateDAO::getPastPlaydatesListBySpecialist($specialist->getId_specialist(), 1, 1000, null, null);
              	
              	foreach ($pastPlaydates as $auxPlaydate) {
              	    
              	    //TODO - De momento elegimos el primero para mostrar el detalle posterior
              	    //$link = "/specialist-dashboard.php?id_playdate=".$auxPlaydate->getId_playdate()."&slug=".$auxPlaydate->getSlug();
              	    $link = "/specialist-dashboard.php?id_past_playdate=".$auxPlaydate->getId_playdate()."#past";
              	    
              	    $interestName = "";
              	    $interestType = "";
              	    
              	    $interests = InterestDAO::getInterestsByPlaydate($auxPlaydate->getId_playdate());
              	    $auxInterest = null;
              	    if(count($interests)>0) {
              	        $auxInterest = $interests[0];
              	        $interestName =$auxInterest->getName();
              	        
              	        switch ($auxInterest->getType()) {
              	            case Interest::$TYPE_TRAINING: {
              	                $interestType=" stem";
              	                break;
              	            }
              	            case Interest::$TYPE_ART: {
              	                $interestType=" creative";
              	                break;
              	            }
              	            case Interest::$TYPE_PLAY: {
              	                $interestType=" outdoor";
              	                break;
              	            }
              	            case Interest::$TYPE_CULTURAL: {
              	                $interestType=" attractions";
              	                break;
              	            }
              	        }
              	    }
              	    
              	    $openSpots = $auxPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($auxPlaydate->getId_playdate());
              	    //$specialist = SpecialistDAO::getSpecialist($auxPlaydate->getId_specialist());
              	    
              	    $selectedBox = (($selectedPastPlaydate!=null) && ($selectedPastPlaydate->getId_playdate()==$auxPlaydate->getId_playdate()));
              	    
              ?>
              
                <div class="col-lg-4">
                  <div class="box-playdates <?php echo $interestType ?><?php echo($selectedBox?" selected-box":"")?>">
                    <?php /*?>
                    <div class="img-container">
                      <img src="<?php echo $auxPlaydate->getPicture() ?>" alt="">
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                    */?>
                    <div class="time">
                      <span class="date"><?php echo Utils::dateFormat($auxPlaydate->getDate())?></span>
                      <span class="hour"><?php echo Utils::get12hourFormat($auxPlaydate->getTime_init())?>-<?php echo Utils::get12hourFormat($auxPlaydate->getTime_end())?></span>
                    </div>
                    <div class="info">
                      <span class="title"><a href="<?php echo $link ?>"><?php echo $auxPlaydate->getName()?></a></span>
                      <span class="subtitle"><?php echo $auxPlaydate->getLoc_city()?>, <?php echo $auxPlaydate->getLoc_state()?></span>
                    </div>
                    <div class="data">
                      <div class="specialists">Specialist: <span><?php echo $specialist->getFullName() ?></span></div>
                    </div>
                   
                    <div class="reserve">
                      <a href="<?php echo $link ?>" class="bt-reserve">View Playdate</a>
                    </div>
                  </div>
                </div>
				<?php } ?>
            <?php 
                if(($pastPlaydates==null) || (count($pastPlaydates)<=0)) {
                ?>
                  <div class="col-lg-12">
                    <div class="no-results"><img src="img/cactus.png"/>
                      <p>You have no past playdates reserved. </p>
                    </div>
                  </div>
                <?php   
                }
                ?>
              </div>
              
<?php if($selectedPastPlaydate!=null) {
              
                  $interestName = "";
                  $interestType = "";
                  
                  $interests = InterestDAO::getInterestsByPlaydate($selectedPastPlaydate->getId_playdate());
                  $auxInterest = null;
                  if(count($interests)>0) {
                      $auxInterest = $interests[0];
                      $interestName =$auxInterest->getName();
                      
                      switch ($auxInterest->getType()) {
                          case Interest::$TYPE_TRAINING: {
                              $interestType=" stem";
                              break;
                          }
                          case Interest::$TYPE_ART: {
                              $interestType=" creative";
                              break;
                          }
                          case Interest::$TYPE_PLAY: {
                              $interestType=" outdoor";
                              break;
                          }
                          case Interest::$TYPE_CULTURAL: {
                              $interestType=" attractions";
                              break;
                          }
                      }
                  }
                  
                  $openSpots = $selectedPastPlaydate->getNum_children() - PlaydateDAO::countPlaydateReservations($selectedPastPlaydate->getId_playdate());
                  //$specialist = SpecialistDAO::getSpecialist($selectedPastPlaydate->getId_specialist());
                  
                  //BUGFIX - If playdate comes from request, it has not ages. We have to calculate with reservations
                  if(($selectedPastPlaydate->getId_request()!=null) && is_numeric($selectedPastPlaydate->getId_request())) {
                      $minAgePd = 100;
                      $maxAgePd = -1;
                      $reservations = PlaydateDAO::getChildrenReservations($selectedPastPlaydate->getId_playdate());
                      foreach ($reservations as $auxReserv) {
                          if($auxReserv->getAge()>$maxAgePd) {
                              $maxAgePd = $auxReserv->getAge();
                          }
                          
                          if($auxReserv->getAge()<$minAgePd) {
                              $minAgePd = $auxReserv->getAge();
                          }
                      }
                      
                      
                      if(($maxAgePd>=0) && ($minAgePd<100)) {
                          $selectedPastPlaydate->setAge_init($minAgePd);
                          $selectedPastPlaydate->setAge_end($maxAgePd);
                      }
                      
                  }
                  
              ?>
              
              <div id="selected-past-playdate" class="row row-current-dashboard row-eq-height">
                <div class="col-lg-4 vcenter" >
                  <div class="box-playdates <?php echo $interestType ?> no-text">
                    <div class="img-container">
                      <a href="/playdate.php?id_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>"><img src="<?php echo $selectedPastPlaydate->getPicture() ?>" alt="" /></a>
                      <span class="cat-box"><?php echo $interestName ?></span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 vcenter">
                  <div class="info-current-dashboard">
                    <div class="info">
                      <span class="title"><a href="/playdate.php?id_playdate=<?php echo $selectedPastPlaydate->getId_playdate() ?>"><?php echo $selectedPastPlaydate->getName() ?></a></span>
                      <span class="subtitle"><?php echo $selectedPastPlaydate->getLoc_city() ?></span>
                    </div>
                     <div class="data">
                      <p  class="place">
                        <span class="address"><?php echo $selectedPastPlaydate->getLoc_address() ?></span>
                        <span class="city"><?php echo $selectedPastPlaydate->getLoc_city() ?>, <?php echo $selectedPastPlaydate->getLoc_state() ?></span>
                      </p>
                    </div>
                    <div class="data">
                      <span class="date"><?php echo(Utils::dateFormat($selectedPastPlaydate->getDate())) ?></span> | <span class="hour"><?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_init()) ?> - <?php echo Utils::get12hourFormat($selectedPastPlaydate->getTime_end()) ?></span>
                    </div>
                    <div class="data">
                      <div class="participants"><span><?php echo $selectedPastPlaydate->getNum_children() ?> / <?php echo $openSpots ?> open spot<?php if($openSpots!=1) { echo ("s");} ?></span></div>
                    </div>
                    <?php /*?>
                    <div class="reserve">
                      <a href="#"  data-toggle="modal" data-target="#last-playdate" class="feedback bt-reserve">Leave feedback</a>
                    </div>
                    */?>
                  </div>
                </div>
              </div>

              <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Reserved By</p>
                  <div class="container-specialist-tags row">
                  	<?php $parents = ParentDAO::getParentsListByPlaydate($selectedPastPlaydate->getId_playdate(), 1, 10, null, null) ?>
                  	<?php foreach ($parents as $auxParent) {
                  	    $reservation = PlaydateDAO::getParentReservationByPlaydate($auxParent->getId_parent(), $selectedPastPlaydate->getId_playdate());
                  	    if($reservation!=null) {
                  	?>
                    <div class="specialist-tag">
                      <img src="<?php echo (($auxParent->getPicture()==null)?"/img/img-profile.jpg":$auxParent->getPicture()) ?>">
                      <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title=""><?php echo $auxParent->getName() ?><span><?php echo $reservation->getNum_spots()?> spot<?php if($reservation->getNum_spots()!=1) {echo("s");}?></span></a>
                    </div>
                    <?php   
                  	    } //if reservation is not null (will be null if cancelled or not found)
                  	    }
                    ?>
                  </div>
                </div>
              </div>
				
			 <div class="row row-dashboard">
                <div class="col-lg-12">
                  <p class="titulo">Specialists</p>
                  <div class="container-specialist-tags row">
                      <a href="/specialist-profile.php?id_specialist=<?php echo $specialist->getId_specialist() ?>&slug=<?php echo $specialist->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $specialist->getPicture()?>">
                        <span><?php echo $specialist->getFullName() ?> | <?php echo $specProfile ?></span>
                      </div></a>
                      
                      <?php 
                      $supportersPast = SpecialistDAO::getSupportersByPlaydate($selectedPastPlaydate->getId_playdate());
                      foreach($supportersPast as $auxSupporterPast) {
                      ?>
                      <a href="/specialist-profile.php?id_specialist=<?php echo $auxSupporterPast->getId_specialist() ?>&slug=<?php echo $auxSupporterPast->getSlug() ?>"><div class="specialist-tag">
                        <img src="<?php echo $auxSupporterPast->getPicture()?>">
                        <span><?php echo $auxSupporterPast->getFullName() ?> | Supporter</span>
                      </div></a>
                      <?php  
                      }
                      ?>
                      
                  </div>
                </div>
              </div>
              
              <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Comment(s)</p>
                </div>
                <?php $updates = CommentDAO::getCommentListByPlaydate($selectedPastPlaydate->getId_playdate()); ?>
                <?php foreach($updates as $auxUpdate) {  
                    $username = "Unknown";
                    if(($auxUpdate->getId_parent()!=null) && is_numeric($auxUpdate->getId_parent())) {
                        $auxParent = ParentDAO::getParent($auxUpdate->getId_parent());
                        $username = $auxParent->getName();
                        $pic = $auxParent->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    } else if(($auxUpdate->getId_specialist()!=null) && is_numeric($auxUpdate->getId_specialist())) {
                    
                        $auxSpecialist = SpecialistDAO::getSpecialist($auxUpdate->getId_specialist());
                        $username = $auxSpecialist->getUsername();
                        $pic = $auxSpecialist->getName();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                    }
                        
                ?>
                <div class="row info-updates">
                    <div class="col-lg-2 date">
                      <img src="<?php echo $pic ?>">
                      <span><strong><?php echo $username ?></strong></span>
                    </div>
                    <div class="col-lg-10 texto">
                      <p class="day"><strong><?php echo Utils::dateFormat($auxUpdate->getIdate())?></strong></p>
                      <p style="color:#878787"><?php echo $auxUpdate->getComment() ?></p>
                    </div>
                </div>
                
                <?php  
                    }
                ?>
              </div>

			  <div class="row row-dashboard updates parents">
                <div class="col-lg-12">
                  <p class="titulo">Feedback</p>
                </div>
                <?php $ratings = RatingDAO::getRatingListByPlaydate($selectedPastPlaydate->getId_playdate()); ?>
                <?php foreach($ratings as $auxRating) { 
                    if(($auxRating->getComment_playdate()!=null) && ($auxRating->getComment_playdate()!="")) {
                        
                        $auxParent = ParentDAO::getParent($auxRating->getId_parent());
                        $pic = $auxParent->getPicture();
                        if(($pic==null) || ($pic=="")) {
                            $pic = "img/img-profile.jpg";
                        }
                        
                ?>
                <div class="row info-updates">
                    <div class="col-lg-2 date">
                      <img src="<?php echo $pic ?>">
                      <span><strong><?php echo $auxParent->getUsername() ?></strong></span>
                    </div>
                    <div class="col-lg-10 texto">
                      <p class="day"><strong><?php echo Utils::dateFormat($auxRating->getIdate())?></strong></p>
                      <p><?php echo $auxRating->getComment_playdate() ?></p>
                    </div>
                </div>
                
                <?php } 
                    }
                ?>
              </div>
              
              <?php } ?>              
              
            </div>

            <div class="tab-pane fade" id="nav-groups" role="tabpanel" aria-labelledby="nav-groups-tab">
              <div class="row row-tabs">

              <!-- grp invitations -->
              <?php 
              $grpInvitations = GroupDAO::getWaitingGroupInvitationsBySpecialist($specialist->getId_specialist());
              foreach($grpInvitations as $auxInvitation) {
                  $numMembers = GroupDAO::countGroupParents($auxInvitation->getId_group());
                  $numMembers += GroupDAO::countGroupSpecialists($auxInvitation->getId_group());
              
              ?>
              <div class="col-lg-4">
                <div class="box-playdates invitation">
                  <div class="invited-title">
                    <p>Your are invited!</p>
                  </div>
                  <div class="info">
                    <span class="title"><?php echo $auxInvitation->getName() ?></span>
                    <span class="subtitle"><?php echo $auxInvitation->getZipcode() ?></span>
                    <span class="subtitle members"><?php echo $numMembers ?> members</span>
                  </div>
                   <div class="data">
                    <p><?php echo $auxInvitation->getDescription(120); ?></p>
                    <a href="/group-public-profile.php?id_group=<?php echo $auxInvitation->getId_group() ?>" class="bt-learn-more">Learn more</a>
                  </div>
                  <div class="reserve">
                    <a href="/specialist-dashboard.php?action=invitationaccept&id_invitation=<?php echo $auxInvitation->getId_invitation()?>#groups" class="bt-reserve bt-join">Join Group</a>
                    <a href="/specialist-dashboard.php?action=invitationdecline&id_invitation=<?php echo $auxInvitation->getId_invitation()?>#groups" class="bt-reserve">Decline</a>
                  </div>
                </div>
              </div>
              <?php } ?>
              <!-- /grp invitations -->
              <!-- parent groups -->
			  <?php 
			  $groups = GroupDAO::getGroupsBySpecialist($specialist->getId_specialist());
			  
			  foreach($groups as $auxGroup) {
			      
			      $numMembers = GroupDAO::countGroupParents($auxGroup->getId_group());
			      $numMembers += GroupDAO::countGroupSpecialists($auxGroup->getId_group());
			      
			      $interestName = "";
			      $interestType = "";
			      
			      $interests = InterestDAO::getInterestsByGroup($auxGroup->getId_group());
			      $auxInterest = null;
			      if(count($interests)>0) {
			          $auxInterest = $interests[0];
			          $interestName =$auxInterest->getName();
			          
			          switch ($auxInterest->getType()) {
			              case Interest::$TYPE_TRAINING: {
			                  $interestType=" stem";
			                  break;
			              }
			              case Interest::$TYPE_ART: {
			                  $interestType=" creative";
			                  break;
			              }
			              case Interest::$TYPE_PLAY: {
			                  $interestType=" outdoor";
			                  break;
			              }
			              case Interest::$TYPE_CULTURAL: {
			                  $interestType=" attractions";
			                  break;
			              }
			          }
			      }
			      
			      $grpPicture = $auxGroup->getPicture();
			      if(($grpPicture==null) || ($grpPicture=="")) {
			          $grpPicture = "/img/default-img-group-profile.jpg";
			      }
			  ?>
              <div class="col-lg-4">
                <div class="box-playdates <?php echo $interestType ?>">
                  <div class="img-container">
                    <img src="<?php echo $grpPicture ?>"/ alt="">
                    <span class="cat-box"><?php echo $interestName ?></span>
                  </div>
                  <div class="info">
                    <span class="title"><?php echo $auxGroup->getName() ?></span>
                    <span class="subtitle"><?php echo $auxGroup->getZipcode() ?></span>
                    <span class="subtitle members"><?php echo $numMembers ?> members</span>
                  </div>
                   <div class="data">
                    <p><?php echo $auxGroup->getDescription(120); ?></p>
                  </div>
                  <div class="reserve">
                    <a href="/groups-dashboard.php?id_group=<?php echo $auxGroup->getId_group() ?>" class="bt-reserve">Dashboard</a>
                  </div>
                </div>
              </div>
              <?php } ?>
			  <!-- /parent groups -->
              </div> 

              <div class="row info-box groups">
                <div class="col-lg-6">                        
                  <p>Add a New Group</p>
                  <form class="form-add-group" method="post" action="/specialist-dashboard.php#groups">
                  	<input type="hidden" name="action" value="addgroup" />
                    <label for="inputSelectGroup" class="sr-only">Select group</label>
                    <input type="text" id="inputSelectGroup" class="form-control" placeholder="Select group" required="" name="name">
                    <button class="btn btn-md btn-primary bt-titulo" type="submit">Request Invite</button>  
                  </form>
                </div>
                <div class="col-lg-6">
                  <p>Create a New Group</p>
                  <form class="form-create-group" action="/specialist-dashboard.php#groups" method="post">
                    <input type="hidden" name="action" value="creategroup" />
                    <label for="inputGroupName" class="sr-only">Group Name</label>
                    <input type="text" id="inputGroupName" class="form-control" placeholder="Group Name" required="required" name="name" />
                    <?php /*
                    <p class="text-invite">Invite friends</p>
                    <div class="mail-container">
                      <label for="inputInviteUser" class="sr-only">Username or Email</label>
                      <div class="cont-invite">
                        <input type="text" id="inputInviteUser" class="form-control" placeholder="Username or Email" required="" ><a href="javascript:;" class="bt-invite" title=""><img src="img/bt-add.png" alt=""/></a>
                      </div>
                    </div>
                    */?>
                    <button class="btn btn-md btn-primary bt-titulo" type="submit">Create Group</button>  
                  </form>
                </div>         
              </div>
              

            </div>


            <div class="tab-pane fade" id="nav-specialists" role="tabpanel" aria-labelledby="nav-specialists-tab">
              <div class="row featured-specialists">
                  
<?php 
		$specialists = SpecialistDAO::getSpecialistsList(1, 4, null, null);
		
		foreach ($specialists as $specialistaux) {
		    $ratingvalue = RatingDAO::getSpecialistValue($specialistaux->getId_specialist());
		    $numPlaydates = PlaydateDAO::countPlaydatesBySpecialist($specialistaux->getId_specialist());
		    
		    $numFamilies = PlaydateDAO::countFamiliesRepeatBySpecialist($specialistaux->getId_specialist());
		?>
    
	    <div class="col-lg-4 col-md-6 col-sm-6 box-specialist">
           <div class="flip">
                <div class="card">
                  <div class="specialist face front">
                    <div class="info">
                       <p><?php echo $specialistaux->getFullName() ?></p>
                       <div class="puntuacion">
                          <ul>
                            <li class="<?php echo(($ratingvalue>0)?"active":"")?>"></li>
                            <li class="<?php echo(($ratingvalue>1)?"active":"")?>"></li>
                            <li class="<?php echo(($ratingvalue>2)?"active":"")?>"></li>
                            <li class="<?php echo(($ratingvalue>3)?"active":"")?>"></li>
                          	<li class="<?php echo(($ratingvalue>4)?"active":"")?>"></li>
                          </ul>
                          <span class="num"><?php echo $ratingvalue ?>/5</span>
                        </div>                
                      
                    </div>             
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                  <div class="face back">
                    <div class="info-back">
                      <div class="ico-specialist family"><span><?php echo $numFamilies ?><br>repeat families</span></div>
                      <div class="ico-specialist pd"><span><?php echo $numPlaydates ?><br>playdates</span></div>
                      <p><?php echo $specialistaux->getAbout_me() ?></p>
                      <a class="bt-profile" title="" href="/specialist-profile.php?id_specialist=<?php echo $specialistaux->getId_specialist()?>&slug=<?php echo $specialistaux->getSlug() ?>">View profile</a>
                    </div>            
                    <img src="<?php echo $specialistaux->getPicture() ?>"/>
                  </div>
                </div>
              </div>
        </div>
		<?php } ?>                  
                  
              </div>

              <div class="row info-box">

                  <div class="col-lg-12">
                    <div class="titulo noImages">
                      <h2>Nominate a specialist</h2>
                      <?php if($nominated) { ?>
                      <p>Your nomination has been sent!</p>
                      <?php } ?>
                      <p>Do you know a child care professional that should join playdate? </p>
                      <form class="form-nominate" action="/specialist-dashboard.php#specialists" method="post">
                        <input type="hidden" name="action" value="nominate" />
                        <label for="inputEmail" class="sr-only">Email</label>
                        <input type="text" id="inputEmail" class="form-control" placeholder="Email" required="" name="nominate_email">
                        <button class="btn btn-md btn-primary bt-titulo" type="submit">Nominate</button>
                      </form>
                    </div>

                  </div>

              </div>

            </div>


            <div class="tab-pane fade" id="nav-parents" role="tabpanel" aria-labelledby="nav-parents-tab">
              <div class="row row-tabs">
              
              	<?php $parents = ParentDAO::getParentsListBySpecialist($specialist->getId_specialist());
              	
              	foreach ($parents as $auxParent) {
              	    $parentPic = $auxParent->getPicture();
              	    if(($parentPic==null)||($parentPic=="")) {
              	        $parentPic = "/img/img-profile.jpg";
              	    }
              	?>
                <div class="col-lg-2">
                  <a href="/parent-public-profile.php?id_parent=<?php echo $auxParent->getId_parent() ?>" title="<?php echo $auxParent->getFullName() ?>" class="box-member">
                    <img src="<?php echo $parentPic ?>"/>
                    <p><?php echo $auxParent->getName() ?></p>
                  </a>
                </div>
                
                <?php } ?>
              </div>
              
              
              <div class="row info-box">

                  <div class="col-lg-12">
                    <div class="titulo noImages">
                      <h2>Invite a Parent</h2>
                      <?php if($parentinvited) { ?>
                      <p>Your invitation has been sent!</p>
                      <?php } ?>
                      <p>Build your business and invite Parents that already know and love you!</p>
                      <form class="form-nominate" action="/specialist-dashboard.php#parents" method="post">
                        <input type="hidden" name="action" value="invite-parent" />
                        <label for="inputEmail" class="sr-only">Email</label>
                        <input type="text" id="inputEmail" class="form-control" placeholder="Email" required="" name="invite_email">
                        <button class="btn btn-md btn-primary bt-titulo" type="submit">Invite</button>
                      </form>
                    </div>

                  </div>

              </div>
              
            </div>


            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

              <form class="form-profile" id="form-profile" method="post" enctype="multipart/form-data" action="specialist-dashboard.php#profile">
              	<input type="hidden" name="action" value="updateprofile" />
              	<input type="hidden" name="profile" value="<?php echo $specialist->getProfile() ?>" />
                <div class="row row-tabs">
                  <div class="col-lg-12">
                     <button class="btn btn-md btn-primary bt-save-profile" type="submit">Save changes</button>
                  </div>
                </div>
                <div class="row row-tabs">
                  <div class="col-lg-4 cont-img-profile">
                    <?php if(($specialist->getPicture()==null) || ($specialist->getPicture() == "")) { ?>
                    <img src="img/img-profile.jpg" alt="" class="img-profile">
                    <?php } else { ?>
                    <img src="<?php echo $specialist->getPicture() ?>" alt="" class="img-profile">
                    <?php } ?>
                    <label class="custom-file">
                      <input type="hidden" name="picture" value="<?php echo $specialist->getPicture() ?>" />
                      <input type="file" id="file" class="custom-file-input" name="formfile" onchange="previewFile()" />
                      <span class="custom-file-control"></span>
                    </label>
                  </div>
                  <div class="col-lg-8">
                    <p class="titulo">Your Personal Info</p>
                    <p class="profile-name info">Name: <span><?php echo $specialist->getFullName() ?></span></p>
                    <p class="profile-username info">Username: <span><?php echo $specialist->getUsername() ?></span></p>
                    <p class="profile-email info">Email: <span><?php echo $specialist->getEmail() ?></span></p>
                    <?php /*
                    <p class="profile-password info"><a href="#" title="">Reset password</a></p>
                    */?>
                    <div class="row">
                      <div class="col-lg-6">
                        <label for="inputNewPassword" class="small">New Password</label>
                        <input type="password" id="inputNewPassword" class="form-control noMarginTop" placeholder="" name="new-password" value="">
                      </div>
                      <div class="col-lg-6">
                      	<?php if($passwordchanged) { ?>
                      	<label for="inputNewPassword" class="small"></label>
                      	<p style="color:#5b5b5b">Password changed</p>
                      	<?php } ?>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-lg-6">
                        <label for="inputCellPhone" class="small">Cell Phone</label>
                        <input name="cellphone" type="text" id="inputCellPhone" class="form-control noMarginTop" placeholder="XXX XXX XXX" value="<?php echo $specialist->getCellphone() ?>">
                      </div>
                      <div class="col-lg-6">
                        <label for="inputPhoneWork" class="small">Work Phone</label>
                        <input type="text" name="phone" id="inputPhoneWork" class="form-control noMarginTop" placeholder="XXX XXX XXX" value="<?php echo $specialist->getPhone() ?>">
                      </div>
                    </div>

                    
                  </div>
                </div>
                <div class="row row-tabs">
                  <div class="col-lg-12">                    
                    <p class="titulo uppercase">Your Neighborhoods</p>
                    <div class="row row-zip-code">
                      <div class="col-lg-12 ">
                        <label for="inputNeighborhoods" class="sr-only">Your Neighborhoods</label>
                        <input type="text" id="location" name="location" class="form-control noMarginTop inputLocation" placeholder="neighborhood">
		                <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                      </div>
                      <div class="container-tags">
                        <?php
                        $neighborhoods = NeighborhoodDAO::getNeighborhoodsListBySpecialist($specialist->getId_specialist());
                        foreach ($neighborhoods as $auxItem) {
                        ?>
                          <div class="tag">
                            <span><?php echo $auxItem->getName()?></span>
                            <a class="bt-delete-tag" href="javascript:;" title="Eliminar tag">X</a>
                            <input type="hidden" value="<?php echo $auxItem->getId_neighborhood() ?>" name="id_neighborhood[]">
                          </div>
                        
                        <?php 
                        }
                        ?>
                      </div>
                    </div>                    
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-5">                    
                    <p class="titulo uppercase">Experience in years</p>
                    <div class="row row-zip-code">
                      <div class="col-lg-12 ">
                        <label for="inputExperience0" class="small toRight">Under 1 Year Old</label>
                        <input type="text" name="exp_range1" id="inputExperience0" class="form-control noMarginTop input-experience" placeholder="2 years" value="<?php echo $specialist->getExp_range1() ?>">
                      </div>
                      <div class="col-lg-12 ">
                        <label for="inputExperience2" class="small toRight">2-3 Years Old</label>
                        <input type="text" name="exp_range2" id="inputExperience2" class="form-control noMarginTop input-experience" placeholder="5 years" value="<?php echo $specialist->getExp_range2() ?>" >
                      </div>
                      <div class="col-lg-12 ">
                        <label for="inputExperience4" class="small toRight">4+ Years Old</label>
                        <input type="text" name="exp_range3" id="inputExperience4" class="form-control noMarginTop input-experience" placeholder="3 years" value="<?php echo $specialist->getExp_range3() ?>">
                      </div>
                    </div>                    
                  </div>
                  <div class="col-lg-7">                    
                    <p class="titulo uppercase">About Me</p>
                    <div class="row row-zip-code">
                      <div class="col-lg-12">                    
                         <label for="aboutMe" class="sr-only">About Me</label>
                         <textarea class="form-control" placeholder="About Me" id="aboutMe" rows="7" name="about_me"><?php echo $specialist->getAbout_me() ?></textarea>
                      </div>
                    </div>                    
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-lg-5">                    
                    <p class="titulo uppercase">Languages</p>
                    <div class="row row-zip-code">
                      <div class="col-lg-12 ">
                        <label for="inputLanguages1" class="sr-only">Language 1</label>
                        <input type="text" id="inputLanguages1" class="form-control noMarginTop " placeholder="Language" name="language_1" value="<?php echo $specialist->getLanguage_1()?>">
                      </div>
                      <div class="col-lg-12 ">
                        <label for="inputLanguages2" class="sr-only">Language 2</label>
                        <input type="text" id="inputLanguages2" class="form-control noMarginTop" placeholder="Language" name="language_2" value="<?php echo $specialist->getLanguage_2()?>">
                      </div>
                      <div class="col-lg-12 ">
                        <label for="inputLanguages3" class="sr-only">Language 3</label>
                        <input type="text" id="inputLanguages3" class="form-control noMarginTop " placeholder="Language" name="language_3" value="<?php echo $specialist->getLanguage_3()?>">
                      </div>
                    </div>                    
                  </div>
                  <div class="col-lg-7">                    
                    <p class="titulo uppercase">Additional</p>
                    <div class="row row-zip-code">
                      <div class="col-lg-12 ">
                        <label for="inputAdditional1" class="sr-only">Additional 1</label>
                        <input type="text" id="inputAdditional1" class="form-control noMarginTop " placeholder="Additional Info" name="additional_1" value="<?php echo $specialist->getAdditional_1()?>">
                      </div>
                      <div class="col-lg-12 ">
                        <label for="inputAdditional2" class="sr-only">Additional 2</label>
                        <input type="text" id="inputAdditional2" class="form-control noMarginTop" placeholder="Additional Info" name="additional_2" value="<?php echo $specialist->getAdditional_2()?>">
                      </div>
                      <div class="col-lg-12 ">
                        <label for="inputAdditional3" class="sr-only">Additional 3</label>
                        <input type="text" id="inputAdditional3" class="form-control noMarginTop " placeholder="Additional Info"  name="additional_3" value="<?php echo $specialist->getAdditional_3()?>">
                      </div>
                    </div>                    
                  </div>
                </div>

                <div class="row specialist-certification">
                  <div class="col-lg-12">                    
                    <p class="titulo uppercase">Certifications*</p>
                    <div class="row row-zip-code">
                      <div class="col-lg-12 ">
                        <?php if($specialist->getCert_nursing()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0219-graduation-hat.svg" width="50" alt=""/><span>Undergraduate Degree in Nursing</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_redcross()>0) { ?>
              <div class="box-certification"  style="color:#878787;">
                <img src="img/ico/main/0489-heart-pulse.svg" width="50" alt=""/><span>Red Cross Special Education</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_language()>0) { ?>
              <div class="box-certification"  style="color:#878787;">
                <img src="img/ico/main/0275-book.svg" width="50" alt=""/><span>Teaching Second Language</span>
              </div> 
              <?php } ?>
              <?php if($specialist->getCert_teaching()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0219-graduation-hat.svg" width="50" alt=""/><span>Teaching Professional</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_child_dev()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0634-accessibility.svg" width="50" alt=""/><span>Child Development Training</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_cpcr()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0489-heart-pulse.svg" width="50" alt=""/><span>CPR Certified</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_partner()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0142-star.svg" width="50" alt=""/><span>Approved Partner</span>
              </div>
              <?php } ?>
              <?php if($specialist->getCert_firstaid()>0) { ?>
              <div class="box-certification" style="color:#878787;">
                 <img src="img/ico/main/0493-first-aid.svg" width="50" alt=""/><span>First Aid Certified</span>
              </div>
              <?php } ?>
                      </div>
                    </div>  
                    <p class="legal">**Certifications can only be updated by PAL admin. Submit your certifications to <a href="mailto:talentconcierge@playdatepal.com">talentconcierge@playdatepal.com</a> and refer to the <a href="https://bamboo-magnolia-2mxk.squarespace.com" target="_blank" title="Knowledge Center">Knowledge Center</a>.</p>                  
                  </div>
                </div>

                <div class="row  row-tabs">
                  <div class="col-lg-12">
                     <button class="btn btn-md btn-primary bt-save-profile last" type="submit">Save changes</button>
                  </div>
                </div> 
              </form>
              

            </div>
            
            <div class="tab-pane fade" id="nav-playpacks" role="tabpanel" aria-labelledby="nav-playpacks-tab">
              <div class="row row-tabs">

                <div class="col-lg-5">
                  <div class="playpack-price">
                    <div class="price">$<?php echo Utils::moneyFormat($earned, 2) ?></div>
                    <div class="texto">lifetime earned</div>
                  </div>
                </div>

                <div class="col-lg-7">
                  <?php 
                  if(($specialist->getPay_stripeid()==null) || ($specialist->getPay_stripeid()=="")) {
                  
                      $authorize_request_body = array(
                          'response_type' => 'code',
                          'scope' => 'read_write',
                          'state' => '1234',
                          "client_secret" => $STRIPE_CONNECT_SECRET,
                          'client_id' => $STRIPE_CONNECT_CLIENTID,
                          'redirect_uri' => $PLAYDATE_STRIPE_URI
                      );
                      
                      $url = $STRIPE_AUTHORIZE_URI . '?' . http_build_query($authorize_request_body,"","&");
                      $buttonMsg = "Set Payment Method";
                      $buttonTarget="_self";
                  } else {
                      $url = "/specialist-view-stripe.php";
                      $buttonMsg = "Open Payment Dashboard";
                      $buttonTarget="_blank";
                  }
                  //<a class="bt-top" href="javascript:;" title="" data-toggle="modal" data-target="#paymentInfo">Change Payment Information</a>
                  ?>
                  <a class="bt-top" href="<?php echo $url ?>" title="" target="<?php echo $buttonTarget ?>"><?php echo $buttonMsg ?></a>
                  <?php if($pay_updated) {?>
                  <p class="message-info">
                     New Payment Information Saved
                  </p>
                  <?php } ?>
                </div>
              </div> 
              <div class="row row-tabs">
                  <div class="col-lg-12">
                    <p class="titulo uppercase">Earned by Playdate</p>
                    <div class="row">
                      <div class="col-lg-5" >
                        <div id="date-container-profile">
                          <div class="input-group date">
                            <input type="text" placeholder="MM/DD/YYYY" class="form-control datepicker2">
                          </div>
                          <span> - </span>
                          <div class="input-group date">
                            <input type="text" placeholder="MM/DD/YYYY" class="form-control datepicker2">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row row-tabs">
                      <div class="col-lg-12">
                        <table id="playpack-credit" class="table table-striped" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Date</th>
                                  <th>Playdate</th>
                                  <th>Amount Playdate</th>
                                  <th>Amount Add-Ons</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php 
                          $payments = PaymentDAO::getPaymentListBySpecialist($specialist->getId_specialist());
                          
                          foreach($payments as $auxPay) {
                              
                              if(true || ($auxPay->getStatus()==Payment::$STATUS_COMPLETED)) {
                          ?>
                              <tr>
                                  <td><?php echo Utils::dateFormat($auxPay->getIdate()) ?></td>
                                  <td><?php echo $auxPay->getName() ?></td>
                                  <td>$<?php echo Utils::moneyFormat($auxPay->getAmount_playdate()) ?></td>
                                  <td>$<?php echo Utils::moneyFormat($auxPay->getAmount_addons()) ?></td>
                              </tr>
                          <?php 
                              }
                          }
                          ?>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              </div> 
            </div>
          </div>
        
        </div>
        <!-- Content -->

      </div>


    </div>


    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
    
    <script>
    $(document).ready(function (){
    	
        $("#updatebtn").on("click", function(){
      		$("html, body").animate({ scrollTop: $('#form-comment').offset().top-100}, 1000);
      	});	

    });
    </script>
    <?php if($selectedPlaydate!=null) { ?>
      <script>
        $(document).ready(function (){
          $(".my-dashboard .nav-tabs").css("display","flex");
          $(".my-dashboard .tab-content").fadeIn(function(){
              $("html, body").animate({ scrollTop: $('#selected-playdate').offset().top-300}, 1000);
          }); 
        });     
      </script>
    <?php } ?>
    
    <?php if($selectedPastPlaydate!=null) { ?>
      <script>
        $(document).ready(function (){
          $(".my-dashboard .nav-tabs").css("display","flex");
          $(".my-dashboard .tab-content").fadeIn(function(){
              $("html, body").animate({ scrollTop: $('#selected-past-playdate').offset().top-300}, 1000);
          }); 
        });     
      </script>
    <?php } ?>

  </body>

</html>

<?php 
/**
 * PLAYDATE - MENU
 */

?>
<!-- login -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-signin" action="/login/index.php" method="post">
              <?php if(isset($backUrl) && ($backUrl!=null)) { ?>
              <input type="hidden" name="backurl" value="<?php echo $backUrl?>" />
              <?php } ?>
              <h2 class="form-signin-heading">Member login</h2>
              <label for="inputEmail" class="sr-only">Username or Email</label>
              <input type="text" id="inputEmail" name="inputEmail" class="form-control" placeholder="Username or Email" required="" >
              <label for="inputPassword" class="sr-only">Password</label>
              <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required="">
              <a title="" href="#" class="forgot">Forgot username or password?</a>
              <button class="btn btn-md btn-primary btn-block"  type="submit">Login</button>
              <p class="request">Or request an invite to <a href="parents-sign-up.html" title="">sign up</a>.</p>
            </form>
          </div>
        </div>
      </div>
    </div>
<!-- /login -->

<!-- navigation -->
<nav class="navbar navbar-expand-lg fixed-top" id="mainNav">

      <div class="container">
        <div class="welcome">Welcome back Anna! | <a href="/logout/">Log out</a></div>
        <a class="navbar-brand" title="" href="/"><img alt="" src="img/logo-playdate.png"/></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" title="" href="/playdates.php">Playdates</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" title="" href="/specialists.php">Specialists</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" title="" href="#">Groups</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" title="" href="#">Request Playdate</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" title="" href="#">Get Playpack</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" title="" href="#">My Dashboard</a>
            </li>
            <?php if($isParent || $isSpecialist) { ?>
            <li class="nav-item">
              <a class="nav-link" title=""  href="/logout/">Log out</a>
            </li>
            <?php } else { ?>
            <li class="nav-item">
              <a class="nav-link" title=""  data-toggle="modal" data-target="#login" href="#">Log in</a>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
</nav>
<!-- /navigation -->
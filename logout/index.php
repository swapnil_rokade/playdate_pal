<?php 
/***
 * PLAYDATE - Logout
 * 
 **/
include_once("../classes/all_classes.php");
include("../common.php");

session_destroy();

header("Location: /");

?>
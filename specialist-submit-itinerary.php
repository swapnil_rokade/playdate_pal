<?php 
/**
 * PLAYDATE - SPECIALIST SUBMIT ITINERARY TO PLAYDATE REQUEST 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

if(!$isSpecialist) { //Función solo para especialistas
    header("Location: /");
}
$specialist = SpecialistDAO::getSpecialist($_SESSION["spec_id"]);

$request = new PlaydateRequest();
$request->readFromRow($_REQUEST);
if($request->getId_request()>0) {
    $request = PlaydateRequestDAO::getPlaydateRequest($request->getId_request());
}
if(($request==null) || ($request->getId_request()<=0)) {
    header("Location: /");
}

if(isset($_REQUEST["operation"]) && ($_REQUEST["operation"]=="createplaydate")) {
    //Crear playdate como respuesta
     
    $playdate = new Playdate();
    $playdate->readFromRow($_REQUEST);
    if($playdate->getName()!=null) {
        $playdate->setId_specialist($specialist->getId_specialist());
        $playdate->setId_parent($request->getId_parent());
        $playdate->setDate($request->getDate());
        $playdate->setTime_init($request->getTime_init());
        $playdate->setTime_end($request->getTime_end());
        $playdate->setStatus(Playdate::$STATUS_REQUEST); //respuesta a request
        $playdate->setPrivacy_type(Playdate::$PRIVACITY_PRIVATE); //response to request is always private
        $playdate = PlaydateDAO::createPlaydate($playdate);
        
        //Si la request tenia grupo, generamos la asociacion
        if(($request->getId_group()!=null) && is_numeric($request->getId_group())) {
            PlaydateDAO::addPlaydateToGroup($playdate->getId_playdate(), $request->getId_group());
        }
        
        //Insertamos sus intereses
        if(!empty($_POST['interests'])) {
            foreach($_POST['interests'] as $interest) {
                InterestDAO::createPlaydateInterest($playdate->getId_playdate(), $interest);
            }
        }
        
        //Insertamos las localizaciones
        if(!empty($_POST['id_neighborhood'])) {
            foreach($_POST['id_neighborhood'] as $auxlocation) {
                NeighborhoodDAO::createPlaydateNeighborhood($playdate->getId_playdate(), $auxlocation);
            }
        }
        
        //Creamos una reserva para el padre, si hay hijos seleccionados o extra kids
        if((!empty($_POST['selectedchildren'])) || ((!empty($_POST['num_kids'])))) { //generate reservation
            $reserved = PlaydateDAO::playdateParentReservation($playdate->getId_playdate(), $request->getId_parent());
            $reservation = PlaydateDAO::getParentReservationByPlaydate($request->getId_parent(), $playdate->getId_playdate());
            
            if(!empty($_POST['selectedchildren'])) {
                foreach($_POST['selectedchildren'] as $auxchild) {
                    $kid = ChildrenDAO::getChildren($auxchild);
                    if($kid!=null) {
                        PlaydateDAO::addChildToParentReservation($reservation->getId_reservation(), $kid->getId_children(), 1, $kid->getAge()); //Added age and numchildren
                    }
                }
            }
            
            if(!empty($_POST['num_kids'])) {
                $i=0;
                foreach($_POST['num_kids'] as $auxnum) {
                    $numKids = $auxnum;
                    $age= $_POST['ages'][$i];
                    
                    if(is_numeric($numKids) && is_numeric($age) && ($numKids>0) && ($age>0)){
                        PlaydateDAO::addChildToParentReservation($reservation->getId_reservation(), null, $numKids, $age); //Added age and numchildren, without id_children
                    }
                    $i++;
                }
            }
        } //fi - generate reservation
        
    }
    
    header("Location: /specialist-dashboard.php#requests");
    
}
?>
<!DOCTYPE html>
<html lang="en">

  <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Submit Itinerary - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.datepicker.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page create-playdate<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>

	<?php include("modal/modal-background-check.php") ?>
    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Submit Itinerary</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">

      <!-- All playdates -->
      <div class="row all-playdates">

        <!-- Sidebar -->
        <?php 
        $parent = ParentDAO::getParent($request->getId_parent()); 
        
        ?>
        <div class="col-lg-3 sidebar  my-dashboard">
            <p class="titulo">Requested by</p>
            <div class="box-playdates">
                <div class="img-container">
                  <img src="<?php echo $parent->getPicture() ?>" alt="">
                </div>
                <div class="info">
                  <a href="parent-public-profile.php?id_parent=<?php echo $parent->getId_parent() ?>" class="title"><?php echo $parent->getFullName() ?></a>
                  <span class="subtitle"><?php echo $parent->getUsername() ?></span>
                  <span class="subtitle members">Chelsea, NYC</span>
                </div>
                <div class="data">
                  <p class="titulo">Groups</p>
                  <?php 
                  $parentGrps = GroupDAO::getGroupsByParent($parent->getId_parent());
                  foreach ($parentGrps as $pGrp) {
                  ?>
                  <p><?php echo $pGrp->getName() ?></p>
                  <?php } ?>
                </div>
                <div class="data">
                  <p class="titulo">Children</p>
                  <?php 
                  $childsParent = ChildrenDAO::getChildrenListByParent($parent->getId_parent());
                  foreach ($childsParent as $pchild) {
                      $genderimg = "img/ico/main/0299-baby2.svg";
                      if($pchild->getGenre()=="girl") {
                          $genderimg = "img/ico/main/0298-baby.svg";
                      }
                  ?>
                  <p class="children"><img src="<?php echo $genderimg ?>" width="50" alt=""/> <span class="name"><?php echo $pchild->getName() ?></span> | <span class="age"><?php echo $pchild->getAge() ?></span></p>
                  <?php 
                  } 
                  ?>
                </div>
                <div class="data">
                  <p class="titulo">Availability</p>
                  <p><span class="day">Mon</span> <span class="hour"><?php echo(($parent->getChildren_avb_mon_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_init()):"")?> - <?php echo(($parent->getChildren_avb_mon_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_mon_end()):"")?></span></p>
                  <p><span class="day">Tue</span> <span class="hour"><?php echo(($parent->getChildren_avb_tue_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_init()):"")?> - <?php echo(($parent->getChildren_avb_tue_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_tue_end()):"")?></span></p>
                  <p><span class="day">Wed</span> <span class="hour"><?php echo(($parent->getChildren_avb_wed_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_init()):"")?> - <?php echo(($parent->getChildren_avb_wed_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_wed_end()):"")?></span></p>
                  <p><span class="day">Thu</span> <span class="hour"><?php echo(($parent->getChildren_avb_thu_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_init()):"")?> - <?php echo(($parent->getChildren_avb_thu_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_thu_end()):"")?></span></p>
                  <p><span class="day">Fri</span> <span class="hour"><?php echo(($parent->getChildren_avb_fri_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_init()):"")?> - <?php echo(($parent->getChildren_avb_fri_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_fri_end()):"")?></span></p>
                  <p><span class="day">Sat</span> <span class="hour"><?php echo(($parent->getChildren_avb_sat_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_init()):"")?> - <?php echo(($parent->getChildren_avb_sat_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sat_end()):"")?></span></p>
                  <p><span class="day">Sun</span> <span class="hour"><?php echo(($parent->getChildren_avb_sun_init()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_init()):"")?> - <?php echo(($parent->getChildren_avb_sun_end()!=null)?Utils::get12hourFormat($parent->getChildren_avb_sun_end()):"")?></span></p>
                </div>
              </div>

        </div>
        <!-- Sidebar -->

         <!-- Content -->
        <div class="col-lg-9">
          <form class="form-request-playdate" id="formCreatePlaydate" action="/specialist-submit-itinerary.php" method="post">
          	<input type="hidden" name="operation" value="createplaydate" />
          	<input type="hidden" name="id_request" value="<?php echo $request->getId_request()?>" /> 
            <div class="row">

              <div class="col-lg-4">
                <p class="titulo">Name</p>
                <label for="inputPlaydateName" class="sr-only">Playdate Name</label>
                <input type="text" id="inputPlaydateName" class="form-control noMarginTop" placeholder="Playdate Name" required="required" name="name">
                
                <p class="titulo" style="margin-top:50px">Suggested Area</p>
                <?php 
                $neighs = NeighborhoodDAO::getNeighborhoodsListByRequest($request->getId_request());
                foreach ($neighs as $auxNeighborhood) {
                    $city = NeighborhoodDAO::getCity($auxNeighborhood->getId_city());
                    
                ?>
                <p>
                <?php echo $auxNeighborhood->getName() ?>, <?php echo $city->getName() ?>
                </p>
                <?php
                }
                ?>
              </div>

              <div class="col-lg-4">
                <p class="titulo">Location</p>
                <div class="form-group">
                  <div class="selectdiv">
                    <label for="venue">
                      <select class="form-control" id="venue" name="venue">
                        <option value="venue">Venue</option>
                        <option value="playground">Playground</option>
                        <option value="familyHome">Family Home</option>
                        <option value="publicAttraction">Public Attraction</option>
                        <option value="indoorPark">Indoor Park</option>
                        <option value="dropInClass">Drop In Class</option>
                        <option value="dropInPlayspace">Drop In Playspace</option>
                        <option value="artStudio">Art Studio</option>
                        <option value="fitnessCenter">Fitness Center</option>
                        <option value="educationalInstitution">Educational Institution</option>
                        <option value="communityCenter">Community Center</option>
                        <option value="familyTheatre">Family Theatre</option>
                        <option value="specialEvent">Special Event</option>
                      </select>
                    </label>
                  </div>
                </div> 
                 <div class="form-group">
                  <label for="inputAddress" class="sr-only">Address</label>
                  <input type="text" id="inputAddress" class="form-control noMarginTop" placeholder="Address" name="loc_address" value="">
                </div> 
                 <div class="form-group">
                  <label for="inputCity" class="sr-only">City</label>
                  <input type="text" id="inputCity" class="form-control noMarginTop" placeholder="City" name="loc_city" value="">
                </div> 
                 <div class="form-group">
                  <label for="inputVenue" class="sr-only">State</label>
                  <input type="text" id="inputState" class="form-control noMarginTop" placeholder="State" name="loc_state" value="">
                </div> 
                 <div class="form-group">
                  <label for="inputZipCode" class="sr-only">Neighborhoods</label>
                  <input type="text" id="location" name="location" class="form-control noMarginTop inputLocation" placeholder="neighborhood">
                  <a href="javascript:;" class="bt-save-profile last bt-add-location " data-toggle="modal" data-target="#playdate-reserved">Add Location</a>
                </div> 
                <div class="container-tags">

                 </div> 
              </div>

              <div class="col-lg-4 type-of-care">
                <p class="titulo">Type of care</p>
              
                  <?php 
                  $types = TypecareDAO::getTypecareList();
                  foreach ($types as $auxType) {
                  ?>
              	  <div class="form-group">
                    <input type="radio" name="type" id="<?php echo $auxType->getId_typecare()?>" value="<?php echo $auxType->getId_typecare()?>" class="form-control" <?php echo(($auxType->getId_typecare()==$request->getType())?"checked=\"checked\"":"") ?> style="border-radius: 25px;">
                    <label for="<?php echo $auxType->getId_typecare()?>"><?php echo $auxType->getName() ?> 
                    	<?php if($auxType->getDescription()!=null) { ?>
                    	<a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="<?php echo $auxType->getDescription() ?>"><i class="fa fa-question-circle"></i></a>
                    	<?php } ?>
                    </label>
                  </div> 
              	  <?php 
                  }
              	  ?>
                
              </div>

            </div>

            <div class="row row-tabs">

               <div class="col-lg-4 date-range">
                <p class="titulo">Date</p>
                  <p class="date"><?php echo Utils::dateFormat($request->getDate()) ?>  </p>

              </div>

              <div class="col-lg-4 time-range">
                <p class="titulo">Time</p>
                <p class="time"><span class="initTime" data-time="<?php echo $request->getTime_init() ?>"><?php echo Utils::get12hourFormat($request->getTime_init()) ?></span> - <span class="endTime" data-time="<?php echo $request->getTime_end() ?>"><?php echo Utils::get12hourFormat($request->getTime_end()) ?></span></p>
              </div>

              <div class="col-lg-4">
                <p class="titulo">Confirmed Kids</p>
                <div id="age-container-profile">
                  <?php 
                  $kids = ChildrenDAO::getChildrenListByRequest($request->getId_request());
                  foreach($kids as $auxKid) {
                  ?>
                  <p><?php echo $auxKid->getName() ?> | <?php echo $auxKid->getAge() ?>
                  <input type="hidden" id="child<?php echo $auxKid->getId_children()?>" class="form-control" placeholder="keyword" name="selectedchildren[]" value="<?php echo $auxKid->getId_children()?>" />
                  </p> 
                  <?php   
                  }
                  
                  $childRequests = PlaydateRequestDAO::getChildrenRequests($request->getId_request());
                  
                  foreach($childRequests as $auxReq) {
                      if($auxReq->getId_children()==null) {
                      ?>
                  <p>+<?php echo $auxReq->getNum_children() ?> kid<?php echo(($auxReq->getNum_children()>1)?"s":"")?> | <?php echo $auxReq->getAge() ?> years
                  <input type="hidden" name="num_kids[]" value="<?php echo $auxReq->getNum_children() ?>">
                  <input type="hidden" name="ages[]" value="<?php echo $auxReq->getAge() ?>">
                  </p> 
                  <?php   
                      }
                  }
                  ?>
                </div>
              </div>

          </div>

          <div class="row row-tabs">
            
            <div class="col-lg-4">
              <div class="row">
                <p class="titulo">Categories</p>
                <div class="col">
                  <div class="collapse multi-collapse show" id="categories-container">
                    
					   <?php $interests = InterestDAO::getInterestsIdByRequest($request->getId_request()); ?>
					   <div class="list-container stem">
                        <div class="form-group">
                          <input type="checkbox" id="1" value="1" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(1, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="1">Academic</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="2" value="2" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(2, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="2">STEM</label>
                        </div> 
                      </div>
                      
                      <div class="list-container creative">
                        <div class="form-group">
                          <input type="checkbox" id="3" value="3" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(3, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="3">Creative</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="4" value="4" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(4, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="4">Performing Arts</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="5" value="5" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(5, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="5">Language &amp; Culture</label>
                        </div> 
                      </div>

                      <div class="list-container outdoor">
                        <div class="form-group">
                          <input type="checkbox" id="6" value="6" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(6, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="6">Play</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="7" value="7" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(7, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="7">Sports &amp; Recreation</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="8" value="8" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(8, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="8">Health &amp; Wellness</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="9" value="9" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(9, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="9">Outdoor</label>
                        </div>
                      </div>

                      <div class="list-container attractions">
                        <div class="form-group">
                          <input type="checkbox" id="10" value="10" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(10, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="10">NYC Museums</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="11" value="11" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(11, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="11">NYC Attractions</label>
                        </div> 
                        <div class="form-group">
                          <input type="checkbox" id="12" value="12" class="form-control" name="interests[]" placeholder="keyword" <?php echo(in_array(12, $interests)?"checked=\"checked\"":""); ?>>
                          <label for="12">NYC Theater</label>
                        </div> 
                      </div>                      
                    
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-8">                    
              <p class="titulo">Description</p>
              <label for="descriptionPlaydate" class="sr-only">Description</label>
              <p><?php echo $request->getInfo() ?></p>
              <p></p>
              <textarea class="form-control" placeholder="Describe your playdate" id="descriptionPlaydate" rows="16" name="description"></textarea>                
            </div>

          </div>

          <div class="row row-tabs">
            
            <div class="col-lg-4">
              <p class="titulo">Banners</p>
              <div class="admin-slider">
                <div class="all-slides">
                  <?php /*
                  <div class="slide selected"><img src="img/box-home-1.jpg"></div>
                  <div class="slide"><img src="img/box-home-2.jpg"></div>
                  <div class="slide"><img src="img/box-home-3.jpg"></div>
                  <div class="slide"><img src="img/box-home-4.jpg"></div>
                  */?>
                  <input type="hidden" name="picture" class="image-selected" value="">
                </div>
                <div class="pagination">
                  <ul>
                    <?php /*
                    <li><a href="javascript:;" class="selected">1</a></li>
                    <li><a href="javascript:;">2</a></li>
                    <li><a href="javascript:;">3</a></li>
                    <li><a href="javascript:;">4</a></li>
                    */?>
                  </ul>
                </div>
              </div>
              <p class="titulo">Privacy</p>
                <p>Private - Closed to the public PAL network.</p>
            </div>


            <div class="col-lg-8 itinerary">                    
              <p class="titulo">Itinerary</p>
              <!-- act1 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act1_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act1_time";
                    //$fieldValue = $parent->getChildren_avb_mon_init();
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity1" name="act1_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act1_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act1_note">
                </div>
              </div>
              <!-- /act1 -->
              
              <!-- act2 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act2_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act2_time";
                    //$fieldValue = $parent->getChildren_avb_mon_init();
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity2" name="act2_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act2_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act2_note">
                </div>
              </div>
              <!-- /act2 -->

              <!-- act3 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act3_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act3_time";
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity3" name="act3_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act3_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act3_note">
                </div>
              </div>
              <!-- /act3 -->
            
            <!-- act4 -->
              <div class="row">
                <div class="col-lg-4">
                  <label class="sr-only" for="act4_hour">Hour</label>
                  <div class="selectTime from">
					<?php 
					$fieldName = "act4_time";
                    include("components/input-time-options-advanced.php");
                    ?>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="selectdiv">
                    <label for="activity1">
                      <select class="form-control" id="activity4" name="act4_id">
                        <option value="">Activity</option>
                        <?php 
                        $selectedActivity=null;
                        include("components/input-activity-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                  <label class="sr-only" for="act4_note">notes</label>
                  <input type="text"  placeholder="Notes" class="form-control itinerary-input" name="act4_note">
                </div>
              </div>
              <!-- /act4 -->
            </div>

          </div>

          <div class="row row-tabs">

              <div class="col-lg-4">
                  <p class="titulo">No. of children <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Based on Parent preferences, you can suggest the number of children for this playdate. If no preferences are listed, we recommend opening the maximum number of spots for flexibilty. Remember, as the group size grows, it is up to you to hire Support Specialists to help facilitate this playdate. "><i class="fa fa-question-circle"></i></a></p>
                  <p>&nbsp;</p>
                  <div class="selectdiv">
                    <label for="inputNumChildren">
                      <select class="form-control" id="inputNumChildren" name="num_children">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                      </select>
                    </label>
                  </div>
              </div>

              <div class="col-lg-8 itinerary activityBudget">                    
              <p class="titulo">Activity Budget <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="The Activity Budget is a per child rate that will be charged to each Parent. If you prefer to be paid in cash or using a mobile payment app, please leave this section blank and include your budget preferences in the description portion above. Activity Budgets are not necessary for a successful playdate, but should be considered if there may be admissions, meals or additional supplies associated with the experience."><i class="fa fa-question-circle"></i></a></p>
              <p>min $<?php echo (($request->getBudget_init()>0)?$request->getBudget_init():" -") ?> to max $<?php echo (($request->getBudget_end()>0)?$request->getBudget_end():" -") ?> requested by Parent</p>
              <!-- addon1 -->
              <div class="row">
                <div class="col-lg-9">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add1_id" name="add1_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="text" id="budget1" placeholder="$0" class="form-control  input-budget" name="add1_price">
                </div>
              </div>  
              <!-- /addon1 -->
              
              <!-- addon2 -->
              <div class="row">
                <div class="col-lg-9">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add2_id" name="add2_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="text" id="budget1" placeholder="$0" class="form-control  input-budget" name="add2_price">
                </div>
              </div>  
              <!-- /addon2 -->

              <!-- addon3 -->
              <div class="row">
                <div class="col-lg-9">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add3_id" name="add3_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="text" id="budget1" placeholder="$0" class="form-control  input-budget" name="add3_price">
                </div>
              </div>  
              <!-- /addon3 -->
              
              <!-- addon4 -->
              <div class="row">
                <div class="col-lg-9">
                  <div class="selectdiv">
                    <label for="addons1">
                      <select class="form-control" id="add4_id" name="add4_id">
                        <option value=""></option>
                        <?php 
                        $selectedAddon=null;
                        include("components/input-addon-options.php");
                        ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="col-lg-3">
                  <label class="sr-only" for="budget1">Budget</label>
                  <input type="text" id="budget1" placeholder="$0" class="form-control  input-budget" name="add4_price">
                </div>
              </div>  
              <!-- /addon4 -->
              
            
            </div>

          </div>

          <div class="row row-tabs">

              <div class="col-lg-4">
                  
				<p class="titulo">Support  <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="Use our Suggested Adult to Child Ratio below to hire the appropriate number of Support Specialists. Suggested Age Group Ratios are: Under 3 - 2 children to 1 adult. 3 - 4 years - 4 children to 1 adult. 5 - 7 years - 5 children to 1 adult. 8+ years - 6 children to 1 adult. We strongly suggest keeping this as your minimum team size."><i class="fa fa-question-circle"></i></a></p>
				<p><?php echo $request->getNum_supporters() ?> requested by Parent</p>
                  <div class="selectdiv">
                    <label for="inputNumSupporters">
                      <select class="form-control" id="inputNumSupporters" name="num_supporters">
                        <option value="1">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>
                    </label>
                  </div>
                  
              </div>

              <div class="col-lg-8 itinerary">                    
                <p class="titulo">Supervised travel for up 2 kids <a href="javascript:;" data-toggle="tooltip" class="grey-tooltip" data-placement="top" title="This is only a suggested budget for Supervised Travel. It will not be included in the per child price of this playdate. Payment via cash or mobile cash app must be facilitated by Parents and Specialists directly to cover the cost of travel and travel time (if time is not already included in Playdate itinerary)."><i class="fa fa-question-circle"></i></a></p>
                <p>Pickup: <?php echo $request->getTravel_pickup() ?></p>
                <div class="row">
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputWalk">Walk</label>
                    <input type="text" id="inputWalk" placeholder="$0" class="form-control input-walk" name="pickup_walk">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTrain">Train</label>
                    <input type="text" id="inputTrain" placeholder="$0" class="form-control input-train" name="pickup_train">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputCar">Car</label>
                    <input type="text" id="inputCar" placeholder="$0" class="form-control input-car" name="pickup_car">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTaxi">Taxi</label>
                    <input type="text" id="inputTaxi" placeholder="$0" class="form-control input-taxi" name="pickup_taxi">
                  </div>
                </div>
                <p>Drop Off: <?php echo $request->getTravel_dropoff() ?></p>
                <div class="row">
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputWalk">Walk</label>
                    <input type="text" id="inputWalk" placeholder="$0" class="form-control input-walk" name="dropoff_walk">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTrain">Train</label>
                    <input type="text" id="inputTrain" placeholder="$0" class="form-control input-train" name="dropoff_train">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputCar">Car</label>
                    <input type="text" id="inputCar" placeholder="$0" class="form-control input-car" name="dropoff_car">
                  </div>  
                  <div class="col-lg-3">
                    <label class="sr-only" for="inputTaxi">Taxi</label>
                    <input type="text" id="inputTaxi" placeholder="$0" class="form-control input-taxi" name="dropoff_taxi">
                  </div>
                </div>

              </div>

            </div>

            
          <div class="row row-tabs">
            <div class="col-lg-4">
              <div class="btnSubmit">
                <div class="form-group">
                  <p class="totalPrice">$<span class="initPrice"> </span>-$<span class="endPrice"> </span></p>
                  <p for="checkPublish" class="messagePrice">you earn if 2-3 children book</p>
                </div>
                <button class="btn btn-md btn-primary bt-save-profile last" type="submit">Submit Itinerary</button>
              </div>
            </div>
          </div>

          </form>
        </div>
        <!-- Content -->

      </div>


    </div>



    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->
  	<script>
      checkInterests();
      $(document).ready(function(){
        var initTime = $(".initTime").attr("data-time");
        var endTime = $(".endTime").attr("data-time");
        var totaltime= endTime-initTime;
        console.log(totaltime)
        if(!isNaN(totaltime) && totaltime>0){
          $(".initPrice").html(25*totaltime);
          $(".endPrice").html(30*totaltime);
          $(".messagePrice").html("you earn if 2-3 children book")
        }else{
          $(".initPrice").html(" ");
          $(".endPrice").html(" ");
          $(".messagePrice").html("select playdate time to calculate")
        }
      })
      $('#formCreatePlaydate').on('submit', function(event) {
        event.preventDefault(); 
        $(".activityBudget .row").each(function(e){
          selectVal = $(this).find("select").val();
          inputVal = $(this).find("input").val();
          if(selectVal != ""){
            if(inputVal ==""){
              activityBudgetError("selectVal")
              return false;
            }          
          }else if(inputVal != ""){
            if(selectVal ==""){            
              activityBudgetError("inputVal");
              return false;
            }          
          }else{
            $('#formCreatePlaydate')[0].submit();
          }
        })
    });

    $(".activityBudget .row select").change(function(){
      $(".errorLocation").remove();
      if($(this).val()!=""){
        if($(this).parent().parent().parent().parent().find("input").val()==""){
          $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you select an option, you have to set its price</p>");
        }
      }else{
        if($(this).parent().parent().parent().parent().find("input").val()!=""){
          $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you select an option, you have to set its price</p>");
        }
      }
    })

    $(".activityBudget .row input").blur(function(){
      $(".errorLocation").remove();
      if($(this).val()!=""){
        if($(this).parent().parent().find("select").val()==""){
          $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you set a price, you have to select its option</p>");
        }
      }else{
        if($(this).parent().parent().find("select").val()!=""){
          $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you set a price, you have to select its option</p>");
        }
      }
    })

    function activityBudgetError(errorMsg){
      $(".errorLocation").remove();
      if(errorMsg=="selectVal"){
        $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you select an option, you have to set its price</p>");
      }else{
        $("#formCreatePlaydate .activityBudget .titulo").after("<p class='errorLocation centered'>If you set a price, you have to select its option</p>");
      }
      $("#formCreatePlaydate button[type='submit']").after("<p class='errorLocation centered'>Review Activity Budget to continue</p>");
    }
      
    </script>
  </body>

</html>

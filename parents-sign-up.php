<?php 
/**
 * PLAYDATE - PARENT SIGN UP 
 */

include_once("classes/all_classes.php");
include_once("common.php");

include_once("connection.php");

$newparent = new ParentPd();

$codeError = null;
$code = "";
$isInvitationCreated = false; //If we create invitation, we will use this flag to show message

if(isset($_REQUEST["action"])){
    
    if($_REQUEST["action"]=="register") {

        $newparent->readFromRow($_REQUEST);
        
        //TEST INVITATION CODE
        
        // if(isset($_REQUEST["code"]) && ($_REQUEST["code"]!=null)) {
        
        //     $code = $_REQUEST["code"];
    	// } 
        
       // $invitation = InvitationDAO::getInvitationByCode($code);
        
        //if(($invitation!=null) && ($invitation->getEmail() == $newparent->getEmail())) {
    		
        
            if($newparent->getEmail()!=null) {
                $newparent = ParentDAO::createParent($newparent);
                $_SESSION['login'] = 0;
                //echo "<pre>";print_r($newparent->getEmail());die();
                MailUtils::sendParentWelcome($newparent->getEmail(), $newparent->getName());
                
            }
        
            if($newparent->getId_parent()>0) {
                //if registered, login and go to dashboard
                
                $_SESSION['parent_id'] = $newparent->getId_parent();
                $_SESSION['parent_name'] = $newparent->getFullName();
                
                header("Location: /welcome.php");
                //header("Location: /my-dashboard.php#profile");
            }
        // } else {
    	// 	//Invitation code error (does not exist or not associated to this email)
        //     $codeError = 1;
        // }
    } else if($_REQUEST["action"]=="request") { 
        //GET REQUEST INFO (data fields are same as parent, so we use ParentPd object)
        $newParent = new ParentPd();
        $newParent->readFromRow($_REQUEST);
        $isInvitationCreated = InvitationDAO::createRequestInvitation($newParent);
        MailUtils::sendRequestAccess($newParent->getName(), $newParent->getEmail());
    }
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign Up - PAL by Project Playdate</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/styles.css?v=<?php echo $RELEASE_VERSION ?>" rel="stylesheet">
	<?php include ('components/google-tag.php'); ?>
  </head>

  <body class="simple-page<?php echo($isConnected?" connected":"") ?>">
    <?php include ('components/menu.php'); ?>

    <!-- Page Header -->
    <header class="masthead">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="site-heading">
              <h1>Parent sign up</h1>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">



    <!-- Featured Specialists -->
    <div class="row intro-row">
        
        <div class="col-lg-6 texto vcenter">
          <h2>Join the playdate family.</h2>
          <p>PAL connects families with shared child care needs to a network of highly selective child care providers. This invite-only platform enables Parents to split the cost of care so that they can create well-paid job opportunities for the most qualitied and talented Specialists.</p>
        </div>
        <div class="col-lg-6 imagen">
          <img src="img/intro-parents.jpg" alt="">
        </div>
    </div>

      <div class="row row-eq-height how-to contact-form parent">
        <div class="col-lg-12 vtop">
          <div class="content-title">
            <h2 class="title-in">Sign up</h2>
            <div class="row texto">
              <div class="col-lg-12">
                <form id="contact-form" name="contact-form" method="post" action="/parents-sign-up.php" role="form">
                	<input type="hidden" id="action" name="action" value="register" />
                	<div class="controls">
                       <div class="row">
                            <div class="col-lg-4">&nbsp;</div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="sr-only" for="form_code">Access Code*</label>
                                    <input id="form_code" type="text" name="code" class="form-control" placeholder="Access Code*" value="<?php echo $code ?>">
                                    <?php if($codeError>0) { ?><span style="color:lightgray">This is not a valid invitation code for your email. Try again.</span><?php } ?>
                                </div>
                            </div>
                            <div class="col-lg-4">&nbsp;</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="form_name">Name</label>
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Name" required="required" data-error="Firstname is required." value="<?php echo $newparent->getName() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="lastname">Last Name</label>
                                    <input id="form_lastname" type="text" name="lastname" class="form-control" placeholder="Last Name" required="required" data-error="Last Name is required." value="<?php echo $newparent->getLastname() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="username">Username</label>
                                    <input id="form_username" type="text" name="username" class="form-control" placeholder="Username" required="required" data-error="Username is required." value="<?php echo $newparent->getUsername() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="sr-only" for="email">Email</label>
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required." value="<?php echo $newparent->getEmail() ?>">
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="zipcode">Zip code</label>
                                    <input id="form_zipcode" type="text" name="zipcode" class="form-control inputLocation" placeholder="Zip Code or neighborhood" required="required" data-error="Zip code is required." value="<?php echo $newparent->getZipcode() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="sr-only" for="password">Password</label>
                                    <input id="form_password" type="password" name="password" class="form-control" placeholder="Password" required="required" data-error="Password is required." value="<?php echo $newparent->getPassword() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                  <input type="checkbox" id="terms" class="form-control" placeholder="keyword" required="required" >
                                  <label for="terms">I accept <a class="terms" href="terms-of-service.php" target="_blank">terms and conditions</a></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-send" value="Gain Access">
                            </div>
                        </div>
                        <div class="row request-access-row">
                            <div class="col-lg-2">&nbsp;</div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                   <a href="javascript:requestInvitation();" class="bt-request-access" title="">Not Yet Invited? Request Access</a>
                                </div>
                            </div>
                            <div class="col-lg-2">&nbsp;</div>
                        </div>
                        <?php if($isInvitationCreated) { ?>
                        <div class="row request-access-row">
                            <div class="col-lg-2">&nbsp;</div>
                            <div class="col-lg-8" id="message" style="text-align:center;color:white;background-color:#815087;padding: 40px 20px;">
                                Your request was sent successfully. We will get in touch shortly.
                            </div>
                            <div class="col-lg-2">&nbsp;</div>
                        </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-lg-12">
                              <p class="legal">*PAL is an invite-only platform to maintain the safety and integrity of this community. If you have not yet received an access code, simply request access at the link above and we will contact you to verify that you are an NYC parent.</p>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>

    </div>

    <!-- TESTIMONIALS -->
    <div class="row playdates-testimonials">
        <div class="col-lg-12">
          <div class="titulo">
            <div class="testimonio">
              <p class="texto">The kids loved it! We moved to NY in April and finding kids for them to play with has been not easy. This was exactly what they needed. The caregivers were outstanding. We have had trouble finding someone our kids were comfortable staying with. My husband and I were thrilled that both kids ran off to have fun and didn't even look back. We will definitely be back and will certainly recommend and support your program anyway that we can.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">My husband and I are thrilled. Our son had a great time. We had a great time - guilt free! This is just an all around a win-win situation.</p>
              <p class="name">Project Playdate Parent</p>
            </div>
            <div class="testimonio">
              <p class="texto">This is awesome! Your service was unparalleled by anything else we've experienced.</p>
              <p class="name">Maryam, Playdate Partner Host</p>
            </div>
            <div class="testimonio">
              <p class="texto">Project Playdate is a wonderful service. My husband and I were so grateful to have a night out and to know that our son was in good hands. Now how do we get Project Playdate to come to California?!</p>
              <p class="name">Shenna Deveza, Mom of One, Union Square</p>
            </div>
             <ul class="nav-testimonios"></ul>
          </div>

        </div>
    </div>
    <!-- FIN TESTIMONIALS-->
	<script>
		function requestInvitation() {
			$('input#action').val("request");
			$('form#contact-form').submit();
		}

	</script>

    <!-- Footer -->
    <?php include ('components/footer.php'); ?>
    <!-- /Footer -->

		<?php if($isInvitationCreated) { ?>
		<script>
		$(document).ready(function (){
			$("html, body").animate({ scrollTop: $('#message').offset().top-300}, 1000);
		});
		</script>
		<?php } ?>

  </body>

</html>

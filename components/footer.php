    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <a class="logo-footer" title="" href="#"><img src="img/logo-playdate.png"/></a>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <ul class="menu-footer">
                <li><a href="/playdates.php" title="">Playdates</a></li>
                <li><a href="/specialists.php" title="">Specialists</a></li>
                <li><a href="/groups.php" title="">Groups</a></li>
                <!--li><a href="/about-us.php" title="">About us</a></li-->
                <li><a href="/contact-us.php" title="">Contact us</a></li>
                <li><a href="/help-center.php" title="">FAQ's</a></li>
           </ul>
          </div>
        </div>
      </div>
      <hr>
      <div class="container">
        <div class="row legal">
          <div class="col-lg-6">
            <ul class="copy">
              <li>&copy; Playdate Labs 2018</li>
              <li><a href="terms-of-service.php" title="">Terms</a></li>
              <li><a href="privacy.php" title="">Privacy</a></li>
            </ul>
          </div>
          <div class="col-lg-6">
            <ul class="redes">
              <li><a href="https://www.facebook.com/PALbyprojectplaydate/" title="Facebook" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
              <li><a href="https://twitter.com/PALbyplaydate" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.linkedin.com/company/palbyprojectplaydate/" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <hr class="last">
      <div class="container">
        <div class="row legal">
          <div class="col-lg-12">
            <p>We do not provide any of the services being offered or rendered by providers (the "provider services"). Our service is limited to a means of communicating, coordinating and transacting with other parties. If our service is used to coordinate the purchase of provider services, it is up to the provider to offer their provider services, and for the customer to determine the suitability or safety of such provider services. We have no responsibility or liability relating to any provider services, any customer payment, or the actions or conduct of any user.</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.datepicker.min.js"></script>
    <script src="vendor/bootstrap/js/jquery.dataTables.min.js"></script>
    <script src="vendor/bootstrap/js/dataTables.bootstrap.min.js"></script>
    
	<!-- playdate scripts -->
	<?php 
	$srcVersion = "1.8";
	if(isset($RELEASE_VERSION)) {
	    $srcVersion = $RELEASE_VERSION;
	}
	?>
	<script src="js/scripts.js?v=<?php echo $srcVersion ?>"></script>
	
	<?php if(isset($_REQUEST["error"]) && ($_REQUEST["error"]==1)) { ?>
	<script>$('#login').modal('show');</script>
	<?php } ?>
    
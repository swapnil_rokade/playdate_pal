<?php 
/**
 * OPTIONS FOR TIME SELECT - EVERY 30 MINUTES
 * PARAMS:  $fieldName, $fieldValue
 */

$formValues= array("8", "8:30", "9", "9:30", "10", "10:30", "11", "11:30", "12", "12:30", "13", "13:30", "14", "14:30", "15", "15:30", "16", "16:30", "17", "17:30", "18", "18:30", "19", "19:30", "20", "20:30", "21", "21:30", "22", "22:30", "23", "23:30", "24");
if((!isset($fieldValue)) || ($fieldValue==null)) {$fieldValue="";}
?>
<?php if($fieldValue!="") { ?>
<a href="javascript:;" data-time="<?php echo $fieldValue ?>" class="selected"><img  src="img/ico/main/0747-alarm2.svg" width="50" alt=""/><?php echo Utils::get12hourFormatInput($fieldValue) ?></a>
<?php } else { ?>
<a href="javascript:;" data-time="" class="selected"><img  src="img/ico/lightgray/0747-alarm2.svg" width="50" alt=""/>---</a>
<?php } ?> 
<a href="javascript:;" data-time=""><img  src="img/ico/lightgray/0747-alarm2.svg" width="50" alt=""/>---</a>
  
  <?php foreach ($formValues as $auxValue) { ?>
  <a href="javascript:;" data-time="<?php echo $auxValue ?>"<?php //echo(($auxValue==$fieldValue)?" class=\"selected\"":"") ?>><img  src="img/ico/main/0747-alarm2.svg" width="50" alt=""/><?php echo Utils::get12hourFormatInput($auxValue) ?></a>
  <?php } ?>
  <input type="hidden" name="<?php echo $fieldName ?>" class="inputSelectTime" value="<?php echo $fieldValue ?>"/>
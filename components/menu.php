<?php 
/**
 * PLAYDATE - MENU
 */
?>
<?php if(!$isConnected) { //NOT CONNECTED - LOGIN MODAL ?>
<!-- login -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-signin" id="form-login" action="/login/index.php" method="post">
              <?php if(isset($backUrl) && ($backUrl!=null)) { ?>
              <input type="hidden" name="backurl" value="<?php echo $backUrl?>" />
              <?php } ?>
              <h2 class="form-signin-heading">Member login</h2>
              <label for="inputEmail" class="sr-only">Username or Email</label>
              <input type="text" id="inputEmail" name="inputEmail" class="form-control" placeholder="Username or Email" required="" >
              <label for="inputPassword" class="sr-only">Password</label>
              <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required="">
              <?php if(isset($_REQUEST["error"]) && ($_REQUEST["error"]==1)) { ?>
              <p class="loginError">Wrong email or password.</p>
              <?php } ?>
              <a title="" href="#" class="forgot">Forgot username or password?</a>
              <button class="btn btn-md btn-primary btn-block"  type="submit">Login</button>
              <p class="request">Or request an invite to <a href="/parent-sign-up.php" title="">sign up</a>.</p>
            </form>
            <form class="form-signin" id="form-recovery" action="/login/index.php" method="post">
              <input type="hidden" name="action" value="reset-password" />
              <h2 class="form-signin-heading">FORGOT YOUR PASSWORD</h2>
              <label for="inputEmailRecovery" class="sr-only">Email</label>
              <p>Enter your username or email address to reset your password.</p>
              <input type="text" id="inputEmailRecovery" name="inputEmailRecovery" class="form-control" placeholder="Email" required="required">              
              <button class="btn btn-md btn-primary btn-block"  type="submit">Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
<!-- /login -->
<?php } ?>
<?php if(false && $isConnected) { //CONNECTED - LAST PLAYDATE VAL ?>
    <!-- last playdate -->
    <div class="modal fade" id="last-playdate" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-last-playdate">
              <h2 class="form-last-playdate-heading">How was your last playdate? </h2>
              <div class="playdate-info">
                <div class="titulo">Bronx Zoo Day Trip</div>
                <div class="info">
                  <span class="fecha">Jan 13th 2018</span> | <span class="hour">12pm-2pm</span>
                  <span class="place">Bronx, NYC</span>
                </div>
              </div>
              <div class="specialist">
                <div class="name">Monica Allen</div>
                <div class="puntuacion">
                  <ul>
                    <li id="voto1"></li>
                    <li id="voto2"></li>
                    <li id="voto3"></li>
                    <li id="voto4"></li>
                    <li id="voto5"></li>
                  </ul>
                  <span class="num">0/5</span>
                </div>
              </div>
              <div class="col-lg-12">                    
                     <label for="aboutSpecialist" class="sr-only">About Group</label>
                     <textarea class="form-control" id="aboutSpecialist" placeholder="Tell us about the specialist. Your feedback will be public in the specialist's page." rows="4"></textarea>
                     <label for="aboutPlaydate" class="sr-only">About Group</label>
                     <textarea class="form-control" id="aboutPlaydate" placeholder="Tell us about the playdate. Your feedback will be puclic in the playdate comments section." rows="4"></textarea>
                </div>
              <button class="btn btn-md btn-primary" type="submit">Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- last playdate -->
<?php } ?>

<!-- navigation -->
<nav class="navbar navbar-expand-xl fixed-top" id="mainNav">

      <div class="container">
      	<?php if(isset($_SESSION['spec_name'])) { ?>
        <div class="welcome">Welcome back, <?php echo $_SESSION['spec_name']?>! &nbsp;&nbsp;|&nbsp;&nbsp; <a href="/logout/">Log out</a></div>  
        <?php } else if(isset($_SESSION['parent_name'])) { ?>
        <div class="welcome">Welcome back, <?php echo $_SESSION['parent_name']?>! &nbsp;&nbsp;|&nbsp;&nbsp; <a href="/logout/">Log out</a></div>
      	<?php } ?>
        <a class="navbar-brand" title="" href="/"><img alt="" src="img/logo-playdate.png"/></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" title="" href="/playdates.php">Playdates</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" title="" href="/specialists.php">Specialists</a>
            </li>
            <?php if(!$isConnected) { ?>
            
            <li class="nav-item">
              <a class="nav-link" title="" href="/apply-specialist.php">Work with us</a>
            </li>
            <?php } ?>
            <!--li class="nav-item">
                  <a class="nav-link" title="" href="/groups.php">Groups</a>
            </li-->
            <li class="nav-item">
                  <a class="nav-link" title="" href="/about-us.php">Safety</a>
            </li>
            <?php if($isLead) { ?>
            <li class="nav-item">
                  <a class="nav-link" title="" href="/specialist-job-board.php">Job Board</a>
            </li>
            <?php } else if($isSupporter) { ?>
            <li class="nav-item">
                  <a class="nav-link" title="" href="/supporter-job-board.php">Job Board</a>
            </li>
            <?php } ?>
            <?php if($isParent || $isSpecialist) { ?>
                
                <?php if($isParent) { ?>
                <li class="nav-item">
                  <a class="nav-link" title="" href="/parent-request-playdate.php">Request Playdate</a>
                </li>
                <?php } else if($isLead) { //Create Playdate only for leads ?>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" title="">Create Playdate</a>
                  <div class="dropdown-menu">
                      <a class="dropdown-item" href="/specialist-create-playdate.php">One Off</a>
                      <a class="dropdown-item" href="/specialist-create-recurring-playdate.php">Recurring</a>
                  </div>
                </li>
                <?php } else if($isSupporter){  ?>
                <li class="nav-item">
                  <a class="nav-link" title="You must be a Lead Specialist to create a playdate!" href="javascript:alert('You must be a Lead Specialist to create a playdate!')">Create Playdate</a>
                </li>
                <?php }  ?>
                
                <?php if($isParent) { ?>
                <li class="nav-item">
                  <a class="nav-link" title="" href="/get-playpacks.php">Get Playpack</a>
                </li>
                <?php } ?>
                
                <?php if($isParent) {
                  $completed = ParentDAO::isCompletedProfile($_SESSION['parent_id']);
                  if(!$completed) { 
                  ?>
                <li class="nav-item">
                  <a class="nav-link" title="" href="/my-dashboard.php#nav-profile">My Dashboard</a>
                </li>
                  <?php } else {?>
                    <li class="nav-item">
                  <a class="nav-link" title="" href="/my-dashboard.php#nav-current">My Dashboard</a>
                </li>
                <?php } } else if($isSpecialist) { ?>
                <li class="nav-item">
                  <a class="nav-link" title="" href="/specialist-dashboard.php">My Dashboard</a>
                </li>
                <?php } ?>
                
                <?php /* ?>
                <li class="nav-item">
                  <a class="nav-link" title=""  href="/logout/">Log out</a>
                </li>
                */?>
            <?php } else { ?>
            <li class="nav-item">
              <a class="nav-link" title=""  href="/parent-sign-up.php">Sign up</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" title=""  data-toggle="modal" data-target="#login" href="#">Log in</a>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
</nav>
<!-- /navigation -->
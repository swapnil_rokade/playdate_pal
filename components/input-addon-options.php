<?php 
/**
 * OPTIONS FOR ADDON SELECT
 * PARAMS:  $selectedAddon = null
 */

if(!isset($selectedAddon)) {
    $selectedAddon = -1;
}

$addons = AddonDAO::getAddonList();
foreach ($addons as $auxAdd) {
?>
<option value="<?php echo $auxAdd->getId_addon() ?>" <?php echo(($selectedAddon==$auxAdd->getId_addon())?"selected=\"selected\"":"")?>><?php echo $auxAdd->getName() ?></option>
<?php } ?>